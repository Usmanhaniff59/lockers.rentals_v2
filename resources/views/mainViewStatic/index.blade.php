<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>OnePage Bootstrap Template - Index</title>
    <meta content="" name="descriptison">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="mainViewStatic/img/favicon.png" rel="icon">
    <link href="mainViewStatic/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="mainViewStatic/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="mainViewStatic/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="mainViewStatic/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="mainViewStatic/vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="mainViewStatic/vendor/venobox/venobox.css" rel="stylesheet">
    <link href="mainViewStatic/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="mainViewStatic/vendor/aos/aos.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="mainViewStatic/css/style.css" rel="stylesheet">
    <script src="https://kit.fontawesome.com/c167da1aca.js" crossorigin="anonymous"></script>
    <!-- =======================================================
    * Template Name: OnePage - v2.0.0
    * Template URL: https://bootstrapmade.com/onepage-multipurpose-bootstrap-template/
    * Author: BootstrapMade.com
    * License: https://bootstrapmade.com/license/
    ======================================================== -->
    <style>
        .section-bg{
            background-color :#87c1ea;
        }
        .ProximaNova-Black{
            font-family: 'ProximaNova-Black';
            color: black;
        }
        .ProximaNova-Regular{
            font-family: 'ProximaNova-Regular';
            color: black;
        }
        .ProximaNova-Bold{
            font-family: 'ProximaNova-Bold';
            color: black;
        }
        .Gotham-Black{
            font-family: 'Gotham-Black';
            color: black;
        }

        @font-face {
            font-family: 'ProximaNova-Black';
            src: url( '/mainViewStatic/css/FontsFree-Net-Proxima-Nova-Black.otf');


        }
        @font-face {

            font-family:'proximanova-regular';

            src: url('/mainViewStatic/css/proximanova-regular.otf');
        }
        @font-face {

            font-family: 'proximanova-bold';

            src: url('/mainViewStatic/css/proximanova-bold.otf');
        }
        @font-face {

            font-family: 'Gotham-Black';

            src: url('/mainViewStatic/css/Gotham-Black.otf');
        }

        .img-fluid {
            width: 100%;
            height: 300px;
            max-height: 100%;
        }
        .img-fluid-logo {
            width: 100%;
            height: 200px;
            max-height: 100%;

        }
        .img-fluid-icon {
            width: 100px;
            height: 120px;
            max-height: 100%;
        }
        .circle{
            height: 25px;
            width: 25px;
            background-color: rgb(44, 85, 167);
            border-radius: 100%;
            color: white;
            display: inline-block;
            padding: 2px 5px;

        }
        .light-pink{
            color: rgb(238, 225, 199);

        }
        .red{
            color: rgb(179, 5, 16);

        }
        .gray{
            color: rgb(154, 189, 185);

        }
        .light-blue{
            color: rgb(147, 170, 204);

        }
        .yellow{
            color: rgb(238, 222, 138);

        }
        .dark-yellow{
            color: rgb(244, 185, 45);

        }
        .blue{
            color: rgb(44, 85, 167);

        }
        .line{
            border-top: 1px solid red;
        }
        .button{
            color: white;
            background-color: rgb(154, 189, 185);
            border-color:rgb(154, 189, 185);
            height: 25%;
            padding: 1rem 0.75rem;
        }
        .border {
            display: inline-block;

            margin: 1%;
            padding: 2%;
        }
        .topmminus{
            transform: translateY(-20%)
        }
        @media screen and (max-width: 992px){
            .topmminus{
                transform: translateY(0%)
            }
        }
        #header{
            background-image: url("/mainViewStatic/img/color-strip.png");
            background-size: contain;
            background-repeat: no-repeat;
            background-position: bottom;
        }

    </style>
</head>

<body>

<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

        <!--<h1 class="logo mr-auto"><a href="index.html">OnePage</a></h1>-->
        <!-- Uncomment below if you prefer to use an image logo -->

        <a href="index.html" class="logo " style="margin-left: 9%;margin-right: 6%"><img src="mainViewStatic/img/lockers/Secure_Logo_RGB_03.png" alt="" class="img-fluid"></a>

        <nav class="nav-menu d-none d-lg-block text-center">
            <ul>
                <li class="active Gotham-Black" style="color: black"><a href="#work" style="color: black">HOW IT WORKS</a></li>
                <li class="Gotham-Black" style="color: black"><a href="#role" style="color: black">YOUR ROLE</a></li>
                <li class="Gotham-Black" style="color: black"><a href="#financial" style="color: black">FINANCIALS</a></li>
                <li class="Gotham-Black" style="color: rgb(179, 5, 16)"><a href="#brochure" style="color: rgb(179, 5, 16)">REQUEST A FREE BROCHURE</a></li>

            </ul>
        </nav><!-- .nav-menu -->



    </div>
</header><!-- End Header -->



<main id="main">

    <!-- ======= main page ======= -->

    <section id="about" class="about" style="background-color:  rgb(154, 189, 185);background-image: url(/mainViewStatic/img/color-strip.png); background-size: contain; background-repeat: no-repeat;padding-top: 5%" >

        <div class="container" data-aos="fade-up">
            <div class="row content" style="padding-top: 3%">

                <div class="col-lg-1"></div>
                <div class="col-lg-4">
                    <br>

                    <h6 class="ProximaNova-Black" style="color: rgb(238, 225, 199);font-size: 16px">
                        UNLOCK YOUR TRUE POTENTIAL.
                    </h6>
                    <h1 class="ProximaNova-Black" style="">
                        A FRANCHISE CONCEPT UNLIKE ANY OTHER.
                    </h1>
                    <h6 class="ProximaNova-Regular" style="font-size: 14px;">Very few businesses offer you ‘residual income’ (that’s income that keeps coming in year after year with minimal work from you) but this one does!  This business redefines ‘part time’ to mean ‘letting the business run itself’. Sure, there’s some work to do getting things set up, but once you’re up and running this is a franchise that can be run alongside any other business or commitments with very minimal input from you. Perhaps you’re just ready to slow things down and enjoy more leisure time?</h6>
                    <h6 class="ProximaNova-Black" style="color: rgb(238, 225, 199);font-size: 20px">Either way, this could be the perfect fit.</h6>
                </div>
                <div class="col-lg-6">
                    <br>
                    <div style="transform:translate(-7%,0%);">
                        <img src="mainViewStatic/img/lockers/Header_Locker_v1.png" class="img-fluid" alt="">
                    </div>
                </div>
                <div class="col-lg-1"></div>

            </div>

        </div>
    </section><!-- End About Section -->

    <!-- ======= how it works======= -->
    <section id="work" class="about-video">

        <div class="container" data-aos="fade-up">

            <div class="row" >
                <div class="col-lg-1"></div>
                <div class="col-lg-5 video-box align-self-baseline text-left" data-aos="fade-right" data-aos-delay="100">

                    <h2 class="ProximaNova-Black blue" style="font-size: 26px">HOW DOES IT WORK?</h2>
                    <h6 class="ProximaNova-Regular" style="font-size: 14px">As with all great business concepts, the idea is really simple. The key to a successful business is to first identify a need, and then provide a cost-effective solution. If you can make it work for all parties then you’re onto a winner</h6>

                </div>

                <div class="col-lg-5 pt-3 pt-lg-0 content" data-aos="fade-left" data-aos-delay="100">
                    <div class="row text-right">
                        <div class="col-lg-4 text-center">
                            <img src="mainViewStatic/img/lockers/man-icon.png" class="img-fluid-icon" alt="">
                            <h4 CLASS="Gotham-Black blue" style="font-size: 13.5px">INSTALL</h4>
                        </div>
                        <div class="col-lg-4 text-center">
                            <img src="mainViewStatic/img/lockers/4-StageIcons-2.png" class="img-fluid-icon"   alt="">
                            <h4 CLASS="Gotham-Black blue" style="font-size: 13.5px">RENT</h4>
                        </div>
                        <div class="col-lg-4 text-center">
                            <img src="mainViewStatic/img/lockers/Earn.png" class="img-fluid-icon"   alt="">
                            <h4 CLASS="Gotham-Black blue" style="font-size: 13.5px">EARN</h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-1"></div>

            </div>

        </div>
    </section><!-- End About Video Section -->

    <!-- ======= images/budget======= -->
    <section id="about-video2" class="about-video">
        <div class="" data-aos="fade-up">

            <div class="row">

                <div class="col-lg-6 video-box align-self-baseline" style="padding-right: 0%;padding-left: 0%;" data-aos="fade-right"  >
                    <div   style="background-image: url('mainViewStatic/img/lockers/image--007.jpg'); background-size: cover; background-repeat: no-repeat;" class="img-fluid" ></div>
                </div>

                <div class="col-lg-6 pt-3 pt-lg-0 content" style="padding-right: 0%;padding-left: 0%" data-aos="fade-left" data-aos-delay="100">
                    <div   style="background-image: url('mainViewStatic/img/lockers/image--002.jpg'); background-size: cover; background-repeat: no-repeat;"   class="img-fluid">
                        <div class="content-overlay" style="background: rgba(255, 173, 35, 0.8); height: 100%; padding: 15px 20px;">
                            <div class="row">

                                <div class="col-1"></div>

                                <div class="col-7">
                                    <h6 class="ProximaNova-Black" style="font-size: 14px;">THE PROBLEM? </h6>
                                    <h4 class="ProximaNova-Black" style="color: #fff;font-size: 26px">HEALTH. SECURITY. BUDGET.</h4>
                                    <p class="ProximaNova-Regular" style="font-size: 14px;">
                                        These days, schoolchildren and university students carry a lot of expensive equipment around. From £100+ trainers to iPhones and Laptops. All of this kit is heavy and valuable, so ideally they’d like somewhere secure to put it whilst they’re at school. Parents regularly put pressure on schools to install lockers. They want good sized, high quality lockers with decent locks. The problem is that most schools have very limited budgets and installing potentially over 1,000 high quality lockers (and maintaining them) comes at enormous cost, which they simply can’t afford.
                                    </p>
                                </div>
                                <div class="col-4"></div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <div class="row">

                <div class="col-lg-6 video-box align-self-baseline" style="padding-right: 0%;padding-left: 0%" data-aos="fade-right"   >

                    <div style="background-image: url(mainViewStatic/img/lockers/image--001.jpg); background-size: cover; background-repeat: no-repeat; height: 300px">
                        <div class="content-lay" style="background: rgba(147, 170, 204, 0.8); height: 100%;  padding: 15px 20px;">
                            <div class="row">
                                <div class="col-4"></div>
                                <div class="col-7">
                                    <h6 class="ProximaNova-Black" style="font-size: 14px;">THE SOLUTION? </h6>
                                    <h4 class="ProximaNova-Black" style="color: #fff;font-size: 26px">SIMPLE. SECURE. FREE. </h4>
                                    <p class="ProximaNova-Regular" style="font-size: 14px;">
                                        At Secure Locker Rentals, we install the lockers free of charge to the school. We then make these lockers available for parents to rent on an annual basis at very low cost. Parents book their lockers online using our secure portal, and we collect their payments.
                                    </p>
                                    <b class="ProximaNova-Black" style="font-size: 14px;">The parents are happy, the school is delighted, the students’ kit is secure and everybody wins!</b>
                                    <h4 class="ProximaNova-Black border" style="color: #fff;font-size: 16px;">REQUEST A FREE BROCHURE</h4>
                                </div>
                                <div class="col-1"></div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 pt-3 pt-lg-0 content" style="padding-right: 0%;padding-left: 0%" data-aos="fade-left" data-aos-delay="100">

                    <div style="background-image: url(mainViewStatic/img/lockers/image--004.jpg); background-size: cover; background-repeat: no-repeat;" class="img-fluid">

                    </div>
                </div>

            </div>

        </div>
    </section><!-- End About Video Section -->

    <!-- ======= Your role======= -->
    <section id="role" class="about-video">
        <div class="container" data-aos="fade-up">

            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-5 video-box align-self-baseline text-left" data-aos="fade-right" data-aos-delay="100">
                    <span class="ProximaNova-Black" style="">YOUR ROLE?</span>
                    <h3 class="ProximaNova-Black blue" style="font-size: 26px">THE ROLE OF A FRANCHISEE FALLS INTO THREE SIMPLE PARTS</h3>
                    <h6 class="ProximaNova-Regular" style="font-size: 14px">As with all great business concepts, the idea is really simple. The key to a successful business is to first identify a need, and then provide a cost-effective solution. If you can make it work for all parties then you’re onto a winner!</h6>
                    <div class="row">
                        <div class="col-lg-4">
                            <img src="mainViewStatic/img/lockers/Handshake.png" class="img-fluid-icon" alt="">
                            <h4 CLASS="ProximaNova-Black blue" style="font-size: 20px">PRESENT</h4>
                        </div>
                        <div class="col-lg-4">
                            <img src="mainViewStatic/img/lockers/4-StageIcons-1.png" class="img-fluid-icon"   alt="">
                            <h4 CLASS="ProximaNova-Black blue" style="font-size: 20px">MANAGE</h4>
                        </div>
                        <div class="col-lg-4">
                            <img src="mainViewStatic/img/lockers/Locker.png" class="img-fluid-icon"   alt="">
                            <h4 CLASS="ProximaNova-Black blue" style="font-size: 20px">MAINTAIN</h4>
                        </div>
                    </div>
                </div>

                <div class="col-lg-5 pt-3 pt-lg-0 content" data-aos="fade-left" data-aos-delay="100" style="padding-left: 40px;margin-top: 17px;">
                    <div class="row">
                        <div class="row">
                            <div class="col-lg-1">
                                <span class="text-center circle">1</span>
                            </div>
                            <div class="col-lg-10">
                                <span class="ProximaNova-Regular" style="font-size: 14px"><b class="blue">Present the concept to the school:</b> surveying the space available and advising on the best configuration of the installation.</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-1">
                                <span class="text-center circle">2</span>
                            </div>
                            <div class="col-lg-10">
                                <span class="ProximaNova-Regular" style="font-size: 14px"><b class="blue">Manage the installation:</b> our team will deliver the lockers direct to the school, and you will oversee the installation and ensure the lockers are all set up correctly.</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-1">
                                <span class="text-center circle">3</span>
                            </div>
                            <div class="col-lg-10">
                        <span class="ProximaNova-Regular" style="font-size: 14px"><b class="blue">Maintain the lockers: </b> Most of the maintenance
comes at the end of the summer term, where lockers are cleaned and codes reset ready for the new intake of students.  Typically there will be the occasional ad-hoc maintenance calls to replace a damaged lock or reset a code but these are minimal and most enquiries are handled by the Secure Locker Rentals Head Office team</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-1"></div>
            </div>

        </div>
    </section><!-- End Your role Section -->

    <!-- ======= cost section ======= -->
    <section id="financial" class="about  " style="background-color: rgb(238, 225, 199)" >
        <div class="container" data-aos="fade-up">

            <div class="row content">
                <div class="col-lg-1"></div>
                <div class="col-lg-3 text-right">

                    <h1 class="ProximaNova-Black red" style="font-size: 26px">
                        WHAT IT COSTS.
                    </h1>
                    <h6 class="ProximaNova-Regular" style="font-size: 14px">As a Secure Locker Rentals Franchisee you will be awarded an <b class="ProximaNova-Bold">exclusive
                            territory</b>, and given all of the training, tools and equipment you need to get your business up and running for just </h6>

                    <h1 class="ProximaNova-Black red" style="font-size: 22px">
                        £19,950 + VAT.
                    </h1>
                    <h6 class="ProximaNova-Regular" style="font-size: 11px">
                        (VAT can be reclaimed – contact us for more information).
                    </h6>
                </div>
                <div class="col-lg-4">
                    <div class="topmminus"><img src="mainViewStatic/img/lockers/shutterstock_1160752531.png" class="img-fluid" alt="">
                    </div>

                </div>
                <div class="col-lg-3 text-left">


                    <h1 class="ProximaNova-Black red" style="font-size: 26px">
                        WHAT YOU CAN EARN.
                    </h1>
                    <h6 class="ProximaNova-Regular"  style="font-size: 14px">A well-run territory with just 10 schools supplied can generate in excess of
                        <b class="red">£40,000</b> per year after all costs, and remember, certain territories already have schools signed-up which you will take over upon joining so you could reach those earnings levels much quicker</h6>

                    <h6 class="ProximaNova-Regular" style="font-size: 11px">
                        (Note: Figures are not guaranteed and actual performance can vary by area, please contact us for more information
                    </h6>
                </div>
                <div class="col-lg-1"></div>
            </div>

        </div>
    </section><!-- End Cost Section -->

    <!-- ======= Requst a Brochure======= -->
    <section id="brochure" class="about-video">
        <div class="container" data-aos="fade-up">

            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-7 video-box align-self-baseline" data-aos="fade-right" data-aos-delay="100">
                    <h2 class="ProximaNova-Black gray" style="font-size: 26px">READY TO FIND OUT MORE?</h2>
                    <h6 class="ProximaNova-Regular" style="font-size: 14px">Simply <b class="gray">request a brochure</b> and we’ll get a copy of our latest prospectus on its way to you. This will take you through the business in much more detail, and will help you decide if this is the right business for you (and if you’re the right franchisee for us!).</h6>

                </div>

                <div class="col-lg-3 pt-3 pt-lg-0 conten text-centert" data-aos="fade-left" data-aos-delay="100">
                    <div class="text-center"style="padding-top: 25px">
                        <button type="button" class="btn btn-info text-center gray button" style=" ">REQUEST A FREE BROCHURE</button>
                    </div>
                </div>
                <div class="col-lg-1"></div>
            </div>

        </div>
    </section><!-- End Requst a Brochure Section -->

    <!-- ======= footer section ======= -->
    <section id="footer" class="about" style="background-color: rgb(238, 222, 138); background-image: url(mainViewStatic/img/color-strip.png); background-size: contain; background-repeat: no-repeat;">
        <div class="container" data-aos="fade-up">

            <div class="row content">

                <div class="col-lg-1"></div>
                <div class="col-lg-3 text-left">

                    <img src="mainViewStatic/img/lockers/Secure_Logo_RGB_03.png" class="" alt="" style="transform: translateY(-14px);height: 55px;width: 50%;max-height: 50%" >
                    <h6 class="ProximaNova-Regular" style="font-size: 12px">14 Stephenson Court, Priory Business Park, Bedford, MK44 3WH
                        Reg No: 6535355
                        VAT No: 936 5596 78 </h6>

                </div>
                <div class="col-lg-3"></div>
                <div class="col-lg-4 text-right">
                    <i class="fa fa-youtube-play fa-2x" aria-hidden="true"></i>
                    <i class="fa fa-instagram fa-2x" aria-hidden="true"></i>
                    <i class="fa fa-facebook-square fa-2x" aria-hidden="true"  ></i>
                </div>
                <div class="col-lg-1"></div>

            </div>

        </div>
    </section><!-- End Cost Section -->

</main><!-- End #main -->




<!-- Vendor JS Files -->
<script src="mainViewStatic/vendor/jquery/jquery.min.js"></script>
<script src="mainViewStatic/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="mainViewStatic/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="mainViewStatic/vendor/php-email-form/validate.js"></script>
<script src="mainViewStatic/vendor/waypoints/jquery.waypoints.min.js"></script>
<script src="mainViewStatic/vendor/counterup/counterup.min.js"></script>
<script src="mainViewStatic/vendor/venobox/venobox.min.js"></script>
<script src="mainViewStatic/vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="mainViewStatic/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="mainViewStatic/vendor/aos/aos.js"></script>

<!-- Template Main JS File -->
<script src="mainViewStatic/js/main.js"></script>

</body>

</html>