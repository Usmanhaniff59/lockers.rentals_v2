<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" style="box-sizing: border-box;">
    <title style="box-sizing: border-box;">Send mail using queue !</title>
    <style >
        .card:hover {
            box-shadow: none;}

        .shadow {
            box-shadow: 0 4px 8px 0 #9e9c9e;}
    </style>
</head>
<body>

<div class="container" style="box-sizing: border-box;width: 100%;padding-right: 15px;padding-left: 15px;margin-right: auto;margin-left: auto;min-width: 992px!important;">
    <div class="shadow-lg p-4 mb-4 bg-light" style="box-sizing: border-box;background-color: #f8f9fa!important;box-shadow: 0 1rem 3rem rgba(0,0,0,.175)!important;margin-bottom: 1.5rem!important;padding: 1.5rem!important;line-height: 1.5em;">{!! $emails !!}</div>

    <div class="row" style="box-sizing: border-box;display: flex;-ms-flex-wrap: wrap;flex-wrap: wrap;margin-right: -15px;margin-left: -15px;">
        <p class="text-muted text-center" style="box-sizing: border-box;margin-top: 0;margin-left: 10px;margin-bottom: 1rem;orphans: 3;widows: 3;text-align: center!important;color: #6c757d!important;">this is an automated email.if you have received this email in error, please delete and notify <label style="color: limegreen;box-sizing: border-box;display: inline-block;margin-bottom: .5rem;">{{ \Config::get('mail.from')['support_email'] }}</label> as it contains confidential data</p>

    </div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"  ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"  ></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous" ></script>


</body>
</html>







