<div id="left" class="fixed">
    <div class="menu_scroll left_scrolled">
        <div class="left_media">
            <div class="media user-media">
                <div class="user-media-toggleHover">
                    <span class="fa fa-user"></span>
                </div>
                <div class="user-wrapper">
                    <a class="user-link" href="#">
                        <img class="media-object img-thumbnail user-img rounded-circle admin_img3" alt="User Picture"
                            src="/backend/img/download.png">
                        <p class="user-info menu_hide">Welcome {{ ucwords(trans(Auth::user()->name)) }}</p>
                    </a>
                </div>
            </div>
            <hr />
        </div>
        <ul id="menu">
            <!--  <li class="active">
                <a href="/admin">
                    <i class="fa fa-tachometer circle"></i>
                            <span class="link-title menu_hide">&nbsp;Sample Dashboard

                            </span>
                </a>
            </li> -->
            @can('Email Management')
            <li class="">
                <a href="/admin/emailinbox">
                    <i class="fa fa-envelope circle"></i><span class="link-title menu_hide">My Emails

                    </span>
                </a>
            </li>
            @endcan
            @can('Posts Management')
            <li class="dropdown_menu">
                <a href="javascript:;">
                    <i class="fa fa-pencil circle"></i><span class="link-title menu_hide">Posts</span>
                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul>
                    @can('Create Post')
                    <li>
                        <a href="/admin/posts/create">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Create New Post
                        </a>
                    </li>
                    @endcan
                    <li>
                        <a href="/admin/posts">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; All Posts
                        </a>
                    </li>

                </ul>
            </li>
            @endcan
            @can('Pages Management')
            <li class="dropdown_menu">
                <a href="javascript:;">
                    <i class="fa fa-pencil circle"></i><span class="link-title menu_hide">Pages</span>
                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul>
                    <li>
                        <a href="/admin/pages">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; All Pages
                        </a>
                    </li>

                </ul>
            </li>
            @endcan
            @can('Company Management')
            <li>
                <a href="javascript:">
                    <i class="fa fa-building circle"></i><span class="link-title menu_hide">Schools</span>
                    {{--<span class="badge badge-pill badge-danger float-right calendar_badge menu_hide">7</span>--}}

                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul>
                    <li>
                        <a href="/admin/company">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; All Schools
                            {{--<span class="badge badge-pill badge-danger float-right calendar_badge menu_hide">7</span>--}}

                        </a>
                    </li>

                    <li>
                        <a href="/admin/company/outer/blocks">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Blocks
                        </a>
                    </li>
                    <li>
                        <a href="/admin/locker/codes">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Manual Codes
                        </a>
                    </li>
                    @can('Company Errors')
                    <li>
                        <a href="/admin/company/errors">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Errors
                        </a>
                    </li>
                    @endcan


                </ul>
            </li>
            @endcan
            @can('Order Management')
            <li>
                <a href="/admin/payment"><i class="fa fa-building circle"></i>Orders
                </a>
            </li>
            @can('Edit Orders')
            <li>
                <a href="/admin/edit/payment"><i class="fa fa-clipboard-check circle"></i>Edit Orders
                </a>
            </li>
            @endcan
            @can('Students')
            <li>
                <a href="/admin/edit/student"><i class="fa fa-user-graduate circle"></i>Students
                </a>
            </li>
            @endcan
            {{--                <li>--}}
            {{--                    <a href="/support">--}}
            {{--                        <i class="fad fa-clipboard-check circle"></i>--}}
            {{--                        &nbsp; Support--}}
            {{--                    </a>--}}
            {{--                </li>--}}
            <li>
                <a href="/admin/reserved/lockers"><i class="fal fa-clock circle"></i>&nbsp;&nbsp;Reserved Bookings
                </a>
            </li>

            {{--                <li>--}}
            {{--                    <a href="javascript:">--}}
            {{--                        <i class="fa fa-building circle"></i>--}}
            {{--                        <span class="link-title menu_hide">&nbsp;Orders</span>--}}
            {{--                        <span class="fa arrow menu_hide"></span>--}}
            {{--                    </a>--}}
            {{--                    <ul>--}}
            {{--                        <li>--}}
            {{--                            <a href="/admin/payment">--}}
            {{--                                <i class="fa fa-angle-right"></i>--}}
            {{--                                &nbsp; All Payments--}}
            {{--                            </a>--}}
            {{--                        </li>--}}
            {{--                        <li>--}}
            {{--                            <a href="/admin/reserved/lockers">--}}
            {{--                                <i class="fa fa-angle-right"></i>--}}
            {{--                                &nbsp; Reserved Orders--}}
            {{--                            </a>--}}
            {{--                        </li>--}}
            {{--                    </ul>--}}
            {{--                </li>--}}
            @endcan
            @can('User Management')
            <li>
                <a href="/admin/users">
                    <i class="fa fa-user circle"></i><span class="link-title menu_hide">User Management</span>
                    {{--                        <span class="fa arrow menu_hide"></span>--}}
                </a>
                {{--                    <ul>--}}
                {{--                        <li>--}}
                {{--                            <a href="/admin/users">--}}
                {{--                                <i class="fa fa-angle-right"></i>--}}
                {{--                                &nbsp; All Users--}}
                {{--                            </a>--}}
                {{--                        </li>--}}
                {{--                    </ul>--}}
            </li>
            @endcan
            @can('Rolls and Permissions Management')
            <li class="dropdown_menu">
                <a href="javascript:;"><i class="fa fa-users circle"></i><span class="link-title menu_hide">Permissions</span>
                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul>
                    @can('Permissions Management')
                    <li>
                        <a href="/admin/permissions"><i class="fa fa-angle-right"></i>Permissions
                        </a>
                    </li>
                    @endcan
                    @can('Rolls Management')
                    <li>
                        <a href="/admin/roles">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Roles
                        </a>
                    </li>
                    @endcan

                </ul>
            </li>
            @endcan
            @can('Document Management')
                <li>
                    <a href="/admin/documents"><i class="fa fa-file circle"></i>Documents
                    </a>
                </li>
            @endcan
            @can('Administration Management')
            <li class="dropdown_menu">
                <a href="javascript:;">
                    <i class="fa fa-align-justify circle"></i><span class="link-title menu_hide">Administration</span>
                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul>
                    @can('Booking Types Management')
                    <li>
                        <a href="/admin/bookingtype"><i class="fa fa-angle-right"></i>Booking Types
                        </a>
                    </li>
                    @endcan
                    @can('Email Templates Management')
                    <li>
                        <a href="/admin/emailtemplates"><i class="fa fa-angle-right"></i>Email Templates
                        </a>
                    </li>
                    @endcan
                    @can('DistributionListGroup Management')
                    <li>
                        <a href="/admin/distributionlists"><i class="fa fa-angle-right"></i>Distribution lists
                        </a>
                    </li>
                    @endcan
                        @can('Locker Block Images')
                            <li>
                                <a href="/admin/company/locker/blocks/images"><i class="fa fa-angle-right"></i>Locker Blocks Images
                                </a>
                            </li>
                        @endcan
                        @can('Block Groups')
                            <li>
                                <a href="/admin/block-group">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Block Groups
                                </a>
                            </li>
                        @endcan
                         @can('Locker Description')
                            <li>
                                <a href="/admin/locker-description">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Locker Description
                                </a>
                            </li>
                        @endcan
                </ul>
            </li>
            @endcan
            @can('Help Management')
            <li class="dropdown_menu">
                <a href="javascript:;">
                    <i class="fa fa-question circle"></i><span class="link-title menu_hide">Help Management</span>
                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul>
                    <li>
                        <a href="/admin/helps">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; All Helps
                        </a>
                    </li>

                </ul>
            </li>
            @endcan
            @can('Language Management')
            <li class="dropdown_menu">
                <a href="javascript:;"><i class="fa fa-language circle"></i><span class="link-title menu_hide">Language</span>
                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul>
                    <li>
                        <a href="/admin/languages">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; All Languages
                        </a>
                    </li>
                    <li>
                        <a href="/admin/labels">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; All Labels
                        </a>
                    </li>
                </ul>
            </li>
            @endcan
            @can('Slider Management')
            <li class="dropdown_menu">
                <a href="javascript:;">
                    <i class="fa fa-sliders circle"></i><span class="link-title menu_hide">Slider Management</span>
                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul>
                    <li>
                        <a href="/admin/sliders">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; All Sliders
                        </a>
                    </li>

                </ul>
            </li>
            @endcan
            @if(Gate::check('Availability Report') || Gate::check('Booking Report') || Gate::check('Sales Report')|| Gate::check('Chat Report')|| Gate::check('Franchisee Report')|| Gate::check('Maintenance OffSale Report')|| Gate::check('Current Maintenance OffSale Report'))
            <li class="dropdown_menu">
                <a href="javascript:;"><i class="fa fa-sliders circle"></i><span class="link-title menu_hide">Admin Reports</span>
                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul>
                    @can('Sales Report')
                    <li>
                        <a href="/admin/reports/sales">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Sales Report
                        </a>
                    </li>
                    @endcan
                    @can('Availability Report')
                    <li>
                        <a href="/admin/reports/availability">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Availability Report
                        </a>
                    </li>
                    @endcan
                    @can('Booking Report')
                    <li>
                        <a href="/admin/reports/booked">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Lockers Booked Report
                        </a>
                    </li>
                    @endcan
                  @can('Locker Number')
                    <li>
                        <a href="/admin/reports/locker-numbers">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Locker Number Report
                        </a>
                    </li>
                    @endcan
                  @can('Chat Report')
                    <li>
                        <a href="/admin/company/chats/excel/report">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Chat Report
                        </a>
                    </li>
                  @endcan
                @can('Franchisee Report')
                    <li>
                        <a href="/admin/company/franchisee/franchisee_index">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Franchisee Report
                        </a>
                     </li>
                @endcan
                @can('Maintenance OffSale Report')
                    <li>
                        <a href="/admin/reports/offsale/index">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Maintenance/OffSale Report
                        </a>
                    </li>
                @endcan
                @can('Current Maintenance OffSale Report')
                    <li>
                        <a href="/admin/reports/currentlocker/index">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Current Maintenance/OffSale Report
                        </a>
                    </li>
                @endcan
                @can('Academic Year Report')
                    <li>
                        <a href="/academic/year/report">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Academic Year Report
                        </a>
                    </li>
                @endcan
                @can('Voucher Payments Report')
                    <li>
                        <a href="/voucher/payment/report">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Voucher Payments Report
                        </a>
                    </li>
                @endcan
                @can('Company Booking Report')
                    <li>
                        <a href="/company/booking/report">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Company Booking Report
                        </a>
                    </li>
                @endcan
                @can('Charity Report')
                    <li>
                        <a href="/charity/report">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Charity Report
                        </a>
                    </li>
                @endcan
                @can('Pupil Premium Report')
                    <li>
                        <a href="/pupil/premium/report">
                            <i class="fa fa-angle-right"></i>
                            &nbsp; Pupil Premium Report
                        </a>
                    </li>
                @endcan
                
                </ul>
            </li> 
            @endif
        </ul>
        <!-- /#menu -->
    </div>
</div>