<!doctype html>
<html class="no-js" lang="en">

<head>

    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- <link rel="shortcut icon" href="/backend/img/logo1.ico"> -->
    <link rel="shortcut icon" href="/frontend/favicon/favicon.jpeg">
<link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <!--global styles-->
    <link type="text/css" rel="stylesheet" href="{{ asset('backend/css/components.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('backend/css/custom.css') }}" />
    <!-- end of global styles-->

    <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" /> -->

    <!--date picker-->
    <link rel="stylesheet" href="/css/bootstrap-datetimepicker.css">

    <script src="https://kit.fontawesome.com/14fa05d5e7.js" crossorigin="anonymous"></script>
    <!--date time picker-->

    @yield('style')
    <style>
        i.circle {
            display: inline-block;
            border-radius: 60px;
            box-shadow: 0px 0px 2px #888;
            padding: 0.5em 0.6em;

        }

        table>thead>tr>th {
            vertical-align: top !important;
            text-align: left !important;
        }

        .upload-area {
            width: 100%;
            height: 200px;
            padding-top: 50px;
            border: 2px solid #dad8d8;
            border-radius: 3px;
            margin: 0 auto;
            text-align: center;
            overflow: hidden;
        }

        .upload-area:hover {
            cursor: pointer;
            background: #f5f2f2;
            box-shadow: 1px 1px 1px 1px #ece7e7;
        }

        .fa-cloud-upload-alt {
            font-size: 55px;
            color: #cfcccc;
        }


        .upload-area h1 {
            text-align: center;
            line-height: 50px;
            font-size: 20px;
            color: #cfcccc;
        }
    </style>
</head>

<body class="body fixedNav_position fixedMenu_left">

    <div class="preloader" style=" position: fixed; width: 100%; height: 100%; top: 0; left: 0; z-index: 100000; 
    backface-visibility: hidden; background: #ffffff;">
        <div class="preloader_img" style="width: 200px; height: 200px; position: absolute; left: 48%; top: 48%;
        background-position: center; z-index: 999999">
            <img src="/backend/img/loader.gif" style=" width: 50px;" alt="loading...">
        </div>
    </div>
    <div id="wrap">
        @include('backend.layouts.top')
        <!-- /#top -->
        <div class="wrapper fixedNav_top">
            @include('backend.layouts.sidebar')
            <!-- /#left -->
            @yield('content')
            <!-- /.inner -->
        </div>
        <!-- /.outer -->


    </div>
    <!-- /#wrap -->
    <script type="text/javascript" src="{{ asset('backend/js/jquery.min.js') }}"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script> -->

    <!-- global scripts-->
    <script type="text/javascript" src="{{ asset('backend/js/components.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/custom.js') }}"></script>
    <!--end of global scripts-->


    @yield('script')
    @stack('script')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
    <script src="/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
          $(document).ready(function() {
var dateToday = new Date();
$('#headerDateS').datepicker({
     minDate: dateToday,
});
// $('#create_end_block').datepicker();
        });
          $('#headerDateS').change(function(){
            var id=$(this).attr('data-id');
            var date= $(this).val();
                $.ajax({
            

            url: '/updatetheSchoolyearCheck',

            type: 'GET',

            dataType: 'JSON',

            cache: false,
            data:{
                id:id,
                date:date,
            },
            success: function (data) {
                $('#createManualCOde').html('Note:It will take 5 to 7 mints to create Manual Code');
                setTimeout(function(){ location.reload(); }, 3000);
                
               


            },

            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            }

        });
          });
    </script>
</body>

</html>