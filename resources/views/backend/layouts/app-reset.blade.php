<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- <link rel="shortcut icon" href="/backend/img/logo1.ico"/> -->
    <link rel="shortcut icon" href="/frontend/favicon/favicon.jpeg">
    <!--Global styles -->
    <link type="text/css" rel="stylesheet" href="/backend/css/components.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/custom.css" />
    <!--End of Global styles -->
    <!--Plugin styles-->
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrapvalidator/css/bootstrapValidator.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/wow/css/animate.css"/>
    <!--End of Plugin styles-->
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">

    <link type="text/css" rel="stylesheet" href="/backend/css/pages/login2.css"/>
</head>
<body class="login_background">
<div class="preloader" style=" position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  z-index: 100000;
  backface-visibility: hidden;
  background: #ffffff;">
    <div class="preloader_img" style="width: 200px;
  height: 200px;
  position: absolute;
  left: 48%;
  top: 48%;
  background-position: center;
z-index: 999999">
        <img src="/backend/img/loader.gif" style=" width: 40px;" alt="loading...">
    </div>
</div>
<div class="container wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
    <div class="row">
        <div class="col-10 mx-auto">
            <div class="row">
                <div class="col-lg-4  col-md-8 col-sm-12  mx-auto login_image login_section login_section_top">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
</div>
<!-- global js -->
<script type="text/javascript" src="/backend/js/jquery.min.js"></script>
<script type="text/javascript" src="/backend/js/popper.min.js"></script>
<script type="text/javascript" src="/backend/js/bootstrap.min.js"></script>
<!-- end of global js-->
<!--Plugin js-->
<script type="text/javascript" src="/backend/vendors/bootstrapvalidator/js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="/backend/vendors/wow/js/wow.min.js"></script>
<script type="text/javascript" src="/backend/vendors/jquery.backstretch/js/jquery.backstretch.js"></script>
<!--End of plugin js-->
<script type="text/javascript" src="/backend/js/pages/login2.js"></script>

</body>

</html>