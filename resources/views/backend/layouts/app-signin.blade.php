<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- <link rel="shortcut icon" href="/backend/img/logo1.ico"/> -->
    <link rel="shortcut icon" href="/frontend/favicon/favicon.jpeg">
    <!--Global styles -->
    <link type="text/css" rel="stylesheet" href="/backend/css/components.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/custom.css" />
    <!--End of Global styles -->
    <!--Plugin styles-->
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrapvalidator/css/bootstrapValidator.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/wow/css/animate.css"/>
    <!--End of Plugin styles-->
    
    <link type="text/css" rel="stylesheet" href="/frontend/vendors/radio_css/css/radiobox.min.css" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/login2.css"/>
    <link type="text/css" rel="stylesheet" href="/frontend/css/pages/radio_checkbox.css" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<style>
    body {
        background-image: url("/frontend/img/intro-carousel/1_login.jpg");
        /* Full height */
        height: 100%;

        /* Center and scale the image nicely */
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        /*background-color: #cccccc;*/
    }

</style>
<body class="login_background">

{{--<div class="preloader" style=" position: fixed;--}}
          {{--width: 100%;--}}
          {{--height: 100%;--}}
          {{--top: 0;--}}
          {{--left: 0;--}}
          {{--z-index: 100000;--}}
          {{--backface-visibility: hidden;--}}
          {{--background: #ffffff;">--}}

    {{--<div class="preloader_img" style="width: 200px;--}}
              {{--height: 200px;--}}
              {{--position: absolute;--}}
              {{--left: 48%;--}}
              {{--top: 48%;--}}
              {{--background-position: center;--}}
            {{--z-index: 999999">--}}

        {{--<img src="/backend/img/loader.gif" style=" width: 40px;" alt="loading...">--}}

    {{--</div>--}}
{{--</div>--}}

<div class="container wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
    <div class="row">
        <div class="col-10 mx-auto">
            <div class="row">
                <div class="col-lg-4  col-md-8 col-sm-12  mx-auto login_image login_section login_section_top">
                    <div class="login_logo login_border_radius1">
                        <h3 class="text-left text-white">
                            <img src="/backend/img/logo.png" style="width: 80% !important;" alt="josh logo" class="admire_logo">
                        </h3>
                    </div>
                    <div class="row m-t-20">
                        <div class="col-12">
                            @yield('header-content')
                        </div>
                    </div>
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
</div>
<!-- global js -->
<script type="text/javascript" src="/backend/js/jquery.min.js"></script>
<script type="text/javascript" src="/backend/js/popper.min.js"></script>
<script type="text/javascript" src="/backend/js/bootstrap.min.js"></script>
<!-- end of global js-->
<!--Plugin js-->
<script type="text/javascript" src="/backend/vendors/bootstrapvalidator/js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="/backend/vendors/wow/js/wow.min.js"></script>
<script type="text/javascript" src="/backend/vendors/jquery.backstretch/js/jquery.backstretch.js"></script>
<!--End of plugin js-->
<script type="text/javascript" src="/backend/js/pages/login2.js"></script>
<script type="text/javascript" src="/frontend/js/pages/radio_checkbox.js"></script>

<script>

    $('body').on('click','.reset-clear',function(e){

        $('#reset_sms_for_confirmation').val('');
        $('#reset_email_for_confirmation').val('');
        $('#reset_passcode_for_confirmation').val('');
        $('.reset-radio-section').removeClass('d-none');
        $('.email-input-section').removeClass('d-none');
        $('.reset-btn-section').removeClass('d-none');
        $("#email_radio"). prop("checked", true)
        $('#email_confirm_msg').addClass('d-none');
        $('#sms_confirm_msg').addClass('d-none');
        $('.sms-input-section').addClass('d-none');
        $('.passcode-section').addClass('d-none');
        $('#reset_sms_validation_not_found').addClass('d-none');
        $('#resetemailconfirmvalidation').addClass('d-none');

    });

    $('body').on('click','#email_radio',function(e){

        $('#email').val('');
        $('.email-section').removeClass('d-none');
        $('.sms-section').addClass('d-none');
        $('#reset_sms_validation_not_found').addClass('d-none');
        $('#resetemailconfirmvalidation').addClass('d-none');

    });

    $('body').on('click','#sms_radio',function(e){

        $('#phone_number').val('');
        $('.email-section').addClass('d-none');
        $('.sms-section').removeClass('d-none');
        $('#reset_sms_validation_not_found').addClass('d-none');
        $('#resetemailconfirmvalidation').addClass('d-none');

    });

    $('body').on('click','.sms_send_btn',function(e){

        e.preventDefault();
        $('#reset_sms_validation_not_found').addClass('d-none');
        $('#resetemailconfirmvalidation').addClass('d-none');

        var phone_number = $.trim($('#phone_number').val());

        if(phone_number.length > 0){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: '/frontend/sendresetsms',
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {phone_number: phone_number},
                success: function (response) {
                    if(response=='user_not_found'){

                        $('#reset_sms_validation_not_found').removeClass('d-none');

                    }else{
                        {{--var dest_url = "{{ url('/logout') }}";--}}
                        {{--$.ajax({--}}
                            {{--type:"POST",--}}
                            {{--url:dest_url,--}}
                            {{--success: function(data){--}}

                            {{--},--}}
                            {{--error: function(data){--}}

                            {{--}--}}
                        {{--})--}}

                        $('#reset_user_id').val(response.user_id);
                        $('#sms_confirm_msg').removeClass('d-none');
                        $('.sms-section').addClass('d-none');
                        $('.email-section').addClass('d-none');
                        // $('.reset-btn-section').addClass('d-none');
                        $('.reset-radio-section').addClass('d-none');

                        $('.passcode-section').removeClass('d-none');
                    }



                },
                error: function (jqXHR) {
                    console.log(jqXHR);

                }
            });


        }

    });

    $('body').on('click','.passcode_confirm_btn',function(e){

        e.preventDefault();

        $('#reset_passcode_validation_not_found').addClass('d-none');
        console.log('dd');

        var reset_user_id = $.trim($('#reset_user_id').val());
        var reset_code = $.trim($('#reset_passcode_for_confirmation').val());

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: '/frontend/validatepasscode',
            type: 'POST',
            dataType: 'JSON',
            cache: false,
            data: {reset_user_id: reset_user_id,reset_code: reset_code},
            success: function (response) {
                console.log(response);
                if(response.status == 'matched'){


                    console.log(response.url,'url');
                    window.location.href = response.url;

                }else{
                    $('#reset_passcode_validation_not_found').removeClass('d-none');
                }



            },
            error: function (jqXHR) {
                console.log(jqXHR);

            }
        });


    });

</script>

</body>

</html>