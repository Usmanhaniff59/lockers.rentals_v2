@php
    $uri_parts = explode('/', $_SERVER['REQUEST_URI']);
    $uri_parts = array_filter($uri_parts);
    if(count($uri_parts) > 3){
        if(is_numeric($uri_parts['3'])){
            $url = "/".$uri_parts['1']."/".$uri_parts['2']."/".$uri_parts['4'];
        }else{
            $url = "/".$uri_parts['1']."/".$uri_parts['2']."/".$uri_parts['3'];
        }
    }
    elseif(count($uri_parts) == 2){
    if(isset($uri_parts['2'])){
        $url = "/".$uri_parts['1']."/".$uri_parts['2'];
    }
    else{
        $url = "/".$uri_parts['1'];
    }

    }
    else{
        $url = "/".$uri_parts['1'];
    }

    $help = \App\Help::where('page_url', $url)->first();
if(is_null($help)){
$help = \App\Help::where('page_url','LIKE',  '%default%')->first();
}


$c_years=\App\SchoolYear::where([['status','1'],['to_date','>=',\Carbon\Carbon::now()]])->get();
$unActiveYear=\App\SchoolYear::where([['status','0'],['to_date','>=',\Carbon\Carbon::now()]])->orderBy('id', 'asc')->first();

@endphp

<div id="top" class="fixed">
    <!-- .navbar -->
    <nav class="navbar navbar-static-top">
        <div class="container-fluid m-0">
            <a class="navbar-brand" id="admin_logo_link" href="{{url('admin')}}" style="padding-top: 4px !important;padding-bottom:0px !important;">
                <h4 id="admin_logo_heading" style="width: 80% !important;"><img src="/frontend/img/admin_logo.png" style="width:100% !important;height: auto !important;" id="admin_logo" alt="logo"></h4>
            </a>
            <div class="menu mr-sm-auto" style="margin-left: 0px !important;">
                <span class="toggle-left" id="menu-toggle">
                    <i class="fa fa-bars"></i>
                </span>
            </div>

            <div style="float: left" class="col-md-4">
           <?php $mi=0; foreach($c_years as $year){
            $mi++;
           $date= date('M Y', strtotime($year->from_date));
           if($mi!=1){
            echo ',';
           }else{
            echo 'Booking Open for: ';
           }
           echo $date;
       }
           ?>
           &nbsp
           </div>
          
        <!--    <div class="col-md-4" >
            <?php  if($unActiveYear){ ?>
             @can('Go live Date')
           <?php
 
           $date= date('Y', strtotime($unActiveYear->from_date));
          echo '<span style="float:left;width:auto;padding:6px;">Open Sep '.$date.'</span>' ;
            ?>
            
           <input readonly="" style="width: 24%; cursor: pointer;" type="text"  id="headerDateS" class="form-control" value="<?php if($unActiveYear->go_live){ echo date('m/d/Y', strtotime($unActiveYear->go_live));}else{ echo date('m/d/Y'); }?>" data-id="{{$unActiveYear->id}}" placeholder="Activate from here" >
                <span style="color: red" id="createManualCOde"></span>
              @endcan
          <?php  }?>
              </div>  -->
              
            {{--<div class="top_search_box d-none d-md-flex">--}}
                {{--<form class="header_input_search">--}}
                    {{--<input type="text" placeholder="Search" name="search">--}}
                    {{--<button type="submit">--}}
                        {{--<span class="font-icon-search text-primary"></span>--}}
                    {{--</button>--}}
                    {{--<div class="overlay"></div>--}}
                {{--</form>--}}
            {{--</div>--}}
            <div class="topnav dropdown-menu-right">
                {{--<div class="btn-group small_device_search" data-toggle="modal"--}}
                     {{--data-target="#search_modal">--}}
                    {{--<i class="fa fa-search text-primary"></i>--}}
                {{--</div>--}}
                {{--<div class="btn-group">--}}
                    {{--<div class="notifications no-bg">--}}
                        {{--<a class="btn btn-default btn-sm messages" data-toggle="dropdown" id="messages_section"> <i--}}
                                    {{--class="fa fa-envelope-o fa-1x"></i>--}}
                            {{--<span class="badge badge-pill badge-warning notifications_badge_top">8</span>--}}
                        {{--</a>--}}
                        {{--<div class="dropdown-menu drop_box_align" role="menu" id="messages_dropdown">--}}
                            {{--<div class="popover-header">You have 8 Messages</div>--}}
                            {{--<div id="messages">--}}
                                {{--<div class="data">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-2">--}}
                                            {{--<img src="/backend/img/mailbox_imgs/5.jpg" class="message-img avatar rounded-circle"--}}
                                                 {{--alt="avatar1"></div>--}}
                                        {{--<div class="col-10 message-data"><strong>hally</strong>--}}
                                            {{--sent you an image.--}}
                                            {{--<br>--}}
                                            {{--<small>add to timeline</small>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="data">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-2">--}}
                                            {{--<img src="/backend/img/mailbox_imgs/8.jpg" class="message-img avatar rounded-circle"--}}
                                                 {{--alt="avatar1"></div>--}}
                                        {{--<div class="col-10 message-data"><strong>Meri</strong>--}}
                                            {{--invitation for party.--}}
                                            {{--<br>--}}
                                            {{--<small>add to timeline</small>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="data">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-2">--}}
                                            {{--<img src="/backend/img/mailbox_imgs/7.jpg" class="message-img avatar rounded-circle"--}}
                                                 {{--alt="avatar1"></div>--}}
                                        {{--<div class="col-10 message-data">--}}
                                            {{--<strong>Remo</strong>--}}
                                            {{--meeting details .--}}
                                            {{--<br>--}}
                                            {{--<small>add to timeline</small>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="data">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-2">--}}
                                            {{--<img src="/backend/img/mailbox_imgs/6.jpg" class="message-img avatar rounded-circle"--}}
                                                 {{--alt="avatar1"></div>--}}
                                        {{--<div class="col-10 message-data">--}}
                                            {{--<strong>David</strong>--}}
                                            {{--upcoming events list.--}}
                                            {{--<br>--}}
                                            {{--<small>add to timeline</small>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="data">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-2">--}}
                                            {{--<img src="/backend/img/mailbox_imgs/5.jpg" class="message-img avatar rounded-circle"--}}
                                                 {{--alt="avatar1"></div>--}}
                                        {{--<div class="col-10 message-data"><strong>hally</strong>--}}
                                            {{--sent you an image.--}}
                                            {{--<br>--}}
                                            {{--<small>add to timeline</small>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="data">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-2">--}}
                                            {{--<img src="/backend/img/mailbox_imgs/8.jpg" class="message-img avatar rounded-circle"--}}
                                                 {{--alt="avatar1"></div>--}}
                                        {{--<div class="col-10 message-data"><strong>Meri</strong>--}}
                                            {{--invitation for party.--}}
                                            {{--<br>--}}
                                            {{--<small>add to timeline</small>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="data">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-2">--}}
                                            {{--<img src="/backend/img/mailbox_imgs/7.jpg" class="message-img avatar rounded-circle"--}}
                                                 {{--alt="avatar1"></div>--}}
                                        {{--<div class="col-10 message-data">--}}
                                            {{--<strong>Remo</strong>--}}
                                            {{--meeting details .--}}
                                            {{--<br>--}}
                                            {{--<small>add to timeline</small>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="data">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-2">--}}
                                            {{--<img src="/backend/img/mailbox_imgs/6.jpg" class="message-img avatar rounded-circle"--}}
                                                 {{--alt="avatar1"></div>--}}
                                        {{--<div class="col-10 message-data">--}}
                                            {{--<strong>David</strong>--}}
                                            {{--upcoming events list.--}}
                                            {{--<br>--}}
                                            {{--<small>add to timeline</small>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="popover-footer">--}}
                                {{--<a href="mail_inbox.html" class="text-white">Inbox</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="btn-group">--}}
                    {{--<div class="notifications messages no-bg">--}}
                        {{--<a class="btn btn-default btn-sm" data-toggle="dropdown" id="notifications_section"> <i--}}
                                    {{--class="fa fa-bell-o"></i>--}}
                            {{--<span class="badge badge-pill badge-danger notifications_badge_top">9</span>--}}
                        {{--</a>--}}
                        {{--<div class="dropdown-menu drop_box_align" role="menu" id="notifications_dropdown">--}}
                            {{--<div class="popover-header">You have 9 Notifications</div>--}}
                            {{--<div id="notifications">--}}
                                {{--<div class="data">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-2">--}}
                                            {{--<img src="/backend/img/mailbox_imgs/1.jpg" class="message-img avatar rounded-circle"--}}
                                                 {{--alt="avatar1"></div>--}}
                                        {{--<div class="col-10 message-data">--}}
                                            {{--<i class="fa fa-clock-o"></i>--}}
                                            {{--<strong>Remo</strong>--}}
                                            {{--sent you an image--}}
                                            {{--<br>--}}
                                            {{--<small class="primary_txt">just now.</small>--}}
                                            {{--<br>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="data">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-2">--}}
                                            {{--<img src="/backend/img/mailbox_imgs/2.jpg" class="message-img avatar rounded-circle"--}}
                                                 {{--alt="avatar1"></div>--}}
                                        {{--<div class="col-10 message-data">--}}
                                            {{--<i class="fa fa-clock-o"></i>--}}
                                            {{--<strong>clay</strong>--}}
                                            {{--business propasals--}}
                                            {{--<br>--}}
                                            {{--<small class="primary_txt">20min Back.</small>--}}
                                            {{--<br>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="data">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-2">--}}
                                            {{--<img src="/backend/img/mailbox_imgs/3.jpg" class="message-img avatar rounded-circle"--}}
                                                 {{--alt="avatar1"></div>--}}
                                        {{--<div class="col-10 message-data">--}}
                                            {{--<i class="fa fa-clock-o"></i>--}}
                                            {{--<strong>John</strong>--}}
                                            {{--meeting at Ritz--}}
                                            {{--<br>--}}
                                            {{--<small class="primary_txt">2hrs Back.</small>--}}
                                            {{--<br>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="data">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-2">--}}
                                            {{--<img src="/backend/img/mailbox_imgs/6.jpg" class="message-img avatar rounded-circle"--}}
                                                 {{--alt="avatar1"></div>--}}
                                        {{--<div class="col-10 message-data">--}}
                                            {{--<i class="fa fa-clock-o"></i>--}}
                                            {{--<strong>Luicy</strong>--}}
                                            {{--Request Invitation--}}
                                            {{--<br>--}}
                                            {{--<small class="primary_txt">Yesterday.</small>--}}
                                            {{--<br>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="data">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-2">--}}
                                            {{--<img src="/backend/img/mailbox_imgs/1.jpg" class="message-img avatar rounded-circle"--}}
                                                 {{--alt="avatar1"></div>--}}
                                        {{--<div class="col-10 message-data">--}}
                                            {{--<i class="fa fa-clock-o"></i>--}}
                                            {{--<strong>Remo</strong>--}}
                                            {{--sent you an image--}}
                                            {{--<br>--}}
                                            {{--<small class="primary_txt">just now.</small>--}}
                                            {{--<br>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="data">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-2">--}}
                                            {{--<img src="/backend/img/mailbox_imgs/2.jpg" class="message-img avatar rounded-circle"--}}
                                                 {{--alt="avatar1"></div>--}}
                                        {{--<div class="col-10 message-data">--}}
                                            {{--<i class="fa fa-clock-o"></i>--}}
                                            {{--<strong>clay</strong>--}}
                                            {{--business propasals--}}
                                            {{--<br>--}}
                                            {{--<small class="primary_txt">20min Back.</small>--}}
                                            {{--<br>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="data">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-2">--}}
                                            {{--<img src="/backend/img/mailbox_imgs/3.jpg" class="message-img avatar rounded-circle"--}}
                                                 {{--alt="avatar1"></div>--}}
                                        {{--<div class="col-10 message-data">--}}
                                            {{--<i class="fa fa-clock-o"></i>--}}
                                            {{--<strong>John</strong>--}}
                                            {{--meeting at Ritz--}}
                                            {{--<br>--}}
                                            {{--<small class="primary_txt">2hrs Back.</small>--}}
                                            {{--<br>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="data">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-2">--}}
                                            {{--<img src="/backend/img/mailbox_imgs/6.jpg" class="message-img avatar rounded-circle"--}}
                                                 {{--alt="avatar1"></div>--}}
                                        {{--<div class="col-10 message-data">--}}
                                            {{--<i class="fa fa-clock-o"></i>--}}
                                            {{--<strong>Luicy</strong>--}}
                                            {{--Request Invitation--}}
                                            {{--<br>--}}
                                            {{--<small class="primary_txt">Yesterday.</small>--}}
                                            {{--<br>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="data">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-2">--}}
                                            {{--<img src="/backend/img/mailbox_imgs/1.jpg" class="message-img avatar rounded-circle"--}}
                                                 {{--alt="avatar1"></div>--}}
                                        {{--<div class="col-10 message-data">--}}
                                            {{--<i class="fa fa-clock-o"></i>--}}
                                            {{--<strong>Remo</strong>--}}
                                            {{--sent you an image--}}
                                            {{--<br>--}}
                                            {{--<small class="primary_txt">just now.</small>--}}
                                            {{--<br>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="popover-footer">--}}
                                {{--<a href="#" class="text-white">View All</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}


                <div class="btn-group">
                    <div class="user-settings no-bg">
                        <button type="button" class="btn btn-default no-bg micheal_btn" data-toggle="dropdown">
                            <img src="/backend/img/admin.jpg" class="admin_img2 img-thumbnail rounded-circle avatar-img"
                                 alt="avatar"> <strong>
                                @if(Auth::user()->username) {{ ucwords(trans(Auth::user()->username)) }}@elseif(Auth::user()->name) {{ ucwords(trans(Auth::user()->name)) }} @elseif(Auth::user()->surname)  {{ ucwords(trans(Auth::user()->surname)) }}  @else   @endif  </strong>
                            <span class="fa fa-sort-down white_bg"></span>
                        </button>
                        <div class="dropdown-menu admire_admin">
                            {{--<a class="dropdown-item title" href="#">--}}
                                {{--@if(Auth::user()->username) Auth::user()->username @elseif(Auth::user()->name) Auth::user()->name @elseif(Auth::user()->surname) Auth::user()->surname @else  No Name! @endif  </a>--}}
                            <a class="dropdown-item" href="/"><i class="fa fa-home"></i>
                                Home</a>
                            <a class="dropdown-item" href="/admin/editprofile"><i class="fa fa-cogs"></i>
                                Edit Profile</a>
{{--                            <a class="dropdown-item" href="#">--}}
{{--                                <i class="fa fa-user"></i>--}}
{{--                                User Status--}}
{{--                            </a>--}}
{{--                            <a class="dropdown-item" href="mail_inbox.html"><i class="fa fa-envelope"></i>--}}
{{--                                Inbox</a>--}}
{{--                            <a class="dropdown-item" href="lockscreen.html"><i class="fa fa-lock"></i>--}}
{{--                                Lock Screen--}}
{{--                            </a>--}}
                            <a href="{{ route('logout') }}" class="dropdown-item"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>
                                Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </div>
                </div>
                <div class="btn-group">
                    <div class="notifications request_section no-bg">
                        <a class="btn btn-default btn-sm messages" id="request_btn" style="margin-right: 0px; margin-left: 20px;"> <i
                                    class="fa fa-question-circle-o" aria-hidden="true" style="font-size: 24px;"></i>
                        </a>
                    </div>
                </div>

            </div>
        </div>
        <!-- /.container-fluid -->
    </nav>
    <!-- /.navbar -->
    <!-- /.head -->
    <div id="request_list" >
        <div class="request_scrollable">
            <ul class="nav nav-tabs m-t-15">
                <li class="nav-item">
                    <a class="nav-link active text-center" href="#help_tab" data-toggle="tab">Help</a>
                </li>
            </ul>
            <hr>
            <div class="tab-content">
                <div class="tab-pane active" id="help_tab">
                    <div id="settings_section">
                        <div class="layout_styles mx-3">
                            <div class="row">

                                @can('Edit Help')
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Help Title:</label>
                                        <input class="form-control" type="text" id="help_title" value="{{$help->title}}">
                                        <input type="hidden" id="help_url" value="{{$url}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Help Details:</label>
                                        <textarea class="form-control" id="help_detail" rows="6" cols="20">{{$help->detail}}</textarea>
                                    </div>
                                    <div id="success-div"></div>
                                    <div class="form-group">
                                        <button id="help_save" onclick="help_save()" class="btn btn-success btn-block">Save</button>
                                    </div>
                                </div>
                                @else
                                    <div class="col-12">
                                        <h4><b>{{$help->title}}</b></h4><hr>
                                        <h5><p>{{$help->detail}}</p></h5>
                                    </div>
                                @endcan
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
<script>

    function help_save() {
        var help_url = $("#help_url").val();
        var help_title = $("#help_title").val();
        var help_detail = $("#help_detail").val();
        var csrf = $('meta[name=csrf-token]').attr("content");
        var dataString = '_token='+csrf+'&help_url='+help_url+'&help_title='+help_title+'&help_detail='+help_detail;
        $.ajax({
            type: 'POST',
            data: dataString,
            url: '/admin/help-ajax',
            success: function (data) {
                if(data == 1){
                    $("#success-div").html('<div class="alert alert-info alert-dismissible fade show" id="success-alert">Help Saved!</div>');
                    window.setTimeout(function () {
                        $("#success-alert").alert('close'); }, 2000);


                }
            }
        });
    }
</script>