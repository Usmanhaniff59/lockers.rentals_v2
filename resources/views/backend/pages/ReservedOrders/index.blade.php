@extends('backend.layouts.app')

@section('title', 'Lockers | Reserved Lockers')
@section('style')
    <!--plugin styles-->
    <link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css" />

    <link type="text/css" rel="stylesheet" href="/backend/vendors/inputlimiter/css/jquery.inputlimiter.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/chosen/css/chosen.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css"/>
    {{--<link type="text/css" rel="stylesheet" href="/backend/vendors/jquery-tagsinput/css/jquery.tagsinput.min.css"/>--}}
    <link type="text/css" rel="stylesheet" href="/backend/vendors/daterangepicker/css/daterangepicker.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datepicker/css/bootstrap-datepicker.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-switch/css/bootstrap-switch.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/multiselect/css/multi-select.css"/>

    <!--End of Plugin styles-->


    <!--Page level styles-->

    <link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css" />
    <style>
        .red{
            background-color: red;
        }
        .green{
            background-color: greenyellow;
        }
        .orrange{
            background-color: orange;
        }
        #reserved_lockers_wrapper>.row:nth-child(1),#reserved_lockers_wrapper>.row:nth-child(1){
            width: 100% !important;
        }

             .ui-datepicker {
            width: 300px;
            height: 300px;
            margin: 5px auto 0;
            font: 12pt Arial, sans-serif;
            /*-webkit-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
            -moz-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);*/
        }

            .ui-datepicker table {
                width: 100%;
            }

        .ui-datepicker-header {
            background: #3399ff;
            color: #ffffff;
            font-family:'Times New Roman';
            border-width: 1px 0 0 0;
            border-style: solid;
            border-color: #111;
        }

        .ui-datepicker-title {
            text-align: center;
            font-size: 15px;

        }

        .ui-datepicker-prev {
            float: left;
            cursor: pointer;
            background-position: center -30px;
        }

        .ui-datepicker-next {
            float: right;
            cursor: pointer;
            background-position: center 0px;
        }

        .ui-datepicker thead {
            background-color: #f7f7f7;

            /*border-bottom: 1px solid #bbb;*/
        }

        .ui-datepicker th {
            background-color:#808080;
            text-transform: uppercase;
            font-size: 8pt;
            color: #666666;
            /*text-shadow: 1px 0px 0px #fff;*/
            /*filter: dropshadow(color=#fff, offx=1, offy=0);*/
        }

        .ui-datepicker tbody td {
            padding: 0;
            /*border-right: 1px solid #808080;*/
        }

            .ui-datepicker tbody td:last-child {
                border-right: 0px;
            }

        .ui-datepicker tbody tr {
            border-bottom: 1px solid #bbb;
        }

            .ui-datepicker tbody tr:last-child {
                border-bottom: 0px;
            }

        .ui-datepicker a {
            text-decoration: none;
        }

        .ui-datepicker td span, .ui-datepicker td a {
            display: inline-block;
            /*font-weight: bold;*/
            text-align: center;
            width: 30px;
            height: 30px;
            line-height: 30px;
            color: #ffffff;
            /*text-shadow: 1px 1px 0px #fff;*/
            /*filter: dropshadow(color=#fff, offx=1, offy=1);*/
        }

        .ui-datepicker-calendar .ui-state-default {
              background: linear-gradient(#999999, #737373);
              color:#ffffff;
              height:40px;
              width:40px;

        }

        .ui-datepicker-calendar .ui-state-hover {
            background: #33adff;
            color: #FFFFFF;
        }

        .ui-datepicker-calendar .ui-state-active {
            background: #33adff;
            -webkit-box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1);
            -moz-box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1);
            box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1);
            color: #e0e0e0;
            text-shadow: 0px 1px 0px #4d7a85;
            border: 1px solid #55838f;
            position: relative;
            margin: -1px;
        }

        .ui-datepicker-unselectable .ui-state-default {
            background: #D6E4BE;
            color: #000;
        }

    </style>
@endsection
@section('content')
    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fas fa-user circle header_icon_margin"></i>
                            Reserved Lockers
                        </h4>
                    </div>
                </div>
            </div>
        </header>

        @include('errors.success')
        @include('errors.error')

        @if(Session::has('flash_message'))
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable float-right" style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-hidden="true" style="margin-top: 4px; margin-left: 15px;"> ×
                    </button>
                    {{ Session::get('flash_message') }}
                </div>
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="outer">
            <div class="inner bg-container">
                <div class="row">
                    <div class="col-12 data_tables">

                        <div class="card m-t-35">
                            <div class="card-header bg-white">
                                <i class="fas fa-user circle header_icon_margin"></i> All Reserved Lockers
                                <div class="radio_basic_swithes_padbott pull-right header_icon_margin_left" style="margin-top: 7px;padding-bottom:0px !important;">
                                    <input type="checkbox"  class="js-switch sm_toggle_show_only_deleted_outerblocks_main" name="roles" value="1" id="get_lost_lockers" />
                                    <span class="radio_switchery_padding">Recycle bin</span>
                                </div>
                            </div>
                            <div class="card-body p-t-10">
                                <div class="row p-t-15">
                                    <div class="col-md-2"><input type="text"  id="search_company" placeholder="Company" class="form-control"></div>
                                    <div class="col-md-2"><input type="text"   id="search_block" placeholder="Block" class="form-control"></div>
                                    <div class="col-md-2"><input type="text"   id="search_date" placeholder="date" class="form-control"></div>
                                    <div class="col-md-2"><input type="text"  id="search_bookingId" placeholder="Booking Id" class="form-control"></div>
                                    <div class="col-md-2"><input type="text"  id="search_name" placeholder="Name" class="form-control"></div>
                                    <div class="col-md-2"><input type="text"  id="search_email" placeholder="Email" class="form-control"></div>


                                </div>
                                <div class=" m-t-25">
                                    <table class="table table-bordered table-hover " style="width: 100%;" id="reserved_lockers">
                                        <thead>
                                        <tr>
                                            <th>Booking ID</th>
                                            <th>Currency</th>
                                            <th>Booking Date</th>
                                            <th>Name</th>
                                            <th>Location</th>
                                            <th>Block</th>
                                            <th>Number</th>
                                            <th style="width: 20%"></th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--- Edit model -->
        <div class="modal fade in " id="restore_booking" style="overflow-y: scroll;" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">

                    <form id="edit_company" action=" " method="POST">
                        @csrf
                        <div class="modal-header bg-primary">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">

                            <hr>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        {{--<input type="text">--}}

                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="modal-footer edit-button-section d-none" style="display: flex;">
                            <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                            <button type="submit" class="btn btn-primary "  id="save-edit"> Save Changes</button>
                        </div>

                    </form>
                    <br>

                </div>
            </div>
        </div>
        {{--<!-- END Edit modal-->--}}
    </div>
@endsection
@section('script')
    <!--plugin scripts-->
    <script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <!-- end of plugin scripts -->

    <script type="text/javascript" src="/backend/vendors/jquery.uniform/js/jquery.uniform.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputlimiter/js/jquery.inputlimiter.js"></script>
    <script type="text/javascript" src="/backend/vendors/chosen/js/chosen.jquery.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
    {{--<script type="text/javascript" src="/backend/vendors/jquery-tagsinput/js/jquery.tagsinput.min.js"></script>--}}
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.validVal.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/moment/js/moment.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/daterangepicker/js/daterangepicker.js"></script>
    <script type="text/javascript" src="/backend/vendors/datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/autosize/js/jquery.autosize.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputmask/js/inputmask.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputmask/js/jquery.inputmask.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputmask/js/inputmask.date.extensions.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputmask/js/inputmask.extensions.js"></script>
    <script type="text/javascript" src="/backend/vendors/multiselect/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.quicksearch/2.3.1/jquery.quicksearch.min.js"></script>

    <!--End of plugin scripts-->

    <!--Page level scripts-->

    <script type="text/javascript" src="/backend/js/form.js"></script>
    <script type="text/javascript" src="/backend/js/pages/form_elements.js"></script>
    <script type="text/javascript" src="/backend/js/pages/datatable_server_side.js"></script>
    <script src="{{ asset('custom/general.js') }}"></script>

    <script type="text/javascript" src="/backend/js/common.js"></script>
    <!-- end of global scripts-->

    <script type="text/javascript">

        var reserved_locker =  reserved_lockers();
       
         $("#search_date").datepicker();

        $(document).ready(function () {

            $("#search_date").change(function() {
                var checkbox_status = $("#get_lost_lockers").prop("checked");
                if(checkbox_status){
                    var lost_locker =  lost_lockers();
                    searchByThis(lost_locker);
                } else {
                    var default_locker =  reserved_lockers()
                    searchByThis(default_locker);
                }
            }); 

            $("#search_company").keypress(function(event) {
                if (event.keyCode === 13) 
                {
                    var checkbox_status = $("#get_lost_lockers").prop("checked");
                    if(checkbox_status){
                         var lost_locker =  lost_lockers();
                        searchByThis(lost_locker);
                    } else {
                        var default_locker =  reserved_lockers()
                        searchByThis(default_locker);
                    }
                }
            });
            $("#search_block").keypress(function(event) {
                if (event.keyCode === 13) {
                    var checkbox_status = $("#get_lost_lockers").prop("checked");
                    if(checkbox_status){
                        var lost_locker =  lost_lockers();
                        searchByThis(lost_locker);
                    } else {
                        var default_locker =  reserved_lockers()
                        searchByThis(default_locker);
                    }
                }
            });
            $("#search_bookingId").keypress(function(event) {
                if (event.keyCode === 13) {
                    var checkbox_status = $("#get_lost_lockers").prop("checked");
                    if(checkbox_status){
                        var lost_locker =  lost_lockers();
                        searchByThis(lost_locker);
                    } else {
                        var default_locker =  reserved_lockers()
                        searchByThis(default_locker);
                    }
                }
            });
            $("#search_name").keypress(function(event) {
                if (event.keyCode === 13) {
                    var checkbox_status = $("#get_lost_lockers").prop("checked");
                    if(checkbox_status){
                        var lost_locker =  lost_lockers();
                        searchByThis(lost_locker);
                    } else {
                        var default_locker =  reserved_lockers()
                        searchByThis(default_locker);
                    }
                }
            });
            $("#search_email").keypress(function(event) {
                if (event.keyCode === 13) {
                    var checkbox_status = $("#get_lost_lockers").prop("checked");
                    if(checkbox_status){
                        var lost_locker =  lost_lockers();
                        searchByThis(lost_locker);
                    } else {
                        var default_locker =  reserved_lockers()
                        searchByThis(default_locker);
                    }
                }
            });
            $('body').on('keypress','#search_input', function(event) {

                if (event.keyCode === 13) {

                    var checkbox_status = $("#get_lost_lockers").prop("checked");
                    if(checkbox_status){
                        var lost_locker =  lost_lockers();
                        searchByThis(lost_locker);
                    } else {
                        var default_locker =  reserved_lockers()
                        searchByThis(default_locker);
                    }
                }
            });


            $('body').on('change','#get_lost_lockers', function() {

                var checkbox_status = $("#get_lost_lockers").prop("checked");
                if(checkbox_status){
                    //  alert('delete');
                    lost_lockers();
                } else {
                    // alert('reserve');
                    reserved_lockers();
                }
                $('#payments_order_length').attr('style', 'width:100%;margin-top:30px;');

                var payments_search = "{{\Illuminate\Support\Facades\Session::get('reserved-lockers')}}";
                $('#reserved_lockers_wrapper>.row:nth-child(1)>div:nth-child(2)').append('<label style="float:right;margin-top:30px;"><input style="display:none;" class="form-control input-sm" id="search_input" value="' + payments_search + '" ></label>');
                var oTable = $('#reserved_lockers').dataTable();
                // Sometime later - filter...
                oTable.fnFilter(payments_search);

                $("#reserved_lockers_wrapper>.row").attr('style','width:100%');
                $("#reserved_lockers_length").addClass('pull-left');
                $("#reserved_lockers_filter").addClass('pull-right');
            });

            $("#reserved_lockers_wrapper>.row").attr('style','width:100%');
            $("#reserved_lockers_length").addClass('pull-left');
            $("#reserved_lockers_filter").addClass('pull-right');

            $('#payments_order_length').attr('style', 'width:100%;margin-top:30px;');

            var searchFunction = 'searchByThis(reserved_locker)';
            var payments_search = "{{\Illuminate\Support\Facades\Session::get('reserved-lockers')}}";

            $('#reserved_lockers_wrapper>.row:nth-child(1)>div:nth-child(2)').append('<label style="float:right;margin-top:30px;"><input type="text" class="form-control input-sm" id="search_input" value="' + payments_search + '" ></label>');

            var oTable = $('#reserved_lockers').dataTable();
            // Sometime later - filter...
            oTable.fnFilter(payments_search);
        });

        function reserved_lockers() {
            $('#reserved_lockers').dataTable().fnClearTable();
            $('#reserved_lockers').dataTable().fnDestroy();
            return $('#reserved_lockers').DataTable({
                processing: true,
                
                searching: false,
                
                "aaSorting": [[ 0, "desc" ]],
                "oLanguage": {
                    "sLengthMenu": "_MENU_",
                    "sSearch": "_INPUT_"
                },
                "aTargets": [ "_all" ],
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                ajax: '<?php echo route('reserved.lockers.show.all'); ?>',
                ajax: {
                    url: '<?php echo e(route('reserved.lockers.show.all')); ?>',
                    data: function (d) {
                        d.search_company = $('#search_company').val();
                        d.search_block = $('#search_block').val();
                        d.search_date = $('#search_date').val();
                        d.search_bookingId = $('#search_bookingId').val();
                        d.search_name = $('#search_name').val();
                        d.search_email = $('#search_email').val();
                        d.search_input = $('#search_input').val();
                    }
                },
                columns: [
                    {data: 'booking_id', name: 'booking_id'},
                    {data: 'company.currency', name: 'company.currency'},
                    {data: 'booking_date', name: 'booking_date'},
                    {data: 'name', name: 'name'},
                    {data: 'company.name', name: 'company.name'},
                    {data: 'block', name: 'block'},
                    {data: 'number', name: 'number'},
                    {data: 'action', name: 'action', orderable: false, searchable: true}
                ],
                initComplete : function() {
                    $("#reserved_lockers_length").addClass('pull-left');
                    $("#reserved_lockers_filter").addClass('pull-right');
                },
            });
        }

        function lost_lockers() {

            $('#reserved_lockers').dataTable().fnClearTable();
            $('#reserved_lockers').dataTable().fnDestroy();

            return $('#reserved_lockers').DataTable({
                processing: true,
               
                searching: false,
                
                "aaSorting": [[ 0, "desc" ]],
                "oLanguage": {
                    "sLengthMenu": "_MENU_",
                    "sSearch": "_INPUT_"
                },
                "aTargets": [ "_all" ],
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                ajax: '<?php echo route('reserved.lockers.delete.all'); ?>',
                ajax: {
                    url: '<?php echo e(route('reserved.lockers.delete.all')); ?>',
                    data: function (d) {
                        d.search_company = $('#search_company').val();
                        d.search_block = $('#search_block').val();
                        d.search_date = $('#search_date').val();
                        d.search_bookingId = $('#search_bookingId').val();
                        d.search_name = $('#search_name').val();
                        d.search_email = $('#search_email').val();
                        d.search_input = $('#search_input').val();
                    }
                },
                columns: [
                    {data: 'booking_id', name: 'booking_id'},
                    {data: 'company.currency', name: 'company.currency'},
                    {data: 'booking_date', name: 'booking_date'},
                    {data: 'name', name: 'name'},
                    {data: 'company.name', name: 'company.name'},
                    {data: 'block', name: 'block'},
                    {data: 'number', name: 'number'},
                    {data: 'action', name: 'action', orderable: false, searchable: true}
                ],


                initComplete : function() {
                    $("#reserved_lockers_length").addClass('pull-left');
                    $("#reserved_lockers_filter").addClass('pull-right');
                },
            });

        }

        function saveid(booking_id){
            console.log(booking_id);
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({

                url: "{{ route('reserved.lockers.restoreBooking') }}",

                type: 'POST',

                dataType: 'JSON',

                cache: false,

                data: {booking_id: booking_id, _token: CSRF_TOKEN},

                success: function (data) {
                    console.log(data);

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                }

            });
        }

    </script>
@endsection