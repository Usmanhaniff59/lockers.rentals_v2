@extends('backend.layouts.app')

@section('title', 'Lockers | Email Inbox')
@section('style')
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/mail_box.css"/>
@endsection
@section('content')

    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-sm-4">
                        <h4 class="nav_top_align">
                            <i class="fa fa-inbox circle"></i>
                            Inbox
                        </h4>
                    </div>
                    <div class="col-sm-8">
                        <ol class="breadcrumb float-right  nav_breadcrumb_top_align">
                            <li class="breadcrumb-item">
                                <a href="/admin">
                                    <i class="fa fa-home" data-pack="default" data-tags=""></i>
                                    Dashboard
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Email</a>
                            </li>
                            <li class="active breadcrumb-item">Inbox</li>
                        </ol>
                    </div>
                </div>
            </div>
        </header>
        <div class="outer">
            <div class="inner bg-container">
                <div class="row web-mail">
                    @include('backend.pages.emailManagement.emailsidebar')
                    <div class="col-lg-9">
                        <div class="card mail media_max_991">
                            <div class="card-header bg-white">
                                <div class="row">
                                    <div class="col-sm-6 col-12 m-t-10 dropdown_list_hover">
                                        <div class="btn-group float-left table-bordereds">
                                            <label class="custom-control custom-checkbox  mb-0 mr-0">
                                                <input type="checkbox" class="custom-control-input select-all">
                                                <span class="custom-control-label"></span>
                                            </label>
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li class="select-all1">
                                                    <span>All</span>
                                                </li>
                                                <li id="select-none">
                                                    <span>None</span>
                                                </li>
                                                <li id="mail_read">
                                                    <span>Read</span>
                                                </li>
                                                <li id="mail_unread">
                                                    <span>UnRead</span>
                                                </li>
                                                <li id="mail_starred">
                                                    <span>Starred</span>
                                                </li>
                                                <li id="mail_unstarred">
                                                    <span>Unstarred</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="btn-group float-left table-bordered text-primary" id="refresh_inbox">
                                            <i class="fa fa-refresh"></i>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-12">
                                        <div class="input-group margin bottom">
                                            <input type="text" class="form-control inbox_search_height m-t-10" placeholder="Search">
                                            <span class="input-group-append">
                                                <button type="button" class="btn btn-primary inbox_search_height m-t-10">Search</button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body m-t-25 p-d-0">
                                <div class="tabs tabs-bordered tabs-icons">
                                    <ul class="nav nav-tabs">
                                        <li class="nav-item" id="primary2">
                                            <a href="#primary" class="nav-link active" data-toggle="tab"
                                               aria-expanded="true"><i class="fa fa-inbox"></i> Primary</a>
                                        </li>
                                        @foreach($folders as $folder)
                                            <li class="nav-item" id="social2">
                                                <a href="#folder{{$folder->id}}" class="nav-link" data-toggle="tab"
                                                   aria-expanded="false"><i class="fa fa-group"></i> {{$folder->name}}</a>
                                            </li>
                                        @endforeach
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane table-responsive reset padding-all fade active show"
                                             id="primary">
                                            <table class="table">
                                                <tbody>
                                                @foreach($mails as $mail)
                                                <tr class="open_email_tr" data-id = "{{$mail->id}}">
                                                    <td>
                                                        <div class="checker m-l-20">
                                                            <label class="custom-control custom-checkbox">
                                                                <input name="checkbox" type="checkbox"
                                                                       class="custom-control-input ">
                                                                <span class="custom-control-label"></span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td><i class="fa fa-star text-warning"></i></td>
                                                    <td class="sent_to_mailview">{{$mail->email}}</td>
                                                    <td class="sent_to_mailview">{{$mail->subject}}</td>
                                                    <td class="sent_to_mailview"></td>
                                                    <td class="sent_to_mailview">{{$mail->created_at}}</td>
                                                </tr>
                                                    @endforeach
                                                </tbody>

                                            </table>
                                            {{ $mails->links() }}
                                        </div>

                                        @foreach($folders as $folder)
                                        <div class="tab-pane table-responsive reset padding-all fade"
                                             id="folder{{$folder->id}}">
                                            <table class="table">
                                                <tbody>

                                                @foreach($foldersemails as $foldersemail)
                                                    @if($foldersemail->email_folder_id == $folder->id)
                                                       <tr class="open_email_tr" data-id = "{{$foldersemail->id}}">
                                                    <td>
                                                        <div class="checker m-l-20">
                                                            <label class="custom-control custom-checkbox">
                                                                <input name="checkbox" type="checkbox"
                                                                       class="custom-control-input ">
                                                                <span class="custom-control-label"></span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td><i class="fa fa-star"></i></td>
                                                    <td class="sent_to_mailview">Peter Norton</td>
                                                    <td class="sent_to_mailview">{{$foldersemail->subject}}</td>
                                                    <td class="sent_to_mailview"></td>
                                                    <td class="sent_to_mailview">{{$foldersemail->created_at}}</td>
                                                </tr>
                                                   @endif
                                                @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                        @endforeach


                                        <div class="tab-pane table-responsive reset padding-all fade"
                                             id="promotions">
                                            <table class="table">
                                                <tbody>
                                                <tr class="mail-unread">
                                                    <td>
                                                        <div class="checker m-l-20">
                                                            <label class="custom-control custom-checkbox">
                                                                <input name="checkbox" type="checkbox"
                                                                       class="custom-control-input ">
                                                                <span class="custom-control-label"></span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td><i class="fa fa-star"></i></td>
                                                    <td class="sent_to_mailview">Ebay</td>
                                                    <td class="sent_to_mailview">Qonsequer novo dolores.</td>
                                                    <td class="sent_to_mailview"></td>
                                                    <td class="sent_to_mailview">11/04/2014 14:35 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="checker m-l-20">
                                                            <label class="custom-control custom-checkbox">
                                                                <input name="checkbox" type="checkbox"
                                                                       class="custom-control-input ">
                                                                <span class="custom-control-label"></span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td><i class="fa fa-star"></i></td>
                                                    <td class="sent_to_mailview">NewEgg</td>
                                                    <td class="sent_to_mailview">Lorem ipsum solet mit dolor.</td>
                                                    <td class="sent_to_mailview"></td>
                                                    <td class="sent_to_mailview">11/04/2014 14:35 AM</td>
                                                </tr>
                                                <tr class="">
                                                    <td>
                                                        <div class="checker m-l-20">
                                                            <label class="custom-control custom-checkbox">
                                                                <input name="checkbox" type="checkbox"
                                                                       class="custom-control-input ">
                                                                <span class="custom-control-label"></span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td><i class="fa fa-star"></i></td>
                                                    <td class="sent_to_mailview">BestBuy</td>
                                                    <td class="sent_to_mailview">Impedit vero possimus dolores.</td>
                                                    <td class="sent_to_mailview"><i class="fa fa-paperclip"></i></td>
                                                    <td class="sent_to_mailview">11/04/2014 14:35 AM</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.inner -->
        </div>
    </div>

@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $(document).on("click", ".open_email_tr", function() {
                var dataId = $(this).attr("data-id");
                window.location.href = '/admin/emailview/'+dataId;
            });
        });
    </script>
    <script type="text/javascript" src="/backend/js/pages/mail_box.js"></script>
@endsection
