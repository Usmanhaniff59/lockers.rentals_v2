@extends('backend.layouts.app')

@section('title', 'Lockers | Email Compose')
@section('style')
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/mail_box.css"/>
@endsection
@section('content')

    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-sm-4  skin_txt">
                        <h4 class="nav_top_align">
                            <i class="fa fa-edit circle"></i>
                            Compose
                        </h4>
                    </div>
                    <div class="col-sm-8  col-12">
                        <ol class="breadcrumb float-right  nav_breadcrumb_top_align">
                            <li class="breadcrumb-item">
                                <a href="index1.html">
                                    <i class="fa fa-home" data-pack="default" data-tags=""></i>
                                    Dashboard
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Email</a>
                            </li>
                            <li class="active breadcrumb-item">Compose</li>
                        </ol>
                    </div>
                </div>
            </div>
        </header>
        <div class="outer">
            <div class="inner bg-container">
                <div class="row web-mail mail_compose">
                    @include('backend.pages.emailManagement.emailsidebar')
                    <div class="col-lg-9">
                        <div class="card media_max_991">
                            <div class="card-header bg-white">
                                <i class="fa fa-edit"></i>
                                Compose Mail
                            </div>
                            <div class="card-body m-t-35">
                                <form action="{{URL::route('sendemail')}}" method="post">
                                    <div class="form-group">
                                        <input type="email" class="form-control" name="email_to" placeholder="To *" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control m-t-25" name="email_subject" placeholder="Subject *" required>
                                    </div>
                                    <div class="form-group mail_compose_wysi">
                                        <textarea class="wysihtml5 form-control m-t-20" name="email_html"></textarea>
                                    </div>
                                    <div class="form-group m-t-20">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-reply"></i> Send</button>
                                        <a href="/admin/emailinbox" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                                    </div>
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.inner -->
        </div>
    </div>

@endsection
@section('script')

    <script type="text/javascript" src="/backend/vendors/bootstrap3-wysihtml5-bower/js/bootstrap3-wysihtml5.all.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/bootstrap3_wysihtml5.js"></script>
    <script type="text/javascript" src="/backend/js/pages/mail_box.js"></script>
@endsection
