@extends('backend.layouts.app')

@section('title', 'Lockers | Email View')
@section('style')
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/mail_box.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/Buttons/css/buttons.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/buttons.css"/>
@endsection
@section('content')

    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-sm-4">
                        <h4 class="nav_top_align">
                            <i class="fa fa-eye circle"></i>
                            View Mail
                        </h4>
                    </div>
                    <div class="col-sm-8">
                        <ol class="breadcrumb float-right  nav_breadcrumb_top_align">
                            <li class="breadcrumb-item">
                                <a href="index1.html">
                                    <i class="fa fa-home" data-pack="default" data-tags=""></i>
                                    Dashboard
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Email</a>
                            </li>
                            <li class="active breadcrumb-item">View Mail</li>
                        </ol>
                    </div>
                </div>
            </div>
        </header>
        <div class="outer">
            <div class="inner bg-container">
                <div class="row web-mail mail_compose">
                    @include('backend.pages.emailManagement.emailsidebar')
                    <div class="col-lg-9">
                        <div class="card media_max_991">
                            <div class="card-header bg-white">
                                <p class="m-t-20">Subject: {{$mail->subject}}</p>
                                <p class="m-t-10"><span>From: {{$mail->email}} </span><span class="float-right">{{$mail->created_at}}</span></p>
                            </div>
                            <div class="card-body m-t-35">
                                {!! $mail->mail_body !!}
                                <br/>
                                <hr>
                                <div class="m-t-20">
                                    <form action="mail_sent.html" class="mail_view_wysi">
                                        <div class="form-group">
                                            <input type="email" class="form-control" id="forward_to" placeholder="To *" required="">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="wysihtml5 form-control m-t-20" placeholder="Reply or Forward"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-primary" id="goto_sent_page">Send</button>
                                            <a class="btn btn-primary" href="mail_view.html">Back</a>
                                        </div>
                                    </form>
                             <button class="btn btn-primary" ><i class="fa fa-reply"></i> Reply</button> {{--        id="view_reply1"--}}
                                    <button class="btn btn-primary" ><i class="fa fa-reply"></i> Forward</button> {{--        id="view_reply2"--}}
                                    @if($mail->deleted == 0)
                                        <a href="/email_delete/{{$mail->id}}" class="btn btn-warning" id="view_reply3"><i class="fa fa-trash-o"></i> Delete</a>
                                    @elseif($mail->deleted == 1)
                                        <a href="/email_trash/{{$mail->id}}" class="btn btn-danger" id="view_reply3"><i class="fa fa-trash-o"></i> Delete Permanently</a>
                                    @endif

                                            <button class="btn btn-danger  dropdown-toggle"
                                                    type="button" id="up1" data-toggle="dropdown"
                                                    aria-haspopup="true" aria-expanded="false">
                                                Move To
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="up1">
                                                <a class="dropdown-item" href="/admin/emailmove/{{$mail->id}}/0">Primary</a>
                                                @foreach($folders as $folder)
                                                   <a class="dropdown-item" href="/admin/emailmove/{{$mail->id}}/{{$folder->id}}">{{$folder->name}}</a>
                                                @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.inner -->
        </div>
    </div>

@endsection
@section('script')
    <script type="text/javascript" src="/backend/js/pages/mail_box.js"></script>
    <script type="text/javascript" src="/backend/vendors/Buttons/js/buttons.js"></script>

@endsection
