@php
    $compose_class = array();
    $inbox_class = array();
    $sent_class = array();
    $spam_class = array();
    $draft_class = array();
    $trash_class = array();
    if(isset($email_page) && $email_page == 'Compose'){
    $compose_class[0] = 'bg-success';
    $compose_class[1] = 'mail_inbox_text_col';
    }
    else{
    $compose_class[0] = '';
    $compose_class[1] = '';
    }
    if(isset($email_page) && $email_page == 'Inbox'){
    $inbox_class[0] = 'bg-success';
    $inbox_class[1] = 'mail_inbox_text_col';
    }
    else{
    $inbox_class[0] = '';
    $inbox_class[1] = '';
    }
    if(isset($email_page) && $email_page == 'Sent'){
    $sent_class[0] = 'bg-success';
    $sent_class[1] = 'mail_inbox_text_col';
    }
    else{
    $sent_class[0] = '';
    $sent_class[1] = '';
    }
    if(isset($email_page) && $email_page == 'Spam'){
    $spam_class[0] = 'bg-success';
    $spam_class[1] = 'mail_inbox_text_col';
    }
    else{
    $spam_class[0] = '';
    $spam_class[1] = '';
    }
    if(isset($email_page) && $email_page == 'Draft'){
    $draft_class[0] = 'bg-success';
    $draft_class[1] = 'mail_inbox_text_col';
    }
    else{
    $draft_class[0] = '';
    $draft_class[1] = '';
    }
    if(isset($email_page) && $email_page == 'Trash'){
    $trash_class[0] = 'bg-success';
    $trash_class[1] = 'mail_inbox_text_col';
    }
    else{
    $trash_class[0] = '';
    $trash_class[1] = '';
    }
@endphp
<div class="col-lg-3 mail_compose_list">
    @if(Session::has('flash_message'))
        <div class="alert alert-info alert-dismissable" style="margin-bottom: 1rem;">
            <button type="button" class="close" data-dismiss="alert"
                    aria-hidden="true" style="margin-top: 4px; margin-left: 15px;"> ×
            </button>
            {{ Session::get('flash_message') }}
        </div>
    @endif
    <div>
        <ul class="list-group">
            <li class="list-group-item {{$compose_class[0]}}">
                <a href="/admin/emailcompose" class="{{$compose_class[1]}}">
                    <i class="fa fa-edit"></i>
                    Compose
                </a>
            </li>
            <li class="list-group-item {{$inbox_class[0]}}">
                <a href="/admin/emailinbox" class="{{$inbox_class[1]}}">
                    <i class="fa fa-inbox"></i>
                    Inbox
                </a>
            </li>
            <li class="list-group-item {{$sent_class[0]}}">
                <a href="/admin/emailsent" class="{{$sent_class[1]}}">
                    <i class="fa fa-sign-out"></i>
                    Sent
                </a>
            </li>
            <li class="list-group-item {{$spam_class[0]}}">
                <a href="/admin/emailspam" class="{{$spam_class[1]}}">
{{--                    <span class="badge badge-pill badge-primary float-right">14</span>--}}
                    <i class="fa fa-eye-slash"></i>
                    Spam
                </a>
            </li>
            <li class="list-group-item {{$draft_class[0]}}">
                <a href="/admin/emaildraft" class="{{$draft_class[1]}}">
{{--                    <span class="badge badge-pill badge-primary float-right">16</span>--}}
                    <i class="fa fa-recycle"></i>
                    Draft
                </a>
            </li>
            <li class="list-group-item {{$trash_class[0]}}">
                <a href="/admin/emailtrash" class="{{$trash_class[1]}}">
{{--                    <span class="badge badge-pill badge-primary float-right">16</span>--}}
                    <i class="fa fa-trash"></i>
                    Trash
                </a>
            </li>
{{--            <li class="list-group-item" id="more_items">--}}
{{--                <a>--}}
{{--                    <i class="fa fa-angle-down float-right"></i>--}}
{{--                    More--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li class="list-group-item starred_mail">--}}
{{--                <a href="#">--}}
{{--                    <span class="badge badge-pill badge-primary float-right">3</span>--}}
{{--                    <i class="fa fa-star"></i>--}}
{{--                    Starred--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li class="list-group-item starred_mail">--}}
{{--                <a href="#">--}}
{{--                    <span class="badge badge-pill badge-primary float-right">14</span>--}}
{{--                    <i class="fa fa-user"></i>--}}
{{--                    Personal--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li class="list-group-item starred_mail">--}}
{{--                <a href="#">--}}
{{--                    <span class="badge badge-pill badge-primary float-right">26</span>--}}
{{--                    <i class="fa fa-shield"></i>--}}
{{--                    Client--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li class="list-group-item starred_mail">--}}
{{--                <a href="#">--}}
{{--                    <span class="badge badge-pill badge-primary float-right">36</span>--}}
{{--                    <i class="fa fa-briefcase "></i>--}}
{{--                    Important--}}
{{--                </a>--}}
{{--            </li>--}}
        </ul>
    </div>
    <div class="mail_ul_active m-t-35">
        <ul class="list-group">
            <li class="list-group-item bg-success">
                <a href="#" class="mail_inbox_text_col">
                    <i class="fa fa-comments"></i>
                    Contacts
                </a>
            </li>
        </ul>
    </div>
    <div>
        <ul class="list-group contact_scroll">
            @foreach($users as $user)
              <li class="list-group-item status_height">
                <a href="#">
                    <span class="status-online m-t-10"></span>
                    &nbsp; {{$user->name}}
                    <span class="float-left">
                                            <img src="/backend/img/mailbox_imgs/1.jpg"
                                                 class="rounded-circle img-responsive float-left inbox_contact_img" alt="Image">
                                        </span>
                </a>
              </li>
            @endforeach

        </ul>
    </div>
</div>