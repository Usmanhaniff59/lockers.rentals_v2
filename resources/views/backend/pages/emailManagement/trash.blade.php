@extends('backend.layouts.app')

@section('title', 'Lockers | Email Trash')
@section('style')
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/mail_box.css"/>
@endsection
@section('content')

    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-sm-4">
                        <h4 class="nav_top_align">
                            <i class="fa fa-trash circle"></i>
                            Trash
                        </h4>
                    </div>
                    <div class="col-sm-8 col-12">
                        <ol class="breadcrumb float-right  nav_breadcrumb_top_align">
                            <li class="breadcrumb-item">
                                <a href="/admin">
                                    <i class="fa fa-home" data-pack="default" data-tags=""></i>
                                    Dashboard
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Email</a>
                            </li>
                            <li class="active breadcrumb-item">Trash</li>
                        </ol>
                    </div>
                </div>
            </div>
        </header>
        <div class="outer">
            <div class="inner bg-container">
                <div class="row web-mail">
                    @include('backend.pages.emailManagement.emailsidebar')
                    <div class="col-lg-9">
                        <div class="card mail mail_sent_all media_max_991">
                            <div class="card-header bg-white">
                                <div class="row">
                                    <div class="col-sm-6 col-12 m-t-10 dropdown_list_hover">
                                        <div class="btn-group float-left table-bordereds">
                                            <label class="custom-control custom-checkbox mb-0 mr-0">
                                                <input type="checkbox" class="custom-control-input select_all_mail">
                                                <span class="custom-control-label"></span>
                                            </label>
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li class="select-all1">
                                                    <span>All</span>
                                                </li>
                                                <li id="select-none">
                                                    <span>None</span>
                                                </li>
                                                <li id="mail_read">
                                                    <span>Read</span>
                                                </li>
                                                <li id="mail_unread">
                                                    <span>UnRead</span>
                                                </li>
                                                <li id="mail_starred">
                                                    <span>Starred</span>
                                                </li>
                                                <li id="mail_unstarred">
                                                    <span>Unstarred</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="btn-group float-left table-bordered text-primary" id="refresh_trash">
                                            <i class="fa fa-refresh"></i>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-12">
                                        <div class="input-group margin bottom">
                                            <input type="text" class="form-control inbox_search_height m-t-10" placeholder="Search">
                                            <span class="input-group-append">
                                                <button type="button" class="btn btn-primary inbox_search_height m-t-10">Search</button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body reset padding padding-all">
                                <div class="table-responsive">
                                    <table class="table">
                                        <table class="table">
                                            <tbody>
                                            @foreach($mails as $mail)
                                                <tr class="open_email_tr" data-id = "{{$mail->id}}">
                                                    <td>
                                                        <div class="checker m-l-20">
                                                            <label class="custom-control custom-checkbox">
                                                                <input name="checkbox" type="checkbox"
                                                                       class="custom-control-input ">
                                                                <span class="custom-control-label"></span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td><i class="fa fa-star text-warning"></i></td>
                                                    <td class="sent_to_mailview">{{$mail->email}}</td>
                                                    <td class="sent_to_mailview">{{$mail->subject}}</td>
                                                    <td class="sent_to_mailview"></td>
                                                    <td class="sent_to_mailview">{{$mail->created_at}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.inner -->
        </div>
    </div>

@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $(document).on("click", ".open_email_tr", function() {
                var dataId = $(this).attr("data-id");
                window.location.href = '/admin/emailview/'+dataId;
            });
        });
    </script>
    <script type="text/javascript" src="/backend/js/pages/mail_box.js"></script>
@endsection
