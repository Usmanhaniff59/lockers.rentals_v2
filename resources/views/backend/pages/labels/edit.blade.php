@extends('backend.layouts.app')

@section('title', 'Lockers | Edit Label')

@section('content')

    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-user-plus"></i>
                            Edit {{$label->key}}
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        <div class="outer">
            <div class="inner bg-container">


                <!--top section widgets-->
                <div class="row widget_countup">
                    <div class="col-12">

                        <hr>
                        {{-- @include ('errors.list') --}}

                        {{ Form::model($label, array('route' => array('labels.update', $label->key), 'method' => 'PUT')) }} {{-- Form model binding to automatically populate our fields with user data --}}

                        <div class="form-group">
                            <h3>{{ Form::label('title', 'Title:') }}
                            <b>{{ Form::label('key', $label->key) }}</b></h3>
                        </div>

                        <div class="form-group">
                            {{ Form::label('label', 'Label') }}
                            {{ Form::textarea('value', null, array('class' => 'form-control')) }}
                        </div>

                        {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

                        {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection