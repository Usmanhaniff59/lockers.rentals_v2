@extends('backend.layouts.app')

@section('title', 'Lockers | Labels')
@section('style')
    <!--plugin styles-->
    <link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css" />


    <link type="text/css" rel="stylesheet" href="/frontend/vendors/chosen/css/chosen.css"/>
    <link type="text/css" rel="stylesheet" href="/frontend/css/pages/form_elements.css"/>


    <!-- end of plugin styles -->
    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css" />
@endsection
@section('content')
    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-tags circle"></i>
                            Label Administration
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        <div class="outer">
            <div class="inner bg-container">
                <div class="row">
                    <div class="col-12 data_tables">

                        <div class="card m-t-35">
                            <div class="card-header bg-white">
                                <div class="row">
                                    @if(Session::has('flash_message'))
                                        <div class="col-md-12">
                                            <div class="alert alert-success alert-dismissable float-right" style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-hidden="true" style="margin-top: 4px; margin-left: 15px;"> ×
                                                </button>
                                                {{ Session::get('flash_message') }}
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <i class="fa fa-tags circle"></i> All Labels  @can('Create Label')
                                            <a href="#create" class="btn btn-primary pull-right btn-md adv_cust_mod_btn" data-toggle="modal" data-href="#create" >Add New Label</a>
                                        @endcan
                                    </div>
                                </div>


                            </div>
                            <div class="card-body p-t-10">
                                <div class=" m-t-25">
                                    <table class="table table-striped table-bordered table-hover " id="sample_6">
                                        <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Language</th>
                                            <th>Label</th>
                                            <th>Operations</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($labels as $label)
                                            <tr>

                                                <td> {{ $label->key }}</td>
                                                <td> {{ $label->lang }}</td>
                                                <td>{!! str_limit($label->value, $limit = 50, $end = '...')  !!}  </td>
                                                {{--<td>{!! Illuminate\Support\Str::limit($label->value, 50, $end = '...')  !!}  </td>--}}

                                                <td>
                                                    @can('Edit Label')
                                                        <a href="#edit" class="btn btn-primary btn-md adv_cust_mod_btn edit-btn pull-left" data-toggle="modal" style="margin-right: 3px;" data-href="#edit"  data-key="{{ $label->key }}" data-lang="{{ $label->lang }}"data-value="{{ $label->value }}" > Edit</a>

                                                    @endcan
                                                        @can('Delete Label')
                                                            <a href="{{route('destroylabel', ['key' => $label->key, 'lang' => $label->lang ]) }}" class="btn btn-danger pull-left" style="margin-right: 3px;">Delete</a>

                                                        @endcan
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!--- create model -->
    <div class="modal fade in display_none" id="create" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                {{ Form::open(array('url' => 'admin/labels')) }}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <h3><i class="fa fa-plus-circle circle"></i> Add Label</h3>
                            <hr>
                            <br>
                            <div class="form-group">
                                {{ Form::label('title', 'Title') }}
                                {{ Form::text('key', '', array('class' => 'form-control')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('language', 'Language') }}
                                {{ Form::select('lang' , $langs, 'en' ,  ['class' => 'chzn-select form-control']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('title', 'Value') }}
                                {{ Form::textarea('value', '', array('class' => 'form-control')) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="display: block">
                    <div class="pull-right" style="padding-bottom: 20px;">
                        <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                        {{ Form::submit('Create Label', array('class' => 'btn btn-primary')) }}
                    </div>
                </div>
                {{ Form::close() }}
                <br>
            </div>
        </div>
    </div>
    <!-- END create modal-->

    <!--- Edit model -->
    <div class="modal fade in display_none" id="edit" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                {{ Form::open(array('route' => array('labels.update', 1), 'method' => 'PUT')) }}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <h3 id="edit-title"></h3>
                            <hr>
                            <br>
                            {{ Form::hidden('id', null, array('class' => 'form-control', 'id' => 'id_edit')) }}
                            {{ Form::hidden('lang', null, array('class' => 'form-control', 'id' => 'lang_edit')) }}

                            <div class="form-group">
                                <h3>{{ Form::label('title', 'Title:') }}
                                    <b>{{ Form::label('key', null, array('id' => 'key_edit')) }}</b></h3>
                            </div>

                            <div class="form-group">
                                {{ Form::label('label', 'Label') }}
                                {{ Form::textarea('value', null, array('class' => 'form-control', 'id' => 'value_edit')) }}
                            </div>

                        </div>

                    </div>
                </div>
                <div class="modal-footer" style="display: block">
                    <div class="pull-right" style="padding-bottom: 20px;">
                        <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                        {{ Form::submit('Edit Label', array('class' => 'btn btn-primary  ')) }}
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!-- END Edit model-->

@endsection
@section('script')
    <!--plugin scripts-->
    <script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <!-- end of plugin scripts -->
    <!--Page level scripts-->
    <script type="text/javascript" src="/backend/js/pages/datatable.js"></script>


    <script type="text/javascript" src="/backend/vendors/chosen/js/chosen.jquery.js"></script>
    <script type="text/javascript" src="/backend/js/pages/form_elements.js"></script>

    <!-- end of global scripts-->
    <script type="text/javascript">

        $('body').on('click','.edit-btn',function(){

            $('#edit-title').html(' <i class="fa fa-pencil circle"></i>  Edit '+ $(this).data('key'));
            $('#id_edit').val($(this).data('key'));
            $('#lang_edit').val($(this).data('lang'));
            $('#key_edit').text($(this).data('key'));
            $('#value_edit').val($(this).data('value'));


        });

    </script>
@endsection