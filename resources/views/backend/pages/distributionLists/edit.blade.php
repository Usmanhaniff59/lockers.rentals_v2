@extends('backend.layouts.app')
@section('style')
    <link type="text/css" rel="stylesheet" media="screen" href="backend/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/wizards.css"/>s.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrapvalidator/css/bootstrapValidator.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/wizards.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/Buttons/css/buttons.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/buttons.css"/>
@endsection

@section('title', 'Lockers | Edit Distribution list Group')

@section('content')
    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-pencil"></i>
                            Edit Distribution list Group
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        <div class="outer">
            <div class="inner bg-container">


                <!--top section widgets-->
                <div class="row widget_countup">
                    <div class="col-12">

                        <hr>
                        @include ('errors.list')




                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection

@section('script')
    <script type="text/javascript" src="/backend/vendors/bootstrapvalidator/js/bootstrapValidator.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/twitter-bootstrap-wizard/js/jquery.bootstrap.wizard.min.js"></script>
    <script type="text/javascript" src="backend/js/pages/form_editors.js"></script>
    <script type="text/javascript" src="/backend/js/pages/wizard.js"></script>
    <script type="text/javascript" src="/backend/vendors/raphael/js/raphael.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/Buttons/js/scrollto.js"></script>
    <script type="text/javascript" src="/backend/vendors/Buttons/js/buttons.js"></script>
@endsection