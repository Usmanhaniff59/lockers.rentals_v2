@extends('backend.layouts.app')

@section('title', 'Lockers | Distribution list Group')
@section('style')
    <!--plugin styles-->
    <link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/chosen/css/chosen.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/jquery-tagsinput/css/jquery.tagsinput.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/modal/css/component.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrapvalidator/css/bootstrapValidator.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/wizards.css"/>
    <link rel="stylesheet" type="text/css" href="/backend/vendors/animate/css/animate.min.css" />
    <!--End of plugin styles-->

    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/form_elements.css"/>

    <style>
        a:hover {
            color: #00bfff;
            background-color: transparent;
            text-decoration: underline;
        }
        a:active {
            color: yellow;
            background-color: transparent;
            text-decoration: underline;
        }
    </style>
@endsection
@section('content')
    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-th-list circle"></i>
                            Distribution list Group Administration
                        </h4>
                    </div>
                </div>
            </div>
        </header>

        @if(Session::has('flash_message'))
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable float-right" style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-hidden="true" style="margin-top: 4px; margin-left: 15px;"> ×
                    </button>
                    {{ Session::get('flash_message') }}
                </div>
            </div>
        @endif

        <div class="outer">
            <div class="inner bg-container">
                <div class="row">
                    <div class="col-12 data_tables">
                        <div class="card m-t-35">
                            <div class="card-header bg-white">
                                <i class="fa fa-th-list circle"></i> All Distribution list Group  @can('Create DistributionListGroup')
                                    <a href="#create" class="btn btn-primary btn-md adv_cust_mod_btn pull-right" data-toggle="modal" data-href="#create" >Add New Distribution Group </a>
                                @endcan
                            </div>
                            <div class="card-body p-t-10">
                                <div class=" m-t-25">
                                    <table class="table table-striped table-bordered table-hover " id="sample_6">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Date/Time Added</th>
                                            <th>Actions</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($distributionLists)
                                            @foreach ($distributionLists as $distributionList)

                                            <tr>
                                                <td><a href="{{ URL::to('admin/distributionlists/'.$distributionList->id) }}" >{{ $distributionList->name }}</a></td>
                                                <td>{{ $distributionList->created_at->format('F d, Y h:ia') }}</td>
                                                <td>
                                                    <a href="#view" class="btn btn-success btn-md adv_cust_mod_btn pull-left view-btn" data-toggle="modal" data-href="#view" data-id="{{ $distributionList->id }}"  data-name="{{ $distributionList->name }}"  style="margin-right: 3px;">View</a>

                                                    {{--<a href="{{ URL::to('admin/distributionlists/'.$distributionList->id) }}" class="btn btn-success pull-left" style="margin-right: 3px;">View</a>--}}
                                                @can('Edit DistributionListGroup')
                                                       {{--<a href="{{ route('distributionlists.edit', $distributionList->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Edit</a>--}}
                                                        <a href="#edit" class="btn btn-info btn-md adv_cust_mod_btn pull-left edit-btn" data-toggle="modal" data-href="#edit" data-id="{{ $distributionList->id }}" data-name="{{ $distributionList->name }}"  style="margin-right: 3px;">Edit</a>
                                                @endcan
                                                @can('Delete DistributionListGroup')
                                                    {!! Form::open(['method' => 'DELETE', 'route' => ['distributionlists.destroy', $distributionList->id] ]) !!}
                                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                                    {!! Form::close() !!}
                                                @endcan

                                                </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--- create model -->
    <div class="modal fade in display_none" id="create" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <form action="{{ route('distributionlists.store') }}"  method="POST" id="add-new-form">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4><i class="fa fa-plus-circle circle"></i> Add New Distribution Group</h4>
                                <hr>
                                <br>
                                <label>Distribution Group Name</label>
                                <p>
                                    <input id="name" name="name" type="text" placeholder="Group Name"
                                           class="form-control"></p>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer" style="display: flex;">
                        <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                        <button type="submit" class="btn btn-primary" id="save-add-btn" data-resource="admin/distributionlists">Save changes</button>
                    </div>
                </form>
                <br>

            </div>
        </div>
    </div>
    <!-- END create modal-->

    <!--- Edit model -->
    <div class="modal fade in display_none" id="edit" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="outer form_wizzards">
                                <div class="inner bg-container">
                                    <div class="row">
                                        <div class="col">
                                            <div class="card">
                                                <div class="card-header bg-white" id="dist_group_header"></div>
                                                   <form method="POST" action="{{ route('distributionlistmembers.store') }}" id="dsitEditForm">
                                                    @csrf
                                                    <div class="card-body m-t-20">
                                                        <div id="rootwizard_no_val">
                                                            <ul class="nav nav-pills">
                                                                <li class="nav-item user1 m-t-15">
                                                                    <a class="nav-link" href="#dist-group" data-toggle="tab"><span
                                                                                class="userprofile_tab">1</span>Distribution List
                                                                        Group
                                                                    </a>
                                                                </li>
                                                                <li class="nav-item finish_tab m-t-15">
                                                                    <a class="nav-link " href="#users" data-toggle="tab"><span>3</span>Users</a>
                                                                </li>
                                                            </ul>
                                                            <div class="tab-content m-t-20">

                                                                <div class="tab-pane" id="dist-group">
                                                                    <br>
                                                                    <h2>Edit Distribution List
                                                                        Group </h2>
                                                                    <hr>
                                                                    <div class="form-group">
                                                                        <label>Distribution List
                                                                            Group</label>
                                                                        <input type="hidden" class="form-control" name="distribution_list_group_id" id="dist_group_id"  placeholder="Name">
                                                                        <input type="text" class="form-control" name="name" id="dist_group_name"  placeholder="Name">

                                                                    </div>
                                                                        <br>
                                                                    <ul class="pager wizard pager_a_cursor_pointer">
                                                                        <li class="previous previous_btn1"><a>Previous</a></li>
                                                                        <li class="next next_btn1"><a>Next</a></li>
                                                                    </ul>
                                                                </div>

                                                                <div class="tab-pane" id="users">
                                                                    <br>
                                                                    <h2>Users </h2>
                                                                    <hr>
                                                                    <div id="users_append"></div>

                                                                    <br>
                                                                    <ul class="pager wizard pager_a_cursor_pointer">
                                                                        <li class="previous previous_btn3"><a>Previous</a></li>
                                                                        <li class="next disabled" id="save-edit-group"><a>Finish</a></li>
                                                                    </ul>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
    <!-- END Edit modal-->

    <!--- view model -->
    <div class="modal fade in display_none" id="view" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h3><i class="fa fa-eye circle"></i> View Distribution List Group Members</h3>
                                <hr>
                                <br>
                                <h4 style="margin-left: 20px" id="group_name"></h4>
                                <hr>
                                <div id="users_view"></div>
                                <hr>

                            </div>
                        </div>
                    </div>
                <br>
            </div>
        </div>
    </div>
    <!-- END view modal-->


@endsection
@section('script')
    <!--plugin scripts-->
    <script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/backend/js/pages/datatable.js"></script>

   <script type="text/javascript" src="/backend/vendors/bootstrapvalidator/js/bootstrapValidator.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/twitter-bootstrap-wizard/js/jquery.bootstrap.wizard.min.js"></script>
    <!--end of plugin scripts-->
    <script src="{{ asset('custom/general.js') }}"></script>

    <script type="text/javascript" src="/backend/js/pages/wizard.js"></script>
    <script type="text/javascript" src="/backend/js/pages/form_elements.js"></script>

    <script type="text/javascript">


        $('body').on('click','.edit-btn',function(e){

            var dist_group_id = $(this).data('id');
            $("#dsitEditForm")[0].reset();
            $('#dist_group_id').val($(this).data('id'));
            $('#dist_group_name').val($(this).data('name'));
            $('#dist_group_header').html('<i class="fa fa-pencil circle"></i> Edit Group ' + $(this).data('name'));
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: '/admin/getnondistributionusers',
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {dist_group_id: dist_group_id, _token: CSRF_TOKEN},
                success: function (data) {
                    console.log(data);
                    if(data.length != 0) {
                        var users = '';
                        $.each(data, function (index, user) {

                            users += '<label class="custom-control custom-checkbox" >';
                            users += '<input type="checkbox" class="custom-control-input" name="users[]" value="' + user.id + '">';
                            users += '<span class="custom-control-label"></span>';
                            users += '<span class="custom-control-description"> ' + user.email + ' (' + user.name + ')</span>';
                            users += '</label>';
                        });
                        // $('#user-error-message').html('');
                        $('#users_append').html(users);
                    }else{
                        $('#users_append').html('<p>No User Available To Add In This Group!</p>');
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                }
            });

        });

        $('body').on('change','#email_template_id',function(e){

            $('#users-append').html('');
            var dist_group_id = $('#dist_group_id').val();
            var email_template_id = $('#email_template_id').val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            if(email_template_id != '') {
                $.ajax({
                    url: '/admin/getnondistributionusers',
                    type: 'POST',
                    dataType: 'JSON',
                    cache: false,
                    data: {dist_group_id: dist_group_id, email_template_id: email_template_id, _token: CSRF_TOKEN},
                    success: function (data) {

                        var users = '';
                        $.each(data, function (index, user) {

                            users += '<label class="custom-control custom-checkbox" >';
                            users += '<input type="checkbox" class="custom-control-input" name="users[]" value="' + user.id + '">';
                            users += '<span class="custom-control-label"></span>';
                            users += '<span class="custom-control-description"> ' + user.email + ' (' + user.name + ')</span>';
                            users += '</label>';
                        });
                        $('#user-error-message').html('');
                        $('#users-append').html(users);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR);
                    }
                });
            }else{
                $('#users-append').html('<p>Select email Template!</p>');
            }

        });

        <!-- view modal data -->
        $('body').on('click','.view-btn',function(e){

            var dist_group_id = $(this).data('id');
            $('#dist_group_id').val($(this).data('id'));
            $('#group_name').html('<b>' + $(this).data('name')+'</b>');
            // $('#dist_group_header').html('<i class="fa fa-file-text-o"></i>Edit Group ' + $(this).data('name'));

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                url: '/admin/distributionlists/view',
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {dist_group_id: dist_group_id , _token: CSRF_TOKEN},
                success: function (data) {
                    console.log(data);
                    var users = '';
                    if(data.length == 0){
                        users +='<p>No Member Selected Yet!</P>';
                    }else {

                        $.each(data, function (index, user) {

                            users += '<p style="margin-left: 20px">' + user.email + ' (' + user.name + ')</p>';

                        });
                    }
                    $('#users_view').html(users);


                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                }
            });


        });


        <!-- submit Edit button modal form  -->
        $('body').on('click','#save-edit-group',function(e){
            $("#dsitEditForm").submit();

        });


    </script>
@endsection