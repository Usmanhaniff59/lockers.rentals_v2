@extends('backend.layouts.app')

@section('title', 'Lockers | Add User')
@section('style')
    <!--plugin styles-->
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/wizards.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrapvalidator/css/bootstrapValidator.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/wizards.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/Buttons/css/buttons.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/buttons.css"/>
    <!--End of global styles-->


    <!-- end of plugin styles -->
@endsection
@section('content')

    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-user-plus"></i>
                             Distribution List Group {{$distributionLists->name}}
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        <div class="outer">
            <div class="inner bg-container">

            @include ('errors.list')
                <!--top section widgets-->
                <div class="row widget_countup">
                    <div class="col-12">
                        <h2>Email Templates </h2>
                        <hr>
                        <input type="hidden" id="dist_group_id" value="{{$distributionLists->id}}">
                        <div class="form-group">
                            <label>Email Templates</label>
                            <select class="custom-select form-control"
                                    title="Select an email template..." name="email_template_id" id="email_template_id">
                                <option value="">Select</option>
                                @foreach($email_templates as $email_template)
                                    <option value="{{$email_template->id}}">{{$email_template->template_name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div id="users-append"></div>




                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <!--plugin scripts-->
    <script type="text/javascript" src="/backend/vendors/bootstrapvalidator/js/bootstrapValidator.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/twitter-bootstrap-wizard/js/jquery.bootstrap.wizard.min.js"></script>
    <!--End of plugin scripts-->
    <!--Page level scripts-->
    <script type="text/javascript" src="/backend/js/pages/wizard.js"></script>
    <script type="text/javascript" src="/backend/vendors/raphael/js/raphael.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/Buttons/js/scrollto.js"></script>
    <script type="text/javascript" src="/backend/vendors/Buttons/js/buttons.js"></script>
    <script>
        $('body').on('change','#email_template_id',function(e){

            $('#users-append').html('');
            var dist_group_id = $('#dist_group_id').val();

            var email_template_id = $('#email_template_id').val();
            console.log(email_template_id);

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            if(email_template_id != '') {
                $.ajax({
                    url: '/admin/getdistributionusers',
                    type: 'POST',
                    dataType: 'JSON',
                    cache: false,
                    data: {dist_group_id: dist_group_id, email_template_id: email_template_id, _token: CSRF_TOKEN},
                    success: function (data) {
                        console.log(data);
                        if(data.length!=0) {
                            var users = '';

                            users += '<h3   style="margin-left: 10px">Group Users</h3><hr>';
                            users += '<ul   style="margin-left: 10px">';


                            $.each(data, function (index, user) {

                                users += '<li >' + user.email + ' (' + user.name + ')</li>';

                            });
                            users += '</ul>';
                            // $('#user-error-message').html('');
                            $('#users-append').html(users);
                        }else{
                            $('#users-append').html('<p>No User Added Against Selected Email Template!</p>');
                        }


                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('error');
                    }
                });
            }else{
                $('#users-append').html('<p>Select email Template!</p>');
            }

        });
    </script>

@endsection
