@extends('backend.layouts.app')

@section('title', 'Lockers | Add Help')

@section('content')

    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-user-plus"></i>
                            Add Help
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        <div class="outer">
            <div class="inner bg-container">


                <!--top section widgets-->
                <div class="row widget_countup">
                    <div class="col-12">

                        <hr>

                        {{-- @include ('errors.list') --}}

                        {{ Form::open(array('url' => 'admin/labels')) }}

                        <div class="form-group">
                            {{ Form::label('title', 'Title') }}
                            {{ Form::text('key', '', array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('language', 'Language') }}
                            {{ Form::select('lang' , array('en' => 'English', 'fr' => 'French'), 'en' ,  ['class' => 'form-control']) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('title', 'Value') }}
                            {{ Form::textarea('value', '', array('class' => 'form-control')) }}
                        </div>


                        {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}

                        {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection