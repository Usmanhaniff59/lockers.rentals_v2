@extends('backend.layouts.app')

@section('title', 'Lockers | Languages')
@section('style')
    <!--plugin styles-->
    <link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css" />

    <link type="text/css" rel="stylesheet" href="/backend/vendors/chosen/css/chosen.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/form_elements.css"/>

    <!-- end of plugin styles -->
    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css" />
@endsection
@section('content')
    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-tags circle"></i>
                            Label Administration
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        <div class="outer">
            <div class="inner bg-container">
                <div class="row">
                    <div class="col-12 data_tables">

                        <div class="card m-t-35">
                            <div class="card-header bg-white">
                                <div class="row">
                                    @if(Session::has('flash_message'))
                                        <div class="col-md-12">
                                            <div class="alert alert-success alert-dismissable float-right" style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-hidden="true" style="margin-top: 4px; margin-left: 15px;"> ×
                                                </button>
                                                {{ Session::get('flash_message') }}
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <i class="fa fa-tags circle"></i> All Languages  @can('Create Language')
                                            <a href="#create" class="btn btn-primary btn-md pull-right adv_cust_mod_btn" data-toggle="modal" data-href="#create" >Add New Language</a>
                                        @endcan
                                    </div>
                                </div>

                            </div>
                            <div class="card-body p-t-10">
                                <div class=" m-t-25">
                                    <table class="table table-striped table-bordered table-hover " id="sample_6">
                                        <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Native Name</th>
                                            <th>Flag</th>
                                            <th>Locale</th>
                                            <th>Canonical Locale</th>
                                            <th>Full Locale</th>
                                            <th>Status</th>
                                            <th>Operations</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($languages as $language)
                                            <tr>

                                                <td> {{ $language->name }}</td>
                                                <td> {{ $language->native_name }}</td>
                                                <td> {{ $language->flag }}</td>
                                                <td> {{ $language->locale }}</td>
                                                <td> {{ $language->canonical_locale }}</td>
                                                <td> {{ $language->full_locale }}</td>
                                                @if($language->status == 0)
                                                    <td>Inactive</td>
                                                @else
                                                    <td>Active</td>
                                                @endif

                                                <td>
                                                    @can('Edit Language')
                                                        <a href="#edit" class="btn btn-primary btn-md adv_cust_mod_btn edit-btn pull-left" data-toggle="modal" style="margin-right: 3px;" data-href="#edit"  data-id="{{ $language->id }}"
                                                           data-name="{{ $language->name }}"  data-native_name="{{ $language->native_name }}"  data-flag="{{ $language->flag }}"  data-locale="{{ $language->locale }}"
                                                           data-canonical_locale="{{ $language->canonical_locale }}"  data-full_locale="{{ $language->full_locale }}"  data-status="{{ $language->status }}" > Edit</a>

                                                    @endcan
                                                        @can('Delete Language')
                                                            <a href="{{route('destroylanguage', $language->id) }}" class="btn btn-danger pull-left" style="margin-right: 3px;">Delete</a>

                                                        @endcan
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!--- create model -->
    <div class="modal fade in display_none" id="create" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                {{ Form::open(array('url' => 'admin/languages')) }}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <h3><i class="fa fa-plus-circle circle"></i> Add Language</h3>
                            <hr>
                            <br>

                            <div class="form-group">
                                {{ Form::label('title', 'Language Title') }}
                                {{ Form::text('name', '', array('class' => 'form-control')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('title', 'Native Name') }}
                                {{ Form::text('native_name', '', array('class' => 'form-control')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('title', 'Flag') }}
                                {{ Form::text('flag', '', array('class' => 'form-control')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('title', 'Locale') }}
                                {{ Form::text('locale', '', array('class' => 'form-control')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('title', 'Canonical Locale') }}
                                {{ Form::text('canonical_locale', '', array('class' => 'form-control')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('title', 'Full Locale') }}
                                {{ Form::text('full_locale', '', array('class' => 'form-control')) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('language', 'Status') }}
                                {{ Form::select('status' , array('1' => 'Active', '0' => 'Inactive'), '1' ,  ['class' => 'form-control chzn-select']) }}
                            </div>


                        </div>

                    </div>
                </div>
                <div class="modal-footer" style="display: block !important;">
                    <div class="pull-right">
                        <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                        {{ Form::submit('Create Language', array('class' => 'btn btn-primary')) }}
                    </div>
                </div>
                {{ Form::close() }}
                <br>
            </div>
        </div>
    </div>
    <!-- END create modal-->

    <!--- Edit model -->
    <div class="modal fade in display_none" id="edit" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                {{ Form::open(array('route' => array('languages.update', 1), 'method' => 'PUT')) }}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <h3 id="edit-title"></h3>
                            <hr>
                            <br>
                            {{ Form::hidden('id', null, array('class' => 'form-control', 'id' => 'id_edit')) }}


                            <div class="form-group">
                                {{ Form::label('title', 'Language Title') }}
                                {{ Form::text('name', null, array('class' => 'form-control', 'id' => 'title_edit')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('title', 'Native Name') }}
                                {{ Form::text('native_name', null, array('class' => 'form-control', 'id' => 'native_name_edit')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('title', 'Flag') }}
                                {{ Form::text('flag', null, array('class' => 'form-control', 'id' => 'flag_edit')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('title', 'Locale') }}
                                {{ Form::text('locale', null, array('class' => 'form-control', 'id' => 'locale_edit')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('title', 'Canonical Locale') }}
                                {{ Form::text('canonical_locale', null, array('class' => 'form-control', 'id' => 'canonical_locale_edit')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('title', 'Full Locale') }}
                                {{ Form::text('full_locale', null, array('class' => 'form-control', 'id' => 'full_locale_edit')) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('language', 'Status') }}
                                {{ Form::select('status' , array('1' => 'Active', '0' => 'Inactive'), '1' ,  ['class' => 'form-control chzn-select', 'id' => 'status_edit']) }}
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer" style="display: block !important;padding-bottom: 20px;">
                    <div class="pull-right" style="padding-bottom: 20px !important;">
                        <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                        {{ Form::submit('Update Language', array('class' => 'btn btn-primary  ')) }}
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!-- END Edit model-->

@endsection
@section('script')
    <!--plugin scripts-->
    <script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <!-- end of plugin scripts -->
    <!--Page level scripts-->

    <script type="text/javascript" src="/backend/vendors/chosen/js/chosen.jquery.js"></script>
    <script type="text/javascript" src="/backend/js/pages/form_elements.js"></script>


    <script type="text/javascript" src="/backend/js/pages/datatable.js"></script>
    <!-- end of global scripts-->
    <script type="text/javascript">

        $('body').on('click','.edit-btn',function(){

            $('#edit-title').html(' <i class="fa fa-pencil circle"></i>  Edit '+ $(this).data('name'));
            $('#id_edit').val($(this).data('id'));
            $('#title_edit').val($(this).data('name'));
            $('#native_name_edit').val($(this).data('native_name'));
            $('#flag_edit').val($(this).data('flag'));
            $('#locale_edit').val($(this).data('locale'));
            $('#canonical_locale_edit').val($(this).data('canonical_locale'));
            $('#full_locale_edit').val($(this).data('full_locale'));
            $('#status_edit').val($(this).data('status'));
        });

    </script>
@endsection