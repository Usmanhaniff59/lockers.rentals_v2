@extends('backend.layouts.app')

@section('title', 'Lockers | Permissions')
@section('style')
        <!--plugin styles-->
<link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css" />
<!-- end of plugin styles -->
<!--Page level styles-->
<link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css" />
@endsection
@section('content')

    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-key circle"></i>
                            Available Permissions
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        @if(Session::has('flash_message'))
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable float-right" style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-hidden="true" style="margin-top: 4px; margin-left: 15px;"> ×
                    </button>
                    {{ Session::get('flash_message') }}
                </div>
            </div>
        @endif
        <div class="outer">
            <div class="inner bg-container">
                <div class="row">
                    <div class="col-12 data_tables">

                        <div class="card m-t-35">
                            <div class="card-header bg-white">
                                <i class="fa fa-key circle"></i> All Permissions
                                {{--<a href="{{ URL::to('admin/permissions/create') }}" class="btn btn-primary" role="button">Add Permission</a>--}}
                                <a href="#create" class="btn btn-primary btn-md pull-right adv_cust_mod_btn" data-toggle="modal" data-href="#create" >Add Permission</a>
                            </div>
                            <div class="card-body p-t-10">
                                <div class=" m-t-25">
                                    <table class="table table-striped table-bordered table-hover " id="sample_6">
                                        <thead>
                                        <tr>
                                            <th>Permissions</th>
                                            <th>Operation</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($permissions as $permission)
                                            <tr>
                                                <td>{{ $permission->name }}</td>
                                                <td>
                                                    {{--<a href="{{ URL::to('admin/permissions/'.$permission->id.'/edit') }}" class="btn btn-info pull-left" style="margin-right: 3px;">Edit</a>--}}
                                                    <a href="#edit" class="btn btn-primary btn-md adv_cust_mod_btn edit-btn pull-left" data-toggle="modal" style="margin-right: 3px;" data-href="#edit"data-id="{{ $permission->id }}" data-name="{{ $permission->name }}"  >Edit</a>

                                                    {!! Form::open(['method' => 'DELETE', 'route' => ['permissions.destroy', $permission->id] ]) !!}
                                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                                    {!! Form::close() !!}

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                </div>
            </div>
        </div>
    </div>

    <!--- create model -->
    <div class="modal fade in display_none" id="create" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                {{ Form::open(array('url' => 'admin/permissions')) }}
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <h3><i class="fa fa-plus-circle circle"></i> Create New Permission</h3>
                                <hr>

                                <div class="form-group">
                                    {{ Form::label('name', 'Name') }}
                                    {{ Form::text('name', '', array('class' => 'form-control')) }}
                                </div>
                                <br>

                                @if(!$roles->isEmpty())

                                    <h4>Assign Permission to Roles</h4>

                                    @foreach ($roles as $role)
                                        {{ Form::checkbox('roles[]',  $role->id ) }}
                                        {{ Form::label($role->name, ucfirst($role->name)) }}<br>

                                    @endforeach

                                @endif

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="display: flex;">
                    <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                    {{ Form::submit('Add', array('class' => 'btn btn-primary  ')) }}
                </div>
                {{ Form::close() }}
                <br>
            </div>
        </div>
    </div>
    <!-- END create modal-->

    <!--- Edit model -->
    <div class="modal fade in display_none" id="edit" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>

                {{ Form::open(array('route' => array('permissions.update', 1), 'method' => 'PUT')) }}
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <h3 id="edit-title"></h3>
                                <hr>

                                {{ Form::hidden('id', null, array('class' => 'form-control' , 'id' => 'id_edit')) }}

                                <div class="form-group">
                                    {{ Form::label('name', 'Permission Name') }}
                                    {{ Form::text('name', null, array('class' => 'form-control' , 'id' => 'name_edit')) }}
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="display: flex;">
                    <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                    {{ Form::submit('Add', array('class' => 'btn btn-primary  ')) }}
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
    <!-- END Edit model-->

@endsection
@section('script')
        <!--plugin scripts-->
    <script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <!-- end of plugin scripts -->
    <!--Page level scripts-->
    <script src="{{ asset('custom/general.js') }}"></script>

    <script type="text/javascript" src="/backend/js/pages/datatable.js"></script>
    <!-- end of global scripts-->
        <script type="text/javascript">

            $('body').on('click','.edit-btn',function(){

                $('#edit-title').html('<i class="fa fa-pencil circle"></i>  Edit '+ $(this).data('name'));
                $('#id_edit').val($(this).data('id'));
                $('#name_edit').val($(this).data('name'));


            });

        </script>
@endsection