{{ Form::model($role, array('route' => array('roles.update', $role->id), 'method' => 'PUT')) }}
    <div class="modal-body">
        <div class="row">
            <div class="col-12">
                <h3 class="m-t-5">
                    <i class="fa fa-pencil circle"></i>
                    Edit Role: {{$role->name}}
                </h3>
                <hr>
                <br>
                <div class="form-group">
                    {{ Form::label('name', 'Role Name') }}
                    {{ Form::text('name', null, array('class' => 'form-control')) }}
                </div>

                <h5><b>Assign Permissions</b></h5>

                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-lg-12 m-t-35">
                            <h5>All Permissions</h5>
                            <select class="form-control" multiple id="multi_select_t" name="permissions[]">
                                <option disabled selected>Selected Permissions</option>
                                @foreach ($permissions as $permission)
                                    <option value="{{$permission->id}}" @if(in_array($permission->id, $selected_role))selected="selected"@endif>{{ Form::label($permission->name, ucfirst($permission->name)) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer" style="display: flex;">
        <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
        {{ Form::submit('Edit', array('class' => 'btn btn-primary')) }}
    </div>
{{ Form::close() }}