@extends('backend.layouts.app')

@section('title', 'Lockers | Roles')
@section('style')
        <!--plugin styles-->
<link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css" />

<link type="text/css" rel="stylesheet" href="/backend/vendors/inputlimiter/css/jquery.inputlimiter.css"/>
<link type="text/css" rel="stylesheet" href="/backend/vendors/chosen/css/chosen.css"/>
<link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css"/>
<link type="text/css" rel="stylesheet" href="/backend/vendors/jquery-tagsinput/css/jquery.tagsinput.css"/>
<link type="text/css" rel="stylesheet" href="/backend/vendors/daterangepicker/css/daterangepicker.css"/>
<link type="text/css" rel="stylesheet" href="/backend/vendors/datepicker/css/bootstrap-datepicker.min.css"/>
<link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-switch/css/bootstrap-switch.min.css"/>
<link type="text/css" rel="stylesheet" href="/backend/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css"/>
<link type="text/css" rel="stylesheet" href="/backend/vendors/multiselect/css/multi-select.css"/>


<!-- end of plugin styles -->
<!--Page level styles-->
<link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/pages/form_elements.css"/>
@endsection
@section('content')

    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-key circle"></i>
                            Roles
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        <div class="outer">
            <div class="inner bg-container">
                <div class="row">
                    <div class="col-12 data_tables">

                        <div class="card m-t-35">
                            <div class="card-header bg-white">
                                <i class="fa fa-key circle"></i> All Roles
                                {{--<a href="{{ URL::to('admin/roles/create') }}" class="btn btn-primary" role="button">Add New Role</a>--}}
                                <a href="#create" class="btn btn-primary btn-md pull-right adv_cust_mod_btn" data-toggle="modal" data-href="#create" >Add New Role</a>
                                {{--<a href="#." class="btn btn-primary btn-md pull-right " id="copy-in-sales-audit-table" >Copy table</a>--}}
                            </div>
                            <div class="card-body p-t-10">
                                <div class=" m-t-25">
                                    <table class="table table-striped table-bordered table-hover " id="sample_6">
                                        <thead>
                                        <tr>
                                            <th>Role</th>
                                            <th>Permissions</th>
                                            <th style="width:100px!important;">Operation</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($roles as $role)
                                            <tr>

                                                <td>{{ $role->name }}</td>

                                                <td>{{  $role->permissions()->pluck('name')->implode(', ') }}</td>{{-- Retrieve array of permissions associated to a role and convert to string --}}
                                                <td>
                                                    <a href="#edit" class="btn btn-primary btn-md adv_cust_mod_btn edit-btn pull-left" data-toggle="modal" style="margin-right: 3px;" data-href="#edit" data-id="{{ $role->id }}" >Edit</a>

                                                    {!! Form::open(['method' => 'DELETE', 'route' => ['roles.destroy', $role->id] ]) !!}
                                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                                    {!! Form::close() !!}

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!--- create model -->
    <div class="modal fade in display_none" id="create" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                {{ Form::open(array('url' => 'admin/roles')) }}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <h3><i class="fa fa-plus-circle circle"></i> Add Role</h3>
                            <hr>
                            <br>
                            <div class="form-group">
                                {{ Form::label('name', 'Name') }}
                                {{ Form::text('name', null, array('class' => 'form-control')) }}
                            </div>

                            <h5><b>Assign Permissions</b></h5>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-lg-12 m-t-35">
                                        <h5>All Permissions</h5>
                                        <div class="form-group">
                                            <select class="form-control" multiple id="multi_select1" name="permissions[]">
                                                <option disabled selected>Selected Permissions</option>
                                                @foreach ($permissions as $permission)
                                                    <option value="{{$permission->id}}">{{ Form::label($permission->name, ucfirst($permission->name)) }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="display: flex;">
                    <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                    {{ Form::submit('Save changes', array('class' => 'btn btn-primary')) }}
                </div>
                {{ Form::close() }}
                <br>
            </div>
        </div>
    </div>
    <!-- END create modal-->

    <!--- Edit model -->
    <div class="modal fade in display_none" id="edit" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                  <div id="display_edit_form"></div>

            </div>
        </div>
    </div>
    <!-- END Edit model-->
@endsection
@section('script')
        <!--plugin scripts-->
    <script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>

    <script type="text/javascript" src="/backend/vendors/jquery.uniform/js/jquery.uniform.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputlimiter/js/jquery.inputlimiter.js"></script>
    <script type="text/javascript" src="/backend/vendors/chosen/js/chosen.jquery.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/jquery-tagsinput/js/jquery.tagsinput.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.validVal.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/moment/js/moment.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/daterangepicker/js/daterangepicker.js"></script>
    <script type="text/javascript" src="/backend/vendors/datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/autosize/js/jquery.autosize.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputmask/js/inputmask.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputmask/js/jquery.inputmask.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputmask/js/inputmask.date.extensions.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputmask/js/inputmask.extensions.js"></script>
    <script type="text/javascript" src="/backend/vendors/multiselect/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.quicksearch/2.3.1/jquery.quicksearch.min.js"></script>



    <!--end of plugin scripts-->

    <!--Page level scripts-->
    <script type="text/javascript" src="/backend/js/pages/datatable.js"></script>
    <script type="text/javascript" src="/backend/js/form.js"></script>
    <script type="text/javascript" src="/backend/js/pages/form_elements.js"></script>
    <script src="{{ asset('custom/general.js') }}"></script>

    <!-- end of global scripts-->
        <script type="text/javascript">

            $('body').on('click','.edit-btn',function(e){

                var id = $(this).data('id');
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                $.ajax({
                    url: '/admin/getroles',
                    type: 'POST',
                    dataType: 'JSON',
                    cache: false,
                    data: {id: id, _token: CSRF_TOKEN},
                    success: function (data) {
                        console.log(data);
                        $('#display_edit_form').html(data);
                        $('#multi_select_t').multiSelect();

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR);
                    }
                });

            });
            //
            // $('body').on('click','#copy-in-sales-audit-table',function(e){
            //
            //     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            //
            //     $.ajax({
            //         url: '/admin/get_sales_count',
            //         type: 'POST',
            //         dataType: 'JSON',
            //         cache: false,
            //         data: { _token: CSRF_TOKEN},
            //         success: function (data) {
            //             console.log(data);
            //             var number_of_requests =  Math.ceil(data/1000);
            //             var page = 0;
            //
            //             sessionStorage.setItem("number_of_requests", number_of_requests);
            //             sessionStorage.setItem("page", page);
            //             insertion_in_sales_audit();
            //             // $('#display_edit_form').html(data);
            //             // $('#multi_select_t').multiSelect();
            //
            //         },
            //         error: function (jqXHR, textStatus, errorThrown) {
            //             console.log(jqXHR);
            //         }
            //     });
            //
            // });
            //
            // function insertion_in_sales_audit() {
            //
            //     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            //     var page =  parseInt(sessionStorage.getItem("page"));
            //
            //     $.ajax({
            //         url: '/admin/add_in_sales_autdit',
            //         type: 'POST',
            //         dataType: 'JSON',
            //         cache: false,
            //         data: { page: page, _token: CSRF_TOKEN},
            //         success: function (data) {
            //             console.log(data);
            //             var number_of_requests =  parseInt(sessionStorage.getItem("number_of_requests"));
            //
            //             if (page == number_of_requests - 1) {
            //               alert('done');
            //
            //             }else{
            //                 sessionStorage.setItem("page",  page+1);
            //                 insertion_in_sales_audit()
            //             }
            //
            //         },
            //         error: function (jqXHR, textStatus, errorThrown) {
            //             console.log(jqXHR);
            //         }
            //     });
            //
            //
            // }
        </script>
@endsection