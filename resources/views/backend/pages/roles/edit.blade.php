@extends('backend.layouts.app')

@section('title', 'Lockers | Edit Role')
@section('style')
    <link type="text/css" rel="stylesheet" href="/backend/vendors/inputlimiter/css/jquery.inputlimiter.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/chosen/css/chosen.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/jquery-tagsinput/css/jquery.tagsinput.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/daterangepicker/css/daterangepicker.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datepicker/css/bootstrap-datepicker.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-switch/css/bootstrap-switch.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/multiselect/css/multi-select.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/form_elements.css"/>
@endsection
@section('content')

    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-key"></i>
                            Edit Role: {{$role->name}}
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        <div class="outer">
            <div class="inner bg-container">


                <!--top section widgets-->
                <div class="row widget_countup">
                    <div class="col-12">
    <hr>
    {{-- @include ('errors.list')
 --}}
    {{ Form::model($role, array('route' => array('roles.update', $role->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('name', 'Role Name') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
    </div>

    <h5><b>Assign Permissions</b></h5>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-lg-6 m-t-35">
                                    <h5>All Permissions</h5>
                                    <select class="form-control" multiple id="multi_select1" name="permissions[]">
                                        <option disabled selected>Selected Permissions</option>
                                        @foreach ($permissions as $permission)
                                            <option value="{{$permission->id}}" @if(in_array($permission->id, $selected_role))selected="selected"@endif>{{ Form::label($permission->name, ucfirst($permission->name)) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>



    {{ Form::submit('Edit', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
        <!-- plugin level scripts -->
    <script type="text/javascript" src="/backend/vendors/jquery.uniform/js/jquery.uniform.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputlimiter/js/jquery.inputlimiter.js"></script>
    <script type="text/javascript" src="/backend/vendors/chosen/js/chosen.jquery.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/jquery-tagsinput/js/jquery.tagsinput.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.validVal.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/moment/js/moment.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/daterangepicker/js/daterangepicker.js"></script>
    <script type="text/javascript" src="/backend/vendors/datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/autosize/js/jquery.autosize.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputmask/js/inputmask.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputmask/js/jquery.inputmask.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputmask/js/inputmask.date.extensions.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputmask/js/inputmask.extensions.js"></script>
    <script type="text/javascript" src="/backend/vendors/multiselect/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.quicksearch/2.3.1/jquery.quicksearch.min.js"></script>
    <!--end of plugin scripts-->
    <script type="text/javascript" src="/backend/js/form.js"></script>
    <script type="text/javascript" src="/backend/js/pages/form_elements.js"></script>
@endsection