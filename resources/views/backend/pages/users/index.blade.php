@extends('backend.layouts.app')

@section('title', 'Lockers | Users')
@section('style')
    <!--plugin styles-->
    <link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css"/>

    <link type="text/css" rel="stylesheet" href="/backend/vendors/inputlimiter/css/jquery.inputlimiter.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/chosen/css/chosen.css"/>
    <link type="text/css" rel="stylesheet"
          href="/backend/vendors/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-switch/css/bootstrap-switch.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/multiselect/css/multi-select.css"/>

    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-switch/css/bootstrap-switch.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/switchery/css/switchery.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/radio_css/css/radiobox.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/checkbox_css/css/checkbox.min.css"/>
    <!--End of Plugin styles-->
    <!--Page level styles-->

    <!-- end of plugin styles -->
    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/form_elements.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/radio_checkbox.css"/>

    <link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css"/>

    <style>

        .page-link {
            padding: 16px;
            color: #525252;
            text-decoration: none;
            background-color: transparent;
        }

        .header_icon_margin {
            margin-right: 10px !important;
        }

        .header_icon_margin_left {
            margin-left: 10px !important;
        }

    </style>
@endsection
@section('content')
    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fas fa-user circle header_icon_margin"></i>
                            User Administration
                        </h4>
                    </div>
                </div>
            </div>
        </header>

        @include('errors.success')
        @include('errors.error')

        @if(Session::has('flash_message'))
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable float-right"
                     style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-hidden="true" style="margin-top: 4px; margin-left: 15px;"> ×
                    </button>
                    {{ Session::get('flash_message') }}
                </div>
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="outer">
            <div class="inner bg-container">
                <div class="row">
                    <div class="col-12 data_tables">

                        <div class="card m-t-35">
                            <div class="card-header bg-white">
                                <i class="fas fa-user circle header_icon_margin"></i> All Users
                                @can('Create User')
                                    <a href="#user-create-modal"
                                       class="btn btn-primary pull-right btn-md  header_icon_margin_left add-new-user-btn"
                                       data-toggle="modal" data-href="#user-create-modal">Add New User</a>
                                @endcan
                                <div class="radio_basic_swithes_padbott pull-right header_icon_margin_left"
                                     style="margin-top: 7px;padding-bottom:0px !important;">
                                    <input type="checkbox" class="js-switch sm_toggle_show_only_deleted_company"
                                           name="roles" value="1" id="get_deleted_users" />
                                    <span class="radio_switchery_padding">Recycle bin</span>
                                </div>


                            </div>
                            <div class="card-body p-t-10">
                                <div class=" m-t-25">

                                    <table class="table  table-bordered table-hover " id="usersTable">
                                        <thead>
                                        <tr>
                                            {{--<th>#</th>--}}
                                            <th>Name</th>
                                            <th>Email</th>
                                            {{--<th>Date/Time Added</th>--}}
                                            <th>User Roles</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                        </thead>

                                    </table>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{--</div>--}}
            </div>
        </div>


    </div>
    <!--- create model -->
    @include('backend.pages.users.create')
    <!-- END create modal-->

    <!--- Edit model -->
    @include('backend.pages.users.edit')
    <!-- END Edit model-->
    </div>
@endsection
@section('script')
    <!--plugin scripts-->
    <script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <!-- end of plugin scripts -->

    <script type="text/javascript" src="/backend/vendors/jquery.uniform/js/jquery.uniform.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputlimiter/js/jquery.inputlimiter.js"></script>
    <script type="text/javascript" src="/backend/vendors/chosen/js/chosen.jquery.js"></script>
    <script type="text/javascript"
            src="/backend/vendors/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/jquery-tagsinput/js/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.validVal.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/moment/js/moment.min.js"></script>

    <script type="text/javascript" src="/backend/vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/autosize/js/jquery.autosize.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputmask/js/inputmask.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputmask/js/jquery.inputmask.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputmask/js/inputmask.extensions.js"></script>
    <script type="text/javascript" src="/backend/vendors/multiselect/js/jquery.multi-select.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery.quicksearch/2.3.1/jquery.quicksearch.min.js"></script>

    <script type="text/javascript" src="/backend/vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>
    <!--End of plugin scripts-->
    <!--Page level scripts-->

    <!--end of plugin scripts-->
    <!--Page level scripts-->
    <script type="text/javascript" src="/backend/js/pages/radio_checkbox.js"></script>
    <script type="text/javascript" src="/backend/js/form.js"></script>
    <script type="text/javascript" src="/backend/js/pages/form_elements.js"></script>
    <script type="text/javascript" src="/backend/js/pages/datatable_server_side.js"></script>
    <script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>
    <script src="{{ asset('custom/general.js') }}"></script>


    {{--<script type="text/javascript" src="/backend/js/pages/radio_checkbox.js"></script>--}}

    {{--<script type="text/javascript" src="/backend/js/common.js"></script>--}}
    <!-- end of global scripts-->

    <script type="text/javascript">

        //----------------------------Ajax Form Submission-------------------------//
        function saveData(form_id, btn_id, modal_id, table_id, callback) {

            // $('#'+ form_id).addClass('was-validated');

            if (btn_id !== '') {
                var btn = $('#' + btn_id);
                var btn_txt = btn.html();
                btn.html("<i class='fa fa-spinner'></i> Processing...");
            }

            $.ajax({

                type: $('#' + form_id).attr('method'),
                url: $('#' + form_id).attr('action'),
                data: $('#' + form_id).serialize(),
                dataType: 'json',
                success: function (data) {
                    //console.log(data);
                    $('#' + form_id).trigger("reset");
                    if (modal_id !== '') {
                        $('#' + modal_id).modal('toggle');
                    }
                    if (table_id !== '') {

                        table_id.draw('page');
                    }
                    if (btn_id !== '') {
                        btn.html(btn_txt);
                    }

                },
                error: function (xhr, detail) {
                    if (btn_id !== '')
                        btn.html(btn_txt);

                    if (xhr.responseJSON.errors.email) {

                        $('#email_error').show()
                        $('#email').focus();
                        $('#email_error').html('<span>' + xhr.responseJSON.errors.email[0] + '</span>');
                    }
                    else {
                        $('#email_error').hide();
                    }
                    if (xhr.responseJSON.errors.name) {

                        $('#name_error').show()
                        $('#name').focus();
                        $('#name_error').html('<span>' + xhr.responseJSON.errors.name[0] + '</span>');
                    }
                    else {
                        $('#name_error').hide();
                    }

                    if (xhr.responseJSON.errors.response_group_id) {

                        $('#response_group_id_error').show()
                        $('#response_group_id_error').html('<span>' + xhr.responseJSON.errors.response_group_id[0] + '</span>');
                    }
                    else {
                        $('#response_group_id_error').hide();
                    }

                     if (xhr.responseJSON.errors.roles) {

                        $('#roles_error').show()
                        $('#roles_error').html('<span>' + xhr.responseJSON.errors.roles[0] + '</span>');
                    }
                    else {
                        $('#roles_error').hide();
                    }

                }
            })
        }

        function resetFormWithOutConfirm(form_id) {

            if ($('#' + form_id).trigger("reset")) {
                $("form#" + form_id + " :input, select, textarea").each(function () {

                    if ($(this).attr('name') == '_token') {

                    } else {
                        $(this).val(null).trigger('change');
                    }
                });

            }

        }
        function searchByThisBooked(table){
            table.draw('page');

        }

        function deleteData(url,token,modal_id,table_id,method = null) {

            type = "POST";
            if(method){
                type = method;
            }

            if (confirm("Are you sure to Delete?")) {

                $.ajax({

                    type: type,
                    url: url,
                    data: {
                        '_token': token
                    },
                    dataType: 'json',
                    success: function (data) {

                        if (table_id !== '') {
                            table_id.draw('page');
                        }

                        if(data.error){
                            errorMessage(data.error);
                        }else{
                            successMessage(data.success);
                        }
                    },
                    error: function (data) {

                    }
                })
            }

        }

        $(document).ready(function () {

            $('body').on('change','#get_deleted_users', function() {
                var checkbox_status = $("#get_deleted_users").prop("checked");

                if (checkbox_status) {
                    searchByThisBooked(user_table)
                } else {
                    searchByThisBooked(user_table)
                }
            });


            if (window.location.href.indexOf("#edit") > -1) {

                var url = window.location.href;
                var x = url.split('?id=')[1];
                var user_id = x;
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                
                $.ajax({
                    url: '/admin/getuserroles',
                    type: 'POST',
                    dataType: 'JSON',
                    cache: false,
                    data: {user_id: user_id, _token: CSRF_TOKEN},
                    success: function (data) {
                        $('#editform_display').html(data);
                        $('#edit').modal('toggle');
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR);
                    }
                });
            }
        });

        function reset_form(form_id) {

            $("#user-save")[0].reset();

        }


        var user_table = userEntryTable();

        function userEntryTable() {

            return $('#usersTable').DataTable({
                processing: true,
                serverSide: true,
                searching: true,
                stateSave: true,
                "oLanguage": {
                    "sLengthMenu": "_MENU_",
                    "sSearch": "_INPUT_"
                },
                "aTargets": ["_all"],

                "order": [],
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                columnDefs: [
                    {   "targets": [0],
                        "searchable": false
                    }
                ],
                 ajax: {
                    url: '<?php echo e(route('users.show.all')); ?>',
                    data: function (d) {
                        d.deleted = $("#get_deleted_users").prop("checked");

                    }
                },
                columns: [
                    {data: 'full_name', name: 'full_name'},
                    {data: 'email', name: 'email'},
                    {data: 'roles', name: 'roles'},
                    {data: 'action', name: 'action', orderable: false, searchable: true}
                ]
            });
        }

        $('body').on('click', '.edit-btn', function (e) {

            var user_id = $(this).data('id');
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                url: '/admin/getuserroles',
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {user_id: user_id, _token: CSRF_TOKEN},
                success: function (data) {
                    $('#editform_display').html(data);
                    setTimeout(
                        function()
                        {
                            $('.password').val('');
                        }, 1000);


                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                }
            });

        });

        $('body').on('click', '.create-new-franchisee-group-button', function (e) {

            $('.new-franchisee-group-input').removeClass('d-none');
            $('.existed-franchisee-group').addClass('d-none');

            $('.existed-franchisee-group-button-field').removeClass('d-none');
            $('.new-franchisee-group-button-field').addClass('d-none');

        });

        $('body').on('click', '.existed-franchisee-group-button , .add-new-user-btn', function (e) {

            $('.existed-franchisee-group').removeClass('d-none');
            $('.new-franchisee-group-input').addClass('d-none');

            $('.new-franchisee-group-button-field').removeClass('d-none');
            $('.existed-franchisee-group-button-field').addClass('d-none');
            $('.franchisee_name').val('');
            $('.password_create').val('');
            $('.validation-error').hide();


        });

        var role_access = "{{auth()->user()->can('Edit User')}}";

        if (role_access.length > 0) {
            var roles = "{{ count($roles) }}";

            for (var i = 0; i < roles; i++) {
                new Switchery(document.querySelector('.sm_toggle_add' + i), {
                    size: 'small',
                    color: '#00c0ef',
                    jackColor: '#fff'
                });

            }
        }

    </script>
@endsection