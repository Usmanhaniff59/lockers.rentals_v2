<-- create model --> --> 
<div class="modal fade in display_none" id="user-create-modal" tabindex="-1" role="dialog" aria-hidden="false">

    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            {{ Form::open(array('route' => 'users.store', 'id'  => "user-save")) }}
            <div class="modal-body">
                @include('errors.js-validation')
                <div class="row">
                    <div class="col-12">
                        <h3><i class="fa fa-plus-circle circle"></i> Create New User</h3>
                        <hr>

                        <div class="form-group">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name', '', array('class' => 'form-control')) }}
                            <div id="name_error" class=" user_error validation-error "></div>

                        </div>

                        <div class="form-group">
                            {{ Form::label('email', 'Email') }}
                            {{ Form::email('email', '', array('class' => 'form-control')) }}
                            <div id="email_error" class=" user_error validation-error "></div>

                        </div>
                        @can('Ticketing Permission')
                        <div class="row">
                            <div class="col-12">
                                <h5><b>Ticketing Response Group</b></h5>
                            </div>
                            <div class="form-group col-12">
                                <label class="radio-inline">
                                    <input type="radio" class="mr-2" name="response_group_id" value="2">Client
                                </label>
                                <label class="radio-inline pl-3">
                                    <input type="radio" class="mr-2" name="response_group_id" checked value="1"> Customer
                                </label>
                                <label class="radio-inline  pl-3">
                                    <input type="radio" class="mr-2" name="response_group_id" value="3">Developer
                                </label>
                                <label class="radio-inline  pl-3">
                                    <input type="radio" class="mr-2" name="response_group_id" value="4">Developer Manager
                                </label>
                                <div id="response_group_id_error" class=" user_error validation-error "></div>

                            </div>

                        </div>
                        @endcan


                        @can('Assign Role To User')
                            <div class='form-group'>
                                <select name="roles" id="roles" class="form-control">
                                    <option value="">Select Role</option>
                                @foreach ($roles as $index=>$role)
                                <option value="{{$role->id}}">{{ ucfirst($role->name) }}</option>
                                   <!--  <div class="form-group">
                                        <div class="radio_basic_swithes_padbott">
                                            <input type="checkbox" class="js-switch sm_toggle_add{{$index}}" name="roles[]" value="{{$role->id}}" />
                                            <span class="radio_switchery_padding">{{ ucfirst($role->name) }}</span>
                                            <br />
                                        </div>
                                    </div> -->
                                @endforeach
                                </select>
                                <div id="roles_error" class=" user_error validation-error "></div>

                            </div>
                        @endcan
                      <div class="row">
                        @can('Assign Company To User')
                            <div class='form-group col-md-6'>
                                <label for="company_id">Company</label>
                                <select class="form-control chzn-select" name="company_id"  id="company_id_user_cr" tabindex="2">
                                    <option value="" selected>Choose Company</option>
                                    @foreach ($companies as $company)
                                        <option value="{{$company->id}}" >{{$company->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endcan
                          @can('Assign Franchisee To User')
                          <div class="col-md-6" id="franchisees-companyId">
                            <div class="form-group  existed-franchisee-group">
                                <label for="franchisee_id">Franchisee</label>
                                <select size="3" multiple class="form-control chzn-select" id="franchisee_id"
                                        name="franchisee_groups[]"  tabindex="8">
                                    @foreach ($franchisees as $franchisee)
                                        <option value="{{$franchisee->id}}" >{{$franchisee->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group new-franchisee-group-input d-none">
                                {{ Form::label('franchisee_name', 'Franchisee Group Name') }}
                                {{ Form::text('franchisee_name', '', array('class' => 'form-control franchisee_name')) }}
                            </div>
                            <h6 class='text-center'>OR</h6>

                            <div class='form-group text-center new-franchisee-group-button-field'>
                                <button type="button"  class="btn btn-primary create-new-franchisee-group-button">Create New Franchisee Group</button>
                            </div>

                            <div class='form-group text-center existed-franchisee-group-button-field d-none'>
                                <button type="button"  class="btn btn-success existed-franchisee-group-button" id="">Choose Existing Franchisee Group</button>
                            </div>
                            </div>
                        @endcan
                    </div>
                        <div class="form-group">
                            {{ Form::label('password', 'Password') }}<br>
                            {{ Form::password('password', array('class' => 'form-control password_create')) }}

                        </div>

                        <div class="form-group">
                            {{ Form::label('password', 'Confirm Password') }}<br>
                            {{ Form::password('password_confirmation', array('class' => 'form-control password_create')) }}

                        </div>
                        @error('password_confirmation')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror

                    </div>
                </div>
            </div>
            <div class="modal-footer" style="display: flex;">
                <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                <button type="button"  id="user-save-btn"  onclick="saveData('user-save','user-save-btn','user-create-modal',user_table)"  class="btn btn-primary">Save</button>

            </div>
            {{ Form::close() }}
            <br>
        </div>
    </div>
</div>
<!-- END create modal -->
