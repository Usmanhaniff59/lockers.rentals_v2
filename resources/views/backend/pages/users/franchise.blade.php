
                    

                    



                      
                        @can('Assign Franchisee To User')

                            <div class="form-group  existed-franchisee-group">
                                <label for="franchisee_id">Franchisee</label>
                                <select size="3" multiple class="form-control chzn-select" id="franchisee_id"
                                        name="franchisee_groups[]"  tabindex="8">
                                    @foreach ($franchisees as $franchisee)
                                    @if(isset($franchisee->franchisee_companies) && !empty($franchisee->franchisee_companies) && count($franchisee->franchisee_companies)>0)
                                        <option value="{{$franchisee->id}}" >{{$franchisee->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group new-franchisee-group-input d-none">
                                {{ Form::label('franchisee_name', 'Franchisee Group Name') }}
                                {{ Form::text('franchisee_name', '', array('class' => 'form-control franchisee_name')) }}
                            </div>
                            <h6 class='text-center'>OR</h6>

                            <div class='form-group text-center new-franchisee-group-button-field'>
                                <button type="button"  class="btn btn-primary create-new-franchisee-group-button">Create New Franchisee Group</button>
                            </div>

                            <div class='form-group text-center existed-franchisee-group-button-field d-none'>
                                <button type="button"  class="btn btn-success existed-franchisee-group-button" id="">Choose Existing Franchisee Group</button>
                            </div>

                        @endcan
              

                   
                       


            <script type="text/javascript">

     $(".hide_search").chosen({disable_search_threshold: 10});

    $(".chzn-select").chosen({allow_single_deselect: true});

         $(".chzn-select-deselect,#select2_sample").chosen();

        $(".chzn-select").chosen({
            allow_single_deselect: true,
            search_contains: true, // kwd can be anywhere
            max_selected_options: 1,
            width: "95%",
            no_results_text: "Oops, nothing found!"
        });

        function Chosen() {
            return Chosen.__super__.constructor.apply(this, arguments);
    }


        //make toggle objects against roles
        var role_access = "{{auth()->user()->can('Edit User')}}";


</script>
