@extends('backend.layouts.app')

@section('title', 'Lockers | Edit User')
@section('style')
<!--plugin styles-->
<link type="text/css" rel="stylesheet" href="/backend/vendors/switchery/css/switchery.min.css" />

@endsection
@section('content')

    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-user-plus"></i>
                            Edit {{$user->name}}
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        <div class="outer">
            <div class="inner bg-container">


                <!--top section widgets-->
                <div class="row widget_countup">
                    <div class="col-12">

    <hr>
                        @if(Session::has('flash_message'))
                            <div class="col-md-12">
                                <div class="alert alert-success alert-dismissable float-right" style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-hidden="true" style="margin-top: 4px; margin-left: 15px;"> ×
                                    </button>
                                    {{ Session::get('flash_message') }}
                                </div>
                            </div>
                        @endif

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
    {{-- @include ('errors.list') --}}

    {{ Form::model($user, array('route' => array('saveprofile', $user->id), 'method' => 'POST')) }} {{-- Form model binding to automatically populate our fields with user data --}}

    <div class="form-group">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('email', 'Email') }}
        {{ Form::email('email', null, array('class' => 'form-control')) }}
    </div>
    @can('Assign Role To User')
        <h5><b>Give Role</b></h5>
        @foreach ($roles as $index=>$role)
            <div class='form-group'>
                <div class="radio_basic_swithes_padbott">
                    {{ Form::checkbox('roles[]',  $role->id, $user->roles,array('class' => 'js-switch sm_toggle_editprofile'.$index.'' )) }}
                    {{ Form::label($role->name, ucfirst($role->name),['class' => 'radio_switchery_padding']) }}<br>

                </div>
            </div>

        @endforeach
    @endcan

    <div class="form-group">
        {{ Form::label('Phone Number', 'Phone Number') }}<br>
        {{ Form::text('phone_number', null, array('class' => 'form-control')) }}

    </div>

    <div class="form-group">
        {{ Form::label('password', 'Password') }}<br>
        {{ Form::password('password', array('class' => 'form-control')) }}

    </div>



    <div class="form-group">
        {{ Form::label('password', 'Confirm Password') }}<br>
        {{ Form::password('password_confirmation', array('class' => 'form-control')) }}

    </div>

    {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
<!--plugin scripts-->
<script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>



<script>
    var role_access = "{{auth()->user()->can('Assign Role To User')}}";

    if(role_access.length >0){
        var roles ="{{ count($roles) }}";

        for(var i =0 ; i< roles ; i++){
            new Switchery(document.querySelector('.sm_toggle_editprofile'+i), { size: 'small', color: '#00c0ef', jackColor: '#fff' });

        }
    }
</script>
@endsection