@extends('backend.layouts.app')

@section('title', 'Lockers | Transactions')
@section('style')
    <!--plugin styles-->
    <link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-switch/css/bootstrap-switch.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/switchery/css/switchery.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/radio_css/css/radiobox.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/checkbox_css/css/checkbox.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/modal/css/component.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-tagsinput/css/bootstrap-tagsinput.css"/>
    <link rel="stylesheet" type="text/css" href="/backend/vendors/animate/css/animate.min.css" />
    <!-- end of plugin styles -->

    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/radio_checkbox.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css" />

    <style>
        .edit-card-header{
            border-bottom: 1px solid #00C0EF;
        }
        .table thead th{
            vertical-align: top;
        }
        table.datatable thead th.no-sort {
            background: none;
            pointer-events: none;
        }
        #refund_table_wrapper>.row{
            width: 100%;
        }
        #refund_table_length{
            float:left !important;
        }
        #refund_table_filter{
            float:right !important;
        }
            .ui-datepicker {
            width: 300px;
            height: 300px;
            margin: 5px auto 0;
            font: 12pt Arial, sans-serif;
            /*-webkit-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
            -moz-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);*/
        }

            .ui-datepicker table {
                width: 100%;
            }

        .ui-datepicker-header {
            background: #3399ff;
            color: #ffffff;
            font-family:'Times New Roman';
            border-width: 1px 0 0 0;
            border-style: solid;
            border-color: #111;
        }

        .ui-datepicker-title {
            text-align: center;
            font-size: 15px;

        }

        .ui-datepicker-prev {
            float: left;
            cursor: pointer;
            background-position: center -30px;
        }

        .ui-datepicker-next {
            float: right;
            cursor: pointer;
            background-position: center 0px;
        }

        .ui-datepicker thead {
            background-color: #f7f7f7;

            /*border-bottom: 1px solid #bbb;*/
        }

        .ui-datepicker th {
            background-color:#808080;
            text-transform: uppercase;
            font-size: 8pt;
            color: #666666;
            /*text-shadow: 1px 0px 0px #fff;*/
            /*filter: dropshadow(color=#fff, offx=1, offy=0);*/
        }

        .ui-datepicker tbody td {
            padding: 0;
            /*border-right: 1px solid #808080;*/
        }

            .ui-datepicker tbody td:last-child {
                border-right: 0px;
            }

        .ui-datepicker tbody tr {
            border-bottom: 1px solid #bbb;
        }

            .ui-datepicker tbody tr:last-child {
                border-bottom: 0px;
            }

        .ui-datepicker a {
            text-decoration: none;
        }

        .ui-datepicker td span, .ui-datepicker td a {
            display: inline-block;
            /*font-weight: bold;*/
            text-align: center;
            width: 30px;
            height: 30px;
            line-height: 30px;
            color: #ffffff;
            /*text-shadow: 1px 1px 0px #fff;*/
            /*filter: dropshadow(color=#fff, offx=1, offy=1);*/
        }

        .ui-datepicker-calendar .ui-state-default {
              background: linear-gradient(#999999, #737373);
              color:#ffffff;
              height:40px;
              width:40px;

        }

        .ui-datepicker-calendar .ui-state-hover {
            background: #33adff;
            color: #FFFFFF;
        }

        .ui-datepicker-calendar .ui-state-active {
            background: #33adff;
            -webkit-box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1);
            -moz-box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1);
            box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1);
            color: #e0e0e0;
            text-shadow: 0px 1px 0px #4d7a85;
            border: 1px solid #55838f;
            position: relative;
            margin: -1px;
        }

        .ui-datepicker-unselectable .ui-state-default {
            background: #D6E4BE;
            color: #000;
        }
    </style>

@endsection
@section('content')
    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-money circle"></i>
                            Transactions Management
                        </h4>
                    </div>
                </div>
            </div>
        </header>


        @if(isset($data))
         <div class="col-md-12" >
                <div class="alert alert-danger alert-dismissable float-right" style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-hidden="true" style="margin-top: 2px; margin-left: 15px;"> ×
                    </button>
                    Payment Amount: {{$payment->amount}}
                    <br>
                     Transaction Amount: {{$data->first()->account_balance}}
                </div>
            </div>
        @endif


    <!--- Price Change model -->
    <div class="modal fade in display_none" id="edit" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                {{ Form::open(array('url' => 'admin/payment/changePrice', 'id' => 'changepriceform')) }}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <h3><i class="fa fa-money circle"></i> Price Change</h3>
                            <hr>
                            <br>
                            <div class='form-group col-md-6 type'>
                                <label for="type">Type</label>
                                <select class="form-control" id="type" name="type">
                                    <option value="" selected>Choose Transaction Type</option>
                                    <option value="c">Charity</option>
                                    <option value="l">Locker</option>
                                </select>
                            </div>
                             <div class="alert alert-danger  fade show d-none" role="alert" id="errormsg_avaempty" style="width: 30%, margin-left: 16px">
                             Please select transaction type to proceed
                            </div>
                            <div class="form-group col-md-6">

                                <input type="hidden" value="<?php echo $transactions->first()->booking_id; ?>" name="booking_id" class="form-control" required="">
                                
                            </div>
                            <div class="form-group col-md-6">
                                <label for = "tamount" >Available Amount</label>
                                <input type="number" min="1" name="tamount" id="tamount" class="form-control" required="" disabled="">
                            </div>

                            <div class="form-group col-md-6">
                                <label for = "amount" > Amount</label>
                                <input type="number" min="1" name="amount" id="amount" class="form-control" min="0"  required>
                            </div>
                           <div class="alert alert-danger  fade show d-none" role="alert" id="errormsg" style="width: 30%, margin-left: 16px">
                              Number should not be greater than the total amount!
                            </div>
                             <div class="alert alert-danger  fade show d-none" role="alert" id="errormsg_numsmall" style="width: 30%, margin-left: 16px">
                              Number should not be smaller than 0!
                            </div>
                            
                            <div class="form-group col-md-6">
                                <label for = "reason" >Reason</label>
                                <textarea name="reason" class="form-control" required="true"></textarea> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                    {{ Form::Button('Save', array('class' => 'btn btn-primary','id'=>'validate_change_price')) }}
                </div>
                {{ Form::close() }}
                <br>
            </div>
        </div>
    </div>
    <!-- END Price Change modal-->


    <!--- Refund model -->
    <div class="modal fade in display_none" id="refund" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                {{ Form::open(array('url' => 'admin/refundPayment','id'=>'refund_form')) }}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <h3><i class="fa fa-money circle"></i> Refund Amount</h3>
                            <hr>
                            <br>
                            <input type="hidden" name="total_outstanding" id="adjustment" value="{{$transactions->first()->refund_balance}}">
                            <input type="hidden" name="booking_id" id="payment_id" value="{{$transactions->first()->booking_id}}">
                            
                             <div class="form-group col-md-6">
                                <label for = "tamount" >Available Amount</label>
                                <input type="number" min="1" name="tamount" id="tamount" class="form-control" required="" disabled="" value="{{$transactions->first()->refund_balance}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for = "amount" > Amount To Refund</label>
                                <input type="text" min="1" name="refund_amount" id="refund_amount" class="form-control" min="0"  required>
                            </div>
                             <div class="alert alert-danger  fade show d-none" role="alert" id="errormsg_refund" style="width: 30%, margin-left: 16px">
                              Number should not be greater than the total amount!
                            </div>
                             <div class="alert alert-danger  fade show d-none" role="alert" id="errormsg_numsmall_refund" style="width: 30%, margin-left: 16px">
                              Number should not be smaller than 0!
                            </div>

                            <div class="form-group col-md-6" >
                                {{ Form::label('notes', 'Notes') }}
                                {{ Form::textarea('notes', '', array('class' => 'form-control ', 'style' => 'height: 100px', 'id'=>"notes")) }}
                              
                            </div>
                            <div class="alert alert-danger  fade show d-none" role="alert" id="errormsg_reason_refund" style="width: 30%, margin-left: 16px, margin : 12px">
                              Notes should not be empty!
                            </div>

                            <div class="form-group col-md-6" >
                                {{ Form::label('message', 'Message') }}
                                {{ Form::textarea('message', '', array('class' => 'form-control ', 'style' => 'height: 100px', 'id'=>"message")) }}
                              
                            </div>

                            <div class="form-group col-md-6">
                                <input type="checkbox" name="sendEmail" id="sendEmail" checked="true">
                                <label for="sendEmail" style="margin-left: 12px">Send customer an email</label>
                            </div>

                             <div class="form-group col-md-6">
                                <input type="checkbox" name="updatepayment" id="updatepayment" checked="true">
                                <label for="updatepayment" style="margin-left: 12px">Update account history</label>
                            </div>
                            
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                    {{ Form::Button('Refund', array('class' => 'btn btn-primary','id'=>'check_price')) }}
                </div>
                {{ Form::close() }}
                <br>
            </div>
        </div>
    </div>
    <!-- END Refund modal-->


        @if(Session::has('flash_message'))
            <div class="col-md-12" id="flash_message">
                <div class="alert alert-success alert-dismissable float-right" style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-hidden="true" style="margin-top: 2px; margin-left: 15px;"> ×
                    </button>
                    {{ Session::get('flash_message') }}
                </div>
            </div>
        @endif


       @if(Session::has('error_message'))
            <div class="col-md-12" id="error_message">
                <div class="alert alert-danger alert-dismissable float-right" style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-hidden="true" style="margin-top: 2px; margin-left: 15px;"> ×
                    </button>
                    {{ Session::get('error_message') }}
                </div>
            </div>
        @endif


        <div class="outer">
            <div class="inner bg-container">
                <div class="row p-t-15">
                     {{ Form::open(array('url' => 'admin/payments/filter', 'id' => 'filterform', 'class' => 'form-inline')) }}
                        <input type="text" id="start_date" name="start_date" class="form-control" style="margin-left: 12px" placeholder="Start Date" >
                        <input type="text" id="end_date" name="end_date" class="form-control" style="margin-left: 12px" placeholder="End Date" >
                         <input type="hidden" id="booking_id" name="booking_id" class="form-control" style="margin-left: 12px" value="{{$transactions->first()->booking_id}}">
                        <input type="submit" id="filter" value="Filter" style="margin-left: 12px" class="btn btn-success">
                    {{ Form::close() }}
                </div>
                <div class="row">
                    @include ('errors.list')
                    <div class="col-12 data_tables">
                        <div class="card m-t-35">
                            <div class="card-header bg-white">
                                <i class="fa fa-money circle"></i> All Transactions
                                {{--@can('Create Company')--}}
                                <a href="{{route('payment.index')}}" class="btn btn-warning pull-right" >Back</a>
                                <a href="#edit" class="btn btn-warning pull-right" data-toggle="modal" data-href="#edit" style="margin-right: 12px">Adjustment</a>
                                @if($transactions->first()->refund_balance != 0)
                                <a href = "#refund" class="btn btn-success pull-right btn-md " style="margin-right: 12px" data-toggle = "modal" id="refund'.$payments->id.'"  data-href = "#refund" data-id = "'. $transactions[0]->id .'" 
                                    data-total_paid_amount = "'.($total_paid_amount - $total_refund) .'" data-total_sales_price = "'. $total_sales_price .'"
                                    data-booking_id = "'. $transactions[0]->booking_id .'" > Refund </a >
                                @endif
                                
                                {{--@endcan--}}
                            </div>
                            <div class="card-body p-t-10">
                                <div class=" m-t-25">
                                    <table class="table table-striped table-bordered table-hover " id="refund_table" style="width: 100%">
                                        <thead>
                                        <tr>
                                            <th class="no-sort">Transaction ID</th>
                                            <th class="no-sort">Name</th>
                                             <th class="no-sort">Status</th>
                                            <th class="no-sort">Transaction</th>
                                            <th class="no-sort">Payments</th>
                                            <th class="no-sort">Adjustment</th>
                                            <th class="no-sort">Refunds</th>
                                            <th class="no-sort">Refund Balance</th>
                                            <th class="no-sort">Account Balance</th>
                                            <th class="no-sort">Notes</th>
                                            <th class="no-sort">Date Time</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($transactions)
                                            @foreach ($transactions as $transaction)

                                                <tr>

                                                    <td>{{ $transaction->id }}</td>
                                                    <td>@if(isset( $transaction->user_id )) {{$transaction->user->name.' '. $transaction->user->surname }}  @endif</td>
                                                     <td>@if(isset($transaction->status))@if($transaction->status == "0000") {{ 'APPROVED' }}  @else {{ 'DECLINED' }} @endif @endif</td>
                                                    @if($transaction->type == '01')
                                                     <td>Locker Payment</td>
                                                    @elseif($transaction->type == '02')
                                                      <td>3D Payment</td>
                                                    @elseif($transaction->type == '03')
                                                      <td>Charity Payment</td>
                                                    @elseif($transaction->type == '04')
                                                        <td>Charity Adjustment</td>
                                                    @elseif($transaction->type == '05')
                                                        <td>Locker Adjustment</td>
                                                    @elseif($transaction->type == '06')
                                                        <td>Refund</td>
                                                    @elseif($transaction->type == '07')
                                                        <td>Invoice Paid</td>

                                                    @endif
                                                    <td>{{ $transaction->amount }}</td>
                                                    @if($transaction->amount_change)
                                                    <td style="color: red">( {{ $transaction->amount_change }} )</td>
                                                    @else
                                                    <td></td>
                                                    @endif
                                                    @if($transaction->refund)
                                                    <td style="color: red">( {{ $transaction->refund }} )</td>
                                                    @else
                                                    <td></td>
                                                    @endif
                                                    @if($transaction->refund_balance != '0')
                                                    <td style="color: red">( {{ $transaction->refund_balance }} )</td>
                                                    @else
                                                    <td></td>
                                                    @endif
                                                    @if($transaction->account_balance != '0')
                                                    <td >{{ $transaction->account_balance }}</td>
                                                    @else
                                                    <td></td>
                                                    @endif
                                                    <td>{{ $transaction->notes }}</td>
                                                    <td>{{ \Carbon\Carbon::parse($transaction->created_at)->format('d M, Y H:i') }}</td>


                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('script')
    <!--plugin scripts-->
    <script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>
    <!-- end of plugin scripts -->
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk="   crossorigin="anonymous"></script>
    <!--Page level scripts-->
    <script type="text/javascript" src="/backend/js/pages/datatable.js"></script>
    <script type="text/javascript" src="/backend/js/pages/radio_checkbox.js"></script>
    <!-- end of global scripts-->
    <script type="text/javascript">

        $("#start_date").datepicker();
        $("#end_date").datepicker();

        $('#flash_message').fadeIn('fast').delay(2000).fadeOut('fast');
        $('#flash_message').fadeIn('slow').delay(15000).hide(0);

       $('#error_message').fadeIn('fast').delay(15000).fadeOut('fast');
        $('#error_message').fadeIn('slow').delay(2000).hide(0);

        $('body').on('click','#filter',function(e){
          var start_date = $("#start_date").val();
          var end_date = $("#end_date").val();
          console.log(start_date, end_date);

           var booking_id = <?php echo$transaction->booking_id; ?>;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({

                url: '/admin/payments/filter',

                type: 'POST',

                dataType: 'JSON',

                cache: false,

                data: {id: booking_id, start_date: start_date, end_date: end_date},

                success: function (data) {
                    console.log(data);
                   
                },

                error: function (jqXHR, textStatus, errorThrown) {

                    console.log(jqXHR);

                }

            });
        });

        var table = $('#refund_table').DataTable({
                "ordering": false,
                "autoWidth": false,
                processing: true,

                searching: true,
                stateSave: true,
               
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            });

        $('body').on('click','.edit-btn',function(e){

            var payment_id = $(this).data('id');
            var total_paid_amount = $(this).data('total_paid_amount');
            var total_sales_price = $(this).data('total_sales_price');

            var refund_amount = (parseFloat(total_paid_amount))-( parseFloat(total_sales_price));
            refund_amount =  refund_amount.toFixed(2);


            if(refund_amount <= 0){
                $('#refund_amount').val(0);
            }else{
                $('#refund_amount').val(refund_amount);
            }

            $('#payment_id').val(payment_id);
            $('#total_paid_amount').val(total_paid_amount);
            $('#total_sales_price').val(total_sales_price);

        });

        $('body').on('click','#check_price',function(e){
            e.preventDefault();
            var adjustment = parseFloat($('#adjustment').val());
            var refund_amount = parseFloat($('#refund_amount').val());
            console.log(typeof(adjustment), typeof(refund_amount));
            var notes = $('#notes').val();
            if(refund_amount < 0) {
               $("#errormsg_numsmall_refund").removeClass('d-none');
                $("#errormsg_numsmall_refund").css('width', '40%');
                $("#errormsg_numsmall_refund").css('margin-left', '16px');
                $('#amount').val(0);
                $("#errormsg_numsmall_refund").show().delay(2000).fadeOut();
              
              
           }else{
               if (parseInt(adjustment) >= parseInt(refund_amount)) {
                   if(notes.length <=0){
                       $("#errormsg_reason_refund").removeClass('d-none');
                        $("#errormsg_reason_refund").css('width', '40%');
                        $("#errormsg_reason_refund").css('margin-left', '16px');
                        $('#amount').val(0);
                        $("#errormsg_reason_refund").show().delay(2000).fadeOut();
                       
                   }else{
                       $("#refund_form").submit();
                   }
               } else {
                   $("#errormsg_refund").removeClass('d-none');
                $("#errormsg_refund").css('width', '40%');
                $("#errormsg_refund").css('margin-left', '16px');
                $('#amount').val(0);
                $("#errormsg_refund").show().delay(2000).fadeOut();
                  
               }
            }
        });

        $('body').on('click','#validate_change_price',function(e){

            e.preventDefault();
            var new_val = parseFloat($('#amount').val());
            var old_val = parseFloat($('#tamount').val());
            //console.log(old_val);
            if(old_val){
                if(new_val > old_val){
                    $("#errormsg").removeClass('d-none');
                    $("#errormsg").css('width', '40%');
                    $("#errormsg").css('margin-left', '16px');
                    $('#amount').val(0);
                    $("#errormsg").show().delay(2000).fadeOut();
                } else if(new_val < 0){
                    $("#errormsg_numsmall").removeClass('d-none');
                    $("#errormsg_numsmall").css('width', '40%');
                    $("#errormsg_numsmall").css('margin-left', '16px');
                    $('#amount').val(0);
                    $("#errormsg_numsmall").show().delay(7000).fadeOut();
                }else {
                    $('#changepriceform').submit();
                }
            } 
            else {
                $("#errormsg_avaempty").removeClass('d-none');
                $("#errormsg_avaempty").css('width', '40%');
                $("#errormsg_avaempty").css('margin-left', '16px');
                $('#amount').val(0);
                $("#errormsg_avaempty").show().delay(7000).fadeOut();
            }
        });

        $('#type').on('change', function() {
            var booking_id = <?php echo$transaction->booking_id; ?>;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({

                url: '/admin/payment/getPaymentTypes/'+ booking_id+ '/'+this.value,

                type: 'GET',

                dataType: 'JSON',

                cache: false,

                // data: {id: booking_id, type: this.value},

                success: function (data) {
                    console.log(data);
                    $('#tamount').val(data);
                },

                error: function (jqXHR, textStatus, errorThrown) {

                    console.log(jqXHR);

                }

            });
        });

        $('#amount').change(function() { 
            console.log(this.value);
            $("#errormsg").hide();
            $("#errormsg_numsmall").hide();
            var new_val = parseFloat(this.value);
            var old_val = parseFloat($('#tamount').val());

            console.log(new_val, old_val);

            if(new_val > old_val){
                $("#errormsg").removeClass('d-none');
                $("#errormsg").css('width', '40%');
                $("#errormsg").css('margin-left', '16px');
                $('#amount').val(0);
                $("#errormsg").show().delay(7000).fadeOut();
            }

            if(new_val < 0){
                $("#errormsg_numsmall").removeClass('d-none');
                $("#errormsg_numsmall").css('width', '40%');
                $("#errormsg_numsmall").css('margin-left', '16px');
                $('#amount').val(0);
                $("#errormsg_numsmall").show().delay(7000).fadeOut();
            }



         });
    </script>
@endsection