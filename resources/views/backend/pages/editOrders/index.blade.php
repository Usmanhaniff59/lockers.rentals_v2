@extends('backend.layouts.app')

@section('title', 'Lockers | Payments')
@section('style')
    <!--plugin styles-->
    <link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-switch/css/bootstrap-switch.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/switchery/css/switchery.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/radio_css/css/radiobox.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/checkbox_css/css/checkbox.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/modal/css/component.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-tagsinput/css/bootstrap-tagsinput.css"/>
    <link rel="stylesheet" type="text/css" href="/backend/vendors/animate/css/animate.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/radio_checkbox.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/chosen/css/chosen.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/form_elements.css"/>
    {{--<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">--}}

    <!-- end of plugin styles -->

    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/radio_checkbox.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css" />

    <style>
         .edit-card-header{
            border-bottom: 1px solid #00C0EF;
        }
        .page-link{
            padding: 16px;
            color: #525252;
            text-decoration: none;
            background-color: transparent;
        }
        .button-style{
            margin: 3px 2px;
            font-size: 12px;
            padding: 4px 6px;
        }
        .table thead th{
            vertical-align: top;
            text-align: left;
        }
         .dataTables_wrapper{
             overflow-x:auto;
         }
        .lockers_printing {
            overflow-x:auto;

        }
         .header_icon_margin{
             margin-right: 10px !important;
         }
         .header_icon_margin_left{
             margin-left: 10px !important;
         }

         .ui-datepicker {
            width: 300px;
            height: 300px;
            margin: 5px auto 0;
            font: 12pt Arial, sans-serif;
            /*-webkit-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
            -moz-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);*/
        }

            .ui-datepicker table {
                width: 100%;
            }

        .ui-datepicker-header {
            background: #3399ff;
            color: #ffffff;
            font-family:'Times New Roman';
            border-width: 1px 0 0 0;
            border-style: solid;
            border-color: #111;
        }

        .ui-datepicker-title {
            text-align: center;
            font-size: 15px;

        }

        .ui-datepicker-prev {
            float: left;
            cursor: pointer;
            background-position: center -30px;
        }

        .ui-datepicker-next {
            float: right;
            cursor: pointer;
            background-position: center 0px;
        }

        .ui-datepicker thead {
            background-color: #f7f7f7;

            /*border-bottom: 1px solid #bbb;*/
        }

        .ui-datepicker th {
            background-color:#808080;
            text-transform: uppercase;
            font-size: 8pt;
            color: #666666;
            /*text-shadow: 1px 0px 0px #fff;*/
            /*filter: dropshadow(color=#fff, offx=1, offy=0);*/
        }

        .ui-datepicker tbody td {
            padding: 0;
            /*border-right: 1px solid #808080;*/
        }

            .ui-datepicker tbody td:last-child {
                border-right: 0px;
            }

        .ui-datepicker tbody tr {
            border-bottom: 1px solid #bbb;
        }

            .ui-datepicker tbody tr:last-child {
                border-bottom: 0px;
            }

        .ui-datepicker a {
            text-decoration: none;
        }

        .ui-datepicker td span, .ui-datepicker td a {
            display: inline-block;
            /*font-weight: bold;*/
            text-align: center;
            width: 30px;
            height: 30px;
            line-height: 30px;
            color: #ffffff;
            /*text-shadow: 1px 1px 0px #fff;*/
            /*filter: dropshadow(color=#fff, offx=1, offy=1);*/
        }

        .ui-datepicker-calendar .ui-state-default {
              background: linear-gradient(#999999, #737373);
              color:#ffffff;
              height:40px;
              width:40px;

        }

        .ui-datepicker-calendar .ui-state-hover {
            background: #33adff;
            color: #FFFFFF;
        }

        .ui-datepicker-calendar .ui-state-active {
            background: #33adff;
            -webkit-box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1);
            -moz-box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1);
            box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1);
            color: #e0e0e0;
            text-shadow: 0px 1px 0px #4d7a85;
            border: 1px solid #55838f;
            position: relative;
            margin: -1px;
        }

        .ui-datepicker-unselectable .ui-state-default {
            background: #D6E4BE;
            color: #000;
        }

         .selected
         {
             background-color: #B0BED9 !important;
         }
         .loader {
             position: absolute;
             top: 52%;
             left: 45%;
             margin: auto;
             display: none;
             border: 16px solid #f3f3f3;
             border-radius: 50%;
             border-top: 16px solid #00c0ef;
             width: 100px;
             height: 100px;
             -webkit-animation: spin 2s linear infinite; /* Safari */
             animation: spin 2s linear infinite;
         }
    </style>

@endsection
@section('content')

    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-money circle header_icon_margin"></i>
                           Edit Order
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        @if(Session::has('flash_message'))
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable float-right" style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-hidden="true" style="margin-top: 4px; margin-left: 15px;"> ×
                    </button>
                    {{ Session::get('flash_message') }}
                </div>
            </div>
        @elseif(Session::has('error_message'))
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissable float-right" style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-hidden="true" style="margin-top: 4px; margin-left: 15px;"> ×
                    </button>
                    {{ Session::get('error_message') }}
                </div>
            </div>

        @endif

        @include('errors.success')
        @include('errors.error')
        <div class="outer">
            <div class="inner bg-container">
                <div class="row">
                    @include ('errors.list')
                    <div class="col-12 data_tables">
                        <!-- BEGIN EXAMPLE1 TABLE PORTLET-->
                        <div class="card">
                            <div class="card-header bg-white">
                                <i class="fa fa-money circle header_icon_margin"></i> All Orders
                                {{--<a href="{{route('lost.booking.index')}}" class="btn btn-primary btn-md adv_cust_mod_btn edit-btn pull-right"  style="margin-right: 3px;" >Show Lost Bookings</a>--}}

                            </div>
                            <div class="card-body m-t-95">
                                <div class="row p-t-15">
                                    <div class="col-md-2"><input type="text" id="search_company" placeholder="Company" class="form-control"></div>
                                    <div class="col-md-2"><input type="text" id="search_block" placeholder="Block" class="form-control"></div>
                                    <div class="col-md-2"><input type="text" id="search_locker_number" placeholder="Locker Number" class="form-control"></div>

                                    <div class="col-md-2"><input type="text" onchange="configDate()" id="search_date" class="form-control" placeholder="Start Date" ></div>
                                    {{--<div class="col-md-2"><input type="text" onchange="configDateEnd()" id="search_date_end" class="form-control" placeholder="End Date" ></div>--}}
                                    <div class="col-md-2"><input type="text" id="search_email" placeholder="Email" class="form-control"></div>
                                    <div class="col-md-2"><input type="text"  id="search_bookingId" placeholder="Booking Id" class="form-control"></div>


                                </div>
                                <div class="row p-t-15">
                                    <div class="col-md-2"></div>

                                    <div class="col-md-6 text-right">  </div>

                                    {{--@can('Move Booking')--}}
                                        <div class="col-md-2 text-right"><a href="#move_block" class="btn btn-info view-user" data-toggle="modal" id="show_moveable_blocks" style="margin-right: 3px;" data-href="#move_block" >Move Block</a>  </div>
                                    {{--@endcan     --}}
                                    @can('Move Booking')
                                        <div class="col-md-2 text-left"><a href="#." class="btn btn-info " id="clear_all_checked" style="margin-right: 3px;">Clear All Checked</a>  </div>
                                    @endcan

                                </div>
                                <div class="">

                                    <div class="pull-sm-right">
                                        <div class="tools pull-sm-right"></div>
                                    </div>

                                </div>

                                <div class="col-md-3">
                                    <div class="alert alert-success alert-dismissable " style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
                                        Use date filters to show records from previous years
                                    </div>
                                </div>
                                <br>


                                <input type="hidden" class="project_export" name="excel_array" value="1">
                                <table class="table table-bordered table-hover" style="width: 100%;" id="payments_order">
                                    <thead>
                                    <tr>
                                        <th> </th>
                                        <th>Booking ID</th>
                                        <th>Booking Date</th>
                                        <th>First Name</th>
                                        <th>Surname</th>
                                        <th>Company</th>
                                        <th>Block</th>
                                        <th>Number</th>
                                        <th>Status</th>
                                        {{--<th>Account Balance</th>--}}
                                        <th style="width: 20%"></th>
                                    </tr>
                                    </thead>
                                 </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

 <!--- Edit model -->
    <div class="modal fade in display_none" id="viewuser" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div id="editform_display">
                </div>

            </div>
        </div>
    </div>
    <!-- END Edit model-->

    <!--- Refund model -->
    <div class="modal fade in display_none" id="edit" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                {{ Form::open(array('url' => 'admin/refundPayment','id'=>'refund_form')) }}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <h3><i class="fa fa-money circle"></i> Refund Amount</h3>
                            <hr>
                            <br>
                            {{ Form::hidden('payment_id', '', array('class' => 'form-control', 'id'=>"payment_id")) }}
                            {{ Form::hidden('total_paid_amount', '', array('class' => 'form-control', 'id'=>"total_paid_amount")) }}
                            {{ Form::hidden('total_sales_price', '', array('class' => 'form-control', 'id'=>"total_sales_price")) }}

                            <div class="form-group">
                                {{ Form::label('refund_amount', 'Refund Amount') }}
                                {{ Form::text('refund_amount', '', array('class' => 'form-control', 'id'=>"refund_amount")) }}
                                <p style="color:red" class="price-validation d-none"></p>
                            </div>
                            <div class="form-group">
                                {{ Form::label('notes', 'Notes') }}
                                {{ Form::textarea('notes', '', array('class' => 'form-control', 'id'=>"notes")) }}
                                <p style="color:red" class="notes-validation d-none"></p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                    {{ Form::Button('Refund', array('class' => 'btn btn-primary','id'=>'check_price')) }}
                </div>
                {{ Form::close() }}
                <br>
            </div>
        </div>
    </div>
    <!-- END Refund modal-->

    <!--- Move Multi Block model -->
    <div class="modal fade in display_none" id="move_block" tabindex="-1" role="dialog" aria-hidden="false">
        {{ Form::open(array('url' => 'admin/refundPayment','id'=>'refund_form')) }}
                <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                    <div class="modal-body">
                    <input type="hidden" id="moveable_lockers">
                        <h3 id="company_name"> </h3>
                        <hr>
                        <br>
                        <div class="row">
                            <div class="col-1"></div>
                            <div class="col-10">
                                <div class="loader" id="block_loader"></div>
                                <div id="block_show"></div>
                            </div>
                            <div class="col-1"></div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-1"></div>
                            <div class="col-5">

                                <div id="display_moveable"></div>



                            </div>
                            <div class="col-5">

                                <div id="display_non_moveable"></div>


                            </div>
                            <div class="col-1"></div>
                        </div>
                        <br>
                        <br>
                        <div class="row">
                            <div class="col-12">
                                <div id="show_multi_done_checkbox" style="margin-left:9%"></div>

                            </div>
                        </div>
                    </div>

                    {{--<div class="modal-footer float-left">--}}


                        {{--<button type="button" data-dismiss="modal" class="btn btn-light">Close</button>--}}
                        {{--{{ Form::Button('save', array('class' => 'btn btn-primary','id'=>'move_booking')) }}--}}
                    {{--</div>--}}
                    <br>
                <div class="modal-footer text-right">
                    <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                    {{ Form::Button('Save', array('class' => 'btn btn-primary','id'=>'save_multi_lcoker')) }}
                </div>

                <br>
            </div>
        </div>
        {{ Form::close() }}
    </div>
    <!-- END Move booking modal-->
    <!--- Move booking model -->
    <div class="modal fade in display_none" id="move_booking" tabindex="-1" role="dialog" 
    aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                {{ Form::open(array('url' => 'admin/refundPayment','id'=>'refund_form')) }}
                <div class="modal-body">

                            <h3><i class="fa fa-money circle"></i> Move Booking</h3>

                    <div id="display_blocks"></div>

                <div class="modal-footer float-left">
                    <div class="checkbox ">
                        <input type="hidden" value="" id="booking_id_current">
                        <input type="hidden" value="" id="current_child_sale_id">
                        <input type="hidden" value="" id="sale_id_next_year">
                        <label>
                            <input type="checkbox" value="" id="hold_next_year">

                            <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                            Hold this locker to be booked while my child is at the school. You will be given the opportunity to confirm this booking in April.
                        </label>
                        <br>
                        <label>
                            <input type="checkbox" value="" id="email_checked">
                            <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                            Send confirmation Email
                        </label>
                        <br>
                        <label>
                            <input type="checkbox" value="">
                            <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                            Done
                        </label>
                    </div>

                   <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                   <button type="button"  id="move_checked" class="btn btn-primary">Save</button>

                    {{--{{ Form::Button('save', array('class' => 'btn btn-primary','id'=>'move_booking')) }}--}}
                </div>
                {{ Form::close() }}
                <br>
            </div>
        </div>
    </div>
    <!-- END Move booking modal-->

@endsection
@section('script')
    <!--plugin scripts-->
    <script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>
    <script type="text/javascript" src="/backend/js/pages/radio_checkbox.js"></script>
    <script type="text/javascript" src="/backend/vendors/chosen/js/chosen.jquery.js"></script>
    <script type="text/javascript" src="/backend/js/orders/choose_locker_order.js"></script>
    <script type="text/javascript" src="/backend/js/pages/form_elements.js"></script>
    <!-- end of plugin scripts -->
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk="   crossorigin="anonymous"></script>

    <!--Page level scripts-->
    <script type="text/javascript" src="/backend/js/pages/datatable_server_side.js"></script>
    <script type="text/javascript" src="{{asset('/backend/js/common.js')}}"></script>
    <script src="{{ asset('custom/general.js') }}"></script>

    <!-- end of global scripts-->
    <script type="text/javascript">
     sessionStorage.setItem("first_selected_company",'');

        function moveBooking(sale_id,booking_id){

            // alert(sale_id);
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                url: '/admin/edit/payment/getBlocks',
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {sale_id: sale_id,booking_id: booking_id, _token: CSRF_TOKEN},
                success: function (data) {
                    // console.log(data);
                    $('#display_blocks').html(data.blocks);
                    if(data.hold_next_year == true){
                        console.log(data.hold_next_year,'data.hold_next_year')
                        $('#hold_next_year').prop('checked', true);
                        $('#booking_id_current').val( data.booking_id);
                        $('#current_child_sale_id').val(data.current_child_sale_id);

                        $('#sale_id_next_year').val(  data.hold_next_year_sale_id);
                    }else{
                        $('#hold_next_year').prop('checked', false);
                        $('#booking_id_current').val( data.booking_id);
                        $('#current_child_sale_id').val(data.current_child_sale_id);

                        $('#sale_id_next_year').val(  data.hold_next_year_sale_id);
                    }
                    if(data.move_checked == true){

                        $('#move_checked').prop('checked', true);
                    }else{
                        $('#move_checked').prop('checked', false);
                    }
                    // if(data.email_checked == true){

                    //     $('#email_checked').prop('checked', true);
                    // }else{
                    //     $('#email_checked').prop('checked', false);
                    // }
                    sessionStorage.removeItem("selected_dropdown_current_tab");
                    sessionStorage.removeItem("selected_dropdown_start");
                    sessionStorage.removeItem("selected_dropdown_end");
                    sessionStorage.removeItem("selected_dropdown_rate");
                    sessionStorage.removeItem("selected_dropdown_tax");
                    sessionStorage.removeItem("selected_dropdown_block_id");
                    sessionStorage.removeItem("selected_dropdown_sale_id");
                    sessionStorage.removeItem("selected_dropdown_counter");
                    sessionStorage.removeItem("selected_dropdown_hold_locker");
                    sessionStorage.removeItem("selected_lockers");
                    $(".blocks").change();

                    var locker_ids = data.locker_ids;
                    console.log(locker_ids);
                    var locker_ids_array = [];
                    $.each(locker_ids, function( index, value ) {

                        if(value !== 'null')
                            locker_ids_array.push(value);
                        else
                            locker_ids_array.push(null);
                    });
                    sessionStorage.setItem("selected_dropdown_current_tab", 0);
                    sessionStorage.setItem("selected_lockers", JSON.stringify(locker_ids_array));

                    var storedArray = JSON.parse(sessionStorage.getItem("selected_lockers"));
                    // console.log(storedArray,'storedArray');

                        getNewLockers();
                    $('.child-name').css("background-color", "#00bf86");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                }
            });
        }

        $('body').on('click', '#hold_next_year', function (e) {

            var current_child_sale_id = $('#current_child_sale_id').val();
            // alert(current_child_sale_id);
            var sale_id_next_year = $('#sale_id_next_year').val();
            var booking_id = $('#booking_id_current').val();


            var checkbox_next = $('#hold_next_year').prop('checked');


            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({

                url: '/admin/edit/payment/updateNextYearBooking',

                type: 'POST',
                dataType: 'JSON',
                cache: false,

                data: {
                    booking_id: booking_id,
                    sale_id_next_year: sale_id_next_year,
                    current_child_sale_id: current_child_sale_id,
                    checkbox_next: checkbox_next,
                    _token: CSRF_TOKEN
                },

                success: function (data) {

                    console.log(data);
                    getNewLockers();

                },

                error: function (jqXHR, textStatus, errorThrown) {



                }

            });


        });

        $('body').on('click', '.choose-locker', function (e) {

            var counter = $(this).data('counter');

            $('#tabValidation'+ counter).addClass('d-none');
            $('#locker_already_exists_msg'+counter).addClass('d-none');

            var sale_id = $(this).data('sale_id');

            var block_id = $(this).data('block_id');
            var locker_id = $(this).data('locker_id');
            var locker_number = $(this).data('locker_number');

            var rate = $(this).data('rate');
            var tax = $(this).data('tax');
            var hold_locker = $(this).data('hold_locker');


            var index_of_sale_ids = sessionStorage.getItem("selected_dropdown_current_tab");
            console.log(index_of_sale_ids,'index_of_sale_ids')

            // sessionStorage.removeItem("selected_lockers");
            var storedArray = JSON.parse(sessionStorage.getItem("selected_lockers"));
            console.log(storedArray,'storedArray')
            var testArray = [];
            var i;

            if (storedArray != null) {
                for (i = 0; i < storedArray.length; i++) {
                    testArray[i] = storedArray[i];
                }
            }

            testArray[index_of_sale_ids] = locker_id;
            console.log(typeof  testArray ,'testArray');

            sessionStorage.setItem("selected_lockers", JSON.stringify(testArray));
            var selected_lockers = JSON.parse(sessionStorage.getItem("selected_lockers"));

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({

                url: '/admin/edit/payment/updateSale',

                type: 'POST',

                dataType: 'JSON',

                cache: false,

                data: {
                    sale_id: sale_id,
                    block_id: block_id,
                    locker_id: locker_id,
                    locker_number: locker_number,
                    rate: rate,
                    tax: tax,
                    hold_locker: hold_locker,
                    _token: CSRF_TOKEN
                },

                success: function (data) {

                    if(data == 'home_page'){
                        document.location.href = "/";
                    }
                    if(data.status == 'already_exists'){
                        $('#locker_already_exists_msg'+counter).removeClass('d-none');
                        const  selected_lockers = JSON.parse(sessionStorage.getItem("selected_lockers"));

                        var  locker_id = parseInt(data.locker_id);

                        const index = selected_lockers.indexOf(locker_id);
                        if (index > -1) {
                            selected_lockers.splice(index, 1);
                        }
                        sessionStorage.setItem("selected_lockers", JSON.stringify(selected_lockers));
                        var reomoved_locker =  JSON.parse(sessionStorage.getItem("selected_lockers"));

                    }else if(data.status == 'available'){
                        $('.nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link').css("color", "black")
                    }
                    getNewLockers();

                },

                error: function (jqXHR, textStatus, errorThrown) {



                }

            });


        });

        $('body').on('click', '#email_checked', function (e) {


            var checkbox_next = $('#email_checked').prop('checked');

            var booking_id = $('#sale_id').val();
            // alert(booking_id)

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                url: '/admin/edit/payment/updateEmailStatus',
                type: 'POST',
                dataType: 'JSON',
                cache: false,

                data: {
                    booking_id: booking_id,
                    checkbox_next: checkbox_next,
                    _token: CSRF_TOKEN
                },
                success: function (data) {
                    console.log(data);
                    var ref = $('#payments_order').DataTable();
                    ref.ajax.reload();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        });

        $('body').on('click', '#move_checked', function (e) {


            var checkbox_next = $('#move_checked').prop('checked');

            var booking_id = $('#sale_id').val();
            var f_name = $('#edit_pupil_premium_first_name').val();
            var l_name = $('#edit_pupil_premium_surname').val();

          
            // alert(booking_id)

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({

                url: '/admin/edit/payment/updateCheckedStatus',

                type: 'POST',
                dataType: 'JSON',
                cache: false,

                data: {
                    booking_id: booking_id,
                    checkbox_next: checkbox_next,
                    _token: CSRF_TOKEN,
                    l_name:l_name,
                    f_name:f_name,
                },

                success: function (data) {

                    console.log(data);
                    // paymentEntryTable()
                    var ref = $('#payments_order').DataTable();
                    ref.ajax.reload();
                    // $('#payments_order')
                    $('#move_booking').modal('toggle');

                },

                error: function (jqXHR, textStatus, errorThrown) {



                }

            });


        });



        var selectedrows=[];
        $('body').on('click', '.selectedrowproject', function() {

            var id=$(this).attr('id');
            console.log(id,'id')
            id=id.split('project');
            console.log(id,'id project')
            row_id='#project_tr'+id[1];
            console.log(row_id,'row_id')

            var company_id=$(this).data('company_id');
            console.log(company_id,'company_id')
            var first_selected_company =  sessionStorage.getItem("first_selected_company");
            console.log(first_selected_company,'first_selected_company');

            if(selectedrows == undefined || selectedrows.length == 0 ) {

                sessionStorage.setItem("first_selected_company", company_id);
                var first_selected_company = sessionStorage.getItem("first_selected_company");

            }
            var first_selected_company =  sessionStorage.getItem("first_selected_company");

            if(first_selected_company == company_id){

                if($(this).is(":checked"))
                {
                    selectedrows.push(id[1]);
                    console.log(selectedrows,'selectedrows checked')

                    $('.project_export').val(JSON.stringify(selectedrows));
                    $(row_id).addClass('selected');
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                    $.ajax({

                        url: '/admin/edit/payment/setSession',

                        type: 'POST',
                        dataType: 'JSON',
                        cache: false,
                        data: {
                            selectedrows: selectedrows ,company_id: company_id ,  _token: CSRF_TOKEN
                        },

                        success: function (data) {

                            $('#block_show').html(data.blocks);

                            // paymentEntryTable()

                        },

                        error: function (jqXHR, textStatus, errorThrown) {


                        }

                    });
                }else
                {
                    if(selectedrows.includes(id[1]))
                    {
                        selectedrows.splice( selectedrows.indexOf(id[1]),1);
                        console.log(selectedrows,'selectedrows unchckd')
                        $('.project_export').val(JSON.stringify(selectedrows));
                        $(row_id).removeClass('selected');

                        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                        $.ajax({

                            url: '/admin/edit/payment/setSession',

                            type: 'POST',
                            dataType: 'JSON',
                            cache: false,
                            data: {
                                selectedrows: selectedrows ,company_id: company_id ,  _token: CSRF_TOKEN
                            },

                            success: function (data) {

                                $('#block_show').html(data.blocks);

                                // paymentEntryTable()

                            },

                            error: function (jqXHR, textStatus, errorThrown) {


                            }

                        });
                    }
                }
            }else{
                alert('You have selected a different company. Please select the same company');
                $('#project'+id[1]).prop('checked', false);
            }


        });

        $('body').on('click', '#show_moveable_blocks', function (e) {
            $('#block_show').html('');
            $('#company_name').html('');
            $('#moveable_lockers').html('');
            $('#display_moveable').html('');
            $('#display_non_moveable').html('');
            $('#show_multi_done_checkbox').html('');
            console.log("Selected Rows : "+selectedrows);
            if(selectedrows.length != 0) {
                var company_id = sessionStorage.getItem("first_selected_company");

                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                $.ajax({

                    url: '/admin/edit/payment/showMoveableBlocks',

                    type: 'POST',
                    dataType: 'JSON',
                    cache: false,
                    data: {
                        ids: selectedrows, company_id: company_id, _token: CSRF_TOKEN
                    },

                    success: function (data) {

                        $('#block_show').html(data.blocks);
                        $('#company_name').html('<i class="fa fa-money circle"></i> ' + data.company);


                        // paymentEntryTable()

                    },

                    error: function (jqXHR, textStatus, errorThrown) {


                    }

                });
            }else{
                $('#display_moveable').html('');
                $('#block_show').html('');
                $('#company_name').html('');
                $('#moveable_lockers').html('');
                $('#display_moveable').html('');
                $('#display_non_moveable').html('');
                $('#show_multi_done_checkbox').html('');
            }



        });

        $('body').on('click', '#save_multi_lcoker', function (e) {

            var checkbox_done = $('#multi_done').prop('checked');


            // var booking_id = $('#sale_id').val();
            // alert(booking_id)
            var  moveable_lockers = JSON.parse(sessionStorage.getItem("moveable_lockers"));
            console.log(moveable_lockers);
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({

                url: '/admin/edit/payment/updateMultiDone',

                type: 'POST',
                dataType: 'JSON',
                cache: false,

                data: {
                    moveable_lockers: moveable_lockers,
                    checkbox_done: checkbox_done,
                    _token: CSRF_TOKEN
                },

                success: function (data) {
                    console.log(data);
                    selectedrows=[];
                    // paymentEntryTable();
                    var ref = $('#payments_order').DataTable();
                    ref.ajax.reload();
                    $('#move_block').modal('toggle');


                },

                error: function (jqXHR, textStatus, errorThrown) {

                }

            });


        });

        $('body').on('change', '#blocks', function (e) {

            $('#display_moveable').html('');
            $('#display_non_moveable').html('');

            console.log("Selected Rows : "+selectedrows);
            var company_id =  sessionStorage.getItem("first_selected_company");
            $(".loader").show();
            jQuery('#block_show').css('opacity', '0.05');
            // alert(company_id)
            var block_id = $('#blocks').val();

                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                $.ajax({

                    url: '/admin/edit/payment/blockValidation',

                    type: 'POST',
                    dataType: 'JSON',
                    cache: false,
                    data: {
                       ids: selectedrows ,company_id: company_id , block_id: block_id ,  _token: CSRF_TOKEN
                    },

                    success: function (data) {
                        console.log(data);
                        $('#display_moveable').html(data.can_move);
                        $('#display_non_moveable').html(data.can_not_move);
                        $('#moveable_lockers').val(data.moveable_lockers);
                        $('#show_multi_done_checkbox').html(data.done_html);
                        var moveable_lockers = [];
                        moveable_lockers = data.moveable_lockers;
                        console.log(moveable_lockers,'moveable_lockers');
                        sessionStorage.setItem("moveable_lockers",JSON.stringify(moveable_lockers));
                        var moveable_lockers = sessionStorage.getItem("moveable_lockers");
                        console.log(moveable_lockers,'session moveable_lockers');
                        $(".loader").hide();
                        jQuery('#block_show').css('opacity', '1');
                        // $('#block_show').html(data.blocks);
                        // $('#move_block').modal('toggle');
                        // paymentEntryTable()
                        // sessionStorage.setItem("first_selected_company",'');
                        // selectedrows = [];
                    },

                    error: function (jqXHR, textStatus, errorThrown) {


                    }

                });



        });

        $('body').on('click', '#clear_all_checked', function (e) {



            // alert(booking_id)
            if (confirm("Are you sure to clear all checked?")) {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                $.ajax({

                    url: '/admin/edit/payment/clearAllChecked',

                    type: 'POST',
                    dataType: 'JSON',
                    cache: false,
                    data: {
                        _token: CSRF_TOKEN
                    },

                    success: function (data) {
                        var ref = $('#payments_order').DataTable();
                    ref.ajax.reload();
                        // paymentEntryTable()

                    },

                    error: function (jqXHR, textStatus, errorThrown) {


                    }

                });
            }


        });

        $(document).ready(function() {
            $("#search_date").datepicker();
            $("#search_date_end").datepicker();
            var refresh = null;
             var search_company = sessionStorage.getItem('search_company');
            $("#search_company").val(search_company);

            var search_block = sessionStorage.getItem('search_block');
            $("#search_block").val(search_block);

            var search_locker_number = sessionStorage.getItem('search_locker_number');
            $("#search_locker_number").val(search_locker_number);

            var search_email = sessionStorage.getItem('search_email');
            $("#search_email").val(search_email);

            var search_bookingId = sessionStorage.getItem('search_bookingId');
            $("#search_bookingId").val(search_bookingId);

            var searchinput = sessionStorage.getItem('search-input');
            $("#searchinput").val(searchinput);

            var search_date = sessionStorage.getItem('search_date');
            $("#search_date").val(search_date);

            var search_date_end = sessionStorage.getItem('search_date_end');
            $("#search_date_end").val(search_date_end);

            $('#payments_order_length').attr('style', 'width:100%;margin-top:30px;');
            var searchFunction = 'searchByThis(payment_table)';
            var payments_search = "{{\Illuminate\Support\Facades\Session::get('payments-search')}}";
            $('#payments_order_wrapper>.row:nth-child(1)>div:nth-child(2)').append('<label style="float:right;margin-top:30px;"><input class="form-control input-sm" id="search-input" value="' + payments_search + '" onblur="' + searchFunction + '"></label>');
              var oTable = $('#payments_order').dataTable();
              // Sometime later - filter...
              oTable.fnFilter(payments_search);

           

            $("#search_company").keypress(function(event) {
                if (event.keyCode === 13) {
                    searchByThis(payment_table);
                    sessionStorage.setItem('search_company', $("#search_company").val())
                }

            });

            $("#search_block").keypress(function(event) {
                if (event.keyCode === 13) {
                    searchByThis(payment_table);
                    console.log($("#search_block").val(),'$("#search_block").val()')
                    sessionStorage.setItem('search_block', $("#search_block").val())
                }

            });
            $("#search_locker_number").keypress(function(event) {
                if (event.keyCode === 13) {
                    searchByThis(payment_table);
                    console.log($("#search_locker_number").val(),'$("#search_locker_number").val()')
                    sessionStorage.setItem('search_locker_number', $("#search_locker_number").val())
                }

            });

            $("#search_email").keypress(function(event) {
                if (event.keyCode === 13) {
                    searchByThis(payment_table);

                }
                sessionStorage.setItem('search_email', $("#search_email").val())
            });
            $("#search_bookingId").keypress(function(event) {
                if (event.keyCode === 13) {
                    searchByThis(payment_table);
                    
                }
                sessionStorage.setItem('search_bookingId', $("#search_bookingId").val())
            });
            $("#search-input").keypress(function(event) {
                if (event.keyCode === 13) {
                    searchByThis(payment_table);
                    
                }
                sessionStorage.setItem('search-input', $("#search-input").val())
            });

        });

        function configDate(){
            searchByThis(payment_table);
            sessionStorage.setItem('search_date', $("#search_date").val());
        }

         function configDateEnd(){
            searchByThis(payment_table);
            sessionStorage.setItem('search_date_end', $("#search_date_end").val());
        }

         var payment_table = paymentEntryTable();

        $('body').on('click','.view-user',function(e){

            var user_id = $(this).data('id');
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                url: '/admin/payment/getuserroles',
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {user_id: user_id, type: 'report', _token: CSRF_TOKEN},
                success: function (data) {
                    $('#editform_display').html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                }
            });
        });

        function paymentEntryTable() {
            // $('#payments_order').dataTable().fnClearTable();
            // $('#payments_order').dataTable().fnDestroy();

            var table= $('#payments_order').DataTable({
                serverSide: true,
                searching: false,
                processing:true,
                stateSave: true,
                 "order": [[ 7, "desc" ]],
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                "oLanguage": {
                    "sLengthMenu": "_MENU_",
                },
                bInfo:false,
                initComplete : function() {
                    $("#payments_order_wrapper>.row").attr('style','width:100%;');
                    $("#payments_order_filter>label").css('float', 'right');
                    $("#payments_order_length>label").css('float', 'left');
                },
                ajax: '<?php echo route('payment.edit.show.all'); ?>',
                ajax: {
                    url: '<?php echo e(route('payment.edit.show.all')); ?>',
                    data: function (d) {
                        d.search_company = $('#search_company').val();
                        d.search_block = $('#search_block').val();
                        d.search_locker_number = $('#search_locker_number').val();
                        d.search_email = $('#search_email').val();
                        d.search_date = $('#search_date').val();
                        d.search_date_end = $('#search_date_end').val();
                        d.search_bookingId = $('#search_bookingId').val();
                        d.search_input = $('#search-input').val();
                    }
                },
                "createdRow": function( row, data, dataIndex,columns,col )
                {
                    $(row).attr('id', 'project_tr'+data['id']);
                    $(row).attr('data-id',data['id']);

                    var id = data['id'].toString();
                    if(selectedrows.includes(id) == true){
                        $(row).addClass('selected');
                    }

                },
                columns: [
                    {data: 'checkbox', name: 'checkbox'},
                    {data: 'booking_id', name: 'booking_id'},
                    {data: 'created', name: 'created'},
                    {data: 'child_first_name', name: 'child_first_name'},
                    {data: 'child_surname', name: 'child_surname'},
                    {data: 'company', name: 'company'},
                    {data: 'block', name: 'block'},
                    {data: 'number', name: 'number'},
                    {data: 'status', name: 'status'},
                    // {data: 'total_paid_amount', name: 'total_paid_amount'},
                    {data: 'action', name: 'action', orderable: false, searchable: true}
                ]
            });
            return table;
        }

        function pre_highlighted_row() {
            console.log("being called");
            if (typeof(sessionStorage.getItem("row_highlited")) !== "undefined" && typeof(sessionStorage.getItem("row_highlited")) !== null) {
                var row = sessionStorage.getItem("row_highlited");
                setTimeout($('#' + row).closest("tr").css({
                    "background-color": "#DAF7A6"
                }));
            }
        }

        function resend_confirm(booking_id) {

            if (window.confirm("Are you sure to Resend Confirmation Email?")) {


                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({

                    url: '/admin/edit/payment/resendEmail',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {booking_id: booking_id},
                    success: function (data) {

                        if(data == 'success'){


                            $('#resend'+booking_id).addClass('d-none');
                        }
                    },
                    error: function (jqXHR) {


                    }

                });


            }

                    }

        $('body').on('click','.edit-btn',function(e){

            $('.price-validation').addClass('d-none');
            $('#notes').val('');
            var payment_id = $(this).data('id');
            var total_paid_amount = $(this).data('total_paid_amount');
            // alert(total_paid_amount);
            var total_sales_price = $(this).data('total_sales_price');

            // alert(total_sales_price);
            // total_paid_amount = 4.87;
            // total_sales_price = 4.005;

           var refund_amount = (parseFloat(total_paid_amount))-( parseFloat(total_sales_price));
            refund_amount =  refund_amount.toFixed(2);


           if(refund_amount <= 0){
               $('#refund_amount').val(0);
           }else{
               $('#refund_amount').val(refund_amount);
           }

            $('#payment_id').val(payment_id);
            $('#total_paid_amount').val(total_paid_amount);
            $('#total_sales_price').val(total_sales_price);

        });

        $('body').on('click','#check_price',function(e){

            e.preventDefault();
           var total_sales_price =  $('#total_sales_price').val();
           var total_paid_amount = $('#total_paid_amount').val();
           var refund_amount = $('#refund_amount').val();
            var notes = $('#notes').val();
           if(refund_amount < 0) {
               $('.price-validation').text('You can\'t add Negative(-) value');
               $('.price-validation').removeClass('d-none');
               $('#refund_amount').focus();

           }else{

               var after_refund_amount = total_paid_amount - refund_amount;
               // if(after_refund_amount == 0){
               //     $('.price-validation').text('No need to refund 0');
               //     $('.price-validation').removeClass('d-none');
               //     $('#refund_amount').focus();
               // }else{

               if (after_refund_amount >= total_sales_price ) {
                   if(notes.length <=0){
                       $('.notes-validation').text('Add a note!');
                       $('.notes-validation').removeClass('d-none');
                       $('#notes').focus();
                   }else{
                       $("#refund_form").submit();
                   }


               } else {
                   // var can_refund_amount = total_paid_amount - total_sales_price;
                   $('.price-validation').text('Refunded Amount is higher then Sale price');
                   $('.price-validation').removeClass('d-none');
                   $('#refund_amount').focus();
               }


               }
           // }

        });

    </script>

    </div>
@endsection

