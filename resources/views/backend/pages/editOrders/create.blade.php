@extends('backend.layouts.app')

@section('title', 'Lockers | Add Payment')

@section('content')

    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-user-plus"></i>
                            Add Payment
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        <div class="outer">
            <div class="inner bg-container">
                <!--top section widgets-->
                <div class="row widget_countup">
                    <div class="col-12">
                        <hr>

                         @include ('errors.list')

                        {{ Form::open(array('url' => 'admin/payment')) }}

                        <div class='form-group'>
                            <label for="status">Location Group</label>
                            <select class="form-control" name="location_group_id" >
                                <option value="" selected>Choose Location Group</option>

                                @foreach ($locationGroups as $locationGroup)
                                        <option value="{{$locationGroup->id}}">{{$locationGroup->name}}</option>
                                @endforeach

                            </select>
                        </div>

                        <div class="form-group">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name', '', array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('stop_auto_rebook', 'Stop Auto Rebook') }}
                            {{ Form::text('stop_auto_rebook', null, array('class' => 'form-control')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('name_number', 'Name Number') }}
                            {{ Form::text('name_number', null, array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('street', 'Street') }}
                            {{ Form::text('street', null, array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('city', 'City') }}
                            {{ Form::text('city', null, array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('city_plus_search', 'City Plus Search') }}
                            {{ Form::text('city_plus_search', null, array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('country', 'Country') }}
                            {{ Form::text('country', null, array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('post_code', 'Postal Code') }}
                            {{ Form::text('post_code', null, array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('address', 'Address') }}
                            {{ Form::text('address', null, array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('Currency', 'Currency') }}<br>

                                {{ Form::radio('currency', 'GBP',true )}}
                                {{ Form::label('GBP', 'GBP') }}
                                {{ Form::radio('currency', 'EUR' )}}
                                {{ Form::label('EUR', 'EUR') }}

                        </div>

                        <div class="form-group">
                            <label for="status">Active</label>
                            <select class="form-control" name="active" >
                                    <option value="1" {{ old('active') == 1 ? 'selected' : '' }}>Yes</option>
                                    <option value="0" {{ old('active') == 0 ? 'selected' : '' }}>No</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="status">Display</label>
                            <select class="form-control" name="display" >
                                    <option value="1" {{ old('display') == 1 ? 'selected' : '' }}>Yes</option>
                                    <option value="0" {{ old('display') == 0 ? 'selected' : '' }}>No</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="status">Check Man</label>
                            <select class="form-control" name="check_man" >
                                    <option value="1" {{ old('check_man') == 1 ? 'selected' : '' }} >Yes</option>
                                    <option value="0" {{ old('check_man') == 0 ? 'selected' : '' }}>No</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="status">Keep Block</label>
                            <select class="form-control" name="keep_block" >
                                    <option value="1" {{ old('keep_block') == 1 ? 'selected' : '' }} >Yes</option>
                                    <option value="0" {{ old('keep_block') == 0 ? 'selected' : '' }}>No</option>
                            </select>
                        </div>

                        {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}

                        {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection