@extends('backend.layouts.app')
@section('title', 'Lockers | Home')
@section('style')
    <!--plugin styles-->


    <link rel="stylesheet" type="text/css" href="/backend/css/pages/widgets.css">
@endsection

@section('content')

    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-home circle"></i>
                            Dashboard
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        <div class="outer">
            <div class="inner bg-container">

                <div class="col-lg-4 col-sm-6 col-12">
                   <div class="bg-warning m-t-35 header_align">
                    <div class="row">
                        <div class="col-lg-12 col-12">
                            <div class="row">
                                <div class="col-12 float-left">
                                    <h2 class="text-white">Messages Sent</h2>
                                </div>
                                <div class="col-12">
                                    <h1 class="text-white float-right" >{{$total_messages}}</h1>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

    <script type="text/javascript" src="/backend/js/pages/widget1.js"></script>

@endsection