@extends('backend.layouts.app')
@section('title', 'Lockers | All Pages')
@section('style')
        <!--plugin styles-->
<link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css" />
<link type="text/css" rel="stylesheet" media="screen" href="/backend/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css"/>
<link type="text/css" rel="stylesheet" href="/backend/vendors/summernote/css/summernote.css"/>

<!-- end of plugin styles -->
<!--Page level styles-->
<link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/pages/form_elements.css"/>
@endsection
@section('content')
    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-table circle"></i>
                            All Pages
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        @if(Session::has('flash_message'))
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable float-right" style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-hidden="true" style="margin-top: 4px; margin-left: 15px;"> ×
                    </button>
                    {{ Session::get('flash_message') }}
                </div>
            </div>
        @endif
        <div class="outer">
            <div class="inner bg-container">
                <div class="row">
                    <div class="col-12 data_tables">

                        <div class="card m-t-35">
                            <div class="card-header bg-white">
                                <i class="fa fa-table circle"></i> All Pages  @can('Create Page')
                                    <a href="#create" class="btn btn-primary pull-right  btn-md adv_cust_mod_btn" data-toggle="modal" data-href="#create" >Add New Page</a>
                                @endcan
                            </div>
                            <div class="card-body p-t-10">
                                <div class=" m-t-25">
                                    <table class="table table-striped table-bordered table-hover " id="sample_6">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($pages as $page)
                                            <tr>
                                                <td><a href="{{ route('pages.show', $page->id ) }}"><b>{{ $page->title }}</b></a></td>
                                                <td>
                                                    <a href="#view" class="btn btn-success btn-md adv_cust_mod_btn view-btn pull-left" data-toggle="modal" style="margin-right: 3px;" data-href="#view"  data-otitle="{{ $page->title }}" data-body="{{ $page->body }}" >View</a>
                                                @can('Edit Page')
                                                        <a href="#edit" class="btn btn-primary btn-md adv_cust_mod_btn edit-btn pull-left" data-toggle="modal" style="margin-right: 3px;" data-href="#edit" data-id="{{ $page->id }}" data-otitle="{{ $page->title }}" data-body="{{ $page->body }}" >Edit</a>
                                                    @endcan
                                                    @can('Delete Page')
                                                    {!! Form::open(['method' => 'DELETE', 'route' => ['pages.destroy', $page->id] ]) !!}
                                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                                    @endcan

                                                    {!! Form::close() !!}

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!--- create model -->
    <div class="modal fade in display_none" id="create" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                {{ Form::open(array('route' => 'pages.store')) }}
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <h3><i class="fa fa-plus-circle circle"></i> Create New Page</h3>
                                <hr>

                                <div class="form-group">
                                    {{ Form::label('title', 'Title') }}
                                    {{ Form::text('title', null, array('class' => 'form-control')) }}
                                    <br>

                                    {{ Form::label('body', 'Page Body') }}
                                    {{ Form::textarea('body', null, array('class' => 'form-control textarea', 'id'=>'details_id')) }}
                                    <br>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="display: flex;">
                    <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                    {{ Form::submit('Create Page', array('class' => 'btn btn-primary  ')) }}
                </div>
                {{ Form::close() }}
                <br>
            </div>
        </div>
    </div>
    <!-- END create modal-->


    <!--- Edit model -->
    <div class="modal fade in display_none" id="edit" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    {{ Form::open(array('route' => array('pages.update', 1), 'method' => 'PUT')) }}
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <h3 id="edit_title"></h3>
                                    <hr>
                                    <div class="form-group">
                                            {{ Form::hidden('id', null, array('class' => 'form-control', 'id'=>'pageId_edit')) }}
                                            <br>
                                            {{ Form::label('title', 'Title') }}
                                            {{ Form::text('title', null, array('class' => 'form-control', 'id'=>'title_edit')) }}
                                            <br>

                                            {{ Form::label('body', 'Page Body') }}
                                            {{ Form::textarea('body', null, array('class' => 'form-control textarea', 'id'=>'body_edit')) }}
                                            <br>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer" style="display: flex;">
                                <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                                {{ Form::submit('Update Page', array('class' => 'btn btn-primary  ')) }}
                                </button>
                            </div>
                        </div>
                    {{ Form::close() }}

                </div>
            </div>
    </div>
    <!-- END Edit model-->

    <!--- View model -->
    <div class="modal fade in display_none" id="view" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <h3 id="view_header"></h3>
                            <hr>
                            <h3 id="title_view"></h3>
                            <p id="body_view"></p>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Edit model-->
@endsection
@section('script')
        <!--plugin scripts-->
    <script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/tinymce/js/tinymce.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap3-wysihtml5-bower/js/bootstrap3-wysihtml5.all.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/bootstrap3_wysihtml5.js"></script>
    <script type="text/javascript" src="/backend/vendors/summernote/js/summernote.min.js"></script>
    <script type="text/javascript" src="/backend/js/pages/form_editors.js"></script>
    <!-- end of plugin scripts -->
    <!--Page level scripts-->
    <script type="text/javascript" src="/backend/js/pages/datatable.js"></script>
    <!-- end of global scripts-->
        <script type="text/javascript">

            $('body').on('click','.edit-btn',function(){

                $('#edit_title').html('<i class="fa fa-pencil circle"></i>  Edit '+ $(this).data('otitle'));
                $('#pageId_edit').val($(this).data('id'));
                $('#title_edit').val($(this).data('otitle'));
                var body = $(this).data('body');
                $('#body_edit').data("wysihtml5").editor.setValue(body);

            });
            $('body').on('click','.view-btn',function(){

                $('#view_header').html('<i class="fa fa-eye circle"></i>  View '+ $(this).data('otitle'));
                $('#title_view').text($(this).data('otitle'))
                $('#body_view').html($(this).data('body'));

                alert(x);

            });

        </script>
@endsection