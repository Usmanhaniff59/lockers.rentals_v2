@extends('backend.layouts.app')

@section('title', 'Lockers | View Post')

@section('content')

    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-key"></i>
                            Add Permission
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        <div class="outer">
            <div class="inner bg-container">


                <!--top section widgets-->
                <div class="row widget_countup">
                    <div class="col-12">
    
    <h1>{{ $page->title }}</h1>
    <hr>
    <p class="lead">{!! $page->body !!}  </p>
    <hr>
    {!! Form::open(['method' => 'DELETE', 'route' => ['pages.destroy', $page->id] ]) !!}
    <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
    @can('Edit Page')
    <a href="{{ route('pages.edit', $page->id) }}" class="btn btn-info" role="button">Edit</a>
    @endcan
    @can('Delete Page')
    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
    @endcan
    {!! Form::close() !!}


                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection