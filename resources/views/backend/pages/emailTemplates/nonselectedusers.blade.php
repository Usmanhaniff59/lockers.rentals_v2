@if ($non_group_users)
@foreach ($non_group_users as $non_group_user)

    <article>
        <h2>{{ $non_group_user->title }}</h2>
        {{ $non_group_user->summary }}
    </article>

@endforeach

{{ $non_group_users->links() }}
@endif