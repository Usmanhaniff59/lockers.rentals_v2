@extends('backend.layouts.app')

@section('title', 'Lockers | Email Template')
@section('style')

    <!--plugin styles-->
    <link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css" />

    <link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css" />

    <link type="text/css" rel="stylesheet" href="/backend/vendors/modal/css/component.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-tagsinput/css/bootstrap-tagsinput.css"/>
    <link rel="stylesheet" type="text/css" href="/backend/vendors/animate/css/animate.min.css" />

    <!--wizard styles-->
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/wizards.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrapvalidator/css/bootstrapValidator.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/wizards.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/Buttons/css/buttons.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/buttons.css"/>


    <link type="text/css" rel="stylesheet" href="/backend/vendors/inputlimiter/css/jquery.inputlimiter.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/chosen/css/chosen.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/jquery-tagsinput/css/jquery.tagsinput.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/daterangepicker/css/daterangepicker.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datepicker/css/bootstrap-datepicker.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-switch/css/bootstrap-switch.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/multiselect/css/multi-select.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/switchery/css/switchery.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/checkbox_css/css/checkbox.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datetimepicker/css/DateTimePicker.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/clockpicker/css/jquery-clockpicker.css" />


    <!-- end of plugin styles -->

    <!--End of Plugin styles-->
    <!--Page level styles-->

    <!-- end of plugin styles -->

    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/portlet.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/advanced_components.css"/>

    <link type="text/css" rel="stylesheet" media="screen" href="/backend/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/summernote/css/summernote.css"/>

    <link type="text/css" rel="stylesheet" href="/backend/css/pages/form_elements.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/radio_checkbox.css" />



    <style>
        a:hover {
            color: #00bfff;
            background-color: transparent;
            text-decoration: underline;
        }
        a:active {
            color: yellow;
            background-color: transparent;
            text-decoration: underline;
        }


        .daterangepicker.dropdown-menu {
            z-index: 9999999 !important;
            position: relative !important;
            top: -545px  !important;
            left: 295px !important;
        }
        /*.cke_dialog_container{*/
            /*z-index: 999999;*/
        /*}*/
           .mce-container{
            z-index: 77777!important;
        }
        .modal{
            z-index: 1000000 !important;
        }



    </style>

@endsection
@section('content')

    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-envelope circle"></i>
                            Email Template Administration
                        </h4>
                    </div>
                </div>
            </div>
        </header>



        @if(Session::has('flash_message'))
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable float-right" style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-hidden="true" style="margin-top: 4px; margin-left: 15px;"> ×
                    </button>
                    {{ Session::get('flash_message') }}
                </div>
            </div>
        @endif
        <div class="outer">

            <div class="inner bg-container">
                {{--<textarea id="tinymce_full" rows="7"  class="template_text" name="template_text"> </textarea>--}}

                <div class="row">
                    <div class="col-12 data_tables">
                        <div class="card m-t-35">
                            <div class="card-header bg-white">
                                <i class="fa fa-envelope circle"></i> All Email Templates
                            </div>
                            <div class="card-body p-t-10">
                                <div class=" m-t-25">
                                    <table class="table table-striped table-bordered table-hover " id="sample_6">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Operations</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($email_templates)
                                            @foreach ($email_templates as $email_template)
                                                <tr>

                                                    <td ><a href="{{ URL::to('admin/emailtemplates/'.$email_template->id) }}" id="templateName{{ $email_template->id }}">{{ $email_template->template_name }}</a></td>

                                                    <td>
                                                        @can('Edit EmailTemplate')
                                                            <a href="#edit" class="btn btn-info btn-md adv_cust_mod_btn pull-left edit-btn" data-toggle="modal" data-href="#edit" data-id="{{ $email_template->id }}"  style="margin-right: 3px;">Edit</a>
                                                        @endcan
                                                        @can('Delete EmailTemplate')
                                                            {!! Form::open(['method' => 'DELETE', 'route' => ['emailtemplates.destroy', $email_template->id] ]) !!}
                                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                                            {!! Form::close() !!}
                                                        @endcan

                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--- Edit model -->
    <div class="modal fade in display_none " id="edit" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="outer form_wizzards">
                                <div class="inner bg-container">
                                    <div class="row">
                                        <div class="col">
                                            <div class="card">
                                                <div class="card-header bg-white" id="template_name_title">
                                                </div>
                                                <form id="modal" action="{{ route('updateemailtemplate') }}" method="POST">
                                                    @csrf
                                                    <div class="card-body m-t-20">
                                                        <div id="rootwizard_no_val">
                                                            <ul class="nav nav-pills">
                                                                <li class="nav-item user1 m-t-15 template-tab d-none">
                                                                    <a class="nav-link tabs-active" href="#email-template" data-toggle="tab" id="email_template_tab"><span
                                                                                class="userprofile_tab">1</span>Email Template
                                                                    </a>
                                                                </li>
                                                                <li class="nav-item user2 m-t-15 template-tab d-none">
                                                                    <a class="nav-link profile_details tabs-active" href="#distribution-list"
                                                                       data-toggle="tab"><span class="profile_tab">2</span>Distribution List
                                                                        Group</a>
                                                                </li>
                                                                <li class="nav-item user3 m-t-15 template-tab d-none">
                                                                    <a class="nav-link groups-checkboxes tabs-active" href="#additional-contacts"
                                                                       data-toggle="tab"><span class="profile_tab">3</span>Additional Contacts</a>
                                                                </li>
                                                                <li class="nav-item user4 m-t-15 template-tab d-none">
                                                                    <a class="nav-link tabs-active" href="#customer-lists"
                                                                       data-toggle="tab"><span class="profile_tab">4</span>Customer Lists</a>
                                                                </li>
                                                                <li class="nav-item finish_tab m-t-15 template-tab d-none">
                                                                    <a class="nav-link tabs-active" href="#customer-list-activation" data-toggle="tab" id="customer-list"><span>5</span>Customer List Activation</a>
                                                                </li>
                                                            </ul>
                                                            <div class="tab-content m-t-20">
                                                                <div class="tab-pane tabs-active" id="email-template">
                                                                    <h3>Email Templates </h3>
                                                                    <hr>
                                                                    <input id="template_id" name="template_id" type="hidden" >
                                                                    <input id="main_template" type="hidden" >
                                                                    <div id="email_template_alt_dropdown"></div>
                                                                    <label>Template Name</label>
                                                                    <p>
                                                                        <input id="template_name" name="template_name" type="text" placeholder="Template Name"
                                                                               class="form-control"></p>
                                                                    <label>Subject</label>
                                                                    <p>
                                                                        <input id="subject" name="subject" type="text" placeholder="subject"
                                                                               class="form-control"></p>
                                                                    <div class="row summer_note_display summer_note_btn">
                                                                        <div class="col">
                                                                            <div class='card m-t-35'>
                                                                                <div class='card-header bg-white '>
                                                                                    Template Text
                                                                                    <!-- tools box -->
                                                                                    <button type="button" class="btn btn-success  button-rounded pull-right system-function-btn" id="system-function-btn" style="padding: 5px 25px; margin-bottom: 20px;">Send Test Email</button>

                                                                                    <div class="float-right box-tools"></div>
                                                                                </div>
                                                                                <div class='card-body pad m-t-25'>
                                                                                   <textarea id="tinymce_full" rows="7"  class="template_text" name="template_text"> </textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <br>
                                                                    <hr>
{{--                                                                    <div class="input_field_sections">--}}
                                                                        <div class='card m-t-35'>
                                                                            <div class='card-body pad m-t-25'>
                                                                                <label>SMS </label>
                                                                                <textarea id="limiter" class="form-control sms_message" cols="50" rows="5" placeholder="SMS message" maxlength="140" name="sms_message"></textarea>
                                                                            </div>
                                                                        </div>
{{--                                                                    </div>--}}
                                                                    <!--- placeholders -->

                                                                    {{--<a href="#create-placeholder" class="btn btn-primary btn-md adv_cust_mod_btn pull-right" data-toggle="modal" data-href="#create-placeholder"  >Add New Placeholder</a>--}}
                                                                    <br>
                                                                    <div class="row">
                                                                        <div class="col-md-12 col-sm-12">
                                                                            <button type="button" class="btn btn-info  button-rounded" id="placeholder_show_hide">Placeholders</button>
                                                                        </div>
                                                                    </div>
                                                                    <br>
                                                                    <div class="row" id="show_place_holder" style="display: none;">
                                                                        <div class="col-md-6 col-sm-6">
                                                                            <h3>Template Placeholders</h3>
                                                                            <hr>
                                                                            <div id="placeholder_append"></div>
                                                                        </div>
                                                                        <div class="col-md-6 col-sm-6">
                                                                            <h3>Standard Placeholders</h3>
                                                                            <hr>
                                                                            <div id="standard_placeholder"></div>
                                                                        </div>
                                                                    </div>
                                                                    <br>


                                                                    <button type="button" class="btn btn-success  button-rounded pull-right system-function-btn" id="system-function-save-btn" style="padding: 5px 25px; margin-bottom: 20px;">Save</button>

                                                                    <ul class="pager wizard pager_a_cursor_pointer template-tab d-none">
                                                                        <li class="previous previous_btn1"><a>Previous</a></li>
                                                                        <li class="next next_btn1"><a>Next</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div class="tab-pane template-tab tabs-active d-none" id="distribution-list">
                                                                    <h3>Distribution List
                                                                        Group </h3>
                                                                    <hr>
                                                                    <div class="left_align custom-controls-stacked" id="distribution_list_group_id"></div>
                                                                    <br>
                                                                    <ul class="pager wizard pager_a_cursor_pointer">
                                                                        <li class="previous previous_btn2"><a>Previous</a></li>
                                                                        <li class="next next_btn2 groups-checkboxes"><a>Next</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div class="tab-pane template-tab tabs-active d-none" id="additional-contacts">
                                                                    <div class="row">
                                                                        <div class="col-md-6 col-sm-6">
                                                                            <h3>Group Members</h3>
                                                                            <hr>
                                                                            <ul id="selected-groups-users"></ul>
                                                                            <input type="hidden" name="group_membrs_data" id="group_membrs_data" >
                                                                        </div>
                                                                        <div class="col-md-6 col-sm-6">

                                                                            <h3>Non Group Members</h3>
                                                                            <hr>
                                                                            <div id="non-selected-users"></div>
                                                                     

                                                                        </div>
                                                                    </div>
                                                                    <br>
                                                                    <ul class="pager wizard pager_a_cursor_pointer">
                                                                        <li class="previous previous_btn3"><a>Previous</a></li>
                                                                        <li class="next next_btn3 "><a>Next</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div class="tab-pane template-tab tabs-active d-none" id="customer-lists">
                                                                    <h3>Customer Lists </h3>
                                                                    <hr>
                                                                    <h4>Choose criteria to message the customer list</h4>
                                                                    <br>

                                                                    <h5>Select locker blocks to send message too</h5>
                                                                    <select class="form-control location-multi-select" multiple id="multi_select4" name="blocks[]">
                                                                        <option disabled selected>Choose a Location</option>

                                                                    </select>

                                                                    <br>

                                                                    <div class="left_align">
                                                                        <div class="checkbox">
                                                                            <label  >
                                                                                <input type="checkbox" value="current" name="currentYear" id="currentYear">
                                                                                <span class="cr"><i class="cr-icon fa fa-check"></i></span><span id="this_year"></span>
                                                                            </label>
                                                                        </div>
                                                                        <div class="checkbox">
                                                                            <label>
                                                                                <input type="checkbox" value="previouse" name="previouseYear" id="previouseYear">
                                                                                <span class="cr"><i class="cr-icon fa fa-check"></i></span><span id="last_year"></span>
                                                                            </label>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row m-b-20">
                                                                        <div class="col-lg-6 input_field_sections">
                                                                            <h5>Refine date based on rental period</h5>
                                                                            <form>
                                                                                <div class="input-group input-group-prepend">
                                                                                        <span class="input-group-text border-right-0 rounded-left">
                                                                                            <i class="fa fa-calendar"></i>
                                                                                        </span>
                                                                                    <input type="text" name="reservation" id="reservation"
                                                                                           class="form-control datepicker" value="">
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>

                                                                    <br>
                                                                    <ul class="pager wizard pager_a_cursor_pointer">
                                                                        <li class="previous previous_btn4"><a>Previous</a></li>
                                                                        <li class="next next_btn4" id="customer-list"><a>Next</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div class="tab-pane template-tab tabs-active d-none" id="customer-list-activation">
                                                                    <h3>Selected Customers</h3>
                                                                    <hr>
                                                                    <br>

                                                                    <div id="rented-users">
                                                                        <p>Choose Criteria!</p>
                                                                    </div>
                                                                    <input type="hidden" name="filtered-users" id="filtered-users">
                                                                    <br>

                                                                    <ul class="pager wizard pager_a_cursor_pointer">
                                                                        <li class="previous previous_btn5"><a>Previous</a></li>
                                                                        <button type="submit" class="btn btn-success  button-rounded pull-right" id="save-btn-edit"> Finish
                                                                        </button>

                                                                        {{--<li class="next"></li>--}}
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
    <!-- END Edit modal-->
@endsection
@section('script')
    <!--plugin scripts-->
    <script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/autosize/js/jquery.autosize.min.js"></script>

    <script type="text/javascript" src="/backend/vendors/jquery.uniform/js/jquery.uniform.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputlimiter/js/jquery.inputlimiter.js"></script>
    <script type="text/javascript" src="/backend/vendors/chosen/js/chosen.jquery.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/jquery-tagsinput/js/jquery.tagsinput.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.validVal.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/moment/js/moment.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/daterangepicker/js/daterangepicker.js"></script>
    <script type="text/javascript" src="/backend/vendors/datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/autosize/js/jquery.autosize.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputmask/js/inputmask.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputmask/js/jquery.inputmask.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputmask/js/inputmask.date.extensions.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputmask/js/inputmask.extensions.js"></script>
    <script type="text/javascript" src="/backend/vendors/multiselect/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.quicksearch/2.3.1/jquery.quicksearch.min.js"></script>


    <script type="text/javascript" src="/backend/vendors/chosen/js/chosen.jquery.js"></script>

    <!-- text editors scripts-->
    <script type="text/javascript" src="/backend/vendors/tinymce/js/tinymce.min.js"></script>
    {{--<script type="text/javascript" src="/backend/vendors/ckeditor/ckeditor.js"></script>--}}
    <script type="text/javascript" src="/backend/vendors/bootstrap3-wysihtml5-bower/js/bootstrap3-wysihtml5.all.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/bootstrap3_wysihtml5.js"></script>
    <script type="text/javascript" src="/backend/vendors/summernote/js/summernote.min.js"></script>
    <!--wizard scripts-->
    <script type="text/javascript" src="/backend/vendors/bootstrapvalidator/js/bootstrapValidator.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/twitter-bootstrap-wizard/js/jquery.bootstrap.wizard.min.js"></script>

    <script type="text/javascript" src="/backend/js/pages/wizard.js"></script>
    <script type="text/javascript" src="/backend/vendors/raphael/js/raphael.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/Buttons/js/scrollto.js"></script>
    <script type="text/javascript" src="/backend/vendors/Buttons/js/buttons.js"></script>

    <script type="text/javascript" src="/backend/vendors/datetimepicker/js/DateTimePicker.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/clockpicker/js/jquery-clockpicker.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>
    <!--End of plugin scripts-->
    <!--Page level scripts-->
    <script src="{{ asset('custom/general.js') }}"></script>

    <script type="text/javascript" src="/backend/js/form.js"></script>
    <script type="text/javascript" src="/backend/js/pages/form_editors.js"></script>
    <script type="text/javascript" src="/backend/js/pages/form_elements.js"></script>
{{--    <script type="text/javascript" src="/backend/js/pages/radio_checkbox.js"></script>--}}
    <script type="text/javascript" src="/backend/js/pages/datetime_piker.js"></script>


<script>

    function destroyEditors(editor)
    {
        CKEDITOR.instances[editor].destroy()
    }

    var matchListUL = document.getElementById('non-selected-users');
    matchListUL.addEventListener('click', function(e) {
        e.preventDefault();
        var url = e.target.href;
        
            var template_id = $('#template_id').val();
            var distribution_groups = [];
            $('.checkboxes:checked').each(function(i){
                distribution_groups[i] = $(this).val();
            });
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {template_id: template_id ,distribution_groups: distribution_groups , _token: CSRF_TOKEN},
        
            success: function (data) {

                var users = '';
                $.each(data.data.non_group_users.data , function (index , user) {

                    users+='<label class="custom-control custom-checkbox">';
                    users+='<input type="checkbox" class="custom-control-input checkboxes" name="non_group_users[]" value="'+ user.id +'">';
                    users+='<span class="custom-control-label"></span>';
                    users+='<span class="custom-control-description">' + user.email + ' (' + user.name + ')</span>';
                    users+='</label>';

                });

                users += data.paginate;
                $('#selected-groups-users').html('');
                $('#non-selected-users').html(users);
              


            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            }
        });
    });
</script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#placeholder_show_hide").click(function () {
                $('#show_place_holder').slideToggle();
            });
        });
        $('body').on('click','.edit-btn',function(e){

            $('.template-tab').addClass('d-none');
            $('#template_name_title').html('');
            var template_id = $(this).data('id');
            // $("#edit-template")[0].reset();
            $("#multi_select4").html('');
            $("#email_template_alt_dropdown").html('');
            $("#currentYear").prop("checked", false);
            $("#previouseYear").prop("checked", false);
            $('#reservation').val('');
            $('#rented-users').html('Choose Criteria From Customer Lists!');
            $('#filtered-users').val('');
            $('textarea#limiter').val('');
            $('.tabs-active').removeClass('active show');
            $('.system-function-btn').addClass('d-none');
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                url: '/admin/getemailtemplate',
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {template_id: template_id , _token: CSRF_TOKEN},
                success: function (data) {

                    console.log(data);
                    if(data.emailtemplate.system_function == 1){
                        $('#email_template_tab').addClass('active show');
                        $('#email-template').addClass('active show');
                        $('.template-tab').addClass('d-none');
                        $('#template_name_title').html('<i class="fa fa-pencil circle"></i > Edit '+ data.emailtemplate.template_name);
                        $('textarea#limiter').val(data.emailtemplate.sms_message);
                        $('#template_id').val(data.emailtemplate.id);
                        var text ='';
                        text+='<select class="form-control" id="email_template_alt" name="email_template_alt" >';
                        text+='<option disabled selected>Choose Email Template Alt</option>';
                        text+='<option value="maim_template">Main Template</option>';
                        $.each(data.emailTemplateAlts,function (index , emailTemplateAlt) {
                            text+='<option value="'+emailTemplateAlt.id+'">'+emailTemplateAlt.tab_name+'</option>';
                        });
                        $('#email_template_alt_dropdown').html(text);

                        text+='</select>';
                        $('#template_name').val(data.emailtemplate.template_name);
                        $('#subject').val(data.emailtemplate.subject);
                        $('#main_template').val(data.emailtemplate.template_text);
                        var content = tinymce.get("tinymce_full").setContent(data.emailtemplate.template_text);
                        // alert(content);
                        // $('.template_text').text(data.emailtemplate.template_text);

                        var placeholders = '';
                        placeholders+='<ul class="list-group" style="margin-left: 20px">';
                        $.each(data.email_template_placeholders , function (index , val) {
                            placeholders+='<li class="input-group">'+ val.placeholder +'</li>';
                        });
                        placeholders+='</ul>';
                        $('#placeholder_append').html(placeholders);
                        $('#email_template_id').val(data.emailtemplate.id);

                        var standard_placeholders = '';
                        standard_placeholders+='<ul class="list-group" style="margin-left: 20px">';
                        $.each(data.standard_placeholders , function (index , val) {
                            standard_placeholders+='<li class="input-group">'+ val.placeholder +'</li>';
                        });
                        standard_placeholders+='</ul>';
                        $('#standard_placeholder').html(standard_placeholders);
                        $('.system-function-btn').removeClass('d-none');
                    }else {
                        $('.template-tab').removeClass('d-none');
                        $('#email_template_tab').addClass('active show');
                        $('#email-template').addClass('active show');

                        $('#template_name_title').html('<i class="fa fa-pencil circle"></i> Edit ' + data.emailtemplate.template_name);
                        $('#template_id').val(data.emailtemplate.id);
                        $('#template_name').val(data.emailtemplate.template_name);
                        $('#subject').val(data.emailtemplate.subject);
                        tinymce.get("tinymce_full").setContent(data.emailtemplate.template_text);


                        var placeholders = '';
                        placeholders += '<ul class="list-group" style="margin-left: 20px">';
                        $.each(data.email_template_placeholders, function (index, val) {
                            placeholders += '<li class="input-group">' + val.placeholder + '</li>';
                        });
                        placeholders += '</ul>';

                        $('#placeholder_append').html(placeholders);
                        $('#email_template_id').val(data.emailtemplate.id);

                        var standard_placeholders = '';
                        standard_placeholders += '<ul class="list-group" style="margin-left: 20px">';
                        $.each(data.standard_placeholders, function (index, val) {
                            standard_placeholders += '<li class="input-group">' + val.placeholder + '</li>';
                        });
                        standard_placeholders += '</ul>';
                        $('#standard_placeholder').html(standard_placeholders);

                        var dist_list_groups = '';
                        $.each(data.distributionLists, function (index, val) {

                            dist_list_groups += '<label class="custom-control custom-checkbox">';
                            dist_list_groups += '<input type="checkbox" class="custom-control-input checkboxes" name="distribution_list_group_id[]" value="' + val.id + '">';
                            dist_list_groups += '<span class="custom-control-label"></span>';
                            dist_list_groups += '<span class="custom-control-description">' + val.name + '</span>';
                            dist_list_groups += '</label>';

                        });
                        $('#distribution_list_group_id').html(dist_list_groups);

                        var locations = '';


                        $.each(data.locations, function (index, location) {
                            if (location.name == null) {

                            } else {
                                locations += '<option value="' + location.id + '">' + location.name + '</option>';
                            }
                        });
                        $("#multi_select4").append(locations).multiSelect("refresh");

                        $("#this_year").text('Lockers rented this year (' + data.this_start_month + ' - ' + data.this_end_month + ')');
                        $("#last_year").text('Lockers rented this year (' + data.last_start_month + ' - ' + data.last_end_month + ')');
                        // $("#this_end_month").text('Lockers rented last year ('+data.this_end_month+')');
                    }
                    $(tinymce.get('tinymce_full').getBody()).find('td').attr('style','border:1px solid lightgray !important;padding:7px;');
                    $(tinymce.get('tinymce_full').getBody()).find('div').attr('style','line-height:1.5rem;');
                    $(tinymce.get('tinymce_full').getBody()).find('p').attr('style','line-height:1.5rem;');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                }
            });

        });

        $('body').on('change','#email_template_alt',function(e){

            var email_template_alt_id = $('#email_template_alt').val();
            if(email_template_alt_id == 'maim_template'){
                var main_temp = $('#main_template').val();
                tinymce.get("tinymce_full").setContent(main_temp);

                // $('#template_text').data("wysihtml5").editor.setValue(main_temp);

            }else {

                // alert(email_template_alt_id);

                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                $.ajax({
                    url: '/admin/getEmailtemplateAlt',
                    type: 'POST',
                    dataType: 'JSON',
                    cache: false,
                    data: {email_template_alt_id: email_template_alt_id, _token: CSRF_TOKEN},
                    success: function (data) {

                        // $('#template_text').data("wysihtml5").editor.setValue(data.text);
                        tinymce.get("tinymce_full").setContent(data.text);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR);
                        // $('.system-function-btn').prop('disabled', false);
                    }
                });
            }

        });

        $('body').on('click','#system-function-btn',function(e){

            var template_id = $('#template_id').val();
            var template_name =$('#template_name').val();
            var subject =$('#subject').val();
            var template_text =  tinymce.get("tinymce_full").getContent();
            // var template_text = $('#template_text').data("wysihtml5").editor.getValue();

            // var template_text = $('#template_text').contents().find('.wysihtml5-editor').text();
            // alert(template_id);
            // alert(template_name);
            // alert(subject);
            // alert(template_text);
            $('.system-function-btn').prop('disabled', true);
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                url: '/admin/sendtestemail',
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {template_id: template_id ,template_name: template_name,subject: subject,template_text: template_text, _token: CSRF_TOKEN},
                success: function (data) {
                    console.log(data);
                    if(data == 'success'){
                        // alert('email send successfully');

                        // $('#edit').modal('toggle');
                        $('.system-function-btn').prop('disabled', false);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    $('.system-function-btn').prop('disabled', false);
                }
            });

        });

        $('body').on('click','#system-function-save-btn',function(e){

            var template_id = $('#template_id').val();
            var template_name =$('#template_name').val();
            var subject =$('#subject').val();
            // var template_text = $('#template_text').data("wysihtml5").editor.getValue();
            var template_text =  tinymce.get("tinymce_full").getContent();
            // var template_text = $('#template_text').contents().find('.wysihtml5-editor').text();
            // alert(template_id);
            // alert(template_name);
            // alert(subject);
            // alert(template_text);
            $('.system-function-btn').prop('disabled', true);
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                url: '/admin/updateSystemFunction',
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {template_id: template_id ,template_name: template_name,subject: subject,template_text: template_text, _token: CSRF_TOKEN},
                success: function (data) {
                    console.log(data);
                    if(data == 'success'){
                        // alert('email send successfully');

                        $('#edit').modal('toggle');
                        $('.system-function-btn').prop('disabled', false);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    $('.system-function-btn').prop('disabled', false);
                }
            });

        });

        <!--groups checkboxes append users in User tab  -->
        $('body').on('click','.groups-checkboxes',function(e){

            var template_id = $('#template_id').val();
            var distribution_groups = [];
            $('.checkboxes:checked').each(function(i){
                distribution_groups[i] = $(this).val();
            });
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                url: '/admin/templatedistributionusers',
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {template_id: template_id ,distribution_groups: distribution_groups , _token: CSRF_TOKEN},
                success: function (data) {


                    if(data.data.status == 'selected') {

                        var groups_users = '';
                        $.each(data.data.groups_users, function (index, user) {
                            groups_users += '<li class="selected-group-members" data-id="' + user.id + '" >' + user.email + ' (' + user.name + ')</li><br>';

                        });
                        $('#selected-groups-users').html(groups_users);
                        $('input:hidden[name=group_membrs_data]').val(data.data.groups_users_id1);

                        var users = '';
                        $.each(data.data.non_group_users.data , function (index , user) {

                            users+='<label class="custom-control custom-checkbox">';
                            users+='<input type="checkbox" class="custom-control-input checkboxes" name="non_group_users[]" value="'+ user.id +'">';
                            users+='<span class="custom-control-label"></span>';
                            users+='<span class="custom-control-description">' + user.email + ' (' + user.name + ')</span>';
                            users+='</label>';

                        });
                     
                        $('#non-selected-users').html(users);

                    }else{

                        var users = '';
                        $.each(data.data.non_group_users.data , function (index , user) {

                            users+='<label class="custom-control custom-checkbox">';
                            users+='<input type="checkbox" class="custom-control-input checkboxes" name="non_group_users[]" value="'+ user.id +'">';
                            users+='<span class="custom-control-label"></span>';
                            users+='<span class="custom-control-description">' + user.email + ' (' + user.name + ')</span>';
                            users+='</label>';

                        });
                        users += data.paginate + '';
                        $('#selected-groups-users').html('');
                        $('#non-selected-users').html(users);

                    }


                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                }
            });

        });

        <!--groups checkboxes append users in User tab  -->
        $('body').on('click','#customer-list',function(e){

            var blocks = $('#multi_select4').val();
            if($('#currentYear').is(":checked")){
                var currentYear = 'current';
            }
            if($('#previouseYear').is(":checked")){
                var previouseYear = 'previouse';
            }
            var reservationDate = $('#reservation').val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                url: '/admin/getsales',
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {blocks: blocks, currentYear: currentYear, previouseYear: previouseYear, reservationDate: reservationDate, _token: CSRF_TOKEN},
                success: function (data) {
                    console.log(data);
                    if(data.status == 'success') {
                        var users = '';
                        var  users_email= [];
                        users += '<ul>';
                        $.each(data.sales, function (index, user) {
                            users += '<li>' + user.email + '</li><br>';
                            users_email.push( user.email );
                        });
                        users += '</ul>';
                        $('#rented-users').html('');
                        $('#rented-users').html(users);
                        $('#filtered-users').val(users_email);

                        if (data.sales.length == 0) {
                            $('#rented-users').html('<p> No Customer Available!!</p>');
                        }
                    }else{
                        $('#rented-users').html('<p> Choose Criteria From Customer Lists!</p>');
                    }


                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                }
            });

        });
    </script>
@endsection