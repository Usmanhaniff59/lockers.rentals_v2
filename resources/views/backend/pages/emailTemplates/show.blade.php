@extends('backend.layouts.app')

@section('title', 'Lockers | View Post')
@section('style')
    <!--plugin styles-->
    <link type="text/css" rel="stylesheet" href="/backend/vendors/radio_css/css/radiobox.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/checkbox_css/css/checkbox.min.css" />
    <!--End of Plugin styles-->

    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/radio_checkbox.css" />
    <!-- end of plugin styles -->
@endsection
@section('content')

    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        {!! Form::open(['method' => 'DELETE', 'route' => ['emailtemplates.destroy', $email_template->id] ]) !!}
                        <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
                        @can('Delete EmailTemplate')
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                        @endcan
                        {!! Form::close() !!}
                        <h4 class="m-t-5">
                            <i class="fa fa-key"></i>
                            View Email Template |
                        </h4>

                    </div>
                </div>
            </div>
        </header>
        <div class="outer">
            <div class="inner bg-container">
                <!--top section widgets-->
                <div class="row widget_countup">
                    <div class="col-12">

                        <h1>   {{ $email_template->template_name }}</h1>
                        <hr>
                        <h3>{{ $email_template->subject }}</h3>

                        <p class="lead">{!! $email_template->template_text !!}  </p>
                        <hr>

                        <a href="#create" class="btn btn-primary btn-md adv_cust_mod_btn pull-right" data-toggle="modal" data-href="#create"  >Add New Placeholder</a>

                        <h1>Placeholders </h1>
                        <hr>
                        <div id="placeholder-append">
                            @foreach($email_template_placeholders as $email_template_placeholder)

                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" >
                                    <span class="custom-control-label"></span>
                                    <span class="custom-control-description">{{ $email_template_placeholder->placeholder }}</span>
                                </label>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer bg-white pull-right" >
             {{--@can('Create Company')--}}
                <a href="{{ route('emailtemplatedistributionlist.create') }}" class="btn btn-success" role="button">Next</a>
             {{--@endcan--}}
        </div>

    </div>
    <!--- create model -->
    <div class="modal fade in display_none" id="create" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <form action=""  method="POST" id="add-new-form">
                    @csrf
                    <div class="modal-body">
                        <div class="row">

                            <div class="col-md-12">
                                <h4>Add New Placeholder</h4>
                                <input id="email_template_id" name="email_template_id" type="hidden" placeholder="Group Name"
                                       class="form-control" value="{{ $email_template->id }}">
                                <label>Placeholder</label>
                                <p>
                                    <textarea id="placeholder" name="placeholder" placeholder="Placeholder"
                                              class="form-control"></textarea></p>

                            </div>

                            <div class="col-md-12">
                                <label> Placeholder Description</label>
                                <p>
                                    <textarea id="description" name="description" placeholder="Placeholder Description"
                                             class="form-control"></textarea></p>

                        </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                        <button type="button" class="btn btn-primary" id="save-add-btn" data-resource="admin/emailtemplateplaceholder">Save changes</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <!-- END create modal-->

@endsection
@section('script')
    <!--plugin scripts-->
    <script type="text/javascript" src="/backend/js/pages/radio_checkbox.js"></script>
    <!-- end plugin scripts -->

    <script type="text/javascript" src="js/pages/sweet_alerts.js"></script>
    <script type="text/javascript">

        $('body').on('click','#save-add-btn',function(e){

            var data = $('#add-new-form').serialize();
            var email_template_id = $('#email_template_id').val();
            var placeholder = $('#placeholder').val();
            var description = $('#description').val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var resource = $(this).data("resource");

            $.ajax({
                url: '/' + resource,
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {email_template_id: email_template_id, placeholder: placeholder ,description: description , _token: CSRF_TOKEN},
                success: function (data) {

                    var str='';
                    str+='<label class="custom-control custom-checkbox">';
                    str+='<input type="checkbox" class="custom-control-input" >';
                    str+='<span class="custom-control-label"></span>';
                    str+='<span class="custom-control-description">'+data.placeholder+'</span>';
                    str+='</label>';

                    $('#placeholder-append').append(str);
                    $('#create').modal('hide');

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                }
            });

        });

    </script>
@endsection