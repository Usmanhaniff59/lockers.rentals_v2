@extends('backend.layouts.app')

@section('title', 'Lockers | Users')
@section('style')
    <!--plugin styles-->
    <link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css" />

    <link type="text/css" rel="stylesheet" href="/backend/vendors/inputlimiter/css/jquery.inputlimiter.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/chosen/css/chosen.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/jquery-tagsinput/css/jquery.tagsinput.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/daterangepicker/css/daterangepicker.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datepicker/css/bootstrap-datepicker.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-switch/css/bootstrap-switch.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/multiselect/css/multi-select.css"/>

    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-switch/css/bootstrap-switch.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/switchery/css/switchery.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/radio_css/css/radiobox.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/checkbox_css/css/checkbox.min.css" />
    <!--End of Plugin styles-->
    <!--Page level styles-->

    <!-- end of plugin styles -->
    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/form_elements.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/radio_checkbox.css" />

    <link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css" />
    <style>

        .page-link{
            padding: 16px;
            color: #525252;
            text-decoration: none;
            background-color: transparent;
        }
        .header_icon_margin{
            margin-right: 10px !important;
        }
        .header_icon_margin_left{
            margin-left: 10px !important;
        }

    </style>
@endsection
@section('content')
    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-money circle header_icon_margin"></i>
                            Lost Booking
                        </h4>
                    </div>
                </div>
            </div>
        </header>

        @include('errors.success')
        @include('errors.error')

        @if(Session::has('flash_message'))
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable float-right" style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-hidden="true" style="margin-top: 4px; margin-left: 15px;"> ×
                    </button>
                    {{ Session::get('flash_message') }}
                </div>
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="outer">
            <div class="inner bg-container">
                <div class="row">
                    <div class="col-12 data_tables">

                        <div class="card m-t-35">
                            <div class="card-header bg-white">
                                <i class="fa fa-money circle header_icon_margin"></i> All Lost Booking

                            </div>
                            <div class="card-body p-t-10">
                                <div class=" m-t-25">
                                    <table class="table table-striped table-bordered table-hover " id="sample_6">
                                        <thead>
                                        <tr>
                                            {{--<th>#</th>--}}
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>User Roles</th>
                                            <th>Operations</th>
                                        </tr>
                                        </thead>

                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{--</div>--}}
            </div>
        </div>


    </div>


    </div>
@endsection
@section('script')
    <!--plugin scripts-->
    <script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <!-- end of plugin scripts -->

    <script type="text/javascript" src="/backend/vendors/jquery.uniform/js/jquery.uniform.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputlimiter/js/jquery.inputlimiter.js"></script>
    <script type="text/javascript" src="/backend/vendors/chosen/js/chosen.jquery.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/jquery-tagsinput/js/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.validVal.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/moment/js/moment.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/daterangepicker/js/daterangepicker.js"></script>
    <script type="text/javascript" src="/backend/vendors/datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/autosize/js/jquery.autosize.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputmask/js/inputmask.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputmask/js/jquery.inputmask.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputmask/js/inputmask.date.extensions.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputmask/js/inputmask.extensions.js"></script>
    <script type="text/javascript" src="/backend/vendors/multiselect/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.quicksearch/2.3.1/jquery.quicksearch.min.js"></script>

    <script type="text/javascript" src="/backend/vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>
    <!--End of plugin scripts-->
    <!--Page level scripts-->

    <!--end of plugin scripts-->
    <!--Page level scripts-->
    <script type="text/javascript" src="/backend/js/pages/radio_checkbox.js"></script>
    <script type="text/javascript" src="/backend/js/form.js"></script>
    <script type="text/javascript" src="/backend/js/pages/form_elements.js"></script>
    <script type="text/javascript" src="/backend/js/pages/datatable_server_side.js"></script>
    <script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>


    {{--            <script type="text/javascript" src="/backend/js/pages/radio_checkbox.js"></script>--}}

    <script type="text/javascript" src="/backend/js/common.js"></script>
    <!-- end of global scripts-->

    <script type="text/javascript">

        // function reset_form(form_id) {
        //
        //     $("#user-save")[0].reset();
        //
        // }



        {{--var user_table =  userEntryTable();--}}

        {{--function userEntryTable() {--}}


            {{--return $('#sample_6').DataTable({--}}
                {{--processing: true,--}}
                {{--serverSide: true,--}}
                {{--searching: true,--}}

                {{--"order":[],--}}
                {{--"lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],--}}
                {{--ajax: '<?php echo route('users.show.all'); ?>',--}}
                {{--columns: [--}}
                    {{--// {data: 'id', name: 'id'},--}}
                    {{--{data: 'name', name: 'name'},--}}
                    {{--{data: 'email', name: 'email'},--}}
                    {{--{data: 'roles', name: 'roles'},--}}
                    {{--{data: 'action', name: 'action', orderable: false, searchable: true}--}}
                {{--]--}}
            {{--});--}}
        {{--}--}}

        {{--$('body').on('click','.edit-btn',function(e){--}}

            {{--var user_id = $(this).data('id');--}}
            {{--var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');--}}

            {{--$.ajax({--}}
                {{--url: '/admin/getuserroles',--}}
                {{--type: 'POST',--}}
                {{--dataType: 'JSON',--}}
                {{--cache: false,--}}
                {{--data: {user_id: user_id, _token: CSRF_TOKEN},--}}
                {{--success: function (data) {--}}
                    {{--$('#editform_display').html(data);--}}
                {{--},--}}
                {{--error: function (jqXHR, textStatus, errorThrown) {--}}
                    {{--console.log(jqXHR);--}}
                {{--}--}}
            {{--});--}}

        {{--});--}}

        {{--$('body').on('click','.create-new-franchisee-group-button',function(e){--}}

            {{--$('.new-franchisee-group-input').removeClass('d-none');--}}
            {{--$('.existed-franchisee-group').addClass('d-none');--}}

            {{--$('.existed-franchisee-group-button-field').removeClass('d-none');--}}
            {{--$('.new-franchisee-group-button-field').addClass('d-none');--}}

        {{--});--}}

        {{--$('body').on('click','.existed-franchisee-group-button',function(e){--}}

            {{--$('.existed-franchisee-group').removeClass('d-none');--}}
            {{--$('.new-franchisee-group-input').addClass('d-none');--}}

            {{--$('.new-franchisee-group-button-field').removeClass('d-none');--}}
            {{--$('.existed-franchisee-group-button-field').addClass('d-none');--}}


        {{--});--}}

        {{--var role_access = "{{auth()->user()->can('Edit User')}}";--}}

        {{--if(role_access.length >0){--}}
            {{--var roles ="{{ count($roles) }}";--}}

            {{--for(var i =0 ; i< roles ; i++){--}}
                {{--new Switchery(document.querySelector('.sm_toggle_add'+i), { size: 'small', color: '#00c0ef', jackColor: '#fff' });--}}

            {{--}--}}
        {{--}--}}


    </script>
@endsection