<style>
    @foreach($childrens as $index=>$children)

        .styled-radio{{++$index}} {
        margin: 5px;
        padding-top: 5px;
        width: 75px;
        height: 100px;
        background: #94AACF;
        border: 5px solid #559DE4;
        border-radius: 10px;
        font-weight: bold;
        font-size: x-large;
        line-height: 40px;
        /*opacity:0.7;*/
        /*filter:alpha(opacity=70);*/

    }

    .styled-radio-red{{$index}} {
        margin: 5px;
        padding-top: 5px;
        width: 75px;
        height: 100px;
        background: #B5040C;
        border: 5px solid #ffb2b2;
        border-radius: 10px;
        font-weight: bold;
        font-size: x-large;
        line-height: 40px;
        /*opacity:0.7;*/
        /*filter:alpha(opacity=70);*/
        background-color: #B5040C;

    }
    .styled-radio-gray{{$index}} {
        margin: 5px;
        padding-top: 5px;
        width: 75px;
        height: 100px;
        background: gray;
        border: 5px solid #9d9d9d ;
        border-radius: 10px;
        font-weight: bold;
        font-size: x-large;
        line-height: 40px;
        /*opacity:0.7;*/
        /*filter:alpha(opacity=70);*/
        /*background-color: #00c0ef;*/

    }



    .gray{{$index}} { background: gray; }

    .invisible-radio{{$index}} {
        /* per https://a11yproject.com/posts/how-to-hide-content/ */
        position: absolute;
        height: 1px;
        width: 1px;
        overflow: hidden;
        clip: rect(1px 1px 1px 1px); /* IE6, IE7 */
        clip: rect(1px, 1px, 1px, 1px);
    }

    .invisible-radio{{$index}}:checked + label .styled-radio{{$index}}::after {

        font-size: 3em;
        display: inline-block;
        box-sizing: border-box;
        padding: 20px;
        width: 150px;
        text-align: center;
    }

    .invisible-radio{{$index}}:not(:checked) + label {
        cursor: pointer;
    }

    .invisible-radio{{$index}}:not(:checked) + label:hover .styled-radio{{$index}} {
        opacity: 0.7;
        background: #00A1FF;
        border: 5px solid #559DE4;
    }
    .invisible-radio{{$index}}:not(:checked) + label:hover .styled-radio{{$index}} .icon-tick {
        padding: 4px;
        border-radius: 100%;
        background-color: #559DE4;

    }

    .invisible-radio{{$index}}:checked + label .styled-radio{{$index}} {
        transform: scale(1, 1);
        background: #9CBBBD!important;
        border: 5px solid #66ff66;
        animation: pulsate 1s alternate ease-in-out infinite;
    }
    .invisible-radio{{$index}}:focus + label .styled-radio{{$index}} .icon-tick {
        padding: 4px;
        border-radius: 100%;
        background-color: #66ff66;
        opacity: 1;
        visibility: visible;
        /*background: white;*/
    }
    @endforeach
            .icon-tick{
        padding: 4px;
        border-radius: 100%;
        background-color: #559DE4;
        opacity: 0.6;
        visibility: hidden;
    }
    .icon-cross{
        padding: 4px 7px;
        border-radius: 100%;
        background-color: red;
        opacity: 0.3;
    }

    i.red-circle {
        display: inline-block;
        border-radius: 100%;
        box-shadow: 0px 0px 2px #B5040C;
        padding: 0.5em 0.6em;
        background-color: #B5040C;
        color: white;


    }

    i.green-circle {
        display: inline-block;
        border-radius: 100%;
        box-shadow: 0px 0px 2px #9CBBBD;
        padding: 0.5em 0.6em;
        background-color: #9CBBBD;
        color: #9CBBBD;
    }

    i.blue-circle {
        display: inline-block;
        border-radius: 100%;
        box-shadow: 0px 0px 2px #66ff66;
        padding: 0.5em 0.6em;
        background-color: #66ff66;
        color: white;
    }
    .child-name{
        color: white;
    }

    .nav-tabs .nav-link, .nav-tabs .nav-item .nav-link {
        color: white;
        background-color: #B5040C;
        border-color: #dee2e6 #dee2e6 #e9e9e9;
    }
    .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
        border: 1px solid transparent;
        color: white;
        border-color:  #666666;
    }
</style>
<input type='hidden' class="form-control" name="daterange" value="false" />
<input type='hidden' class="form-control" id="sale_id" name="sale_id" value="{{$booking_id}}" />

<input type='hidden' class="form-control" id="child_counter" name="child_counter" value="{{$childrens_count}}" />

<div class="row">
                    <div class="form-group col-md-6">
                        <label for="voucher_id">First Name</label>
                        <input type="text" class="form-control" id="edit_pupil_premium_first_name" name="first_name" value="{{$childrens[0]->child_first_name}} ">
                        <div id="first_name_error" class="pupil_premium_error validation-error"></div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="voucher_id">Surname</label>
                        <input type="text" class="form-control" id="edit_pupil_premium_surname" name="surname" value="{{$childrens[0]->child_surname}}">
                        <div id="surname_error" class="pupil_premium_error validation-error"></div>
                    </div>
                </div>
<div class="card m-t-35">
    <div class="card-header bg-white edit-card-header">
        <ul class="nav nav-tabs card-header-tabs float-left">
            @foreach($child_blocks as $index=>$child_block)
                @if($index==0)
                    <li class="nav-item" style="margin: 20px">
                        <a class="nav-link active child-name selected-child tab-show"  href="#tab{{$index}}" data-toggle="tab" id="tab-pills{{$index}}" >{{$childrens[$index]->child_first_name}} {{$childrens[$index]->child_surname}}</a>
                    </li>
                @else
                    <li class="nav-item" style="margin: 20px">
                        <a class="nav-link child-name selected-child tab-show" href="#tab{{$index}}" data-toggle="tab" id="tab-pills{{$index}}" >{{$childrens[$index]->child_first_name}} {{$childrens[$index]->child_surname}}</a>
                    </li>
                @endif
            @endforeach

        </ul>
    </div>
    <div class="card-body">
        <div class="tab-content text-justify">
            @foreach($child_blocks as $index=>$child_block)

                @if($index==0)
                    <div class="tab-pane tab-show active" id="tab{{$index}}">
                        <br>

                        <h5 id=""  style="margin-left: 25px" class="assign_class" data-id = "" data-value = "" ><b>{{isset($child_academic_year[$index]) ? $child_academic_year[$index] : '' }}</b></h5>
                        <span id=""  style="margin-left: 25px" class="assign_class"   >{{$selected_block_and_number_group[$index][0]}}</span>
                        <p id=""  style="margin-left: 25px" class="assign_class"   > Locker Number :  {{$selected_block_and_number_group[$index][1]}}</p>
                        <div id="tabValidation{{$index}}" style="color: red" class="assign_class d-none validation_text" data-id = "choose-locker-select-child-locker-validation" data-value = "{{t('choose-locker-select-child-locker-validation')}}" >{!! t('choose-locker-select-child-locker-validation')!!}</div>
                        <br>
                        <div class="assign_class d-none" style="color: red" id="locker_already_exists_msg{{$index}}" data-id = "choose-locker-already-exists-locker" data-value = "{{t('choose-locker-already-exists-locker')}}" >{!! t('choose-locker-already-exists-locker')!!}</div>

                        <div class="card" style="margin-bottom: 20px;">
                            <div class="card-body m-t-20">
                                <div class="form-group">
                                    @if($location_group == 1)

                                        <p class="assign_class" data-id = "choose-locker-child-name-date-title" data-placeholders="{{$child_name_date_placeholders}}" data-placeholderval="{{$childrens[$index]->child_first_name}} {{$childrens[$index]->child_surname}},{{Carbon\Carbon::parse($childrens[$index]->start)->format('d M, Y')}} - {{Carbon\Carbon::parse($childrens[$index]->end)->format('d M, Y')}}" data-value = "{{t('choose-locker-child-name-date-title')}}" >{!! str_replace(explode(',', $child_name_date_placeholders),[ '',Carbon\Carbon::parse($childrens[$index]->start)->format('d M, Y').' - '.Carbon\Carbon::parse($childrens[$index]->end)->format('d M, Y')],t('choose-locker-child-name-date-title')) !!} </p>

                                    @elseif($location_group == 2)
                                        {{--                                                        <p class="assign_class" data-id = "choose-locker-child-name-date-title" data-placeholders="%child_name%,%date%" data-placeholderval="{{$childrens[$index]->child_first_name}} {{$childrens[$index]->child_surname}},{{Carbon\Carbon::parse($childrens[$index]->start)->format('d-M-Y')}} - {{Carbon\Carbon::parse($childrens[$index]->end)->format('d-M-Y')}}" data-value = "{{t('choose-locker-child-name-date-title')}}" data-steps="yes" >{!!  str_replace(["%child_name%" ,"%date%"],[$childrens[$index]->child_first_name ,Carbon\Carbon::parse($childrens[$index]->start)->format('d-M-Y') - Carbon\Carbon::parse($childrens[$index]->end)->format('d-M-Y')],t('choose-locker-child-name-date-title'))  !!}</p>--}}
                                        <p class="assign_class" data-id = "choose-locker-child-name-date-title" data-placeholders="{{$child_name_date_placeholders}}" data-placeholderval="{{$childrens[$index]->child_first_name}} {{$childrens[$index]->child_surname}},{{Carbon\Carbon::parse($childrens[$index]->start)->format('d M, Y')}} - {{Carbon\Carbon::parse($childrens[$index]->end)->format('d M, Y')}}" data-value = "{{t('choose-locker-child-name-date-title')}}" >{!! str_replace(explode(',', $child_name_date_placeholders),[ '',Carbon\Carbon::parse($childrens[$index]->start)->format('d M, Y').' - '.Carbon\Carbon::parse($childrens[$index]->end)->format('d M, Y')],t('choose-locker-child-name-date-title')) !!}</p>

                                    @else
                                        <p class="assign_class" data-id = "choose-locker-child-name-date-title" data-placeholders="{{$child_name_date_placeholders}}" data-placeholderval="{{$childrens[$index]->child_first_name}} {{$childrens[$index]->child_surname}},{{Carbon\Carbon::parse($childrens[$index]->start)->format('d M, Y H:i')}} - {{Carbon\Carbon::parse($childrens[$index]->end)->format('d M, Y H:i')}}" data-value = "{{t('choose-locker-child-name-date-title')}}" >{!! str_replace(explode(',', $child_name_date_placeholders),[ '' ,Carbon\Carbon::parse($childrens[$index]->start)->format('d M, Y H:i').' - '.Carbon\Carbon::parse($childrens[$index]->end)->format('d M, Y H:i')],t('choose-locker-child-name-date-title')) !!} </p>

                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="blocks" class="assign_class" data-id = "choose-locker-select-block-title" data-value = "{{t('choose-locker-select-block-title')}}" >{!! t('choose-locker-select-block-title')!!}</label>
                                    {!! $child_block !!}
                                </div>
                                <br>
                                <div class="lockers_printing" style="text-align:left;" id="display_lockers{{$index+1}}"></div>
                                <div id="booked_block{{$index+1}}" style="color: red" class="assign_class d-none validation_text" data-id = "choose-locker-block-is-booked-msg" data-value = "{{t('choose-locker-block-is-booked-msg')}}" >{!! t('choose-locker-block-is-booked-msg')!!}</div>
                                <br>

                            </div>
                        </div>
                    </div>
                @else
                    <div class="tab-pane tab-show" id="tab{{$index}}">
                        <br>
                        <h5 id="" class="assign_class" data-id = "choose-locker-select-child-locker-validation" data-value = "{{t('choose-locker-select-child-locker-validation')}}" ><b  style="margin-left: 25px">{{$child_academic_year[$index]}}</b></h5>
                        <span id=""  style="margin-left: 25px" class="assign_class"   >{{$selected_block_and_number_group[$index][0]}} </span>
                        <p id=""  style="margin-left: 25px" class="assign_class"   > Locker Number :  {{$selected_block_and_number_group[$index][1]}}</p>

                        <div id="tabValidation{{$index}}" style="color: red" class="assign_class d-none validation_text" data-id = "choose-locker-select-child-locker-validation" data-value = "{{t('choose-locker-select-child-locker-validation')}}" >{!! t('choose-locker-select-child-locker-validation')!!}</div>

                        <div class="assign_class d-none" style="color: red" id="locker_already_exists_msg{{$index}}" data-id = "choose-locker-already-exists-locker" data-value = "{{t('choose-locker-already-exists-locker')}}" >{!! t('choose-locker-already-exists-locker')!!}</div>
                        <br>
                        <div class="card" style="margin-bottom: 20px;">
                            <div class="card-body m-t-20">
                                <div class="form-group">
                                    @if($location_group == 1)

                                        <p class="assign_class" data-id = "choose-locker-child-name-date-title" data-placeholders="{{$child_name_date_placeholders}}" data-placeholderval="{{$childrens[$index]->child_first_name}} {{$childrens[$index]->child_surname}},{{Carbon\Carbon::parse($childrens[$index]->start)->format('d M, Y')}} - {{Carbon\Carbon::parse($childrens[$index]->end)->format('d M, Y')}}" data-value = "{{t('choose-locker-child-name-date-title')}}" >{!! str_replace(explode(',', $child_name_date_placeholders),['' , Carbon\Carbon::parse($childrens[$index]->start)->format('d M, Y').' - '.Carbon\Carbon::parse($childrens[$index]->end)->format('d M, Y')],t('choose-locker-child-name-date-title')) !!} </p>

                                    @elseif($location_group == 2)
                                        {{--                                                        <p class="assign_class" data-id = "choose-locker-child-name-date-title" data-placeholders="%child_name%,%date%" data-placeholderval="{{$childrens[$index]->child_first_name}} {{$childrens[$index]->child_surname}},{{Carbon\Carbon::parse($childrens[$index]->start)->format('d-M-Y')}} - {{Carbon\Carbon::parse($childrens[$index]->end)->format('d-M-Y')}}" data-value = "{{t('choose-locker-child-name-date-title')}}" data-steps="yes" >{!!  str_replace(["%child_name%" ,"%date%"],[$childrens[$index]->child_first_name ,Carbon\Carbon::parse($childrens[$index]->start)->format('d-M-Y') - Carbon\Carbon::parse($childrens[$index]->end)->format('d-M-Y')],t('choose-locker-child-name-date-title'))  !!}</p>--}}
                                        <p class="assign_class" data-id = "choose-locker-child-name-date-title" data-placeholders="{{$child_name_date_placeholders}}" data-placeholderval="{{$childrens[$index]->child_first_name}} {{$childrens[$index]->child_surname}},{{Carbon\Carbon::parse($childrens[$index]->start)->format('d M, Y')}} - {{Carbon\Carbon::parse($childrens[$index]->end)->format('d M, Y')}}" data-value = "{{t('choose-locker-child-name-date-title')}}" >{!! str_replace(explode(',', $child_name_date_placeholders),['' ,Carbon\Carbon::parse($childrens[$index]->start)->format('d M, Y').' - '.Carbon\Carbon::parse($childrens[$index]->end)->format('d M, Y')],t('choose-locker-child-name-date-title')) !!}</p>

                                    @else
                                        <p class="assign_class" data-id = "choose-locker-child-name-date-title" data-placeholders="{{$child_name_date_placeholders}}" data-placeholderval="{{$childrens[$index]->child_first_name}} {{$childrens[$index]->child_surname}},{{Carbon\Carbon::parse($childrens[$index]->start)->format('d M, Y H:i')}} - {{Carbon\Carbon::parse($childrens[$index]->end)->format('d M, Y H:i')}}" data-value = "{{t('choose-locker-child-name-date-title')}}" >{!! str_replace(explode(',', $child_name_date_placeholders),['' ,Carbon\Carbon::parse($childrens[$index]->start)->format('d M, Y H:i').' - '.Carbon\Carbon::parse($childrens[$index]->end)->format('d M, Y H:i')],t('choose-locker-child-name-date-title')) !!} </p>

                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="blocks" class="assign_class" data-id = "choose-locker-select-block-title" data-value = "{{t('choose-locker-select-block-title')}}" >{!! t('choose-locker-select-block-title')!!}</label>
                                    {!! $child_block !!}
                                </div>
                                <br>
                                <div class="lockers_printing" style="text-align:left;" id="display_lockers{{$index+1}}"></div>
                                <div id="booked_block{{$index+1}}" style="color: red" class="assign_class d-none validation_text" data-id = "choose-locker-block-is-booked-msg" data-value = "{{t('choose-locker-block-is-booked-msg')}}" >{!! t('choose-locker-block-is-booked-msg')!!}</div>
                                <br>
                            </div>
                        </div>
                    </div>
                @endif

            @endforeach
        </div>
    </div>
</div>