@extends('backend.layouts.app')

@section('title', 'Lockers | Transactions')
@section('style')
    <!--plugin styles-->
    <link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-switch/css/bootstrap-switch.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/switchery/css/switchery.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/radio_css/css/radiobox.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/checkbox_css/css/checkbox.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/modal/css/component.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-tagsinput/css/bootstrap-tagsinput.css"/>
    <link rel="stylesheet" type="text/css" href="/backend/vendors/animate/css/animate.min.css" />
    <!-- end of plugin styles -->

    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/radio_checkbox.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css" />

    <style>
        .edit-card-header{
            border-bottom: 1px solid #00C0EF;
        }
        .table thead th{
            vertical-align: top;
        }
    </style>

@endsection
@section('content')
    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-money circle"></i>
                            Booking Management
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        @if(Session::has('flash_message'))
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable float-right" style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-hidden="true" style="margin-top: 4px; margin-left: 15px;"> ×
                    </button>
                    {{ Session::get('flash_message') }}
                </div>
            </div>
        @endif
        <div class="outer">
            <div class="inner bg-container">
                <div class="row">
                    @include ('errors.list')
                    <div class="col-12 data_tables">
                        <div class="card m-t-35">
                            <div class="card-header bg-white">
                                <i class="fa fa-money circle"></i> All Bookings
                                {{--@can('Create Company')--}}
                                <a href="{{route('payment.index')}}" class="btn btn-warning pull-right" >Back</a>                                {{--@endcan--}}
                            </div>
                            <div class="card-body p-t-10">
                                <div class=" m-t-25">
                                    <table class="table table-striped table-bordered table-hover " id="sample_6">
                                        <thead>
                                        <tr>
                                            <th>Location</th>
                                            <th>Locker Number</th>
                                            <th>Name</th>
                                            <th>Code</th>
                                            <th>Start</th>
                                            <th>End</th>
                                            <th>Status</th>
                                            <th>Cost</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($transactions)
                                            @foreach ($transactions as $transaction)
                                                <tr>
                                                    <td>@if(!empty($transaction->block)){{ $transaction->block->name }} @endif</td>
                                                    <td>@if(!empty($transaction->locker)){{ $transaction->locker->locker_number }}@endif</td>
                                                    <td>{{ $transaction->child_first_name.' '.$transaction->child_surname }}</td>
                                                    <td>{{  $transaction->net_pass }}</td>
                                                    <td>{{ \Carbon\Carbon::parse($transaction->start)->format('d M, Y H:i') }}</td>
                                                    <td>{{\Carbon\Carbon::parse($transaction->end)->format('d M, Y H:i')  }}</td>
                                                    <td>@if($transaction->status == "r") {{"Reserved"}} @elseif($transaction->status == "b") {{"Booked"}} @else {{"Lost" }}@endif </td>
                                                    <td id="update.{{$transaction->sale_id }}">
                                                        @if($transaction->status == "b")
                                                            @if(auth()->user()->can('Account Balance'))
                                                                {{ $transaction->price }}
                                                            @else
                                                                N/A
                                                            @endif
                                                        @else 
                                                            {{"0.00"}}
                                                        @endif</td>
                                                    <td>
                                                        @if (auth()->user()->can('Transaction Log Order')) 
                                                        <a href="#edit" class="btn btn-warning edit-status"  data-id="{{$transaction->id}}" data-status="{{$transaction->status}}" data-toggle="modal" data-href="#edit" style="margin-right: 12px">Edit</a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        @if($charities)
                                            @foreach ($charities as $charity)
                                                <tr>
                                                    <td>Charity</td>
                                                    <td> </td>
                                                    <td>@if(isset($charity->user_id ))@if(  isset($charity->user->name) ||  isset($charity->user->surname) ) {{$charity->user->name.' '. $charity->user->surname }} @else $charity->user->username  @endif @endif</td>
                                                    <td> </td>
                                                    <td> </td>
                                                    <td> </td>
                                                    <td> </td>
                                                    <td id="update.{{$charity->sale_id }}">@if($charity->status == "b"){{ $charity->price }}@else {{"0.00"}}@endif</td>
                                                    <td></td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <!--- Price Change model -->
    <div class="modal fade in display_none" id="edit" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                {{ Form::open(array('url' => 'admin/payment/changeStatus', 'id' => 'changepriceform')) }}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <h3><i class="fa fa-money circle"></i> Status Change</h3>
                            <hr>
                            <br>
                            <div class='form-group col-md-6 type'>
                                <label for="type">Type</label>
                                <select class="form-control" id="type" name="type">
                                    <option value="" selected>Choose Status Type</option>
                                    <option value="b">Booked</option>
                                    <option value="r">Reserved</option>
                                    <option value="l">Lost</option>
                                </select>
                            </div>

                            <input type="hidden" name="booking_id" id="id_to_submit" />

                            <input type="date" name="date" placeholder="date" id="date" class="d-none col-6 form-control" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                    {{ Form::Button('Save', array('class' => 'btn btn-primary','id'=>'validate_change_price')) }}
                </div>
                {{ Form::close() }}
                <br>
            </div>
        </div>
    </div>
    <!-- END Price Change modal-->

@endsection
@section('script')
    <!--plugin scripts-->
    <script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>
    <!-- end of plugin scripts -->

    <!--Page level scripts-->
    <script type="text/javascript" src="/backend/js/pages/datatable.js"></script>
    <script type="text/javascript" src="/backend/js/pages/radio_checkbox.js"></script>
    <!-- end of global scripts-->
    <script type="text/javascript">

        $('#type').on('change', function() {
       if(this.value == "r"){
        $('#date').removeClass('d-none');
       }
        });

        $('body').on('click','#validate_change_price',function(e){
            $('#changepriceform').submit();
        });




        $('body').on('click','.edit-status',function(e){

            var id = $(this).data('id');
            var status = $(this).data('status');
            console.log(id);
            $('#id_to_submit').val(id);
            $('#type').val(status);
        });


        $('body').on('click','.edit-btn',function(e){

            var sale_id = $(this).data('id');
            var price = $(this).data('price');
            var booking_id = $(this).data('booking_id');
            var price_original = $(this).data('price_original');

            $("#edit_price_discount_form")[0].reset();
            $('#sale_price').val(price);
            $('#original_price_hidden').val(price_original);
            $('#sale_id').val(sale_id);
            $('#booking_id').val(booking_id);
            $('#price_original').val(price_original);

            // $('#check_price').attr("price_original",price_original);

        });

        $('body').on('click','#check_price',function(e){

            e.preventDefault();
            $('.validation-msg').addClass('d-none');
            var price_original = parseInt($('#original_price_hidden').val());

            var refund_price = parseInt($('#sale_price').val());

            var notes = $('#notes').val();

            if(refund_price < price_original){
                if(notes.length <=0){
                    $('.notes-validation').text('Add a note!');
                    $('.notes-validation').removeClass('d-none');
                    $('#notes').focus();
                }else {
                    $("#edit_price_discount_form").submit();
                }

            }else if(refund_price == price_original){
                $('.price-validation').text('Price is equal to Original Price');
                $('.price-validation').removeClass('d-none');
                  $('#sale_price').focus();
            }else{
                $('.price-validation').text('Price is greater then Original Price');
                $('.price-validation').removeClass('d-none');
                $('#sale_price').focus();
            }

        });

    </script>
@endsection