
<label class="custom-control custom-radio  d-inline-block "style="margin-left: 10px;">
    <input id="all_vouchers" name="payment" value="all" type="radio" class="custom-control-input payments-radio" checked>
    <span class="custom-control-label"></span>
    <span class="custom-control-description">All</span>
</label>
<label class="custom-control custom-radio  d-inline-block "  style="margin-left: 10px;">
    <input id="used_vouchers" name="payment" type="radio" value="paid" class="custom-control-input payments-radio">
    <span class="custom-control-label"></span>
    <span class="custom-control-description ">Paid </span>
</label>
<label class="custom-control custom-radio  d-inline-block " style="margin-left: 10px;">
    <input id="issued_vouchers" name="payment" type="radio" value="lost" class="custom-control-input payments-radio">
    <span class="custom-control-label"></span>
    <span class="custom-control-description ">Lost  </span>
</label>
<label class="custom-control custom-radio  d-inline-block " style="margin-left: 10px;">
    <input id="not_issued_vouchers" name="payment" type="radio" value="reserved" class="custom-control-input payments-radio">
    <span class="custom-control-label"></span>
    <span class="custom-control-description ">Reserved </span>
</label>
