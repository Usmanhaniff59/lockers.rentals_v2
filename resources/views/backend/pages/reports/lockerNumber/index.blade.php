@extends('backend.layouts.app')

@section('title', 'Lockers | Locker Report')
@section('style')
    <!--plugin styles-->
    <link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-switch/css/bootstrap-switch.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/switchery/css/switchery.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/radio_css/css/radiobox.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/checkbox_css/css/checkbox.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/modal/css/component.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-tagsinput/css/bootstrap-tagsinput.css"/>
    <link rel="stylesheet" type="text/css" href="/backend/vendors/animate/css/animate.min.css"/>
    <!-- end of plugin styles -->

    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/radio_checkbox.css"/>


    <link type="text/css" rel="stylesheet" href="/backend/vendors/chosen/css/chosen.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/form_elements.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/datepicker-jquery.css"/>


    <style>
        @media (min-width: 768px) {
            .modal-xl {
                width: 90%;
                max-width: 1200px;
            }
        }

        .flex-container {
            display: flex;
            justify-content: center;
            flex-wrap: wrap;
        }

        .flex-container label {
            margin: 15px;
        }

        .loader {
            position: absolute;
            top: 15%;
            left: 45%;
            margin: auto;
            display: none;
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #00c0ef;
            width: 150px;
            height: 150px;
            -webkit-animation: spin 2s linear infinite;
            /* Safari */
            animation: spin 2s linear infinite;
        }

        .loader-percentage {
            position: absolute;
            top: 21%;
            left: 50%;
            margin: auto;
            display: none;
        }

        /* Safari */
        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }

        .excel-loader {
            display: none;
        }
    </style>

@endsection
@section('content')
    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-building circle header_icon_margin"></i>
                            Reports
                        </h4>
                    </div>
                </div>
            </div>
        </header>


        <div class="outer">
            <div class="inner bg-container">

                <div class="loader" id="block_loader">
                </div>
                <h2 class="loader-percentage"><b id="percentage">0%</b></h2>
                <div id="lockers_div">
                    <div class="row">


                        @include ('errors.list')

                        <div class="col-12 data_tables">
                            <div class="card m-t-35">
                                <div class="card-header bg-white">
                                    <i class="fa fa-building circle header_icon_margin"></i> Locker Numbers
                                </div>

                                    <div class="row p-t-15" style="margin-left: 12px">

                                        <button type="button" class="form_submit_check btn btn-warning  ml-2"
                                                onclick="spinner_start();exportTableToExcel()">
                                        <span class="assign_class label">
                                            EXCEL EXPORT
                                        </span>
                                        </button>
                                    </div>

                                <div class="row p-t-15" style="margin-left: 12px">
                                  <!--   <div class="col-md-2"><input type="text" id="search_company" placeholder="Company"
                                                                 class="form-control"></div> -->
                                 <!--    <div class="col-md-2"><input type="text" id="search_block" placeholder="Block"
                                                                 class="form-control"></div> -->
                                    <div class="col-md-3"><input type="text" value="<?php echo date("Y-m-d");?>" 
                                                                 onchange="searchByThis(availability_reports_table)"
                                                                 id="search_start_date"
                                                                 class="form-control"
                                                                 placeholder="Date (defaults to todays Date)"></div>
                                                                 <div class="col-md-3">
                                                                     <!-- locker group <input type="text" value=""
                                                                 id="lockerGroup"
                                                                 name="lockerGroup" 
                                                                 class="form-control"
                                                                 placeholder="Locker Group">-->
                                                                     <select id="lockerGroup"
                                                                 name="lockerGroup" 
                                                                 class="form-control">
                                                                         <option>Select Group</option>
                                                                         @foreach ($BlockGroup as $blockgroup)
                                                                        <option value="{{$blockgroup->name}}">{{$blockgroup->name}}</option>
                                                                         @endforeach
                                                                     </select>
                                                                 </div>
                                     <div class="col-md-3"><input type="text" value=""
                                                                 id="lockerNumber"
                                                                 name="lockerNumber" 
                                                                 class="form-control"
                                                                 placeholder="Locker Number"></div>
                                </div>
                                {{--@endif--}}

                                <div class="row p-t-15" style="margin-left: 12px"></div>

                                <div class="card-body p-t-10">
                                    <div class="row" style="width: 100% !important;">
                                        <div class="col-md-12 m-t-25">
                                            <div class="table-responsive">
                                                <table class="table  table-bordered table-hover  " style="width:100%"
                                                       id="sample_11">
                                                    <thead>
                                                    <tr>
                                                        <!-- {{--<th>#</th>--}} -->
                                                        <th>Locker Group</th>
                                                        <th>Locker Number</th>
                                                        <th>Start Date</th>
                                                        <th>End Date</th>
                                                        <th>Code</th>
                                                    </tr>
                                                    </thead>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <!--plugin scripts-->
    <script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
    <!-- <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script> -->
    <!-- <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script> -->
    <!-- <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script> -->
    <!-- <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script> -->
    <!-- <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script> -->
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>
    <!-- end of plugin scripts -->

    <!--Page level scripts-->
    <!-- <script type="text/javascript" src="/backend/js/pages/datatable_server_side.js"></script> -->
    <script type="text/javascript" src="/backend/js/pages/radio_checkbox.js"></script>
    <!-- <script type="text/javascript" src="{{asset('/backend/js/common.js')}}"></script> -->


    <script type="text/javascript" src="/backend/vendors/chosen/js/chosen.jquery.js"></script>
    <script type="text/javascript" src="/backend/js/pages/form_elements.js"></script>

    <!-- end of plugin scripts -->
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"
            integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk=" crossorigin="anonymous"></script>

    <!--Page level scripts-->
    <script type="text/javascript" src="/backend/js/pages/datatable_server_side.js"></script>
    <script type="text/javascript" src="{{asset('/backend/js/common.js')}}"></script>
    <script type="text/javascript" src="/backend/sheetjs/dist/xlsx.full.min.js"></script>
    <script type="text/javascript" src="/backend/filesaver/dist/FileSaver.js"></script>
    <script src="{{ asset('custom/general.js') }}"></script>

    <script type="text/javascript">
        sessionStorage.setItem("booked_data", JSON.stringify([]));

        function excel_filters() {
            var company = $('#search_company_excel').val($('#search_company').val());
            // alert($('#search_company_excel').val())
            $('#search_block_excel').val($('#search_block').val());
            $('#search_start_date_excel').val($('#search_start_date').val());
            $('#search-input_excel').val($('#search-input').val());

        }

        function spinner_start() {
            $(".loader").show();
            $(".loader-percentage").show();
            jQuery('#lockers_div').css('opacity', '0.05');
        }

        function spinner_close() {
            // $(".excel-loader").hide();
            $(".loader").hide();
            $(".loader-percentage").hide();
            jQuery('#lockers_div').css('opacity', '1');
        }

        function exportTableToExcel() {


            $('#percentage').html(0 + '%');

            var ws_data = [['Locker Group', 'Locker Number', 'Start Date', 'End Date','Code']];
            sessionStorage.setItem("booked_data", JSON.stringify(ws_data));
            // wb.SheetNames.push("Test Sheet");
            // var ws = XLSX.utils.aoa_to_sheet(ws_data);
            var total_record = '';
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            // var search_company = $('#search_company').val();
            // var search_block = $('#search_block').val();
            var search_start_date = $('#search_start_date').val();
            var search_input = $('#search-input').val();
            setTimeout(
                function () {

                    $.ajax({
                        url: '/admin/reports/lockernumbers/count',
                        type: 'POST',
                        dataType: 'JSON',
                        async: false,
                        data: {
                            // search_company: search_company,
                            // search_block: search_block,
                            search_start_date: search_start_date,
                            search_input: search_input,
                            _token: CSRF_TOKEN
                        },
                        success: function (response) {

                            total_record = response;
                            console.log(total_record, 'total_record');
                            var number_of_requests = Math.ceil(total_record / 1000);
                            var percentage = parseFloat(100 / number_of_requests)
                            console.log(percentage, 'percentage');
                            var total_percentage = 0;
                            var page = 0;
                            var percentage_whole_number = 1;

                            sessionStorage.setItem("number_of_requests", number_of_requests);
                            sessionStorage.setItem("percentage", percentage);
                            sessionStorage.setItem("total_percentage", total_percentage);
                            sessionStorage.setItem("page", page);
                            sessionStorage.setItem("percentage_whole_number", percentage_whole_number);
                            get_excel_data();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR);
                        }
                    });


                }, 1000);
        }

        function get_excel_data() {

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            // var search_company = $('#search_company').val();
            // var search_block = $('#search_block').val();
            var search_start_date = $('#search_start_date').val();
            var search_input = $('#search-input').val();
            var page = parseInt(sessionStorage.getItem("page"));
            var percentage_whole_number = parseInt(sessionStorage.getItem("percentage_whole_number"));


            $.ajax({
                url: '/admin/reports/lockernumbers_excelFile',
                type: 'POST',
                dataType: 'JSON',

                data: {
                    page: page,
                    // search_company: search_company,
                    // search_block: search_block,
                    search_start_date: search_start_date,
                    search_input: search_input,
                    _token: CSRF_TOKEN
                },


                success: function (response) {

                    var ws_data = JSON.parse(sessionStorage.getItem("booked_data"));

                    var total_percentage = parseFloat(sessionStorage.getItem("total_percentage"));

                    var percentage = parseFloat(sessionStorage.getItem("percentage"));
                    total_percentage = total_percentage + percentage;
                    total_percentage = total_percentage.toFixed(2);

                    if (percentage_whole_number < total_percentage) {

                        percentage_whole_number = percentage_whole_number + 1;
                        sessionStorage.setItem("percentage_whole_number", percentage_whole_number);
                        $('#percentage').html(Math.floor(total_percentage) + '%');
                    }

                    var number_of_requests = parseInt(sessionStorage.getItem("number_of_requests"));

                    $.each(response.data, function (index, val) {
                        ws_data.push([val.blockgroup['name'], val.locker_number, val.start_date, val.end_date,val.code]);
                    });


                    sessionStorage.setItem("booked_data", JSON.stringify(ws_data));
                    sessionStorage.setItem("total_percentage", total_percentage);

                    if (page == number_of_requests - 1) {
                        var wb = XLSX.utils.book_new();
                        wb.Props = {
                            Title: "Locker Numbers",
                            Subject: "Locker Numbers",
                            Author: "Locker Rentals",
                            CreatedDate: new Date()
                        };

                        var ws_data = JSON.parse(sessionStorage.getItem("booked_data"));

                        var ws = XLSX.utils.aoa_to_sheet(ws_data);
                        var wbd = XLSX.utils.book_append_sheet(wb, ws, "Sheet" + page);

                        var wopts = {bookType: 'xlsx', bookSST: false, type: 'array'};

                        var wbout = XLSX.write(wb, wopts);

                        /* the saveAs call downloads a file on the local machine */
                        saveAs(new Blob([wbout], {type: "application/octet-stream"}), "LockerNumber.xlsx");
                        spinner_close();
                    } else {
                        sessionStorage.setItem("page", page + 1);
                        get_excel_data()
                    }


                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                }
            });

        }

        $(document).ready(function () {
            $('.sorting_disabled').removeClass('sorting_asc');
            $("#search_start_date").datepicker();
            $("#search_end_date").datepicker();


            var searchFunction = 'searchByThis(availability_reports_table)';
            var payments_search = "{{\Illuminate\Support\Facades\Session::get('payments-search')}}";
            $('#sample_11_wrapper>.row:nth-child(1)>div:nth-child(2)').append('<label style="float:right;margin-top:30px;"><input class="form-control input-sm" id="search-input" value="' + payments_search + '" onblur="' + searchFunction + '"></label>');
            var oTable = $('#sample_11').dataTable();
            // Sometime later - filter...
            oTable.fnFilter(payments_search);


            // $("#search_company").keypress(function (event) {
            //     if (event.keyCode === 13) {
            //         searchByThis(availability_reports_table);
            //         excel_filters();
            //     }
            //     sessionStorage.setItem('search_company_available', $("#search_company").val())
            // });

            // $("#search_block").keypress(function (event) {
            //     if (event.keyCode === 13) {
            //         searchByThis(availability_reports_table);
            //         excel_filters();
            //     }
            //     sessionStorage.setItem('search_block_available', $("#search_block").val())
            // });
            $("#search-input").keypress(function (event) {
                if (event.keyCode === 13) {
                    searchByThis(availability_reports_table);
                    excel_filters();
                }
                sessionStorage.setItem('search-input_available', $("#search-input").val())
            });
        });

        var availability_reports_table = availabilityTable();
        $('#search_start_date').change(function(){

            availabilityTable();
        });
        $('#lockerGroup,#lockerNumber').keyup(function(){
            availabilityTable();
        });
        function availabilityTable() {
            excel_filters();
           
            $('#sample_11').dataTable().fnClearTable();
            $('#sample_11').dataTable().fnDestroy();

            $("#sample_11_filter>label").css('float', 'right');
            $("#sample_11_length>label").css('float', 'left');
 var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            return $('#sample_11').DataTable({
                responsive: true,
                serverSide: true,
                stateSave: true,
                processing: true,
                searching: false,
                "ordering": false,
                "pageLength": 10,
           
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                "oLanguage": {
                    "sLengthMenu": "_MENU_",
                    "sSearch": "_INPUT_"
                },
                "aTargets": ["_all"],
                initComplete: function () {
                    $("#sample_11_filter>label").css('float', 'right');
                    $("#sample_11_length>label").css('float', 'left');
                },

                ajax: {
                    url: '<?php echo e(route('reports.show.all.locker.numbers')); ?>',
                    method:'POST',
                    data: function (d) {
                        d.lockerGroup = $('#lockerGroup').val();
                        d.lockerNumber = $('#lockerNumber').val();
                        d.search_start_date = $('#search_start_date').val();
                        d.search_input = $('#search-input').val();
                        d._token= CSRF_TOKEN;
                    }
                },
                columns: [
                    // {data: 'id', name: 'id'},
                    {data: 'blockName', name: 'blockName'},
                    {data: 'lockerNumber', name: 'lockerNumber'},
                    {data: 'StartDate', name: 'StartDate'},
                    {data: 'end_date', name: 'end_date'},
                    {data: 'code', name: 'code'},

                ],
            });
        }


        // function changeStartDate(){
        //     var start_date = $("#search_start_date").val();
        //     $.ajax({
        //         url: '/admin/reports/set/start/availability/'+ start_date,
        //         type: 'GET',
        //         dataType: 'JSON',
        //         cache: false,

        //         success: function (data) {
        //             console.log(data);
        //             searchByThis(availability_reports_table);
        //             },
        //         error: function (jqXHR, textStatus, errorThrown) {
        //             console.log(jqXHR);
        //         }
        //     });
        // }
        // function changeEndDate(){
        //     var end_date = $("#search_end_date").val();
        //     $.ajax({
        //         url: '/admin/reports/set/start/availability/'+ end_date,
        //         type: 'GET',
        //         dataType: 'JSON',
        //         cache: false,

        //         success: function (data) {
        //             console.log(data);
        //             searchByThis(availability_reports_table);
        //             },
        //         error: function (jqXHR, textStatus, errorThrown) {
        //             console.log(jqXHR);
        //         }
        //     });
        // }


    </script>


@endsection