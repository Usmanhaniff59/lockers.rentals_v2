@extends('backend.layouts.app')

@section('title', 'Lockers | Companies')
@section('style')
<!--plugin styles-->
<link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-switch/css/bootstrap-switch.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/switchery/css/switchery.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/radio_css/css/radiobox.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/checkbox_css/css/checkbox.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/modal/css/component.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-tagsinput/css/bootstrap-tagsinput.css" />
<link rel="stylesheet" type="text/css" href="/backend/vendors/animate/css/animate.min.css" />
<!-- end of plugin styles -->

<!--Page level styles-->
<link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/pages/radio_checkbox.css" />


<link type="text/css" rel="stylesheet" href="/backend/vendors/chosen/css/chosen.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/pages/form_elements.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/pages/datepicker-jquery.css" />


<style>
    @media (min-width: 768px) {
        .modal-xl {
            width: 90%;
            max-width: 1200px;
        }
    }

    .flex-container {
        display: flex;
        justify-content: center;
        flex-wrap: wrap;
    }

    .flex-container label {
        margin: 15px;
    }

    .loader {
        position: absolute;
        top: 15%;
        left: 45%;
        margin: auto;
        display: none;
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #00c0ef;
        width: 150px;
        height: 150px;
        -webkit-animation: spin 2s linear infinite;
        /* Safari */
        animation: spin 2s linear infinite;
    }

    .loader-percentage {
        position: absolute;
        top: 21%;
        left: 50%;
        margin: auto;

        display: none;
    }

    /* Safari */
    @-webkit-keyframes spin {
        0% {
            -webkit-transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
        }
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }

        100% {
            transform: rotate(360deg);
        }
    }

    .excel-loader {
        display: none;
    }
</style>

@endsection
@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
            <div class="row no-gutters">
                <div class="col-6">
                    <h4 class="m-t-5">
                        <i class="fa fa-building circle header_icon_margin"></i>
                        Reports
                    </h4>
                </div>
            </div>
        </div>
    </header>
    @if(Session::has('flash_message'))
    <div class="col-md-12">
        <div class="alert alert-success alert-dismissable float-right"
            style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"
                style="margin-top: 4px; margin-left: 15px;"> ×
            </button>
            {{ Session::get('flash_message') }}
        </div>
    </div>
    @endif
    @if (session('status'))
    <div class="alert alert-danger">
        {{ session('status') }}
    </div>
    @endif

    @include('errors.success')
    @include('errors.error')

    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                {{--<div style="z-index: 19000" class="excel-loader">--}}
                {{--<h2 style="position:absolute;top:25%;left:38%;color: #00c0ef" ><b id="percentage">0%</b></h2>--}}
                {{--<div class="spinner-border " style="position:absolute;width:200px;height:200px;top:15%;left:30%;color: #00c0ef"></div>--}}
                {{--</div>--}}
                <div class="loader" id="block_loader">
                </div>
                <h2 class="loader-percentage"><b id="percentage">0%</b></h2>
                <div id="lockers_div">
                    @include ('errors.list')
                    <div class="col-12 data_tables">
                        <div class="card m-t-35">
                            <div class="card-header bg-white">
                                <i class="fa fa-building circle header_icon_margin"></i> Chat Reports
                            </div>
                            <form class="form-style" id="export_excel_project" method="POST"
                                action="{{ url('admin/reports/booked/excel/export') }}">
                                <div class="row p-t-15" style="margin-left: 12px">

                                    {!! csrf_field() !!}

                                    <button type="button" class="form_submit_check btn btn-warning  ml-2"
                                        onclick="spinner_start();exportTableToExcel()">
                                        <span class="assign_class label">
                                            EXCEL EXPORT
                                        </span>
                                    </button>
                                </div>

                            </form>

                            <div class="row p-t-15" style="margin-left: 12px; margin-right: 12px">
                                <div class="col-md-2"><input type="text" id="search_company" placeholder="Company Name"
                                        class="form-control"></div>
                                <div class="col-md-2"> <select type="text" id="search_company_type"
                                        placeholder="Franchise" class="form-control">
                                        {{--<option id="Prospective">Prospective</option>--}}
                                        <option value="">Chat Type</option>
                                        <option value="all">All</option>
                                        @foreach($chat_types as $chat_type)
                                        <option value="{{$chat_type->id}}">{{$chat_type->type}}</option>
                                        @endforeach

                                    </select></div>
                                <div class="col-md-2"><input type="text" id="search_details_chat" placeholder="Details"
                                        class="form-control"></div>
                                <div class="col-md-2"><input type="text" id="search_persons_name"
                                        placeholder="Persons Name" class="form-control"></div>
                                <div class="col-md-2"><input type="text" onchange="searchByThis(chat_reports_table)"
                                        id="search_date" class="form-control" placeholder="Date"></div>


                            </div>

                            <div class="card-body p-t-10">
                                <div class="row" style="width: 100% !important;">
                                    <div class="col-md-12 m-t-25">
                                        <div class="table-responsive">
                                            <table class="table  table-bordered table-hover  " style="width:100%"
                                                id="chat_report">
                                                <thead>
                                                    <tr>
                                                        <th>Title</th>
                                                        <th>Company</th>
                                                        <th>Type</th>
                                                        <th>Person Name</th>
                                                        <th>Details</th>
                                                        <th>Date</th>
                                                    </tr>
                                                </thead>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!--- Edit model -->
<div class="modal fade in display_none" id="viewuser" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div id="editform_display">
            </div>

        </div>
    </div>
</div>
<!-- END Edit model-->
@endsection
@section('script')
<!--plugin scripts-->
<script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="/backend/vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>
<!-- end of plugin scripts -->

<!--Page level scripts-->
<script type="text/javascript" src="/backend/js/pages/datatable_server_side.js"></script>
<script type="text/javascript" src="/backend/js/pages/radio_checkbox.js"></script>
<script type="text/javascript" src="{{asset('/backend/js/common.js')}}"></script>
<script src="{{ asset('custom/general.js') }}"></script>


<script type="text/javascript" src="/backend/vendors/chosen/js/chosen.jquery.js"></script>
<script type="text/javascript" src="/backend/js/pages/form_elements.js"></script>

<!-- end of plugin scripts -->
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"
    integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk=" crossorigin="anonymous"></script>

<!--Page level scripts-->
<script type="text/javascript" src="/backend/js/pages/datatable_server_side.js"></script>
<script type="text/javascript" src="{{asset('/backend/js/common.js')}}"></script>
<script type="text/javascript" src="/backend/sheetjs/dist/xlsx.full.min.js"></script>
<script type="text/javascript" src="/backend/filesaver/dist/FileSaver.js"></script>


<script type="text/javascript">
    sessionStorage.setItem("chat_data", JSON.stringify([]));

        function spinner_start() {
            $(".loader").show();
            $(".loader-percentage").show();
            jQuery('#lockers_div').css('opacity', '0.05');
        }
        function spinner_close() {
            // $(".excel-loader").hide();
            $(".loader").hide();
            $(".loader-percentage").hide();
            jQuery('#lockers_div').css('opacity', '1');
        }
        function exportTableToExcel(){

            $('#percentage').html(0+'%');

            var ws_data = [['Title','Company', 'Type', 'Name', 'Details', 'Created At']];
            sessionStorage.setItem("chat_data", JSON.stringify(ws_data));

            var total_record = '';
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            var search_company = $('#search_company').val();
            var search_company_type = $('#search_company_type').val();
            var search_details_chat = $('#search_details_chat').val();
            var search_persons_name = $('#search_persons_name').val();
            var search_date = $('#search_date').val();
            var search_input = $('#search-input').val();

            setTimeout(
                function()
                {
                    $.ajax({
                        url: '/admin/company/chats/count',
                        type: 'POST',
                        dataType: 'JSON',
                        async: false,
                        data: {search_input: search_input,search_company: search_company,search_company_type: search_company_type,search_details_chat: search_details_chat,search_persons_name: search_persons_name,search_date: search_date,_token: CSRF_TOKEN},
                        success: function (response) {

                            total_record = response;

                            var number_of_requests = Math.ceil(total_record/1000);
                            var percentage = parseFloat(100/number_of_requests);
                            var total_percentage = 0;
                            var page = 0;
                            var percentage_whole_number = 1;

                            sessionStorage.setItem("number_of_requests", number_of_requests);
                            sessionStorage.setItem("percentage", percentage);
                            sessionStorage.setItem("total_percentage", total_percentage);
                            sessionStorage.setItem("page", page);
                            sessionStorage.setItem("percentage_whole_number", percentage_whole_number);

                            get_excel_data();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR);
                        }
                    });

                }, 1000);
        }


        function get_excel_data(){

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            var search_company = $('#search_company').val();
            var search_company_type = $('#search_company_type').val();
            var search_details_chat = $('#search_details_chat').val();
            var search_persons_name = $('#search_persons_name').val();
            var search_date = $('#search_date').val();
            var search_input = $('#search-input').val();
            var page =  parseInt(sessionStorage.getItem("page"));
            var percentage_whole_number =  parseInt(sessionStorage.getItem("percentage_whole_number"));

            $.ajax({
                url: '/admin/company/chats/get/data',
                type: 'POST',
                dataType: 'JSON',
                data: {page: page,search_input: search_input,search_company: search_company,search_company_type: search_company_type,search_details_chat: search_details_chat,search_persons_name: search_persons_name,search_date: search_date,_token: CSRF_TOKEN},

                success: function (response) {
                    console.log(response);

                    var ws_data = JSON.parse(sessionStorage.getItem("chat_data"));
                    var total_percentage =  parseFloat(sessionStorage.getItem("total_percentage"));

                    var percentage =  parseFloat(sessionStorage.getItem("percentage"));
                    total_percentage = total_percentage + percentage;
                    total_percentage = total_percentage.toFixed(2);

                    if(percentage_whole_number < total_percentage ) {

                        percentage_whole_number = percentage_whole_number +1;
                        sessionStorage.setItem("percentage_whole_number", percentage_whole_number);
                        $('#percentage').html(Math.floor(total_percentage) + '%');
                    }
                    var number_of_requests =  parseInt(sessionStorage.getItem("number_of_requests"));
                    console.log(number_of_requests,'number_of_requests');

                    $.each(response.data, function (index, val) {
                        ws_data.push([ val.title,val.company_name,  val.type, val.name1,val.details,val.created_at1]);
                    });

                    sessionStorage.setItem("chat_data", JSON.stringify(ws_data));
                    sessionStorage.setItem("total_percentage", total_percentage);

                    if (page == number_of_requests - 1) {
                        var wb = XLSX.utils.book_new();
                        wb.Props = {
                            Title: "Chat Report",
                            Subject: "Booked Report",
                            Author: "Locker Rentals",
                            CreatedDate: new Date()
                        };
                        var ws_data = JSON.parse(sessionStorage.getItem("chat_data"));

                        var ws = XLSX.utils.aoa_to_sheet(ws_data);
                        var wbd = XLSX.utils.book_append_sheet(wb, ws, "Sheet" + page);

                        var wopts = {bookType: 'xlsx', bookSST: false, type: 'array'};

                        var wbout = XLSX.write(wb, wopts);

                        /* the saveAs call downloads a file on the local machine */
                        saveAs(new Blob([wbout],{type:"application/octet-stream"}), "ChatReport.xlsx");
                        spinner_close();

                    }else{
                        sessionStorage.setItem("page",  page+1);
                        get_excel_data()
                    }


                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                }
            });

        }




        $(document).ready(function() {


                $("#search_date").datepicker();



                $("#search_company").keypress(function(event) {
                    if (event.keyCode === 13) {
                        searchByThisBooked(chat_reports_table);

                    }

                });
                 $("#search_company_type").change(function(event) {
                     searchByThisBooked(chat_reports_table);

                 });


                $("#search_details_chat").keypress(function(event) {
                    if (event.keyCode === 13) {
                        searchByThisBooked(chat_reports_table);

                    }

                });
                 $("#search_persons_name").keypress(function(event) {
                    if (event.keyCode === 13) {
                        searchByThisBooked(chat_reports_table);
                    }

                });


            });

        function searchByThisBooked(table){
            table.draw('page');

        }

        var chat_reports_table = chatReportTable();

        function chatReportTable() {

                $('#chat_report').dataTable().fnClearTable();
                $('#chat_report').dataTable().fnDestroy();

                $("#chat_report_filter>label").css('float', 'right');
                $("#chat_report_length>label").css('float', 'left');

                return $('#chat_report').DataTable({
                    responsive:true,
                    serverSide: true,
                    stateSave: true,
                    processing: true,
                    searching: false,
                    "pageLength" : 10,
                    "order": [[ 0, "asc" ], [ 1, "asc" ],[ 2, "asc" ]],
                    "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],

                    "oLanguage": {
                        "sLengthMenu": "_MENU_",
                        "sSearch": "_INPUT_"
                    },
                    "aTargets": [ "_all" ],
                    initComplete : function() {
                        $("#chat_report_filter>label").css('float', 'right');
                        $("#chat_report_length>label").css('float', 'left');
                    },

                    ajax: {
                        url: '/admin/company/chats/chatajax',
                        data: function (d) {
                            d.search_company = $('#search_company').val();
                            d.search_company_type = $('#search_company_type').val();
                            d.search_persons_name = $('#search_persons_name').val();
                            d.search_details_chat = $('#search_details_chat').val();
                            d.search_date = $('#search_date').val();
                        }
                    },
                    columns: [
                    {data: 'title', name: 'title'},
                    {data: 'company_name', name: 'company_name'},
                        {data: 'type', name: 'type'},
                        {data: 'name1', name: 'name1'},
                        {data: 'details', name: 'details'},
                        {data: 'created_at1', name: 'created_at1'},
                    ],
                });
            }


</script>


@endsection