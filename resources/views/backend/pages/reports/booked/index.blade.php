@extends('backend.layouts.app')

@section('title', 'Lockers | Companies')
@section('style')
    <!--plugin styles-->
    <link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-switch/css/bootstrap-switch.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/switchery/css/switchery.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/radio_css/css/radiobox.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/checkbox_css/css/checkbox.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/modal/css/component.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-tagsinput/css/bootstrap-tagsinput.css"/>
    <link rel="stylesheet" type="text/css" href="/backend/vendors/animate/css/animate.min.css"/>
    <!-- end of plugin styles -->

    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/radio_checkbox.css"/>

    <link type="text/css" rel="stylesheet" href="/backend/vendors/chosen/css/chosen.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/form_elements.css"/>
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">--}}
    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>--}}
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>--}}
    {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>--}}

    <style>
        @media (min-width: 768px) {
            .modal-xl {
                width: 90%;
                max-width: 1200px;
            }
        }

        .flex-container {
            display: flex;
            justify-content: center;
            flex-wrap: wrap;
        }

        .flex-container label {
            margin: 15px;
        }

        .ui-datepicker {
            width: 300px;
            height: 300px;
            margin: 5px auto 0;
            font: 12pt Arial, sans-serif;
            /*-webkit-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
                -moz-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);*/
        }

        .ui-datepicker table {
            width: 100%;
        }

        .ui-datepicker-header {
            background: #3399ff;
            color: #ffffff;
            font-family: 'Times New Roman';
            border-width: 1px 0 0 0;
            border-style: solid;
            border-color: #111;
        }

        .ui-datepicker-title {
            text-align: center;
            font-size: 15px;

        }

        .ui-datepicker-prev {
            float: left;
            cursor: pointer;
            background-position: center -30px;
        }

        .ui-datepicker-next {
            float: right;
            cursor: pointer;
            background-position: center 0px;
        }

        .ui-datepicker thead {
            background-color: #f7f7f7;

            /*border-bottom: 1px solid #bbb;*/
        }

        .ui-datepicker th {
            background-color: #808080;
            text-transform: uppercase;
            font-size: 8pt;
            color: #666666;
            /*text-shadow: 1px 0px 0px #fff;*/
            /*filter: dropshadow(color=#fff, offx=1, offy=0);*/
        }

        .ui-datepicker tbody td {
            padding: 0;
            /*border-right: 1px solid #808080;*/
        }

        .ui-datepicker tbody td:last-child {
            border-right: 0px;
        }

        .ui-datepicker tbody tr {
            border-bottom: 1px solid #bbb;
        }

        .ui-datepicker tbody tr:last-child {
            border-bottom: 0px;
        }

        .ui-datepicker a {
            text-decoration: none;
        }

        .ui-datepicker td span,
        .ui-datepicker td a {
            display: inline-block;
            /*font-weight: bold;*/
            text-align: center;
            width: 30px;
            height: 30px;
            line-height: 30px;
            color: #ffffff;
            /*text-shadow: 1px 1px 0px #fff;*/
            /*filter: dropshadow(color=#fff, offx=1, offy=1);*/
        }

        .ui-datepicker-calendar .ui-state-default {
            background: linear-gradient(#999999, #737373);
            color: #ffffff;
            height: 40px;
            width: 40px;

        }

        .ui-datepicker-calendar .ui-state-hover {
            background: #33adff;
            color: #FFFFFF;
        }

        .ui-datepicker-calendar .ui-state-active {
            background: #33adff;
            -webkit-box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1);
            -moz-box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1);
            box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1);
            color: #e0e0e0;
            text-shadow: 0px 1px 0px #4d7a85;
            border: 1px solid #55838f;
            position: relative;
            margin: -1px;
        }

        .ui-datepicker-unselectable .ui-state-default {
            background: #D6E4BE;
            color: #000;
        }

        /* Safari */
        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }

        .excel-loader {
            display: none;
        }

        .loader {
            position: absolute;
            top: 15%;
            left: 45%;
            margin: auto;
            display: none;
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #00c0ef;
            width: 150px;
            height: 150px;
            -webkit-animation: spin 2s linear infinite;
            /* Safari */
            animation: spin 2s linear infinite;
        }

        .loader-percentage {
            position: absolute;
            top: 19%;
            left: 50%;
            margin: auto;

            display: none;
        }

        /* Safari */
        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }
    </style>

@endsection
@section('content')
    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-building circle header_icon_margin"></i>
                            Reports
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        @if(Session::has('flash_message'))
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable float-right"
                     style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"
                            style="margin-top: 4px; margin-left: 15px;"> ×
                    </button>
                    {{ Session::get('flash_message') }}
                </div>
            </div>
        @endif
        @if (session('status'))
            <div class="alert alert-danger">
                {{ session('status') }}
            </div>
        @endif

        @include('errors.success')
        @include('errors.error')

        <div class="outer">
            <div class="inner bg-container">
                <div class="row">
                    {{--<div style="z-index: 19000" class="excel-loader">--}}
                    {{--<h2 style="position:absolute;top:25%;left:38%;color: #00c0ef" ><b id="percentage">0%</b></h2>--}}
                    {{--<div class="spinner-border " style="position:absolute;width:200px;height:200px;top:15%;left:30%;color: #00c0ef"></div>--}}
                    {{--</div>--}}
                    <div class="loader" id="block_loader"></div>
                    <h2 class="loader-percentage"><b id="percentage">0%</b></h2>
                    <div id="lockers_div">
                        @include ('errors.list')
                        <div class="col-12 data_tables">
                            <div class="card m-t-35">
                                <div class="card-header bg-white">
                                    <i class="fa fa-building circle header_icon_margin"></i> Lockers Booked Report
                                </div>

                                <div class="row p-t-15" style="margin-left: 12px">

                                    <button type="button" class="form_submit_check btn btn-warning  ml-2"
                                            onclick="spinner_start();exportTableToExcel()">
                                        <span class="assign_class label">
                                            EXCEL EXPORT
                                        </span>
                                    </button>
                                </div>

                                <div class="row p-t-15" style="margin-left: 12px; margin-right: 12px">
                                    <div class="col-md-2"><input type="text" id="search_bookingId"
                                                                 placeholder="Booking Id"
                                                                 class="form-control"></div>
                                    <div class="col-md-2"><input type="text" id="search_company" placeholder="Company"
                                                                 class="form-control"></div>
                                    <div class="col-md-2"><input type="text" id="search_block" placeholder="Block"
                                                                 class="form-control"></div>

                                    <div class="col-md-2">
                                        <input type="text"
                                               onchange="searchByThisBooked(availability_reports_table)"
                                               id="search_date"
                                               class="form-control" placeholder="Select Date">
                                    </div>
                                    <div class="col-md-2"><input type="text" id="search_name" placeholder="Name"
                                                                 class="form-control">
                                    </div>
                                    <div class="col-md-2"><input type="text" id="search_surname" placeholder="Surname"
                                                                 class="form-control">
                                    </div>
                                </div>
                                <div class="row p-t-15" style="margin-left: 12px; margin-right: 12px">
                                    <div class="col-md-2"><input type="text" id="search_lockernum"
                                                                 placeholder="Locker Number" class="form-control">
                                    </div>
                                </div>

                                <div class="card-body p-t-10">

                                    <div class="row" style="width: 100% !important;">
                                        <div class="col-md-12 m-t-25">
                                            <div class="table-responsive">
                                                <table class="table  table-bordered table-hover  " style="width:100%"
                                                       id="sample_6">
                                                    <thead>
                                                    <tr>
                                                        <th>Booking ID</th>
                                                        <th>Company</th>
                                                        <th>Block</th>
                                                        <th>Locker Number</th>
                                                        <th>Name</th>
                                                        <th>Surname</th>
                                                        <th>Code</th>
                                                        <th>Start</th>
                                                        <th>End</th>
                                                        <th>Actions</th>
                                                    </tr>

                                                    </thead>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!--- Edit model -->
    <div class="modal fade in display_none" id="viewuser" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div id="editform_display">
                </div>

            </div>
        </div>
    </div>
    <!-- END Edit model-->
@endsection
@section('script')
    <!--plugin scripts-->
    <script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>
    <!-- end of plugin scripts -->

    <!--Page level scripts-->
    <script type="text/javascript" src="/backend/js/pages/datatable_server_side.js"></script>
    <script type="text/javascript" src="/backend/js/pages/radio_checkbox.js"></script>
    <script type="text/javascript" src="{{asset('/backend/js/common.js')}}"></script>
    <script src="{{ asset('custom/general.js') }}"></script>


    <script type="text/javascript" src="/backend/vendors/chosen/js/chosen.jquery.js"></script>
    <script type="text/javascript" src="/backend/js/pages/form_elements.js"></script>

    <!-- end of plugin scripts -->
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"
            integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk=" crossorigin="anonymous"></script>

    <!--Page level scripts-->
    <script type="text/javascript" src="/backend/js/pages/datatable_server_side.js"></script>
    <script type="text/javascript" src="{{asset('/backend/js/common.js')}}"></script>
    <script type="text/javascript" src="/backend/sheetjs/dist/xlsx.full.min.js"></script>
    <script type="text/javascript" src="/backend/filesaver/dist/FileSaver.js"></script>
    <script type="text/javascript">
        sessionStorage.setItem("booked_data", JSON.stringify([]));

        function spinner_start() {
            $(".loader").show();
            $(".loader-percentage").show();
            jQuery('#lockers_div').css('opacity', '0.05');
        }

        function spinner_close() {
            // $(".excel-loader").hide();
            $(".loader").hide();
            $(".loader-percentage").hide();
            jQuery('#lockers_div').css('opacity', '1');
        }

        function exportTableToExcel() {

            $('#percentage').html(0 + '%');

            var ws_data = [['Booking ID', 'Company', 'Block', 'Locker Number', 'Name', 'Surname', 'Code', 'Start', 'End']];
            sessionStorage.setItem("booked_data", JSON.stringify(ws_data));

            var total_record = '';
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var booking_id = $('#search_bookingId').val();
            var search_company = $('#search_company').val();
            var search_block = $('#search_block').val();
            var search_date = $('#search_date').val();
            var search_name = $('#search_name').val();
            var search_surname = $('#search_surname').val();
            var search_lockernum = $('#search_lockernum').val();
            var search_input = $('#search-input').val();
            setTimeout(
                function () {
                    $.ajax({
                        url: '/admin/reports/booked/report/count',
                        type: 'POST',
                        dataType: 'JSON',
                        async: false,
                        data: {
                            booking_id: booking_id,
                            search_company: search_company,
                            search_block: search_block,
                            search_date: search_date,
                            search_name: search_name,
                            search_surname: search_surname,
                            search_lockernum: search_lockernum,
                            search_input: search_input,
                            _token: CSRF_TOKEN
                        },
                        success: function (response) {

                            total_record = response;

                            var number_of_requests = Math.ceil(total_record / 1000);
                            var percentage = parseFloat(100 / number_of_requests);
                            var total_percentage = 0;
                            var page = 0;
                            var percentage_whole_number = 1;

                            sessionStorage.setItem("number_of_requests", number_of_requests);
                            sessionStorage.setItem("percentage", percentage);
                            sessionStorage.setItem("total_percentage", total_percentage);
                            sessionStorage.setItem("page", page);
                            sessionStorage.setItem("percentage_whole_number", percentage_whole_number);

                            get_excel_data();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR);
                        }
                    });

                }, 1000);
        }


        function get_excel_data() {

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var booking_id = $('#search_bookingId').val();
            var search_company = $('#search_company').val();
            var search_block = $('#search_block').val();
            var search_date = $('#search_date').val();

            var search_name = $('#search_name').val();
            var search_surname = $('#search_surname').val();
            var search_lockernum = $('#search_lockernum').val();
            var search_input = $('#search-input').val();
            var page = parseInt(sessionStorage.getItem("page"));
            var percentage_whole_number = parseInt(sessionStorage.getItem("percentage_whole_number"));

            $.ajax({
                url: '/admin/reports/booked/report',
                type: 'POST',
                dataType: 'JSON',

                data: {
                    page: page,
                    booking_id: booking_id,
                    search_company: search_company,
                    search_block: search_block,
                    search_date: search_date,
                    search_name: search_name,
                    search_surname: search_surname,
                    search_lockernum: search_lockernum,
                    search_input: search_input,
                    _token: CSRF_TOKEN
                },

                success: function (response) {

                    var ws_data = JSON.parse(sessionStorage.getItem("booked_data"));
                    var total_percentage = parseFloat(sessionStorage.getItem("total_percentage"));

                    var percentage = parseFloat(sessionStorage.getItem("percentage"));
                    total_percentage = total_percentage + percentage;
                    total_percentage = total_percentage.toFixed(2);

                    if (percentage_whole_number < total_percentage) {

                        percentage_whole_number = percentage_whole_number + 1;
                        sessionStorage.setItem("percentage_whole_number", percentage_whole_number);
                        $('#percentage').html(Math.floor(total_percentage) + '%');
                    }
                    var number_of_requests = parseInt(sessionStorage.getItem("number_of_requests"));
                    console.log(number_of_requests, 'number_of_requests');

                    $.each(response.data, function (index, val) {
                        ws_data.push([val.booking_id, val.company_name, val.block, val.locker_number, val.child_first_name, val.child_surname, val.net_pass, val.start, val.end]);
                    });

                    sessionStorage.setItem("booked_data", JSON.stringify(ws_data));
                    sessionStorage.setItem("total_percentage", total_percentage);

                    if (page == number_of_requests - 1) {
                        var wb = XLSX.utils.book_new();
                        wb.Props = {
                            Title: "Booked Report",
                            Subject: "Booked Report",
                            Author: "Locker Rentals",
                            CreatedDate: new Date()
                        };
                        var ws_data = JSON.parse(sessionStorage.getItem("booked_data"));

                        var ws = XLSX.utils.aoa_to_sheet(ws_data);
                        var wbd = XLSX.utils.book_append_sheet(wb, ws, "Sheet" + page);

                        var wopts = {bookType: 'xlsx', bookSST: false, type: 'array'};

                        var wbout = XLSX.write(wb, wopts);

                        /* the saveAs call downloads a file on the local machine */
                        saveAs(new Blob([wbout], {type: "application/octet-stream"}), "BookedReport.xlsx");
                        spinner_close();

                    } else {
                        sessionStorage.setItem("page", page + 1);
                        get_excel_data()
                    }


                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                }
            });

        }


        function searchByThisBooked(table) {
            table.draw('page');
            booked_excel_filters();
        }

        function booked_excel_filters() {

            $('#search_bookingId_excel').val($('#search_bookingId').val());
            $('#search_company_excel').val($('#search_company').val());
            // alert($('#search_company_excel').val())
            $('#search_block_excel').val($('#search_block').val());
            $('#search_date_excel').val($('#search_date').val());
            $('#search_name_excel').val($('#search_name').val());
            $('#search_surname_excel').val($('#search_surname').val());
            $('#search_lockernum_excel').val($('#search_lockernum').val());
            $('#search_input_excel').val($('#search-input').val());

        }

        $(document).ready(function () {


            $("#search_date").datepicker();

            var searchFunction = 'searchByThisBooked(availability_reports_table)';
            var payments_search = "{{\Illuminate\Support\Facades\Session::get('payments-search')}}";
            $('#sample_6_wrapper>.row:nth-child(1)>div:nth-child(2)').append('<label style="float:right;margin-top:30px;"><input class="form-control input-sm" id="search-input" value="' + payments_search + '" onblur="' + searchFunction + '"></label>');
            var oTable = $('#sample_6').dataTable();
            // Sometime later - filter...
            oTable.fnFilter(payments_search);


            $("#search_bookingId").keypress(function (event) {
                if (event.keyCode === 13) {
                    searchByThisBooked(availability_reports_table);

                }

            });
            $("#search_company").keypress(function (event) {
                if (event.keyCode === 13) {
                    searchByThisBooked(availability_reports_table);

                }

            });


            $("#search_block").keypress(function (event) {
                if (event.keyCode === 13) {
                    searchByThisBooked(availability_reports_table);

                }

            });
            $("#search-input").keypress(function (event) {
                if (event.keyCode === 13) {
                    searchByThisBooked(availability_reports_table);
                }

            });

            $("#search_name").keypress(function (event) {
                if (event.keyCode === 13) {
                    searchByThisBooked(availability_reports_table);

                }

            });

            $("#search_surname").keypress(function (event) {
                if (event.keyCode === 13) {
                    searchByThisBooked(availability_reports_table);

                }

            });

            $("#search_lockernum").keypress(function (event) {
                if (event.keyCode === 13) {
                    searchByThisBooked(availability_reports_table);

                }

            });


        });

        $('body').on('click', '.view-user', function (e) {

            var user_id = $(this).data('id');
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                url: '/admin/getuserrolespayment',
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {user_id: user_id, _token: CSRF_TOKEN},
                success: function (data) {
                    $('#editform_display').html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                }
            });
        });

        var availability_reports_table = availabilityTable();

        function availabilityTable() {
            $('#sample_6').dataTable().fnClearTable();
            $('#sample_6').dataTable().fnDestroy();

            $("#sample_6_filter>label").css('float', 'right');
            $("#sample_6_length>label").css('float', 'left');

            return $('#sample_6').DataTable({
                responsive: true,
                serverSide: true,
                stateSave: true,
                processing: true,
                searching: false,
                "order": [[3, "ASC"]],
                "pageLength": 10,
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                stateSave: true,
                "oLanguage": {
                    "sLengthMenu": "_MENU_",
                    "sSearch": "_INPUT_"
                },
                "aTargets": ["_all"],
                initComplete: function () {
                    $("#sample_6_filter>label").css('float', 'right');
                    $("#sample_6_length>label").css('float', 'left');
                },

                ajax: {
                    url: '<?php echo e(route('reports.show.all.booked')); ?>',
                    data: function (d) {
                        d.booking_id = $('#search_bookingId').val();
                        d.search_company = $('#search_company').val();
                        d.search_block = $('#search_block').val();
                        d.search_date = $('#search_date').val();
                        d.search_name = $('#search_name').val();
                        d.search_surname = $('#search_surname').val();
                        d.search_lockernum = $('#search_lockernum').val();
                        d.search_input = $('#search-input').val();
                    }
                },
                columns: [
                    {data: 'booking_id', name: 'booking_id'},
                    {data: 'company_name', name: 'company_name'},
                    {data: 'block', name: 'block'},
                    {data: 'locker_number', name: 'locker_number'},
                    {data: 'child_first_name', name: 'child_first_name'},
                    {data: 'child_surname', name: 'child_surname'},
                    {data: 'net_pass', name: 'net_pass'},
                    {data: 'start', name: 'start'},
                    {data: 'end', name: 'end'},
                    {data: 'action', name: 'action'},
                ],
            });
        }

    </script>


@endsection