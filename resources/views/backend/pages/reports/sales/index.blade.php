@extends('backend.layouts.app')

@section('title', 'Lockers | Companies')
@section('style')
    <!--plugin styles-->
    <link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-switch/css/bootstrap-switch.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/switchery/css/switchery.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/radio_css/css/radiobox.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/checkbox_css/css/checkbox.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/modal/css/component.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-tagsinput/css/bootstrap-tagsinput.css"/>
    <link rel="stylesheet" type="text/css" href="/backend/vendors/animate/css/animate.min.css"/>
    <!-- end of plugin styles -->

    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/radio_checkbox.css"/>


    <link type="text/css" rel="stylesheet" href="/backend/vendors/chosen/css/chosen.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/form_elements.css"/>


    <style>

        @media (min-width: 768px) {
            .modal-xl {
                width: 90%;
                max-width: 1200px;
            }
        }

        .flex-container {
            display: flex;
            justify-content: center;
            flex-wrap: wrap;
        }

        .flex-container label {
            margin: 15px;
        }

        .ui-datepicker {
            width: 300px;
            height: 300px;
            margin: 5px auto 0;
            font: 12pt Arial, sans-serif;
            /*-webkit-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
            -moz-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);*/
        }

        .ui-datepicker table {
            width: 100%;
        }

        .ui-datepicker-header {
            background: #3399ff;
            color: #ffffff;
            font-family: 'Times New Roman';
            border-width: 1px 0 0 0;
            border-style: solid;
            border-color: #111;
        }

        .ui-datepicker-title {
            text-align: center;
            font-size: 15px;

        }

        .ui-datepicker-prev {
            float: left;
            cursor: pointer;
            background-position: center -30px;
        }

        .ui-datepicker-next {
            float: right;
            cursor: pointer;
            background-position: center 0px;
        }

        .ui-datepicker thead {
            background-color: #f7f7f7;

            /*border-bottom: 1px solid #bbb;*/
        }

        .ui-datepicker th {
            background-color: #808080;
            text-transform: uppercase;
            font-size: 8pt;
            color: #666666;
            /*text-shadow: 1px 0px 0px #fff;*/
            /*filter: dropshadow(color=#fff, offx=1, offy=0);*/
        }

        .ui-datepicker tbody td {
            padding: 0;
            /*border-right: 1px solid #808080;*/
        }

        .ui-datepicker tbody td:last-child {
            border-right: 0px;
        }

        .ui-datepicker tbody tr {
            border-bottom: 1px solid #bbb;
        }

        .ui-datepicker tbody tr:last-child {
            border-bottom: 0px;
        }

        .ui-datepicker a {
            text-decoration: none;
        }

        .ui-datepicker td span, .ui-datepicker td a {
            display: inline-block;
            /*font-weight: bold;*/
            text-align: center;
            width: 30px;
            height: 30px;
            line-height: 30px;
            color: #ffffff;
            /*text-shadow: 1px 1px 0px #fff;*/
            /*filter: dropshadow(color=#fff, offx=1, offy=1);*/
        }

        .ui-datepicker-calendar .ui-state-default {
            background: linear-gradient(#999999, #737373);
            color: #ffffff;
            height: 40px;
            width: 40px;

        }

        .ui-datepicker-calendar .ui-state-hover {
            background: #33adff;
            color: #FFFFFF;
        }

        .ui-datepicker-calendar .ui-state-active {
            background: #33adff;
            -webkit-box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1);
            -moz-box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1);
            box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1);
            color: #e0e0e0;
            text-shadow: 0px 1px 0px #4d7a85;
            border: 1px solid #55838f;
            position: relative;
            margin: -1px;
        }

        .ui-datepicker-unselectable .ui-state-default {
            background: #D6E4BE;
            color: #000;
        }


    </style>

@endsection
@section('content')
    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-building circle header_icon_margin"></i>
                            Reports
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        @if(Session::has('flash_message'))
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable float-right"
                     style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-hidden="true" style="margin-top: 4px; margin-left: 15px;"> ×
                    </button>
                    {{ Session::get('flash_message') }}
                </div>
            </div>
        @endif
        @if (session('status'))
            <div class="alert alert-danger">
                {{ session('status') }}
            </div>
        @endif

        @include('errors.success')
        @include('errors.error')
        <div class="outer">
            <div class="inner bg-container">
                <div class="row">
                    @include ('errors.list')
                    <div class="col-12 data_tables">
                        <div class="card m-t-35">
                            <div class="card-header bg-white">
                                <i class="fa fa-building circle header_icon_margin"></i> Sales Report
                            </div>

                            <div class="row p-t-15" style="margin-left: 12px">
                                <div class="col-md-2"><input type="text" id="search_currency" placeholder="Currency"
                                                             class="form-control"></div>
                                <div class="col-md-2"><input type="text"
                                                             onchange="searchByThis(availability_reports_table)"
                                                             id="search_start_date" class="form-control"
                                                             placeholder="Start Date"></div>
                                <div class="col-md-2"><input type="text"
                                                             onchange="searchByThis(availability_reports_table)"
                                                             id="search_end_date" class="form-control"
                                                             placeholder="End Date"></div>
                            </div>

                            <div class="card-body p-t-10">
                                <div class="row" style="width: 100% !important;">
                                    <div class="col-md-12 m-t-25">
                                        <div class="table-responsive">
                                            <table class="table  table-bordered table-hover  " style="width:100%"
                                                   id="sample_6">
                                                <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Number of Sales</th>
                                                    <th>Sale</th>
                                                    <th>Refund</th>
                                                    <th>Currency</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <!--plugin scripts-->
    <script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>
    <!-- end of plugin scripts -->

    <!--Page level scripts-->
    <script type="text/javascript" src="/backend/js/pages/datatable_server_side.js"></script>
    <script type="text/javascript" src="/backend/js/pages/radio_checkbox.js"></script>
    <script type="text/javascript" src="{{asset('/backend/js/common.js')}}"></script>
    <script src="{{ asset('custom/general.js') }}"></script>


    <script type="text/javascript" src="/backend/vendors/chosen/js/chosen.jquery.js"></script>
    <script type="text/javascript" src="/backend/js/pages/form_elements.js"></script>

    <!-- end of plugin scripts -->
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"
            integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk=" crossorigin="anonymous"></script>

    <!--Page level scripts-->
    <script type="text/javascript" src="/backend/js/pages/datatable_server_side.js"></script>
    <script type="text/javascript" src="{{asset('/backend/js/common.js')}}"></script>

    <script type="text/javascript">

        $(document).ready(function () {
            $("#search_start_date").datepicker();
            $("#search_end_date").datepicker();


            var searchFunction = 'searchByThis(availability_reports_table)';
            var payments_search = "{{\Illuminate\Support\Facades\Session::get('payments-search')}}";
            $('#sample_6_wrapper>.row:nth-child(1)>div:nth-child(2)').append('<label style="float:right;margin-top:30px;"><input class="form-control input-sm" id="search-input" value="' + payments_search + '" onblur="' + searchFunction + '"></label>');
            var oTable = $('#sample_6').dataTable();
            // Sometime later - filter...
            oTable.fnFilter(payments_search);


            $("#search_currency").keypress(function (event) {
                if (event.keyCode === 13) {
                    searchByThis(availability_reports_table);

                }

            });
            $("#search-input").keypress(function (event) {
                if (event.keyCode === 13) {
                    searchByThis(availability_reports_table);

                }

            });
        });

        var availability_reports_table = availabilityTable();

        function availabilityTable() {
            $('#sample_6').dataTable().fnClearTable();
            $('#sample_6').dataTable().fnDestroy();

            $("#sample_6_filter>label").css('float', 'right');
            $("#sample_6_length>label").css('float', 'left');

            return $('#sample_6').DataTable({
                responsive: true,
                serverSide: true,
                stateSave: true,
                processing: true,
                searching: false,
                "pageLength": 10,
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                "oLanguage": {
                    "sLengthMenu": "_MENU_",
                    "sSearch": "_INPUT_"
                },
                "aTargets": ["_all"],
                initComplete: function () {
                    $("#sample_6_filter>label").css('float', 'right');
                    $("#sample_6_length>label").css('float', 'left');
                },

                ajax: {
                    url: '<?php echo e(route('reports.show.all.sales')); ?>',
                    data: function (d) {
                        d.search_currency = $('#search_currency').val();
                        d.search_start_date = $('#search_start_date').val();
                        d.search_end_date = $('#search_end_date').val();
                        d.search_input = $('#search-input').val();
                    }
                },
                columns: [
                    {data: 'date', name: 'date'},
                    {data: 'sales_count', name: 'sales_count'},
                    {data: 'amount', name: 'amount'},
                    {data: 'refund', name: 'refund'},
                    {data: 'currency', name: 'currency'}
                ],
            });
        }


    </script>


@endsection