<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
<table>
    {{--{{dd($data)}}--}}
    <thead>
    <tr>
        <th>Booking ID</th>
        <th>Company</th>
        <th>Block</th>
        <th>Locker Number</th>
        <th>Name</th>
        <th>Surname</th>
        <th>Code</th>
        <th>Start</th>
        <th>End</th>

    </tr>
    </thead>
    <tbody>
    @foreach($data as $value)
        <tr>
            <td>

                {{$value->booking_id}}
            </td>
            <td>
                <?php

                $company= \App\Company::where('id', $value->company_id)->first();
                if(isset($company->name)){
                    $company_name = $company->name;
                } else {
                    $company_name = '';
                }
                ?>
                {{$company_name}}
            </td>
            <td>
                <?php
                $block= \App\Block::where('id', $value->block_id)->first();
                if(isset($block->name)){
                    $block_name = $block->name;
                } else {
                    $block_name = '';
                }
                ?>
                {{$block_name}}
            </td>
            <td>
                {{$value->locker_number}}
            </td>

            <td>

                {{$value->child_first_name}}
            </td>
            <td>

                {{$value->child_surname}}
            </td>
            <td>

                Code : {{ substr(sprintf("%04d",$value->net_pass),  -4)}}

            </td>
            <td>
                <?php
                $start = date("F jS, Y H:m", strtotime($value->start))
                ?>
                {{$start}}
            </td>
            <td>
                <?php
                $end = date("F jS, Y H:m", strtotime($value->end))
                ?>
                {{$end}}
            </td>



        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>
