<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
<table>
    {{--{{dd($data)}}--}}
    <thead>
    <tr>
             <th>Company Name</th>
             <th>Block</th>
             <th>Locker Number</th>
             <th>Manual Code</th>
             <th>Status</th>

    </tr>
    </thead>
    <tbody>
    @foreach($data as $value)
        <tr>
                 <td>
                 <?php

                     $company= \App\Company::where('id', $value->company_id)->first();
                     if(isset($company->name)){
                         $company_name = $company->name;
                     } else {
                         $company_name = '';
                     }
                     ?>
                      {{$company_name}}
                </td>
                <td>
                    <?php
                    $block= \App\Block::where('id', $value->block_id)->first();
                    if(isset($block->name)){
                    $block_name = $block->name;
                    } else {
                    $block_name = '';
                    }
                    ?>
                    {{$block_name}}
                </td>
                <td>
                    {{$value->locker_number}}
                </td>
               <td>
                   <?php

                   $manual_code = \App\ManualLockerCode::where('locker_id', $value->id)->where('start','<=', date('Y-m-d 00:00:00',strtotime($date_passed)))
                       ->where('end','>=', date('Y-m-d 00:00:00',strtotime($date_passed)))->first();

                   if(empty($manual_code)){
                       $code = "";
                   } else {

                       $code = substr(sprintf("%04d",$manual_code->code),  -4);
                   }
                   ?>
                   {{--{{dd($code)}}--}}
                       Code : {{ $code  }}
               </td>
               <td>
                   <?php
                   $lockerStatus = \App\Sale::Where('locker_id',$value->id)
                       ->where('start','<=',$this_month_end_date)
                       ->Where('end','>=',$this_month_start_date)
                       // ->where('start','<=',$this_month_start_date)
                       //  ->Where('end','>=',$this_month_end_date)
                       ->where(function ($status) {
                           $status->where('status', 'b')
                               ->orWhere('status', 'r');
                       })
                       ->orderBy('status', 'ASC')
                       ->first();

                   if(empty($lockerStatus->status)){
                       $status = "Available";
                   }
                   else if ($lockerStatus->status == 'b') {
                       $status =  "Booked";
                   } else if ($lockerStatus->status == 'r') {
                       $status =  "Reserved";
                   }
                   ?>
                   {{$status }}
               </td>


        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>
