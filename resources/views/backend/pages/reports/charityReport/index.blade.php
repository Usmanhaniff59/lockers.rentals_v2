@extends('backend.layouts.app')
@section('title', 'Charity | Report')
@section('style')
    <!--plugin styles-->
    <link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-switch/css/bootstrap-switch.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/switchery/css/switchery.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/radio_css/css/radiobox.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/checkbox_css/css/checkbox.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/modal/css/component.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-tagsinput/css/bootstrap-tagsinput.css"/>
    <link rel="stylesheet" type="text/css" href="/backend/vendors/animate/css/animate.min.css"/>
    <!-- end of plugin styles -->
    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/radio_checkbox.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/chosen/css/chosen.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/form_elements.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/datepicker-jquery.css"/>


    <style>
        @media (min-width: 768px) {
            .modal-xl {
                width: 90%;
                max-width: 1200px;
            }
        }

        .flex-container {
            display: flex;
            justify-content: center;
            flex-wrap: wrap;
        }

        .flex-container label {
            margin: 15px;
        }

        .loader {
            position: absolute;
            top: 15%;
            left: 45%;
            margin: auto;
            display: none;
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #00c0ef;
            width: 150px;
            height: 150px;
            -webkit-animation: spin 2s linear infinite;
            /* Safari */
            animation: spin 2s linear infinite;
        }

        .loader-percentage {
            position: absolute;
            top: 21%;
            left: 50%;
            margin: auto;
            display: none;
        }

        /* Safari */
        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }

        .excel-loader {
            display: none;
        }
    </style>
@endsection
@section('content')
    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-building circle header_icon_margin"></i>
                            Reports
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        <div class="outer">
            <div class="inner bg-container">
                <div class="loader" id="block_loader">
                </div>
                <h2 class="loader-percentage"><b id="percentage">0%</b></h2>
                <div id="lockers_div">
                    <div class="row">
                        @include ('errors.list')
                        <div class="col-12 data_tables">
                            <div class="card m-t-35">
                                <div class="card-header bg-white">
                                    <i class="fa fa-building circle header_icon_margin"></i> Charity Report
                                </div>
                                <div class="row p-t-15" style="margin-left: 12px">
                                    <button type="button" class="form_submit_check btn btn-warning  ml-2"
                                            onclick="spinner_start();exportTableToExcel()">
                                    <span class="assign_class label">
                                        EXCEL EXPORT
                                    </span>
                                    </button>
                                </div>
                                <div class="row p-t-15" style="margin-left: 12px; margin-right: 12px">
                                    <div class="col-md-2">
                                        <input type="text" id="search_year" placeholder="Year" class="form-control">
                                    </div>

                                    <div class="col-md-2">
                                        <select class="form-control chzn-select" id="search_month"
                                                placeholder="Select Month" tabindex="2">
                                            <option value="">Select Month</option>
                                            <option value="1">January</option>
                                            <option value="2">February</option>
                                            <option value="3">March</option>
                                            <option value="4">April</option>
                                            <option value="5">May</option>
                                            <option value="6">June</option>
                                            <option value="7">July</option>
                                            <option value="8">August</option>
                                            <option value="9">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="card-body p-t-10">
                                    <div class="row" style="width: 100% !important;">
                                        <div class="col-md-12 m-t-25">
                                            <div class="table-responsive">
                                                <table class="table  table-bordered table-hover  " style="width:100%"
                                                       id="charity_report">
                                                    <thead>
                                                    <tr>
                                                        <th>Year</th>
                                                        <th>Month</th>
                                                        <th> Currency</th>
                                                        <th>Bookings</th>
                                                        <th> Payment</th>
                                                        <th>Charity Transactions</th>
                                                        <th>Charity Revenue</th>
                                                        <th>Total Payment</th>
                                                        <th>Charity Rev(%)</th>
                                                        <th>Charity processed</th>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <!--plugin scripts-->
    <script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>
    <!-- end of plugin scripts -->

    <!--Page level scripts-->
    <script type="text/javascript" src="/backend/js/pages/datatable_server_side.js"></script>
    <script type="text/javascript" src="/backend/js/pages/radio_checkbox.js"></script>

    <script type="text/javascript" src="/backend/vendors/chosen/js/chosen.jquery.js"></script>
    <script type="text/javascript" src="/backend/js/pages/form_elements.js"></script>

    <!-- end of plugin scripts -->
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"
            integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk=" crossorigin="anonymous"></script>

    <!--Page level scripts-->
    <script type="text/javascript" src="/backend/js/pages/datatable_server_side.js"></script>
    <script type="text/javascript" src="{{asset('/backend/js/common.js')}}"></script>
    <script type="text/javascript" src="/backend/sheetjs/dist/xlsx.full.min.js"></script>
    <script type="text/javascript" src="/backend/filesaver/dist/FileSaver.js"></script>
    <script type="text/javascript">

        sessionStorage.setItem("booked_data", JSON.stringify([]));

        function spinner_start() {
            $(".loader").show();
            $(".loader-percentage").show();
            jQuery('#lockers_div').css('opacity', '0.05');
        }

        function spinner_close() {
            $(".loader").hide();
            $(".loader-percentage").hide();
            jQuery('#lockers_div').css('opacity', '1');
        }

        function exportTableToExcel() {
            $('#percentage').html(0 + '%');
            var ws_data = [['Year', ' Month', 'Currency', 'Bookings', ' Payment', 'Charity Transactions', ' Charity Revenue', 'Total Payment', 'Charity Rev(%)', 'Charity processed']];
            sessionStorage.setItem("booked_data", JSON.stringify(ws_data));
            var total_record = '';
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var search_year = $('#search_year').val();
            var search_month = $('#search_month').val();

            setTimeout(
                function () {
                    $.ajax({
                        url: '/charity/report/count',
                        type: 'POST',
                        dataType: 'JSON',
                        async: false,
                        data: {search_year: search_year, search_month: search_month, _token: CSRF_TOKEN},
                        success: function (response) {
                            console.log('export table to excel');
                            total_record = response;
                            console.log(total_record, 'total_record');
                            var number_of_requests = Math.ceil(total_record / 1000);
                            var percentage = parseFloat(100 / number_of_requests)
                            console.log(percentage, 'percentage');
                            var total_percentage = 0;
                            var page = 0;
                            var percentage_whole_number = 1;

                            sessionStorage.setItem("number_of_requests", number_of_requests);
                            sessionStorage.setItem("percentage", percentage);
                            sessionStorage.setItem("total_percentage", total_percentage);
                            sessionStorage.setItem("page", page);
                            sessionStorage.setItem("percentage_whole_number", percentage_whole_number);
                            get_excel_data();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR);
                        }
                    });
                }, 1000);
        }

        function get_excel_data() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var search_year = $('#search_year').val();
            var search_month = $('#search_month').val();
            var page = parseInt(sessionStorage.getItem("page"));
            var percentage_whole_number = parseInt(sessionStorage.getItem("percentage_whole_number"));

            $.ajax({
                url: '/charity/report/data',
                type: 'POST',
                dataType: 'JSON',
                data: {page: page, search_year: search_year, search_month: search_month, _token: CSRF_TOKEN},
                success: function (response) {
                    console.log('Get excel data');

                    var ws_data = JSON.parse(sessionStorage.getItem("booked_data"));
                    var total_percentage = parseFloat(sessionStorage.getItem("total_percentage"));
                    var percentage = parseFloat(sessionStorage.getItem("percentage"));
                    total_percentage = total_percentage + percentage;
                    total_percentage = total_percentage.toFixed(2);

                    if (percentage_whole_number < total_percentage) {

                        percentage_whole_number = percentage_whole_number + 1;
                        sessionStorage.setItem("percentage_whole_number", percentage_whole_number);
                        $('#percentage').html(Math.floor(total_percentage) + '%');
                    }

                    var number_of_requests = parseInt(sessionStorage.getItem("number_of_requests"));

                    $.each(response.data, function (index, val) {
                        if (val.month == 1) {
                            val.month = 'January'
                        }
                        if (val.month == 2) {
                            val.month = 'February'
                        }
                        if (val.month == 3) {
                            val.month = 'March'
                        }
                        if (val.month == 4) {
                            val.month = 'April'
                        }
                        if (val.month == 5) {
                            val.month = 'May'
                        }
                        if (val.month == 6) {
                            val.month = 'June'
                        }
                        if (val.month == 7) {
                            val.month = 'July'
                        }
                        if (val.month == 8) {
                            val.month = 'August'
                        }
                        if (val.month == 9) {
                            val.month = 'September'
                        }
                        if (val.month == 10) {
                            val.month = 'Octuber'
                        }
                        if (val.month == 11) {
                            val.month = 'November'
                        }
                        if (val.month == 12) {
                            val.month = 'December'
                        }
                        if (val.charity_processed == 1) {
                            var charity_processed = 'Yes';
                        } else {
                            var charity_processed = 'No';

                        }

                        var charity_revenue_percentage = val.charity_revenue_percentage + '%';
                        ws_data.push([val.year, val.month, val.currency, val.bookings, val.payment, val.charity_transaction, val.charity_revenue, val.total_payment, charity_revenue_percentage, charity_processed]);
                    });
                    sessionStorage.setItem("booked_data", JSON.stringify(ws_data));
                    sessionStorage.setItem("total_percentage", total_percentage);
                    if (page == number_of_requests - 1) {
                        var wb = XLSX.utils.book_new();
                        wb.Props = {
                            Title: "Academic Year Report",
                            Subject: "Academic Year Report",
                            Author: "Locker Rentals",
                            CreatedDate: new Date()
                        };
                        var ws_data = JSON.parse(sessionStorage.getItem("booked_data"));
                        var ws = XLSX.utils.aoa_to_sheet(ws_data);
                        var wbd = XLSX.utils.book_append_sheet(wb, ws, "Sheet" + page);
                        var wopts = {bookType: 'xlsx', bookSST: false, type: 'array'};
                        var wbout = XLSX.write(wb, wopts);
                        saveAs(new Blob([wbout], {type: "application/octet-stream"}), "CharityReport.xlsx");
                        spinner_close();
                    } else {
                        sessionStorage.setItem("page", page + 1);
                        get_excel_data()
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                }
            });
        }

        $(document).ready(function () {


            $("#search_year").keypress(function (event) {
                if (event.keyCode === 13) {
                    searchByThis(charity_reports_table);
                }
            });
            $("#search_month").change(function (event) {

                searchByThis(charity_reports_table);
            });
        });

        function searchByThis(table) {
            table.draw('page');
        }

        var charity_reports_table = charityPaymentReports();

        function charityPaymentReports() {
            $('#charity_report').dataTable().fnClearTable();
            $('#charity_report').dataTable().fnDestroy();

            $("#charity_report_filter>label").css('float', 'right');
            $("#charity_report_length>label").css('float', 'left');

            return $('#charity_report').DataTable({
                responsive: true,
                serverSide: true,
                stateSave: true,
                processing: true,
                searching: false,
                "pageLength": 10,
                "order": [[0, "asc"], [1, "asc"], [2, "desc"]],
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                "oLanguage": {
                    "sLengthMenu": "_MENU_",
                    "sSearch": "_INPUT_"
                },
                "aTargets": ["_all"],
                initComplete: function () {
                    $("#charity_report_filter>label").css('float', 'right');
                    $("#charity_report_length>label").css('float', 'left');
                },
                // "createdRow": function ( row, data, index ) {
                //     console.log(row,'row');
                //     console.log(data,'data');
                //     console.log(index,'index');
                //
                // },
                ajax: {
                    url: '<?php echo e(route('charity.report.show.all')); ?>',
                    data: function (d) {
                        d.search_year = $('#search_year').val();
                        d.search_month = $('#search_month').val();
                    }
                },
                columns: [
                    {data: 'year', name: 'year'},
                    {data: 'month', name: 'month'},
                    {data: 'currency', name: 'currency'},
                    {data: 'bookings', name: 'bookings'},
                    {data: 'payment', name: 'payment'},
                    {data: 'charity_transaction', name: 'charity_transaction'},
                    {data: 'charity_revenue', name: 'charity_revenue'},
                    {data: 'total_payment', name: 'total_payment'},
                    {data: 'charity_revenue_percentage', name: 'charity_revenue_percentage'},
                    {data: 'action', name: 'action', orderable: false, searchable: true}


                ],
            });
        }

        $('body').on('click', '.checkbox', function () {
            if ($(this).prop("checked") == true)
                var status = 1;
            else
                var status = 0;

            var id = $(this).data('id');
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: "/charity/report/update/checkbox",
                type: "POST",
                dataType: 'JSON',
                data: {id: id, status: status, _token: CSRF_TOKEN},
                success: function (response) {
                    searchByThis(charity_reports_table);
                },
            });
        });

    </script>


@endsection