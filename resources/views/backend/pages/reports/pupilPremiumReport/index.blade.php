@extends('backend.layouts.app')

@section('title', 'Lockers | Companies')
@section('style')
<!--plugin styles-->
<link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-switch/css/bootstrap-switch.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/switchery/css/switchery.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/radio_css/css/radiobox.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/checkbox_css/css/checkbox.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/modal/css/component.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-tagsinput/css/bootstrap-tagsinput.css" />
<link rel="stylesheet" type="text/css" href="/backend/vendors/animate/css/animate.min.css" />
<!-- end of plugin styles -->

<!--Page level styles-->
<link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/pages/radio_checkbox.css" />


<link type="text/css" rel="stylesheet" href="/backend/vendors/chosen/css/chosen.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/pages/form_elements.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/pages/datepicker-jquery.css" />


<style>
    @media (min-width: 768px) {
        .modal-xl {
            width: 90%;
            max-width: 1200px;
        }
    }

    .flex-container {
        display: flex;
        justify-content: center;
        flex-wrap: wrap;
    }

    .flex-container label {
        margin: 15px;
    }

    .loader {
        position: absolute;
        top: 15%;
        left: 45%;
        margin: auto;
        display: none;
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #00c0ef;
        width: 150px;
        height: 150px;
        -webkit-animation: spin 2s linear infinite;
        /* Safari */
        animation: spin 2s linear infinite;
    }

    .loader-percentage {
        position: absolute;
        top: 21%;
        left: 50%;
        margin: auto;
        display: none;
    }

    /* Safari */
    @-webkit-keyframes spin {
        0% {
            -webkit-transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
        }
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }

        100% {
            transform: rotate(360deg);
        }
    }

    .excel-loader {
        display: none;
    }
</style>

@endsection
@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
            <div class="row no-gutters">
                <div class="col-6">
                    <h4 class="m-t-5">
                        <i class="fa fa-building circle header_icon_margin"></i>
                        Reports
                    </h4>
                </div>
            </div>
        </div>
    </header>


    <div class="outer">
        <div class="inner bg-container">
            {{--<div style="z-index: 19000" class="excel-loader">--}}
            {{--<h2 style="position:absolute;top:25%;left:38%;color: #00c0ef" ><b id="percentage">0%</b></h2>--}}
            {{--<div class="spinner-border " style="position:absolute;width:200px;height:200px;top:15%;left:30%;color: #00c0ef"></div>--}}
            {{--</div>--}}
            <div class="loader" id="block_loader">
            </div>
            <h2 class="loader-percentage"><b id="percentage">0%</b></h2>
            <div id="lockers_div">
                <div class="row">

                    @include ('errors.list')

                    <div class="col-12 data_tables">
                        <div class="card m-t-35">
                            <div class="card-header bg-white">
                                <i class="fa fa-building circle header_icon_margin"></i> Pupil Premium Report 
                            </div>
                            {{--@if(Auth()->user()->hasPermissionTo('csv_plantandequipmentReport') || Auth::user()->all_companies == 1 )--}}
                            <form class="form-style" id="export_excel_project" method="POST"
                                action="{{ url('admin/reports/availability/excel/export') }}">
                                <div class="row p-t-15" style="margin-left: 12px">
                                    {!! csrf_field() !!}
                                    <button type="button" class="form_submit_check btn btn-warning  ml-2"
                                        onclick="spinner_start();exportTableToExcel()">
                                        <span class="assign_class label">
                                            EXCEL EXPORT
                                        </span>
                                    </button>
                                </div>
                            </form>

                            <!--  -->
                            <div class="row">
        <div class="col-md-12" style="margin: 24px 1px 0 0;">
            <div class="form-group col-md-4" style="float:left;">
                <label><b>Live/Archive Filters</b></label>
                <div class="clearfix"></div>
                <label class="custom-control custom-radio signin_radio1 d-inline-block pr-3" style="margin-left: 10px;">
        <input id="move_radio" name="search_block" value="Live" type="radio" class="custom-control-input search-input">
        <span class="custom-control-label"></span>
        <span class="custom-control-description">Live</span>
    </label>
    <label class="custom-control custom-radio signin_radio1 d-inline-block pr-3" style="margin-left: 10px;">
        <input id="move_radio" name="search_block" value="Archive" type="radio" class="custom-control-input search-input">
        <span class="custom-control-label"></span>
        <span class="custom-control-description">Archive</span>
    </label>
      <label class="custom-control custom-radio signin_radio1 d-inline-block pr-3" style="margin-left: 10px;">
        <input id="move_radio" checked name="search_block" value="All" type="radio" class="custom-control-input search-input">
        <span class="custom-control-label"></span>
        <span class="custom-control-description">All</span>
    </label>
               <!--  <label><input type="radio" name="search_block" class="search-input" value="Live"><span style="padding: 0 0px 0 15px;">Live</span></label>
                <label><input name="search_block" class="search-input" value="Archive" type="radio"><span style="padding:0 0px 0 15px;">Archive</span></label>
                <label><input checked type="radio" value="All" class="search-input" name="search_block"><span style="padding:0 0px 0 15px;">All</span></label> -->
                 
                <!-- </div> -->


            </div>
            <div class="form-group col-md-3" style="float:left;">
                <label><b>Status Filters</b></label>
                <div class="clearfix"></div>
                  <label class="custom-control custom-radio signin_radio1 d-inline-block pr-3" style="margin-left: 10px;">
        <input id="move_radio" name="status_filter" value="Booked" type="radio" class="custom-control-input search-input">
        <span class="custom-control-label"></span>
        <span class="custom-control-description">Booked</span>
    </label>
     <label class="custom-control custom-radio signin_radio1 d-inline-block pr-3" style="margin-left: 10px;">
        <input id="move_radio" name="status_filter" value="Lost" type="radio" class="custom-control-input search-input">
        <span class="custom-control-label"></span>
        <span class="custom-control-description">Lost</span>
    </label>
                <!--  <label><input type="radio" name="status_filter" class="search-input" value="Booked"><span style="padding: 0 0px 0 15px;">Booked</span></label>
                  <label><input type="radio" name="status_filter" class="search-input" value="Lost"><span style="padding: 0 0px 0 15px;">Lost</span></label> -->
            </div>
            <div class="col-md-3" style="float:left;">
                <label><b>Select Date</b></label>
                <div class="clearfix"></div>
                 <input type="text"  id="mid_date" class="form-control" placeholder="Select Date" >
            </div>
          <!--   <div class="form-group col-md-2 " style="float: right;">
                <label></label>
                <a href="#create_pupil_premium" class="input_create_pupil_premium_empty btn btn-primary create_pupil_premium_btn" data-toggle="modal" id=""
                     style="margin-top: 30px;"> Add</a>
            </div> -->
            {{--<div class="form-group" style="padding-right: 9.5rem !important;" id="toggle_switch">--}}
                {{--<div class="radio_basic_swithes_padbott text-right ">--}}
                    {{--<input type="checkbox" class="js-switch sm_toggle_company_voucher_assign" name="send_voucher_code" id="send_voucher_code"--}}
                            {{--/>--}}
                    {{--<span class="radio_switchery_padding">Send Voucher Code  </span>--}}
                    {{--<br />--}}
                {{--</div>--}}
            {{--</div>--}}

        </div>
        
    </div>
                            <!--  -->
                            <div class="row p-t-15" style="margin-left: 12px; margin-right: 12px">
                                {{--<div class="col-md-2">--}}
                                    {{--<input type="text" onchange="searchByThisBooked(availability_reports_table)" id="search_start_date" class="form-control" placeholder="Start date">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-2">--}}
                                    {{--<input type="text" onchange="searchByThisBooked(availability_reports_table)" id="search_end_date" class="form-control" placeholder="End date">--}}
                                {{--</div>--}}
                                                            <div class="row p-t-15" style="margin-left: 12px; margin-right: 12px">

                                <div class="col-md-2"><input type="text" id="search_bookingId" placeholder="Booking Id"
                                class="form-control"></div>


                            
                            <div class="col-md-2"><input type="text" id="search_company"
                                placeholder="Company" class="form-control"></div>    
                            
                            <div class="col-md-2"><input type="text" id="search_block"
                                placeholder="Block" class="form-control"></div>    
                           
                            <div class="col-md-2"><input type="text" id="search_lockerNo"
                                placeholder="Locker Number" class="form-control"></div>
                                <div class="col-md-2 " ><input type="text" id="search_name"
                                                               placeholder="Name" class="form-control"></div>
                            </div>

                            <!-- <div class="col-md-2"><input type="text" id="s"
                                placeholder="Month" class="form-control"></div>    -->

                            
                               
                            <!-- {{--@endif--}} -->

                            <!-- <div class="row p-t-15" style="margin-left: 12px"></div> -->

                            <div class="card-body p-t-10">
                                <div class="row" style="width: 100% !important;">
                                    <div class="col-md-12 m-t-25">
                                        <div class="table-responsive">
                                            <table class="table  table-bordered table-hover  " style="width:100%"
                                                id="PupilPremium_Report">
                                                <thead>
                                                    <tr>
                                                        <th>Booking Id</th>
                                                        <th>Company</th>
                                                        <th>Block</th>
                                                        <th>Locker Number</th>
                                                        <th>Child Name</th>
                                                        <th>Start</th>
                                                        <th>End</th>

                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<!--plugin scripts-->
<script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="/backend/vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>
<!-- end of plugin scripts -->

<!--Page level scripts-->
<script type="text/javascript" src="/backend/js/pages/datatable_server_side.js"></script>
<script type="text/javascript" src="/backend/js/pages/radio_checkbox.js"></script>
<script type="text/javascript" src="{{asset('/backend/js/common.js')}}"></script>

<script src="{{ asset('custom/general.js') }}"></script>

<script type="text/javascript" src="/backend/vendors/chosen/js/chosen.jquery.js"></script>
<script type="text/javascript" src="/backend/js/pages/form_elements.js"></script>

<!-- end of plugin scripts -->
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"
    integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk=" crossorigin="anonymous"></script>

<!--Page level scripts-->
<script type="text/javascript" src="/backend/js/pages/datatable_server_side.js"></script>
<script type="text/javascript" src="{{asset('/backend/js/common.js')}}"></script>
<script type="text/javascript" src="/backend/sheetjs/dist/xlsx.full.min.js"></script>
<script type="text/javascript" src="/backend/filesaver/dist/FileSaver.js"></script>

<script type="text/javascript">
   $(document).ready(function() {

$('#mid_date').datepicker()
    .on("input change", function (e) {
        voucherPaymentsTable();
    // console.log("Date changed: ", e.target.value);
});
            // $("#mid_date").datepicker();
        });
    sessionStorage.setItem("booked_data", JSON.stringify([]));

        function spinner_start() {
            $(".loader").show();
            $(".loader-percentage").show();
            jQuery('#lockers_div').css('opacity', '0.05');
        }
        function spinner_close() {
            // $(".excel-loader").hide();
            $(".loader").hide();
            $(".loader-percentage").hide();
            jQuery('#lockers_div').css('opacity', '1');
        }
        function exportTableToExcel(){
            $('#percentage').html(0+'%');

            var ws_data = [['Booking ID', 'Company','Block',' Locker Number',' Name', ' Start', ' end ']];
            sessionStorage.setItem("booked_data", JSON.stringify(ws_data));
            // wb.SheetNames.push("Test Sheet");
            // var ws = XLSX.utils.aoa_to_sheet(ws_data);
           var total_record = '';
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var search_bookingId = $('#search_bookingId').val();
            var search_name = $('#search_name').val();
            var search_company = $('#search_company').val();
            var search_block = $('#search_block').val();
            var search_lockerNo = $('#search_lockerNo').val();
            setTimeout(
                function()
                {

                    $.ajax({
                        url: '/pupil/premium/report/count',
                        type: 'POST',
                        dataType: 'JSON',
                        async: false,
                        data: {search_bookingId: search_bookingId,search_name: search_name,search_company: search_company,search_block: search_block,search_lockerNo: search_lockerNo,_token: CSRF_TOKEN},
                        success: function (response) {
                            console.log('export table to excel');
                            total_record = response;
                            console.log(total_record,'total_record');
                            var number_of_requests = Math.ceil(total_record/1000);
                            var percentage = parseFloat(100/number_of_requests)
                            console.log(percentage,'percentage');
                            var total_percentage = 0;
                            var page = 0;
                            var percentage_whole_number = 1;

                            sessionStorage.setItem("number_of_requests", number_of_requests);
                            sessionStorage.setItem("percentage", percentage);
                            sessionStorage.setItem("total_percentage", total_percentage);
                            sessionStorage.setItem("page", page);
                            sessionStorage.setItem("percentage_whole_number", percentage_whole_number);
                            get_excel_data();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR);
                        }
                    });


                }, 1000);
        }

        function get_excel_data(){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var search_bookingId = $('#search_bookingId').val();
            var search_name = $('#search_name').val();
            var search_company = $('#search_company').val();
            var search_block = $('#search_block').val();
            var search_lockerNo = $('#search_lockerNo').val();;
            var page =  parseInt(sessionStorage.getItem("page"));
            var percentage_whole_number =  parseInt(sessionStorage.getItem("percentage_whole_number"));
           

            $.ajax({
                url: '/pupil/premium/report/data',
                type: 'POST',
                dataType: 'JSON',

                data: {page:page,search_bookingId: search_bookingId,search_name: search_name,search_company: search_company,search_block: search_block,search_lockerNo: search_lockerNo,_token: CSRF_TOKEN},

                success: function (response) {
                            console.log('Get excel data');

                    var ws_data = JSON.parse(sessionStorage.getItem("booked_data"));

                    var total_percentage =  parseFloat(sessionStorage.getItem("total_percentage"));

                    var percentage =  parseFloat(sessionStorage.getItem("percentage"));
                    total_percentage = total_percentage + percentage;
                    total_percentage = total_percentage.toFixed(2);

                    if(percentage_whole_number < total_percentage ) {

                          percentage_whole_number = percentage_whole_number +1;
                        sessionStorage.setItem("percentage_whole_number", percentage_whole_number);
                        $('#percentage').html(Math.floor(total_percentage) + '%');
                    }

                    var number_of_requests =  parseInt(sessionStorage.getItem("number_of_requests"));

                    $.each(response.data ,function (index , val) {
                        var lockers_sold_percentage = val.lockers_sold_percentage+'%';
                        if(val.change_from_last_year_percentage < 0){
                            var change_from_last_year_percentage = '('+val.change_from_last_year_percentage+'%)';

                        }else{
                            var change_from_last_year_percentage = val.change_from_last_year_percentage+'%';

                        }
                        ws_data.push([ val.booking_id,val.company_name, val.block_name,val.locker_number,val.childName,val.start,val.end]);
                    });
                    sessionStorage.setItem("booked_data", JSON.stringify(ws_data));
                    sessionStorage.setItem("total_percentage", total_percentage);

                    if (page == number_of_requests - 1) {
                        var wb = XLSX.utils.book_new();
                        wb.Props = {
                            Title: "Academic Year Report",
                            Subject: "Academic Year Report",
                            Author: "Locker Rentals",
                            CreatedDate: new Date()
                        };

                        var ws_data = JSON.parse(sessionStorage.getItem("booked_data"));

                        var ws = XLSX.utils.aoa_to_sheet(ws_data);
                        var wbd = XLSX.utils.book_append_sheet(wb, ws, "Sheet" + page);

                        var wopts = {bookType: 'xlsx', bookSST: false, type: 'array'};

                        var wbout = XLSX.write(wb, wopts);

                        /* the saveAs call downloads a file on the local machine */
                        saveAs(new Blob([wbout],{type:"application/octet-stream"}), "PupilPremium.xlsx");
                        spinner_close();
                    }else{
                        sessionStorage.setItem("page",  page+1);
                        get_excel_data()
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                }
            });
        }
        $(document).ready(function() {
            var searchFunction = 'searchByThis(voucher_payments_table)';
            var payments_search = "{{\Illuminate\Support\Facades\Session::get('payments-search')}}";
            $('#academic_year_wrapper>.row:nth-child(1)>div:nth-child(2)').append('<label style="float:right;margin-top:30px;"><input class="form-control input-sm" id="search-input" value="' + payments_search + '" onblur="' + searchFunction + '"></label>');
              var oTable = $('#PupilPremium_Report').dataTable();
              // Sometime later - filter...
              oTable.fnFilter(payments_search);
            $("#search_bookingId").keypress(function(event) {
                if (event.keyCode === 13) {
                    searchByThis(voucher_payments_table);   
                }
            });
            $("#search_name").keypress(function(event) {
                if (event.keyCode === 13) {
                    searchByThis(voucher_payments_table);
                }
            });
            $("#search_company").keypress(function(event) {
                if (event.keyCode === 13) {
                    searchByThis(voucher_payments_table);
                }
            });
            $("#search_block").keypress(function(event) {
                if (event.keyCode === 13) {
                    searchByThis(voucher_payments_table);
                }
            });
            $("#search_lockerNo").keypress(function(event) {
                if (event.keyCode === 13) {
                    searchByThis(voucher_payments_table);
                }
            });
        });
        function searchByThisBooked(table){
            table.draw('page');
        }
        var voucher_payments_table = voucherPaymentsTable();
        $('.search-input').click(function(){
       voucherPaymentsTable();
    });
        function voucherPaymentsTable() {
            $('#PupilPremium_Report').dataTable().fnClearTable();
            $('#PupilPremium_Report').dataTable().fnDestroy();

            $("#academic_year_filter>label").css('float', 'right');
            $("#academic_year_length>label").css('float', 'left');
            return $('#PupilPremium_Report').DataTable({
                responsive:true,
                serverSide: true,
                stateSave: true,
                processing: true,
                searching: false,
                "pageLength" : 10,
                "order": [[ 0, "asc" ], [ 1, "asc" ],[ 2, "asc" ]],
                "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
                 "oLanguage": {
                      "sLengthMenu": "_MENU_",
                            "sSearch": "_INPUT_"
                    },
                    "aTargets": [ "_all" ],
                    initComplete : function() {
                        $("#academic_year_filter>label").css('float', 'right');
                        $("#academic_year_length>label").css('float', 'left');
                    },
                ajax: {
                    url: '<?php echo e(route('pupil.premium.report.show.all')); ?>',
                        data: function (d) {
                             d.search_bookingId = $('#search_bookingId').val();
                             d.search_name = $('#search_name').val();
                             d.search_company = $('#search_company').val();
                             d.search_block = $('#search_block').val();
                             d.search_lockerNo = $('#search_lockerNo').val();
                             d.searchParam = $('input[name="search_block"]:checked').val();
                             d.searchStatus = $('input[name="status_filter"]:checked').val();
                             d.searchDate = $('#mid_date').val();

                        }
                },
                columns: [
                    {data: 'booking_id', name: 'booking_id'},
                    {data: 'company_name', name: 'company_name'},
                    {data: 'block_name', name: 'block_name'},
                    {data: 'locker_number', name: 'locker_number'},
                    {data: 'childName', name: 'childName'},
                    {data: 'start', name: 'start'},
                    {data: 'end', name: 'end'}
                ],
            });
        }
    
</script>


@endsection