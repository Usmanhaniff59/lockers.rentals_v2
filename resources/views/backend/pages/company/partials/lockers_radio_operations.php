<div class="form-group col-md-4">
    <label class="custom-control custom-radio signin_radio1 d-inline-block pr-3">
        <input id="display_radio" name="radio1" value="display" type="radio" class="custom-control-input" checked>
        <span class="custom-control-label"></span>
        <span class="custom-control-description">Renumber</span>
    </label>

    <label class="custom-control custom-radio signin_radio2 d-inline-block" style="margin-left: 10px;">
        <input id="blank_radio" name="radio1" type="radio" value="blank" class="custom-control-input">
        <span class="custom-control-label"></span>
        <span class="custom-control-description ">Blank </span>
    </label>

    <label class="custom-control custom-radio signin_radio2 d-inline-block" style="margin-left: 10px;">
        <input id="maintenance_radio" name="radio1" type="radio" value="maintenance" class="custom-control-input">
        <span class="custom-control-label"></span>
        <span class="custom-control-description">Maintenance</span>
    </label>
    <label class="custom-control custom-radio signin_radio2 d-inline-block">
        <input id="onoffsale" name="radio1" type="radio" value="on_off_sale" class="custom-control-input">
        <span class="custom-control-label"></span>
        <span class="custom-control-description">On Sale/Off Sale</span>
    </label>
    <label class="custom-control custom-radio signin_radio1 d-inline-block pr-3" style="margin-left: 10px;">
        <input id="move_radio" name="radio1" value="move" type="radio" class="custom-control-input">
        <span class="custom-control-label"></span>
        <span class="custom-control-description">Move WithIn Block</span>
    </label>

    <label class="custom-control custom-radio signin_radio1 d-inline-block pr-3" style="">
        <input id="move_out_side_block_radio" name="radio1" value="move_out_side_block" type="radio"
            class="custom-control-input">
        <span class="custom-control-label"></span>
        <span class="custom-control-description">Move Outside Block</span>
    </label>


    <label class="custom-control custom-radio signin_radio2 d-inline-block" style="margin-left: 10px;">
        <input id="delete_radio" name="radio1" type="radio" value="delete" class="custom-control-input">
        <span class="custom-control-label"></span>
        <span class="custom-control-description">Delete/Restore </span>
    </label>
</div>

<div class="col-md-2 display_edit_div display-on-load">
    <button type="button" id="locker_active_refresh" class="btn btn-primary pull-right"
        style="display: inline-block; float:left;">Refresh</button>
</div>
<div class="form-group col-md-2 display_edit_div display-on-load">
    <input name="locker_name" id="locker_name" class="form-control" type="number" value="" min="1" max="999" readonly>
    <input type="hidden" name="locker_id" id="locker_id" value="">
    <label id="error_label" style="color: red;"></label>
</div>
<div class="form-group col-md-2 display_edit_div display-on-load" style="max-width:10.6666666667%">
    <input name="roles" id="reorder_checkbox" type="checkbox" value="1">
    <label for="reorder_checkbox">Reorder</label>
</div>
<div class="col-md-2 display_edit_div display-on-load">
    <button type="button" id="locker_active_submit" class="btn btn-primary pull-right"
        style="display: inline-block; float:left;">Submit</button>
</div>


<div class="col-md-2 display_blank_div no-display-on-load">
    <button type="button" id="locker_blank_submit" class="btn btn-primary pull-right"
        style="display: inline-block; float:left;">Submit</button>
</div>
<div class="col-md-2 display_delete_div d-none no-display-on-load">
    <button type="button" id="locker_delete_submit" class="btn btn-primary pull-right"
        style="display: inline-block; float:left;">Submit</button>
</div>
<div class="form-group col-md-3 display_maintenance_div d-none no-display-on-load">
    <select id="maintenance_type" class="form-control" placeholder="Type">
        <option value="">select Type</option>
        <option value="cleaning">Cleaning</option>
        <option value="fixing">Fixing</option>
    </select>
    <label id="error_maintenance_type" style="color: red;"></label>
</div>
<div class="form-group col-md-3 display_maintenance_div d-none no-display-on-load">
    <input type='text'
    <input type='text'
           class="form-control datepicker" id="maintenance_till_date"
           name="end" placeholder="Maintenance till"
    />
<!--    <input type="text" id="maintenance_till_date" class="form-control" >-->
    <label id="error_maintenance_till_date" style="color: red;"></label>
</div>
<div class="col-md-2 display_maintenance_div d-none no-display-on-load">
    <button type="button" id="locker_maintenance_submit" class="btn btn-primary pull-right"
        style="display: inline-block; float:left;">Submit</button>
</div>


<div class="form-group col-md-2 display_move_div d-none no-display-on-load">
    <input name="move_locker_number" id="move_locker_number" class="form-control" type="number" value="" min="1"
        max="999" placeholder="Locker Number">
    <label id="error_label_move" style="color: red;"></label>
</div>
<div class="col-md-2 display_move_div d-none no-display-on-load">
    <button type="button" id="locker_move_submit" class="btn btn-primary pull-right"
        style="display: inline-block; float:left;">Submit</button>
</div>

<div class="form-group col-md-4 display_move_out_side_block_div d-none no-display-on-load">

    <!--        <label for="sel1">Select Block:</label>-->
    <select class="form-control" id="move_locker_outside_block">
    </select>

    <label id="error_label_move_outside_block" style="color: red;"></label>
</div>
<div class="col-md-2 display_move_out_side_block_div d-none no-display-on-load">
    <button type="button" id="locker_move_out_side_block_submit" class="btn btn-primary pull-right"
        style="display: inline-block; float:left;">Submit</button>
</div>

<div class="col-md-2 display_on_off_sale_div d-none no-display-on-load">
    <button type="button" id="locker_on_off_sale_submit" class="btn btn-primary pull-right"
        style="display: inline-block; float:left;">Submit</button>
</div>