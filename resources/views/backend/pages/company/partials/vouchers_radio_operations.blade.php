
    <label class="custom-control custom-radio  d-inline-block "style="margin-left: 10px;">
        <input id="all_vouchers" name="voucher" value="all" type="radio" class="custom-control-input vouchers-radio" checked>
        <span class="custom-control-label"></span>
        <span class="custom-control-description">All</span>
    </label>
    <label class="custom-control custom-radio  d-inline-block "  style="margin-left: 10px;">
        <input id="used_vouchers" name="voucher" type="radio" value="used" class="custom-control-input vouchers-radio">
        <span class="custom-control-label"></span>
        <span class="custom-control-description ">Used </span>
    </label>
    <label class="custom-control custom-radio  d-inline-block " style="margin-left: 10px;">
        <input id="issued_vouchers" name="voucher" type="radio" value="issued" class="custom-control-input vouchers-radio">
        <span class="custom-control-label"></span>
        <span class="custom-control-description ">Issued </span>
    </label>
    <label class="custom-control custom-radio  d-inline-block " style="margin-left: 10px;">
        <input id="not_issued_vouchers" name="voucher" type="radio" value="not_issued" class="custom-control-input vouchers-radio">
        <span class="custom-control-label"></span>
        <span class="custom-control-description ">Not Issued</span>
    </label>
