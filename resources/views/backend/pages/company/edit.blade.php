<!--- Edit model -->
<div class="modal fade in display_none" id="edit" style="overflow-y: scroll;" tabindex="-1" role="dialog"
    aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <form class="modal-content" id="edit_company" action="{{ route('updatecompany') }}" method="POST">
            @csrf
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <input type="hidden" id="company_id">
            <div class="modal-body">

                <h3 class="m-t-5" style="margin-left: 10px;margin-top: 20px;" id="comany_name_header"></h3>


                <hr>
                <div class="row">
                    <div class="col-lg-12">

                        <div class="card m-t-35">
                            <div class="card-header bg-white edit-card-header">
                                <ul class="nav nav-tabs card-header-tabs float-left">
                                    <li class="nav-item">
                                        <a class="nav-link tabs active" href="#location-group" data-toggle="tab"
                                            id="location_group_tab">Location Group</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link tabs" href="#general-info" data-toggle="tab"
                                            id="general_info_tab">General Info</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link tabs" href="#company-info" data-toggle="tab"
                                            id="company_info_tab">Info</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link tabs" href="#school-years" data-toggle="tab">School Academic
                                            Years</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link tabs" href="#status" data-toggle="tab">Status</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#blocks" data-toggle="tab" id="blocks_tab">Blocks</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#lockers" data-toggle="tab" id="lockers_tab"
                                            onclick="clickLocker()">Lockers</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#chats" data-toggle="tab" id="chats_tab">Chat</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#documents" data-toggle="tab"
                                            id="documents_tab">Document</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#vouchers" data-toggle="tab"
                                            id="vouchers_tab">Voucher</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#pupil-premium" data-toggle="tab"
                                           id="pupil_premium_tab">Pupil Premium</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#user-assign" data-toggle="tab"
                                           id="user_assign">Users</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content text-justify">
                                    <div class="tab-pane active" id="location-group">
                                        <br>
                                        <div class='form-group' id="location_groups"></div>
                                    </div>
                                    <div class="tab-pane" id="general-info">
                                        <br>

                                        <input type="hidden" class="form-control" name="id" id="comapny_id">
                                        <input type="hidden" class="form-control" name="location_group_idd"
                                            id="location_group_idd">


                                        <div class="row">
                                            <div class="form-group name-container col-md-6">
                                                <label>Name</label>
                                                <input type="text" class="form-control" name="name_comp" id="name_edit">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Name Number</label>
                                                <input type="text" class="form-control" name="name_number_comp"
                                                    id="name_number_edit">

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Street</label>
                                                <input type="text" class="form-control" name="street_comp"
                                                    id="street_edit">

                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>City</label>
                                                <input type="text" class="form-control" name="city_comp" id="city_edit">

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Country</label>
                                                <input type="text" class="form-control" name="country_comp"
                                                    id="country_edit">

                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Postal Code</label>
                                                <input type="text" class="form-control" name="post_code"
                                                    id="post_code_edit">

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label>Note</label>
                                                <textarea type="text" class="form-control" name="note"
                                                    id="note_edit"></textarea>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="currency" id="gbp_edit"
                                                                value="GBP">
                                                            <span class="cr"><i class="cr-icon fa fa-circle"></i></span>
                                                            GBP
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="currency" value="EUR"
                                                                id="eur_edit">
                                                            <span class="cr"><i class="cr-icon fa fa-circle"></i></span>
                                                            EUR
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                         {{--<div class="row ">--}}
                                             {{--<div class="form-group col-md-12">--}}
                                                 {{--<div class="radio_basic_swithes_padbott stop_auto_rebook_edit">--}}
                                                     {{--<input type="checkbox" class="js-switch sm_toggle_stop_auto_rebook_edit" name="stop_auto_rebook" value="0" id="stop_auto_rebook_edit" />--}}
                                                     {{--<span class="radio_switchery_padding">Stop Auto Rebook</span>--}}
                                                     {{--<br />--}}
                                                 {{--</div>--}}
                                             {{--</div>--}}
                                         {{--</div> --}}
                                    </div>

                                    <div class="tab-pane" id="school-years">
                                        <br>
                                        @foreach($school_academic_year as $value)
                                        <input type="checkbox" name="roles[]" value="{{$value->id}}"
                                            id="edit_school_company_{{$value->id}}" />
                                        <span class="radio_switchery_padding">{{$value->school_year}}</span>
                                        <br />
                                        @endforeach
                                    </div>


                                    <div class="tab-pane" id="status">
                                        <br>
                                        <div id="company_status_edit"></div>
                                        <div class="form-group">
                                            <div class="radio_basic_swithes_padbott">
                                                <input type="checkbox" class="js-switch sm_toggle_active" name="active"
                                                    id="active_edit" />
                                                <span class="radio_switchery_padding">Active</span>
                                                <br />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="radio_basic_swithes_padbott">
                                                <input type="checkbox" class="js-switch sm_toggle_display"
                                                    name="display" id="display_edit" />
                                                <span class="radio_switchery_padding">Display</span>
                                                <br />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="radio_basic_swithes_padbott">
                                                <input type="checkbox" class="js-switch sm_toggle_display"
                                                    name="live" value="1" id="live_edit" />
                                                <span class="radio_switchery_padding">Go Live</span>
                                                <span id="golive_error" style="color: red;"></span>
                                                <br />
                                            </div>
                                        </div>
                                        @can('All Records')
                                        <div class="form-group">
                                            <select type="text" id="status_type" name="status_type"
                                                placeholder="Status Type" class="form-control">
                                                <option value="">Select Status Type</option>
                                                <option value="company">Customer</option>
                                                <option value="leads">LEADS</option>
                                                <option value="prospective">Prospective</option>
                                                <option value="unmatched">NOT ASSIGNED</option>

                                            </select>
                                        </div>
                                        @endcan
                                        <div class="form-group">
                                            <div class="form-group custom-controls-stacked">
                                                <input type="radio" class="js-switch sm_toggle_keep_block_edit"
                                                    name="keep_block" id="keep_block_edit" value="1" />
                                                <span class="radio_switchery_padding">Allow users to rebook this same
                                                    locker</span>
                                                <br />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-group custom-controls-stacked">
                                                <input type="radio" class="js-switch sm_toggle_keep_block_rebook_edit"
                                                    name="keep_block" id="keep_block_edit_rebook" value="2" />
                                                <span class="radio_switchery_padding">Allow users to rebook a
                                                    locker</span>
                                                <br />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-group custom-controls-stacked">
                                                <input type="radio"
                                                    class="js-switch sm_toggle_keep_block_no_rebook_edit"
                                                    name="keep_block" id="keep_block_edit_no_rebook" value="3" />
                                                <span class="radio_switchery_padding">Do not allow users to rebook a
                                                    locker next year</span>
                                                <br />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="blocks">
                                        @include('backend.pages.company.blocks.index')
                                        <br>
                                    </div>

                                    <div class="tab-pane" id="chats">
                                        @include('backend.pages.company.chats.index')
                                        <br>
                                    </div>

                                    <div class="tab-pane" id="documents">
                                        @include('backend.pages.company.documents.index')
                                        <br>
                                    </div>

                                    <div class="tab-pane" id="lockers">
                                        @include('backend.pages.company.lockers.index')
                                        <br>
                                    </div>
                                    <div class="tab-pane" id="vouchers">
                                        @include('backend.pages.company.vouchers.index')
                                        <br>
                                    </div>
                                    <div class="tab-pane" id="pupil-premium">
                                        @include('backend.pages.company.pupilPremium.index')
                                        <br>
                                    </div>
                                    <div class="tab-pane" id="company-info">
                                        @include('backend.pages.company.info.index')
                                        <br>
                                    </div>
                                    <div class="tab-pane" id="user-assign">
                                        @include('backend.pages.company.users.index')
                                        <br>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer edit_footer">
                <button type="button" id="save_company" class="btn btn-primary pull-right "> Save Changes</button>
                <button type="button" data-dismiss="modal" class="btn pull-right btn-light"
                    style="margin-right: 20px">Close</button>
            </div>

        </form>
        <br>
    </div>
</div>

<script type="text/javascript">
    // vouchersTable();

    // new Switchery(document.querySelector('.sm_toggle_company_voucher_assign'), { size: 'small', color: '#00c0ef', jackColor: '#fff' });


</script>