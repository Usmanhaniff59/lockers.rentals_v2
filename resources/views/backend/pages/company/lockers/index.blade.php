<style>
</style>
<a href="#create_block" data-toggle="modal"></a>
<br>
<div class="card-body p-t-10">

    <div class=" m-t-25">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body" style="padding:0 1.25rem 0rem 1.25rem !important;">
                        <div class="row" style="margin-top: 20px !important;">
                            @include('backend.pages.company.partials.lockers_radio_operations')
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-9">
                <label for="sel1">Select Block:</label>
                <select class="form-control" id="blocksul">
                </select>
            </div>
            <div class="form-group m-t-40">
                <input name="roles" id="deleted_checkbox" type="checkbox" value="1">
                <label for="deleted_checkbox">Recycle bin</label>

            </div>

        </div>

        <div class=" m-t-35">
            <div class="border_box">
                @include('backend.pages.company.partials.locker_types_color')
            </div>
        </div>

        <br>

        <div style="overflow-x:auto;">
            <div class="loader" id="block_loader">

            </div>

            <div id="lockers_div"></div>
        </div>
    </div>
</div>


<script type="text/javascript" src="/backend/js/jquery.min.js"></script>
<script type="text/javascript" src="/backend/js/blocks/block_lockers.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"
    integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk=" crossorigin="anonymous"></script>

<script type="text/javascript">


        //lockers not editable
       function clickOnlockerNotEditable(id,locker_number,active,display){
                   $("#error_label").html("");

                   var radio_option = $('input[name=radio1]:checked').val();
                   var locker_active = 0;
                   var locker_display = 0;
                   var checkboxvalue = 0;
                var  selected_lockers = JSON.parse(sessionStorage.getItem("selected_lockers_backend"));

                if($("#deleted_checkbox").prop("checked") == true){
                           checkboxvalue = 1;
                   }
                   else if($("#deleted_checkbox").prop("checked") == false){
                           checkboxvalue = 0;
                   }


                   if(radio_option == 'display'){
                       $("#locker_name").attr("readonly", false);
                       $("#locker_name").val(locker_number);
                       $("#locker_id").val(id);
                   }

                   else if(radio_option == 'blank'){

                    if(checkboxvalue == 0){

                        var locker_checed = $('#locker'+id).prop("checked");

                        if(locker_checed == false){

                            selected_lockers.push([locker_number, id]);
                            sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));
                            console.log(selected_lockers,'selected_lockers in blank checked');

                            $('#locker'+id).prop('checked', true);

                            $('#locker'+id).css("transform", "scale(1, 1)");
                            $('#locker'+id).css("border", "5px solid #66ff66");

                            $('#'+id).css("transform", "scale(1, 1)");
                            $('#'+id) .css("border", "5px solid #66ff66");

                        }else if(locker_checed == true){
                            const  selected_lockers_in_deselect = JSON.parse(sessionStorage.getItem("selected_lockers_backend"));
                            $.each(selected_lockers_in_deselect ,function (index, value) {
                                console.log(value,'value');
                                if(value[0] == locker_number && value[1] == id ){
                                    // console.log('same');
                                    selected_lockers_in_deselect.splice(index, 1);

                                    sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers_in_deselect));
                                }
                                console.log(selected_lockers_in_deselect,'selected_lockers in blank unchecked');

                            });

                            $('#locker'+id).prop('checked', false);

                            $('#locker'+id).css("transform", "scale(1, 1)");
                            $('#'+id).css("transform", "scale(1, 1)");
                            if($('#locker'+id).data('blank') == true) {
                                $('#locker' + id).css("border", "5px solid #9d9d9d");
                                $('#'+id) .css("border", "5px solid #9d9d9d");
                            }else{

                                $('#locker' + id).css("border", "5px solid #F1F1F1");
                                $('#'+id) .css("border", "5px solid #F1F1F1");
                            }

                        }

                    }

                }

                   else if(radio_option == 'on_off_sale'){

               var locker_checed = $('#locker'+id).prop("checked");

               if(locker_checed == false){

                   selected_lockers.push([locker_number, id]);
                   sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));
                   console.log(selected_lockers,'selected_lockers');

                   $('#locker'+id).prop('checked', true);

                   $('#locker'+id).css("transform", "scale(1, 1)");
                   $('#locker'+id).css("border", "5px solid #66ff66");

                   $('#'+id).css("transform", "scale(1, 1)");
                   $('#'+id) .css("border", "5px solid #66ff66");

               }else if(locker_checed == true){
                   const  selected_lockers_in_deselect = JSON.parse(sessionStorage.getItem("selected_lockers_backend"));
                   $.each(selected_lockers_in_deselect ,function (index, value) {
                       console.log(value,'value');
                       if(value[0] == locker_number && value[1] == id ){
                           // console.log('same');
                           selected_lockers_in_deselect.splice(index, 1);
                           console.log(selected_lockers_in_deselect);
                           sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers_in_deselect));
                       }
                   });

                   $('#locker'+id).prop('checked', false);

                   $('#locker'+id).css("transform", "scale(1, 1)");
                   $('#'+id).css("transform", "scale(1, 1)");

                   if($('#locker'+id).data('blank') == true) {
                       $('#locker' + id).css("border", "5px solid #9d9d9d");
                       $('#'+id) .css("border", "5px solid #9d9d9d");
                   }else{

                       $('#locker' + id).css("border", "5px solid #F1F1F1");
                       $('#'+id) .css("border", "5px solid #F1F1F1");
                   }

               }


           }

                   else if(radio_option == 'maintenance'){

                       var locker_checed = $('#locker'+id).prop("checked");

                       if(locker_checed == false){

                           selected_lockers.push([locker_number, id]);
                           sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));
                           console.log(selected_lockers,'selected_lockers');

                           $('#locker'+id).prop('checked', true);

                           if($('#locker'+id).data('type') == 'maintenance') {
                               $('#locker' + id).css("border", "5px solid #9d9d9d");
                               $('#'+id) .css("border", "5px solid #9d9d9d");
                           }else{

                               $('#locker' + id).css("border", "5px solid #F1F1F1");
                               $('#'+id) .css("border", "5px solid #F1F1F1");
                           }

                           $('#locker'+id).css("transform", "scale(1, 1)");
                           $('#locker'+id).css("border", "5px solid #66ff66");

                           $('#'+id).css("transform", "scale(1, 1)");
                           $('#'+id) .css("border", "5px solid #66ff66");

                       }else if(locker_checed == true){
                           const  selected_lockers_in_deselect = JSON.parse(sessionStorage.getItem("selected_lockers_backend"));
                           $.each(selected_lockers_in_deselect ,function (index, value) {
                               console.log(value,'value');
                               if(value[0] == locker_number && value[1] == id ){
                                   // console.log('same');
                                   selected_lockers_in_deselect.splice(index, 1);
                                   console.log(selected_lockers_in_deselect);
                                   sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers_in_deselect));
                               }
                           });

                           $('#locker'+id).prop('checked', false);

                           $('#locker'+id).css("transform", "scale(1, 1)");
                           $('#'+id).css("transform", "scale(1, 1)");
                           if($('#locker'+id).data('type') == 'blank') {
                               $('#locker' + id).css("border", "5px solid #9d9d9d");
                               $('#'+id) .css("border", "5px solid #9d9d9d");
                           }else{

                               $('#locker' + id).css("border", "5px solid #F1F1F1");
                               $('#'+id) .css("border", "5px solid #F1F1F1");
                           }

                       }

                   }

                   else if(radio_option == 'delete'){

                       var locker_checed = $('#locker'+id).prop("checked");

                       if(locker_checed == false){

                           selected_lockers.push([locker_number, id]);
                           sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));
                           console.log(selected_lockers,'selected_lockers in delete checked');

                           $('#locker'+id).prop('checked', true);

                           $('#locker'+id).css("transform", "scale(1, 1)");
                           $('#locker'+id).css("border", "5px solid #66ff66");

                           $('#'+id).css("transform", "scale(1, 1)");
                           $('#'+id) .css("border", "5px solid #66ff66");

                       }else if(locker_checed == true){
                           const  selected_lockers_in_deselect = JSON.parse(sessionStorage.getItem("selected_lockers_backend"));

                           $.each(selected_lockers_in_deselect ,function (index, value) {
                               console.log(value,'value');
                               if(value[0] == locker_number && value[1] == id ){
                                   // console.log('same');
                                   selected_lockers_in_deselect.splice(index, 1);

                                   sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers_in_deselect));
                               }
                               console.log(selected_lockers,'selected_lockers in delete unchecked');
                           });

                           $('#locker'+id).prop('checked', false);

                           $('#locker'+id).css("transform", "scale(1, 1)");
                           $('#'+id).css("transform", "scale(1, 1)");
                           if($('#locker'+id).data('blank') == true) {
                               $('#locker' + id).css("border", "5px solid #9d9d9d");
                               $('#'+id) .css("border", "5px solid #9d9d9d");
                           }else{

                               $('#locker' + id).css("border", "5px solid #F1F1F1");
                               $('#'+id) .css("border", "5px solid #F1F1F1");
                           }

                       }

                   }

                   else if(radio_option == 'move'){

                               var locker_checed = $('#locker'+id).prop("checked");
                               console.log(selected_lockers,'selected_lockers');


                               if(locker_checed == false){
                                   //perviouse locker deselect

                                   selected_lockers.push([locker_number, id]);
                                   sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));
                                   console.log(selected_lockers,'selected_lockers');

                                   $('#locker'+id).prop('checked', true);

                                   $('#locker'+id).css("transform", "scale(1, 1)");
                                   $('#locker'+id).css("border", "5px solid #66ff66");

                                   $('#'+id).css("transform", "scale(1, 1)");
                                   $('#'+id) .css("border", "5px solid #66ff66");

                               }else if(locker_checed == true){
                                   const  selected_lockers_in_deselect = JSON.parse(sessionStorage.getItem("selected_lockers_backend"));
                                   $.each(selected_lockers_in_deselect ,function (index, value) {
                                       console.log(value,'value');
                                       if(value[0] == locker_number && value[1] == id ){
                                           // console.log('same');
                                           selected_lockers_in_deselect.splice(index, 1);
                                           console.log(selected_lockers_in_deselect);
                                           sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers_in_deselect));
                                       }
                                   });

                                   $('#locker'+id).prop('checked', false);

                                   $('#locker'+id).css("transform", "scale(1, 1)");
                                   $('#'+id).css("transform", "scale(1, 1)");

                                   if($('#locker'+id).data('blank') == true) {
                                       $('#locker' + id).css("border", "5px solid #9d9d9d");
                                       $('#'+id) .css("border", "5px solid #9d9d9d");
                                   }else{

                                       $('#locker' + id).css("border", "5px solid #F1F1F1");
                                       $('#'+id) .css("border", "5px solid #F1F1F1");
                                   }

                               }


                           }

                   else if(radio_option == 'move_out_side_block'){

                               var locker_checed = $('#locker'+id).prop("checked");
                               console.log(selected_lockers,'selected_lockers');

                               if(locker_checed == false){
                                   //perviouse locker deselect

                                   selected_lockers.push([locker_number, id]);
                                   sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));
                                   console.log(selected_lockers,'selected_lockers');

                                   $('#locker'+id).prop('checked', true);

                                   $('#locker'+id).css("transform", "scale(1, 1)");
                                   $('#locker'+id).css("border", "5px solid #66ff66");

                                   $('#'+id).css("transform", "scale(1, 1)");
                                   $('#'+id) .css("border", "5px solid #66ff66");

                               }else if(locker_checed == true){
                                   const  selected_lockers_in_deselect = JSON.parse(sessionStorage.getItem("selected_lockers_backend"));
                                   $.each(selected_lockers_in_deselect ,function (index, value) {
                                       console.log(value,'value');
                                       if(value[0] == locker_number && value[1] == id ){
                                           // console.log('same');
                                           selected_lockers_in_deselect.splice(index, 1);
                                           console.log(selected_lockers_in_deselect);
                                           sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers_in_deselect));
                                       }
                                   });

                                   $('#locker'+id).prop('checked', false);

                                   $('#locker'+id).css("transform", "scale(1, 1)");
                                   $('#'+id).css("transform", "scale(1, 1)");

                                   if($('#locker'+id).data('blank') == true) {
                                       $('#locker' + id).css("border", "5px solid #9d9d9d");
                                       $('#'+id) .css("border", "5px solid #9d9d9d");
                                   }else{

                                       $('#locker' + id).css("border", "5px solid #F1F1F1");
                                       $('#'+id) .css("border", "5px solid #F1F1F1");
                                   }

                               }


                           }

           }

        //lockers
       function lockerAction(selected_lockers,locker_active,locker_display,reorder){
           var csrf = $('meta[name=csrf-token]').attr("content");
           console.log(csrf,'csrf');
           $(".loader").show();
           jQuery('#lockers_div').css('opacity', '0.05');


           $.ajax({
               url: 'backend/posteditlocker',
               type: "post",
               data: {
                   selected_lockers: selected_lockers,
                   // locker_id: locker_id,
                   locker_active: locker_active,
                   locker_display: locker_display,
                   reorder: reorder,
                   _token: csrf,
               },
               success: function (response) {
                   if(response == 11){
                       $("#error_label").html("Locker Number Already in Use");
                   }
                   else{
                       //set empty selected array
                       var  selected_lockers =[];
                       sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));

                       $("#error_label").html("");
                       $('#lockerModal').modal('hide');

                       var block_id = $("#blocksul").val();
                       var checkboxvalue = 0;
                       if ($("#deleted_checkbox").prop("checked") == true) {
                           checkboxvalue = 1;
                       } else if ($("#deleted_checkbox").prop("checked") == false) {
                           checkboxvalue = 0;
                       }
                       create_lockers_ajax(block_id, checkboxvalue);

                   }

               },
               error: function (jqXHR, textStatus, errorThrown) {
                   console.log(textStatus, errorThrown);
               }
           });
       }

       function maintenanceAction(selected_lockers,maintenance_type,maintenance_till_date,locker_active,locker_display){
           var csrf = $('meta[name=csrf-token]').attr("content");
           $(".loader").show();
           jQuery('#lockers_div').css('opacity', '0.05');
           $.ajax({
               url: "backend/maintenancelocker",
               type: "post",
               data: {
                   selected_lockers: selected_lockers,
                   maintenance_type: maintenance_type,
                   maintenance_till_date: maintenance_till_date,
                   locker_active: locker_active,
                   locker_display: locker_display,
                   _token: csrf,
               },
               success: function (response) {
                   if(response == 11){
                       $("#error_label").html("Locker Number Already in Use");
                   }
                   else{
                       //set empty selected array
                       var  selected_lockers =[];
                       sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));

                       $("#error_label").html("");

                       $('#lockerModal').modal('hide');

                       var block_id = $("#blocksul").val();
                       var checkboxvalue = 0;
                       if ($("#deleted_checkbox").prop("checked") == true) {
                           checkboxvalue = 1;
                       } else if ($("#deleted_checkbox").prop("checked") == false) {
                           checkboxvalue = 0;
                       }
                       create_lockers_ajax(block_id, checkboxvalue);

                   }

               },
               error: function (jqXHR, textStatus, errorThrown) {
                   console.log(textStatus, errorThrown);
               }
           });
       }

       function onOffSaleAction(selected_lockers ,locker_active){
           var csrf = $('meta[name=csrf-token]').attr("content");
           $(".loader").show();
           jQuery('#lockers_div').css('opacity', '0.05');
           $.ajax({
               url: "backend/onOffSale",
               type: "post",
               data: {
                   selected_lockers: selected_lockers,
                   locker_active: locker_active,
                   _token: csrf,
               },
               success: function (response) {
                   if(response == 11){
                       $("#error_label").html("Locker Number Already in Use");
                   }
                   else{
                       //set empty selected array
                       var  selected_lockers =[];
                       sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));

                       $("#error_label").html("");

                       $('#lockerModal').modal('hide');

                       var block_id = $("#blocksul").val();
                       var checkboxvalue = 0;
                       if ($("#deleted_checkbox").prop("checked") == true) {
                           checkboxvalue = 1;
                       } else if ($("#deleted_checkbox").prop("checked") == false) {
                           checkboxvalue = 0;
                       }
                       create_lockers_ajax(block_id, checkboxvalue);

                   }

               },
               error: function (jqXHR, textStatus, errorThrown) {
                   console.log(textStatus, errorThrown);
               }
           });
       }

       function MoveWithinBlockAction(selected_lockers ,move_locker_number){
           var csrf = $('meta[name=csrf-token]').attr("content");
           $(".loader").show();
           jQuery('#lockers_div').css('opacity', '0.05');
           var url =  '{{ route('moveWithInBlock') }}';
           $.ajax({
               url: url,
               type: "post",
               data: {
                   selected_lockers: selected_lockers,
                   move_locker_number: move_locker_number,
                   _token: csrf,
               },
               success: function (response) {
                   if(response == 11){
                       $("#error_label").html("Locker Number Already in Use");
                   }
                   else{
                       //set empty selected array
                       var  selected_lockers =[];
                       sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));

                       $("#error_label").html("");

                       $('#lockerModal').modal('hide');

                       var block_id = $("#blocksul").val();
                       var checkboxvalue = 0;
                       if ($("#deleted_checkbox").prop("checked") == true) {
                           checkboxvalue = 1;
                       } else if ($("#deleted_checkbox").prop("checked") == false) {
                           checkboxvalue = 0;
                       }
                       create_lockers_ajax(block_id, checkboxvalue);

                   }

               },
               error: function (jqXHR, textStatus, errorThrown) {
                   console.log(textStatus, errorThrown);
               }
           });
       }

       function MoveOutSideBlockAction(selected_lockers ,block_id){

           var csrf = $('meta[name=csrf-token]').attr("content");
           $(".loader").show();
           jQuery('#lockers_div').css('opacity', '0.05');
           var url =  '{{ route('moveOutSideBlock') }}';
           $.ajax({
               url: url,
               type: "post",
               data: {
                   selected_lockers: selected_lockers,
                   block_id: block_id,
                   _token: csrf,
               },
               success: function (response) {
                   if(response == 11){
                       $("#error_label").html("Locker Number Already in Use");
                   }
                   else{
                       //set empty selected array
                       var  selected_lockers =[];
                       sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));

                       $("#error_label").html("");

                       $('#lockerModal').modal('hide');

                       var block_id = $("#blocksul").val();
                       var checkboxvalue = 0;
                       if ($("#deleted_checkbox").prop("checked") == true) {
                           checkboxvalue = 1;
                       } else if ($("#deleted_checkbox").prop("checked") == false) {
                           checkboxvalue = 0;
                       }
                       create_lockers_ajax(block_id, checkboxvalue);

                   }

               },
               error: function (jqXHR, textStatus, errorThrown) {
                   console.log(textStatus, errorThrown);
               }
           });
       }

       //when click on lockers tab
        function clickLocker(){
                 jQuery('#lockers_div').css('opacity', '0.05');
                var block_id = $("#blocksul").val();
                var checkboxvalue = 0;
                if($("#deleted_checkbox").prop("checked") == true){
                        checkboxvalue = 1;
                }
                else if($("#deleted_checkbox").prop("checked") == false){
                        checkboxvalue = 0;
                }
                create_lockers_ajax(block_id, checkboxvalue);
        }

        //when change deleted lockers recycle bin
        $('body').on('change','#deleted_checkbox', function() {
                $(".loader").show();
                jQuery('#lockers_div').css('opacity', '0.05');
                var checkboxvalue = 0;
                if($(this).prop("checked") == true){
                        checkboxvalue = 1;
                        $(".display_edit_div").hide();
                }
                else if($(this).prop("checked") == false){
                        checkboxvalue = 0;
                        $("#locker_name").attr("readonly", true);
                        $("#locker_name").val('');
                        $("#locker_id").val('');
                        var radio_option = $('input[name=radio1]:checked').val();
                        $("#error_label").html("");
                        if(radio_option == 'display'){
                                $(".display_edit_div").show();
                        }
                }
                var block_id = $("#blocksul").val();
                create_lockers_ajax(block_id, checkboxvalue);
        });


        $('body').on('change','#locker_active', function() {
                if($(this).val() == 'show'){
                        $("#reorder_checkbox").prop("disabled", false);
                        $("#reorder_checkbox_div").show();
                        $("#locker_name").show();
                }
                if($(this).val() == 'hide'){
                        $("#reorder_checkbox").prop("disabled", true);
                        $("#locker_name").hide();
                        $("#reorder_checkbox_div").hide();
                }
                if($(this).val() == 'delete'){
                        $("#reorder_checkbox").prop("disabled", true);
                        $("#reorder_checkbox_div").hide();
                        $("#locker_name").show();
                }
        });

        //blocks dropdown in lockers tab
        $('body').on('change','#blocksul', function() {
                //loader showing
                $(".loader").show();
                jQuery('#lockers_div').css('opacity', '0.05');
                var block_id = $(this).val();
                //is recycle bin is checked
                var checkboxvalue = 0;
                if($("#deleted_checkbox").prop("checked") == true){
                        checkboxvalue = 1;
                }
                else if($("#deleted_checkbox").prop("checked") == false){
                        checkboxvalue = 0;
                }

                //get lockers of selected block in dropdwon
                create_lockers_ajax(block_id, checkboxvalue);

        });

        $("#tabs").click("tabsselect", function (event, ui) {
                 var $tabs = $("#tabs").tabs();
                var selected = $tabs.tabs("option", "selected");
        });

        //get lockers of selected block in dropdwon
        function create_lockers_ajax(block_id, checkboxvalue){
          
                $.ajax({
                        type: "GET",
                        url: "backend/getlockersblock",
                        data: {
                                block_id: block_id,
                                checkboxvalue: checkboxvalue,
                        },
                        cache: false,
                        success: function(data){
                                $("#lockers_div").html(data.output);

                                //convert lockers rows to column in table
                                $("#lockers_div").each(function () {
                                        var $this = $(this);
                                        var newrows = [];
                                        $this.find("tr").each(function () {
                                                var i = 0;
                                                $(this).find("td").each(function () {
                                                        i++;
                                                        if (newrows[i] === undefined) {
                                                                newrows[i] = $("<tr></tr>");
                                                        }
                                                        newrows[i].append($(this));
                                                });
                                        });
                                        $this.find("tr").remove();
                                        $.each(newrows, function () {
                                                $this.append(this);
                                        });
                                        return false;
                                });

                           var array_total =  data.lockers_in_single_column_array.length;

                            var locker_height = 1 ;
                            var number_of_lockers = 0 ;
                            for(var i=0 ; i < array_total ; i++) {
                                 if (i == 0) {

                                }else if(data.lockers_in_single_column_array[i-1] == data.lockers_in_single_column_array[i] ){

                                    var next_count = i+1;
                                    locker_height = locker_height + 1;
                                    if(next_count == array_total){
                                        var config_height = Math.floor(locker_height/2);
                                        var config_placement = next_count - config_height;
                                        $('.lockers'+config_placement).text(locker_height + 'x' +  data.lockers_in_single_column_array[i])
                                        var add_lockers_column_event = 'add_lockers_column('+data.last_order_in_single_column_array[i][0]+','+data.last_order_in_single_column_array[i][1]+')';
                                        console.log(add_lockers_column_event,'add_lockers_column_event');
                                        $('.add_lockers_column'+config_placement).attr('onclick', add_lockers_column_event);
                                        $('.add_lockers_column'+config_placement).removeClass('d-none');
                                        $('.line_cut'+next_count).addClass("line-right");
                                        var double_incr = next_count+1;
                                        $('.line_cut'+double_incr).addClass("line-left");
                                    }
                                }else if(data.lockers_in_single_column_array[i-1] != data.lockers_in_single_column_array[i]){

                                      var config_height = Math.floor(locker_height/2);
                                     var config_placement = i - config_height;
                                    $('.lockers'+config_placement).text(locker_height + 'x' +  data.lockers_in_single_column_array[i-1])
                                     var add_lockers_column_event = 'add_lockers_column('+data.last_order_in_single_column_array[i-1][0]+','+data.last_order_in_single_column_array[i-1][1]+')';
                                     console.log(add_lockers_column_event,'add_lockers_column_event');
                                     $('.add_lockers_column'+config_placement).attr('onclick', add_lockers_column_event);
                                     $('.add_lockers_column'+config_placement).removeClass('d-none');
                                    // $('.lockers'+config_placement).css("color", "black");

                                     $('.line_cut'+i).addClass("line-right");
                                     var double_incr = i+1;
                                     $('.line_cut'+double_incr).addClass("line-left");
                                     var next_count = i+1;
                                     locker_height = 1;
                                     if(next_count == array_total){
                                         // console.log(' not equal last printing')
                                         var config_height = Math.floor(locker_height/2);
                                         var config_placement = next_count - config_height;
                                         $('.lockers'+config_placement).text(locker_height + 'x' +  data.lockers_in_single_column_array[i])
                                         var add_lockers_column_event = 'add_lockers_column('+data.last_order_in_single_column_array[i][0]+','+data.last_order_in_single_column_array[i][1]+')';
                                         console.log(add_lockers_column_event,'add_lockers_column_event');
                                         $('.add_lockers_column'+config_placement).attr('onclick', add_lockers_column_event);
                                         $('.add_lockers_column'+config_placement).removeClass('d-none');
                                         // $('.lockers'+config_placement).css("color", "black");
                                         $('.line_cut'+next_count).addClass("line-right");
                                         var double_incr = next_count+1;
                                         $('.line_cut'+double_incr).addClass("line-left");

                                     }

                                }


                            }

                                $(".loader").hide();
                                jQuery('#lockers_div').css('opacity', '1');
                        }
                });
        }

        //add lockers column after
       function add_lockers_column(block_id,last_order) {
           // alert(block_id)
           if (confirm("Are you sure to Add New Column?")) {

               var csrf = $('meta[name=csrf-token]').attr("content");

               var url = '{{ route('addLockerColumn') }}';
               $.ajax({
                   url: url,
                   type: "post",
                   data: {
                       block_id: block_id,
                       last_order: last_order,
                       _token: csrf,
                   },
                   success: function (response) {
                       var checkboxvalue = 0;
                       if($("#deleted_checkbox").prop("checked") == true){
                           checkboxvalue = 1;
                       }
                       else if($("#deleted_checkbox").prop("checked") == false){
                           checkboxvalue = 0;
                       }
                       create_lockers_ajax(block_id, checkboxvalue);

                   },
                   error: function (jqXHR, textStatus, errorThrown) {
                       console.log(textStatus, errorThrown);
                   }
               });
           }
       }

</script>