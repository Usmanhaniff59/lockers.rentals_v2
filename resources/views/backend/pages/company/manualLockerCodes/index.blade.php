@extends('backend.layouts.app')

@section('title', 'Lockers | Companies')
@section('style')
    <!--plugin styles-->
    <link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-switch/css/bootstrap-switch.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/switchery/css/switchery.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/radio_css/css/radiobox.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/checkbox_css/css/checkbox.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/modal/css/component.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-tagsinput/css/bootstrap-tagsinput.css"/>
    <link rel="stylesheet" type="text/css" href="/backend/vendors/animate/css/animate.min.css" />
    <!-- end of plugin styles -->

    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/radio_checkbox.css" />
    

    <link type="text/css" rel="stylesheet" href="/backend/vendors/chosen/css/chosen.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/form_elements.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/datepicker-jquery.css"/>


    <style>

        @media (min-width: 768px) {
            .modal-xl {
                width: 90%;
                max-width:1200px;
            }
           /* .box-resize{
                display: inherit;
            }*/
         /*   .box{
                float: revert !important;
                max-width: inherit;
            }*/

        }
        @media (max-width: 767px) {
             .box-resize{
                width: auto !important;
         
            }
        }

        .flex-container {
            display: flex;
            justify-content: center;
            flex-wrap: wrap;
        }

        .flex-container label {
            margin: 15px;
        }
        .box{
            /*background-color: lightgrey;*/
            width: 233px;
            border: 5px solid red;
            padding: 3px;
            float: right;
            margin-right: 45px;
        }
#search-input{
    display: none;
}

    </style>

@endsection
@section('content')
    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-building circle header_icon_margin"></i>
                            Codes
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        @if(Session::has('flash_message'))
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable float-right" style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-hidden="true" style="margin-top: 4px; margin-left: 15px;"> ×
                    </button>
                    {{ Session::get('flash_message') }}
                </div>
            </div>
        @endif
        @if (session('status'))
            <div class="alert alert-danger">
                {{ session('status') }}
            </div>
        @endif

        @include('errors.success')
        @include('errors.error')
        <div class="outer">
            <div class="inner bg-container">
                <div class="row">
                    @include ('errors.list')
                    <div class="col-12 data_tables">
                        <div class="card m-t-35">
                            <div class="card-header bg-white">
                                <i class="fa fa-building circle header_icon_margin"></i> Manual Locker Codes
                            </div>
                          
                            <form class="form-style" id="export_excel_project" method="POST" action="{{ url('admin/locker/codes/excel/export') }}">
                                <div class="row p-t-15" style="margin-left: 12px">

                                    {!! csrf_field() !!}
                                    <input type="hidden" class="report_qr_export" name="excel_array" value="1">
                                    <input type="hidden" id="search_company_excel" name="search_company" placeholder="Company" class="form-control">
                                    <input type="hidden" id="search_block_excel"name="search_block" placeholder="Block" class="form-control">
                                    <input type="hidden"   id="search_start_date_excel" name="search_start_date" class="form-control" placeholder="Date (defaults to todays Date)" >
                                    <input type="hidden"   id="search_input_excel" name="search_input" class="form-control" placeholder="Date (defaults to todays Date)" >

                                    <button  type="submit" class="form_submit_check btn btn-warning  ml-2">
                                        <span class="assign_class label"  >
                                          EXCEL EXPORT
                                        </span>
                                    </button>
                                </div>

                            </form>
                            <div class="row p-t-15" style="margin-left: 12px">
                                <div class="col-md-2"><input type="text" id="search_company"  placeholder="Company" class="form-control"></div>
                                <div class="col-md-2"><input type="text" id="search_block" placeholder="Block" class="form-control"></div>
                                <div class="col-md-3"><input type="text" onchange="codesTable()" id="search_start_date" class="form-control" placeholder="Date (defaults to todays Date)" ></div>

                            <!--     <div class="col-md-5 box-resize">
                                    <div class="box">
                                        <b>*Note:</b>Red color show, edited rows
                                      </div>
                                </div> -->

                            </div>
                            <div class="row">
        <div class="col-md-12" style="margin: 24px 1px 0 0;">
            <div class="form-group col-md-4" style="float:left;">
                <label><b>Edit/Not Edit</b></label>
                <div class="clearfix"></div>
                <label class="custom-control custom-radio signin_radio1 d-inline-block pr-3" style="margin-left: 10px;">
        <input id="move_radio_edit" name="search_code" value="edit" type="radio" class="custom-control-input search-input radio-search">
        <span class="custom-control-label"></span>
        <span class="custom-control-description">Edited</span>
    </label>
    <label class="custom-control custom-radio signin_radio1 d-inline-block pr-3" style="margin-left: 10px;">
        <input id="move_radio_not_edit" name="search_code" value="not_edit" type="radio" class="custom-control-input search-input radio-search">
        <span class="custom-control-label"></span>
        <span class="custom-control-description">Not Edited</span>
    </label>
      <label class="custom-control custom-radio signin_radio1 d-inline-block pr-3" style="margin-left: 10px;">
        <input id="move_radio_all" checked name="search_code" value="All" type="radio" class="custom-control-input search-input radio-search">
        <span class="custom-control-label"></span>
        <span class="custom-control-description">All</span>
    </label>

            </div>

        </div>
      
        </div>
      
              <div class="col-md-12">
                  <div class="row">
            <div class="col-2 pr-2">
                    <div class="padding10" style="background: #ff8086; text-align: center;"><span style="color: white">Edited</span></div>
                </div>
                  <div class="col-2 pr-2">
                    <div class="padding10" style="background: orange; text-align: center;"><span style="color: white">Not Edited</span></div>
                </div>
                 <div class="col-2 pr-2">
                    <div class="padding10" style="background: #eaee09; text-align: center;"><span style="color: white">Old Code</span></div>
                </div>
             </div>
            
        </div>
                           

                            <div class="row p-t-15" style="margin-left: 12px">

                            </div>

                            <div class="card-body p-t-10">
                                <div class="row" style="width: 100% !important;">
                                    <div class="col-md-12 m-t-25">
                                        <div class="table-responsive">
                                            <table class="table  table-bordered table-hover  " style="width:100%"  id="sample_6">
                                                <thead>
                                                <tr>
                                                    {{--<th>#</th>--}}
                                                    <th>Company Name </th>
                                                    <th>Block</th>
                                                    <th>Block Group</th>

                                                    <th>Locker Number</th>
                                                    <th>Manual Code</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--- Edit model -->
    <div class="modal fade in display_none" id="edit" style="overflow-y: scroll;" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">

                <form  action="javascript:void(0)" id="editCodeMannuallocker" method="POST">
                    @csrf
                    <div class="modal-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">

                        <div class="card m-t-35">
                            <div class="card-header bg-white edit-card-header">

                            </div>
                            <div class="card-body">
                                <br>
                                        <h3><i class="fa fa-pencil circle"></i> Edit Code </h3>

                                        <div class="row">
                                            <div class="col-12">
                                                <input type="hidden" name="id" id="manual_code_id" class="form-control">
                                                <input type="hidden" name="locker_id" id="manual_locker_id" class="form-control">
                                                <input type="hidden" name="search_date" id="search_date" class="form-control">

                                                <div class="form-group">
                                                    <label name = "name" >Code</label>
                                                    <input type="text" name="code" id="code" class="form-control">
                                                </div>

                                            </div>
                                        </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer " style="display: flex;">
                        <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                        <button type="submit" class="btn btn-primary "  id="save-edit"> Save Changes</button>
                    </div>

                </form>
                <br>
            </div>
        </div>
    </div>
    <!-- END Edit modal-->



@endsection
@section('script')
    <!--plugin scripts-->
    <script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>
    <!-- end of plugin scripts -->

    <!--Page level scripts-->
    <script type="text/javascript" src="/backend/js/pages/datatable_server_side.js"></script>
    <script type="text/javascript" src="/backend/js/pages/radio_checkbox.js"></script>
    <script type="text/javascript" src="{{asset('/backend/js/common.js')}}"></script>
    <script src="{{ asset('custom/general.js') }}"></script>


    <script type="text/javascript" src="/backend/vendors/chosen/js/chosen.jquery.js"></script>
    <script type="text/javascript" src="/backend/js/pages/form_elements.js"></script>

    <!-- end of plugin scripts -->
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk="   crossorigin="anonymous"></script>

    <!--Page level scripts-->
    <script type="text/javascript" src="/backend/js/pages/datatable_server_side.js"></script>
    <script type="text/javascript" src="{{asset('/backend/js/common.js')}}"></script>

    <script type="text/javascript">
         $('.radio-search').click(function(){
       codesTable();
    });
        function excel_filters() {
            var company = $('#search_company_excel').val($('#search_company').val());
            // alert($('#search_company_excel').val())
            $('#search_block_excel').val($('#search_block').val());
            $('#search_start_date_excel').val($('#search_start_date').val());
            $('#search-input_excel').val($('#search-input').val());

        }
        $(document).ready(function() {
            $("#search_start_date").datepicker();
            $("#search_end_date").datepicker();


            var searchFunction = 'searchByThis(availability_reports_table)';
            var payments_search = "{{\Illuminate\Support\Facades\Session::get('payments-search')}}";
            $('#sample_6_wrapper>.row:nth-child(1)>div:nth-child(2)').append('<label style="float:right;margin-top:30px;"><input class="form-control input-sm" id="search-input" value="' + payments_search + '" onblur="' + searchFunction + '"></label>');
            var oTable = $('#sample_6').dataTable();
            // Sometime later - filter...
            oTable.fnFilter(payments_search);


            $("#search_company").keypress(function(event) {
                codesTable();
                if (event.keyCode === 13) {
                    searchByThis(availability_reports_table);
                    excel_filters();
                }
                sessionStorage.setItem('search_company_available', $("#search_company").val())
            });

            $("#search_block").keypress(function(event) {
                codesTable();
                if (event.keyCode === 13) {
                    searchByThis(availability_reports_table);
                    excel_filters();
                }
                sessionStorage.setItem('search_block_available', $("#search_block").val())
            });
            $("#search-input").keypress(function(event) {
                codesTable();
                if (event.keyCode === 13) {
                    searchByThis(availability_reports_table);
                    excel_filters();
                }
                sessionStorage.setItem('search-input_available', $("#search-input").val())
            });
        });

        var availability_reports_table = codesTable();

        function codesTable() {
            excel_filters();
            // alert('dd');
            $('#sample_6').dataTable().fnClearTable();
            $('#sample_6').dataTable().fnDestroy();

            return $('#sample_6').DataTable({
                responsive: true,
                serverSide: true,
                stateSave: true,
                processing: true,
                searching: false,
                "pageLength": 10,
                "order": [[ 0, "asc" ], [ 1, "asc" ],[ 2, "asc" ]],
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                "oLanguage": {
                    "sLengthMenu": "_MENU_",
                    "sSearch": "_INPUT_"
                },
                "aTargets": ["_all"],
                initComplete: function () {
                    $("#sample_6_wrapper>.row").attr('style', 'width:100%;');
                    $("#sample_6_filter>label").css('float', 'right');
                    $("#sample_6_length>label").css('float', 'left');
                },

                ajax: {
                    url: '<?php echo e(route('locker.codes.show.all')); ?>',
                    data: function (d) {
                        d.search_company = $('#search_company').val();
                        d.search_block = $('#search_block').val();
                        d.search_start_date = $('#search_start_date').val();

                        d.search_input = $('#search-input').val();
                        d.radio = $('input[name="search_code"]:checked').val();
                    }
                },
                columns: [
                    // {data: 'id', name: 'id'},
                    {data: 'company_name', name: 'company_name', orderable: false},
                    {data: 'block', name: 'block', orderable: false},
                    {data: 'block_group', name: 'block_group', orderable: false},

                    {data: 'locker_number', name: 'locker_number', orderable: false},
                    {data: 'manual_code', name: 'manual_code' , orderable: false},

                    {data: 'action', name: 'action', orderable: false, searchable: true}

                ],
            });
        }

        function getCode(locker_id , date_passed) {


            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
        url: '{{ route('locker.codes.get.locker.code') }}',
        type: 'POST',
        dataType: 'JSON',
         data: {locker_id: locker_id,date_passed: date_passed, _token: CSRF_TOKEN},
        cache: false,

        success: function (data) {
            console.log(data);
            $('#manual_locker_id').val(data.locker_id);
            var search_start_date = $('#search_date').val($('#search_start_date').val());
            console.log(data.manual_code.code);
            $('#code').val(data.manual_code.code);
            $('#manual_code_id').val(data.manual_code.id);



            },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    });

        }


        // function changeStartDate(){
        //     var start_date = $("#search_start_date").val();
        //     $.ajax({
        //         url: '/admin/reports/set/start/availability/'+ start_date,
        //         type: 'GET',
        //         dataType: 'JSON',
        //         cache: false,

        //         success: function (data) {
        //             console.log(data);
        //             searchByThis(availability_reports_table);
        //             },
        //         error: function (jqXHR, textStatus, errorThrown) {
        //             console.log(jqXHR);
        //         }
        //     });
        // }
        // function changeEndDate(){
        //     var end_date = $("#search_end_date").val();
        //     $.ajax({
        //         url: '/admin/reports/set/start/availability/'+ end_date,
        //         type: 'GET',
        //         dataType: 'JSON',
        //         cache: false,

        //         success: function (data) {
        //             console.log(data);
        //             searchByThis(availability_reports_table);
        //             },
        //         error: function (jqXHR, textStatus, errorThrown) {
        //             console.log(jqXHR);
        //         }
        //     });
        // }
        $('#editCodeMannuallocker').submit(function(){

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
        url: '{{ route("locker.codes.update.code") }}',
        type: 'POST',
        dataType: 'JSON',
         data: {
            id:$("#manual_code_id").val(),
            locker_id:$("#manual_locker_id").val(),
            code:$("#code").val(),
            search_date:$("#search_date").val(),



          _token: CSRF_TOKEN
      },
        cache: false,

        success: function (data) {
            var id=$("#manual_locker_id").val();
            var co=$("#code").val();
            $('.code-'+id).css('background-color','#ff8086');
            $('#c-'+id).html(co);
            $('#edit').modal('toggle');
            console.log(data);
            // $('#manual_locker_id').val(data.locker_id);
            // var search_start_date = $('#search_date').val($('#search_start_date').val());
            // console.log(data.manual_code.code);
            // $('#code').val(data.manual_code.code);
            // $('#manual_code_id').val(data.manual_code.id);



            },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    });
        });

    </script>



@endsection
