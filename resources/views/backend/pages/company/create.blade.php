<div class="modal fade in display_none" id="create" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            {!! Form::open(['route' => ['company.store'], 'id' => 'save_company_data']) !!}
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-md-12">
                                <h3><i class="fa fa-plus-circle circle"></i> Add New Company</h3>
                            </div>
                        </div>
                        <hr>
                        <br>
                        <div class="row">
                            <div class='form-group col-md-6'>
                                <label for="status">Location Group</label>
                                <select class="form-control chzn-select" name="location_group_id"
                                    id="location_group_id">
                                    <option value="" selected>Choose Location Group</option>
                                    @foreach ($locationGroups as $locationGroup)
                                    <option value="{{$locationGroup->id}}"
                                        {{ old('location_group_id') == $locationGroup->id ? 'selected' : '' }}>
                                        {{$locationGroup->name}}</option>
                                    @endforeach
                                </select>
                                <span class="location_group_id general_error" style=""></span>
                            </div>

                            <div class="form-group col-md-6">
                                {{ Form::label('name', 'Name') }}
                                {{ Form::text('name', '', array('class' => 'form-control','id'=>'company_name')) }}
                                <span class="company_name general_error" style=""></span>
                            </div>
                        </div>
                        <div class="row">

                            <div class="form-group col-md-6">
                                {{ Form::label('name_number', 'Name Number') }}
                                {{ Form::text('name_number', null, array('class' => 'form-control')) }}
                            </div>

                            <div class="form-group col-md-6">
                                {{ Form::label('street', 'Street') }}
                                {{ Form::text('street', null, array('class' => 'form-control')) }}
                            </div>


                        </div>
                        <div class="row">


                            <div class="form-group col-md-6">
                                {{ Form::label('city', 'City') }}
                                {{ Form::text('city', null, array('class' => 'form-control')) }}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('country', 'Country') }}
                                {{ Form::text('country', null, array('class' => 'form-control')) }}
                            </div>
                        </div>
                        <div class="row">

                            <div class="form-group col-md-6">
                                {{ Form::label('post_code', 'Postal Code') }}
                                {{ Form::text('post_code', null, array('class' => 'form-control')) }}

                                <div class="form-group">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="currency" value="GBP"
                                                {{ (old('currency') == 'GBP') ? 'checked' : '' }} checked>
                                            <span class="cr"><i class="cr-icon fa fa-circle"></i></span>
                                            GBP
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="currency" value="EUR"
                                                {{ (old('currency') == 'EUR') ? 'checked' : '' }}>
                                            <span class="cr"><i class="cr-icon fa fa-circle"></i></span>
                                            EUR
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="radio_basic_swithes_padbott">
                                        <input type="checkbox" class="js-switch sm_toggle" name="active"
                                            {{ old('active') == 'on' ? 'checked' : '' }} checked />
                                        <span class="radio_switchery_padding">Active</span>
                                        <br />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="radio_basic_swithes_padbott">
                                        <input type="checkbox" class="js-switch sm_toggle_checked " name="display"
                                            {{ old('display') == 'on' ? 'checked' : '' }} />
                                        <span class="radio_switchery_padding">Display</span>
                                        <br />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="radio_basic_swithes_padbott">
                                        <input type="checkbox" class="js-switch sm_toggle_keep_block " name="keep_block"
                                            {{ old('keep_block') == 'on' ? 'checked' : '' }} />
                                        <span class="radio_switchery_padding">Keep Block (Allow students to reserve this
                                            locker next year)</span>
                                        <br />
                                    </div>
                                </div>

                                @can('Assign Company To Franchisee')
                                <div class='form-group existed-franchisee-group'>
                                    <label for="franchisee_id">Franchisee</label>
                                    <select class="form-control chzn-select" name="franchisee_id" tabindex="2">
                                        <option value="" selected>Choose Franchisee</option>
                                        @foreach ($franchises as $franchisee)
                                        <option value="{{$franchisee->id}}">{{$franchisee->name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                                @endcan
                                @can('Assign Status Type To Company')
                                <div class='form-group existed-franchisee-group'>
                                    <label for="franchisee_id">Status Type</label>
                                    <select class="form-control chzn-select" id="search_status_type"
                                        placeholder="Status Type" name="status_type" tabindex="2">
                                        <option value="">Select Status Type</option>
                                        <option value="all">All </option>
                                        <option value="company">Customer</option>
                                        <option value="leads">LEADS</option>
                                        <option value="prospective">Prospective</option>
                                        <option value="unmatched">NOT ASSIGNED</option>
                                    </select>

                                </div>
                                @endcan
                            </div>
                            <div class="form-group col-md-6">
                                <br>
                                <div>
                                    <h3>School Academic Year </h3>
                                </div>
                                <div class="pl-3">

                                    @foreach($school_academic_year as $value)
                                    <input type="checkbox" name="roles[]" value="{{$value->id}}" />
                                    <span class="radio_switchery_padding">{{$value->school_year}}</span>
                                    <br />
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="display: flex;">
                <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                {{ Form::submit('Save changes', array('class' => 'btn btn-primary .save_company')) }}
            </div>
            {{ Form::close() }}
            <br>
        </div>
    </div>
</div>