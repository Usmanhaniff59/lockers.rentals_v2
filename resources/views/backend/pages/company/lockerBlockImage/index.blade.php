@extends('backend.layouts.app')
@section('title', 'Locker Blocks  | Images')
@section('style')
<!--plugin styles-->
<link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-switch/css/bootstrap-switch.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/switchery/css/switchery.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/radio_css/css/radiobox.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/checkbox_css/css/checkbox.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/modal/css/component.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-tagsinput/css/bootstrap-tagsinput.css" />
<link rel="stylesheet" type="text/css" href="/backend/vendors/animate/css/animate.min.css" />
<!-- end of plugin styles -->
<!--Page level styles-->
<link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/pages/radio_checkbox.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/chosen/css/chosen.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/pages/form_elements.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/pages/datepicker-jquery.css" />


<style>
    @media (min-width: 768px) {
        .modal-xl {
            width: 90%;
            max-width: 1200px;
        }
    }
    .block-images{
        width: 25px;
    }
    .flex-container {
        display: flex;
        justify-content: center;
        flex-wrap: wrap;
    }

    .flex-container label {
        margin: 15px;
    }

    .loader {
        position: absolute;
        top: 15%;
        left: 45%;
        margin: auto;
        display: none;
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #00c0ef;
        width: 150px;
        height: 150px;
        -webkit-animation: spin 2s linear infinite;
        /* Safari */
        animation: spin 2s linear infinite;
    }

    .loader-percentage {
        position: absolute;
        top: 21%;
        left: 50%;
        margin: auto;
        display: none;
    }

    /* Safari */
    @-webkit-keyframes spin {
        0% {
            -webkit-transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
        }
    }
    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }

        100% {
            transform: rotate(360deg);
        }
    }
    .excel-loader {
        display: none;
    }

.zoom {
  /*padding: 50px;*/
  /*background-color: green;*/
  /*transition: transform .2s;*/
  width: 200px;
  height: 200px;
  /*margin: 0 auto;*/
}



</style>
@endsection
@section('content')
@include('backend.pages.company.lockerBlockImage.create')
@include('backend.pages.company.lockerBlockImage.edit')


<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
            <div class="row no-gutters">
                <div class="col-6">
                    <h4 class="m-t-5">
                        <i class="fa fa-building circle header_icon_margin"></i>
                        Company Management
                    </h4>
                </div>
            </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="loader" id="block_loader">
            </div>
            <h2 class="loader-percentage"><b id="percentage">0%</b></h2>
            <div id="lockers_div">
                <div class="row">
                    @include ('errors.list')
                    <div class="col-12 data_tables">
                        <div class="card m-t-35">
                            <div class="card-header bg-white">
                                <i class="fa fa-building circle header_icon_margin"></i> Locker Blocks Images 
                            </div>
                            <div class="card-body p-t-10">
                                <div class="row" style="width: 100% !important;">
                                    <div class="col-md-12 m-t-25">
                                        <a href="#create_document" class="input_create_document_empty btn btn-primary" data-toggle="modal"
                                        style="float: right;">Add</a>
                                        <div class="table-responsive">
                                            <table class="table  table-bordered table-hover  " style="width:100%"

                                                id="locker_blockImageTable">
                                                <thead>
                                                    <tr>
                                                        <!-- <th>File</th> -->
                                                        <th>Image Type</th>
                                                        <th>Tier</th>
                                                        <th>Action </th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<!--plugin scripts-->
<script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="/backend/vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>
<!-- end of plugin scripts -->

<!--Page level scripts-->
<script type="text/javascript" src="/backend/js/pages/datatable_server_side.js"></script>
<script type="text/javascript" src="/backend/js/pages/radio_checkbox.js"></script>
<script src="{{ asset('custom/general.js') }}"></script>

<script type="text/javascript" src="/backend/vendors/chosen/js/chosen.jquery.js"></script>
<script type="text/javascript" src="/backend/js/pages/form_elements.js"></script>

<!-- end of plugin scripts -->
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"
    integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk=" crossorigin="anonymous"></script>

<!--Page level scripts-->
<script type="text/javascript" src="/backend/js/pages/datatable_server_side.js"></script>
<script type="text/javascript" src="{{asset('/backend/js/common.js')}}"></script>
<script type="text/javascript" src="/backend/sheetjs/dist/xlsx.full.min.js"></script>
<script type="text/javascript" src="/backend/filesaver/dist/FileSaver.js"></script>
<script type="text/javascript">

    sessionStorage.setItem("booked_data", JSON.stringify([]));

        function spinner_start() {
            $(".loader").show();
            $(".loader-percentage").show();
            jQuery('#lockers_div').css('opacity', '0.05');
        }
        function spinner_close() {
            $(".loader").hide();
            $(".loader-percentage").hide();
            jQuery('#lockers_div').css('opacity', '1');
        }
        

        var charity_reports_table = lockerBlockImage();

        function lockerBlockImage() {
            $('#locker_blockImageTable').dataTable().fnClearTable();
            $('#locker_blockImageTable').dataTable().fnDestroy();

            return $('#locker_blockImageTable').DataTable({
                responsive:true,
                serverSide: true,
                stateSave: true,
                processing: true,
                searching: false,
                "pageLength" : 10,
                "ordering": false,
               "order": [[ 0, "asc" ], [ 1, "asc" ],[ 2, "desc" ]],
                "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
                 "oLanguage": {
                      "sLengthMenu": "_MENU_",
                            "sSearch": "_INPUT_"
                    },
                    "aTargets": [ "_all" ],
                    initComplete : function() {
                        $("#charity_report_filter>label").css('float', 'right');
                        $("#charity_report_length>label").css('float', 'left');
                    },
                ajax: {
                    url: '<?php echo e(route('company.locker.blocks.images.show.all')); ?>',
                },
                columns: [
                    // {data: 'file_name', name: 'file_name'},
                    {data: 'image_type', name: 'image_type'},
                    {data: 'tier', name: 'tier'},
                    {data: 'action', name: 'action', orderable: false, searchable: true}
                ],
            });
        }
</script>
@endsection