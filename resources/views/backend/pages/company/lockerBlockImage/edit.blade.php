<div class="modal fade in display_none model-document-id" style="overflow-y: scroll;" id="edit_document" tabindex="-1"
    role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-md-12">
                                <h3><i class="fa fa-pencil circle"></i> Edit Document</h3>
                            </div>
                        </div>
                        <hr>
                        <div class="loader" id="block_loader">
                        </div>
                        <form id="documentEditForm" enctype="multipart/form-data">
                            <input type="hidden" id="id">
                            <div class="row">
                                <div class='form-group col-md-12'>
                                    <label for="document_type">Image Type</label>
                                    <select class="form-control" name="image_type" id="image_type">
                                        <option value="I" >Internal</option>
                                        <option value="O">External</option>
                                    </select>
                                </div>
                                <div class='form-group col-md-12'>
                                    <label for="tier">Block Tier</label>
                                    <input type="text" name="tier" id="tier_update" value="" placeholder="Block Tier" class="form-control">
                                    <div id="tier_update_error" class="validation-error"></div>

                                </div>
                                <input type="hidden" name="id" id="edit_form_id">
                                <div class="form-group col-md-12">
                                    <label for="editfile_name" class="upload-area" id="editfile">
                                        <i class="fas fa-cloud-upload-alt"></i>
                                        <h1 id="placeholder" class="placeholder">Click or drop file here</h1>
                                    </label>
                                    <br>
                                    <input id="editfile_name" name="file_name" type="file" style="display:none"  />
                                </div>
                               <div class='form-group col-md-12'>
                                 <label for="tier">Current Image</label>
                                 <div class="clearfix"></div>
                                <img class="block-images zoom center" style="width: 200px;" id="image-fupload" src="">
                                </div>
                            </div>
                            <div class="modal-footer" style="display: flex;">
                                <button type="button" id="edit_cancel" data-dismiss="modal"
                                    class="btn btn-light">Cancel</button>
                                <button type="submit" class="btn btn-primary update"
                                    id="edit_document_btn">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>
<script type="text/javascript">
    $("html").on("drop", function (e) {
            e.preventDefault();
            e.stopPropagation();
            file = e.originalEvent.dataTransfer.files;
            $("#placeholder").text(file[0].name);
            dragcheck = 2;
        });

    $("#editfile_name").change(function () {
            var filees = $("#editfile_name").val();
            var filename = filees.split('\\').pop();
            $("#placeholder").text(filename);
            dragcheck=0;
        });

    $("#documentEditForm").submit(function(event){
            // spinner_start();
            event.preventDefault();
            $('button#edit_document_btn').prop('disabled', true);
            formdata =  new FormData(this);
            var file_name=$("#editfile_name").val();
            console.log(file_name);
            console.log('submit');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: '{{route('company.locker.blocks.images.update')}}',
                data: formdata,
                // dataType:'JSON',
                contentType:false,
                cache:false,
                processData:false,
                
                success: function(){
                    // spinner_close();
                    $("#documentEditForm")[0].reset();
                    $("h1").text("Click or drop file here");
                    $("#edit_document").modal('hide');
                    $('#edit').modal('show');
                    $('button#edit_document_btn').prop('disabled', false);
                    lockerBlockImage();
                },
                error: function (xhr) {
                    // spinner_close();
                    $('.document_error').show();
                    $('button#edit_document_btn').prop('disabled', false);

                    if(xhr.responseJSON.errors.document_type){
                        $('#image_type_error').show();  
                        $('#image_type_error').html('<span>'+ xhr.responseJSON.errors.document_type[0] + '</span>');
                    }
                    else{
                        $('#image_type_error').hide();
                    }

                    if(xhr.responseJSON.errors.tier){
                        $('#tier_update_error').show();
                        $('#tier_update_error').html('<span>'+ xhr.responseJSON.errors.tier[0] + '</span>');
                    }
                    else{
                        $('#tier_update_error').hide();
                    }
                  
                    if(xhr.responseJSON.errors.file){
                        $('#document_file_error').show();
                        $('#document_file_error').html('<span>'+ xhr.responseJSON.errors.file[0] + '</span>');
                    }
                    else{
                        $('#document_file_error').hide();
                    }
                }
            });
        });
    $(document).on('click','#open_block_modal',function(){
        // alert('123');
        $('#edit_document').modal('toggle');
        var tier=$(this).attr('data-tier');
         if(tier!=undefined){
                $('#tier_update').val(tier);
            }
            var imageType=$(this).attr('data-imageType');
            // alert(imageType);
                    if(imageType!=undefined && imageType!=''){
             $('#image_type').val(imageType);
            }else{
                 $('#image_type').val('O');
            }
            var image=$(this).attr('data-image');
            $('#image-fupload').attr('src', '/uploads/'+image);

            var id=$(this).attr('data-id');
            $('#edit_form_id').val(id);
            
    });
    // function saveDocumentId(id,tier,imageType,image){

    //           if(tier!=undefined){
    //             $('#tier_update').val(tier);
    //         }
            
    //         if(imageType!=undefined){
    //          $('#image_type').val(imageType);
    //         }else{
    //              $('#image_type').val('O');
    //         }

    //          $('#image-fupload').attr('src', '/uploads/'+image);
    //         // var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    //         // $.ajax({
    //         //     url: '/admin/company/locker/blocks/images/editabledata/',
    //         //     type: 'POST',
    //         //     dataType: 'JSON',
    //         //     cache: false,
    //         //     data: {id: id, _token: CSRF_TOKEN},
    //         //     success: function (LockerBlockImage) {
    //         //         console.log(LockerBlockImage);
    //         //         $('#image_type').val(LockerBlockImage.image_type);
    //         //         $('#tier_update').val(LockerBlockImage.tier);
    //         //         $('#edit_form_id').val(LockerBlockImage.id);

    //         //         // $('#file_name').val(file_name);
    //         //         $("#placeholder").text(LockerBlockImage.file_name);
    //         //         console.log(LockerBlockImage);
    //         //     },
    //         //     error: function (jqXHR, textStatus, errorThrown) {
    //         //         console.log(jqXHR);
    //         //     }
    //         // });
    // }
</script>