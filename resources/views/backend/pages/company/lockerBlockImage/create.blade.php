<div class="modal fade in display_none" id="create_document" style="overflow-y: scroll;" tabindex="-1" role="dialog"
    aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-md-12">
                                <h3><i class="fa fa-pencil circle"></i> Create Document</h3>
                            </div>
                        </div>
                        <hr>
                        <div class="loader" id="block_loader">
                        </div>
                        <form id="documentsForm" enctype="multipart/form-data">
                            <div class="row">
                                <div class='form-group col-md-12'>
                                    <label for="document_type">Image Type</label>
                                    <select class="form-control input_empty" name="image_type" id="choose_imagetype">
                                        <option value="I" selected>Inside</option>
                                        <option value="O">Outside</option>    
                                    </select>
                                    <div id="image_type_error" class="validation-error"></div>

                                </div>
                                <div class='form-group col-md-12'>
                                    <label for="tier">Block Tier</label>
                                   <input type="text" name="tier"  placeholder="Block Tier" class="input_empty form-control">
                                    <div id="tier_error" class="validation-error"></div>

                                </div>
                                <div class="form-group col-md-12">
                                    <br>
                                    <label for="file" class="upload-area" id="uploadfile">
                                        <i class="fas fa-cloud-upload-alt"></i>
                                        <h1 id="drag_text">Click or drop file here</h1>
                                    </label>
                                    <br>
                                    <input id="file" name="file_name" type="file" style="display:none" />
                                </div>
                                <div id="file_error" class="validation-error"></div>
                            </div>
                            <div class="modal-footer" style="display: flex;">
                                <button type="button" data-dismiss="modal" class="btn cancel btn-light">Cancel</button>
                                <button type="submit" class="btn btn-primary" id="create_document_btn">Upload</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>
<script type="text/javascript" src="/backend/js/jquery.min.js"></script>

<script type="text/javascript">
    $( document ).ready(function() {
        $(function () {
        // preventing page from redirecting
        $("html").on("dragover", function (e) {
            e.preventDefault();
            e.stopPropagation();
            $("h1").text("Drag Your File Here!");
        });

        $("html").on("drop", function (e) {
            e.preventDefault();
            e.stopPropagation();
            file = e.originalEvent.dataTransfer.files;
            console.log()
            $("#drag_text").text(file[0].name);
            dragcheck = 2;
        });

        // Drag enter
        $('.upload-area').on('dragenter', function (e) {
            e.stopPropagation();
            e.preventDefault();
            $("h1").text("Drop Here!");
        });

        // Drag over
        $('.upload-area').on('dragover', function (e) {
            e.stopPropagation();
            e.preventDefault();
            $("h1").text("Drop Here!");
        });
        // file selected
        $("#file").change(function () {
            var filees = $("#file").val();
            var filename = filees.split('\\').pop();
            $("#drag_text").text(filename);
            dragcheck=0;
        });
        });
        $("#documentsForm").submit(function(event){
            // spinner_start();
            $('.validation-error').hide();
            event.preventDefault();
            $('button#create_document_btn').prop('disabled', true);
            formdata =  new FormData(this);

           console.log('submit');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: '{{route('company.locker.blocks.images.upload')}}',
                data: formdata,
                // dataType:'JSON',
                contentType:false,
                cache:false,
                processData:false,
                
                success: function(){
                    // spinner_close();
                    $("#documentsForm")[0].reset();
                    $("h1").text("Click or drop file here");
                    $('#create_document').modal('hide');
                    $('#edit').modal('show');
                    $('button#create_document_btn').prop('disabled', false);
                    lockerBlockImage();
                },
                error: function (xhr) {
                    // spinner_close();
                    $('.validation-error').show();
                    $('button#create_document_btn').prop('disabled', false);

                    if(xhr.responseJSON.errors.image_type){
                        $('#image_type_error').show();  
                        $('#image_type_error').html('<span>'+ xhr.responseJSON.errors.image_type[0] + '</span>');
                    }
                    else{
                        $('#image_type_error').hide();
                    }
                    if(xhr.responseJSON.errors.tier){
                        $('#tier_error').show();
                        $('#tier_error').html('<span>'+ xhr.responseJSON.errors.tier[0] + '</span>');
                    }
                    else{
                        $('#tier_error').hide();
                    }
                  
                    if(xhr.responseJSON.errors.file){
                        $('#document_file_error').show();
                        $('#document_file_error').html('<span>'+ xhr.responseJSON.errors.file[0] + '</span>');
                    }
                    else{
                        $('#document_file_error').hide();
                    }
                }
            });
        });

    });

</script>