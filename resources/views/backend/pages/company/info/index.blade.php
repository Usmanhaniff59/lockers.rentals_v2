<div class="card-body">

    <br>
    <div class="row" style="width: 100% !important;">
        <div class="col-md-12">

            <div class="row">
                <div class="form-group col-md-6">

                    <label>Territory Code</label>
                    <select class="form-control" name="territory_code" id="territory_code_info">
                        <option value="">Choose Franchisee</option>
                        @foreach ($franchises as $franchisee)
                        <option value="{{$franchisee->id}}">{{$franchisee->name}}</option>
                        @endforeach
                    </select>

                </div>
                <div class="form-group col-md-6">
                    <label>Short PostCode</label>
                    <input type="text" class="form-control edit_company_info" name="short_post_code"
                        id="short_post_code_info">

                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Buzz Code</label>
                    <input type="text" class="form-control edit_company_info" name="buzz_code" id="buzz_code_info">

                </div>
                <div class="form-group col-md-6">
                    <label>School Lea Code</label>
                    <input type="text" class="form-control edit_company_info" name="school_lea_code"
                        id="school_lea_code_info">

                </div>

            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Town</label>
                    <input type="text" class="form-control edit_company_info" name="town" id="town_info">

                </div>
                <div class="form-group col-md-6">
                    <label>Telephone</label>
                    <input type="text" class="form-control edit_company_info" name="telephone" id="telephone_info">

                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Gender</label>
                    <input type="text" class="form-control edit_company_info" name="gender" id="gender_info">

                </div>
                <div class="form-group col-md-6">
                    <label>La</label>
                    <input type="text" class="form-control edit_company_info" name="la" id="la_info">

                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Low Age</label>
                    <input type="text" class="form-control edit_company_info" name="low_age" id="low_age_info">

                </div>
                <div class="form-group col-md-6">
                    <label>High Age</label>
                    <input type="text" class="form-control edit_company_info" name="high_age" id="high_age_info">

                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Open Date</label>
                    <input type='date' style="pointer:cursor" class="form-control" id='open_date' name="open_date" />
                </div>
                <div class="form-group col-md-6">
                    <label>Sixth Form</label>
                    <input type="text" class="form-control edit_company_info" name="sixth_form" id="sixth_form_info">

                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Type Of Establishment</label>
                    <input type="text" class="form-control edit_company_info" name="type_of_establishment"
                        id="type_of_establishment_info">

                </div>

                <div class="form-group col-md-6">
                    <label>Phase Of Education</label>
                    <input type="text" class="form-control edit_company_info" name="phase_of_education"
                        id="phase_of_education_info">

                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Boarding School</label>
                    <input type="text" class="form-control edit_company_info" name="boarding_school"
                        id="boarding_school_info">

                </div>
                <div class="form-group col-md-6">
                    <label>Academy Trust</label>
                    <input type="text" class="form-control edit_company_info" name="academy_trust"
                        id="academy_trust_info">

                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Religious Affiliation</label>
                    <input type="text" class="form-control edit_company_info" name="religious_affiliation"
                        id="religious_affiliation_info">

                </div>
                <div class="form-group col-md-6">
                    <label>Free School Meals Percentage</label>
                    <input type="text" class="form-control edit_company_info" name="free_school_meals_percentage"
                        id="free_school_meals_percentage_info">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label>Number Of Pupils</label>
                    <input type="text" class="form-control edit_company_info" name="number_of_pupils"
                        id="number_of_pupils_info">
                </div>
                <div class="form-group col-md-6">
                    <label>Area</label>
                    <input type="text" class="form-control edit_company_info" name="area" id="area_info">

                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>General Email</label>
                    <input type="text" class="form-control edit_company_info" name="general_email"
                        id="general_email_info">

                </div>
                <div class="form-group col-md-6">
                    <label>Website Address</label>
                    <input type="text" class="form-control edit_company_info" name="website_address"
                        id="website_address_info">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Head Title</label>
                    <input type="text" class="form-control edit_company_info" name="head_title" id="head_title_info">
                </div>
                <div class="form-group col-md-6">
                    <label>Head First</label>
                    <input type="text" class="form-control edit_company_info" name="head_first" id="head_first_info">

                </div>

            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Head Surname</label>
                    <input type="text" class="form-control edit_company_info" name="head_surname"
                        id="head_surname_info">
                </div>
            </div>




        </div>

    </div>
</div>
<script>

</script>