<div class="modal fade in display_none" id="create_vouchers" style="overflow-y: scroll;" tabindex="-1" role="dialog"
    aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-md-12">
                                <h3><i class="fa fa-pencil circle"></i> Create Vouchers</h3>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Valid From</label>
                                {{--<input type='date' style="pointer:cursor" class="form-control" id='voucher_start_date' name="start" />--}}
                                <input type='text'
                                       class="form-control assign_class datetimepicker" id="voucher_start_date"
                                       name="start"
                                       />

                                <div id="start_error" class=" voucher_error validation-error"></div>
                            </div>

                            <div class="form-group col-md-6">
                                <label>Valid Too</label>
                                 <input type='text'
                                       class="form-control assign_class datetimepicker" id="voucher_end_date"
                                       name="end"
                                />
                                <div id="end_error" class=" voucher_error validation-error "></div>
                            </div>


                            <div class='form-group col-md-6'>
                                <label for="voucher_id">Dscount (%)</label>
                                <input type='text' style="pointer:cursor" class="form-control" id='voucher_discount_percent' name="discount_percent" />
                                <div id="discount_percent_error" class="  voucher_error validation-error"></div>
                            </div>


                            <div class="form-group col-md-6">
                                <label for="voucher_id">Number of Vouchers</label>
                                <input type='text'  class="form-control" id='number_of_vouchers' name="number_of_vouchers" />
                                <div id="number_of_vouchers_error" class="voucher_error validation-error"></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="display: flex;">
                <button type="button" data-dismiss="modal" class="btn btn-light">Cancel</button>
                <button type="button" class="btn btn-primary" id="create_vouchers_btn">Save</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>
<script type="text/javascript" src="/backend/js/jquery.min.js"></script>

<script type="text/javascript">
    $( document ).ready(function() {

     $(".input_create_vouchers_empty").click(function () {
         $(".input_empty").val('');
    });

        var optSimple = {
            sideBySide:true,
            format: "DD MMM, YYYY",
        };
        $('.datetimepicker').datetimepicker(
            optSimple
        );


    $('button#create_vouchers_btn').click(function(){

        $('button#create_vouchers_btn').prop('disabled', true);
        var start  = $('#voucher_start_date').val();
        // var start = new Date(start);
        console.log(start);
        var end  = $('#voucher_end_date').val();
        console.log(end);

        var discount_percent = $('#voucher_discount_percent').val();
        var number_of_vouchers = $('#number_of_vouchers').val();
        var company_id = $('#company_id').val();

        $("#chatsTable_filter>label").css('float', 'right');

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({

		    url: '/admin/company/vouchers/store',
		    type: 'POST',
		    data: {
                start: start,
                end: end,
                discount_percent: discount_percent,
                number_of_vouchers: number_of_vouchers,
                company_id: company_id,
                _token: CSRF_TOKEN
            },

		    success: function (data) {
		        console.log(data);
                $('#create_vouchers').modal('hide');
                $('#edit').modal('show');
                $('button#create_vouchers_btn').prop('disabled', false);
				// refreshTableAfterChat();
                vouchersTable();
		    },

            error: function (xhr, detail) {

                $('.voucher_error').show();
                $('button#create_vouchers_btn').prop('disabled', false);

                if(xhr.responseJSON.errors.start){

                    $('#start_error').show()
                    $('#start_error').html('<span>'+ xhr.responseJSON.errors.start[0] + '</span>');
                }
                else{
                    $('#start_error').hide();
                }
                if(xhr.responseJSON.errors.end){
                    $('#end_error').show();
                    $('#end_error').html('<span>'+ xhr.responseJSON.errors.end[0] + '</span>');
                }
                else{
                    $('#end_error').hide();
                }
                if(xhr.responseJSON.errors.discount_percent){
                    $('#discount_percent_error').show()
                    $('#discount_percent_error').html('<span>'+ xhr.responseJSON.errors.discount_percent[0] + '</span>');
                }
                else{
                    $('#discount_percent_error').hide();
                }
                if(xhr.responseJSON.errors.number_of_vouchers){
                    $('#number_of_vouchers_error').show()
                    $('#number_of_vouchers_error').html('<span >'+ xhr.responseJSON.errors.number_of_vouchers[0] + '</span>');
                }
                else{
                    $('#number_of_vouchers_error').hide();
                }

		    }


    	});

	})

});

</script>