<div class="modal fade in display_none" id="assign_voucher_code" style="overflow-y: scroll;" tabindex="-1" role="dialog"
     aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form id="assign_voucher_form" action="javascript:void(0)">
            <div class="modal-body">
                <div class="row">

                    <div class="col-md-11">
                        <h3><i class="fa fa-pencil circle"></i> Assign Code</h3>
                    </div>
                </div>

                <hr>
                <br>
                <input type='hidden'  class="form-control" id='voucher_code_id' name="voucher_code_id" />

                <div class="row">
                    <div class="form-group col-md-6">
                        <label>Name<span style="color: red">*</span></label>
                        <input type='text' style="pointer:cursor" class="form-control" id='assignee_name' name="assignee_name" />
                        <div id="assignee_name_error" class=" assign_code_error validation-error"></div>
                    </div>

                    <div class="form-group col-md-6">
                        <label>Email <span style="color: red">*</span></label>
                        <input type='text' class="form-control" id='assignee_email' name="assignee_email" />
                        <div id="assignee_email_error" class=" assign_code_error validation-error "></div>
                    </div>


                    <div class='form-group col-md-12'>
                        <label for="voucher_id">Message</label>
                        <textarea type="text" name="assignee_message" id="assignee_message"
                                  class="chat-detail-border input_empty form-control" rows="6"></textarea>
                        <div id="assignee_message_error" class=" assign_code_error validation-error "></div>
                     </div>


                </div>
                <div class="modal-footer" style="display: flex;">
                    <button type="button" data-dismiss="modal" class="btn btn-light">Cancel</button>
                    <button type="button" class="btn btn-primary" id="assign_vouchers_btn">Send</button>
                </div>
                <hr>
                <div id="previous_messages">
                </div>
            </div>
        </form>

        </div>
    </div>
</div>



