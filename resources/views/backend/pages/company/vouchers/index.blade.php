<div class="card-body">
    <div class="row">

        <div class="col-md-6">
            <div class="form-group mt-4">
                @include('backend.pages.company.partials.vouchers_radio_operations')
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group mt-4 ">
                <a href="#create_vouchers" class="input_create_voucher_empty btn btn-primary" data-toggle="modal"
                   style="float: right; margin-right: 62px; "> Add</a>
            </div>
            <div class="form-group mt-2" style="padding-right: 9.5rem !important;" id="toggle_switch">
                <div class="radio_basic_swithes_padbott text-right ">
                    <input type="checkbox" class="js-switch sm_toggle_company_voucher_assign" name="send_voucher_code"
                           id="send_voucher_code"
                    />
                    <span class="radio_switchery_padding">Send Voucher Code  </span>
                    <br/>
                </div>
            </div>
        </div>


    </div>
    <br>
    <div class="row" style="width: 100% !important;">
        <div class="col-md-12">
            <table class="table  table-bordered table-hover" id="vouchersTable" style="width: 100% !important;">
                <thead>
                <tr>
                    <th style="width:220px">Start</th>
                    <th style="width:220px">End</th>
                    <th style="width:220px">Code</th>
                    <th style="width:300px">Discount</th>
                    <th style="width:180px">Email</th>
                    <th style="width:180px"></th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


<script type="text/javascript">
    // vouchersTable();
    $('body').on('click', '.vouchers-radio', function (e) {


        if ($(this).prop("checked") == true) {
            var record_type = $(this).val();
        }
        else if ($(this).prop("checked") == false) {
            var record_type = 0;
        }
        vouchersTable(record_type);
    })

    function vouchersTable(record_type) {

        record_type = record_type || 'all';

        $('#vouchersTable').dataTable().fnClearTable();
        $('#vouchersTable').dataTable().fnDestroy();

        return $('#vouchersTable').DataTable({
            initComplete: function () {
                $("#vouchersTable_wrapper>.row").attr('style', 'width:100%;');
                $("#vouchersTable_filter>label").css('float', 'right');
                $("#vouchersTable_length>label").css('float', 'left');
            },
            "order": [[0, 'desc'], [1, 'asc']],

            language: {
                'lengthMenu': '_MENU_',
                "search": '',
                "searchPlaceholder": "Search..."
            },

            "autoWidth": false,

            "columnDefs": [
                {"width": "210px", "targets": 0},
                {"width": "210px", "targets": 1},
                {"width": "330px", "targets": 2},
                {"width": "210px", "targets": 3},

            ],

            ajax: {
                url: '<?php echo e(route('company.vouchers.show.all')); ?>',
                data: function (d) {

                    d.company_id = $('#company_id').val();
                    d.record_type = record_type;

                }
            },
            columns: [
                {data: 'start', name: 'start'},
                {data: 'end', name: 'end'},
                {data: 'code', name: 'code'},
                {data: 'discount_percent', name: 'discount_percent'},
                {data: 'email', name: 'email'},
                {data: 'action', name: 'action', orderable: false, searchable: true}

            ],

        });

    }

    $('body').on('change', '#send_voucher_code', function (e) {
        // $('button#assign_vouchers_btn').prop('disabled', true);

        console.log($('#send_voucher_code').is(':checked'));
        if ($('#send_voucher_code').is(':checked') == true) {
            var checked = 1;
        } else {
            var checked = 0;
        }
        var company_id = $('#company_id').val();

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');


        $.ajax({
            url: '/admin/company/vouchers/send/voucher/code/status',
            type: 'POST',
            dataType: 'JSON',
            cache: false,
            data: {company_id: company_id, checked: checked, _token: CSRF_TOKEN},
            success: function (data) {

            },
            error: function (xhr, detail) {

            }
        });


    });

    $('body').on('click', '#assign_vouchers_btn', function (e) {

        $('button#assign_vouchers_btn').prop('disabled', true);
        if ($('#send_voucher_code').is(':checked') == true) {
            var checked = 1;
        } else {
            var checked = 0;
        }
        var company_id = $('#company_id').val();
        var voucher_code_id = $('#voucher_code_id').val();
        var assignee_name = $('#assignee_name').val();
        var assignee_email = $('#assignee_email').val();
        var assignee_message = $('#assignee_message').val();

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            url: '/admin/company/vouchers/assignCode',
            type: 'POST',
            dataType: 'JSON',
            cache: false,
            data: {
                checked: checked,
                company_id: company_id,
                assignee_name: assignee_name,
                assignee_email: assignee_email,
                assignee_message: assignee_message,
                voucher_code_id: voucher_code_id,
                _token: CSRF_TOKEN
            },
            success: function (data) {

                $('button#assign_vouchers_btn').prop('disabled', false);
                $('#assign_voucher_code').modal('toggle');
                vouchersTable();
            },
            error: function (xhr, detail) {

                $('.assign_code_error').show();
                $('button#assign_vouchers_btn').prop('disabled', false);
                console.log(xhr.responseJSON.errors)
                console.log(xhr.responseJSON.errors.assignee_name)

                if (xhr.responseJSON.errors.assignee_name) {


                    $('#assignee_name_error').show()
                    $('#assignee_name_error').html('<span>' + xhr.responseJSON.errors.assignee_name[0] + '</span>');
                }
                else {
                    $('#assignee_name_error').hide();
                }
                if (xhr.responseJSON.errors.assignee_email) {
                    $('#assignee_email_error').show();
                    $('#assignee_email_error').html('<span>' + xhr.responseJSON.errors.assignee_email[0] + '</span>');
                }
                else {
                    $('#assignee_email_error').hide();
                }

                if (xhr.responseJSON.errors.assignee_message) {
                    $('#assignee_message_error').show()
                    $('#assignee_message_error').html('<span>' + xhr.responseJSON.errors.assignee_message[0] + '</span>');
                }
                else {
                    $('#assignee_message_error').hide();
                }


            }
        });

    });


    $('body').on('click', '#assign_voucher_code_btn', function (e) {


        if ($('#send_voucher_code').is(':checked') == true) {
            $('#assign_voucher_form').trigger("reset");
            $('.assign_code_error').text('');
            $('#assign_voucher_code').modal('toggle');
            var voucher_code_id = $(this).data('id');
            $('#voucher_code_id').val(voucher_code_id);

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                url: '/admin/company/vouchers/showMessages',
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {voucher_code_id: voucher_code_id, _token: CSRF_TOKEN},
                success: function (data) {
                    console.log(data, 'data');
                    var messages = '';
                    $.each(data, function (index, val) {

                        messages += '<span><b>' + val.name + '</b> , ' + val.email + '</span><br>';
                        messages += '<span>' + val.message + '</span><br>';
                        messages += '<span>' + val.created_at + '</span><br><br>';
                    });
                    $('#previous_messages').html(messages);
                },
                error: function (xhr, detail) {

                }
            });
        } else {

            var voucher_code_id = $(this).data('id');
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                url: '/admin/company/vouchers/updateIssuedStatus',
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {voucher_code_id: voucher_code_id, _token: CSRF_TOKEN},
                success: function (data) {
                    vouchersTable();
                },
                error: function (xhr, detail) {

                }
            });

        }

    });


</script>