<div class="modal fade in display_none" id="create_chat" style="overflow-y: scroll;" tabindex="-1" role="dialog"
    aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-md-12">
                                <h3><i class="fa fa-pencil circle"></i> Create Chat</h3>
                            </div>
                        </div>
                        <hr>
                        <form id="chatsForm">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label name="detail">Title</label>
                                    <input type="text" name="title" id="title"
                                        class="chat-title-border input_empty form-control" rows="6">
                                </div>
                                <div id="chat_title_error" class="col-md-12 chat_error"></div>

                                <div class='form-group col-md-12 '>
                                    <label for="chat_id">Chat Type</label>
                                    <select type="text" name="chat_id" id="choose_chat_type" placeholder="Franchise"
                                        class="form-control">

                                        <option value="">Chat Type</option>
                                        @foreach($chat_types as $chat_type)
                                        <option value="{{$chat_type->id}}">{{$chat_type->type}}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <div id="chat_type_error" class="col-md-12  chat_error"></div>

                                <div class="form-group col-md-12">
                                    <label name="detail">Detail</label>
                                    <textarea type="text" name="detail" id="detail"
                                        class="chat-detail-border input_empty form-control" rows="6"></textarea>
                                </div>
                                <div id="chat_detail_error" class="col-md-12 chat_error"></div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="display: flex;">
                <button type="button" data-dismiss="modal" class="btn btn-light">Cancel</button>
                <button type="button" class="btn btn-primary" id="create_chat_btn">Save</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>
<script type="text/javascript" src="/backend/js/jquery.min.js"></script>

<script type="text/javascript">

    $( document ).ready(function() {

     $(".input_create_chat_empty").click(function () {
         $(".input_empty").val('');
    });

    $('button#create_chat_btn').click(function(){
        
        $('button#create_chat_btn').prop('disabled', true);
		var title = $('#title').val();
		var detail = $('#detail').val();
        var type = $('#choose_chat_type').val();
       
        $("#chatsTable_filter>label").css('float', 'right');

		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

		$.ajax({

		    url: 'company/chats/store',
		    type: 'POST',
		    data: {
                type: type,
                detail: detail,
                title: title,
                _token: CSRF_TOKEN
            },

		    success: function (data) {
		        $('#create_chat').modal('hide');
                $('#edit').modal('show');
                $('button#create_chat_btn').prop('disabled', false);
				refreshTableAfterChat();
				chatsTable();
		    },

            error: function (xhr, detail) {
               
                $('.chat_error').show();
                $('button#create_chat_btn').prop('disabled', false);
                if(xhr.responseJSON.errors.title){
                    $('#chat_title_error').show()
                    $('#chat_title_error').html('<span>'+ xhr.responseJSON.errors.title[0] + '</span>');
                }
                else{
                    $('#chat_title_error').hide();
                }
                if(xhr.responseJSON.errors.type){
                    $('#chat_type_error').show();
                    $('#chat_type_error').html('<span>'+ xhr.responseJSON.errors.type[0] + '</span>');
                }
                else{
                    $('#chat_type_error').hide();
                }
                if(xhr.responseJSON.errors.detail){
                    $('#chat_detail_error').show()
                    $('#chat_detail_error').html('<span>'+ xhr.responseJSON.errors.detail[0] + '</span>');
                }
                else{
                    $('#chat_detail_error').hide();
                }
              
		    }
            

    	});
		
	})

});

</script>