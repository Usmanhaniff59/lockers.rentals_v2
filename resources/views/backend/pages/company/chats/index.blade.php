<div class="card-body">
    <div class="row">
        <div class="col-md-12">
            <a href="#create_chat" class="input_create_chat_empty btn btn-primary" data-toggle="modal"
                style="float: right; margin-top: 12px; margin-right: 62px; ">Add</a>

        </div>
    </div>
    <br>
    <div class="row" style="width: 100% !important;">
        <div class="col-md-12">
            <table class="table  table-bordered table-hover" id="chatsTable" style="width: 100% !important;">
                <thead>
                    <tr>
                        <th style="width:220px">Title</th>
                        <th style="width:220px">Name</th>
                        <th style="width:220px">Type</th>
                        <th style="width:300px">Detail</th>
                        <th style="width:180px">Created At</th>
                        <th style="width:180px"></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


<script type="text/javascript">

    function chatsTable() {        
        $('#chatsTable').dataTable().fnClearTable();
        $('#chatsTable').dataTable().fnDestroy();

        return $('#chatsTable').DataTable({
            initComplete : function() {
                $("#chatsTable_wrapper>.row").attr('style','width:100%;');
                $("#chatsTable_filter>label").css('float', 'right');
                $("#chatsTable_length>label").css('float', 'left');
            },
            "order":[[0,'desc'],[1,'asc']],
            "order": [],
            language: {
                'lengthMenu': '_MENU_',
                "search": '',
                "searchPlaceholder": "Search..."
            },

            "autoWidth": false,
            // "scrollY": "800px",
            "columnDefs": [
                { "width": "210px", "targets": 0 },
                { "width": "210px", "targets": 1 },
                { "width": "330px", "targets": 2 },
                { "width": "210px", "targets": 3 },

            ],
            ajax: '<?php echo route('company.chats.show.all'); ?>',
            columns: [
                {data: 'title', name: 'title'},
                {data: 'user_id', name: 'user_id'},
                {data: 'chat_type_id', name: 'chat_type_id'},
                {data: 'details', name: 'detail'},
                {data: 'created_at', name: 'created_at'},
                {data: 'action', name: 'action', orderable: false, searchable: true}

            ],

        });

    }

</script>