<div class="modal fade in display_none model-document-id" style="overflow-y: scroll;" id="edit_document" tabindex="-1"
    role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-md-12">
                                <h3><i class="fa fa-pencil circle"></i> Edit Document</h3>
                            </div>
                        </div>
                        <hr>
                        <div class="loader" id="block_loader">
                        </div>
                        <form id="documentEditForm" enctype="multipart/form-data">
                            <input type="hidden" id="id">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Title</label>
                                    <input type="text" name="edit_title" id="edit_title"
                                        class="input_empty form-control">
                                </div>
                                <div class='form-group col-md-12'>
                                    <label for="document_type">Type</label>
                                    <select class="form-control input_empty" name="edit_type" id="edit_type">
                                        <option value="">Choose Document Type</option>
                                        @foreach($document_types->where('location', 'Company')->where('active', '1') as
                                        $document_type)
                                        <option value="{{$document_type->id}}">{{$document_type->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="edit_file" class="upload-area" id="editfile">
                                        <i class="fas fa-cloud-upload-alt"></i>
                                        <h1 id="placeholder" class="placeholder">Click or drop file here</h1>
                                    </label>
                                    <br>
                                    <input id="edit_file" name="edit_file" type="file" style="display:none" />
                                </div>

                            </div>
                            <div class="modal-footer" style="display: flex;">
                                <button type="button" id="edit_cancel" data-dismiss="modal"
                                    class="btn btn-light">Cancel</button>
                                <button type="submit" class="btn btn-primary update"
                                    id="edit_document_btn">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>

<script type="text/javascript">
    $("html").on("drop", function (e) {
            e.preventDefault();
            e.stopPropagation();
            file = e.originalEvent.dataTransfer.files;
            $("#placeholder").text(file[0].name);
            dragcheck = 2;
        });
    $("#edit_file").change(function () {
            var filees = $("#edit_file").val();
            var filename = filees.split('\\').pop();
            $("#placeholder").text(filename);
            dragcheck=0;
        });
</script>