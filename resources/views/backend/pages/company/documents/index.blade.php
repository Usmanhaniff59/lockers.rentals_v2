
<div class="card-body">
    <div class="row">
        <div class="col-md-12">
            <a href="#create_document" class="input_create_document_empty btn btn-primary" data-toggle="modal"
                style="float: right; margin-top: 12px; margin-right: 62px; ">Add</a>

        </div>
    </div>
    <br>
    <div class="row" style="width: 100% !important;">
        <div class="col-md-12">
            <table class="table  table-bordered table-hover" id="documentsTable" style="width: 100% !important;">
                <thead>
                    <tr>
                        <th style="width:220px">Title</th>
                        <th style="width:220px">Type</th>
                        <!-- <th style="width:220px">Uploaded By</th>
                        <th style="width:220px">Date Updated</th> -->
                        <th style="width:220px">Details</th>
                        <th style="width:330px">&nbsp;</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>



<script type="text/javascript">
  
    function documentsTable() {
        $('#documentsTable').dataTable().fnClearTable();
        $('#documentsTable').dataTable().fnDestroy();

        return $('#documentsTable').DataTable({
            initComplete : function() {
                $("#documentsTable_wrapper>.row").attr('style','width:100%;');
                $("#documentsTable_filter>label").css('float', 'right');
                $("#documentsTable_length>label").css('float', 'left');
            },
            "order":[[0,'desc'],[1,'asc']],
            "order": [],
            language: {
                'lengthMenu': '_MENU_',
                "search": '',
                "searchPlaceholder": "Search..."
            },


            "autoWidth": false,

            "columnDefs": [
                { "width": "220px", "targets": 0 },
                { "width": "220px", "targets": 1 },
                { "width": "220px", "targets": 2 },
                { "width": "330px", "targets": 3 },
                // { "width": "220px", "targets": 4 },
            ],
            ajax: '<?php echo route('company.documents.show.all'); ?>',
            columns: [
                {data: 'title', name: 'title'},
                {data: 'type_id', name: 'type_id'},
                {data: 'details', name: 'details'},
                // {data: 'updated_at', name: 'updated_at'},
                {data: 'action', name: 'action', orderable: false, searchable: true}
            ],

        });

    }
</script>