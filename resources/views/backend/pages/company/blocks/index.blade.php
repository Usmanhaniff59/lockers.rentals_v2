<div class="card-body">
    <div class="row">
        <div class="col-md-12">
            <a href="javascript:void(0)" class="input_create_block_empty btn btn-primary" data-toggle="modal"
                style="float: right; margin-top: 12px; margin-right: 62px; ">Add</a>
            <div style="float: right; margin-top: 16px; padding-right: 24px; ">
                <input type="checkbox" class="js-switch sm_toggle_show_only_deleted_block" name="roles" value="1"
                    id="get_deleted_blocks" />
                <span class="radio_switchery_padding">Recycle bin</span>
            </div>
        </div>
    </div>
    <br>
    <div class="row" style="width: 100% !important;">
        <div class="col-md-12">
            <table class="table  table-bordered table-hover  " id="blocksTable" style="width: 100% !important;">
                <thead>
                    <tr>
                        <th>Type</th>
                        <th>Name</th>
                        <th>Tier</th>
                        <th>Blocks</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">

</script>