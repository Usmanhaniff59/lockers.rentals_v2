

<div class="modal fade in display_none model-block-id" style="overflow-y: scroll;" id="edit_block" tabindex="-1" role="dialog" aria-hidden="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
		    <div class="modal-body">
		        <div class="row">
		            <div class="col-12">
		                <div class="row">
							<div class="col-md-12">
								<h3><i class="fa fa-pencil circle"></i> Edit </h3>
							</div>
						</div>
		                <hr>
		                <form id="creat_edit_block">
							<div class="row">
								<div class="form-group col-md-6">
									<label name = "name" >Name</label>
									<input type="text" name="name" id="namee_edit" class="form-control">
								</div>
								<div class='form-group col-md-6 existed-franchisee-group'>
									<label for="franchisee_id">Block Group</label>
									<select class="form-control input_empty" id="block_group_edit">
										<option value="" selected>Choose block group</option>
										@foreach ($BlockGroup as $block)
										<option value="{{$block->id}}">{{$block->name}}</option>
										@endforeach
										<!-- <option value="m">Manual</option> -->
										
									</select>
									<p style="color: red"  id="BlockeditError"></p>
								</div>
								
							</div>
							<div class="row">
								<div class='form-group col-md-6 existed-franchisee-group'>
									<label for="franchisee_id">Lock Type</label>
									<select class="form-control" id="lock_type_edit">
										<option value="">Choose Lock Type</option>
										<option value="m">Manual</option>
										<option value="s">Static</option>
										<option value="a">Automatic</option>
									</select>
								</div>
								<div class="form-group col-md-6">
									<label name = "name" >Order</label>
									<input type="number" min="1" name="name" id="order_edit" class="form-control">
								</div>

							


							</div>

							<div class="row">
									<div class='form-group col-md-6 existed-franchisee-group'>
									<label for="franchisee_id">Block Type</label>
									<select class="form-control" id="block_type_edit" name="block_type_edit">
										<option value="I">Inside</option>
										<option value="O">Outside</option>
									</select>
								</div>
								<div class="form-group col-md-6">
									<label name = "name" >Tier</label>
									<!-- <input type="number" min="1" name="name" class="form-control" id="block_height_edit"> -->
									<select name="name" class="form-control"
										   id="block_height_edit">
										   	
										   </select>
								</div>
								

							</div>
							<div class="row">
								<div class="form-group col-md-6 ">
									<label name = "name" >Locker Blocks</label>
									<input type="number" min="1" name="locker_blocks_edit" class="form-control" id="locker_blocks_edit">
								</div>

								<div class='form-group col-md-6 existed-franchisee-group'>
									<label for="franchisee_id">School Year</label>
									<select class="chzn-select input_empty"  multiple="" multiple class="chosen-select" size="3" style="display: none;"  name="franchisee_id" id="choose_school_year_edit">
										<option value="" selected>Choose School Year</option>
									</select>
								</div>

							

							</div>

							<div class="row">
								<div class='form-group col-md-6'>
									<label for="status">Rates</label>
									<select class="form-control" name="rate_id" id="rate_id" >
										<option value="" selected>Choose Rate</option>
										@foreach ($rates as $rate)
											<option value="{{$rate->id}}"  >{{$rate->name}}</option>
										@endforeach
									</select>
								</div>
								<div class='form-group col-md-6'>
									<label for="status">Locker Description</label>
									<select class="form-control input_empty" name="color_locker" id="color_locker_edit">
										<option value="" selected>Choose Color</option>
										@foreach ($LockerColor as $color_c)
										<option value="{{$color_c->id}}">{{$color_c->color}}</option>
										@endforeach
									</select>
								</div>
								
									<input type="hidden" name="asda" id="block_id">
									<p style="color: red"  id="image_validation_edit"></p>

								</div>
								<div class="row date_blocks">
									



								<div class="form-group col-md-6">
					                <label><b>Start Date</b></label>
					                <div class="clearfix"></div>
					                 <input readonly="" style="cursor: pointer;" type="text"  id="start_block" class="form-control" placeholder="Start Date" >
					            </div>
					            	<div class="form-group col-md-6 end_block">
					                <label><b>End Date</b></label>
					                <div class="clearfix"></div>
					                 <input readonly="" style="cursor: pointer;" type="text"  id="end_block"  class="form-control" placeholder="End Date" >
					                 <p style="color: red"  id="dateError"></p>
					            </div>

							</div>
							<div class="row">
								<div class="form-group col-md-6">
									<div class="checkbox">
										<label>
											<input type="checkbox"  id="display_edit_update" >
											<span class="cr"><i class="cr-icon fa fa-check"></i></span>
											Display
										</label>
									</div>
								</div>
							</div>
	                    </form>
		            </div>
		        </div>
		    </div>
		    <div class="modal-footer" style="display: flex;">
		        <button type="button" data-dismiss="modal" class="btn btn-light">Cancel</button>
		        <button type="button" class="btn btn-primary" id="create_edit">Update</button>
		    </div>
		</div>
	</div>
</div>

<script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>

<script type="text/javascript">
       $(function() {
// var myDate1 = Date.parseExact("29-11-2010", "dd-MM-yyyy");
$('#start_block').datepicker({
   dateFormat: 'dd-mm-yy',
    dateonly: true,
   });
// $('#end_block').datepicker();
$("#end_block").datepicker({
   dateFormat: 'dd-mm-yy',
    dateonly: true,
   });
 
            // $("#mid_date").datepicker();
        });

	$( document ).ready(function() {
	    //school years
	    $.ajax({

		    url: '/getSchoolYear',

		    type: 'GET',

		    dataType: 'JSON',

		    cache: false,

		    success: function (data) {

		        var html = '';
		        for (var i = 0; i < data.length; i++){
		        	html += '<option value="'+data[i].id+'">'+data[i].school_year+'</option> ';
		        }
		        $('#choose_school_year_edit').html(html);
		       


		    },

		    error: function (jqXHR, textStatus, errorThrown) {
		        console.log(jqXHR);
		    }

    	});


	});


	// $(document).on('click','#block-edit-company',function(){
	// 	var id=$(this).attr('data-id');
	// 	$('#block_id').val(id);
	// 	var name = $(this).attr('data-name');
	// 	$('#namee_edit').val(name);

	// 	var locker_type= $(this).attr('data-lockerType');
	// 	alert(locker_type);
	// 	// $('#lock_type_edit').val(locker_type);
	// 	$("#lock_type_edit option[value='"+locker_type+"']").attr('selected','selected'); 
	// 	$('#edit').modal('toggle');

	// });
	//update the block
	$('#display_edit_update').click(function(){
		
		if($(this).is(":checked")){
			$('.date_blocks').css('display','');

		}else{
			$('.date_blocks').css('display','none');
			
		}
	});
	$('button#create_edit').click(function(){
        $('#image_validation_edit').text('');
        $('button#create_edit').prop('disabled', true);
		var name = $('#namee_edit').val();
		var lock_type = $('#lock_type_edit').val();
		var block_group = $('#block_group_edit').val();

		var order =  $('#order_edit').val();
		var block_height = $('#block_height_edit').val();
		var locker_blocks = $('#locker_blocks_edit').val();
		var block_type = $('#block_type_edit').val();
		var choose_school_year = $('#choose_school_year_edit').val();
		// console.log(choose_school_year);
		var rate_id = $('#rate_id').val();
		var color_locker = $('#color_locker_edit').val();
		
		var company_id = $('#comapny_id').val();
		var location_group_idd = $('#location_group_idd').val();
		var id = $('#block_id').val();

		var block_images_edit = $('#block_images_edit').val();
		var display = $('#display_edit_update').is(":checked");

		var start_date = $('#start_block').val();
		var res = start_date.split("-");
		var start_date1=res[1]+'-'+res[0]+'-'+res[2];
		var endDate = $('#end_block').val();
		if(endDate!=''){
			var res1 = endDate.split("-");
			 var endDate1=res1[1]+'-'+res1[0]+'-'+res1[2];
		// var diff = new Date(endDate - start_date);
		var start = new Date(start_date1),
		    end   = new Date(endDate1),
		 diffDays = Math.floor((end - start) / (1000 * 60 * 60 * 24), 10); 
		// var days = diff/1000/60/60/24;
			
	}else{
		var diffDays=0;
	}

		// alert(diffTime);
		var i=0;
		if(endDate!='' && diffDays<0)
		{
			$('#dateError').html('End date should be greater than Start date ');
			$('button#create_edit').prop('disabled', false);
		i++;

		}else{
			$('#dateError').html(' ');
		}


				if(block_group=='')
		{
			$('#BlockeditError').html('Select Block Group');
			$('button#create_edit').prop('disabled', false);
			i++;
		}else{
			$('#BlockeditError').html(' ');
		}

		console.log(display);
		var is_display = 0;
		if(display){
			is_display = 1;
		} else {
			is_display = 0;
		}
		// console.log(name, lock_type, order, block_height, locker_blocks, display, image, choose_school_year, block_images_edit);
		if(i==0){
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
		$.ajax({

		    url: '/blocks/update/'+id,

		    type: 'PUT',

		    dataType: 'JSON',

		    cache: false,

		    data: {
                name: name,
                lock_type: lock_type,
                order_list: order,
                block_height: block_height,
                locker_blocks: locker_blocks,
                display: is_display,
                active: 1,
                block_type: block_type,
                choose_school_year: choose_school_year,
                company_id: company_id,
				rate_id: rate_id,
                location_group_id: location_group_idd,
                block_images_edit: block_images_edit,
                _token: CSRF_TOKEN,
                block_group:block_group,
                color_locker:color_locker,
                start_date:start_date,
                endDate:endDate,
            },

		    success: function (data) {

                $('button#create_edit').prop('disabled', false);

                if(data=='image_validation'){

                    $('#image_validation_edit').text('This configuration does not have an image');

                }else {
                    $('#edit_block').modal('hide');
                    $('#edit_block').modal('toggle');
                    $('#creat_edit_block').trigger("reset");
                    getLatest();
                    blocksTable();
                }
		    },
		    error: function (jqXHR, textStatus, errorThrown) {
                $('button#create_edit').prop('disabled', false);
		    }

    	});
	}
		
	})

</script>