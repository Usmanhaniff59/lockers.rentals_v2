<div class="modal fade in display_none" id="create_block" style="overflow-y: scroll;" tabindex="-1" role="dialog"
	aria-hidden="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-12">
						<div class="row">
							<div class="col-md-12">
								<h3><i class="fa fa-pencil circle"></i> Create </h3>
							</div>
						</div>
						<hr>
						<form id="creat_block">
							<div class="row">
								<div class="form-group col-md-6">
									<label name="name">Name</label>
									<input type="text" name="name" id="namee" class="input_empty form-control">
								</div>
								<div class='form-group col-md-6 existed-franchisee-group'>
									<label for="franchisee_id">Block Group</label>
									<select class="form-control input_empty" id="block_group">
										<option value="" selected>Choose block group</option>
										@foreach ($BlockGroup as $block)
										<option value="{{$block->id}}">{{$block->name}}</option>
										@endforeach
										<!-- <option value="m">Manual</option> -->
										
									</select>
									 <p style="color: red"  id="BlockError"></p>
								</div>
							
							</div>

							<div class="row">
									<div class='form-group col-md-6 existed-franchisee-group'>
									<label for="franchisee_id">Lock Type</label>
									<select class="form-control input_empty" id="lock_type">
										<option value="" selected>Choose Lock Type</option>
										<option value="m">Manual</option>
										<option value="s">Static</option>
										<option value="a">Automatic</option>
									</select>
								</div>
								<div class="form-group col-md-6">
									<label name="name">Order</label>
									<input type="number" min="1" name="name" id="order"
										class="input_empty form-control">
								</div>

								

							</div>
							<div class="row">
								<div class='form-group col-md-6'>
									<label for="block_type">Block Type</label>
									<select class="form-control" id="block_type" name="block_type">
 										<option value="I" selected>Inside</option>
										<option value="O">Outside</option>
 									</select>
								</div>
								<div class="form-group col-md-6">
									<label name="name">Tier</label>
									<!-- <input type="number" min="1" name="name" class="form-control input_empty"
										   id="block_height"> -->
										   <select name="name" class="form-control input_empty"
										   id="block_height">
										   	
										   </select>
								</div>
							
							</div>


							<div class="row">
									<div class="form-group col-md-6">
									<label name="name">Locker Blocks</label>
									<input type="number" min="1" name="name" class="form-control input_empty"
										id="locker_blocks">
								</div>

								<div class='form-group col-md-6 existed-franchisee-group'>
									<label for="franchisee_id">School Year</label>
									<select class="chzn-select input_empty" multiple="" multiple class="chosen-select"
											size="3" style="display: none;" name="franchisee_id" id="choose_school_year">

									</select>
									<!-- <select class="js-example-basic-multiple" name="states[]" multiple="multiple">
									  <option value="AL">Alabama</option>
									  <option value="WY">Wyoming</option>
									</select> -->
								</div>

							

							</div>
							<div class="row">
									<div class='form-group col-md-6'>
									<label for="status">Rates</label>
									<select class="form-control input_empty" name="rate_id" id="rate_id_create">
										<option value="" selected>Choose Rate</option>
										@foreach ($rates as $rate)
										<option value="{{$rate->id}}">{{$rate->name}}</option>
										@endforeach
									</select>
								</div>
									<div class='form-group col-md-6'>
									<label for="status">Locker Description</label>
									<select class="form-control input_empty" name="color_locker" id="color_locker_create">
										<option value="">Choose Color</option>
										@foreach ($LockerColor as $color_c)
										<option value="{{$color_c->id}}">{{$color_c->color}}</option>
										@endforeach
									</select>
								</div>
							
							</div>
							<div  class="row create_date_block">
								
								<div class="form-group col-md-6">
					                <label><b>Start Date</b></label>
					                <div class="clearfix"></div>
					                 <input readonly="" style="cursor: pointer;" type="text"  id="create_start_block" class="form-control" value="<?php echo date('d-m-Y')?>" placeholder="Start Date" >
					            </div>
					            	<div class="form-group col-md-6">
					                <label><b>End Date</b></label>
					                <div class="clearfix"></div>
					                 <input readonly="" style="cursor: pointer;" type="text"  id="create_end_block" class="form-control" placeholder="End Date" >
					                 <p style="color: red"  id="dateErrorCreate"></p>
					            </div>
							</div>
							<div class="row">
									<div class="form-group col-md-6">
									<div class="checkbox">
										<label>
											<input type="checkbox" id="display" class="input_empty_checkbox">
											<span class="cr"><i class="cr-icon fa fa-check"></i></span>
											Display
										</label>
									</div>
									<p style="color: red"  id="image_validation_create"></p>
								</div>	
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="modal-footer" style="display: flex;">
				<button type="button" data-dismiss="modal" class="btn btn-light">Cancel</button>
				<button type="button" class="btn btn-primary" id="create">Create</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>
<script type="text/javascript" src="/backend/js/jquery.min.js"></script>

<script type="text/javascript">
		$('#display').click(function(){
		
		if($(this).is(":checked")){
			$('.create_date_block').css('display','');

		}else{
			$('.create_date_block').css('display','none');
			
		}
	});
	   $(document).ready(function() {

$('#create_start_block').datepicker({

    dateFormat: 'dd-mm-yy',
    dateonly: true,
   });
$('#create_end_block').datepicker({
    dateFormat: 'dd-mm-yy',
    dateonly: true,
   });
        });
	$( document ).ready(function() {
		fetchTier();
	    //empty the block create form
		$(".input_create_block_empty").click(function () {
			$(".input_empty").val('');
            $('#image_validation_create').text('');
            $(".input_empty_checkbox").attr('checked',false);
		});

		//get school year
	    $.ajax({

		    url: '/getSchoolYear',

		    type: 'GET',

		    dataType: 'JSON',

		    cache: false,

		    success: function (data) {

		        var html = '';
		        for (var i = 0; i < data.length; i++){
		        	if(i==0){
		        	html += '<option value="">'+data[i].school_year+'</option> ';
			        }else{
			        	html += '<option value="'+data[i].id+'">'+data[i].school_year+'</option> ';
			        }
		        }
		        $('#choose_school_year').html(html);

		    },

		    error: function (jqXHR, textStatus, errorThrown) {

		    }

    	});

	});

function fetchTier(){
		var val=$('#block_type').val();
		
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

		$.ajax({

		    url: '/blocks/tiers',

		    type: 'POST',

		    dataType: 'JSON',

		    cache: false,

		    data: {
               
                block_type: val,
                _token: CSRF_TOKEN
            },

		    success: function (data) {
		    	console.log(data);
		    	console.log('data tier');
		    	var html='';
		    	html+='<option value="">Select Tier</option>';

		    	for (var i = 0; i < data.length; i++){
		        	html += '<option value="'+data[i].tier+'">'+data[i].tier+'</option> ';
		        }

             $('#block_height').html(html);
		    },

		    error: function (jqXHR, textStatus, errorThrown) {
                $('button#create').prop('disabled', false);
		    }

    	});


}
	$('#block_type').change(function(){
		// alert('123');
		fetchTier();
	
});
	//create new block in company
	$('button#create').click(function(){
        $('#image_validation_create').text('');
        $('button#create').prop('disabled', true);
		var name = $('#namee').val();
		var lock_type = $('#lock_type').val();
		var order =  $('#order').val();
		var block_height = $('#block_height').val();
		var locker_blocks = $('#locker_blocks').val();
		var block_type = $('#block_type').val();
		var choose_school_year = $('#choose_school_year').val();
		var block_group = $('#block_group').val();


		var company_id = $('#comapny_id').val();
		var location_group_idd = $('#location_group_idd').val();
		var rate_id = $('#rate_id_create').val();
		var color_locker = $('#color_locker_create').val();
	
		var start_date = $('#create_start_block').val();
		var endDate = $('#create_end_block').val();
		var i=0;
		if(endDate!='' && start_date>endDate)
		{
			$('#dateErrorCreate').html('End date should be greater than Start date ');
			  $('button#create').prop('disabled', false);
			  i++;
			// return false;

		}else{
			$('#dateErrorCreate').html(' ');
		}
				if(block_group=='')
		{
			$('#BlockError').html('Select Block Group');
			$('button#create').prop('disabled', false);
			i++;
		}else{
			$('#BlockError').html(' ');
		}
		var display = $('#display').is(":checked")

		var is_display = 0;
		if(display){
			is_display = 1;
		} else {
			is_display = 0;
		}

		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
		if(i==0){
		$.ajax({

		    url: '/blocks/store',

		    type: 'POST',

		    dataType: 'JSON',

		    cache: false,

		    data: {
                name: name,
                lock_type: lock_type,
                order_list: order,
                block_height: block_height,
                locker_blocks: locker_blocks,
                display: is_display,
				rate_id: rate_id,
                active: 1,
                block_type: block_type,
				school_academic_year_id: choose_school_year,
                company_id: company_id,
                location_group_id: location_group_idd,
                _token: CSRF_TOKEN,
                block_group:block_group,
                color_locker:color_locker,
                start_date:start_date,
                endDate:endDate,
            },

		    success: function (data) {

                $('button#create').prop('disabled', false);

                if(data=='image_validation'){

					$('#image_validation_create').text('This configuration does not have an image');

                }else {

                    $('#create_block').modal('hide');
                    $('#create_block').modal('toggle');

                    refreshTable();
                    getLatest();
                    blocksTable();
                }
		    },

		    error: function (jqXHR, textStatus, errorThrown) {
                $('button#create').prop('disabled', false);
		    }

    	});
	}
		
	})

	//update the blocks dropdown in lockers tab
	function getLatest(){
		$.ajax({
		    url: '/blocks/getUpdated',
		    type: 'GET',
		    dataType: 'JSON',
		    cache: false,
		    success: function (data) {

		      $('#blocksul').html(data);
		      $('#move_locker_outside_block').html(data);
		    },
		    error: function (jqXHR, textStatus, errorThrown) {

		    }
    	});
	}

</script>