<style type="text/css">
   .search-input{
    position: absolute;
    margin-top: 6px;
   } 
   .add-user-button{
    margin-top: 12px;
   }
</style>

<div class="card-body">
    <div class="row">
        <div class="col-md-12 add-user-button">
            @can('Create User')
                                   <!--  <a href="javascript:void(0)"
                                       class="btn btn-primary pull-right btn-md  header_icon_margin_left add-new-user-btn"
                                      id="add-new-user-btn">Add New User</a> -->
                                      <a href="#user-create-modal" data-toggle="modal" class="btn btn-info pull-left button-style user-edit-btn" onclick="addUserCompany(41081)" style="margin-right: 62px; float: right;">Add New User</a>
                                @endcan
        </div>
    </div>
    <!-- <div class="row">
        <div class="col-md-12" style="margin: 24px 1px 0 0;">
            <div class="form-group col-md-4" style="float:left;">
                <label><b>Live/Archive Filters</b></label>
                <div class="clearfix"></div>
                <label class="custom-control custom-radio signin_radio1 d-inline-block pr-3" style="margin-left: 10px;">
        <input id="move_radio" name="search_block" value="Live" type="radio" class="custom-control-input search-input">
        <span class="custom-control-label"></span>
        <span class="custom-control-description">Live</span>
    </label>
    <label class="custom-control custom-radio signin_radio1 d-inline-block pr-3" style="margin-left: 10px;">
        <input id="move_radio" name="search_block" value="Archive" type="radio" class="custom-control-input search-input">
        <span class="custom-control-label"></span>
        <span class="custom-control-description">Archive</span>
    </label>
      <label class="custom-control custom-radio signin_radio1 d-inline-block pr-3" style="margin-left: 10px;">
        <input id="move_radio" checked name="search_block" value="All" type="radio" class="custom-control-input search-input">
        <span class="custom-control-label"></span>
        <span class="custom-control-description">All</span>
    </label>
            </div>
            <div class="form-group col-md-3" style="float:left;">
                <label><b>Status Filters</b></label>
                <div class="clearfix"></div>
                  <label class="custom-control custom-radio signin_radio1 d-inline-block pr-3" style="margin-left: 10px;">
        <input id="move_radio" name="status_filter" value="Booked" type="radio" class="custom-control-input search-input">
        <span class="custom-control-label"></span>
        <span class="custom-control-description">Booked</span>
    </label>
     <label class="custom-control custom-radio signin_radio1 d-inline-block pr-3" style="margin-left: 10px;">
        <input id="move_radio" name="status_filter" value="Lost" type="radio" class="custom-control-input search-input">
        <span class="custom-control-label"></span>
        <span class="custom-control-description">Lost</span>
    </label>
            </div>
            <div class="col-md-3" style="float:left;">
                <label><b>Select Date</b></label>
                <div class="clearfix"></div>
                 <input type="text"  id="mid_date" class="form-control" placeholder="Select Date" >
            </div>
            <div class="form-group col-md-2 " style="float: right;">
                <label></label>
                <a href="#create_pupil_premium" class="input_create_pupil_premium_empty btn btn-primary create_pupil_premium_btn" data-toggle="modal" id=""
                     style="margin-top: 30px;"> Add</a>
            </div>
        </div>
        
    </div> -->
    <br>
    <div class="row" style="width: 100% !important;">
        <div class="col-md-12">
            <table class="table  table-bordered table-hover" id="assign-user" style="width: 100% !important;">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <!-- <th>Block</th> -->
                        <!-- <th>Locker Number</th> -->
                        <!-- <th>Start</th> -->
                        <!-- <th>End</th> -->
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript" src="/backend/js/orders/choose_locker_order.js"></script>
 <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk="   crossorigin="anonymous"></script>

<script type="text/javascript">
   

    function assignTableUSers() {

        $('#assign-user').dataTable().fnClearTable();
        $('#assign-user').dataTable().fnDestroy();

        return $('#assign-user').DataTable({
            initComplete : function() {
                $("#assign-user_wrapper>.row").attr('style','width:100%;');
                $("#assign-user_filter>label").css('float', 'right');
                $("#assign-user_length>label").css('float', 'left');
            },
            "order":[[0,'desc'],[1,'asc']],

            language: {
                'lengthMenu': '_MENU_',
                "search": '',
                "searchPlaceholder": "Search..."
            },

            "autoWidth": true,


            ajax: {
                url: '<?php echo e(route('company.users.show.all')); ?>',
                data: function (d) {
                    // alert($('#mid_date').val());
                    d.company_id = $('#company_id').val();
                    // d.searchParam = $('input[name="search_block"]:checked').val();
                    // d.searchStatus = $('input[name="status_filter"]:checked').val();
                    // d.searchDate = $('#mid_date').val();



                }
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'action', name: 'action', orderable: false, searchable: true}

            ],

        });

    }




</script>