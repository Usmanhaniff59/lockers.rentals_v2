  {{ Form::open(array('route' => 'company.users.store', 'id'  => "user-save")) }}
            <div class="modal-body">
                @include('errors.js-validation')
                <div class="row">
                    <div class="col-12">
                        <h3><i class="fa fa-plus-circle circle"></i> Create New User</h3>
                        <hr>
                        <input type="hidden" name="company_id" id="company_id" value="{{$companyId}}">
                        <div class="form-group">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name', '', array('class' => 'form-control')) }}
                            <div id="add_name_error" class=" user_error validation-error "></div>

                        </div>

                        <div class="form-group">
                            {{ Form::label('email', 'Email') }}
                            {{ Form::email('email', '', array('class' => 'form-control')) }}
                            <div id="add_email_error" class=" user_error validation-error "></div>

                        </div>
                        @can('Ticketing Permission')
                        <div class="row">
                            <div class="col-12">
                                <h5><b>Ticketing Response Group</b></h5>
                            </div>
                            <div class="form-group col-12">
                                <label class="radio-inline">
                                    <input type="radio" class="mr-2" name="response_group_id" value="2">Client
                                </label>
                                <label class="radio-inline pl-3">
                                    <input type="radio" class="mr-2" name="response_group_id" checked value="1"> Customer
                                </label>
                                <label class="radio-inline  pl-3">
                                    <input type="radio" class="mr-2" name="response_group_id" value="3">Developer
                                </label>
                                <label class="radio-inline  pl-3">
                                    <input type="radio" class="mr-2" name="response_group_id" value="4">Developer Manager
                                </label>
                                <div id="add_response_group_id_error" class=" user_error validation-error "></div>

                            </div>

                        </div>
                        @endcan


                        <input type="hidden" name="roles" id="roles" value="3">
                      <!--   @can('Assign Role To User')
                            <div class='form-group'>
                                <select name="roles" id="roles" class="form-control">
                                    <option value="">Select Role</option>
                                @foreach ($roles as $index=>$role)
                                @if($role->name=='User')
                                <option value="{{$role->id}}">{{ ucfirst($role->name) }}</option>
                                @endif
                             
                                @endforeach
                                </select>
                            </div>
                            <div id="add_roles_error" class=" user_error validation-error "></div>
                        @endcan -->
                     <!--    @can('Assign Franchisee To User')

                            <div class="form-group  existed-franchisee-group">
                                <label for="franchisee_id">Franchisee</label>
                                <select size="3" multiple class="form-control chzn-select" id="franchisee_id"
                                        name="franchisee_groups[]"  tabindex="8">
                                    @foreach ($franchisees as $franchisee)
                                    @if(isset($franchisee->franchisee_companies) && !empty($franchisee->franchisee_companies) && count($franchisee->franchisee_companies)>0)
                                        <option value="{{$franchisee->id}}" >{{$franchisee->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group new-franchisee-group-input d-none">
                                {{ Form::label('franchisee_name', 'Franchisee Group Name') }}
                                {{ Form::text('franchisee_name', '', array('class' => 'form-control franchisee_name')) }}
                            </div>
                            <h6 class='text-center'>OR</h6>

                            <div class='form-group text-center new-franchisee-group-button-field'>
                                <button type="button"  class="btn btn-primary create-new-franchisee-group-button">Create New Franchisee Group</button>
                            </div>

                            <div class='form-group text-center existed-franchisee-group-button-field d-none'>
                                <button type="button"  class="btn btn-success existed-franchisee-group-button" id="">Choose Existing Franchisee Group</button>
                            </div>

                        @endcan -->
              

                        <div class="form-group">
                            {{ Form::label('password', 'Password') }}<br>
                            {{ Form::password('password', array('class' => 'form-control password_create')) }}
                             <div id="add_password_error" class=" user_error validation-error "></div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('password', 'Confirm Password') }}<br>
                            {{ Form::password('password_confirmation', array('class' => 'form-control password_create')) }}

                        </div>
                        @error('password_confirmation')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror

                    </div>
                </div>
            </div>
            <div class="modal-footer" style="display: flex;">
                <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                <button type="button"  id="user-save-btn"  onclick="saveData('user-save','user-save-btn','user-create-modal',user_table)"  class="btn btn-primary">Save</button>

            </div>
            {{ Form::close() }}

            <script type="text/javascript">

     $(".hide_search").chosen({disable_search_threshold: 10});

    $(".chzn-select").chosen({allow_single_deselect: true});

         $(".chzn-select-deselect,#select2_sample").chosen();

        $(".chzn-select").chosen({
            allow_single_deselect: true,
            search_contains: true, // kwd can be anywhere
            max_selected_options: 1,
            width: "95%",
            no_results_text: "Oops, nothing found!"
        });

        function Chosen() {
            return Chosen.__super__.constructor.apply(this, arguments);
    }


        //make toggle objects against roles
        var role_access = "{{auth()->user()->can('Edit User')}}";

        if (role_access.length > 0) {
            var roles = "{{ count($roles) }}";
            console.log(roles);
            for (var i = 0; i < roles; i++) {
                new Switchery(document.querySelector('.sm_toggle_update' + i), {
                    size: 'small',
                    color: '#00c0ef',
                    jackColor: '#fff'
                });

            }
        }

</script>
