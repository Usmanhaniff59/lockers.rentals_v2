
<div class="alert alert-danger print-error-msg" style="display:none">
        <ul></ul>
    </div>
<!-- <div class="alert alert-danger error-messages" style="display:none"></div> -->
<form action="javascript:void(0)" method="put" class="" id="update-user-info">
	@csrf
<!-- {{ Form::model($user, array('route' => array('company.users.update', $user->id), 'method' => 'PUT')) }} {{-- Form model binding to automatically populate our fields with user data --}} -->
<div class="modal-body">
    <div class="row">
        <div class="col-12">
            <h3><i class="fa fa-pencil circle"></i>
                Edit {{$user->name}}</h3>
            <hr>
            <input type="hidden" name="user_profile" value="{{$user->id}}">
            <div class="row">
                <div class="form-group col-6">
                    {{ Form::label('name', 'Name') }}
                    {{ Form::text('name', null, array('class' => 'form-control')) }}
                    <span id="name_error" class=" user_error validation-error "></span>
                </div>
                
                <div class="form-group col-6">
                    {{ Form::label('email', 'Email') }}
                    {{ Form::email('email', null, array('class' => 'form-control')) }}
                     <span id="email_error" class=" user_error validation-error "></span>
                </div>
               
            </div>

            <div class="row">
                <div class="form-group col-6">
                    {{ Form::label('phone_number', 'Phone Number') }}
                    {{ Form::tel('phone_number', null, array('class' => 'form-control')) }}
                </div>
                <div class="form-group col-6">
                    {{ Form::label('address_1', 'Address 1') }}
                    {{ Form::text('address_1', null, array('class' => 'form-control')) }}
                </div>
            </div>



            <div class="row">
                <div class="form-group col-6">
                    {{ Form::label('address_2', 'Address 2') }}
                    {{ Form::text('address_2', null, array('class' => 'form-control')) }}
                </div>
                <div class="form-group col-6">
                    {{ Form::label('city', 'City') }}
                    {{ Form::text('city', null, array('class' => 'form-control')) }}
                </div>
            </div>

            <div class="row">
                <div class="form-group col-6">
                    {{ Form::label('post_code', 'Post Code') }}
                    {{ Form::text('post_code', null, array('class' => 'form-control')) }}
                </div>
                <div class="form-group col-6">
                    {{ Form::label('state', 'State') }}
                    {{ Form::text('state', null, array('class' => 'form-control')) }}
                </div>
            </div>

            @can('Ticketing Permission')
            <div class="row">
                <div class="col-12">
                    <h5><b>Ticketing Response Group</b></h5>
                </div>
                <div class="form-group col-12">
                    <label class="radio-inline">
                        <input type="radio" class="mr-2" name="response_group_id" @if($user->response_group_id==2) checked @endif value="2">Client
                    </label>
                    <label class="radio-inline pl-3">
                        <input type="radio" class="mr-2" name="response_group_id" @if($user->response_group_id==1) checked @endif value="1"> Customer
                    </label>
                    <label class="radio-inline  pl-3">
                        <input type="radio" class="mr-2" name="response_group_id" @if($user->response_group_id==3) checked @endif value="3">Developer
                    </label>
                    <label class="radio-inline  pl-3">
                        <input type="radio" class="mr-2" name="response_group_id" @if($user->response_group_id==4) checked @endif value="4">Developer Manager
                    </label>
                      <span id="response_group_id_error" class=" user_error validation-error "></span>

                </div>
            </div>
             @endcan

            <div class="row">
                <div class='form-group  col-6'>
                    <label for="country">Country</label>
                    <select tabindex="5" class="form-control chzn-select assign_class" id="country"   name="country" >
                        <option value="" selected>Select Country</option>
                        <option value="GB" @if($user->country) {{ "GB" == $user->country ? 'selected="selected"' : '' }} @endif >UK</option>
                        <option value="FR" @if($user->country) {{ "FR" == $user->country ? 'selected="selected"' : '' }} @endif >France</option>
                    </select>
                </div>
            </div>
                        <input type="hidden" name="roles" id="roles" value="3">


<!-- 
            @can('Assign Role To User')
                <h5><b>Give Role</b></h5>
                 <div class='form-group'>
                                <select name="roles" id="roles" class="form-control">
                                    <option value="">Select Role</option>
                @foreach ($roles as $index=>$role)
                @if($role->name=='User')
                 <option value="{{$role->id}}" <?php if($user->roles[0]->id==$role->id) echo 'selected'; ?>>{{ ucfirst($role->name) }}</option>
                 @endif

                @endforeach
                     </select>
                     <span id="roles_error" class=" user_error validation-error "></span>
                     
                            </div>
            @endcan -->
    
           


            <div class="row">
                <div class="form-group  col-6">
                    {{ Form::label('password', 'Password') }}<br>
                    {{ Form::password('password', array('class' => 'form-control password' , 'autocomplete'=> 'off' )) }}
                    <span id="password_error" class=" user_error validation-error "></span>
                </div>
                


                <div class="form-group  col-6">
                    {{ Form::label('password', 'Confirm Password') }}<br>
                    {{ Form::password('password_confirmation', array('class' => 'form-control password', 'autocomplete'=> 'off')) }}

                </div>
            </div>


        </div>
    </div>
</div>
<div class="modal-footer" style="display: flex;">
    <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
    <input type="submit" class="btn btn-primary" name="submit" value="Update User">
    <!-- {{ Form::submit('Update User', array('class' => 'btn btn-primary  ')) }} -->
</div>
<!-- {{ Form::close() }} -->
</form>


<script type="text/javascript">

     $(".hide_search").chosen({disable_search_threshold: 10});

    $(".chzn-select-edit").chosen({allow_single_deselect: true});

         $(".chzn-select-edit-deselect,#select2_sample").chosen();

        $(".chzn-select-edit").chosen({
            allow_single_deselect: true,
            search_contains: true, // kwd can be anywhere
            max_selected_options: 1,
            width: "95%",
            no_results_text: "Oops, nothing found!"
        });

        function Chosen() {
            return Chosen.__super__.constructor.apply(this, arguments);
    }


        //make toggle objects against roles
        var role_access = "{{auth()->user()->can('Edit User')}}";

        if (role_access.length > 0) {
            var roles = "{{ count($roles) }}";
            console.log(roles);
            for (var i = 0; i < roles; i++) {
                new Switchery(document.querySelector('.sm_toggle_update' + i), {
                    size: 'small',
                    color: '#00c0ef',
                    jackColor: '#fff'
                });

            }
        }

</script>
