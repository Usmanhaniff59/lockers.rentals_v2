<div class="modal fade in display_none" id="user-create-modal" tabindex="-1" role="dialog" aria-hidden="false">

    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div id="create_form_display">
            </div>
          
            <br>
        </div>
    </div>
</div>