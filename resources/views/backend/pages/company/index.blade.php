@extends('backend.layouts.app')

@section('title', 'Lockers | Companies')
@section('style')
    <style>
        .validation-error {
            color: red
        }

        .loader {
            position: absolute;
            top: 40%;
            left: 40%;
            margin: auto;
            display: none;
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #00c0ef;
            width: 150px;
            height: 150px;
            -webkit-animation: spin 2s linear infinite;
            /* Safari */
            animation: spin 2s linear infinite;

        }

        .display-on-load,
        .no-display-on-load {
            display: none;
        }

        .chat_error,
        .document_error,
        .general_error {
            font-weight: 500;
            color: red;
            margin-top: -10px;
        }

        .header_error {
            color: white !important;
            background: red !important;
            border-color: #dee2e6 #dee2e6 #e9e9e9 !important;
        }

        .header_error:hover {
            color: white !important;
        }

        #move_booking {
            position: absolute;
            bottom: -65%;

        }

    </style>
    <!--plugin styles-->
    <link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-switch/css/bootstrap-switch.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/switchery/css/switchery.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/radio_css/css/radiobox.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/checkbox_css/css/checkbox.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/modal/css/component.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-tagsinput/css/bootstrap-tagsinput.css"/>
    <link rel="stylesheet" type="text/css" href="/backend/vendors/animate/css/animate.min.css"/>


    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/radio_checkbox.css"/>


    <link type="text/css" rel="stylesheet" href="/backend/vendors/chosen/css/chosen.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/form_elements.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/datepicker-jquery.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/blocks/blocks.css"/>








    {{--<link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css" />--}}

    {{--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">--}}
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/datepicker-jquery.css"/>
    <style>
        /*.school_header h3::after {*/
        /*content: '';*/
        /*position: absolute;*/
        /*display: block;*/
        /*width: 40px;*/
        /*height: 3px;*/
        /*background: #F5B54A;*/
        /*bottom: 0;*/
        /*left: calc(50% - 60px);*/
        /*}*/
        /*body {*/
        /*font-family: Ubuntu, Helvetica, Arial, sans-serif;*/
        /*}*/
    </style>
    <style>
        /*.red{*/
        /*background-color: red;*/
        /*}*/
        /*.green{*/
        /*background-color: greenyellow;*/
        /*}*/
        /*.orrange{*/
        /*background-color: orange;*/
        /*}*/
    </style>

@endsection
@section('content')
    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-building circle header_icon_margin"></i>
                            School Management
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        @if(Session::has('flash_message'))
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable float-right"
                     style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"
                            style="margin-top: 4px; margin-left: 15px;"> ×
                    </button>
                    {{ Session::get('flash_message') }}
                </div>
            </div>
        @endif
        @if (session('status'))
            <div class="alert alert-danger">
                {{ session('status') }}
            </div>
        @endif

        @include('errors.success')
        @include('errors.error')
        <div class="outer">
            <div class="inner bg-container">
                <div class="row">
                    @include ('errors.list')
                    <div class="col-12 data_tables">
                        <div class="card m-t-35">
                            <div class="card-header bg-white">
                                <i class="fa fa-building circle header_icon_margin"></i> All Schools
                                @can('Create Company')
                                    <a href="#create"
                                       class="btn btn-primary btn-md adv_cust_mod_btn pull-right header_icon_margin_left"
                                       data-toggle="modal" data-href="#create">Add New School</a>
                                @endcan
                                <div class="radio_basic_swithes_padbott pull-right header_icon_margin_left"
                                     style="margin-top: 7px;padding-bottom:0px !important;">
                                    <input type="checkbox" class="js-switch sm_toggle_show_only_deleted_company"
                                           name="roles" value="1" id="get_deleted_companies"/>
                                    <span class="radio_switchery_padding">Recycle bin</span>
                                </div>

                            </div>
                            <div class="card-body p-t-10">
                                <div class="row p-t-15">
                                    <div class="col-md-3">
                                        @can('All Records')
                                            <label>Status Type</label>
                                            <select class="form-control chzn-select" id="search_status_type"
                                                    placeholder="Status Type" tabindex="2">
                                                <option value="">Select Status Type</option>
                                                <option value="all">All</option>
                                                <option value="company">Customer</option>
                                                <option value="leads">LEADS</option>
                                                <option value="prospective">Prospective</option>
                                                <option value="unmatched">NOT ASSIGNED</option>
                                            </select>


                                        @endcan
                                    </div>

                                    <div class="col-md-3">
                                        @can('All Records')

                                            <label>Franchise Group</label>
                                            <select class="form-control chzn-select" id="search_franchise"
                                                    placeholder="Franchise" tabindex="2">
                                                <option value="">Select Franchise Group</option>
                                                <option value="all">All</option>
                                                @foreach($franchises as $franchise)
                                                    <option value="{{$franchise->id}}">{{$franchise->name}}</option>
                                                @endforeach
                                            </select>

                                        @endcan
                                    </div>
                                  <!--   <div class="col-md-3">
                                      

                                            <label>Un Active Years</label>
                                            <select class="form-control chzn-select" id=""
                                                     tabindex="2">
                                              
                                               
                                                    <option value="{{$unActive_school_years->id}}">{{date('M Y',strtotime($unActive_school_years->from_date))}},{{date('M Y',strtotime($unActive_school_years->to_date))}}</option>
                                               
                                            </select>

                                    </div> -->

                                </div>
                                <div class="row" style="width: 100% !important;">
                                    <div class="col-md-12 m-t-25">
                                        <div class="table-responsive">
                                            <table class="table  table-bordered table-hover  " style="width:100%"
                                                   id="sample_6">
                                                <thead>
                                                <tr>
                                                    {{--<th>#</th>--}}
                                                    <th>Name</th>
                                                    <th>City</th>
                                                    <th>Country</th>
                                                    <th>Post Code</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--- create model -->
    @include('backend.pages.company.create')
    <!-- END create modal-->

    <!--- Edit model -->
    @include('backend.pages.company.edit')
    <!-- END Edit modal-->

    @include('backend.pages.company.blocks.edit')
    @include('backend.pages.company.blocks.create')
    @include('backend.pages.company.chats.create')
    @include('backend.pages.company.chats.view')
    @include('backend.pages.company.documents.create')
    @include('backend.pages.company.documents.edit')
    @include('backend.pages.company.vouchers.create')
    @include('backend.pages.company.pupilPremium.create')
    @include('backend.pages.company.pupilPremium.edit')
    @include('backend.pages.company.vouchers.assignCode')
    @include('backend.pages.company.users.edit')
    @include('backend.pages.company.users.create')
@endsection
@section('script')
    <!--plugin scripts-->
    <script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>
    <!-- end of plugin scripts -->

    <!--Page level scripts-->
    <script type="text/javascript" src="/backend/vendors/chosen/js/chosen.jquery.js"></script>
    <script type="text/javascript" src="/backend/js/pages/form_elements.js"></script>
    <script type="text/javascript" src="/backend/js/pages/datatable_server_side.js"></script>
    <script type="text/javascript" src="/backend/js/pages/radio_checkbox.js"></script>
    <script type="text/javascript" src="{{asset('/backend/js/common.js')}}"></script>
     <script type="text/javascript" src="{{asset('/backend/js/jquery-ui.js')}}"></script>
    <!-- <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk="   crossorigin="anonymous"></script> -->
    <script src="{{ asset('custom/general.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script type="text/javascript">
         $(document).ready(function() {

            $("#search_start_date").datepicker();
        });

        var dragcheck;
        //company validation of location group
        $("body").on('submit', '#save_company_data', function () {
            var location_group_id = $("#location_group_id").val();


            var company_name = $("#company_name").val();
            console.log(location_group_id);
            console.log(company_name);
            if (location_group_id == "") {

                $(".location_group_id").html('Location Group is required');
                return false;
            } else if (company_name == '') {
                $(".location_group_id").html('');
                console.log("inhere");
                $(".company_name").html('Name is required');
                return false;
            }
            else {
                $(".company_name").html('');
            }
            return true;
        });

        //blocks tab click
        $('body').on('click', '#blocks_tab', function () {
            blocksTable();
            $('.edit_footer').addClass('d-none');
        });

        //documents tab click
        $('body').on('click', '#documents_tab', function () {
            documentsTable();
            $('.edit_footer').addClass('d-none');
        });
        //lockers tab click
        $('body').on('click', '#lockers_tab', function () {

            $('.edit_footer').addClass('d-none');


        });

        //remove footer of save button when click on this class
        $('body').on('click', '.tabs', function () {
            $('.edit_footer').removeClass('d-none');
        });
        //********************************Start show & hide modal when click on create & edit button**********
        //show edit modal
        $('#edit').on('show.bs.modal', function () {
            $('.nav-link').removeClass('header_error');

        })
        //show edit modal
        $('#edit').on('hidden.bs.modal', function () {

            $('.general_error').remove();
        })

        $('#create_block').on('show.bs.modal', function () {

            $('#edit').modal('toggle');
        })

        $('#create_chat').on('show.bs.modal', function () {
            $('.chat_error').hide();
            // $("#chatsForm")[0].reset();
            $('#edit').modal('toggle');
        })

        $('#create_chat').on('hidden.bs.modal', function () {

            $('#edit').modal('toggle');
        })


        $('#view_chat').on('hidden.bs.modal', function () {

            $('#edit').modal('toggle');
        })

        $('#view_chat').on('show.bs.modal', function () {
            $('#edit').modal('toggle');
        })

        $('#create_vouchers').on('show.bs.modal', function () {
            // $('.voucher_error').hide();
            $('#edit').modal('toggle');
        })
        $('#create_vouchers').on('hidden.bs.modal', function () {

            $('#edit').modal('toggle');
        })
        $('#create_pupil_premium').on('show.bs.modal', function () {
            // $('.voucher_error').hide();
            $('#edit').modal('toggle');
        })
        $('#create_pupil_premium').on('hidden.bs.modal', function () {

            $('#edit').modal('toggle');
        })

        $('#move_booking').on('show.bs.modal', function () {
            // $('.voucher_error').hide();
            $('#edit').modal('toggle');
        });

        $('#move_booking').on('hidden.bs.modal', function () {
            $('#edit').modal('toggle');
        });

        $('#assign_voucher_code').on('show.bs.modal', function () {
            // $('.voucher_error').hide();
            $('#edit').modal('toggle');
        })
        $('#assign_voucher_code').on('hidden.bs.modal', function () {

            $('#edit').modal('toggle');
        })

        //hides company edit modal when the document modal is opened
        $('#create_document').on('show.bs.modal', function () {
            $('.document_error').hide();
            $("#documentsForm")[0].reset();
            $("h1").text("Click or drop file here");
            $('#edit').modal('toggle');
        })

        //shows company edit modal when the document modal is closed
        $('#create_document').on('hidden.bs.modal', function () {
            $("#documentsForm")[0].reset();
            $("h1").text("Click or drop file here");
            $('#edit').modal('toggle');
        })

        //hides company edit modal when the document edit  modal is opened
        $('#edit_document').on('show.bs.modal', function () {
            $('#edit').modal('toggle');
        })

        //shows company edit modal when the document edit modal is closed
        $('#edit_document').on('hidden.bs.modal', function () {

            $('#edit').modal('toggle');

        })

        $('#create_block').on('hidden.bs.modal', function () {
            $('#edit').modal('toggle');

        })

        $('#edit_block').on('show.bs.modal', function () {

            $('#edit').modal('toggle');
        })

        $('#lockerModal').on('show.bs.modal', function () {

            $('#edit').modal('toggle');
        })

        $('#lockerModal').on('hide.bs.modal', function () {

            $('#edit').modal('toggle');
        })

        $('#edit_block').on('hidden.bs.modal', function () {
            $('#edit').modal('toggle');
        })

        $('#edit-company-user').on('show.bs.modal', function () {

            $('#edit').modal('toggle');
        })
        $('#edit-company-user').on('hidden.bs.modal', function () {
            $('#edit').modal('toggle');
        })
        $('#user-create-modal').on('show.bs.modal', function () {

            $('#edit').modal('toggle');
        })
        $('#user-create-modal').on('hidden.bs.modal', function () {
            $('#edit').modal('toggle');
        })

        $('#edit').on('hidden.bs.modal', function () {
            $('.loader').css('display', 'none');
            $("#lockers_div").html('');
            console.log("hidden");
        })

        //********************************End show & hide modal when click on create & edit button**********

        //****** start radio clicks show & hide function on locers tab ************
        $('#blank_radio').on('click', function () {

            $('.no-display-on-load').show();

        })

        $('#onoffsale').on('click', function () {

            $('.no-display-on-load').show();

        })
        $('#move_out_side_block_radio').on('click', function () {

            $('.no-display-on-load').show();

        })
        $('#move_radio').on('click', function () {

            $('.no-display-on-load').show();

        })
        $('#delete_radio').on('click', function () {

            $('.no-display-on-load').show();

        })
        $('#maintenance_radio').on('click', function () {

            $('.no-display-on-load').show();

        })
        //****** End radio clicks show & hide function on locers tab ************

        //********* tabls click actions on edit companies ***********
        $('body').on('click', '#chats_tab', function () {

            chatsTable();
            $('.edit_footer').addClass('d-none');
        });

        $('body').on('click', '#vouchers_tab', function () {

            vouchersTable();
            $('.edit_footer').addClass('d-none');
        });
        $('body').on('click', '#pupil_premium_tab', function () {

            pupilPremiumTable();
            $('.edit_footer').addClass('d-none');
        });
        $('body').on('click', '#user_assign', function () {

             assignTableUSers();
            $('.edit_footer').addClass('d-none');
        });


    </script>

    <script type="text/javascript">
        $("#close_edit_one").click(function () {
            $('#edit').modal('toggle');
        });

        function blocksTable() {
            $('#blocksTable').dataTable().fnClearTable();
            $('#blocksTable').dataTable().fnDestroy();

            // $('#blocksTable').empty();
            return $('#blocksTable').DataTable({
                "order": [[0, 'desc'], [1, 'asc']],
                "order": [],
                // "lengthMenu":[[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                language: {
                    'lengthMenu': '_MENU_',
                    "search": '',
                    "searchPlaceholder": "Search..."
                },

                // "oLanguage": {
                //   "sLengthMenu": "_MENU_",
                //    "sSearch": "_INPUT_"
                // },
                "autoWidth": false,
                "scrollY": "800px",
                "columnDefs": [
                    {"width": "100px", "targets": 0},
                    {"width": "200px", "targets": 1},
                    {"width": "100px", "targets": 2},
                    {"width": "100px", "targets": 3},
                    {"width": "100px", "targets": 4},
                ],
                // initComplete : function() {
                //     $("#blocksTable_length").css('margin-left', '-25px');
                // },
                // "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                ajax: '<?php echo route('company.blocks.show.all'); ?>',
                columns: [
                    {data: 'lock_type', name: 'lock_type'},
                    {data: 'name', name: 'name'},
                    {data: 'block_height', name: 'block_height'},
                    {data: 'locker_blocks', name: 'locker_blocks'},
                    {data: 'action', name: 'action', orderable: false, searchable: true}
                ],

            });

        }

        function refreshTable() {
            $('#blocksTable').each(function () {
                dt = $(this).dataTable();
                dt.fnDraw();
            });
        }

        $("body").on('change', '#search_status_type', function () {
            // searchByThis(companyEntryTable);
            companyEntryTable();

        });
        $("body").on('change', '#search_franchise', function () {
            // searchByThis(companyEntryTable);
            companyEntryTable();


        });

        function refreshTableAfterChat() {
            $('#chatsTable').each(function () {
                dt = $(this).dataTable();
                dt.fnDraw();
            });
        }

        //companies table of datables
        var company_table = companyEntryTable();

        function companyEntryTable() {
            $('#sample_6').dataTable().fnClearTable();
            $('#sample_6').dataTable().fnDestroy();

            $("#sample_6_filter>label").css('float', 'right');
            $("#sample_6_length>label").css('float', 'left');

            return $('#sample_6').DataTable({
                responsive: true,
                serverSide: true,
                processing: true,

                "order": [],
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                stateSave: true,
                "oLanguage": {
                    "sLengthMenu": "_MENU_",
                    "sSearch": "_INPUT_"
                },
                "aTargets": ["_all"],
                initComplete: function () {
                    $("#sample_6_filter>label").css('float', 'right');
                    $("#sample_6_length>label").css('float', 'left');
                },
                ajax: '<?php echo route('company.show.all'); ?>',
                ajax: {
                    url: '<?php echo e(route('company.show.all')); ?>',
                    data: function (d) {
                        d.search_status_type = $('#search_status_type').val();
                        d.search_franchise = $('#search_franchise').val();

                    }
                },
                columns: [
                    // {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'city', name: 'city'},
                    {data: 'country', name: 'country'},
                    {data: 'post_code', name: 'post_code'},
                    {data: 'action', name: 'action', orderable: false, searchable: true}
                ],
            });
        }

        $(document).ready(function () {
            $("#sample_6_filter>label").css('float', 'right');
            $("#sample_6_length>label").css('float', 'left');

            $('#stop_auto_rebook_edit').prop('checked', true);

            //stop auto_rebook edit radios
            $("body").on('click', '.stop_auto_rebook_edit', function () {
                if ($('#stop_auto_rebook_edit').val() == '0') {
                    if ($('#stop_auto_rebook_edit').val() == '1') {
                        $("#stop_auto_rebook_edit").val('0');
                        $('#stop_auto_rebook_edit').attr('checked', false);
                    } else {
                        $("#stop_auto_rebook_edit").val('1');
                        $('#stop_auto_rebook_edit').attr('checked', true);
                    }
                } else {
                    if ($('#stop_auto_rebook_edit').val() == '1') {
                        $("#stop_auto_rebook_edit").val('0');
                        $('#stop_auto_rebook_edit').attr('checked', false);
                    } else {
                        $("#stop_auto_rebook_edit").val('1');
                        $('#stop_auto_rebook_edit').attr('checked', true);
                    }
                }

            });
        });

        //view chat modal
        $('body').on('click', '#view_chat_btn', function () {
            var id = $(this).data('id');
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                url: '/admin/company/chats/view',
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {id: id, _token: CSRF_TOKEN},
                success: function (data) {
                    console.log(data);
                    $('#view_chat_title_in_model').text(data.title);
                    $('#view_chat_in_modal').text(data.details);

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                }
            });

        });

        //click on edit button oh companies
        $('body').on('click', '.edit-btn', function (e) {
            // alert('123');

            $('.no-display-on-load').hide();
            $('.display-on-load').show();
            var company_id = $(this).data('id');
            $("#edit_company")[0].reset();
            $('#company_id').val(company_id);
            $('#comany_name_header').html('<i class="fa fa-pencil circle"></i> Edit ' + $(this).data('name'));
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');


            $.ajax({
                url: '/admin/getcompany',
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {company_id: company_id, _token: CSRF_TOKEN},
                success: function (data) {
                    //location group in edit modal
                    // console.log(data);
                    var location_groups = '';
                    $.each(data.locationGroups, function (index, location) {
                        if (data.company.location_group_id == location.id) {
                            location_groups += '<div class="radio">';
                            location_groups += '<label>';
                            location_groups += '<input type="radio" name="location_group_id" value="' + location.id + '" checked>';
                            location_groups += '<span class="cr"><i class="cr-icon fa fa-circle"></i></span>' + location.name + '</label>';
                            location_groups += '</div>';
                        } else {
                            location_groups += '<div class="radio">';
                            location_groups += '<label>';
                            location_groups += '<input type="radio" name="location_group_id" value="' + location.id + '">';
                            location_groups += '<span class="cr"><i class="cr-icon fa fa-circle"></i></span>' + location.name + '</label>';
                            location_groups += '</div>';
                        }
                    });
                    // alert(data.franchisee)
                    $('#location_groups').html(location_groups);
                    // $('#franchisee_id_edit').val(data.franchisee);
                     $("#franchisee_id_edit option[value='"+data.franchisee+"']").attr('selected','selected'); 
                     $('.chosen-single span').html(data.franchisee_name);

                    $('#status_type').val(data.company.status_type);
                    $('#open_date').val(data.company.open_date);

                    //send_voucher_code in Vouchers tabe
                    var checkbox = document.querySelector('.sm_toggle_company_voucher_assign');
                    $('.switchery-small').remove();
                    var switchary = new Switchery(checkbox, {size: 'small', color: '#00c0ef', jackColor: '#fff'});
                    if (data.company.send_voucher_code == 1) {
                        $('#send_voucher_code').trigger('click');
                    }

                    //general info in edit modal
                    if (data.company.currency == 'GBP') {
                        $('#gbp_edit').prop('checked', true);
                    } else {
                        $('#eur_edit').prop('checked', true);
                    }

                    $(".edit_company_info").each(function () {
                        var valueid = $(this).attr('id');
                        var temp_col = valueid.split('_info');
                        var col_name = temp_col[0];
                        var vaall = data['company'][col_name];
                        $('#' + valueid).val(vaall);
                    });
                   
                    $('#location_group_idd').val(data.company.location_group_id);
                    $('#comapny_id').val(data.company.id);
                    $('#name_edit').val(data.company.name);
                    $('#note_edit').text(data.company.note);
                    $('#territory_code_info').val(data.company.territory_code);


                    if (data.company.stop_auto_rebook == 1) {
                        $('#stop_auto_rebook_edit').prop('checked', true);
                    } else {
                        $('#stop_auto_rebook_edit').prop('checked', false);
                    }
                    // $('#stop_auto_rebook_edit').val(data.company.stop_auto_rebook);
                    $('#name_number_edit').val(data.company.name_number);
                    $('#street_edit').val(data.company.street);
                    $('#city_edit').val(data.company.city);
                    $('#country_edit').val(data.company.country);
                    $('#post_code_edit').val(data.company.post_code);
                    $('#blocksul').html(data.blocks);
                    $('#move_locker_outside_block').html(data.blocks);


                    if (data.company.active == 1) {

                        $('#active_edit').trigger('click');

                    } else {
                        $('#active_edit').trigger('click');
                        $('#active_edit').trigger('click');
                    }
                    if (data.company.display == 1) {
                        $('#display_edit').trigger('click');

                    } else {
                        $('#display_edit').trigger('click');
                        $('#display_edit').trigger('click');
                    }
                    // alert(data.golive);
                    if(data.golive!='' && data.golive!=null && data.golive!=undefined)
                    {
                        if(data.golive==1)
                        {
                            $('#live_edit').prop('checked', true);
                        }else{
                            $('#live_edit').prop('checked', false);
                        }
                    }else{
                         $('#live_edit').prop('checked', false);
                    }
                    if (data.company.keep_block == 1) {
                        $('#keep_block_edit').prop("checked", true);

                    } else if (data.company.keep_block == 2) {
                        $('#keep_block_edit_rebook').prop("checked", true);

                    }
                    else if (data.company.keep_block == 3) {
                        $('#keep_block_edit_no_rebook').prop("checked", true);

                    }

                    if (data.company_school_year) {

                        var ar = data.company_school_year
                        console.log(ar);
                        console.log("array");
                        for (var i = 0; i < ar.length; i++) {
                            $('#edit_school_company_' + ar[i].school_academic_year_id).prop('checked', true);
                        }
                    }

                    if (data.school_academic_year) {
                        var ar = data.school_academic_year
                        console.log(ar);
                        console.log("array_school years");
                        $('#choose_school_year_edit').html('');
                        $('#choose_school_year').html('');
                        $('#choose_school_year_edit').html('<option value="null">Choose Option</option>');
                        $('#choose_school_year').html('<option value="null">Choose Option</option>');
                        $.each(data.school_academic_year, function (i, item) {
                            $('#choose_school_year_edit').append($('<option>', {
                                value: item[0].id,
                                text: item[0].school_year
                            }));

                            $('#choose_school_year').append($('<option>', {
                                value: item[0].id,
                                text: item[0].school_year
                            }));
                        });
                        $("#choose_school_year").trigger("chosen:updated");
                        $("#choose_school_year_edit").trigger("chosen:updated");


                        // for(var i = 0; i < ar.length; i++){
                        //     console.log('a');
                        //    $('#choose_school_year_edit').append($('<option>', {
                        //         value: ar[i].id,
                        //         text: ar[i].school_year,
                        //     }));
                        //     $('#choose_school_year').append($('<option>', {
                        //         value: ar[i].id,
                        //         text: ar[i].school_year,
                        //     }));
                        // }
                    }

                    blocksTable();
                    chatsTable();
                    documentsTable();
                    vouchersTable();


                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                }
            });
            $('body').on('click', '#save_company', function (event) {

                event.preventDefault();

                if (!$("input[name='location_group_id']:checked").val()) {
                    if (!$('#location_groups .general_error').html()) {
                        $('#location_groups').append('<span class="general_error">Location group field is required</span>');
                    }
                    $('#location_group_tab').addClass('header_error');
                }
                else {
                    $('#location_groups .general_error').remove();
                    $('#location_group_tab').removeClass('header_error');
                }

                if (!$("#name_edit").val()) {
                    if (!$('.name-container .general_error').html()) {
                        $('.name-container').append('<span class="general_error">Name field is required</span>');
                    }
                    $('#general_info_tab').addClass('header_error');
                }
                else {
                    $('.name-container .general_error').remove();
                    $('#general_info_tab').removeClass('header_error');
                }
                if ($("input[name='location_group_id']:checked").val() && $("#name_edit").val()) {
                    $('#edit_company').submit();
                }


            });


        });


    </script>

    <script type="text/javascript">
        function spinner_start() {
            $(".loader").show();
            jQuery('#documentEditForm').css('opacity', '0.05');
            jQuery('#documentsForm').css('opacity', '0.05');
        }

        function spinner_close() {

            // $(".excel-loader").hide();
            $(".loader").hide();
            jQuery('#documentEditForm').css('opacity', '1');
            jQuery('#documentsForm').css('opacity', '1');
        }

        //document reset form
        $current_documentid = 0;
        $('.cancel').click(function () {
            $("#documentsForm")[0].reset();
            $("h1").text("Click or drop file here");
        })

        //save documents
        function saveDocumentId(id) {
            $current_documentid = id;
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: '/admin/company/documents/show',
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {id: $current_documentid, _token: CSRF_TOKEN},
                success: function (data) {
                    $('#edit_type').val(data.type_id);
                    $('#edit_title').val(data.title);
                    $("#placeholder").text(data.original_path);
                    console.log(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                }
            });
        }

        $("#documentEditForm").submit(function (event) {
            spinner_start();
            event.preventDefault();
            $('button#edit_document_btn').prop('disabled', true);
            type = $('#edit_type').val();
            title = $('#edit_title').val();
            formdata = new FormData(this);
            formdata.append('id', $current_documentid);

            if (dragcheck == 2) {
                console.log(file[0]);
                formdata.append('edit_file', file[0]);
                dragcheck = 0;
            }

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: 'company/documents/update',
                data: formdata,
                contentType: false,
                cache: false,
                processData: false,
                success: function () {
                    spinner_close();
                    $('#edit_document').modal('hide');
                    $('#edit').modal('show');
                    $('button#edit_document_btn').prop('disabled', false);
                    documentsTable();

                },
                error: function (error) {
                    spinner_close()
                    $('button#edit_document_btn').prop('disabled', false);

                }
            });
        });

        //get blocks details
        function saveid(id) {
            // alert('123');
           // spinner_start();
            console.log(id, "new");
            $('#image_validation_edit').text('');
            $.ajax({

                url: '/getBlockDetails/' + id,

                type: 'GET',

                dataType: 'JSON',

                cache: false,

                success: function (data) {
                    // alert(data.data.block_group);
                       $('#start_block').val('');
                        $('#end_block').val('');
                            if($('#display_edit_update').is(":checked")){
                            $('#date_blocks').css('display','');

                            }else{
                                $('#date_blocks').css('display','none');
                                
                            }
                          
                    if(data.data.start_date && data.data.start_date!='0000-00-00') 
                    {
                         var d = new Date(data.data.start_date);
                        var month = d.getMonth();
                        var day = d.getDate();
                        var year=d.getFullYear();
                        var output =  (day<10 ? '0' : '') + day+ '-'+(month<10 ? '0' : '') + month + '-' + d.getFullYear()
                            ;
                            var date = data.data.start_date;
                            date = moment(date, "YYYY-MM-DD HH:mm:ss").toDate();
                            $('#start_block').datepicker("setDate", date );
                            // $('#start_block').datepicker("setDate", new Date(year,month,day) );
                        // $('#start_block').val(output);
                        // $('#start_block').val(output);
                        
                    }else{

                        var d = new Date();
                        var month = d.getMonth();
                        var day = d.getDate();
                        var year=d.getFullYear();

                       var output =  (day<10 ? '0' : '') + day+ '-'+(month<10 ? '0' : '') + month + '-' + d.getFullYear()
                            ;
                              $('#start_block').datepicker("setDate", new Date(year,month,day) );
                        // $('#start_block').val(output);

                    }
                    if(data.data.end_date)
                    {
                          var d = new Date(data.data.end_date);
                        var month = d.getMonth();
                        var day = d.getDate();
                        var year=d.getFullYear();

                       var output =  (day<10 ? '0' : '') + day+ '-'+(month<10 ? '0' : '') + month + '-' + d.getFullYear()
                            ;
                        // $('#end_block').val(output);
                        var date = data.data.end_date;
                            date = moment(date, "YYYY-MM-DD HH:mm:ss").toDate();
                          $('#end_block').datepicker("setDate",date );
                          // $('#end_block').datepicker("setDate", new Date(year,month,day) );
                    }
                    $('#namee_edit').val(data.data.name);
                    $('#block_group_edit option:selected').removeAttr('selected');
                     $("#block_group_edit option[value='"+data.data.block_group+"']").attr('selected','selected');
                    $('#lock_type_edit').val(data.data.lock_type);
                    $('#order_edit').val(data.data.order_list);
                    // $('#block_height_edit').val(data.data.block_height);
                    fetchTierEdit(data.data.block_height);
                    $('#locker_blocks_edit').val(data.data.locker_blocks);
                    $('#block_images_edit').val(data.data.locker_block_image_id);
                    // $('#choose_school_year_edit').val(data.data.school_academic_year_id);
                    $('#block_id').val(data.data.id);
                    $('#rate_id').val(data.data.rate_id);
                    $('#color_locker_edit').val(data.data.color_locker);
                    $("#block_type_edit option[value='"+data.data.block_type+"']").attr('selected','selected');
                    // $('#block_type_edit').val(data.data.block_type);
                    var ar = data.years;
                    var x = []
                    for (var i = 0; ar.length > i; i++) {

                        // console.log(ar[i].school_academic_year_id)
                        x.push(ar[i].school_academic_year_id)
                    }
                    $("#choose_school_year_edit").val(x).trigger("chosen:updated");

                    if (data.data.display == 1) {
                        $('#display_edit_update').prop('checked', true);

                    } else {
                        $('#display_edit_update').prop('checked', false);
                    }
                    $('#edit_block').modal('toggle');
                    // spinner_close();
                },

                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    // spinner_close();
                }

            });
        }
            $('#block_type_edit').change(function(){
        // alert('123');
        fetchTierEdit(-1);
    
});

            $('#live_edit').click(function(){
                console.log('go live edit the table');
               if($(this).prop('checked') == true){
                        //do something
                       var val=1;
                    }else{
                        // alert('2');
                        var val=0;
                    }
                     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                     var company_id = $('#company_id').val();
                        $.ajax({
                            url: '/status/go_live',
                            type: 'POST',
                            dataType: 'JSON',
                            cache: false,
                            data: {
                                status: val,
                                _token: CSRF_TOKEN,
                                company_id:company_id,
                            },
                            success: function (data) {
                                if(data==2)
                                {
                                    $('#golive_error').html('No year availabe for activation');
                                }
                                // alert('123') ;
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                // $('button#create').prop('disabled', false);
                            }

                             });
            });
        function fetchTierEdit(tierVal){
        var val=$('#block_type_edit').val();
          
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.ajax({

            url: '/blocks/tiers',

            type: 'POST',

            dataType: 'JSON',

            cache: false,

            data: {
               
                block_type: val,
                _token: CSRF_TOKEN
            },

            success: function (data) {
                console.log(data);
                console.log('data tier');
                var html='';
                html+='<option value="">Select Tier</option>';

                for (var i = 0; i < data.length; i++){
                    if(tierVal==data[i].tier)
                    {
                        var select="selected";
                    }else{
                        var select="";

                    }
                    html += '<option '+select+' value="'+data[i].tier+'">'+data[i].tier+'</option> ';
                }

             $('#block_height_edit').html(html);
            },

            error: function (jqXHR, textStatus, errorThrown) {
                $('button#create').prop('disabled', false);
            }

        });


}
    </script>

    <script type="text/javascript">

        //show deleted companies
        function deletecompanyEntryTable() {
            $('#sample_6').dataTable().fnClearTable();
            $('#sample_6').dataTable().fnDestroy();

            $("#sample_6_filter>label").css('float', 'right');
            $("#sample_6_length>label").css('float', 'left');

            return $('#sample_6').DataTable({
                responsive: true,
                initComplete: function () {
                    $("#sample_6_filter>label").css('float', 'right');
                    $("#sample_6_length>label").css('float', 'left');
                },
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                "oLanguage": {
                    "sLengthMenu": "_MENU_",
                    "sSearch": "_INPUT_"
                },
                stateSave: true,
                "aTargets": ["_all"],
                ajax: '<?php echo route('company.show.deleted'); ?>',
                columns: [
                    // {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'city', name: 'city'},
                    {data: 'country', name: 'country'},
                    {data: 'action', name: 'action', orderable: false, searchable: true}
                ],
            });
        }

        //when click on recyle bin
        $('body').on('change', '#get_deleted_companies', function () {
            var checkbox_status = $("#get_deleted_companies").prop("checked");

            if (checkbox_status) {
                deletecompanyEntryTable();
            } else {
                companyEntryTable();
            }
        });

    </script>

    <script type="text/javascript">

        //deleted blocks
        function deleteblocksTable() {
            $('#blocksTable').dataTable().fnClearTable();
            $('#blocksTable').dataTable().fnDestroy();

            // $('#blocksTable').empty();
            return $('#blocksTable').DataTable({
                stateSave: true,
                "order": [[0, 'desc'], [1, 'asc']],
                // "lengthMenu":[[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                language: {
                    'lengthMenu': '_MENU_',
                    "search": '',
                    "searchPlaceholder": "Search..."
                },
                // "oLanguage": {
                //   "sLengthMenu": "_MENU_",
                //    "sSearch": "_INPUT_"
                // },
                "autoWidth": false,
                "scrollY": "800px",
                "columnDefs": [
                    {"width": "100px", "targets": 0},
                    {"width": "200px", "targets": 1},
                    {"width": "100px", "targets": 2},
                    {"width": "100px", "targets": 3},
                    {"width": "100px", "targets": 4},
                ],
                // initComplete : function() {
                //     $("#blocksTable_length").css('margin-left', '-25px');
                // },
                // "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                ajax: '<?php echo route('company.blocks.show.delete'); ?>',
                columns: [
                    {data: 'lock_type', name: 'lock_type'},
                    {data: 'name', name: 'name'},
                    {data: 'block_height', name: 'block_height'},
                    {data: 'locker_blocks', name: 'locker_blocks'},
                    {data: 'action', name: 'action', orderable: false, searchable: true}
                ],

            });

        }

        //get deleted blocks
        $('body').on('change', '#get_deleted_blocks', function () {
            // alert('123');
            var checkbox_status = $("#get_deleted_blocks").prop("checked");
            if (checkbox_status) {
                deleteblocksTable();
            } else {
                blocksTable();
            }
        });

        //deleet block
        function deleteBlock(id) {
            $.ajax({

                url: '/admin/company/blocks/destroy/' + id,

                type: 'get',

                dataType: 'JSON',

                cache: false,

                success: function (data) {
                    console.log(data);
                    blocksTable();
                    getLatest();

                },

                error: function (jqXHR, textStatus, errorThrown) {

                    console.log(jqXHR);

                }

            });
        }
 $(document).on('click','.delete-doc',function(){
    var id = $(this).attr('data-id');
    $('#edit').modal('toggle');
    // alert(id);
    swal({
  title: "Are you sure?",
  text: "Once you Delete, you will not be able to recover it again!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {
     $.ajax({

                url: '/admin/company/documents/destroy/' + id,

                type: 'get',

                dataType: 'JSON',

                cache: false,

                success: function (data) {
                    console.log(data);
                    $('#edit').modal('toggle');
                    documentsTable();
                    // getLatest();

                },

                error: function (jqXHR, textStatus, errorThrown) {

                    console.log(jqXHR);

                }

            });
  } else {
    $('#edit').modal('toggle');
    // swal("Your imaginary file is safe!");
  }
});

   });
//  $('#add-new-user-btn').click(function(){
// $()
//  });
     // $('body').on('click', '.user-edit-btn', function (e) {
        function userEdit(id) {
            // body...
            // alert('123');
             // $('#edit').modal('toggle');
             // $('#edit').hide();
              // $('#edit-company-user').modal('toggle');
              var company_id = $('#company_id').val();
            var user_id =id;
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                url: '/admin/companygetuserroles',
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {user_id: user_id, _token: CSRF_TOKEN,company_id:company_id},
                success: function (data) {
                    console.log(data);
                    console.log('this is the data to edit user');
                    // alert(data.modal);
                    var html=data.modal;
                    // var franchisee=data.franchisees;
                    $('.body').addClass('modal-open');
                    $('#editform_display').html(html);

                    // setTimeout(
                    //     function()
                    //     {
                    //         $('.password').val('');
                    //     }, 1000);


                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                }
            });

        // });
    }
    function addUserCompany(argument) {
            var company_id = $('#company_id').val();
            // var user_id =id;
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
     $.ajax({
                url: '/admin/companyFranchise',
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {_token: CSRF_TOKEN,company_id:company_id},
                success: function (data) {
                    $('#create_form_display').html(data);
                     $('.body').addClass('modal-open');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                }
            });

}

        $(document).on('submit','#update-user-info',function(){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var form = $(this);
            $.ajax({
                url: '<?php echo e(route('company.users.update')); ?>',
                type: 'PUT',
                dataType: 'JSON',
                cache: false,
                data: form.serialize(),
                success: function (data) {
                    // alert('123');
                    // $('#editform_display').html(data);
                    // setTimeout(
                    //     function()
                    //     {
                    //         $('.password').val('');
                    //     }, 1000);
                    if(data.error=='updated')
                    {
                         $('#edit-company-user').modal('toggle');
                        $('#edit').modal('toggle');
                         $(".print-error-msg").find("ul").html('');
                         $(".print-error-msg").css('display','none');
                     }else if($.isEmptyObject(data.error)){
                        $('#edit-company-user').modal('toggle');
                        $('#edit').modal('toggle');
                         $(".print-error-msg").find("ul").html('');
                         $(".print-error-msg").css('display','none');
                    }else{
                      $(".print-error-msg").find("ul").html('');
            $(".print-error-msg").css('display','block');
                        // console.log(data);
                        $.each( data.error, function( key, value ) {
                           
                            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
                            // $("#"+key+"_error").html(value);
                        });
                    }

                },   error: function (xhr, detail) {
                    // alert('123');
                    // if (btn_id !== '')
                    //     btn.html(btn_txt);

                    if (xhr.responseJSON.errors.email) {

                        $('#email_error').show()
                        $('#email').focus();
                        $('#email_error').html('<span>' + xhr.responseJSON.errors.email[0] + '</span>');
                    }
                    else {
                        $('#email_error').hide();
                    }
                    if (xhr.responseJSON.errors.name) {

                        $('#name_error').show()
                        $('#name').focus();
                        $('#name_error').html('<span>' + xhr.responseJSON.errors.name[0] + '</span>');
                    }
                    else {
                        $('#name_error').hide();
                    }
                     if (xhr.responseJSON.errors.password) {

                        $('#password_error').show()
                        $('#password').focus();
                        $('#password_error').html('<span>' + xhr.responseJSON.errors.password[0] + '</span>');
                    }
                    else {
                        $('#password_error').hide();
                    }

                    if (xhr.responseJSON.errors.response_group_id) {

                        $('#response_group_id_error').show()
                        $('#response_group_id_error').html('<span>' + xhr.responseJSON.errors.response_group_id[0] + '</span>');
                    }
                    else {
                        $('#response_group_id_error').hide();
                    }

                     if (xhr.responseJSON.errors.roles) {

                        $('#roles_error').show()
                        $('#roles_error').html('<span>' + xhr.responseJSON.errors.roles[0] + '</span>');
                    }
                    else {
                        $('#roles_error').hide();
                    }

                }
    });
        });

         //----------------------------Ajax Form Submission-------------------------//
        function saveData(form_id, btn_id, modal_id, table_id, callback) {
            spinner_start();
            // $('#'+ form_id).addClass('was-validated');
           $('#company_id_user').val($('#company_id').val())
            if (btn_id !== '') {
                var btn = $('#' + btn_id);
                var btn_txt = btn.html();
                btn.html("<i class='fa fa-spinner'></i> Processing...");
            }

            $.ajax({

                type: $('#' + form_id).attr('method'),
                url: $('#' + form_id).attr('action'),
                data: $('#' + form_id).serialize(),
                dataType: 'json',
                success: function (data) {
                    //console.log(data);
                    spinner_close();
                    $('#' + form_id).trigger("reset");
                    if (modal_id !== '') {
                        $('#' + modal_id).modal('toggle');
                        $('#edit').modal('toggle');
                        assignTableUSers();
                    }
                    if (table_id !== '') {

                        table_id.draw('page');
                    }
                    if (btn_id !== '') {
                        btn.html(btn_txt);
                    }

                },
                error: function (xhr, detail) {
                    // alert('123');
                    spinner_close();
                    if (btn_id !== '')
                        btn.html(btn_txt);

                    if (xhr.responseJSON.errors.email) {

                        $('#add_email_error').show()
                        $('#email').focus();
                        $('#add_email_error').html('<span>' + xhr.responseJSON.errors.email[0] + '</span>');
                    }
                    else {
                        $('#add_email_error').hide();
                    }
                    if (xhr.responseJSON.errors.name) {

                        $('#add_name_error').show()
                        $('#name').focus();
                        $('#add_name_error').html('<span>' + xhr.responseJSON.errors.name[0] + '</span>');
                    }
                    else {
                        $('#add_name_error').hide();
                    }
                     if (xhr.responseJSON.errors.password) {

                        $('#add_password_error').show()
                        $('#password').focus();
                        $('#add_password_error').html('<span>' + xhr.responseJSON.errors.password[0] + '</span>');
                    }
                    else {
                        $('#add_password_error').hide();
                    }

                    if (xhr.responseJSON.errors.response_group_id) {

                        $('#add_response_group_id_error').show()
                        $('#add_response_group_id_error').html('<span>' + xhr.responseJSON.errors.response_group_id[0] + '</span>');
                    }
                    else {
                        $('#add_response_group_id_error').hide();
                    }

                    if (xhr.responseJSON.errors.roles) {

                        $('#add_roles_error').show()
                        $('#add_roles_error').html('<span>' + xhr.responseJSON.errors.roles[0] + '</span>');
                    }
                    else {
                        $('#add_roles_error').hide();
                    }

                }
            })
        }
         var user_table = userEntryTable();
         function userEntryTable() {

            return $('#usersTable').DataTable({
                processing: true,
                serverSide: true,
                searching: true,
                stateSave: true,
                "oLanguage": {
                    "sLengthMenu": "_MENU_",
                    "sSearch": "_INPUT_"
                },
                "aTargets": ["_all"],

                "order": [],
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                columnDefs: [
                    {   "targets": [0],
                        "searchable": false
                    }
                ],
                 ajax: {
                    url: '<?php echo e(route('users.show.all')); ?>',
                    data: function (d) {
                        d.deleted = $("#get_deleted_users").prop("checked");

                    }
                },
                columns: [
                    {data: 'full_name', name: 'full_name'},
                    {data: 'email', name: 'email'},
                    {data: 'roles', name: 'roles'},
                    {data: 'action', name: 'action', orderable: false, searchable: true}
                ]
            });
        }


        $(document).on('click','.user-delete-btn',function(){
    var id = $(this).attr('data-id');
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $('#edit').modal('toggle');
    // alert(id);
    swal({
  title: "Are you sure?",
  text: "you want to remove the user from the company!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {
     $.ajax({

                url: '/admin/company/users/destroy',

                type: 'POST',

                dataType: 'JSON',

                cache: false,
               data: {id: id, _token: CSRF_TOKEN},

                success: function (data) {
                    console.log(data);

                    $('#edit').modal('toggle');
                    assignTableUSers();
                    // getLatest();

                },

                error: function (jqXHR, textStatus, errorThrown) {

                    console.log(jqXHR);

                }

            });
  } else {
    $('#edit').modal('toggle');
    // swal("Your imaginary file is safe!");
  }
});

   });


        // 
            $('body').on('click', '.create-new-franchisee-group-button', function (e) {

            $('.new-franchisee-group-input').removeClass('d-none');
            $('.existed-franchisee-group').addClass('d-none');

            $('.existed-franchisee-group-button-field').removeClass('d-none');
            $('.new-franchisee-group-button-field').addClass('d-none');

        });

        $('body').on('click', '.existed-franchisee-group-button , .add-new-user-btn', function (e) {

            $('.existed-franchisee-group').removeClass('d-none');
            $('.new-franchisee-group-input').addClass('d-none');

            $('.new-franchisee-group-button-field').removeClass('d-none');
            $('.existed-franchisee-group-button-field').addClass('d-none');
            $('.franchisee_name').val('');
            $('.password_create').val('');
            $('.validation-error').hide();


        });
        $('.input_create_block_empty').click(function(){
             $('#creat_block').trigger("reset");
                    $('.create_date_block').css('display','none');
                    // $("#choose_school_year").val(" ");
                    $("#choose_school_year").val(null).trigger("change");
            $('#create_block').modal('toggle');
        });
    </script>




@endsection