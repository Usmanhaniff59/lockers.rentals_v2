@extends('backend.layouts.app')

@section('title', 'Lockers | Companies')
@section('style')
<link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-switch/css/bootstrap-switch.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/switchery/css/switchery.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/radio_css/css/radiobox.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/checkbox_css/css/checkbox.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/modal/css/component.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-tagsinput/css/bootstrap-tagsinput.css" />
<link rel="stylesheet" type="text/css" href="/backend/vendors/animate/css/animate.min.css" />
<!-- end of plugin styles -->

<!--Page level styles-->
<link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/pages/radio_checkbox.css" />


<link type="text/css" rel="stylesheet" href="/backend/css/pages/datepicker-jquery.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/chosen/css/chosen.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/pages/form_elements.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/blocks/blocks.css" />


@endsection
@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
            <div class="row no-gutters">
                <div class="col-6">
                    <h4 class="m-t-5">
                        <i class="fa fa-building circle header_icon_margin"></i>
                        Company Management
                    </h4>
                </div>
            </div>
        </div>
    </header>
    @if(Session::has('flash_message'))
    <div class="col-md-12">
        <div class="alert alert-success alert-dismissable float-right"
            style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"
                style="margin-top: 4px; margin-left: 15px;"> ×
            </button>
            {{ Session::get('flash_message') }}
        </div>
    </div>
    @endif
    @if (session('status'))
    <div class="alert alert-danger">
        {{ session('status') }}
    </div>
    @endif
    @include('errors.success')
    @include('errors.error')
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                @include ('errors.list')
                <div class="col-12 data_tables">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">
                            <i class="fa fa-building circle header_icon_margin"></i> All Blocks

                            <div class="radio_basic_swithes_padbott pull-right header_icon_margin_left"
                                style="margin-top: 7px;padding-bottom:0px !important;">
                                <input type="checkbox" class="js-switch sm_toggle_show_only_deleted_outerblocks_main"
                                    name="roles" value="1" id="get_deleted_blocks" />
                                <span class="radio_switchery_padding">Recycle bin</span>
                            </div>
                        </div>
                        <div class="card-body p-t-10">

                            <div class=" m-t-25">
                                <table class="table  table-bordered table-hover" id="outer_blocks_table"
                                    style="width: 100% !important;">
                                    <thead>
                                        <tr>
                                            <th>Company</th>
                                            <th>Block</th>
                                            <th>Lockers Available</th>
                                            <th>Price</th>
                                            <th>Tier</th>
                                            <th>Locker Blocks</th>
                                            <th>Numbering</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>

                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--- Edit model -->
    <div class="modal fade in display_none" id="edit" style="overflow-y: scroll;" tabindex="-1" role="dialog"
        aria-hidden="false">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <input type="hidden" id="blocksul" />
                <form id="edit_company" action="{{ route('updatecompany') }}" method="POST">
                    @csrf
                    <div class="modal-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <h3 class="m-t-5" style="margin-left: 10px;margin-top: 20px;" id="comany_name_header"></h3>
                        <hr>
                        <div class="card m-t-35">
                            <div class="card-header bg-white edit-card-header">
                                <ul class="nav nav-tabs card-header-tabs float-left">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#lcokers" id="locker_tab"
                                            data-toggle="tab">Lockers</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#block-edit" id="block_tab"
                                            data-toggle="tab">Block</a>
                                    </li>

                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content text-justify">
                                    <div class="tab-pane active" id="lcokers">
                                        <div class="row">
                                            <div class="col-md-12" style="margin-top: 20px;">
                                                <div class="card">
                                                    <div class="card-body"
                                                        style="padding:0 1.25rem 0rem 1.25rem !important;">
                                                        <div class="row" style="margin-top: 20px !important;">
                                                            @include('backend.pages.company.partials.lockers_radio_operations')

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group m-t-40">
                                            <div class="radio_basic_swithes_padbott">

                                                <input type="checkbox"
                                                    class="js-switch sm_toggle_show_only_deleted_outerblock"
                                                    name="roles" value="1" id="deleted_checkbox" />
                                                <span class="radio_switchery_padding">Show only deleted</span>
                                            </div>
                                        </div>
                                        <br>

                                        <div class=" m-t-35">
                                            <div class="border_box">
                                                @include('backend.pages.company.partials.locker_types_color')
                                            </div>
                                        </div>

                                        <br>

                                        <div style="overflow-x:auto;">
                                            <div class="loader" id="block_loader"></div>
                                            <div id="lockers_div"></div>
                                        </div>

                                    </div>
                                    <div class="tab-pane" id="block-edit">
                                        <br>
                                        <h3><i class="fa fa-pencil circle"></i> Edit </h3>
                                        <hr>
                                        <div class="row">
                                            <div class="col-6">

                                                <form>
                                                    <div class="form-group">
                                                        <label name="name">Name</label>
                                                        <input type="text" name="name" id="namee_edit"
                                                            class="form-control">
                                                    </div>

                                                    <div class='form-group existed-franchisee-group'>
                                                        <label for="franchisee_id">Lock Type</label>
                                                        <select class="form-control" id="lock_type_edit">
                                                            <option value="" selected>Choose Lock Type</option>
                                                            <option value="m">Manual</option>
                                                            <option value="s">Static</option>
                                                            <option value="a">Automatic</option>
                                                        </select>
                                                    </div>
                                                     <div class='form-group existed-franchisee-group'>
                                                        <label for="block_type">BLock Type</label>
                                                        <select class="form-control" id="block_type" name="block_type">
                                                        <option value="I" selected="">Inside</option>
                                                        <option value="O">Outside</option>
                                                    </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label name="name">Locker Blocks</label>
                                                        <input type="number" min="1" name="name" class="form-control"
                                                            id="locker_blocks_edit">
                                                    </div>


                                                    <div class="form-group">

                                                        <label for="franchisee_id">Locker Block Images</label>
                                                        <select class="form-control " name="franchisee_id"
                                                            id="image_edit">
                                                            <option value="" selected>Choose Locker Block Images
                                                            </option>
                                                            @foreach ($locker_block_images as $locker_block_image)
                                                            <option value="{{$locker_block_image->id}}">
                                                                {{$locker_block_image->file_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                 



                                                    <div class='form-group'>
                                                        <label for="status">Rates</label>
                                                        <select class="form-control" name="rate_id" id="rate_id">
                                                            <option value="" selected>Choose Rate</option>

                                                            @foreach ($rates as $rate)
                                                            <option value="{{$rate->id}}">{{$rate->name}}</option>
                                                            @endforeach

                                                        </select>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" id="display_edit_update">
                                                            <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                                            Display
                                                        </label>
                                                    </div>

                                                </form>
                                            </div>
                                            <div class="col-6">

<!--  -->
                                                <form>
                                                     <div class='form-group existed-franchisee-group'>
                                                        <label for="franchisee_id">Block Group</label>
                                                      <select class="form-control input_empty" id="block_group">
                                                            <option value="" selected>Choose block group</option>
                                                            @foreach ($BlockGroup as $block)
                                                            <option value="{{$block->id}}">{{$block->name}}</option>
                                                            @endforeach
                                                            <!-- <option value="m">Manual</option> -->
                                                            
                                                        </select>
                                                    </div>
                                                        <div class="form-group">
                                                        <label name="name">Order</label>
                                                        <input type="number" min="1" name="name" id="order_edit"
                                                            class="form-control">
                                                    </div>
                                                       <div class="form-group">
                                                        <label name="name">Tier</label>
                                                       <select name="name" class="form-control input_empty" id="block_height_edit">
                                                                
                                                               </select>
                                                    </div>


                                                
                                                    <div class='form-group col-md-6 existed-franchisee-group'>
                                                        <label for="franchisee_id">School Year</label>
                                                        <select class="chzn-select input_empty" multiple="" multiple
                                                            class="chosen-select" size="3" style="display: none;"
                                                            name="franchisee_id" id="choose_school_year_edit">
                                                            <option value="" selected>Choose School Year</option>
                                                            @foreach ($school_years as $school_year)
                                                            <option value="{{$school_year->id}}">
                                                                {{$school_year->school_year}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                     <div class="form-group">
                                                        <label name="name">Locker Description</label>
                                                        <select class="form-control input_empty" name="color_locker" id="color_locker">
                                                            <option value="">Choose Color</option>
                                                            @foreach ($LockerColor as $color_c)
                                                            <option value="{{$color_c->id}}">{{$color_c->color}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer edit-button-section d-none" style="display: flex;">
                        <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                        <button type="submit" class="btn btn-primary " id="save-edit"> Save Changes</button>
                    </div>
                    <input type="hidden" name="comp" id="comapny_id">
                </form>
                <br>
            </div>
        </div>
    </div>
    <!-- END Edit modal-->
</div>
@endsection
@section('script')
<script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="/backend/vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>
<!-- end of plugin scripts -->

<!--Page level scripts-->
<script type="text/javascript" src="/backend/js/pages/datatable_server_side.js"></script>
<script type="text/javascript" src="/backend/js/pages/radio_checkbox.js"></script>
<script type="text/javascript" src="{{asset('/backend/js/common.js')}}"></script>

<script type="text/javascript" src="/backend/js/blocks/block_lockers.js"></script>
<script type="text/javascript" src="/backend/vendors/chosen/js/chosen.jquery.js"></script>
<script type="text/javascript" src="/backend/js/pages/form_elements.js"></script>
<script src="{{ asset('custom/general.js') }}"></script>

{{--<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>--}}
<!-- end of global scripts-->


<script type="text/javascript">
    // $("#maintenance_till_date").datepicker();

        // $(document).ready(function(){
        //    var searchFunction = 'searchByThis(outer_blocks_table)';
        //     $('#outer_blocks_table_wrapper>div:nth-child(1)>div:nth-child(2)').html('<label style="float:right;"><input class="form-control input-sm" id="search-input" onkeyup="'+searchFunction+'"></label>');
        // });
$(document).ready(function(){
    fetchTier();
});
    $(document).on('change','#block_type',function(){
       console.log('block type');
        fetchTier();
    
});

        $('body').on('change','#get_deleted_blocks', function() {
            var checkbox_status = $("#get_deleted_blocks").prop("checked");
            if(checkbox_status){
                deleteouterblockEntryTable();
            } else {
                outerBlocksEntryTable();
            }
        });

        var outer_blocks_table =  outerBlocksEntryTable();

        function outerBlocksEntryTable() {

            $('#outer_blocks_table').dataTable().fnClearTable();
            $('#outer_blocks_table').dataTable().fnDestroy();

            return $('#outer_blocks_table').DataTable({

                "order": [],
                "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
               

                columnDefs: [
                    {
                    targets: [ 0 ],
                    orderData: [ 0, 1 ]
                    },
                    {
                        targets: [ 1 ],
                        orderData: [ 1, 0 ]
                    },
                    {
                        targets: [ 2 ],
                        orderData: [ 1, 0 ]
                    },
                    {
                        targets: [ 3 ],
                        orderData: [ 1, 0 ]
                    },
                    {
                        targets: [ 4 ],
                        orderData: [ 1, 0 ]
                    },
                    {
                        targets: [ 5 ],
                        orderData: [ 1, 0 ]
                    },
                    {
                        targets: [ 6 ],
                        orderData: [ 1, 0 ]
                    },

                ],
                "oLanguage": {
                    "sLengthMenu": "_MENU_",
                    "sSearch": "_INPUT_"
                },
                "aTargets": [ "_all" ],
                ajax: '<?php echo route('company.outer.blocks.show.all'); ?>',

                "createdRow": function( row, data, dataIndex,columns ){
                    $(row).attr('id', 'block_tr'+data['id']);
                },
                columns: [
                    // {data: 'id', name: 'id'},
                    // {data: 'company.name', name: 'company.name'},
                    // {data: 'name', name: 'blocks.name'},
                    // {data: 'locker_available', name: 'locker_available'},
                    // {data: 'rate.unit', name: 'rate.unit'},
                    // {data: 'block_height', name: 'blocks.block_height'},
                    // {data: 'locker_blocks', name: 'blocks.locker_blocks'},
                    // {data: 'numbering', name: 'numbering'},
                    // {data: 'action', name: 'action', orderable: false, searchable: true}

                    {data: 'company_name', name: 'company_name'},
                    {data: 'name', name: 'blocks.name'},
                    {data: 'locker_available', name: 'locker_available'},
                    {data: 'rateunit', name: 'rateunit'},
                    {data: 'block_height', name: 'blocks.block_height'},
                    {data: 'locker_blocks', name: 'blocks.locker_blocks'},
                    {data: 'numbering', name: 'numbering'},
                    {data: 'action', name: 'action', orderable: false, searchable: true}
                ],

            });

        }

        function deleteouterblockEntryTable() {

            $('#outer_blocks_table').dataTable().fnClearTable();
            $('#outer_blocks_table').dataTable().fnDestroy();

            return $('#outer_blocks_table').DataTable({

                "order": [],
                "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],

                stateSave: true,
                columnDefs: [
                    {
                        targets: [ 0 ],
                        orderData: [ 0, 1 ]
                    },
                    {
                        targets: [ 1 ],
                        orderData: [ 1, 0 ]
                    },
                    {
                        targets: [ 2 ],
                        orderData: [ 1, 0 ]
                    },
                    {
                        targets: [ 3 ],
                        orderData: [ 1, 0 ]
                    },
                    {
                        targets: [ 4 ],
                        orderData: [ 1, 0 ]
                    },
                    {
                        targets: [ 5 ],
                        orderData: [ 1, 0 ]
                    },
                    {
                        targets: [ 6 ],
                        orderData: [ 1, 0 ]
                    },

                ],
                "oLanguage": {
                    "sLengthMenu": "_MENU_",
                    "sSearch": "_INPUT_"
                },
                "aTargets": [ "_all" ],
                ajax: '<?php echo route('company.outer.blocks.show.delete'); ?>',
                columns: [
                    // {data: 'id', name: 'id'},
                    {data: 'company', name: 'company'},
                    {data: 'name', name: 'name'},
                    {data: 'locker_available', name: 'locker_available'},
                    {data: 'Price', name: 'Price'},
                    {data: 'tier', name: 'tier'},
                    {data: 'locker_blocks', name: 'locker_blocks'},
                    {data: 'numbering', name: 'numbering'},
                    {data: 'action', name: 'action', orderable: false, searchable: true}
                ],
                // initComplete : function() {
                //     var searchFunction = 'searchByThis(outer_blocks_table)';
                //     $('#outer_blocks_table_wrapper>div:nth-child(1)>div:nth-child(2)').html('<label style="float:right;"><input class="form-comtrol input-sm" id="search-input" onkeyup="'+searchFunction+'"></label>');
                // },
            });
        }

        $('body').on('click','#locker_tab',function(e){
            $('.edit-button-section').addClass('d-none');
        });

        $('body').on('click','#block_tab',function(e){
            $('.edit-button-section').removeClass('d-none');
        });

        function clickOnlockerNotEditable(id,locker_number,active,display){
            $("#error_label").html("");
            console.log("hi");
            var radio_option = $('input[name=radio1]:checked').val();
            var locker_active = 0;
            var locker_display = 0;
            var checkboxvalue = 0;
            var  selected_lockers = JSON.parse(sessionStorage.getItem("selected_lockers_backend"));

            if($("#deleted_checkbox").prop("checked") == true){
                checkboxvalue = 1;
            }
            else if($("#deleted_checkbox").prop("checked") == false){
                checkboxvalue = 0;
            }
            // if(radio_option == 'display'){
            //         $("#locker_name").attr("readonly", false);
            //         $("#locker_name").val(locker_number);
            //         $("#locker_id").val(id);
            // }
            // if(radio_option == 'blank') {
            //     if (checkboxvalue == 0) {
            //         locker_active = 1;
            //         locker_display = 0;
            //         var url = '/admin/backend/posteditlocker';
            //         lockerAction(locker_number, id, locker_active, locker_display, 0, url);
            //     }
            //
            // }

            if(radio_option == 'display'){
                $("#locker_name").attr("readonly", false);
                $("#locker_name").val(locker_number);
                $("#locker_id").val(id);
            }

            else if(radio_option == 'blank'){

                if(checkboxvalue == 0){

                    var locker_checed = $('#locker'+id).prop("checked");

                    if(locker_checed == false){

                        selected_lockers.push([locker_number, id]);
                        sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));
                        console.log(selected_lockers,'selected_lockers in blank checked');

                        $('#locker'+id).prop('checked', true);

                        $('#locker'+id).css("transform", "scale(1, 1)");
                        $('#locker'+id).css("border", "5px solid #66ff66");

                        $('#'+id).css("transform", "scale(1, 1)");
                        $('#'+id) .css("border", "5px solid #66ff66");

                    }else if(locker_checed == true){
                        const  selected_lockers_in_deselect = JSON.parse(sessionStorage.getItem("selected_lockers_backend"));
                        $.each(selected_lockers_in_deselect ,function (index, value) {
                            console.log(value,'value');
                            if(value[0] == locker_number && value[1] == id ){
                                // console.log('same');
                                selected_lockers_in_deselect.splice(index, 1);

                                sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers_in_deselect));
                            }
                            console.log(selected_lockers_in_deselect,'selected_lockers in blank unchecked');

                        });

                        $('#locker'+id).prop('checked', false);

                        $('#locker'+id).css("transform", "scale(1, 1)");
                        $('#'+id).css("transform", "scale(1, 1)");
                        if($('#locker'+id).data('blank') == true) {
                            $('#locker' + id).css("border", "5px solid #9d9d9d");
                            $('#'+id) .css("border", "5px solid #9d9d9d");
                        }else{

                            $('#locker' + id).css("border", "5px solid #F1F1F1");
                            $('#'+id) .css("border", "5px solid #F1F1F1");
                        }

                    }

                    // locker_active = 1;
                    // locker_display = 0;
                    // alert('blank')
                    // lockerAction(locker_number,id,locker_active,locker_display,0);
                }

            }

            else if(radio_option == 'delete'){

                var locker_checed = $('#locker'+id).prop("checked");

                if(locker_checed == false){

                    selected_lockers.push([locker_number, id]);
                    sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));
                    console.log(selected_lockers,'selected_lockers in delete checked');

                    $('#locker'+id).prop('checked', true);

                    $('#locker'+id).css("transform", "scale(1, 1)");
                    $('#locker'+id).css("border", "5px solid #66ff66");

                    $('#'+id).css("transform", "scale(1, 1)");
                    $('#'+id) .css("border", "5px solid #66ff66");

                }else if(locker_checed == true){
                    const  selected_lockers_in_deselect = JSON.parse(sessionStorage.getItem("selected_lockers_backend"));

                    $.each(selected_lockers_in_deselect ,function (index, value) {
                        console.log(value,'value');
                        if(value[0] == locker_number && value[1] == id ){
                            // console.log('same');
                            selected_lockers_in_deselect.splice(index, 1);

                            sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers_in_deselect));
                        }
                        console.log(selected_lockers,'selected_lockers in delete unchecked');
                    });

                    $('#locker'+id).prop('checked', false);

                    $('#locker'+id).css("transform", "scale(1, 1)");
                    $('#'+id).css("transform", "scale(1, 1)");
                    if($('#locker'+id).data('blank') == true) {
                        $('#locker' + id).css("border", "5px solid #9d9d9d");
                        $('#'+id) .css("border", "5px solid #9d9d9d");
                    }else{

                        $('#locker' + id).css("border", "5px solid #F1F1F1");
                        $('#'+id) .css("border", "5px solid #F1F1F1");
                    }

                }

                // if(checkboxvalue == 0){
                //         locker_active = 0;
                //         locker_display = 1;
                // }
                // else{
                //         locker_active = 1;
                //         locker_display = 1;
                // }
                //
                // lockerAction(locker_number,id,locker_active,locker_display,0);
            }

            else if(radio_option == 'maintenance'){

                var locker_checed = $('#locker'+id).prop("checked");

                if(locker_checed == false){

                    selected_lockers.push([locker_number, id]);
                    sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));
                    console.log(selected_lockers,'selected_lockers');

                    $('#locker'+id).prop('checked', true);

                    if($('#locker'+id).data('type') == 'maintenance') {
                        $('#locker' + id).css("border", "5px solid #9d9d9d");
                        $('#'+id) .css("border", "5px solid #9d9d9d");
                    }else{

                        $('#locker' + id).css("border", "5px solid #F1F1F1");
                        $('#'+id) .css("border", "5px solid #F1F1F1");
                    }

                    $('#locker'+id).css("transform", "scale(1, 1)");
                    $('#locker'+id).css("border", "5px solid #66ff66");

                    $('#'+id).css("transform", "scale(1, 1)");
                    $('#'+id) .css("border", "5px solid #66ff66");

                }else if(locker_checed == true){
                    const  selected_lockers_in_deselect = JSON.parse(sessionStorage.getItem("selected_lockers_backend"));
                    $.each(selected_lockers_in_deselect ,function (index, value) {
                        console.log(value,'value');
                        if(value[0] == locker_number && value[1] == id ){
                            // console.log('same');
                            selected_lockers_in_deselect.splice(index, 1);
                            console.log(selected_lockers_in_deselect);
                            sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers_in_deselect));
                        }
                    });

                    $('#locker'+id).prop('checked', false);

                    $('#locker'+id).css("transform", "scale(1, 1)");
                    $('#'+id).css("transform", "scale(1, 1)");
                    if($('#locker'+id).data('type') == 'blank') {
                        $('#locker' + id).css("border", "5px solid #9d9d9d");
                        $('#'+id) .css("border", "5px solid #9d9d9d");
                    }else{

                        $('#locker' + id).css("border", "5px solid #F1F1F1");
                        $('#'+id) .css("border", "5px solid #F1F1F1");
                    }

                }

                // if(checkboxvalue == 0){
                //         locker_active = 0;
                //         locker_display = 1;
                // }
                // else{
                //         locker_active = 1;
                //         locker_display = 1;
                // }
                //
                // lockerAction(locker_number,id,locker_active,locker_display,0);
            }

            else if(radio_option == 'on_off_sale'){

                var locker_checed = $('#locker'+id).prop("checked");

                if(locker_checed == false){

                    selected_lockers.push([locker_number, id]);
                    sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));
                    console.log(selected_lockers,'selected_lockers');

                    $('#locker'+id).prop('checked', true);

                    $('#locker'+id).css("transform", "scale(1, 1)");
                    $('#locker'+id).css("border", "5px solid #66ff66");

                    $('#'+id).css("transform", "scale(1, 1)");
                    $('#'+id) .css("border", "5px solid #66ff66");

                }else if(locker_checed == true){
                    const  selected_lockers_in_deselect = JSON.parse(sessionStorage.getItem("selected_lockers_backend"));
                    $.each(selected_lockers_in_deselect ,function (index, value) {
                        console.log(value,'value');
                        if(value[0] == locker_number && value[1] == id ){
                            // console.log('same');
                            selected_lockers_in_deselect.splice(index, 1);
                            console.log(selected_lockers_in_deselect);
                            sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers_in_deselect));
                        }
                    });

                    $('#locker'+id).prop('checked', false);

                    $('#locker'+id).css("transform", "scale(1, 1)");
                    $('#'+id).css("transform", "scale(1, 1)");

                    if($('#locker'+id).data('blank') == true) {
                        $('#locker' + id).css("border", "5px solid #9d9d9d");
                        $('#'+id) .css("border", "5px solid #9d9d9d");
                    }else{

                        $('#locker' + id).css("border", "5px solid #F1F1F1");
                        $('#'+id) .css("border", "5px solid #F1F1F1");
                    }

                }

                // if(checkboxvalue == 0){
                //         locker_active = 0;
                //         locker_display = 1;
                // }
                // else{
                //         locker_active = 1;
                //         locker_display = 1;
                // }
                //
                // lockerAction(locker_number,id,locker_active,locker_display,0);
            }

            else if(radio_option == 'move'){

                var locker_checed = $('#locker'+id).prop("checked");
                console.log(selected_lockers,'selected_lockers');


                if(locker_checed == false){
                    //perviouse locker deselect

                    selected_lockers.push([locker_number, id]);
                    sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));
                    console.log(selected_lockers,'selected_lockers');

                    $('#locker'+id).prop('checked', true);

                    $('#locker'+id).css("transform", "scale(1, 1)");
                    $('#locker'+id).css("border", "5px solid #66ff66");

                    $('#'+id).css("transform", "scale(1, 1)");
                    $('#'+id) .css("border", "5px solid #66ff66");

                }else if(locker_checed == true){
                    const  selected_lockers_in_deselect = JSON.parse(sessionStorage.getItem("selected_lockers_backend"));
                    $.each(selected_lockers_in_deselect ,function (index, value) {
                        console.log(value,'value');
                        if(value[0] == locker_number && value[1] == id ){
                            // console.log('same');
                            selected_lockers_in_deselect.splice(index, 1);
                            console.log(selected_lockers_in_deselect);
                            sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers_in_deselect));
                        }
                    });

                    $('#locker'+id).prop('checked', false);

                    $('#locker'+id).css("transform", "scale(1, 1)");
                    $('#'+id).css("transform", "scale(1, 1)");

                    if($('#locker'+id).data('blank') == true) {
                        $('#locker' + id).css("border", "5px solid #9d9d9d");
                        $('#'+id) .css("border", "5px solid #9d9d9d");
                    }else{

                        $('#locker' + id).css("border", "5px solid #F1F1F1");
                        $('#'+id) .css("border", "5px solid #F1F1F1");
                    }

                }


            }

            else if(radio_option == 'move_out_side_block'){

                var locker_checed = $('#locker'+id).prop("checked");
                console.log(selected_lockers,'selected_lockers');

                if(locker_checed == false){
                    //perviouse locker deselect

                    selected_lockers.push([locker_number, id]);
                    sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));
                    console.log(selected_lockers,'selected_lockers');

                    $('#locker'+id).prop('checked', true);

                    $('#locker'+id).css("transform", "scale(1, 1)");
                    $('#locker'+id).css("border", "5px solid #66ff66");

                    $('#'+id).css("transform", "scale(1, 1)");
                    $('#'+id) .css("border", "5px solid #66ff66");

                }else if(locker_checed == true){
                    const  selected_lockers_in_deselect = JSON.parse(sessionStorage.getItem("selected_lockers_backend"));
                    $.each(selected_lockers_in_deselect ,function (index, value) {
                        console.log(value,'value');
                        if(value[0] == locker_number && value[1] == id ){
                            // console.log('same');
                            selected_lockers_in_deselect.splice(index, 1);
                            console.log(selected_lockers_in_deselect);
                            sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers_in_deselect));
                        }
                    });

                    $('#locker'+id).prop('checked', false);

                    $('#locker'+id).css("transform", "scale(1, 1)");
                    $('#'+id).css("transform", "scale(1, 1)");

                    if($('#locker'+id).data('blank') == true) {
                        $('#locker' + id).css("border", "5px solid #9d9d9d");
                        $('#'+id) .css("border", "5px solid #9d9d9d");
                    }else{

                        $('#locker' + id).css("border", "5px solid #F1F1F1");
                        $('#'+id) .css("border", "5px solid #F1F1F1");
                    }

                }


            }

            // if(radio_option == 'delete'){

            //         if(checkboxvalue == 0){
            //                 locker_active = 0;
            //                 locker_display = 1;
            //         }
            //         else{
            //                 locker_active = 1;
            //                 locker_display = 1;
            //         }

            //         lockerAction(locker_number,id,locker_active,locker_display,0);
            // }
        }

        function MoveWithinBlockAction(selected_lockers ,move_locker_number){
            var csrf = $('meta[name=csrf-token]').attr("content");
            $(".loader").show();
            jQuery('#lockers_div').css('opacity', '0.05');
            var url =  '{{ route('moveWithInBlock') }}';
            $.ajax({
                url: url,
                type: "post",
                data: {
                    selected_lockers: selected_lockers,
                    move_locker_number: move_locker_number,
                    _token: csrf,
                },
                success: function (response) {
                    if(response == 11){
                        $("#error_label").html("Locker Number Already in Use");
                    }
                    else{
                        //set empty selected array
                        var  selected_lockers =[];
                        sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));

                        $("#error_label").html("");

                        $('#lockerModal').modal('hide');

                        var block_id = $("#blocksul").val();
                        var checkboxvalue = 0;
                        if ($("#deleted_checkbox").prop("checked") == true) {
                            checkboxvalue = 1;
                        } else if ($("#deleted_checkbox").prop("checked") == false) {
                            checkboxvalue = 0;
                        }
                        create_lockers_ajax(block_id, checkboxvalue);

                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            });
        }

        function MoveOutSideBlockAction(selected_lockers ,block_id){

            var csrf = $('meta[name=csrf-token]').attr("content");
            $(".loader").show();
            jQuery('#lockers_div').css('opacity', '0.05');
            var url =  '{{ route('moveOutSideBlock') }}';
            $.ajax({
                url: url,
                type: "post",
                data: {
                    selected_lockers: selected_lockers,
                    block_id: block_id,
                    _token: csrf,
                },
                success: function (response) {
                    if(response == 11){
                        $("#error_label").html("Locker Number Already in Use");
                    }
                    else{
                        //set empty selected array
                        var  selected_lockers =[];
                        sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));

                        $("#error_label").html("");

                        $('#lockerModal').modal('hide');

                        var block_id = $("#blocksul").val();
                        var checkboxvalue = 0;
                        if ($("#deleted_checkbox").prop("checked") == true) {
                            checkboxvalue = 1;
                        } else if ($("#deleted_checkbox").prop("checked") == false) {
                            checkboxvalue = 0;
                        }
                        create_lockers_ajax(block_id, checkboxvalue);

                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            });
        }

        function lockerAction(selected_lockers,locker_active,locker_display,reorder){
            // alert(selected_lockers,'selected_lockers');
            var csrf = $('meta[name=csrf-token]').attr("content");
            console.log(csrf,'csrf');
            $(".loader").show();
            jQuery('#lockers_div').css('opacity', '0.05');
            console.log(selected_lockers,'selected_lockers');
            console.log(locker_active,'locker_active');

            $.ajax({
                url: "/admin/backend/posteditlocker",
                type: "post",
                data: {
                    selected_lockers: selected_lockers,
                    // locker_id: locker_id,
                    locker_active: locker_active,
                    locker_display: locker_display,
                    reorder: reorder,
                    _token: csrf,
                },
                success: function (response) {
                    if(response == 11){
                        $("#error_label").html("Locker Number Already in Use");
                    }
                    else{
                        //set empty selected array
                        var  selected_lockers =[];
                        sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));

                        $("#error_label").html("");
                        $('#lockerModal').modal('hide');

                        var block_id = $("#blocksul").val();
                        var checkboxvalue = 0;
                        if ($("#deleted_checkbox").prop("checked") == true) {
                            checkboxvalue = 1;
                        } else if ($("#deleted_checkbox").prop("checked") == false) {
                            checkboxvalue = 0;
                        }
                        create_lockers_ajax(block_id, checkboxvalue);

                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            });
        }

        function maintenanceAction(selected_lockers,maintenance_till_date,locker_active,locker_display){
            var csrf = $('meta[name=csrf-token]').attr("content");
            $(".loader").show();
            jQuery('#lockers_div').css('opacity', '0.05');
            $.ajax({
                url: "/admin/backend/maintenancelocker",
                type: "post",
                data: {
                    selected_lockers: selected_lockers,
                    maintenance_till_date: maintenance_till_date,
                    locker_active: locker_active,
                    locker_display: locker_display,
                    _token: csrf,
                },
                success: function (response) {
                    if(response == 11){
                        $("#error_label").html("Locker Number Already in Use");
                    }
                    else{
                        //set empty selected array
                        var  selected_lockers =[];
                        sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));

                        $("#error_label").html("");

                        $('#lockerModal').modal('hide');

                        var block_id = $("#blocksul").val();
                        var checkboxvalue = 0;
                        if ($("#deleted_checkbox").prop("checked") == true) {
                            checkboxvalue = 1;
                        } else if ($("#deleted_checkbox").prop("checked") == false) {
                            checkboxvalue = 0;
                        }
                        create_lockers_ajax(block_id, checkboxvalue);

                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            });
        }

        function onOffSaleAction(selected_lockers ,locker_active){
            var csrf = $('meta[name=csrf-token]').attr("content");
            $(".loader").show();
            jQuery('#lockers_div').css('opacity', '0.05');
            $.ajax({
                url: "/admin/backend/onOffSale",
                type: "post",
                data: {
                    selected_lockers: selected_lockers,
                    locker_active: locker_active,
                    _token: csrf,
                },
                success: function (response) {
                    if(response == 11){
                        $("#error_label").html("Locker Number Already in Use");
                    }
                    else{
                        //set empty selected array
                        var  selected_lockers =[];
                        sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));

                        $("#error_label").html("");

                        $('#lockerModal').modal('hide');

                        var block_id = $("#blocksul").val();
                        var checkboxvalue = 0;
                        if ($("#deleted_checkbox").prop("checked") == true) {
                            checkboxvalue = 1;
                        } else if ($("#deleted_checkbox").prop("checked") == false) {
                            checkboxvalue = 0;
                        }
                        create_lockers_ajax(block_id, checkboxvalue);

                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            });
        }

        function getLatest(){
            $.ajax({
                url: '/blocks/getUpdated',
                type: 'GET',
                dataType: 'JSON',
                cache: false,
                success: function (data) {

                    // $('#blocksul').html(data);
                    $('#move_locker_outside_block').html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {

                }
            });
        }
        // $('body').on('click','#locker_active_refresh',function(e){
        //     $(".loader").show();
        //     jQuery('#lockers_div').css('opacity', '0.05');
        //     var block_id = $("#blocksul").val();
        //     var checkboxvalue = 0;
        //     if ($("#deleted_checkbox").prop("checked") == true) {
        //         checkboxvalue = 1;
        //     } else if ($("#deleted_checkbox").prop("checked") == false) {
        //         checkboxvalue = 0;
        //     }
        //     create_lockers_ajax(block_id, checkboxvalue);
        // });
        //
        // $('body').on('click','#locker_active_submit',function(e){
        //     var locker_name = $("#locker_name").val();
        //     var locker_id = $("#locker_id").val();
        //     if(locker_name == ''){
        //         $("#error_label").html("Please click any locker");
        //     }
        //     var reorder = 0;
        //     if($("#reorder_checkbox").prop("checked") == true){
        //         reorder = 1;
        //     }
        //     else if($("#reorder_checkbox").prop("checked") == false){
        //         reorder = 0;
        //     }
        //     var status = $("#locker_active").val();
        //     var locker_name_input = document.getElementById("locker_name");
        //     const isValidlocker_name = locker_name_input.checkValidity();
        //     if(isValidlocker_name) {
        //         $("#error_label").html("");
        //         lockerAction(locker_name,locker_id,1,1,reorder);
        //         $("#locker_name").attr("readonly", true);
        //         $("#locker_name").val('');
        //         $("#locker_id").val('');
        //         $("#reorder_checkbox"). prop("checked", false);
        //     }
        //     else{
        //         $("#error_label").html("Please input value between 1-999.");
        //     }
        // });

        function clickLocker(){
            $(".loader").show();
            jQuery('#lockers_div').css('opacity', '0.05');
            var block_id = $("#blocksul").val();
            var checkboxvalue = 0;
            if($("#deleted_checkbox").prop("checked") == true){
                checkboxvalue = 1;
            }
            else if($("#deleted_checkbox").prop("checked") == false){
                checkboxvalue = 0;
            }
            create_lockers_ajax(block_id, checkboxvalue);
        }

        $('body').on('change','#deleted_checkbox', function() {
            $(".loader").show();
            jQuery('#lockers_div').css('opacity', '0.05');
            var checkboxvalue = 0;
            if($(this).prop("checked") == true){
                checkboxvalue = 1;
                $(".display_edit_div").hide();
            }
            else if($(this).prop("checked") == false){
                checkboxvalue = 0;
                $("#locker_name").attr("readonly", true);
                $("#locker_name").val('');
                $("#locker_id").val('');
                var radio_option = $('input[name=radio1]:checked').val();
                $("#error_label").html("");
                if(radio_option == 'display'){
                    $(".display_edit_div").show();
                }
            }
            var block_id = $("#blocksul").val();
            create_lockers_ajax(block_id, checkboxvalue);
        });

        $('body').on('change','#locker_active', function() {
            if($(this).val() == 'show'){
                $("#reorder_checkbox").prop("disabled", false);
                $("#reorder_checkbox_div").show();
                $("#locker_name").show();
            }
            if($(this).val() == 'hide'){
                $("#reorder_checkbox").prop("disabled", true);
                $("#locker_name").hide();
                $("#reorder_checkbox_div").hide();
            }
            if($(this).val() == 'delete'){
                $("#reorder_checkbox").prop("disabled", true);
                $("#reorder_checkbox_div").hide();
                $("#locker_name").show();
            }
        });

        $('body').on('click','#outerblockedit', function() {

            var block_id = $(this).data('id');
             var company_id = $(this).data('companyid');
             $("#comapny_id").val(company_id);
            $("#blocksul").val(block_id);
            var checkboxvalue = 0;
            if($("#deleted_checkbox").prop("checked") == true){
                checkboxvalue = 1;
            }
            else if($("#deleted_checkbox").prop("checked") == false){
                checkboxvalue = 0;
            }
            create_lockers_ajax(block_id, checkboxvalue);
            saveid(block_id, company_id);
        });

        $("#tabs").click("tabsselect", function (event, ui) {
            debugger;
            var $tabs = $("#tabs").tabs();
            var selected = $tabs.tabs("option", "selected"); // => 0
            // alert(selected);
        });

        function addLockersColumn(block_id) {
            // alert(block_id)
            if (confirm("Are you sure to Add New Column?")) {

                var csrf = $('meta[name=csrf-token]').attr("content");

                var url = '{{ route('addLockerColumn') }}';
                $.ajax({
                    url: url,
                    type: "post",
                    data: {
                        block_id: block_id,
                        _token: csrf,
                    },
                    success: function (response) {
                        var checkboxvalue = 0;
                        if($("#deleted_checkbox").prop("checked") == true){
                            checkboxvalue = 1;
                        }
                        else if($("#deleted_checkbox").prop("checked") == false){
                            checkboxvalue = 0;
                        }
                        create_lockers_ajax(block_id, checkboxvalue);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
            }
        }



        function create_lockers_ajax(block_id, checkboxvalue){
            $.ajax({
                type: "GET",
                url: "/admin/backend/getlockersblock",
                data: {
                    block_id: block_id,
                    checkboxvalue: checkboxvalue,
                },
                cache: false,
                success: function(data){
                    $("#lockers_div").html(data.output);
                    $("#lockers_div").each(function () {
                        var $this = $(this);
                        var newrows = [];
                        $this.find("tr").each(function () {
                            var i = 0;
                            $(this).find("td").each(function () {
                                i++;
                                if (newrows[i] === undefined) {
                                    newrows[i] = $("<tr></tr>");
                                }
                                newrows[i].append($(this));
                            });
                        });
                        $this.find("tr").remove();
                        $.each(newrows, function () {
                            $this.append(this);
                        });
                        // if (hide_show_locker_status == 'Grid Hidden') {
                        //         $('.red-lockers').addClass('d-none');
                        //
                        // }
                        return false;
                    });

                    var array_total =  data.lockers_in_single_column_array.length;
                    // console.log( data.lockers_in_single_column_array ,' data.lockers_in_single_column_array')
                    // console.log(array_total ,' array_total')
                    var locker_height = 1 ;
                    var number_of_lockers = 0 ;
                    for(var i=0 ; i < array_total ; i++) {
                        if (i == 0) {
                            // console.log(locker_height +'*' + data.lockers_in_single_column_array[i],'height');

                        }else if(data.lockers_in_single_column_array[i-1] == data.lockers_in_single_column_array[i] ){

                            var next_count = i+1;
                            locker_height = locker_height + 1;
                            if(next_count == array_total){
                                // console.log(' last printing')
                                var config_height = Math.floor(locker_height/2);
                                var config_placement = next_count - config_height;
                                $('.lockers'+config_placement).text(locker_height + 'x' +  data.lockers_in_single_column_array[i])
                                var add_lockers_column_event = 'add_lockers_column('+data.last_order_in_single_column_array[i][0]+','+data.last_order_in_single_column_array[i][1]+')';
                                console.log(add_lockers_column_event,'add_lockers_column_event');
                                $('.add_lockers_column'+config_placement).attr('onclick', add_lockers_column_event);
                                $('.add_lockers_column'+config_placement).removeClass('d-none');
                                // $('.lockers'+config_placement).css("color", "black");
                                $('.line_cut'+next_count).addClass("line-right");
                                var double_incr = next_count+1;
                                $('.line_cut'+double_incr).addClass("line-left");
                            }
                        }else if(data.lockers_in_single_column_array[i-1] != data.lockers_in_single_column_array[i]){

                            // console.log(locker_height + '*' +  data.lockers_in_single_column_array[i-1],'not equal')
                            var config_height = Math.floor(locker_height/2);
                            var config_placement = i - config_height;
                            $('.lockers'+config_placement).text(locker_height + 'x' +  data.lockers_in_single_column_array[i-1])
                            var add_lockers_column_event = 'add_lockers_column('+data.last_order_in_single_column_array[i-1][0]+','+data.last_order_in_single_column_array[i-1][1]+')';
                            console.log(add_lockers_column_event,'add_lockers_column_event');
                            $('.add_lockers_column'+config_placement).attr('onclick', add_lockers_column_event);
                            $('.add_lockers_column'+config_placement).removeClass('d-none');
                            // $('.lockers'+config_placement).css("color", "black");

                            $('.line_cut'+i).addClass("line-right");
                            var double_incr = i+1;
                            $('.line_cut'+double_incr).addClass("line-left");
                            var next_count = i+1;
                            locker_height = 1;
                            if(next_count == array_total){
                                // console.log(' not equal last printing')
                                var config_height = Math.floor(locker_height/2);
                                var config_placement = next_count - config_height;
                                $('.lockers'+config_placement).text(locker_height + 'x' +  data.lockers_in_single_column_array[i])
                                var add_lockers_column_event = 'add_lockers_column('+data.last_order_in_single_column_array[i][0]+','+data.last_order_in_single_column_array[i][1]+')';
                                console.log(add_lockers_column_event,'add_lockers_column_event');
                                $('.add_lockers_column'+config_placement).attr('onclick', add_lockers_column_event);
                                $('.add_lockers_column'+config_placement).removeClass('d-none');
                                // $('.lockers'+config_placement).css("color", "black");
                                $('.line_cut'+next_count).addClass("line-right");
                                var double_incr = next_count+1;
                                $('.line_cut'+double_incr).addClass("line-left");

                            }

                        }


                    }

                    $(".loader").hide();
                    jQuery('#lockers_div').css('opacity', '1');
                }
            });
        }

        function add_lockers_column(block_id,last_order) {
            // alert(block_id)
            if (confirm("Are you sure to Add New Column?")) {

                var csrf = $('meta[name=csrf-token]').attr("content");

                var url = '{{ route('addLockerColumn') }}';
                $.ajax({
                    url: url,
                    type: "post",
                    data: {
                        block_id: block_id,
                        last_order: last_order,
                        _token: csrf,
                    },
                    success: function (response) {
                        var checkboxvalue = 0;
                        if($("#deleted_checkbox").prop("checked") == true){
                            checkboxvalue = 1;
                        }
                        else if($("#deleted_checkbox").prop("checked") == false){
                            checkboxvalue = 0;
                        }
                        create_lockers_ajax(block_id, checkboxvalue);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
            }
        }
        function saveid(id, cmpid){
            console.log(id, cmpid);
             var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({

                url: '/getBlockDetailsouter',

                type: 'POST',

                dataType: 'JSON',

                cache: false,

                data: {company_id: cmpid, block_id: id, _token: CSRF_TOKEN},

                success: function (data) {
                    console.log(data);
                    getLatest();
                    $('#namee_edit').val(data.data.name);
                    $('#lock_type_edit').val(data.data.lock_type);
                    $('#order_edit').val(data.data.order_list);
                    $('#block_height_edit').val(data.data.block_height);
                    $('#locker_blocks_edit').val(data.data.locker_blocks);
                    $('#image_edit').val(data.data.locker_block_image_id);
                    $('#block_group').val(data.data.block_group);
                    $('#color_locker').val(data.data.color_locker);

                    // $('#block_height_edit').val(data.data.block_height);


                    console.log("as", data.data.locker_block_image_id);
                    console.log($('#image_edit'));
                   // $('#choose_school_year_edit').val(data.data.school_academic_year_id);
                    $('#block_id').val(data.data.id);
                    $('#rate_id').val(data.data.rate_id);


                    if(data.data.display == 1){
                        $('#display_edit_update').prop('checked', true);
                    }else{
                        $('#display_edit_update').prop('checked', false);
                    }

                     var ar = data.years;
                 var x =[]
                 for(var i = 0; ar.length > i; i++){
                  
                   // console.log(ar[i].school_academic_year_id)
                    x.push(ar[i].school_academic_year_id)
                 }
                   $("#choose_school_year_edit").val(x).trigger("chosen:updated");
},
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                }

            });
        }

         $('body').on('click','#save-edit', function() {
             $('#save-edit').prop('disabled', true);

            var name = $('#namee_edit').val();
            var lock_type = $('#lock_type_edit').val();
            var order =  $('#order_edit').val();
            var block_height = $('#block_height_edit').val();
            var locker_blocks = $('#locker_blocks_edit').val();
            var image = $('#image_edit').val();
            var choose_school_year = $('#choose_school_year_edit').val();
            var rate_id = $('#rate_id').val();

            var company_id = $('#comapny_id').val();
            var location_group_idd = $('#location_group_idd').val();
            var block_id = $('#blocksul').val();

            var block_group = $('#block_group').val();
            var color_locker = $('#color_locker').val();


            var display = $('#display_edit_update').is(":checked");
            console.log(display);
            var is_display = 0;
            if(display){
                is_display = 1;
            } else {
                is_display = 0;
            }

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({

                url: '/blocksouter/update/'+block_id,

                type: 'PUT',

                dataType: 'JSON',

                cache: false,

                data: {
                    name: name,
                    lock_type: lock_type,
                    order_list: order,
                    block_height: block_height,
                    locker_blocks: locker_blocks,
                    display: is_display,
                    active: 1,
                    image: image,
                    choose_school_year: choose_school_year,
                    company_id: company_id,
                    rate_id: rate_id,
                    block_images_edit: image,
                    location_group_id: location_group_idd,
                    block_group:block_group,
                    color_locker:color_locker,
                    _token: CSRF_TOKEN
                },

                success: function (data) {
                    console.log(data);
                    if (is_display =='1'){
                        $("#block_tr"+block_id).addClass('alert-success');
                        $("#block_tr"+block_id).removeClass('alert-warning');
                    }else{
                        $("#block_tr"+block_id).removeClass('alert-success');
                        $("#block_tr"+block_id).addClass('alert-warning');
                    }
                    $('#edit').modal('toggle');
                    console.log('test');
                    $('#save-edit').prop('disabled', false);

                    // $('#outer_blocks_table').each(function() {
                    //     dt = $(this).dataTable();
                    //     dt.fnDraw();
                    // });
                    outerBlocksEntryTable();
                },

                error: function (jqXHR, textStatus, errorThrown) {
                    $('#save-edit').prop('disabled', false);
                    console.log(jqXHR);
                }

            });

        })

        new Switchery(document.querySelector('.sm_toggle_show_only_deleted_outerblock'), { size: 'small', color: '#00c0ef', jackColor: '#fff' });
        new Switchery(document.querySelector('.sm_toggle_reorder_outerblock'), { size: 'small', color: '#00c0ef', jackColor: '#fff' });
        new Switchery(document.querySelector('.sm_toggle_show_only_deleted_outerblocks_main'), { size: 'small', color: '#00c0ef', jackColor: '#fff' });

function fetchTier(){
        var val=$('#block_type').val();
       
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.ajax({

            url: '/blocks/tiers',

            type: 'POST',

            dataType: 'JSON',

            cache: false,

            data: {
               
                block_type: val,
                _token: CSRF_TOKEN
            },

            success: function (data) {
                console.log(data);
                console.log('data tier');
                var html='';
                html+='<option value="">Select Tier</option>';

                for (var i = 0; i < data.length; i++){
                    html += '<option value="'+data[i].tier+'">'+data[i].tier+'</option> ';
                }

             $('#block_height_edit').html(html);
            },

            error: function (jqXHR, textStatus, errorThrown) {
                $('button#create').prop('disabled', false);
            }

        });


}

</script>


@endsection