@extends('backend.layouts.app')
@section('title', 'Lockers | Edit Cutomer')
@section('style')
    <style>
        #header {
            position: relative!important;
            background-color: gray;
        }
        .nav-item{
             color: blue;
        }
    </style>
    <link rel="stylesheet" href="/frontend/summernote/summernote-bs4.css">
@endsection
@section('content')

    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-user-plus"></i>
                            Edit {{$company->name}}
                        </h4>
                    </div>
                </div>
            </div>
        </header>

        <div class="row">

            <div class="col-lg">
                <div class="card m-t-35">
                    <div class="card-header bg-white">
                        <ul class="nav nav-tabs card-header-tabs float-left">
                            <li class="nav-item">
                                <a class="nav-link active" href="#location-group" data-toggle="tab">Location Group</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#general-info" data-toggle="tab">General Info</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#status" data-toggle="tab">Status</a>
                            </li>
                        </ul>
                    </div>

                    <div class="card-body">
                        {{ Form::model($company, array('route' => array('company.update', $company->id), 'method' => 'PUT')) }} {{-- Form model binding to automatically populate our fields with user data --}}

                        <div class="tab-content text-justify">
                            <div class="tab-pane active" id="location-group">
                                <br>
                                <h3>Edit Location Group</h3>

                                <div class='form-group'>
                                    @foreach ($locationGroups as $locationGroup)
                                        @if($company->location_group_id == $locationGroup->id)
                                            {{ Form::radio('location_group_id', $locationGroup->id, true ,['checked' => 'checked'])}}
                                            {{ Form::label($locationGroup->name, $locationGroup->name) }}<br>
                                        @else
                                            {{ Form::radio('location_group_id', $locationGroup->id, true )}}
                                            {{ Form::label($locationGroup->name, $locationGroup->name) }}<br>
                                        @endif

                                    @endforeach
                                </div>
                            </div>
                            <div class="tab-pane" id="general-info">
                                <br>
                                <div class="form-group">
                                    {{ Form::label('Currency', 'Currency') }}<br>
                                    @if($company->currency == 'GBP')
                                        {{ Form::radio('currency', $company->currency, true ,['checked' => 'checked'])}}
                                        {{ Form::label($company->currency, 'GBP') }}
                                        {{ Form::radio('currency', 'EUR', true )}}
                                        {{ Form::label('EUR', 'EUR') }}
                                    @else
                                        {{ Form::radio('currency', $company->currency, true )}}
                                        {{ Form::label('GBP', 'GBP') }}
                                        {{ Form::radio('currency', $company->currency, true ,['checked' => 'checked'] )}}
                                    {{ Form::label($company->currency, 'EUR') }}
                                    @endif
                                </div>

                                <div class="form-group">
                                    {{ Form::label('name', 'Name') }}
                                    {{ Form::text('name', null, array('class' => 'form-control')) }}
                                </div>

                                {{--<div class="form-group">--}}
                                    {{--{{ Form::label('stop_auto_rebook', 'Stop Auto Rebook') }}--}}
                                    {{--{{ Form::text('stop_auto_rebook', null, array('class' => 'form-control')) }}--}}
                                {{--</div>--}}

                                <div class='form-group'>
                                    <div class="radio_basic_swithes_padbott">
                                        {{ Form::checkbox('stop_auto_rebook' ,array('class' => 'js-switch sm_toggle_update' )) }}
                                        {{--{{ Form::label('stop_auto_rebook, Stop Auto Rebook,['class' => 'radio_switchery_padding']) }}<br>--}}

                                    </div>
                                </div>


                                <div class="form-group">
                                    {{ Form::label('name_number', 'Name Number') }}
                                    {{ Form::text('name_number', null, array('class' => 'form-control')) }}
                                </div>

                                <div class="form-group">
                                    {{ Form::label('street', 'Street') }}
                                    {{ Form::text('street', null, array('class' => 'form-control')) }}
                                </div>

                                <div class="form-group">
                                    {{ Form::label('city', 'City') }}
                                    {{ Form::text('city', null, array('class' => 'form-control')) }}
                                </div>

                                <div class="form-group">
                                    {{ Form::label('city_plus_search', 'City Plus Search') }}
                                    {{ Form::text('city_plus_search', null, array('class' => 'form-control')) }}
                                </div>

                                <div class="form-group">
                                    {{ Form::label('country', 'Country') }}
                                    {{ Form::text('country', null, array('class' => 'form-control')) }}
                                </div>

                                <div class="form-group">
                                    {{ Form::label('post_code', 'Postal Code') }}
                                    {{ Form::text('post_code', null, array('class' => 'form-control')) }}
                                </div>

                                <div class="form-group">
                                    {{ Form::label('address', 'Address') }}
                                    {{ Form::text('address', null, array('class' => 'form-control')) }}
                                </div>
                            </div>
                            <div class="tab-pane" id="status">
                                <br>
                                <div class="form-group">
                                    <label for="status">Active</label>
                                    <select class="form-control" name="active" >
                                        @if($company->active == 1)
                                            <option value="1" selected>Yes</option>
                                            <option value="0">No</option>
                                        @else
                                            <option value="1" >Yes</option>
                                            <option value="0" selected>No</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="status">Display</label>
                                    <select class="form-control" name="display" >
                                        @if($company->display == 1)
                                            <option value="1" selected>Yes</option>
                                            <option value="0">No</option>
                                        @else
                                            <option value="1" >Yes</option>
                                            <option value="0" selected>No</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="status">Check Man</label>
                                    <select class="form-control" name="check_man" >
                                        @if($company->check_man == 1)
                                            <option value="1" selected>Yes</option>
                                            <option value="0">No</option>
                                        @else
                                            <option value="1" >Yes</option>
                                            <option value="0" selected>No</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="status">Keep Block</label>
                                    <select class="form-control" name="keep_block" >
                                        @if($company->keep_block == 1)
                                            <option value="1" selected>Yes</option>
                                            <option value="0">No</option>
                                        @else
                                            <option value="1" >Yes</option>
                                            <option value="0" selected>No</option>
                                        @endif
                                    </select>
                                </div>

                            </div>
                        </div>
                        {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

                        {{ Form::close() }}
                    </div>


                </div>
            </div>

        </div>
    </div>


@endsection
