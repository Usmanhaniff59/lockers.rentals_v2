<div class="modal fade in display_none" id="view_chat" style="overflow-y: scroll;" tabindex="-1" role="dialog"
     aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-md-11">
                        <h3><i class="fa fa-pencil circle"></i> View Chat</h3>
                    </div>
                </div>

                <hr>
                <br>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-8">
                        <h3 id="view_chat_title_in_model"></h3>
                       <div id="view_chat_in_modal"></div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <br>
                <br><br>
                <br>
            </div>

        </div>
    </div>
</div>



