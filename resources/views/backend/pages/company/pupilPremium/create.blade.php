
<style type="text/css">
    #confrimation_email{
        width: auto;
        float: left;
    }
    .pupil-display{
        display: none;
    }
    #blade-html{
        width: 100%;
        /*float: left*/
        margin: auto;
    }
</style>
<div class="modal fade in display_none" id="create_pupil_premium" style="overflow-y: scroll;" tabindex="-1" role="dialog"
    aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-md-12">
                                <h3><i class="fa fa-pencil circle"></i> Create Pupil Premium</h3>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>School Year</label>
                                     <select class="form-control input_empty" name="school_year_id"
                                            id="pupil_premium_school_year_id">
                                        <option value="">Choose School Year</option>
                                        @foreach($school_years as $school_year)
                                            <option value="{{$school_year->id}}">{{$school_year->title}}</option>
                                        @endforeach
                                    </select>
                                <div id="school_year_id_error" class=" pupil_premium_error validation-error"></div>
                            </div>
                            <div class="form-group col-md-6">
                                <label>School Academic Year</label>
                                     <select class="form-control input_empty" name="school_academic_year_id"
                                            id="pupil_premium_school_academic_year_id">
                                        <!-- <option value="">Choose School Academic Year</option> -->
                                       <!--  @foreach($school_academic_year as $academic_year)
                                            <option value="{{$academic_year->id}}">{{$academic_year->school_year}}</option>
                                        @endforeach -->
                                    </select>
                                <div id="school_academic_year_id_error" class=" pupil_premium_error validation-error"></div>
                            </div>
                            <div id="blade-html" class="row">
                                
                            </div>


                         <!--    <div class="form-group col-md-6">
                                <label>Blocks</label>
                                {{--<input type='date' style="pointer:cursor" class="form-control" id='voucher_start_date' name="start" />--}}
                                <select class="form-control input_empty" name="block_id"
                                        id="pupil_premium_block_id">

                                </select>
                                <div id="block_id_error" class=" pupil_premium_error validation-error"></div>
                            </div> -->
                            <div class="form-group col-md-12 pupil-display"><h4>Assign</h4><hr> </div>
                           <div class="form-group col-md-6 pupil-display">
                                <label>Assign To</label>
                                {{--<input type='date' style="pointer:cursor" class="form-control" id='voucher_start_date' name="start" />--}}
                                <select class="form-control input_empty" name="user_id"
                                        id="pupil_premium_user_id">

                                </select>
                                <div id="user_id_error" class=" pupil_premium_error validation-error"></div>
                            </div>

                        <!--     <div class="form-group col-md-6">
                                <label for="voucher_id">Number of Lockers</label>
                                <input type='text'  class="form-control" id='pupil_premium_number_of_lockers' name="number_of_lockers" />
                                <div id="number_of_lockers_error" class="pupil_premium_error validation-error"></div>
                                <div id="number_of_lockers_booking_validation" class="pupil_premium_error validation-error"></div>
                            </div> -->
                            <div class="form-group col-md-6 pupil-display">
                                <label for="voucher_id">First Name</label>
                                <input type='text'  class="form-control" id='pupil_premium_first_name' name="first_name" value="Pupil"  />
                                <div id="first_name_error" class="pupil_premium_error validation-error"></div>
                            </div>
                            <div class="form-group col-md-6 pupil-display">
                                <label for="voucher_id">Sur name</label>
                                <input type='text'  class="form-control" id='pupil_premium_surname' name="surname" value="Premium" />
                                <div id="surname_error" class="pupil_premium_error validation-error"></div>
                            </div>
                             <div class="form-group col-md-12 pupil-display">
                               <!--  <label for="confrimation_email" style="margin-top: 45px;">  <input type='checkbox' style="margin-right: 15px;margin-top: 6px;"  id='confrimation_email' value="1" name="confrimation_email"  />Send Confirmation Email</label> -->
                              
                                 <!--  <label class="custom-control custom-radio signin_radio1 d-inline-block pr-3" style="margin-left: 10px;">
                                    <input id="confrimation_email" name="confrimation_email" value="1" type="checkbox" class="custom-control-input search-input">
                                    <span class="custom-control-label"></span>
                                    <span class="custom-control-description">Send Confirmation Email</span>
                                </label> -->
                              <!--  <label class="custom-control custom-checkbox">
                                        <input type="checkbox" id="confrimation_email" name="confrimation_email" value="1"  class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Send Confirmation Email</span>
                                        </label> -->
                                        <div class="checkbox ">
                                        <label>
                            <input type="checkbox" id="confrimation_email" name="confrimation_email" value="1">

                            <span class="cr"><i class="cr-icon fa fa-check" aria-hidden="true"></i></span>
                           Send Confirmation Email
                        </label>
                    </div>
                                <div id="confrimation_email_error" class="pupil_premium_error validation-error"></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="display: flex;">
                 <button type="button" data-dismiss="modal" class="btn btn-light">Cancel</button>
                <button type="button" class="btn btn-primary" id="save_pupil_premium_btn">Save</button>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript" src="/backblock_id/vblock_idors/switchery/js/switchery.min.js"></script>
<script type="text/javascript" src="/backblock_id/js/jquery.min.js"></script>

<script type="text/javascript">
    $( document ).ready(function() {

        $(".input_create_pupil_premium_empty").click(function () {
            $(".input_empty").val('');
        });

        var optSimple = {
            sideBySide:true,
            format: "DD MMM, YYYY",
        };
        $('.datetimepicker').datetimepicker(
            optSimple
        );

        $('body').on('change','#pupil_premium_school_academic_year_id,#pupil_premium_school_year_id',function(e){
              var school_year  = $('#pupil_premium_school_year_id').val();
            $('button#save_pupil_premium_btn').prop('disabled', false);
            $('button#save_pupil_premium_btn').css('cursor', 'default');
            var school_academic_year_id  = $('#pupil_premium_school_academic_year_id').val();
            if(school_year!='' && school_year!=undefined && school_year!=null ){
                // alert(school_academic_year_id);
                 $('#school_academic_year_id_error').html('');
                 $('#school_academic_year_id_error').hide();
                 $('#school_year_id_error').html('');
            $('#school_year_id_error').hide();
            $('.pupil-display').css('display','block');
            
          
            

            var company_id  = $('#company_id').val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({

                url: '/admin/company/pupil/premium/get/blocks',
                type: 'POST',
                data: {
                    school_academic_year_id: school_academic_year_id,
                    school_year:school_year,
                    company_id: company_id,
                    _token: CSRF_TOKEN
                },

                success: function (response) {
                    console.log('testing create');
                    console.log(response);
                    var blocks ='';

                    blocks+=' <div class="form-group col-md-12"><h4>Blocks</h4><hr>  </div>';
                     if(response!=null && response!=undefined && response!=''){
                    $.each(response ,function (index,val) { 
                       
                        if(val.block!=null && val.block!=undefined && val.block!=''){
                        blocks+=' <div class="form-group col-md-6">';
                                blocks+='<label>'+val.block.name+' ('+val['city']['total_available']+'/'+val['country']+')</label>';
                                // {{--<input type='date' style="pointer:cursor" class="form-control" id='voucher_start_date' name="start" />--}}
                                blocks+='<input type="text" class="form-control block-input" data-name="'+val.block.name+'" data-blockId="'+val.block.id+'" data-available="'+val['city']['total_available']+'" data-all="'+val['country']+'" placeholder="Enter the value" data-id="'+index+'" id="block-'+index+'" name="block[]" >';

                                blocks+='';
                                blocks+='<div id="block_id_error_'+index+'" class="pupil_premium_error validation-error"></div>';
                           blocks+=' </div> ';
                       }
                        
                    //     console.log(val['block']);
                    //     console.log("val['block']");
                    //     if(val.block!=null && val.block!=undefined && val.block!=''){
                    //     blocks+='<option data-id="'+val['city']['total_available']+'" value="'+val.block_id+'"> '+val.block.name+' ('+val['city']['total_available']+'/'+val['country']+') </option>';
                    });
                    }else{
                    blocks+='<div class="form-group col-md-12"><h4 style="text-align:center;">No blocks available</h4><hr> </div>';
                    $('.pupil-display').css('display','none');
                    $('button#save_pupil_premium_btn').prop('disabled', true);
                    $('button#save_pupil_premium_btn').css('cursor', 'no-drop');

                   }
                   blocks+='<div id="block_id_error"  class=" form-group col-md-12 pupil_premium_error validation-error"></div>';
                   
                    // });

                    $('#blade-html').html(blocks);

                },

                error: function (xhr, detail) {



                }


            });
        }else{
            $('#school_year_id_error').html('Choose School Year');
            $('#school_year_id_error').show();

             var blocks ='';

                    blocks+=' <div class="form-group col-md-12"><h4>Blocks</h4><hr>  </div>';
                     blocks+='<div class="form-group col-md-12"><h4 style="text-align:center;">No blocks available</h4><hr> </div>';
                       blocks+='<div id="block_id_error"  class=" form-group col-md-12 pupil_premium_error validation-error"></div>';
                   
                    // });
                      $('.pupil-display').css('display','none');
                    $('button#save_pupil_premium_btn').prop('disabled', true);
                    $('button#save_pupil_premium_btn').css('cursor', 'no-drop');
                    $('#blade-html').html(blocks);
                   

        }

        });
           $(document).on('keyup','.block-input',function(){
            $('#block_id_error').hide();
                var number_of_lockers = $(this).val();
                var availableLockers=$(this).attr('data-available');
                var index=$(this).attr('data-id');
                
                // if(number_of_lockers=0 && number_of_lockers!='')
                // {
                //      $('#block_id_error_'+index).show();
                //         $('#block_id_error_'+index).html('<span>The number of lockers must be at least 1.</span>')
                //         return false;
                //     }else{
                //          $('#block_id_error_'+index).hide();
                //         $('#block_id_error_'+index).html(' ')
                //     }
                   


                if(availableLockers!=undefined && Number(availableLockers)<Number(number_of_lockers))
                {
                    

                    $('#block_id_error_'+index).show();
                    if(availableLockers==0)
                    {
                        $('#block_id_error_'+index).html('<span>No blocks available</span>');
                    }else{
                        console.log(number_of_lockers+'number_of_lockers')
                        console.log(availableLockers+'123123');
                        $('#block_id_error_'+index).html('<span>Can only book '+availableLockers+' lockers</span>');
                    }
                }else{
                   $('#block_id_error_'+index).hide();
                   $('#block_id_error_'+index).html(' ')
                }
               
        });
        $('button#save_pupil_premium_btn').click(function(){

            // $('button#save_pupil_premium_btn').prop('disabled', true);
            var school_academic_year_id  = $('#pupil_premium_school_academic_year_id').val();

            var school_year_id  = $('#pupil_premium_school_year_id').val();

            // var block_id  = $('#pupil_premium_block_id').val();
            var user_id = $('#pupil_premium_user_id').val();
            var number_of_lockers=$("input[name='block[]']")
              .map(function(){return $(this).val();}).get();
           
            var block_id= $("input[name='block[]']")
              .map(function(){return $(this).attr('data-blockId');}).get();
            // var number_of_lockers = $('#pupil_premium_number_of_lockers').val();

            var first_name = $('#pupil_premium_first_name').val();
            var surname = $('#pupil_premium_surname').val();
            var company_id = $('#company_id').val();
            
            if($("#confrimation_email").prop('checked') == true){
        var confrimation_email =true;
}else{
    var confrimation_email=false
}


            $("#pupilPremiumTable_filter>label").css('float', 'right');

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({

                url: '/admin/company/pupil/premium/store',
                type: 'POST',
                data: {
                    company_id: company_id,
                    school_year_id: school_year_id,
                    school_academic_year_id: school_academic_year_id,
                    block_id: block_id,
                    user_id: user_id,
                    number_of_lockers: number_of_lockers,
                    first_name: first_name,
                    surname: surname,
                    _token: CSRF_TOKEN,
                    confrimation_email:confrimation_email,
                },

                success: function (response) {
                    console.log(response);
                    if(response.status == 'booking limit exceeds'){
                        $('.pupil_premium_error').show();
                        $('#number_of_lockers_booking_validation').text(response.msg);
                        return false;
                    }else if(response.status == 'validation_error'){
                          $('#block_id_error').show();
                        $('#block_id_error').text(response.msg);
                         return false;
                    }else if(response.status=='bookLookerError'){
                        $('#block_id_error_'+response.key).show();
                        $('#block_id_error_'+response.key).text(response.msg);
                         return false;
                    }
                    $('#create_pupil_premium').modal('hide');
                    $('#edit').modal('show');
                    $('button#save_pupil_premium_btn').prop('disabled', false);
                    // refreshTableAfterChat();
                    
                    pupilPremiumTable();
                },

                error: function (xhr, detail) {

                    $('.pupil_premium_error').show();
                    $('button#save_pupil_premium_btn').prop('disabled', false);

                    if(xhr.responseJSON.errors.school_year_id){

                        $('#school_year_id_error').show()
                        $('#school_year_id_error').html('<span>'+ xhr.responseJSON.errors.school_year_id[0] + '</span>');
                    }
                    else{
                        $('#school_year_id_error').hide();
                    }
                    if(xhr.responseJSON.errors.school_academic_year_id){

                        $('#school_academic_year_id_error').show()
                        $('#school_academic_year_id_error').html('<span>'+ xhr.responseJSON.errors.school_academic_year_id[0] + '</span>');
                    }
                    else{
                        $('#school_academic_year_id_error').hide();
                    }
                    if(xhr.responseJSON.errors.block_id){
                        $('#block_id_error_0').show();
                        $('#block_id_error_0').html('<span>'+ xhr.responseJSON.errors.block_id[0] + '</span>');
                    }
                    else{
                        $('#block_id_error_0').hide();
                    }
                    if(xhr.responseJSON.errors.user_id){
                        $('#user_id_error').show()
                        $('#user_id_error').html('<span>'+ xhr.responseJSON.errors.user_id[0] + '</span>');
                    }
                    else{
                        $('#user_id_error').hide();
                    }
                    if(xhr.responseJSON.errors.number_of_lockers){
                        $('#number_of_lockers_error_0').show()
                        $('#number_of_lockers_error_0').html('<span >'+ xhr.responseJSON.errors.number_of_lockers[0] + '</span>');
                    }
                    else{
                        $('#number_of_lockers_error_0').hide();
                    }
                    if(xhr.responseJSON.errors.first_name){
                        $('#first_name_error').show()
                        $('#first_name_error').html('<span >'+ xhr.responseJSON.errors.first_name[0] + '</span>');
                    }
                    else{
                        $('#first_name_error').hide();
                    }
                    if(xhr.responseJSON.errors.surname){
                        $('#surname_error').show()
                        $('#surname_error').html('<span >'+ xhr.responseJSON.errors.surname[0] + '</span>');
                    }
                    else{
                        $('#surname_error').hide();
                    }


                }


            });

        })

    });

</script>