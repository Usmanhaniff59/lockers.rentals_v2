<!--- Move booking model -->
<div class="modal fade in display_none" id="move_booking" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            {{ Form::open(array('url' => 'admin/refundPayment','id'=>'refund_form')) }}
            <div class="modal-body">

                <h3><i class="fa fa-money circle"></i> Move Booking</h3>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="voucher_id">Frist Name</label>
                        <input type='text'  class="form-control" id='edit_pupil_premium_first_name' name="first_name"   />
                        <div id="first_name_error" class="pupil_premium_error validation-error"></div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="voucher_id">Surname</label>
                        <input type='text'  class="form-control" id='edit_pupil_premium_surname' name="surname"   />
                        <div id="surname_error" class="pupil_premium_error validation-error"></div>
                    </div>
                </div>
                <div id="display_blocks"></div>

                <div class="modal-footer float-left">
                    <div class="checkbox ">
                        <input type="hidden" value="" id="booking_id_current">
                        <input type="hidden" value="" id="current_child_sale_id">
                        <input type="hidden" value="" id="sale_id_next_year">
                        <label>
                            <input type="checkbox" value="" id="hold_next_year">

                            <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                            Hold this locker to be booked while my child is at the school. You will be given the opportunity to confirm this booking in April.
                        </label>
                        <br>
                        <label>
                            <input type="checkbox" value="" id="email_checked">
                            <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                            Send confirmation Email
                        </label>
                        <br>
                        <label>
                            <input type="checkbox" value="" id="move_checked">
                            <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                            Done
                        </label>
                    </div>

                </div>

                <div class="modal-footer ">
                    <button type="button" data-dismiss="modal" style="margin-top: 15px;" class="btn btn-light ">Close</button>
                    <button type="button" class="btn btn-primary" style="margin-top: 15px;" id="update_pupil_premium_btn">Save</button>

                </div>

                {{ Form::close() }}
                <br>
            </div>
        </div>
    </div>
</div>
<script>
    $('body').on('click','#update_pupil_premium_btn',function(e){



        var child_first_name = $('#edit_pupil_premium_first_name').val();
        var child_surname = $('#edit_pupil_premium_surname').val();
        var sale_id = $('#current_child_sale_id').val();
        $('.pupil_premium_error').hide();

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');


        $.ajax({
            url: '/admin/company/pupil/premium/updateName',
            type: 'POST',
            dataType: 'JSON',
            cache: false,
            data: { child_first_name: child_first_name, child_surname: child_surname, sale_id: sale_id,
                _token: CSRF_TOKEN},
            success: function (response) {

                $('#move_booking').modal('hide');
                $('#edit').modal('show');
                pupilPremiumTable();
            },
            error: function (xhr, detail) {

            }
        });

    });
    $('body').on('click', '#email_checked', function (e) {
            var checkbox_next = $('#email_checked').prop('checked');
            var booking_id = $('#sale_id').val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                url: '/admin/edit/payment/updateEmailStatus',
                //usingEditorderController
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {
                    booking_id: booking_id,
                    checkbox_next: checkbox_next,
                    _token: CSRF_TOKEN
                },
                success: function (data) {
                    console.log(data);
                    var ref = $('#payments_order').DataTable();
                    ref.ajax.reload();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        });

</script>