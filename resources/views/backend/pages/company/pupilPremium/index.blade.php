<style type="text/css">
   .search-input{
    position: absolute;
    margin-top: 6px;
   } 
</style>

<div class="card-body">
    <div class="row">
        <div class="col-md-12" style="margin: 24px 1px 0 0;">
            <div class="form-group col-md-4" style="float:left;">
                <label><b>Live/Archive Filters</b></label>
                <div class="clearfix"></div>
                <label class="custom-control custom-radio signin_radio1 d-inline-block pr-3" style="margin-left: 10px;">
        <input id="move_radio" name="search_block" value="Live" type="radio" class="custom-control-input search-input">
        <span class="custom-control-label"></span>
        <span class="custom-control-description">Live</span>
    </label>
    <label class="custom-control custom-radio signin_radio1 d-inline-block pr-3" style="margin-left: 10px;">
        <input id="move_radio" name="search_block" value="Archive" type="radio" class="custom-control-input search-input">
        <span class="custom-control-label"></span>
        <span class="custom-control-description">Archive</span>
    </label>
      <label class="custom-control custom-radio signin_radio1 d-inline-block pr-3" style="margin-left: 10px;">
        <input id="move_radio" checked name="search_block" value="All" type="radio" class="custom-control-input search-input">
        <span class="custom-control-label"></span>
        <span class="custom-control-description">All</span>
    </label>
               <!--  <label><input type="radio" name="search_block" class="search-input" value="Live"><span style="padding: 0 0px 0 15px;">Live</span></label>
                <label><input name="search_block" class="search-input" value="Archive" type="radio"><span style="padding:0 0px 0 15px;">Archive</span></label>
                <label><input checked type="radio" value="All" class="search-input" name="search_block"><span style="padding:0 0px 0 15px;">All</span></label> -->
                 
                <!-- </div> -->


            </div>
            <div class="form-group col-md-3" style="float:left;">
                <label><b>Status Filters</b></label>
                <div class="clearfix"></div>
                  <label class="custom-control custom-radio signin_radio1 d-inline-block pr-3" style="margin-left: 10px;">
        <input id="move_radio" name="status_filter" value="Booked" type="radio" class="custom-control-input search-input">
        <span class="custom-control-label"></span>
        <span class="custom-control-description">Booked</span>
    </label>
     <label class="custom-control custom-radio signin_radio1 d-inline-block pr-3" style="margin-left: 10px;">
        <input id="move_radio" name="status_filter" value="Lost" type="radio" class="custom-control-input search-input">
        <span class="custom-control-label"></span>
        <span class="custom-control-description">Lost</span>
    </label>
                <!--  <label><input type="radio" name="status_filter" class="search-input" value="Booked"><span style="padding: 0 0px 0 15px;">Booked</span></label>
                  <label><input type="radio" name="status_filter" class="search-input" value="Lost"><span style="padding: 0 0px 0 15px;">Lost</span></label> -->
            </div>
            <div class="col-md-3" style="float:left;">
                <label><b>Select Date</b></label>
                <div class="clearfix"></div>
                 <input type="text"  id="mid_date" class="form-control" placeholder="Select Date" >
            </div>
            <div class="form-group col-md-2 " style="float: right;">
                <label></label>
                <a href="#create_pupil_premium" class="input_create_pupil_premium_empty btn btn-primary create_pupil_premium_btn" data-toggle="modal" id=""
                     style="margin-top: 30px;"> Add</a>
            </div>
            {{--<div class="form-group" style="padding-right: 9.5rem !important;" id="toggle_switch">--}}
                {{--<div class="radio_basic_swithes_padbott text-right ">--}}
                    {{--<input type="checkbox" class="js-switch sm_toggle_company_voucher_assign" name="send_voucher_code" id="send_voucher_code"--}}
                            {{--/>--}}
                    {{--<span class="radio_switchery_padding">Send Voucher Code  </span>--}}
                    {{--<br />--}}
                {{--</div>--}}
            {{--</div>--}}

        </div>
        
    </div>
    <br>
    <div class="row" style="width: 100% !important;">
        <div class="col-md-12">
            <table class="table  table-bordered table-hover" id="pupilPremiumTable" style="width: 100% !important;">
                <thead>
                    <tr>
                        <th>Booking ID</th>
                        <th>Name</th>
                        <th>Block</th>
                        <th>Locker Number</th>
                        <th>Start</th>
                        <th>End</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript" src="/backend/js/orders/choose_locker_order.js"></script>
 <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk="   crossorigin="anonymous"></script>

<script type="text/javascript">
     $(document).ready(function() {

$('#mid_date').datepicker()
    .on("input change", function (e) {
        pupilPremiumTable();
    // console.log("Date changed: ", e.target.value);
});
            // $("#mid_date").datepicker();
        });

    $('body').on('click','.move_booking_btn',function(e){

        $('#edit_pupil_premium_first_name').val($(this).data('child_first_name'));
        $('#edit_pupil_premium_surname').val($(this).data('child_surname'));

    });

    function moveBooking(sale_id,booking_id){

        // alert(sale_id);
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            url: '/admin/edit/student/getBlocks',
            type: 'POST',
            dataType: 'JSON',
            cache: false,
            data: {sale_id: sale_id,booking_id: booking_id, _token: CSRF_TOKEN},
            success: function (data) {
                // console.log(data);
                $('#display_blocks').html(data.blocks);
                if(data.hold_next_year == true){
                    console.log(data.hold_next_year,'data.hold_next_year')
                    $('#hold_next_year').prop('checked', true);
                    $('#booking_id_current').val( data.booking_id);
                    $('#current_child_sale_id').val(data.current_child_sale_id);

                    $('#sale_id_next_year').val(  data.hold_next_year_sale_id);
                }else{
                    $('#hold_next_year').prop('checked', false);
                    $('#booking_id_current').val( data.booking_id);
                    $('#current_child_sale_id').val(data.current_child_sale_id);

                    $('#sale_id_next_year').val(  data.hold_next_year_sale_id);
                }
                if(data.move_checked == true){

                    $('#move_checked').prop('checked', true);
                }else{
                    $('#move_checked').prop('checked', false);
                }
                sessionStorage.removeItem("selected_dropdown_current_tab");
                sessionStorage.removeItem("selected_dropdown_start");
                sessionStorage.removeItem("selected_dropdown_end");
                sessionStorage.removeItem("selected_dropdown_rate");
                sessionStorage.removeItem("selected_dropdown_tax");
                sessionStorage.removeItem("selected_dropdown_block_id");
                sessionStorage.removeItem("selected_dropdown_sale_id");
                sessionStorage.removeItem("selected_dropdown_counter");
                sessionStorage.removeItem("selected_dropdown_hold_locker");
                sessionStorage.removeItem("selected_lockers");
                $(".blocks").change();

                var locker_ids = data.locker_ids;
                console.log(locker_ids);
                var locker_ids_array = [];
                $.each(locker_ids, function( index, value ) {

                    if(value !== 'null')
                        locker_ids_array.push(value);
                    else
                        locker_ids_array.push(null);
                });
                sessionStorage.setItem("selected_dropdown_current_tab", 0);
                sessionStorage.setItem("selected_lockers", JSON.stringify(locker_ids_array));

                var storedArray = JSON.parse(sessionStorage.getItem("selected_lockers"));
                // console.log(storedArray,'storedArray');

                getNewLockers();
                $('.child-name').css("background-color", "#00bf86");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            }
        });
    }

    $('body').on('click', '.choose-locker', function (e) {

        var counter = $(this).data('counter');

        $('#tabValidation'+ counter).addClass('d-none');
        $('#locker_already_exists_msg'+counter).addClass('d-none');

        var sale_id = $(this).data('sale_id');

        var block_id = $(this).data('block_id');
        var locker_id = $(this).data('locker_id');
        var locker_number = $(this).data('locker_number');

        var rate = $(this).data('rate');
        var tax = $(this).data('tax');
        var hold_locker = $(this).data('hold_locker');


        var index_of_sale_ids = sessionStorage.getItem("selected_dropdown_current_tab");
        console.log(index_of_sale_ids,'index_of_sale_ids')

        // sessionStorage.removeItem("selected_lockers");
        var storedArray = JSON.parse(sessionStorage.getItem("selected_lockers"));
        console.log(storedArray,'storedArray')
        var testArray = [];
        var i;

        if (storedArray != null) {
            for (i = 0; i < storedArray.length; i++) {
                testArray[i] = storedArray[i];
            }
        }

        testArray[index_of_sale_ids] = locker_id;
        console.log(typeof  testArray ,'testArray');

        sessionStorage.setItem("selected_lockers", JSON.stringify(testArray));
        var selected_lockers = JSON.parse(sessionStorage.getItem("selected_lockers"));

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.ajax({

            url: '/admin/edit/student/updateSale',

            type: 'POST',

            dataType: 'JSON',

            cache: false,

            data: {
                sale_id: sale_id,
                block_id: block_id,
                locker_id: locker_id,
                locker_number: locker_number,
                rate: rate,
                tax: tax,
                hold_locker: hold_locker,
                _token: CSRF_TOKEN
            },

            success: function (data) {

                if(data == 'home_page'){
                    document.location.href = "/";
                }
                if(data.status == 'already_exists'){
                    $('#locker_already_exists_msg'+counter).removeClass('d-none');
                    const  selected_lockers = JSON.parse(sessionStorage.getItem("selected_lockers"));

                    var  locker_id = parseInt(data.locker_id);

                    const index = selected_lockers.indexOf(locker_id);
                    if (index > -1) {
                        selected_lockers.splice(index, 1);
                    }
                    sessionStorage.setItem("selected_lockers", JSON.stringify(selected_lockers));
                    var reomoved_locker =  JSON.parse(sessionStorage.getItem("selected_lockers"));

                }else if(data.status == 'available'){
                    // $('.nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link').css("color", "black")
                }
                getNewLockers();

            },

            error: function (jqXHR, textStatus, errorThrown) {



            }

        });


    });

    $('body').on('click','.create_pupil_premium_btn',function(e){


        var company_id = $('#company_id').val();
        $('.pupil_premium_error').hide();
        $('#blade-html').html('');
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');


        $.ajax({
            url: '/admin/company/pupil/premium/get/franchise',
            type: 'POST',
            dataType: 'JSON',
            cache: false,
            data: { company_id: company_id,
                _token: CSRF_TOKEN},
            success: function (response) {
                // console.log(response);
                var users ='';
                var years='';
                var r_users=response.users; 
                var r_years=response.years;
                users+='<option value="">Assign To</option>';

                $.each(r_users ,function (index,val) {
                    users+='<option value="'+ val['id']+'">'+ val['full_name']+'</option>';
                });
                years+='<option value="">Choose School Academic Year</option>';
                    $.each(r_years ,function (index,val) {
                        // console.log(val);
                        // console.log('val testing');

                    years+='<option value="'+ val['school_academic_year']['id']+'">'+ val['school_academic_year']['school_year']+'</option>';
                });
             $('#pupil_premium_user_id').html(users);
             $('#pupil_premium_school_academic_year_id').html(years);
             
                    pupilPremiumTable();
            },
            error: function (xhr, detail) {

                $('.assign_code_error').show();
                $('button#assign_vouchers_btn').prop('disabled', false);
                console.log(xhr.responseJSON.errors)
                console.log(xhr.responseJSON.errors.assignee_name)

                if(xhr.responseJSON.errors.assignee_name){


                    $('#assignee_name_error').show()
                    $('#assignee_name_error').html('<span>'+ xhr.responseJSON.errors.assignee_name[0] + '</span>');
                }
                else{
                    $('#assignee_name_error').hide();
                }
                if(xhr.responseJSON.errors.assignee_email){
                    $('#assignee_email_error').show();
                    $('#assignee_email_error').html('<span>'+ xhr.responseJSON.errors.assignee_email[0] + '</span>');
                }
                else{
                    $('#assignee_email_error').hide();
                }

                if(xhr.responseJSON.errors.assignee_message){
                    $('#assignee_message_error').show()
                    $('#assignee_message_error').html('<span>'+ xhr.responseJSON.errors.assignee_message[0] + '</span>');
                }
                else{
                    $('#assignee_message_error').hide();
                }


            }
        });

    });

    $('.search-input').click(function(){
       pupilPremiumTable();
    });
    pupilPremiumTable();

    function pupilPremiumTable() {

        $('#pupilPremiumTable').dataTable().fnClearTable();
        $('#pupilPremiumTable').dataTable().fnDestroy();

        return $('#pupilPremiumTable').DataTable({
            initComplete : function() {
                $("#pupilPremiumTable_wrapper>.row").attr('style','width:100%;');
                $("#pupilPremiumTable_filter>label").css('float', 'right');
                $("#pupilPremiumTable_length>label").css('float', 'left');
            },
            "order":[[0,'desc'],[1,'asc']],

            language: {
                'lengthMenu': '_MENU_',
                "search": '',
                "searchPlaceholder": "Search..."
            },

            "autoWidth": false,

            "columnDefs": [
                { "width": "210px", "targets": 0 },
                { "width": "210px", "targets": 1 },
                { "width": "330px", "targets": 2 },
                { "width": "210px", "targets": 3 },

            ],

            ajax: {
                url: '<?php echo e(route('company.pupil.premium.show.all')); ?>',
                data: function (d) {
                    // alert($('#mid_date').val());
                    d.company_id = $('#company_id').val();
                    d.searchParam = $('input[name="search_block"]:checked').val();
                    d.searchStatus = $('input[name="status_filter"]:checked').val();
                    d.searchDate = $('#mid_date').val();



                }
            },
            columns: [
                {data: 'booking_id', name: 'booking_id'},
                {data: 'childName', name: 'childName'},
                {data: 'block_name', name: 'block_name'},
                {data: 'locker_number', name: 'locker_number'},
                {data: 'start', name: 'start'},
                {data: 'end', name: 'end'},
                {data: 'action', name: 'action', orderable: false, searchable: true}

            ],

        });

    }




</script>