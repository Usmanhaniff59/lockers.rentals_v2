@extends('backend.layouts.app')

@section('title', 'Lockers | Errors')
@section('style')
    <!--plugin styles-->
    <link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css" />

    <link type="text/css" rel="stylesheet" href="/backend/vendors/inputlimiter/css/jquery.inputlimiter.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/chosen/css/chosen.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/jquery-tagsinput/css/jquery.tagsinput.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/daterangepicker/css/daterangepicker.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datepicker/css/bootstrap-datepicker.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-switch/css/bootstrap-switch.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/multiselect/css/multi-select.css"/>

    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-switch/css/bootstrap-switch.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/switchery/css/switchery.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/radio_css/css/radiobox.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/checkbox_css/css/checkbox.min.css" />
    <!--End of Plugin styles-->
    <!--Page level styles-->

    <!-- end of plugin styles -->
    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/form_elements.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/radio_checkbox.css" />

    <link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css" />
    <style>

        .page-link{
            padding: 16px;
            color: #525252;
            text-decoration: none;
            background-color: transparent;
        }
        .header_icon_margin{
            margin-right: 10px !important;
        }
        .header_icon_margin_left{
            margin-left: 10px !important;
        }
        .button-style{
            margin: 3px 2px;
            font-size: 12px;
            padding: 4px 6px;
        }

    </style>
@endsection
@section('content')
    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fas fa-user circle header_icon_margin"></i>
                            Company Errors
                        </h4>
                    </div>
                </div>
            </div>
        </header>



        @include('errors.success')
        @include('errors.error')

        @if(Session::has('flash_message'))
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable float-right" style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-hidden="true" style="margin-top: 4px; margin-left: 15px;"> ×
                    </button>
                    {{ Session::get('flash_message') }}
                </div>
            </div>
        @endif

        {{--@if ($errors->any())--}}
            {{--<div class="alert alert-danger">--}}
                {{--<ul>--}}
                    {{--@foreach ($errors->all() as $error)--}}
                        {{--<li>{{ $error }}</li>--}}
                    {{--@endforeach--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--@endif--}}

        <div class="outer">
            <div class="inner bg-container">
                <div class="row">
                    <div class="col-12 data_tables">

                        <div class="card m-t-35">
                            {{--<div class="card-header bg-white">--}}
                                {{--<i class="fas fa-user circle header_icon_margin"></i> All Users--}}
                                {{--@can('Create User')--}}
                                    {{--<a href="#user-create-modal" class="btn btn-primary pull-right btn-md adv_cust_mod_btn header_icon_margin_left" data-toggle="modal" data-href="#user-create-modal" onclick="reset_form('user-save')" >Add New User</a>--}}
                                {{--@endcan--}}
                            {{--</div>--}}
                            <div class="card-body p-t-10">
                                <div class=" m-t-25">
                                    <table class="table table-striped table-bordered table-hover " id="company_error">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Company</th>
                                                <th>Block</th>
                                                <th>Type</th>
                                                <th>Details</th>
                                                <th></th>
                                            </tr>
                                        </thead>

                                    </table>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{--</div>--}}
            </div>
        </div>

        <!--- Move booking model -->
        <div class="modal fade in display_none" id="move_booking" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    {{ Form::open(array('url' => 'admin/refundPayment','id'=>'refund_form')) }}
                    <div class="modal-body">

                        <h3><i class="fa fa-money circle"></i> Move Booking</h3>

                        <div id="display_blocks"></div>

                    </div>
                    <div class="modal-footer float-left">
                        <div class="checkbox ">
                            <label>
                                <input type="checkbox" value="" id="hold_next_year">
                                <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                Hold this locker to be booked while my child is at the school. You will be given the opportunity to confirm this booking in April.
                            </label>
                            <br>
                            <label>
                                <input type="checkbox" value="" id="move_checked">
                                <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                Done
                            </label>
                        </div>

                        {{--<button type="button" data-dismiss="modal" class="btn btn-light">Close</button>--}}
                        {{--{{ Form::Button('save', array('class' => 'btn btn-primary','id'=>'move_booking')) }}--}}
                    </div>
                    {{ Form::close() }}
                    <br>
                </div>
            </div>
        </div>
        <!-- END Move booking modal-->

    </div>
@endsection
@section('script')
    <!--plugin scripts-->
    <script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <!-- end of plugin scripts -->

    <script type="text/javascript" src="/backend/vendors/jquery.uniform/js/jquery.uniform.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputlimiter/js/jquery.inputlimiter.js"></script>
    <script type="text/javascript" src="/backend/vendors/chosen/js/chosen.jquery.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/jquery-tagsinput/js/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.validVal.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/moment/js/moment.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/daterangepicker/js/daterangepicker.js"></script>
    <script type="text/javascript" src="/backend/vendors/datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/autosize/js/jquery.autosize.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputmask/js/inputmask.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputmask/js/jquery.inputmask.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputmask/js/inputmask.date.extensions.js"></script>
    <script type="text/javascript" src="/backend/vendors/inputmask/js/inputmask.extensions.js"></script>
    <script type="text/javascript" src="/backend/vendors/multiselect/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.quicksearch/2.3.1/jquery.quicksearch.min.js"></script>

    <script type="text/javascript" src="/backend/vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>
    <!--End of plugin scripts-->
    <!--Page level scripts-->

    <!--end of plugin scripts-->
    <!--Page level scripts-->
    <script type="text/javascript" src="/backend/js/pages/radio_checkbox.js"></script>
    <script type="text/javascript" src="/backend/js/form.js"></script>
    <script type="text/javascript" src="/backend/js/pages/form_elements.js"></script>
    <script type="text/javascript" src="/backend/js/pages/datatable_server_side.js"></script>
    <script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>
    <script src="{{ asset('custom/general.js') }}"></script>


    {{--            <script type="text/javascript" src="/backend/js/pages/radio_checkbox.js"></script>--}}

    <script type="text/javascript" src="/backend/js/common.js"></script>
    <script type="text/javascript" src="/backend/js/orders/choose_locker_order.js"></script>

    <!-- end of global scripts-->

    <script type="text/javascript">
        // function reset_form(form_id) {
        //
        //     $("#user-save")[0].reset();
        //
        // }
        function moveBooking(booking_id){
            // alert(booking_id);
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                url: '/admin/payment/getBlocks',
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {booking_id: booking_id, _token: CSRF_TOKEN},
                success: function (data) {
                    // console.log(data);
                    $('#display_blocks').html(data.blocks);
                    if(data.hold_next_year == true){
                        // console.log(data.hold_next_year,'data.hold_next_year')
                        $('#hold_next_year').prop('checked', true);
                    }else{
                        $('#hold_next_year').prop('checked', false);
                    }
                    if(data.move_checked == true){

                        $('#move_checked').prop('checked', true);
                    }else{
                        $('#move_checked').prop('checked', false);
                    }

                    sessionStorage.removeItem("selected_dropdown_current_tab");
                    sessionStorage.removeItem("selected_dropdown_start");
                    sessionStorage.removeItem("selected_dropdown_end");
                    sessionStorage.removeItem("selected_dropdown_rate");
                    sessionStorage.removeItem("selected_dropdown_tax");
                    sessionStorage.removeItem("selected_dropdown_block_id");
                    sessionStorage.removeItem("selected_dropdown_sale_id");
                    sessionStorage.removeItem("selected_dropdown_counter");
                    sessionStorage.removeItem("selected_dropdown_hold_locker");
                    sessionStorage.removeItem("selected_lockers");

                    $(".blocks").change();

                    var locker_ids = data.locker_ids;
                    // console.log(locker_ids);
                    var locker_ids_array = [];
                    $.each(locker_ids, function( index, value ) {

                        if(value !== 'null')
                            locker_ids_array.push(value);
                        else
                            locker_ids_array.push(null);
                    });
                    sessionStorage.setItem("selected_dropdown_current_tab", 0);
                    sessionStorage.setItem("selected_lockers", JSON.stringify(locker_ids_array));

                    var storedArray = JSON.parse(sessionStorage.getItem("selected_lockers"));
                    // console.log(storedArray,'storedArray');

                    getNewLockers();
                    $('.child-name').css("background-color", "#00bf86");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // console.log(jqXHR);
                }
            });
        }

        function resolveError(id){
            // alert(id);
            if (window.confirm("Are you sure to change Resolved?")) {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                $.ajax({
                    url: '/admin/company/errors/resolved',
                    type: 'POST',
                    dataType: 'JSON',
                    cache: false,
                    data: {id: id, _token: CSRF_TOKEN},
                    success: function (data) {
                        console.log(data);
                        errorEntryTable();
                        // $('#move_booking').modal('toggle');
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR);
                    }
                });
            }
        }

        $('body').on('click', '.choose-locker', function (e) {

            var counter = $(this).data('counter');

            $('#tabValidation'+ counter).addClass('d-none');
            $('#locker_already_exists_msg'+counter).addClass('d-none');

            var sale_id = $(this).data('sale_id');

            var block_id = $(this).data('block_id');
            var locker_id = $(this).data('locker_id');
            var locker_number = $(this).data('locker_number');

            var rate = $(this).data('rate');
            var tax = $(this).data('tax');
            var hold_locker = $(this).data('hold_locker');


            var index_of_sale_ids = sessionStorage.getItem("selected_dropdown_current_tab");
            console.log(index_of_sale_ids,'index_of_sale_ids')

            // sessionStorage.removeItem("selected_lockers");
            var storedArray = JSON.parse(sessionStorage.getItem("selected_lockers"));
            console.log(storedArray,'storedArray')
            var testArray = [];
            var i;

            if (storedArray != null) {
                for (i = 0; i < storedArray.length; i++) {
                    testArray[i] = storedArray[i];
                }
            }

            testArray[index_of_sale_ids] = locker_id;
            console.log(typeof  testArray ,'testArray');

            sessionStorage.setItem("selected_lockers", JSON.stringify(testArray));
            var selected_lockers = JSON.parse(sessionStorage.getItem("selected_lockers"));

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({

                url: '/admin/payment/updateSale',

                type: 'POST',

                dataType: 'JSON',

                cache: false,

                data: {
                    sale_id: sale_id,
                    block_id: block_id,
                    locker_id: locker_id,
                    locker_number: locker_number,
                    rate: rate,
                    tax: tax,
                    hold_locker: hold_locker,
                    _token: CSRF_TOKEN
                },

                success: function (data) {

                    if(data == 'home_page'){
                        document.location.href = "/";
                    }
                    if(data.status == 'already_exists'){
                        $('#locker_already_exists_msg'+counter).removeClass('d-none');
                        const  selected_lockers = JSON.parse(sessionStorage.getItem("selected_lockers"));

                        var  locker_id = parseInt(data.locker_id);

                        const index = selected_lockers.indexOf(locker_id);
                        if (index > -1) {
                            selected_lockers.splice(index, 1);
                        }
                        sessionStorage.setItem("selected_lockers", JSON.stringify(selected_lockers));
                        var reomoved_locker =  JSON.parse(sessionStorage.getItem("selected_lockers"));

                    }else if(data.status == 'available'){
                        $('.nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link').css("color", "black")
                    }
                    getNewLockers();

                },

                error: function (jqXHR, textStatus, errorThrown) {



                }

            });


        });

        $('body').on('click', '#hold_next_year', function (e) {


            var checkbox_next = $('#hold_next_year').prop('checked');

            var booking_id = $('#sale_id').val();
            alert(booking_id)

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({

                url: '/admin/payment/updateNextYearBooking',

                type: 'POST',
                dataType: 'JSON',
                cache: false,

                data: {
                    booking_id: booking_id,
                    checkbox_next: checkbox_next,
                    _token: CSRF_TOKEN
                },

                success: function (data) {

                    console.log(data);
                    getNewLockers();

                },

                error: function (jqXHR, textStatus, errorThrown) {



                }

            });


        });

        $('body').on('click', '#move_checked', function (e) {


            var checkbox_next = $('#move_checked').prop('checked');

            var booking_id = $('#sale_id').val();
            // alert(booking_id)

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({

                url: '/admin/payment/updateCheckedStatus',

                type: 'POST',
                dataType: 'JSON',
                cache: false,

                data: {
                    booking_id: booking_id,
                    checkbox_next: checkbox_next,
                    _token: CSRF_TOKEN
                },

                success: function (data) {

                    console.log(data);
                    errorEntryTable()
                    $('#move_booking').modal('toggle');

                },

                error: function (jqXHR, textStatus, errorThrown) {



                }

            });


        });

        $('body').on('click', '#clear_all_checked', function (e) {



            // alert(booking_id)
            if (confirm("Are you sure to clear all checked?")) {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                $.ajax({

                    url: '/admin/payment/clearAllChecked',

                    type: 'POST',
                    dataType: 'JSON',
                    cache: false,
                    data: {
                        _token: CSRF_TOKEN
                    },

                    success: function (data) {

                        paymentEntryTable()

                    },

                    error: function (jqXHR, textStatus, errorThrown) {


                    }

                });
            }


        });

        $('body').on('click', '.choose-locker', function (e) {

            var counter = $(this).data('counter');

            $('#tabValidation'+ counter).addClass('d-none');
            $('#locker_already_exists_msg'+counter).addClass('d-none');

            var sale_id = $(this).data('sale_id');

            var block_id = $(this).data('block_id');
            var locker_id = $(this).data('locker_id');
            var locker_number = $(this).data('locker_number');

            var rate = $(this).data('rate');
            var tax = $(this).data('tax');
            var hold_locker = $(this).data('hold_locker');


            var index_of_sale_ids = sessionStorage.getItem("selected_dropdown_current_tab");
            console.log(index_of_sale_ids,'index_of_sale_ids')

            // sessionStorage.removeItem("selected_lockers");
            var storedArray = JSON.parse(sessionStorage.getItem("selected_lockers"));
            console.log(storedArray,'storedArray')
            var testArray = [];
            var i;

            if (storedArray != null) {
                for (i = 0; i < storedArray.length; i++) {
                    testArray[i] = storedArray[i];
                }
            }

            testArray[index_of_sale_ids] = locker_id;
            console.log(typeof  testArray ,'testArray');

            sessionStorage.setItem("selected_lockers", JSON.stringify(testArray));
            var selected_lockers = JSON.parse(sessionStorage.getItem("selected_lockers"));

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({

                url: '/admin/payment/updateSale',

                type: 'POST',

                dataType: 'JSON',

                cache: false,

                data: {
                    sale_id: sale_id,
                    block_id: block_id,
                    locker_id: locker_id,
                    locker_number: locker_number,
                    rate: rate,
                    tax: tax,
                    hold_locker: hold_locker,
                    _token: CSRF_TOKEN
                },

                success: function (data) {

                    if(data == 'home_page'){
                        document.location.href = "/";
                    }
                    if(data.status == 'already_exists'){
                        $('#locker_already_exists_msg'+counter).removeClass('d-none');
                        const  selected_lockers = JSON.parse(sessionStorage.getItem("selected_lockers"));

                        var  locker_id = parseInt(data.locker_id);

                        const index = selected_lockers.indexOf(locker_id);
                        if (index > -1) {
                            selected_lockers.splice(index, 1);
                        }
                        sessionStorage.setItem("selected_lockers", JSON.stringify(selected_lockers));
                        var reomoved_locker =  JSON.parse(sessionStorage.getItem("selected_lockers"));

                    }else if(data.status == 'available'){
                        $('.nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link').css("color", "black")
                    }
                    getNewLockers();

                },

                error: function (jqXHR, textStatus, errorThrown) {



                }

            });


        });

        var errors_table =  errorEntryTable();

        function errorEntryTable() {

            $('#company_error').dataTable().fnClearTable();
            $('#company_error').dataTable().fnDestroy();

            return $('#company_error').DataTable({

                responsive: true,
                serverSide: true,
                stateSave: true,
                processing: true,
                searching: false,
                columnDefs: [
                    {   "targets": [0],
                        "visible": false,
                        "searchable": false
                    }
                ],
                "pageLength": 10,
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                "oLanguage": {
                    "sLengthMenu": "_MENU_",
                    "sSearch": "_INPUT_"
                },
                "aTargets": ["_all"],
                "order": [[ 0, "desc" ]],

                initComplete: function () {
                    $("#company_error_wrapper>.row table").attr('style', 'width:100%;');
                    $("#company_error_filter>label").css('float', 'right');
                    $("#company_error_length>label").css('float', 'left');
                },

                ajax: '<?php echo route('company.errors.show.all'); ?>',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'company', name: 'company'},
                    {data: 'block', name: 'block'},
                    {data: 'error_type', name: 'error_type'},
                    {data: 'details', name: 'details'},
                    {data: 'action', name: 'action', orderable: false, searchable: true}
                ]
            });

        }

    </script>
@endsection