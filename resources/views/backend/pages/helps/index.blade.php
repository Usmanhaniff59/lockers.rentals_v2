@extends('backend.layouts.app')

@section('title', 'Lockers | Helps')
@section('style')
    <!--plugin styles-->
    <link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css" />
    <!-- end of plugin styles -->
    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css" />
@endsection
@section('content')
    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-question-circle-o circle"></i>
                            Help Administration
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        <div class="outer">
            <div class="inner bg-container">
                <div class="row">
                    <div class="col-12 data_tables">

                        <div class="card m-t-35">
                            <div class="card-header bg-white">
                                <i class="fa fa-question-circle circle"></i> All Helps  @can('Create Help')
                                {{--<a href="{{ route('helps.create') }}" class="btn btn-primary" role="button">Add New Help</a>--}}
                                 <a href="#create" class="btn btn-primary btn-md pull-right adv_cust_mod_btn" data-toggle="modal" data-href="#create" >Add New Help</a>
                                @endcan
                            </div>
                            <div class="card-body p-t-10">
                                <div class=" m-t-25">
                                    <table class="table table-striped table-bordered table-hover " id="sample_6">
                                        <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>URL</th>
                                            <th>Operations</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($helps)
                                           @foreach ($helps as $help)
                                            <tr>

                                                <td>{{ $help->id }} {{ $help->title }}</td>
                                                <td>{{ $help->page_url }}</td>
                                                <td>
                                                    @can('Edit Help')
                                                        <a href="#edit" class="btn btn-primary btn-md adv_cust_mod_btn edit-btn pull-left" data-toggle="modal" style="margin-right: 3px;" data-href="#edit" data-id="{{ $help->id }}" data-otitle="{{ $help->title }}" data-detail="{{ $help->detail }}" data-page_url="{{ $help->page_url }}" >Edit</a>
                                                    @endcan
                                                        @can('Delete Help')
                                                            {!! Form::open(['method' => 'DELETE', 'route' => ['helps.destroy', $help->id] ]) !!}
                                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                                            {!! Form::close() !!}
                                                        @endcan
                                                </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!--- create model -->
    <div class="modal fade in display_none" id="create" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                {{ Form::open(array('url' => 'admin/helps')) }}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <h3><i class="fa fa-plus-circle circle"></i> Add Help</h3>
                            <hr>
                            <br>

                            <div class="form-group">
                                {{ Form::label('title', 'Title') }}
                                {{ Form::text('title', '', array('class' => 'form-control')) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('detail', 'Detail') }}
                                {{ Form::textarea('detail', '', array('class' => 'form-control')) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('page_url', 'Page Url') }}<br>
                                {{ Form::text('page_url','', array('class' => 'form-control')) }}

                            </div>

                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                    {{ Form::submit('Create Help', array('class' => 'btn btn-primary')) }}
                </div>
                {{ Form::close() }}
                <br>
            </div>
        </div>
    </div>
    <!-- END create modal-->

    <!--- Edit model -->
    <div class="modal fade in display_none" id="edit" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                {{ Form::open(array('route' => array('helps.update', 1), 'method' => 'PUT')) }}
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <h3 id="edit-title"></h3>
                                <hr>
                                <br>
                                {{ Form::hidden('id', null, array('class' => 'form-control', 'id' => 'id_edit')) }}

                                <div class="form-group">
                                    {{ Form::label('title', 'Title') }}
                                    {{ Form::text('title', null, array('class' => 'form-control', 'id' => 'title_edit')) }}
                                </div>

                                <div class="form-group">
                                    {{ Form::label('detail', 'Detail') }}
                                    {{ Form::textarea('detail', null, array('class' => 'form-control', 'id' => 'detail_edit')) }}
                                </div>

                                <div class="form-group">
                                    {{ Form::label('page_url', 'Page Url') }}<br>
                                    {{ Form::text('page_url',null, array('class' => 'form-control', 'id' => 'page_url_edit')) }}

                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                        {{ Form::submit('Edit Help', array('class' => 'btn btn-primary  ')) }}
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!-- END Edit model-->

@endsection
@section('script')
    <!--plugin scripts-->
    <script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <!-- end of plugin scripts -->
    <!--Page level scripts-->
    <script type="text/javascript" src="/backend/js/pages/datatable.js"></script>
    <!-- end of global scripts-->
    <script type="text/javascript">

        $('body').on('click','.edit-btn',function(){

            $('#edit-title').html('<i class="fa fa-pencil circle"></i>  Edit '+ $(this).data('otitle'));
            $('#id_edit').val($(this).data('id'));
            $('#title_edit').val($(this).data('otitle'));
            $('#detail_edit').val($(this).data('detail'));
            var pagurl = $(this).data('page_url');
            $('#page_url_edit').val(pagurl);

        });

    </script>
@endsection