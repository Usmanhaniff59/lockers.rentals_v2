@extends('backend.layouts.app')

@section('title', 'Lockers | Add Help')

@section('content')

    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-user-plus"></i>
                            Add Help
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        <div class="outer">
            <div class="inner bg-container">
                <!--top section widgets-->
                <div class="row widget_countup">
                    <div class="col-12">

                        <hr>

                        {{-- @include ('errors.list') --}}

                        {{ Form::open(array('url' => 'admin/helps')) }}

                        <div class="form-group">
                            {{ Form::label('title', 'Title') }}
                            {{ Form::text('title', '', array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('detail', 'Detail') }}
                            {{ Form::textarea('detail', '', array('class' => 'form-control')) }}
                        </div>


                        <div class="form-group">
                            {{ Form::label('page_url', 'Page Url') }}<br>
                            {{ Form::text('page_url','', array('class' => 'form-control')) }}

                        </div>

                        {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}

                        {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection