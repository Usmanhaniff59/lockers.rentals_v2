@extends('backend.layouts.app')

@section('title', 'Lockers | Edit Help')

@section('content')

    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-user-plus"></i>
                            Edit {{$help->name}}
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        <div class="outer">
            <div class="inner bg-container">
                <!--top section widgets-->
                <div class="row widget_countup">
                    <div class="col-12">

                        <hr>
                        {{-- @include ('errors.list') --}}

                        {{ Form::model($help, array('route' => array('helps.update', $help->id), 'method' => 'PUT')) }} {{-- Form model binding to automatically populate our fields with user data --}}

                        <div class="form-group">
                            {{ Form::label('title', 'Title') }}
                            {{ Form::text('title', null, array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('detail', 'Detail') }}
                            {{ Form::textarea('detail', null, array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('page_url', 'Page Url') }}<br>
                            {{ Form::text('page_url',null, array('class' => 'form-control')) }}

                        </div>

                        {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

                        {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection