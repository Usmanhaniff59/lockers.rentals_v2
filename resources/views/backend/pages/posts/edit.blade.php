@extends('backend.layouts.app')
@section('style')
    <link type="text/css" rel="stylesheet" media="screen" href="backend/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css"/>
@endsection

@section('title', 'Lockers | Edit Post')

@section('content')
    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-pencil"></i>
                            Edit Post
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        <div class="outer">
            <div class="inner bg-container">


                <!--top section widgets-->
                <div class="row widget_countup">
                    <div class="col-12">

        <hr>
        @include ('errors.list')
            {{ Form::model($post, array('route' => array('posts.update', $post->id), 'method' => 'PUT')) }}
            <div class="form-group">
            {{ Form::label('title', 'Title') }}
            {{ Form::text('title', null, array('class' => 'form-control')) }}<br>

            {{ Form::label('body', 'Post Body') }}
            {{ Form::textarea('body', null, array('class' => 'form-control textarea form_editors_textarea_wysihtml')) }}<br>
                <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
            {{ Form::submit('Save', array('class' => 'btn btn-success')) }}

            {{ Form::close() }}
    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript" src="backend/vendors/bootstrap3-wysihtml5-bower/js/bootstrap3-wysihtml5.all.min.js"></script>
    <script type="text/javascript" src="backend/js/pluginjs/bootstrap3_wysihtml5.js"></script>
    <script type="text/javascript" src="backend/js/pages/form_editors.js"></script>
@endsection