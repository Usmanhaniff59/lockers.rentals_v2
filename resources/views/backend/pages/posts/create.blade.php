@extends('backend.layouts.app')
@section('style')
    <link type="text/css" rel="stylesheet" media="screen" href="/backend/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/summernote/css/summernote.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/form_elements.css"/>
@endsection
@section('title', 'Lockers | Post')

@section('content')
    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-pencil"></i>
                            Create New Post
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        <div class="outer">
            <div class="inner bg-container">


                <!--top section widgets-->
                <div class="row widget_countup">
                    <div class="col-12">


        {{-- @include ('errors.list') --}}

        {{-- Using the Laravel HTML Form Collective to create our form --}}
        {{ Form::open(array('route' => 'posts.store')) }}

        <div class="form-group">
            {{ Form::label('title', 'Title') }}
            {{ Form::text('title', null, array('class' => 'form-control')) }}
            <br>

            {{ Form::label('body', 'Post Body') }}
            {{ Form::textarea('body', null, array('class' => 'form-control textarea form_editors_textarea_wysihtml', 'id'=>'details_id')) }}
            <br>
            <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
            {{ Form::submit('Create Post', array('class' => 'btn btn-success  ')) }}
            {{ Form::close() }}
        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript" src="/backend/vendors/tinymce/js/tinymce.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap3-wysihtml5-bower/js/bootstrap3-wysihtml5.all.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/bootstrap3_wysihtml5.js"></script>
    <script type="text/javascript" src="/backend/vendors/summernote/js/summernote.min.js"></script>
    <script type="text/javascript" src="/backend/js/pages/form_editors.js"></script>
@endsection