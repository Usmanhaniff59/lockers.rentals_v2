@extends('backend.layouts.app')

@section('title', 'Lockers | Sliders')
@section('style')
    <!--plugin styles-->
    <link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-switch/css/bootstrap-switch.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/switchery/css/switchery.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/radio_css/css/radiobox.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/checkbox_css/css/checkbox.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/modal/css/component.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-tagsinput/css/bootstrap-tagsinput.css"/>
    <link rel="stylesheet" type="text/css" href="/backend/vendors/animate/css/animate.min.css" />
    <!-- end of plugin styles -->

    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/radio_checkbox.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css" />

    <style>
        .edit-card-header{
            border-bottom: 1px solid dodgerblue;
        }
    </style>

@endsection
@section('content')
    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-sliders circle"></i>
                             Slider Management
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        @if(Session::has('flash_message'))
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable float-right" style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-hidden="true" style="margin-top: 4px; margin-left: 15px;"> ×
                    </button>
                    {{ Session::get('flash_message') }}
                </div>
            </div>
        @endif
        <div class="outer">
            <div class="inner bg-container">
                <div class="row">
                    @include ('errors.list')
                    <div class="col-12 data_tables">
                        <div class="card m-t-35">
                            <div class="card-header bg-white">
                                <i class="fa fa-sliders circle"></i> All Sliders  @can('Create Slider')
                                    <a href="#create" class="btn btn-primary btn-md pull-right adv_cust_mod_btn" data-toggle="modal" data-href="#create" >Add New Slider</a>
                                @endcan
                            </div>
                            <div class="card-body p-t-10">
                                <div class=" m-t-25">
                                    <table class="table table-striped table-bordered table-hover " id="sample_6">
                                        <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>ButtonText</th>
                                            <th>img_url</th>
                                            <th>Action</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($sliders)
                                            @foreach ($sliders as $slider)

                                                <tr>

                                                    <td>{{ $slider->title }}</td>
                                                    <td>{{ $slider->button_text }}</td>
                                                    <td>{{ $slider->img_url }}</td>
                                                    <td>
                                                        @can('Edit Slider')
                                                            <a href="#edit" class="btn btn-primary btn-md adv_cust_mod_btn edit-btn pull-left" data-toggle="modal" style="margin-right: 3px;" data-href="#edit" data-id="{{ $slider->id }}" data-otitle="{{ $slider->title }}" data-button_text="{{ $slider->button_text }}" data-button_link="{{ $slider->button_link }}" data-open_in_new_tab="{{ $slider->open_in_new_tab }}"  >Edit</a>
                                                        @endcan
                                                        @can('Delete Slider')
                                                            {!! Form::open(['method' => 'DELETE', 'route' => ['sliders.destroy', $slider->id] ]) !!}
                                                                {{ Form::hidden('title' ,$slider->title ) }}
                                                                {{ Form::hidden('button_text',$slider->button_text) }}
                                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                                            {!! Form::close() !!}
                                                        @endcan

                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--- create model -->
    <div class="modal fade in display_none" id="create" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                {{ Form::open(array('url' => 'admin/sliders' , 'files' => true)) }}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <h3><i class="fa fa-plus-circle circle"></i> Add New Slider</h3>
                            <hr>
                            <br>
                            <div class="form-group">
                                {{ Form::label('title', 'Title') }}
                                {{ Form::text('title', '', array('class' => 'form-control')) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('button_text', 'Button Text') }}
                                {{ Form::text('button_text', null, array('class' => 'form-control')) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('button_link', 'Button Link') }}
                                {{ Form::text('button_link', null, array('class' => 'form-control')) }}
                            </div>
                            <div class="left_align">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"  name="open_in_new_tab">
                                        <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                        Link Open In New Tab
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('img_url', 'Image Url') }}<br>
                                {{ Form::file('img_url', null, array('class' => 'form-control')) }}
                            </div>


                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                    {{ Form::submit('Save changes', array('class' => 'btn btn-primary')) }}
                </div>
                {{ Form::close() }}
                <br>
            </div>
        </div>
    </div>
    <!-- END create modal-->


    <!--- Edit model -->
    <div class="modal fade in display_none" id="edit" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <form class="modal-content" id="edit_company" action="{{ route('sliders.update',1) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <h3 id="edit-title"></h3>
                                <hr>
                                <br>

                                    <input  type="hidden" class="form-control" name="slider_id" id="slider_id">
                                    <input  type="hidden" class="form-control" name="oldTitle" id="oldTitle">
                                    <input  type="hidden" class="form-control" name="oldbutton_text" id="oldbutton_text">

                                <div class="form-group">
                                    <lablel>Title</lablel>
                                    <input  type="text" class="form-control" name="title" id="title_edit">
                                </div>

                                <div class="form-group">
                                    <lablel>Button Text</lablel>
                                    <input  type="text" class="form-control" name="button_text" id="button_text_edit">

                                </div>
                                <div class="form-group">
                                    <lablel>Button Link</lablel>
                                    <input  type="text" class="form-control" name="button_link" id="button_link_edit">

                                </div>
                                <div class="left_align">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"  name="open_in_new_tab" id="open_in_new_tab">
                                            <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                            Link Open In New Tab
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <lablel>Image</lablel>
                                    <input  type="file" class="form-control" name="img_url" id="img_url_edit">
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                    <button type="submit" class="btn btn-primary pull-right" > Save Changes
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END Edit model-->

@endsection
@section('script')
    <!--plugin scripts-->
    <script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>
    <!-- end of plugin scripts -->

    <!--Page level scripts-->
    <script type="text/javascript" src="/backend/js/pages/datatable.js"></script>
    <script type="text/javascript" src="/backend/js/pages/radio_checkbox.js"></script>
    <!-- end of global scripts-->
    <script type="text/javascript">

        $('body').on('click','.edit-btn',function(){

            $('#edit-title').html('<i class="fa fa-pencil circle"></i>  Edit '+ $(this).data('otitle'));
            $('#slider_id').val($(this).data('id'));
            $('#oldTitle').val($(this).data('otitle'));
            $('#oldbutton_text').val($(this).data('button_text'));

            $('#title_edit').val($(this).data('otitle'));
            $('#button_text_edit').val($(this).data('button_text'));
            $('#button_link_edit').val($(this).data('button_link'));
            var open_in_new_tab = $(this).data('open_in_new_tab');
            if(open_in_new_tab == 1 ){
                $('#open_in_new_tab').prop("checked", true);
            }else{
                $('#open_in_new_tab').prop("checked", false);
            }

        });

    </script>
@endsection