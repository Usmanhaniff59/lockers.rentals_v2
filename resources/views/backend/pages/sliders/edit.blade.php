@extends('backend.layouts.app')

@section('title', 'Lockers | Edit Label')
@section('style')
    <!--plugin styles-->

    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-switch/css/bootstrap-switch.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/switchery/css/switchery.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/radio_css/css/radiobox.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/checkbox_css/css/checkbox.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/modal/css/component.css"/>
    <link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-tagsinput/css/bootstrap-tagsinput.css"/>
    <link rel="stylesheet" type="text/css" href="/backend/vendors/animate/css/animate.min.css" />
    <!-- end of plugin styles -->

    <!--Page level styles-->

    <link type="text/css" rel="stylesheet" href="/backend/css/pages/radio_checkbox.css" />


    <style>
        .edit-card-header{
            border-bottom: 1px solid dodgerblue;
        }
    </style>
@section('content')

    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-user-plus"></i>
                            Edit {{$slider->title}}
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        <div class="outer">
            <div class="inner bg-container">


                <!--top section widgets-->
                <div class="row widget_countup">
                    <div class="col-12">

                        <hr>
                        {{-- @include ('errors.list') --}}

                        <form  id="edit_company" action="{{ route('sliders.update', $slider->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            {{ method_field('PATCH') }}
                            <input  type="hidden" class="form-control" name="id" id="slider_id">

                            <div class="form-group">
                                <lablel>Title</lablel>
                                <input  type="text" class="form-control" name="title" id="title_edit" value="{{$slider->title}}">
                            </div>

                            <div class="form-group">
                                <lablel>Button Text</lablel>
                                <input  type="text" class="form-control" name="buttonText" id="buttonText_edit" value="{{$slider->buttonText}}">

                            </div>
                            <div class="form-group">
                                <lablel>Button Link</lablel>
                                <input  type="text" class="form-control" name="button_link" id="button_link_edit" value="{{$slider->button_link}}">

                            </div>

                            <div class="left_align">
                                <div class="checkbox">
                                    <label>
                                        @if($slider->open_in_new_tab == 1)
                                            <input type="checkbox"  name="open_in_new_tab" checked>
                                        @else
                                            <input type="checkbox"  name="open_in_new_tab">
                                        @endif
                                        <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                        Link Open In New Tab
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <lablel>Image Url</lablel>
                                <input  type="file" class="form-control" name="img_url" id="img_url_edit" >

                            </div>

                            <div class="form-group">
                                <lablel>Description</lablel>
                                <input  type="text" class="form-control" name="description" id="description_edit" value="{{$slider->description}}">

                            </div>

                            <button type="submit" class="btn btn-primary pull-right" > Save Changes
                            </button>


                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection