@extends('backend.layouts.app')

@section('title', 'Lockers | Locker Description')
@section('style')
    <!--plugin styles-->
    <link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css" />
    <!-- <link type="text/css" rel="stylesheet" href="/backend/css/jquery.dataTables.min.css" /> -->

    <!-- end of plugin styles -->
    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css" />
    <style type="text/css">
        .error{
            color: red;
        }
        .data_tables .row{
            width: 100%;
        }
        #sample_8{
            width: 100% !important;
        }

        /*toggle swtich css*/
        .switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

/* Hide default HTML checkbox */
.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
    </style>
@endsection
@section('content')
    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            <i class="fa fa-cubes circle"></i>
                            Locker Description
                        </h4>
                    </div>
                </div>
            </div>
        </header>
        @if(Session::has('flash_message'))
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable float-right" style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-hidden="true" style="margin-top: 4px; margin-left: 15px;"> ×
                    </button>
                    {{ Session::get('flash_message') }}
                </div>
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="outer">
            <div class="inner bg-container">
                <div class="row">
                    <div class="col-md-12 data_tables">

                        <div class="card m-t-35">
                            <div class="card-header bg-white">
                                <i class="fa fa-cubes circle"></i> All Colors
                                <a href="javascript:void(0)" style="float: right"  data-toggle="modal" data-target="#edit" class="btn btn-primary" role="button">Add New Color</a>

                            </div>
                            <div class="card-body p-t-10">
                                <div class=" m-t-25">
                                    <table class="table table-bordered table-hover " id="sample_8">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <!-- <th>Date/Time Added</th> -->
                                            <th>Operations</th>
                                        </tr>
                                        </thead>
                                     
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!--- Edit model -->
    <div class="modal fade in display_none" id="edit" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <form class="modal-content" id="block-group" action="{{ route('locker_description.store') }}" method="POST">
                    @csrf
                    <!-- {{ method_field('PUT') }} -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <h3 id="edit_title"></h3>
                                <hr>
                                <br>
                                <!-- <input  type="hidden" class="form-control" name="id" id="bookingTypeId_edit"> -->
                                <div class="form-group">

                                    <lablel>Name</lablel>
                                    <input  type="text" class="form-control" name="color" id="name_edit">
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                        <button type="submit" class="btn btn-primary pull-right" > Save Changes
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END Edit model-->

    <div class="modal fade in display_none" id="edit_block" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <form class="modal-content" id="block-group" action="{{ route('locker_description.update.status') }}" method="POST">
                    @csrf
                    <!-- {{ method_field('PUT') }} -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <h3 id="edit_title"></h3>
                                <hr>
                                <br>
                                <input  type="hidden" class="form-control" name="id" value="" id="blockTypeId">
                                <div class="form-group">

                                    <lablel>Name</lablel>
                                    <input  type="text" class="form-control" name="color" value="" id="name_update">
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
                        <button type="submit" class="btn btn-primary pull-right" > Save Changes
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <!--plugin scripts-->
    <!-- <script type="text/javascript" src="/backend/js/jquery.min.js"></script> -->

    <script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
    <!-- end of plugin scripts -->
    <!--Page level scripts-->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="/backend/js/pages/datatable.js"></script>
    <script type="text/javascript" src="/backend/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/backend/js/blocks/custom-validation.js"></script>
    <script type="text/javascript" src="/backend/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/backend/js/jquery.dataTables.rowReordering.js"></script>



    <script src="{{ asset('custom/general.js') }}"></script>

    <!-- end of global scripts-->
    <script type="text/javascript">

        $('body').on('click','.edit-btn',function(e){

            $('#edit_title').html('<i class="fa fa-pencil circle"></i>  Edit '+ $(this).data('name'));
            $('#bookingTypeId_edit').val($(this).data('id'));
            $('#name_edit').val($(this).data('name'));


        });


         var company_table = blockgroupFun();

        function blockgroupFun() {
            $('#sample_8').dataTable().fnClearTable();
            $('#sample_8').dataTable().fnDestroy();

            $("#sample_8_filter>label").css('float', 'right');
            $("#sample_8_length>label").css('float', 'left');
            var table7 = $('#sample_8').DataTable({
                responsive: true,
                serverSide: true,
                processing: true,
                "ordering": false,
                
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                stateSave: true,
                "oLanguage": {
                    "sLengthMenu": "_MENU_",
                    "sSearch": "_INPUT_"
                },
                "aTargets": ["_all"],
                initComplete: function () {
                    $("#sample_8_filter>label").css('float', 'right');
                    $("#sample_8_length>label").css('float', 'left');
                },
                
                ajax: {
                    url: '<?php echo e(route('locker_description.show.all')); ?>',
                    type: 'POST',
                    data: function (d) {
                        d._token="{{ csrf_token() }}";
                      
                    }
                },
             
                columns: [
                    
                    {data: 'name', name: 'name'},
                    {data: 'action', name: 'action'}
                ],
            });

        }
    


$(document).on('change','.toggle-checkbox',function(){
    if($(this).val()==1)
    {
        var id=$(this).attr('data-id');
        $(this).val(0);
              $.ajax({
        type: "POST", 
        dataType: "json", 
        url: '<?php echo e(route('locker_description.update.active')); ?>',
        data: {
          status:0,
          id:id,
          '_token': "{{ csrf_token() }}",
          

        },
        success: function(response) {
            if (response==1) {

            } else {
            }
        }
      });
    }else{
         var id=$(this).attr('data-id');
        $(this).val(1);
              $.ajax({
        type: "POST", 
        dataType: "json", 
        url: '<?php echo e(route('locker_description.update.active')); ?>',
        data: {
          status:1,
          id:id,
          '_token': "{{ csrf_token() }}",
          

        },
        success: function(response) {
            if (response==1) {

            } else {
            }
        }
      });
    }
});

$(document).on('click','.edit-block',function(){
  

    $('#blockTypeId').val($(this).attr('data-id'));
    $('#name_update').val($(this).attr('data-name'));

$('#edit_block').modal('toggle');
});
$(document).on('click','.delete-block',function(){
     var id=$(this).attr('data-id');
     var table=$(this).attr('data-table');
swal({
  title: "Are you sure?",
  text: "Once deleted, you will not be able to recover this!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {
           $.ajax({
        type: "POST", 
        dataType: "json", 
        url: '<?php echo e(route('locker_description.update.deleteBlock')); ?>',
        data: {
            '_token': "{{ csrf_token() }}",
             id:id,
             table:table,
        },
        success: function(response) {
            if (response==1) {
                $('#delete-block-'+id).closest('tr').remove();
                swal("Poof! Your record has been deleted!", {
                      icon: "success",
                    });
            } else {
                swal("Error! Please Try Again");
            }
        }
      });
    
  } else {
    swal("Your record is safe!");
  }
});
});
         // sort the array 
// $('#sample_8').on('row-reorder.dt', function(e, details, edit){   
//    for(var i = 0; i < details.length; i++){
//     console.log(details[i]);
//       console.log(
//          'Node', details[i].node, 
//          'moved from', details[i].oldPosition, 
//          'to', details[i].newPosition
//       );
//    }
// });


// $('#sample_8').on('mousedown.rowReorder touchstart.rowReorder', 'tbody tr td:eq(0)', function(){
//    var tr = $(this).closest('tr');    
//    console.log('Started dragging row', tr);
    
//    $(document).on('mousemove.rowReorder touchmove.rowReorder', function(){
//       console.log('Dragging row', tr);
//    });
// });

        $(document).ready(function(){
             $("#sample_8_filter>label").css('float', 'right');
            $("#sample_8_length>label").css('float', 'left');

        })

    </script>
@endsection