<link type="text/css" rel="stylesheet" href="/frontend/vendors/chosen/css/chosen.css"/>
<link type="text/css" rel="stylesheet" href="/frontend/css/pages/form_elements.css"/>
{{ Form::model($user, array('route' => array('payment.updateUser', $user->id), 'method' => 'PUT')) }} {{-- Form model binding to automatically populate our fields with user data --}}
<div class="modal-body">
    <div class="row">
        <div class="col-12">
            <h3><i class="fa fa-pencil circle"></i>
                Edit {{$user->name}}</h3>
            <hr>

            <div class="row">
                <div class="form-group col-6">
                    {{ Form::label('name', 'Name') }}
                    {{ Form::text('name', null, array('class' => 'form-control')) }}
                </div>

                <div class="form-group col-6">
                    {{ Form::label('email', 'Email') }}
                    {{ Form::email('email', null, array('class' => 'form-control')) }}
                </div>
            </div>

            <div class="row">
                <div class="form-group col-6">
                    {{ Form::label('phone_number', 'Phone Number') }}
                    {{ Form::tel('phone_number', null, array('class' => 'form-control')) }}
                </div>
                <div class="form-group col-6">
                    {{ Form::label('address_1', 'Address 1') }}
                    {{ Form::text('address_1', null, array('class' => 'form-control')) }}
                </div>
            </div>



            <div class="row">
               <div class="form-group col-6">
                {{ Form::label('address_2', 'Address 2') }}
                {{ Form::text('address_2', null, array('class' => 'form-control')) }}
            </div>
        </div>



        @can('Assign Role To User')
{{--            <h5><b>Give Role</b></h5>--}}

            {{--        @foreach ($roles as $index=>$role)--}}
            {{--        <div class='form-group  col-6'>--}}
            {{--            <div class="radio_basic_swithes_padbott">--}}
            {{--                {{ Form::checkbox('roles[]',  $role->id, $user->roles,array('class' => 'js-switch sm_toggle_update'.$index.'' )) }}--}}
            {{--                {{ Form::label($role->name, ucfirst($role->name),['class' => 'radio_switchery_padding']) }}<br>--}}

            {{--            </div>--}}
            {{--        </div>--}}

            {{--        @endforeach--}}

        @endcan
        {{--@can('Assign Franchisee To User')--}}
        {{--<select class="form-control"  name="franchisee_id">--}}
        {{--<option value="" selected>Choose Franchisee</option>--}}
        {{--@foreach ($franchisees as $franchisee)--}}
        {{--<option value="{{$franchisee->id}}" {{ $franchisee->id == $user->franchisee_id ? 'selected' : '' }}>{{$franchisee->name}}</option>--}}
        {{--@endforeach--}}
        {{--</select>--}}
        {{--@endcan--}}
        <div class="row">

            {{--            @can('Assign Company To User')--}}
            {{--            <div class='form-group  col-6'>--}}
            {{--                <label for="franchisee_id">Company</label>--}}
            {{--                <select class="form-control chzn-select" name="company_id" tabindex="2">--}}
            {{--                    <option value="" selected>Choose Company</option>--}}
            {{--                    @foreach ($companies as $company)--}}
            {{--                    <option value="{{$company->id}}" {{ $company->id == $user->company_id ? 'selected' : '' }} >{{$company->name}}</option>--}}
            {{--                    @endforeach--}}
            {{--                </select>--}}
            {{--            </div>--}}
            {{--            @endcan--}}
            {{--            @can('Assign Franchisee To User')--}}
            {{--            <div class="col-6">--}}
            {{--               <div class='form-group  existed-franchisee-group'>--}}
            {{--                <label for="franchisee_id">Franchisee</label>--}}

            {{--                <select class="form-control chzn-select"  name="franchisee_id">--}}
            {{--                    <option value="" selected>Choose Franchisee</option>--}}
            {{--                    @foreach ($franchisees as $franchisee)--}}
            {{--                    <option value="{{$franchisee->id}}" {{ $franchisee->id == $user->franchisee_id ? 'selected' : '' }}>{{$franchisee->name}}</option>--}}
            {{--                    @endforeach--}}
            {{--                </select>--}}

            {{--            </div>--}}

            {{--            <div class="form-group  new-franchisee-group-input d-none">--}}
            {{--                {{ Form::label('franchisee_name', 'Franchisee Group Name') }}--}}
            {{--                {{ Form::text('franchisee_name', '', array('class' => 'form-control')) }}--}}
            {{--            </div>--}}
            {{--            <h6 class='text-center'>OR</h6>--}}

            {{--            <div class='form-group  text-center new-franchisee-group-button-field'>--}}
            {{--                <button type="button"  class="btn btn-primary create-new-franchisee-group-button">Create New Franchisee Group</button>--}}
            {{--            </div>--}}

            {{--            <div class='form-group  text-center existed-franchisee-group-button-field d-none'>--}}
            {{--                <button type="button"  class="btn btn-success existed-franchisee-group-button" id="">Choose Existing Franchisee Group</button>--}}
            {{--            </div>--}}
            {{--        </div>--}}
            {{--        --}}
            {{--        @endcan--}}
        </div>



        {{--<div class="form-group  col-6">--}}
        {{--{{ Form::select('franchisee_id',array('L' => 'Large', 'S' => 'Small'),null,array('class' => 'form-control chzn-select','tabindex'=>'2'))}}--}}
        {{--</div>--}}
        <div class="row">
            {{--    <div class="form-group  col-6">--}}
            {{--        {{ Form::label('password', 'Password') }}<br>--}}
            {{--        {{ Form::password('password', array('class' => 'form-control')) }}--}}

            {{--    </div>--}}

            {{--    <div class="form-group  col-6">--}}
            {{--        {{ Form::label('password', 'Confirm Password') }}<br>--}}
            {{--        {{ Form::password('password_confirmation', array('class' => 'form-control')) }}--}}

            {{--    </div>--}}
        </div>


    </div>
</div>
</div>
<div class="modal-footer" style="display: flex;">
    <button type="button" data-dismiss="modal" class="btn btn-light">Close</button>
    {{ Form::submit('Update User', array('class' => 'btn btn-primary  ')) }}
</div>
{{ Form::close() }}


<script type="text/javascript" src="/backend/vendors/chosen/js/chosen.jquery.js"></script>
<script type="text/javascript" src="/backend/js/pages/form_elements.js"></script>

<script type="text/javascript">

    var role_access = "{{auth()->user()->can('Edit User')}}";

    if(role_access.length >0) {
        var roles = "{{ count($roles) }}";
        console.log(roles);
        for (var i = 0; i < roles; i++) {
            new Switchery(document.querySelector('.sm_toggle_update' + i), {
                size: 'small',
                color: '#00c0ef',
                jackColor: '#fff'
            });

        }
    }
</script>
