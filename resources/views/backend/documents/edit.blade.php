<div class="modal fade in display_none model-document-id" style="overflow-y: scroll;" id="edit_document" tabindex="-1"
    role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-md-12">
                                <h3><i class="fa fa-pencil circle"></i> Edit Document</h3>
                            </div>
                        </div>
                        <hr>
                        <div class="loader" id="block_loader">
                        </div>
                        <form id="documentEditForm" enctype="multipart/form-data">
                            <input type="hidden" id="id">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label name="edit_title">Title</label>
                                    <input type="text" name="edit_title" id="edit_title"
                                        class="input_empty form-control">
                                </div>
                                <div class='form-group col-md-12'>
                                    <label for="document_type">Type</label>
                                    <select class="form-control input_empty" name="edit_type" id="edit_type">
                                        <option value="">Choose Document Type</option>
                                        @foreach($document_types->where('location', 'Global')->where('active', '1') as
                                        $document_type)
                                        <option value="{{$document_type->id}}">{{$document_type->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-md-12">
                                    <br>
                                    <label for="edit_file" class="upload-area" id="uploadfile">
                                        <i class="fas fa-cloud-upload-alt"></i>
                                        <h1 id="placeholder" class="common">Click or drop file here</h1>
                                    </label>
                                    <br>
                                    <input id="edit_file" name="edit_file" type="file" style="display:none" />

                                </div>

                            </div>
                            <div class="modal-footer" style="display: flex;">
                                <button type="button" data-dismiss="modal" class="btn btn-light">Cancel</button>
                                <button type="submit" class="btn btn-primary" id="edit_document_btn">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $("#edit_file").change(function () {
        console.log("here");
        var filees = $("#edit_file").val();
        var filename = filees.split('\\').pop();
        $("#placeholder").text(filename);
        dragcheck=0;
    });
    $("html").on("drop", function (e) {
        e.preventDefault();
        e.stopPropagation();
        file = e.originalEvent.dataTransfer.files;
        $("#placeholder").text(file[0].name);
        dragcheck = 2;
    });
    $current_documentid = 0;
    function saveDocumentId(id){
        console.log(id);
        $current_documentid = id;   
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: '/admin/documents/show',
            type: 'POST',
            dataType: 'JSON',
            cache: false,
            data: {id: $current_documentid, _token: CSRF_TOKEN},
            success: function (data) {
                $('#edit_type').val(data.type_id);
                $('#edit_title').val(data.title);
                $("#placeholder").text(data.original_path);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            }                        
        });    
            
    }     
        
</script>