@extends('backend.layouts.app')

@section('title', 'Lockers | Companies')
@section('style')
<style>
    .loader {
        position: absolute;
        top: 40%;
        left: 40%;
        margin: auto;
        display: none;
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #00c0ef;
        width: 150px;
        height: 150px;
        -webkit-animation: spin 2s linear infinite;
        /* Safari */
        animation: spin 2s linear infinite;
    }

    .document_error {
        font-weight: 500;
        color: red;
        margin-top: -10px;
    }

    .error-border-bottom {
        border-bottom: 1px solid red !important;
    }
</style>
<!--plugin styles-->
<link type="text/css" rel="stylesheet" href="/backend/vendors/select2/css/select2.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/scroller.bootstrap.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/colReorder.bootstrap.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/datatables/css/dataTables.bootstrap.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/pages/dataTables.bootstrap.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/plugincss/responsive.dataTables.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-switch/css/bootstrap-switch.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/switchery/css/switchery.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/radio_css/css/radiobox.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/checkbox_css/css/checkbox.min.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/modal/css/component.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/bootstrap-tagsinput/css/bootstrap-tagsinput.css" />
<link rel="stylesheet" type="text/css" href="/backend/vendors/animate/css/animate.min.css" />
<!-- end of plugin styles -->

<!--Page level styles-->
<link type="text/css" rel="stylesheet" href="/backend/css/pages/tables.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/pages/radio_checkbox.css" />


<link type="text/css" rel="stylesheet" href="/backend/css/pages/datepicker-jquery.css" />
<link type="text/css" rel="stylesheet" href="/backend/vendors/chosen/css/chosen.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/pages/form_elements.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/blocks/blocks.css" />


@endsection
@section('content')
<div id="content" class="bg-co  ntainer">
    <header class="head">
        <div class="main-bar">
            <div class="row no-gutters">
                <div class="col-6">
                    <h4 class="m-t-5">
                        <i class="fa fa-building circle header_icon_margin"></i>
                        Document Management
                    </h4>
                </div>
            </div>
        </div>
    </header>
    @if(Session::has('flash_message'))
    <div class="col-md-12">
        <div class="alert alert-success alert-dismissable float-right"
            style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"
                style="margin-top: 4px; margin-left: 15px;"> ×
            </button>
            {{ Session::get('flash_message') }}
        </div>
    </div>
    @endif
    @if (session('status'))
    <div class="alert alert-danger">
        {{ session('status') }}
    </div>
    @endif
    @include('errors.success')
    @include('errors.error')
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                @include ('errors.list')
                <div class="col-12 data_tables">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">
                            <i class="fa fa-file circle header_icon_margin"></i> All Documents
                        </div>
                        <div class="bg-white">
                            <a href="#create_document" class="input_create_document_empty btn btn-primary"
                                data-toggle="modal" style="float: right; margin-top: 12px; margin-right: 52px; ">Add</a>

                        </div>
                        <div class="card-body p-t-10">
                            <div class=" m-t-25">

                                <table class="table  table-bordered table-hover" id="documentsTable"
                                    style="width: 100% !important;">
                                    <thead>
                                        <tr>
                                            <th style="width:220px">Title</th>
                                            <th style="width:220px">Type</th>
                                            <th style="width:220px">Details</th>

                                          <!--   <th style="width:220px">Uploaded By</th>
                                            <th style="width:220px">Date Updated</th> -->
                                            <th style="width:330px">&nbsp;</th>
                                        </tr>
                                    </thead>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
@include('backend.documents.create')
@include('backend.documents.edit')
@endsection

@section('script')
<script type="text/javascript" src="/backend/vendors/select2/js/select2.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="/backend/js/pluginjs/dataTables.tableTools.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.colReorder.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="/backend/js/pluginjs/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.rowReorder.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/buttons.print.min.js"></script>
<script type="text/javascript" src="/backend/vendors/datatables/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="/backend/vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="/backend/vendors/switchery/js/switchery.min.js"></script>
<!-- end of plugin scripts -->

<!--Page level scripts-->
<script type="text/javascript" src="/backend/js/pages/datatable_server_side.js"></script>
<script type="text/javascript" src="/backend/js/pages/radio_checkbox.js"></script>
<script type="text/javascript" src="{{asset('/backend/js/common.js')}}"></script>
<script src="{{ asset('custom/general.js') }}"></script>

<script type="text/javascript" src="/backend/js/blocks/block_lockers.js"></script>
<script type="text/javascript" src="/backend/vendors/chosen/js/chosen.jquery.js"></script>
<script type="text/javascript" src="/backend/js/pages/form_elements.js"></script>

{{--<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>--}}
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- end of global scripts-->

<script>
    $('#create_document').on('show.bs.modal', function () {
        $("#documentsForm")[0].reset();
        $("h1").text("Click or drop file here");
        $('.document_error').hide();
        $('.document-title-border').removeClass('error-border-bottom');
        $('#uploadfile').removeClass('error-border-bottom');
        $('#choose_document_type').removeClass('error-border-bottom');
       
    })
    
    var dragcheck;
    $('#edit_document').on('hidden.bs.modal', function () {
            
        $(".upload-heading-create").text("Click or drop file here");
        $("#documentsForm")[0].reset();
    })

    $('#create_document').on('hidden.bs.modal', function () {
        console.log('inhere');
        $("#documentsForm")[0].reset();
        $("upload-heading-create").text("Click or drop file here");
    })
       
        documentsTable();
        
        $("#documentEditForm").submit(function(event){
            spinner_start();
            event.preventDefault();
            $('button#edit_document_btn').prop('disabled', true);
            type= $('#edit_type').val();
            title= $('#edit_title').val();
            formdata =  new FormData(this);
            formdata.append('id',$current_documentid);
            if(dragcheck == 2){
                console.log(file[0]);
                formdata.append('edit_file', file[0]);
                dragcheck = 0;
            }

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: 'company/documents/update' ,
                data: formdata,
                contentType:false,
                cache:false,
                processData:false,
                success: function(){
                    $('#edit_document').modal('hide');
                    $('#edit').modal('show');
                    $('button#edit_document_btn').prop('disabled', false);
                    documentsTable();
                    spinner_close()
                
                },
                error: function (error) {
                    spinner_close()
                    $('button#edit_document_btn').prop('disabled', false);

                }
            });
        });
           

        function documentsTable() {
            $('#documentsTable').dataTable().fnClearTable();
            $('#documentsTable').dataTable().fnDestroy();
        
            return $('#documentsTable').DataTable({
                initComplete : function() {
                    $("#documentsTable_wrapper>.row").attr('style','width:100%;');
                    $("#documentsTable_filter>label").css('float', 'right');
                    $("#documentsTable_length>label").css('float', 'left');
                },
                "order":[[0,'desc'],[1,'asc']],
                "order": [],
                language: {
                    'lengthMenu': '_MENU_',
                    "search": '',
                    "searchPlaceholder": "Search..."
                },

            
                "autoWidth": false,
                "scrollY": "800px",
                "columnDefs": [
                { "width": "220px", "targets": 0 },
                { "width": "220px", "targets": 1 },
                { "width": "220px", "targets": 2 },
                { "width": "330px", "targets": 3 },
                // { "width": "220px", "targets": 4 },
                ],
                ajax: '<?php echo route('documents.show.all'); ?>',     
                columns: [
                    {data: 'title', name: 'title'},
                    {data: 'type_id', name: 'type_id'},
                    {data: 'details', name: 'details'},
                    // {data: 'updated_at', name: 'updated_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: true}
                ],

            });
            
        
        }   
        function spinner_start() {
            $(".loader").show();
            jQuery('#documentEditForm').css('opacity', '0.05');
            jQuery('#documentsForm').css('opacity', '0.05');
        }
        function spinner_close() {
            // $(".excel-loader").hide();
            $(".loader").hide();
            jQuery('#documentEditForm').css('opacity', '1');
            jQuery('#documentsForm').css('opacity', '1');
            
        }

    $(document).on('click','.delete-doc',function(){
    var id = $(this).attr('data-id');
    swal({
  title: "Are you sure?",
  text: "Once you Delete, you will not be able to recover it again!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {
     $.ajax({

                url: '/admin/company/documents/destroy/' + id,

                type: 'get',

                dataType: 'JSON',

                cache: false,

                success: function (data) {
                    console.log(data);
                    // $('#edit').modal('toggle');
                    documentsTable();
                    // getLatest();

                },

                error: function (jqXHR, textStatus, errorThrown) {

                    console.log(jqXHR);

                }

            });
  } else {
  }
});

   });
       
   
</script>
@endsection