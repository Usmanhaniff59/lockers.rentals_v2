@extends('frontend.layouts.apphome')
@section('style')
    <link rel="stylesheet" href="/frontend/vendors/bootstrapvalidator/css/bootstrapValidator.min.css"/>
    <!--page level styles-->
    <link rel="stylesheet" href="/frontend/vendors/swiper/css/swiper.min.css" />
    <!--Page level styles-->
    <link rel="stylesheet" href="/frontend/css/pages/general_components.css" />
    <style>
        .form-title{
            color: #b30510;
        }
    </style>
@endsection
@section('content')
    <main id="main">
        <section id="about">
            <div class="container">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12" style="padding-bottom:50px;padding-top:20px;">
                                <div class="mid">
                                    <div class="midTop-text"></div>
                                    <h2 class="form-title">Site Undergoing Maintenance</h2>
                                    {{--<p>--}}
                                        {{--Locker Rentals is being rebuilt with improved features that will improve both the booking process, and new functionality for example we are launching an online account to enable users to  mange locker bookings in a portal. The New site will be launched early April.--}}
                                    {{--</p>   --}}
                                    <p>
                                        Locker Rentals system is currently undergoing maintenance to improve the system. We will be live again within the next 24hrs
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection