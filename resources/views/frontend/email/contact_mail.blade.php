<table style="width: 100%;">
    <tr style=" background-color:#f8fafc; padding: 5px;width: 100%;">
        <td colspan="3" style="width: 100%;padding: 20px;text-align: center;">
            <p style="font-size: 20px;"> <b>{{$subject}}</b> </p>
        </td>
    </tr>
    <tr style="width: 100%;">
        <td style="width: 10%;"></td>
        <td style="width: 60%;padding: 20px;">
            <p style="font-size: 20px;"><b>Subject </b><br>  {{$subject}} </p>
            <p style="font-size: 20px;"><b>School Name </b><br>  {{$school_name}} </p>
            <p style="font-size: 20px;"><b>Booking ID Reference Number </b><br>  {{$bookingId}} </p>
            <p style="font-size: 20px;"><b>Phone  </b><br>  {{$phone}} </p>
            <p style="font-size: 20px;"><b>Email  </b><br> {{$email}} </p>
            <p style="font-size: 20px;"><b>Message  </b><br> {{$my_message}}</p>
        </td>
        <td style="width: 10%;"></td>
    </tr>
</table>
