<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Locker</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicons -->
    <link rel="shortcut icon" href="/frontend/favicon/favicon.jpeg">
    {{--<link href="img/favicon.png" rel="icon">--}}
    <link href="img/apple-touch-icon.png" rel="apple-touch-icon">


    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/frontend/css/components.css"/>
    <!-- Bootstrap CSS File -->
    <link href="/frontend/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">


    <link rel="stylesheet" href="/frontend/css/custom.css"/>
    <!-- Libraries CSS Files -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/solid.css" rel="stylesheet">
    <link href="/frontend/lib/animate/animate.min.css" rel="stylesheet">
    <link href="/frontend/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="/frontend/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="/frontend/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

    <!-- Main Stylesheet File -->


    <link href="/frontend/css/style.css" rel="stylesheet">
    <link href="/frontend/css/mystyle.css" rel="stylesheet">
    <link rel="stylesheet" href="/frontend/summernote/summernote-bs4.css">

    <!--End of global styles-->
    <!--Plugin styles-->
    {{--<link type="text/css" rel="stylesheet" href="/frontend/vendors/bootstrap-switch/css/bootstrap-switch.min.css" />--}}
    {{--<link type="text/css" rel="stylesheet" href="/frontend/vendors/switchery/css/switchery.min.css" />--}}
    {{--<link type="text/css" rel="stylesheet" href="/frontend/vendors/checkbox_css/css/checkbox.min.css" />--}}
    {{--<!--End of Plugin styles-->--}}
    {{--<!--Page level styles-->--}}
    {{--<link type="text/css" rel="stylesheet" href="/frontend/css/pages/radio_checkbox.css" />--}}

    <link href="/frontend/css/bootstrap-toggle.min.css" rel="stylesheet">

    <style>
        #locale {
            height: auto;
        }
        .cms-modal-footer{
            display: block;
        }
        .toggle btn btn-danger off{
            height: 42px;
            width: 132px;
        }
    </style>
    <script src="https://kit.fontawesome.com/14fa05d5e7.js" crossorigin="anonymous"></script>
    @yield('style')


</head>

<body>
<!--==========================
  Header
============================-->
@include('frontend.layouts.headerhome')

<!--==========================
  flash-messages
============================-->

@include('frontend.pages.home.homeslider')

<!--==========================
  flash-messages
============================-->
@include('errors.flash-message')

<!--==========================
  Page Content
============================-->
@yield('content')


<!--==========================
  Edit Modal
============================-->
<!-- The Modal -->
@include('frontend.pages.home.cmsmodal')
<!--==========================
  Footer
============================-->
@include('frontend.layouts.footer')


<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
<!-- Uncomment below i you want to use a preloader -->
<!-- <div id="preloader"></div> -->
<script src="/frontend/js/components.js"></script>
<script src="/frontend/js/custom.js"></script>
<!-- JavaScript Libraries -->
<script src="/frontend/lib/jquery/jquery.min.js"></script>
<script src="/frontend/lib/jquery/jquery-migrate.min.js"></script>
<script src="/frontend/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/frontend/lib/easing/easing.min.js"></script>
<script src="/frontend/lib/superfish/hoverIntent.js"></script>
<script src="/frontend/lib/superfish/superfish.min.js"></script>
<script src="/frontend/lib/wow/wow.min.js"></script>
<script src="/frontend/lib/waypoints/waypoints.min.js"></script>
<script src="/frontend/lib/counterup/counterup.min.js"></script>
<script src="/frontend/lib/owlcarousel/owl.carousel.min.js"></script>
<script src="/frontend/lib/isotope/isotope.pkgd.min.js"></script>
<script src="/frontend/lib/lightbox/js/lightbox.min.js"></script>
<script src="/frontend/lib/touchSwipe/jquery.touchSwipe.min.js"></script>
<!-- Contact Form JavaScript File -->
<script src="/frontend/contactform/contactform.js"></script>
<!--End of global scripts-->

<!--Plugin scripts-->

<!-- Template Main Javascript File -->
<script src="/frontend/js/main.js"></script>
<script type="text/javascript" src="/frontend/summernote/summernote-bs4.js"></script>

<script src="/frontend/js/bootstrap-toggle.min.js"></script>


<script type="text/javascript" src="{{ asset('frontend/js/cms.js') }}"></script>
<script type="text/javascript">
     $(window).scroll(function() {
    if ($(this).scrollTop() > 30) {

      $('#header').addClass('header-scrolled');
    } else {
      $('#header').removeClass('header-scrolled');
    }
  });

  if ($(window).scrollTop() > 30) {
    $('#header').addClass('header-scrolled');
  }
</script>

@yield('script')
</body>
</html>
