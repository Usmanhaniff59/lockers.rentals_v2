
<footer id="footer">
{{--    <div class="accept-cookies-main" style="display: none;">--}}
{{--        <h3 class="cookies-content"  >--}}
{{--            <span class="assign_class" data-id="accept-cookies-text" data-value="{{t('accept-cookies-text')}}">{!! t('accept-cookies-text')!!}</span> <a class="cookies-link assign_class" data-id="cookies-learn-more" data-value="{{t('cookies-learn-more')}}" href="/policy">{!! t('cookies-learn-more')!!}</a>--}}
{{--        </h3>--}}
{{--        <a href="javascript:;" class="btn button cookies-button assign_class" data-id ="accept-cookies-link-text" data-value ="{{t('accept-cookies-link-text')}}" onclick="checkCookie('0')">{!! t('accept-cookies-link-text')!!}</a>--}}
{{--        <span class="close-cookies-button"><i class="fa fa-times"></i></span>--}}
{{--    </div>--}}
    <div class="footer-top">
        <div class="container footer-container">
            <div class="row">
                <div class="col-lg-3 col-md-6 footer-contact">
                    <h4 class="assign_class"  data-id = "footer-address-heading" data-value ="{{t('footer-address-heading')}}">{!! t('footer-address-heading')!!}</h4>
                    <div class="" style="color: white;">
                        <p class="facebook assign_class"  data-id ="footer-addresstext" data-value = "{{t('footer-addresstext')}}">{!! t('footer-addresstext')!!}</p>
                    </div>
                </div>
                <div class="col-lg-1 col-md-6 footer-contact"></div>
                <div class="col-lg-3 col-md-6 footer-contact">
                    <h4 class="assign_class"  data-id ="footer-web-portal-heading" data-value ="{{t('footer-web-portal-heading')}}">{!! t('footer-web-portal-heading')!!}</h4>
                    <div class="social-links">
                        @if(isset(Auth::user()->id))
                            <a href="/admin/payment" class="facebook assign_class"  data-id ="footer-web-portal-link" data-value = "{{t('footer-web-portal-link')}}"><i class="fad fa-lock"></i></a>
                        @else
                            <a href="/login" class="facebook assign_class"  data-id ="footer-web-portal-link" data-value = "{{t('footer-web-portal-link')}}"><i class="fad fa-lock"></i></a>
                        @endif
                    </div>
                </div>
                <div class="col-lg-1 col-md-6 footer-contact"></div>
                <div class="col-lg-3 col-md-6 footer-contact">
                    <h4 class="assign_class"  data-id = "contact-us-footer" data-value = "{{t('contact-us-footer')}}">{!! t('contact-us-footer')!!}</h4>
                    <div class="social-links">
                        <a href="/" class="twitter assign_class"  data-id = "twitter-link-footer" data-value = "{{t('twitter-link-footer')}}"><i class="fad fa-home-lg-alt"></i></a>
                        <a href="/aboutUs" class="facebook assign_class"  data-id = "facebook-link-footer" data-value = "{{t('facebook-link-footer')}}"><i class="fad fa-info"></i></a>
                        <a href="/contactUs" class="facebook assign_class"  data-id = "facebook-link-footer" data-value = "{{t('facebook-link-footer')}}"><i class="fad fa-question"></i></a>
                        <a href="/policy" class="facebook assign_class"  data-id = "facebook-link-footer" data-value = "{{t('facebook-link-footer')}}"><i class="fad fa-shield-check"></i></a>
                        <a href="/schools.pdf" target="blank" class="facebook assign_class"  data-id = "facebook-link-footer" data-value = "{{t('facebook-link-footer')}}"><i class="fad fa-download"></i></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="container">
        <div class="copyright">
            &copy; Prefect Equipment Limited all rights reserved
        </div>
        <div class="credits">
            Designed by  <a href="http://xweb4u.com/">xweb4u</a>
        </div>
    </div>
    
</footer><!-- #footer -->

<a href="#." class="back-to-top"><i class="fa fa-chevron-up"></i></a>
