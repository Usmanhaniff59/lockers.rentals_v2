<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">

    <title>Locker</title>

    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <meta content="" name="keywords">

    <meta content="" name="description">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Favicons -->
       <link rel="shortcut icon" href="/frontend/favicon/favicon.jpeg">
    <!-- {{--<link href="img/favicon.png" rel="icon"> -->

    <!-- <link href="img/apple-touch-icon.png" rel="apple-touch-icon"> -->



    <!-- Google Fonts -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    {{--<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">--}}
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@0,900;1,900&display=swap" rel="stylesheet">



<!-- Bootstrap CSS File -->
    <link rel="stylesheet" href="/frontend/css/components.css"/>
    <link href="/frontend/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/frontend/css/custom.css"/>


    <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">




    <!-- Libraries CSS Files -->

    {{--<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" rel="stylesheet">--}}
    {{--<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/solid.css" rel="stylesheet">--}}

    <link href="/frontend/lib/animate/animate.min.css" rel="stylesheet">

    <link href="/frontend/lib/ionicons/css/ionicons.min.css" rel="stylesheet">

    <link href="/frontend/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

    <link href="/frontend/lib/lightbox/css/lightbox.min.css" rel="stylesheet">



    <!-- Main Stylesheet File -->

    <!--End of the global styles-->


    <link href="/frontend/css/style.css" rel="stylesheet">

    <link href="/frontend/css/mystyle.css" rel="stylesheet">

    <link rel="stylesheet" href="/frontend/summernote/summernote-bs4.css">



    <!--End of global styles-->

    <!--Plugin styles-->

    {{--<link type="text/css" rel="stylesheet" href="/frontend/vendors/bootstrap-switch/css/bootstrap-switch.min.css" />--}}

    {{--<link type="text/css" rel="stylesheet" href="/frontend/vendors/switchery/css/switchery.min.css" />--}}

    {{--<link type="text/css" rel="stylesheet" href="/frontend/vendors/checkbox_css/css/checkbox.min.css" />--}}

    {{--<!--End of Plugin styles-->--}}

    {{--<!--Page level styles-->--}}

    {{--<link type="text/css" rel="stylesheet" href="/frontend/css/pages/radio_checkbox.css" />--}}


    <link href="/frontend/css/bootstrap-toggle.min.css" rel="stylesheet">
    <style>

        #locale {

            height: auto;

        }

        #mobile-nav-toggle i {

            padding-top: 35px;

        }

        #intro {

            height: 19vh;

        }

        .cms-modal-footer{
            display: block;
        }
@media (min-width: 1200px){
#header #logo img {
    width: 120px;
}
}
    </style>
    <script src="https://kit.fontawesome.com/14fa05d5e7.js" crossorigin="anonymous"></script>
    @yield('style')
</head>
<body>

<!--==========================

  Header

============================-->

@include('token')
@include('frontend.layouts.header')
<!--==========================
  flash-messages
============================-->

<section id="intro"></section><!-- #intro -->
<!--==========================

  flash-messages

============================-->

@include('errors.flash-message')



<!--==========================

  Page Content

============================-->

@yield('content')
<!--==========================

  Edit Modal

============================-->

<!-- The Modal -->

@include('frontend.pages.home.cmsmodal')

<!--==========================

  Footer

============================-->

@include('frontend.layouts.footer')
<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

<!-- Uncomment below i you want to use a preloader -->

<!-- <div id="preloader"></div> -->


<script src="/frontend/js/components.js"></script>
<script src="/frontend/js/custom.js"></script>
<!-- JavaScript Libraries -->

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js">


    <script src="/frontend/lib/jquery/jquery.min.js"></script>
{{--<script src="https://use.fontawesome.com/e290fa82f2.js"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/solid.js"></script>--}}

<script src="/frontend/lib/jquery/jquery-migrate.min.js"></script>

<script src="/frontend/lib/bootstrap/js/bootstrap.bundle.min.js"></script>

<script src="/frontend/lib/easing/easing.min.js"></script>

<script src="/frontend/lib/superfish/hoverIntent.js"></script>

<script src="/frontend/lib/superfish/superfish.min.js"></script>

<script src="/frontend/lib/wow/wow.min.js"></script>

<script src="/frontend/lib/waypoints/waypoints.min.js"></script>

<script src="/frontend/lib/counterup/counterup.min.js"></script>

<script src="/frontend/lib/owlcarousel/owl.carousel.min.js"></script>

<script src="/frontend/lib/isotope/isotope.pkgd.min.js"></script>

<script src="/frontend/lib/lightbox/js/lightbox.min.js"></script>

<script src="/frontend/lib/touchSwipe/jquery.touchSwipe.min.js"></script>

<!-- Contact Form JavaScript File -->

<script src="/frontend/contactform/contactform.js"></script>

<!--End of global scripts-->



<!--Plugin scripts-->



<!-- Template Main Javascript File -->

<script src="/frontend/js/main.js"></script>

<script type="text/javascript" src="/frontend/summernote/summernote-bs4.js"></script>

<script src="/frontend/js/bootstrap-toggle.min.js"></script>
<script type="text/javascript" src="{{ asset('frontend/js/cms.js') }}"></script>
@yield('script')
<script type="text/javascript">
    $(document).ready(function(){
        $('#header').scrollTop($('#header')[0].scrollHeight);
    });
     $(window).scroll(function() {
    if ($(this).scrollTop() > 1) {

      $('#header').addClass('header-scrolled');
    } else {
      $('#header').removeClass('header-scrolled');
    }
  });

  if ($(window).scrollTop() > 1) {
    $('#header').addClass('header-scrolled');
  }
</script>
</body>
</html>

