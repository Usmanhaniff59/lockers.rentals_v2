<?php
use App\Language;

$lang = Language::where('status',1)->get();
$languages = array();
foreach ($lang as $l)
{
    $languages[$l->locale] = strtoupper($l->locale);

}

?>
<style>
    
 
    .intro-home{
        width: 76%;
    }
    .front_logo_text{
        position: absolute;
        top: 100%;
        left: 4.7%;
        display: block;
        color: white;
    }

    .dropbtn {
        color: white;
        padding: 16px;
        font-size: 16px;
        border: none;
        cursor: pointer;
    }

    .dropdown {
        position: relative;
        display: inline-block;
    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f9f9f9;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
    }

    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
    }

    .dropdown-content a:hover {background-color: #f1f1f1}

    .dropdown:hover .dropdown-content {
        display: block;
    }

</style>

<header id="header" class="pt-0">
    <div id="top-bar" class="bg-dark text-center">
        <div class="container-fluid py-1">
{{--            <span class="phone-number small text-warning mx-2 assign_class" data-id = "phone-number-header" data-value = "{{t('phone-number-header')}}"> {!! t('phone-number-header')!!}</span>--}}
{{--            <span class="office-timing small text-warning mx-2 assign_class" data-id = "office-timing-header" data-value = "{{t('office-timing-header')}}">{!! t('office-timing-header')!!}</span>--}}
        </div>
    </div>
    <div class="container-fluid main-page-header" @can('Edit CMS') style="width: 100%;" @else style="width: 91%;" @endcan >
        <div id="logo" class="pull-left">
            <!--  <h1><a href="/" class="scrollto">Locker <label style="border-left: 4px solid #F5B54A;">&nbsp;Rentals</label> </a></h1> -->
            <!-- Uncomment below if you prefer to use an image logo -->
            <a href="/"><img src="/frontend/img/logo.png"  alt="" title=""/></a>
            {{--                    <span class="front_logo_text">Secure Rentals</span>--}}
        </div>

        @can('Edit CMS')
            @if(\Illuminate\Support\Facades\Session::get('switchCMS') == 'true')
                <input type="checkbox" style=""  class="text-center" data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-on="Edit CMS" data-off="Edit CMS" id="edit_cms_switch" checked>
            @else
                <input type="checkbox" style="" class="text-center" data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-on="Edit CMS" data-off="Edit CMS" id="edit_cms_switch" >
            @endif
        @endcan

        <nav id="nav-menu-container nav-home" class="nav-homewrapp">

            <ul class="nav-menu">
                <li><a href="@t('get-started-button-link')"><button type="button" class="green-button button_text assign_class" data-id = "request-locker-button" data-value = "{{t('request-locker-button')}}" style="border-radius: 3%;text-transform: uppercase;">{!! t('request-locker-button')!!}</button></a></li>
             


                <li><a href="/" class="assign_class navigation" data-id = "home-menu" data-value = "{{t('home-menu')}}">{!! t('home-menu')!!}</a></li>
                <li><a href="/aboutUs" class="assign_class  navigation" data-id = "about-us-menu" data-value = "{{t('about-us-menu')}}">{!! t('about-us-menu')!!}</a></li>
                {{--<li><a href="/frontend/requestavailability" class="assign_class" data-id = "request-form-menu" data-value = "{{t('request-form-menu')}}">{!! t('request-form-menu')!!}</a></li>--}}

                @if($_SERVER['HTTP_HOST']=="dev.locker.rentals")
                    <li><a href="/support" class="assign_class  navigation" data-id = "support-menu" data-value = "{{t('support-menu')}}">{!! t('support-menu')!!}</a></li>
                @else
                    <li><a href="/contact-us"  class="assign_class  navigation" data-id= "contactUs-menu" data-value = "{{t('contactUs-menu')}}">{!! t('contactUs-menu')!!}</a></li>
                @endif

                    <li><a href="/faq" class="assign_class  navigation" data-id = "faq-menu" data-value = "{{t('faq-menu')}}">{!! t('faq-menu')!!}</a></li>
                <li>
                    @if(isset(Auth::user()->id))
                        <div class="dropdown">
                            <a href="/admin/payment" class="dropbtn">Admin</a>
                            <div class="dropdown-content">
                                <a href="/admin/payment">Admin</a>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </div>
                    @else
                        <a href="/login" class="assign_class  navigation" data-id = "admin-login-menu" data-value = "{{t('admin-login-menu')}}">{!! t('admin-login-menu')!!}</a>
                    @endif
                </li>
                <li>{!! Form::open(['method' => 'POST', 'route' => 'changelocale', 'class' => 'form-inline navbar-select']) !!}

                    <div class="form-group @if($errors->first('locale')) has-error @endif">

                        {!! Form::select(
                            'locale',
                            $languages,
                            \App::getLocale(),
                            [
                                'id'       => 'locale',
                                'class'    => 'form-control',
                                'required' => 'required',
                                'onchange' => 'this.form.submit()',
                            ]
                        ) !!}
                        <small class="text-danger">{{ $errors->first('locale') }}</small>
                    </div>

                    <div class="btn-group pull-right sr-only">
                        {!! Form::submit("Change", ['class' => 'btn btn-success']) !!}
                    </div>

                    {!! Form::close() !!}</li>

            </ul>
        </nav><!-- #nav-menu-container -->
        <a href="javascript:void(0)" class="crossbtn fa fa-bars"></a>
    <div class="mobile-menu-wrap">
        <ul class="nav-menu sf-js-enabled sf-arrows" style="touch-action: pan-y;">
                <li class=""><a href="/frontend/requestavailability"><button type="button" class="green-button button_text assign_class" data-id="request-locker-button" data-value="School Enquiry" style="border-radius: 3%;text-transform: uppercase;">School Enquiry</button></a></li>
             <!--    <li>
                    <button type="button" class="green-button button_text assign_class" style="border-radius: 3%;text-transform: uppercase;" data-toggle="modal" data-id="resend-confirmation-email-button" data-value="Resend My Confirmation" data-target="#resend-confirmation-email-modal">Resend My Confirmation</button>
                </li> -->

                    
<!-- 
                <li> <button type="button" class="green-button button_text assign_class reset-clear" id="reset-btn" data-toggle="modal" data-target="#reset-email-modal" data-id="reset-email-button" data-value="Reset My Password" style="border-radius: 3%;text-transform: uppercase;">Reset My Password</button></li>
                     -->

                <li><a href="/" class="assign_class navigation" data-id="home-menu" data-value="<p>Home</p>"><p>Home</p></a></li>
                <li><a href="/aboutUs" class="assign_class navigation" data-id="about-us-menu" data-value="About">About</a></li>
                

                                    <li><a href="/contact-us" class="assign_class navigation" data-id="contactUs-menu" data-value="Contact Us">Contact Us</a></li>
                
                    <li><a href="/faq" class="assign_class navigation" data-id="faq-menu" data-value="FAQ">FAQ</a></li>
                @if(isset(Auth::user()->id))
                     <li>
                                            <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    Logout
                                </a>
                                    </li>
                                    @else
                                     <li>
                                            <a href="/login" class="assign_class navigation" data-id="admin-login-menu" data-value="Login">Login</a>
                                    </li>
                                    @endif
                <li><form method="POST" action="http://test.locker.rentals/changelocale" accept-charset="UTF-8" class="form-inline navbar-select"><input name="_token" type="hidden" value="z5eb0mxWpbzgncqhnIWrWPvlnSyK6IOCT7KkOwur">

                    <div class="form-group ">

                        <select id="locale" class="form-control" required="required" onchange="this.form.submit()" name="locale"><option value="en" selected="selected">EN</option><option value="fr">FR</option></select>
                        <small class="text-danger"></small>
                    </div>

                    <div class="btn-group pull-right sr-only">
                        <input class="btn btn-success" type="submit" value="Change">
                    </div>

                    </form></li>

            </ul>
    </div>
        


    </div>
</header><!-- #header -->

