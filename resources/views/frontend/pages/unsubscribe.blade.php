@extends('frontend.layouts.app')
@section('style')
    <script src="https://code.jquery.com/jquery-3.4.1.js" crossorigin="anonymous"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>
    <style></style>
@endsection
    @section('content')

        {{--@if(session()->has('locale'))--}}
            {{--{{ dd(session()->get('locale'))}}--}}
        {{--@endif--}}
        <main id="main">
            <!--==========================
                 About Us Section
               ============================-->
            <section id="about">
                <div class="container">
                    <header class="section-header">
                        <h3 class="assign_class" data-id = "unsub-heading" data-value = "{{t('unsub-heading')}}">{!! t('unsub-heading')!!}</h3>
                        <p class="assign_class" data-id = "unsub-text" data-value = "{{t('unsub-text')}}">{!! t('unsub-text')!!}</p>
                    </header>
                    <div class="row about-cols">
                        <div class="col-md-12 wow fadeInUp" data-wow-delay="0.1s">
                            <div class="about-col">
                                <div class="img">
                                    <img  alt="" class="img-fluid">
                                    <div class="icon" style="background-color: white"><i class="fad fa-envelope" style="color: red"></i></div>
                                </div>
                             
                                 <p class="assign_class m-t-35" data-id = "unsub-section1-text" data-placeholders="%login%" data-placeholderval="<a href='/login' style='color:#F5B54A'>Login</a>" style="text-align: center;" data-value = "{{t('unsub-section1-text')}}"  > {!!  str_replace(["%login%"],['<a href="/login" style="color:#F5B54A">Login</a>'],t('unsub-section1-text'))  !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- #about -->
        </main>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#sel1").select2();
        });
    </script>

@endsection