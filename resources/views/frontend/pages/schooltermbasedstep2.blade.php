@extends('frontend.layouts.app')

@section('style')


    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">--}}
    {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>--}}

   <script src="https://code.jquery.com/jquery-3.4.1.js" crossorigin="anonymous"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>
@endsection

    @section('content')
        <main id="main">

            <section id="portfolio"  class="section-bg" >
                <form action="{{route('schooltermbased')}}" method="post">
                <div class="container">

                    <header class="section-header">
                        <h3 class="section-title">Type of Booking for {{$school_name}}</h3>
                    </header>
<h4>Please choose a location from the below selection. After you have done this press `Proceed` to enter the duration that you require the locker.
    Select the school year you wish to rent your locker for and pay for multiple years to lock in the rate on each year</h4>
                    <div class="col-md-8 ">
                        <h4>School Name: {{$school_name}}</h4>
                        <h4>From: {{$year->from_date->format('d M, Y')}}</h4>
                        <h4>To: {{$year->to_date->format('d M, Y')}}</h4>
                        <div>
                            <h5>Please tick box to confirm that you have read and accept our policy</h5>
                            <input type="checkbox" name="terms_conditions"> <a class="green-button button_text" onclick="show_terms()" style="margin: 10px;">Terms and Conditions</a>
                        </div>

                        <div style="margin: 10px; border: 2px solid darkgrey; border-radius: 10px; padding: 10px; display: none;" id="terms_div">
                            <p>
                                This rental agreement is between Prefect Rentals and the lessee </p>
                            <ul>
                                <li>The rental period is from the point of rental (regardless of date) until the end of the current school year. Reduced rates for short date ranges are not available</li>
                                <li>Rental fees are non-refundable</li>
                                <li>Rental of lockers is non-transferable</li>
                                <li>The locker can only be used by the lessee and lockers cannot be shared</li>
                                <li>Lockers can only be used for the storage of personal items that belong to the lessee and must not include illegal items or any item prohibited by the school</li>
                                <li>Prefect will not be held liable for any loss, damage or theft of items kept in the lockers.</li>
                                <li>The school has the right to inspect the contents of the locker without prior notice</li>
                                <li>At the end of the school year all items must be removed from the locker and the locker left empty. Any items left in the locker will be removed and disposed of without exception</li>
                                <li>The locker should be left clean and in good condition</li>
                                <li>Any damage or vandalism must be reported to the school immediately</li>
                                <li>Codes can be reset or forgotten codes reissued by visiting Locker.Rentals</li>
                            </ul>
                            <p> For assistance or support issues (other than to retrieve your code) please contact us on 0330 311 1003 or rentals@prefectlockers.com
                            </p>
                        </div>
                        <div class="text-center"><a href="/" class="green-button button_text assign_class" data-id = "request-form-button" data-value = "{{t('request-form-button')}}">Restart Booking</a>
                            <button type="submit" class="green-button button_text assign_class" data-id = "request-form-button" data-value = "{{t('request-form-button')}}" name="step2">Check Availability</button>
                        </div>
                    </div>

                </div>
                    @csrf
                </form>
            </section><!-- #portfolio -->



        </main>


@endsection

@section('script')
    <script type="text/javascript">

        $(document).ready(function() {

            $("#sel1").select2();

        });

        function show_terms() {
            var x = document.getElementById("terms_div");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }

    </script>

@endsection