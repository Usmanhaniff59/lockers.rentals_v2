@extends('frontend.layouts.app')

@section('style')
    <script src="https://code.jquery.com/jquery-3.4.1.js" crossorigin="anonymous"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>
    <style>

    </style>
@endsection



@section('content')

    <main id="main">



        <!--==========================

          Skills Section

        ============================-->

        <section id="skills">

            <div class="container">



                <header class="section-header">

                    <h3 class="assign_class" data-id = "request-form-label" data-value = "{{t('request-form-label')}}">{!! t('request-form-label')!!}</h3>
                    <p><span class="assign_class" data-id="requestform-policy-review" data-value="{{t('requestform-policy-review')}}">{!! t('requestform-policy-review')!!}</span> <a href="/policy" class="assign_class" data-id="requestform-policy-review-link" data-value="{{t('requestform-policy-review-link')}}">{!! t('requestform-policy-review-link')!!}</a></p>
                </header>

                @if($errors->any())

                    <h4>{{$errors->first()}}</h4>

                @endif

                <form action="/frontend/saverequestavailability" method="post" role="form">

                    @csrf

                    <div class="col-md-8 offset-md-2">



                        <div class="form-group">

                            <select class="form-control" name="type" id="sel1"  >

                                <option value="">Select Type</option>

                                <option value="Schools">Education Lockers</option>

                                <option value="Daily">Daily Rentals</option>

                                <option value="Hourly">Hourly Rentals</option>

                            </select>

                        </div>

                        <div class="form-group">
                            @if((session()->has('company_info')))
                                <input type="text" name="location" class="form-control" id="location" placeholder="Location" value="{{session()->get('company_info')->name}} " />

                            @else

                                <input type="text" name="location" class="form-control" id="location" placeholder="Location"  />
                            @endif
                            <div class="validation"></div>

                        </div>

                        <div class="form-group">

                            @if((session()->has('company_info')))
                                <input type="text" name="town" class="form-control" id="town" placeholder="Town" value="{{session()->get('company_info')->city}}"/>
                            @else

                                <input type="text" name="town" class="form-control" id="town" placeholder="Town" />
                            @endif


                            <div class="validation"></div>

                        </div>

                        <div class="form-group">

                            <input type="text" name="first_name" class="form-control" id="first_name" placeholder="First Name" />

                            <div class="validation"></div>

                        </div>

                        <div class="form-group">

                            <input type="text" name="surname" class="form-control" id="surname" placeholder="Surname" />

                            <div class="validation"></div>

                        </div>

                        <div class="form-group">

                            <input type="email" name="email" class="form-control" id="email" placeholder="Email" />

                            {{--<input type="hidden" name="recapcha" class="form-control" id="recapcha" placeholder="Email" />--}}

                            <div class="validation"></div>

                        </div>

                        <div class="form-group">

                            <textarea class="form-control" name="notes" rows="5" data-msg="Please write something for us" placeholder="Notes"></textarea>

                            <div class="validation"></div>
                            <input type="hidden" name="recaptcha" id="recaptcha">

                        </div>

                        <div class="text-center"><button type="submit" class="green-button button_text assign_class" data-id = "request-form-button" data-value = "{{t('request-form-button')}}">{!! t('request-form-button') !!}</button></div>

                    </div>

                </form>

            </div>

        </section>





    </main>





@endsection

{{--<script src="https://www.google.com/recaptcha/api.js?render={{ config('services.recaptcha.sitekey') }}"></script>--}}
{{--<script>--}}
{{--grecaptcha.ready(function() {--}}
{{--grecaptcha.execute('{{ config('services.recaptcha.sitekey') }}', {action: 'contact'}).then(function(token) {--}}
{{--if (token) {--}}
{{--document.getElementById('recaptcha').value = token;--}}
{{--}--}}
{{--});--}}
{{--});--}}
{{--</script>--}}




@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#sel1").select2();
        });
    </script>
@endsection