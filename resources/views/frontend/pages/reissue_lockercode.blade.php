@extends('frontend.layouts.app')

@section('style')

    @endsection

    @section('content')
        <main id="main">


            <!--==========================
              Skills Section
            ============================-->
            <section id="skills">
                <div class="container">

                    <header class="section-header">
                        <p>Please fill in the below form to get your locker code re-issued<br />
                            Select the school year you wish to rent your locker for and pay for multiple years to lock in the rate on each year</p>

                    </header>
                    <form action="" method="post" role="form" class="contactForm">
                        <div class="col-md-8 offset-md-2">


                            <div class="form-group">
                                <input type="email" name="email" class="form-control" id="email" placeholder="Email" />
                                <div class="validation"></div>
                            </div>

                        <div class="text-center"><button type="submit">Issue Locker Code</button></div>
                            </div>
                    </form>
                </div>
            </section>


        </main>


@endsection

@section('script')


@endsection