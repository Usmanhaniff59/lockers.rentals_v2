@extends('frontend.layouts.apphome')

@section('style')
    <link rel="stylesheet" href="/frontend/vendors/bootstrapvalidator/css/bootstrapValidator.min.css"/>
    <!--page level styles-->

    <link rel="stylesheet" href="/frontend/vendors/swiper/css/swiper.min.css" />
    <!--Page level styles-->
    <link rel="stylesheet" href="/frontend/css/pages/general_components.css" />
    <link type="text/css" rel="stylesheet" href="/frontend/vendors/radio_css/css/radiobox.min.css" />
    <link type="text/css" rel="stylesheet" href="/frontend/css/pages/radio_checkbox.css" />
    <style>

        .locations{

            height:150px;

            overflow-y: scroll;

        }
        .text{

            margin-bottom: 0px;

        }
        div .toggle btn btn-success{
            height: 88px;
        }
        .nav-tabs .nav-link.active {
            background-color: #F5B54A;
            color: white;
        }
        .nav-tabs .nav-link.active:hover {
            background-color: #9ABDB7;

        }
        .red-identifier{
            background: red;
        }
        .company-info{

            color: black;
        }
    </style>

@endsection


@section('content')

    <?php

    if(\Illuminate\Support\Facades\Auth::check() == true){

        $user_email = \Illuminate\Support\Facades\Auth::user()->email;

        $user_id = \Illuminate\Support\Facades\Auth::user()->id;

    }else{

        $user_email = 0;

        $user_id = 0;

    }

    //   dd($user);
    ?>

    <main id="main">

        <!-- confirm email modal -->
        <div class="modal" id="resend-confirmation-email-modal" role="dialog" aria-hidden="false">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">{!! t('resend-confirmation-email-button')!!}</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                    </div>
                    <div class="modal-body">

                        <input type="text" id="email_for_confirmation" class="form-control assign_class" type="email" placeholder="Enter your email" pattern="/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/"/>
                        <div id="emailconfirmvalidation" style="color: red" class="assign_class d-none " >Please enter a valid email address</div>
                        <div id="emailconfirmvalidation_not_found" style="color: red" class="assign_class d-none " >Requested email is not found</div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="init_confirm_email">Send</button>
                        <button type="button" class="btn btn-default " id="dismiss_modal_confirm" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end confirm email modal -->

        <!-- reset email modal -->
        {{--<div class="modal" id="reset-email-modal" role="dialog" aria-hidden="false">--}}
            {{--<div class="modal-dialog">--}}
                {{--<!-- Modal content-->--}}
                {{--<div class="modal-content">--}}
                    {{--<div class="modal-header">--}}
                        {{--<h4 class="modal-title">{!! t('resend-confirmation-email-button')!!}</h4>--}}
                        {{--<button type="button" class="close" id="reset_dismiss_modal_confirm2" data-dismiss="modal">&times;</button>--}}

                    {{--</div>--}}
                    {{--<div class="modal-body">--}}

                        {{--<p>Please enter your Mobile Phone Number or Email and we will re-send you your instructions to enter the portal and view your bookings</p>--}}

                        {{--<div class="form-check reset-radio-section">--}}
                            {{--<input type="radio" class="form-check-input" id="email_radio" name="reset-radio"  checked />--}}
                            {{--<label class="form-check-label" for="materialUnchecked" style="margin-top: 0px">Email</label>--}}
                        {{--</div>--}}
                        {{--<div class="form-check reset-radio-section">--}}
                            {{--<input type="radio" class="form-check-input" id="sms_radio" name="reset-radio"  />--}}
                            {{--<label class="form-check-label" for="materialUnchecked" style="margin-top: 0px">SMS</label>--}}
                        {{--</div>--}}
                        {{--<br>--}}
                        {{--<div class="form-check email-input-section">--}}
                            {{--<input type="text" id="reset_email_for_confirmation" class="form-control assign_class" type="email" placeholder="Enter Your Email" pattern="/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/"/>--}}
                            {{--<div id="resetemailconfirmvalidation" style="color: red" class="assign_class d-none " >Please enter a valid email address</div>--}}
                            {{--<div id="reset_email_confirm_validation_not_found" style="color: red" class="assign_class d-none " >Requested email is not found</div>--}}
                            {{--<p>We have send a password reset to your Email. If you have not received the email within 15 minutes, you may have added an invalid email address. Please check your spam email folder if you do not receive an email.</p>--}}
                        {{--</div>--}}
                        {{--<div class="form-check sms-input-section d-none">--}}
                            {{--<input type="text" id="reset_sms_for_confirmation" class="form-control assign_class" placeholder="Enter Your Phone Number" />--}}
                            {{--<div id="emailconfirmvalidation" style="color: red" class="assign_class d-none " >Please enter a valid email address</div>--}}
                            {{--<div id="reset_sms_validation_not_found" style="color: red" class="assign_class d-none " >Phone Number does not match our Records</div>--}}
                            {{--<p>We have send a code to your Mobile Number. If you have not received the code within 15 minutes, you may have added an invalid Mobile Number.</p>--}}

                        {{--</div>--}}
                        {{--<input type="hidden" name="user_id" id="reset_user_id">--}}
                        {{--<div class="form-check passcode-section d-none">--}}
                            {{--<input type="text" id="reset_passcode_for_confirmation" class="form-control assign_class" placeholder="Enter Your Code" />--}}
                            {{--<div id="emailconfirmvalidation" style="color: red" class="assign_class d-none " >Please enter a valid email address</div>--}}
                            {{--<div id="reset_passcode_validation_not_found" style="color: red" class="assign_class d-none" >Code does not match our Records</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        <!-- end confirm email modal -->

        <div class="modal" id="reset-email-modal"   >
            <div class="modal-dialog modal-lg">
                <form class="modal-content" id="edit_company" action="{{ route('updatecompany') }}" method="POST">
                    @csrf
                    <div class="modal-header" style="background-color: #F5B54A;color: white">

                        <h4 class="modal-title text-center assign_class"   data-id = "home-reset-password-header" data-value = "{{t('home-reset-password-header')}}" > {!! t('home-reset-password-header')!!} </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>

                    <div class="modal-body">

                        <p class="assign_class"   data-id = "home-reset-email-instruction-msg" data-value = "{{t('home-reset-email-instruction-msg')}}" > {!! t('home-reset-email-instruction-msg')!!}</p>
                        <div class="radio disabled  reset-radio-section pl-3">
                            <label class="text-warning">
                                <input type="radio"  id="email_radio" name="reset-radio" checked >
                                <span class="cr"><i class="cr-icon fa fa-circle"></i></span>

                                <span style="color: black;">Email</span>
                            </label>
                        </div>
                        <div class="radio disabled  reset-radio-section pl-3">
                            <label class="text-warning">
                                <input type="radio"   id="sms_radio" name="reset-radio" >
                                <span class="cr"><i class="cr-icon fa fa-circle"></i></span>
                                <span style="color: black;">SMS</span>
                            </label>
                        </div>

                        {{--<div class="form-check reset-radio-section">--}}
                            {{--<input type="radio" class="form-check-input" id="email_radio" name="reset-radio"  style="background-color: #F5B54A;color: white" checked />--}}
                            {{--<label class="form-check-label" for="materialUnchecked" style="margin-top: 0px">Email</label>--}}
                        {{--</div>--}}
                        {{--<div class="form-check reset-radio-section">--}}
                            {{--<input type="radio" class="form-check-input" id="sms_radio" name="reset-radio" style="background-color: #F5B54A;color: white"  />--}}
                            {{--<label class="form-check-label" for="materialUnchecked" style="margin-top: 0px">SMS</label>--}}
                        {{--</div>--}}
                        <br>
                        <div class="form-check email-input-section">
                            <input type="text" id="reset_email_for_confirmation" class="form-control assign_class" type="email" placeholder="Enter Your Email" pattern="/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/"/>
                            <div id="resetemailconfirmvalidation" style="color: red" class="assign_class d-none " >Please enter a valid email address</div>
                            <div id="reset_email_confirm_validation_not_found" style="color: red" class="assign_class d-none " >Requested email is not found</div>
                            <p id="email_confirm_msg" class="d-none assign_class"   data-id = "home-email-confirmation-msg" data-value = "{{t('home-email-confirmation-msg')}}" > {!! t('home-email-confirmation-msg')!!} </p>
                        </div>
                        <div class="form-check sms-input-section d-none">
                            <input type="text" id="reset_sms_for_confirmation" class="form-control assign_class" placeholder="Enter Your Phone Number" />
                            {{--<div id="emailconfirmvalidation" style="color: red" class="assign_class d-none " >Please enter a valid email address</div>--}}
                            <div id="reset_sms_validation_not_found" style="color: red" class="assign_class d-none " >Phone Number does not match our Records</div>

                        </div>
                        <input type="hidden" name="user_id" id="reset_user_id">
                        <div class="form-check passcode-section d-none">
                            <input type="text" id="reset_passcode_for_confirmation" class="form-control assign_class" placeholder="Enter Your Code" />
                            {{--<div id="emailconfirmvalidation" style="color: red" class="assign_class d-none " >Please enter a valid email address</div>--}}
                            <div id="reset_passcode_validation_not_found" style="color: red" class="assign_class d-none" >Code does not match our Records</div>
                            <p id="sms_confirm_msg" class="d-none assign_class"   data-id = "home-sms-confirmation-msg" data-value = "{{t('home-sms-confirmation-msg')}}" > {!! t('home-sms-confirmation-msg')!!}</p>

                        </div>
                    </div>

                    <div class="modal-footer reset-btn-section">
                        <button type="button" class="btn btn-default " id="reset_dismiss_modal_confirm" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-default" id="reset_email_btn" style="background-color: #F5B54A;color: white">Send</button>
                    </div>
                    <div class="modal-footer passcode-section d-none">
                        <button type="button" class="btn btn-default" id="reset_passcode_dismiss_modal_confirm" data-dismiss="modal" >Close</button>
                        <button type="button" class="btn btn-default" id="passcode_confirm_btn" style="background-color: #F5B54A;color: white">Send</button>
                    </div>

                </form>
                <br>
            </div>
        </div>

        <section id="about">

            <!--==========================

             Find location

            ============================-->

            <div class="container intro-home">
                {{--<div style="height: 10px">--}}
                {{--<input type="checkbox" checked data-toggle="toggle" data-on="Ready" data-off="Not Ready" data-onstyle="success" data-offstyle="danger">--}}
                {{--</div>--}}
                @if(!Auth::check())
                <div class="card m-t-10 " style="border-radius: 0px; border:0px;">
                    <div class="card-body">
                        <div class="assign_class p-b-20 text-center" data-id = "home-login-reset-text" data-value = "{{t('home-login-reset-text')}}" style="text-align: center;">
                            {!!  str_replace(["%login%","%reset%"],['<a href="/login" style="text-decoration: underline;" >Login</a>','<a href="#." data-toggle="modal" class="reset-clear" data-target="#reset-email-modal" style="text-decoration: underline;" >here</a>'],t('home-login-reset-text'))  !!}
                        </div>

                    </div>
                </div>
                @endif
                <br>
                <div class="section-header">
                    <h3 class="assign_class text-blue-color"  data-id = "find-location-text" data-value = "{{t('find-location-text')}}" data-steps="yes">{!! t('find-location-text') !!}<div class="after-title-step2 red-identifier"></div><div class="after-title-step3  red-identifier"></div><div class="after-title-step4  red-identifier"></div><div class="after-title-step5  red-identifier"></div></h3>
                </div>

                <div class="p-3 mb-5 bg-white text-center">

                    <div class="row">

                        <div class="col-md-2 col-sm-12">&nbsp;</div>

                        <div class="col-md-8 col-sm-12">
                            <br>
                            {{--<h2 class="assign_class"  data-id = "chose-location-text" data-value = "{{t('chose-location-text')}}">{!! t('chose-location-text')!!} </h2>--}}

                            <form method="post" action="{{ route('schooltermbased') }}">

                                <div class="col-md-12 col-sm-12">

                                    <div class="form-group">
                                        <input type="text" name="location" class="form-control assign_class"  data-id = "location-input-place-holder" data-value = "{{t('location-input-place-holder')}}" id="location_search"  placeholder="@t('location-input-place-holder')"  data-inputplaceholder="yes" autocomplete="off"/>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <div id="display_locations"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group" id="next_stage_proceede"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" style="padding-bottom:20px;">
                                            <div><span class="assign_class" data-id="home-policy-review" data-value="{{t('home-policy-review')}}">{!! t('home-policy-review')!!}</span></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="text-center">

                                    <button type="submit" data-toggle="tooltip" title="" class="bg-blue-color assign_class home-find-location" id="location_proceed_button"   data-id ="proceed-button" data-value = "{{t('proceed-button')}}" disabled>{!! t('proceed-button') !!}</button>
                                   <!--  @if(!Auth::check())
                                        <a href="{{url('/login')}}" data-toggle="tooltip" title="" class=" green-button-proceed assign_class home-find-location" id="login_button" style="display:inline-block;"   data-id ="login" data-value = "{{t('login')}}" disabled>{!! t('login') !!}</a>

                                    @endif -->
                                </div>

                                <hr>
                                <input type="hidden" name="_token" id="csrf-schooltermbased" value="{{ Session::token() }}" />

                            </form>



                            <div class="row" id="request-form-div">

                                <div class="col-md-12 col-sm-12">

                                    <div id="school_req_response"></div>
                                    <div id="capcha_error"></div>


                                    <form id="request_form" action="{{route('schoolRequest')}}" method="post">

                                        <div class="text-center d-none" id="school_request" >

                                            <div class="locker-non-availability assign_class" data-id = "home-non-availability-text"   data-value = "{{t('home-non-availability-text')}}" data-text="{!! t('home-non-availability-text') !!}" ></div>

                                            <div class="form-group">

                                                <input type="text" name="school_name" class="form-control assign_class" id="school_name"  data-id = "home-request-schoolname-placeholder" data-value = "{{t('home-request-schoolname-placeholder')}}" placeholder="@t('home-request-schoolname-placeholder')" data-inputplaceholder="yes"/>

                                                <div id="schoolNameValidation" style="color: red" class="assign_class validation_text d-none" data-id = "home-request-school-name-validation" data-value = "{{t('home-request-school-name-validation')}}" >{!! t('home-request-school-name-validation')!!}</div>

                                            </div>

                                            <div class="form-group">

                                                <input type="text" name="line_1_address" class="form-control assign_class" id="line_1_address" data-id = "home-request-line1address-placeholder" data-value = "{{t('home-request-line1address-placeholder')}}" placeholder="@t('home-request-line1address-placeholder')" data-inputplaceholder="yes" />

                                                <p id="line1AddressValidation" style="color: red" class="assign_class validation_text d-none" data-id = "home-request-line1address-validation" data-value = "{{t('home-request-line1address-validation')}}" >{!! t('home-request-line1address-validation')!!}</p>

                                            </div>

                                            <div class="form-group">

                                                <input type="text" name="line_2_address" class="form-control assign_class" id="line_2_address" placeholder="{!! t('home-request-line2-address-placeholder')!!}"   data-id = "home-request-line2-address-placeholder" data-value = "{{t('home-request-line2-address-placeholder')}}" data-inputplaceholder="yes" />

                                                <div id="line2AddressValidation" style="color: red" class="assign_class validation_text d-none" data-id = "home-request-line2address-validation" data-value = "{{t('home-request-line2address-validation')}}" >{!! t('home-request-line2address-validation')!!}</div>

                                            </div>

                                            <div class="form-group">

                                                <input type="text" name="town" class="form-control assign_class" id="town"  placeholder="{!! t('home-request-town-placeholder')!!}"   data-id = "home-request-town-placeholder" data-value = "{{t('home-request-town-placeholder')}}" data-inputplaceholder="yes"/>

                                                <div id="townValidation" style="color: red" class="assign_class d-none validation_text" data-id = "home-request-town-validation" data-value = "{{t('home-request-town-validation')}}" >{!! t('home-request-town-validation')!!}</div>

                                            </div>

                                            <div class="form-group">

                                                <input type="text" name="post_code" class="form-control assign_class" id="post_code" placeholder="{!! t('home-request-post-code-placeholder')!!}"   data-id = "home-request-post-code-placeholder" data-value = "{{t('home-request-post-code-placeholder')}}" data-inputplaceholder="yes" />

                                                <div id="postCodeValidation" style="color: red" class="assign_class d-none validation_text" data-id = "home-request-post-code-validation" data-value = "{{t('home-request-post-code-validation')}}" >{!! t('home-request-post-code-validation')!!}</div>

                                            </div>

                                            <div class="form-group">

                                                <input type="text" name="country" class="form-control assign_class" id="country" placeholder="{!! t('home-request-country-placeholder')!!}"   data-id = "home-request-country-placeholder" data-value = "{{t('home-request-country-placeholder')}}" data-inputplaceholder="yes" />

                                                <div id="countryValidation" style="color: red" class="assign_class d-none validation_text" data-id = "home-request-country-validation" data-value = "{{t('home-request-country-validation')}}" >{!! t('home-request-country-validation')!!}</div>

                                            </div>


                                            <input type="hidden" name="user_id" class="form-control" id="user_id" value="{{ $user_id }}"/>
                                            <input type="hidden" name="user_email" class="form-control" id="user_email" value="{{ $user_email }}"/>
                                            <input type="hidden" name="recaptcha" id="recaptcha">


                                            <p class="text assign_class" id="email_msg" data-id = "home-logged-email-text"   data-value = "{{t('home-logged-email-text')}}" data-text="{!! t('home-logged-email-text')!!}" ></p>

                                            <div id="login_request" class="d-none">

                                                <p id="">Please complete the below contact details if you wish to be contacted with updates</p>

                                                <div class="card m-t-35" id="login_card">

                                                    <div class="card-header bg-white">
                                                        <ul class="nav nav-tabs card-header-tabs float-left">
                                                            <li class="nav-item">
                                                                <a class="nav-link active button_text assign_class get_text" href="#tab1" data-toggle="tab" data-id = "home-request-sign-in-tab-text" data-value = "{{t('home-request-sign-in-tab-text')}}" >{!! t('home-request-sign-in-tab-text') !!}</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link button_text assign_class get_text" href="#tab2" data-toggle="tab" data-id = "home-request-create-account-tab-text" data-value = "{{t('home-request-create-account-tab-text')}}" >{!! t('home-request-create-account-tab-text') !!}</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link button_text assign_class get_text" href="#tab3" data-toggle="tab">Forgot password</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="tab-content text-justify">
                                                            <div class="tab-pane active" id="tab1">
                                                                <h2 class="card-title text-center assign_class assign_class" style="margin: 20px"  data-id = "home-request-sign-in-title" data-value = "{{t('home-request-sign-in-title')}}" >{!! t('home-request-sign-in-title') !!}</h2>
                                                                <form class="login" action="{{ url('/login') }}" method="post" data-type="json" id="signInForm">
                                                                    <div class="form-group">
                                                                        <input type="text" name="email" class="form-control assign_class" id="signInEmail" placeholder="{!! t('home-request-sign-in-email-placeholder')!!}"   data-id = "home-request-sign-in-email-placeholder" data-value = "{{t('home-request-sign-in-email-placeholder')}}" data-inputplaceholder="yes"/>
                                                                        <div id="signInEmailValidation" style="color: red" class="assign_class d-none validation_text" data-id = "home-request-sign-in-email-validation" data-value = "{{t('home-request-sign-in-email-validation')}}" >{!! t('home-request-sign-in-email-validation')!!}</div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <input type="password" name="password" class="form-control assign_class" id="signInPassword" placeholder="{!! t('home-request-sign-in-password-placeholder')!!}" data-id = "home-request-sign-in-password-placeholder" data-value = "{{t('home-request-sign-in-password-placeholder')}}" data-inputplaceholder="yes"/>
                                                                        <div id="signInPasswordValidation" style="color: red" class="assign_class d-none validation_text" data-id = "home-request-sign-in-password-validation" data-value = "{{t('home-request-sign-in-password-validation')}}" >{!! t('home-request-sign-in-password-validation')!!}</div>
                                                                    </div>
                                                                    <div class="form-group text-center">
                                                                        <button type="button" class="green-button button_text assign_class" style="margin: 20px;padding: 7px 70px;" id="signIn-button" data-id = "home-request-sign-in-button" data-value = "{{t('home-request-sign-in-button')}}" >{!! t('home-request-sign-in-button')!!}</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <div class="tab-pane" id="tab2">
                                                                <h2 class="card-title text-center assign_class assign_class" style="margin: 20px"  data-id = "home-request-sign-up-title" data-value = "{{t('home-request-sign-up-title')}}" >{!! t('home-request-sign-up-title') !!}</h2>
                                                                <p class="card-title text-center assign_class assign_class" style="margin: 20px"  data-id = "home-request-sign-up-sub-title-msg" data-value = "{{t('home-request-sign-up-sub-title-msg')}}" >{!! t('home-request-sign-up-sub-title-msg') !!}</p>
                                                                <div class="form-group">
                                                                    <input type="text" name="first_name" class="form-control assign_class" id="signUpFirstName" placeholder="{!! t('home-request-sign-up-first-name-placeholder')!!}"   data-id = "home-request-sign-up-first-name-placeholder" data-value = "{{t('home-request-sign-up-first-name-placeholder')}}" data-inputplaceholder="yes"/>
                                                                    <div id="signUpFirstNameValidation" style="color: red" class="assign_class d-none validation_text" data-id = "home-request-sign-up-first-name-validation" data-value = "{{t('home-request-sign-up-first-name-validation')}}" >{!! t('home-request-sign-up-first-name-validation')!!}</div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="text" name="surname" class="form-control assign_class" id="signUpSurname" placeholder="{!! t('home-request-sign-up-surname-placeholder')!!}"   data-id = "home-request-sign-up-surname-placeholder" data-value = "{{t('home-request-sign-up-surname-placeholder')}}" data-inputplaceholder="yes"/>
                                                                    <div id="signUpSurnameValidation" style="color: red" class="assign_class d-none validation_text" data-id = "home-request-sign-up-surname-validation" data-value = "{{t('home-request-sign-up-surname-validation')}}" >{!! t('home-request-sign-up-surname-validation')!!}</div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="text" name="email" class="form-control assign_class" id="signUpEmail" placeholder="{!! t('home-request-sign-up-email-placeholder')!!}"   data-id = "home-request-sign-up-email-placeholder" data-value = "{{t('home-request-sign-up-email-placeholder')}}" data-inputplaceholder="yes"/>
                                                                    <div id="signUpEmailValidation" style="color: red" class="assign_class d-none validation_text" data-id = "home-request-sign-up-email-validation" data-value = "{{t('home-request-sign-up-email-validation')}}" >{!! t('home-request-sign-up-email-validation')!!}</div>
                                                                    <div class="card-title text-center assign_class assign_class validation_text d-none" id="user_exist_msg" style="margin: 20px;color: red"  data-id = "home-request-sign-up-user-exist-msg" data-value = "{{t('home-request-sign-up-user-exist-msg')}}" >{!! t('home-request-sign-up-user-exist-msg') !!}</div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="password" name="password" class="form-control assign_class" id="signUpPassword" placeholder="{!! t('home-request-sign-up-password-placeholder')!!}" data-id = "home-request-sign-up-password-placeholder" data-value = "{{t('home-request-sign-up-password-placeholder')}}" data-inputplaceholder="yes"/>
                                                                    <div id="signUpPasswordValidation" style="color: red" class="assign_class d-none validation_text" data-id = "home-request-sign-up-password-validation" data-value = "{{t('home-request-sign-up-password-validation')}}" >{!! t('home-request-sign-up-password-validation')!!}</div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="password" name="password_confirmation" class="form-control assign_class" id="signUpConfirmPassword" placeholder="{!! t('home-request-sign-up-confirm-password-placeholder')!!}" data-id = "home-request-sign-up-confirm-password-placeholder" data-value = "{{t('home-request-sign-up-confirm-password-placeholder')}}" data-inputplaceholder="yes"/>
                                                                    <div id="signUpConfirmPasswordValidation" style="color: red" class="assign_class d-none validation_text" data-id = "home-request-sign-up-confirm-password-validation" data-value = "{{t('home-request-sign-up-confirm-password-validation')}}" >{!! t('home-request-sign-up-confirm-password-validation')!!}</div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="text" name="phone_number" class="form-control assign_class" id="signUpPhoneNumber" placeholder="{!! t('home-request-sign-up-phone-number-placeholder')!!}"   data-id = "home-request-sign-up-phone-number-placeholder" data-value = "{{t('home-request-sign-up-phone-number-placeholder')}}" data-inputplaceholder="yes"/>
                                                                    <div id="signUpPhoneNumberValidation" style="color: red" class="assign_class d-none validation_text" data-id = "home-request-sign-up-phone-number-validation" data-value = "{{t('home-request-sign-up-phone-number-validation')}}" >{!! t('home-request-sign-up-phone-number-validation')!!}</div>
                                                                </div>
                                                                <div class="form-group text-center">
                                                                    <button type="button" class="green-button button_text assign_class" style="margin: 20px;padding: 7px 70px;" id="signUp-button" data-id = "home-request-sign-up-button" data-value = "{{t('home-request-sign-up-button')}}" >{!! t('home-request-sign-up-button')!!}</button>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane" id="tab3">
                                                                <h2 class="card-title text-center assign_class assign_class" style="margin: 20px"  data-id = "home-request-forget-password-title" data-value = "{{t('home-request-forget-password-title')}}" >{!! t('home-request-forget-password-title') !!}</h2>
                                                                <p class="card-title text-center assign_class assign_class" style="margin: 20px"  data-id = "home-request-forget-password-sub-title-msg" data-value = "{{t('home-request-forget-password-sub-title-msg')}}" >{!! t('home-request-forget-password-sub-title-msg') !!}</p>
                                                                <form class="form-horizontal" id="ResetPassForm" role="form" method="POST" action="{{ route('password.email') }}">
                                                                    {{ csrf_field() }}
                                                                    <div class="form-group">
                                                                        <input type="email" name="email" id="passwordResetEmail" class="form-control assign_class" value="{{ old('email') }}" required autofocus id="signUpEmail" placeholder="{!! t('home-request-forget-password-email-placeholder')!!}"   data-id = "home-request-forget-password-email-placeholder" data-value = "{{t('home-request-forget-password-email-placeholder')}}" data-inputplaceholder="yes"/>
                                                                        <div id="passwordResetEmailValidation" style="color: red" class="assign_class d-none validation_text" data-id = "home-request-forget-password-email-validation" data-value = "{{t('home-request-forget-password-email-validation')}}" >{!! t('home-request-forget-password-email-validation')!!}</div>
                                                                        <div id="emailExistValidation" style="color: red" class="assign_class d-none validation_text" data-id = "home-request-forget-password-email-exist-validation" data-value = "{{t('home-request-forget-password-email-exist-validation')}}" >{!! t('home-request-forget-password-email-exist-validation')!!}</div>
                                                                        <div id="emailSubmitValidation" style="color: red" class="assign_class d-none validation_text" data-id = "home-request-forget-password-send-email-validation" data-value = "{{t('home-request-forget-password-send-email-validation')}}" >{!! t('home-request-forget-password-send-email-validation')!!}</div>
                                                                    </div>
                                                                    {{--<input type="email" class="form-control b_r_20" id="email" name="email" value="{{ old('email') }}" required autofocus>--}}
                                                                    @if ($errors->has('email'))
                                                                        <span class="help-block" style="color: white">
                                                                           <strong>{{ $errors->first('email') }}</strong>
                                                                        </span>
                                                                    @endif
                                                                    <div class="form-group text-center">
                                                                        <button type="button" class="green-button button_text assign_class" style="margin: 20px;padding: 7px 70px;" id="password_reset_button" data-id = "home-request-forget-password-button" data-value = "{{t('home-request-forget-password-button')}}" >{!! t('home-request-forget-password-button')!!}</button>
                                                                    </div>
                                                                    {{--<div class="text-center login_bottom">--}}
                                                                    {{--<button type="submit" class="btn btn-mint btn-block b_r_20 m-t-10 m-r-20">Send Password Reset Link</button>--}}
                                                                    {{--</div>--}}
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <button type="submit" class="green-button button_text assign_class" id="submit_request" data-id = "home-request-new-school-button" data-value = "{{t('home-request-new-school-button')}}" >{!! t('home-request-new-school-button')!!}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
            <div class="container intro-home">
                @if(\Illuminate\Support\Facades\Auth::check() == true)
                    @foreach($lockerInfo as $lockers)
                        <div class="card m-t-10">
                            <div class="card-body">
                                <h3 class="assign_class" data-id = "home-booking-detail" data-value = "{{t('home-booking-detail')}}" >{!! t('home-booking-detail')!!}</h3>
                                <div><span class="company-info assign_class" data-id = "home-booking-id-card" data-value = "{{t('home-booking-id-card')}}" >{!! t('home-booking-id-card')!!} </span> : {{$lockers[0]->booking_id}}</div>
                                <div><span class="company-info assign_class" data-id = "home-company-info-card" data-value = "{{t('home-company-info-card')}}" >{!! t('home-company-info-card')!!} </span> : {{$lockers[0]->company_name}}</div>
                                <br>
                                @foreach($lockers as $locker)
                                    <div><span class="company-info assign_class" data-id = "home-booking-info-child-name" data-value = "{{t('home-booking-info-child-name')}}" >{!! t('home-booking-info-child-name')!!}  </span> : {{$locker->childName}}</div>
                                    <div><span class="company-info assign_class" data-id = "home-booking-info-locker-block" data-value = "{{t('home-booking-info-locker-block')}}" >{!! t('home-booking-info-locker-block')!!} </span> : {{$locker->block_name}}</div>
                                    <div><span class="company-info assign_class" data-id = "home-booking-info-locker-number" data-value = "{{t('home-booking-info-locker-number')}}" >{!! t('home-booking-info-locker-number')!!} </span> : {{$locker->locker_number}}</div>
                                    <div><span class="company-info assign_class" data-id = "home-start-date-card" data-value = "{{t('home-start-date-card')}}" >{!! t('home-start-date-card')!!} </span> : {{date('d M, Y', strtotime($locker->start))}}</div>
                                    <div><span class="company-info assign_class" data-id = "home-end-date-card" data-value = "{{t('home-end-date-card')}}" >{!! t('home-end-date-card')!!} </span> : {{date('d M, Y', strtotime($locker->end))}}</div><br>
                                @endforeach
                                <p><a href ="{{url('/admin/reserved/lockers/schooltermbasedreserved/'.$lockers[0]->booking_id)}}"><button type="submit" data-toggle="tooltip" title="" class="green-button-proceed assign_class" data-id = "home-purchase-button-card" data-value = "{{t('home-purchase-button-card')}}"  data-original-title="">{!! t('home-purchase-button-card')!!} </button></a></p>
                                {{--<p><form method="POST" action="{{ route('reserved.lockers.schooltermbasedreserved') }}">--}}
                                {{--<input type="hidden" name="id" value="{{$lockers[0]->booking_id}}"/>--}}
                                {{--@csrf--}}
                                {{--<button type="submit" data-toggle="tooltip" title="" class="green-button-proceed assign_class" data-id = "home-purchase-button-card" data-value = "{{t('home-purchase-button-card')}}"  data-original-title="">{!! t('home-purchase-button-card')!!} </button>--}}
                                {{--</form> </p>--}}
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="card m-t-10" style="border: 0px; border-radius: 0px;">
                        <div class="card-body" style="box-shadow: none;">

                            <div class="assign_class p-b-20" data-id = "home-ofline-login-text" data-value = "{{t('home-ofline-login-text')}}" style="text-align: center;">
                                {!!  str_replace(["%login%"],['<a href="/login" >Login</a>'],t('home-ofline-login-text'))  !!}
                            </div>
                        </div>
                    </div>
                @endif

            </div>
            </div>
            {{--{{dd(Session::get('companyId'))}}--}}

        </section>

    </main>



@endsection

@section('script')

    <!--search location script-->

    <script src="https://www.google.com/recaptcha/api.js?render={{ config('services.recaptcha.sitekey') }}"></script>
    <script src="/frontend/vendors/bootstrapvalidator/js/bootstrapValidator.min.js"></script>
    <script src="/frontend/vendors/twitter-bootstrap-wizard/js/jquery.bootstrap.wizard.min.js"></script>
    <!--End of plugin scripts-->
    <!--Page level scripts-->
    <script src="/frontend/vendors/swiper/js/swiper.min.js"></script>
    <script src="/frontend/js/pages/cards.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
    <script type="text/javascript" src="/frontend/js/pages/radio_checkbox.js"></script>
    <script>
        grecaptcha.ready(function() {
            grecaptcha.execute('{{ config('services.recaptcha.sitekey') }}', {action: 'contact'}).then(function(token) {
                if (token) {
                    document.getElementById('recaptcha').value = token;
                }
            });
        });
    </script>

    <script type="application/javascript">

        $(window).bind("pageshow", function(event) {
    if (event.originalEvent.persisted) {
        // window.location.reload() 
       $('#display_locations').html('');
       $('#next_stage_proceede').html('');
       
    }
});
        $( document ).ready(function() {
            // alert('123');
            
            var location = "{{Session::get('location')}}";
            $('[data-toggle="tooltip"]').tooltip();
            if(location != ''){

                var event = jQuery.Event( "keyup" );
                $( "#location_search" ).trigger( event );
                var companyId = "{{Session::get('companyId')}}";

                // debugger;
                setTimeout(function() {
                    //your code to be executed after 1 second
                    $("#companyId"+companyId).prop('checked', true);
                    var event = jQuery.Event( "click" );
                    $( "#companyId"+companyId).trigger(event);
                }, 1500);


            }

            sessionStorage.removeItem("selected_dropdown_current_tab");
            sessionStorage.removeItem("selected_dropdown_start");
            sessionStorage.removeItem("selected_dropdown_end");
            sessionStorage.removeItem("selected_dropdown_rate");
            sessionStorage.removeItem("selected_dropdown_tax");
            sessionStorage.removeItem("selected_dropdown_block_id");
            sessionStorage.removeItem("selected_dropdown_sale_id");
            sessionStorage.removeItem("selected_dropdown_counter");
            sessionStorage.removeItem("selected_dropdown_hold_locker");
            sessionStorage.removeItem("selected_lockers");

        });

        $('body').on('keyup','#location_search',function(e){

            var location = $('#location_search').val();

            $('#display_locations').html('');

            $('#locker-non-availability').html('');

            $('#next_stage_proceede').html('');

            $('#school_req_response').html('');

            $('#capcha_error').html('');

            $('#request_form')[0].reset();

            $('#school_request').addClass('d-none');

            $('#login_request').addClass('d-none');

            $('#location_proceed_button').attr("disabled", true);

            $('#location_proceed_button').css({"opacity":"0.5","cursor":"no-drop"});
            $('#location_proceed_button').attr('title','Please select location');
            $('#location_proceed_button').attr('data-toggle','tooltip');

            $("#location_proceed_button").hover(function() {

                $(this).css("background", "");

            }, function() {

                $(this).css("background", "");

            });

            if(location.length >= 3) {

                $('#next_stage_proceede').html('');

                // var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });


                $.ajax({

                    url: '/frontend/searchlocation',

                    type: 'POST',

                    dataType: 'JSON',

                    cache: false,

                    data: {location: location },

                    success: function (data) {

                        if(data.locations.length != 0) {

                            $('#display_locations').html(data.radio_locations);

                        }else{

                            $('#display_locations').html('');

                            var searchedLocation = $('#location_search').val();
                            $('#school_name').val(searchedLocation);

                            var user_id = $('#user_id').val();

                            //if user not login
                            if(user_id == 0 ){

                                // alert('LOGIN');
                                $('#submit_request').addClass('d-none');

                                var str = $('.locker-non-availability').data('text');

                                var data_id = $('.locker-non-availability').data('id');

                                // var token = $("meta[name='csrf-token']").attr("content");
                                $.ajaxSetup({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    }
                                });

                                $.ajax({

                                    url:"/frontend/placeholders",

                                    method:"POST",

                                    data:{data_id:data_id},

                                    success:function(data){



                                        var placeholder = data.placeholder;

                                        var text = str.replace(placeholder, location);

                                        $('.locker-non-availability').text(text);

                                        var placeholderValues =location;

                                        $('.locker-non-availability').data('placeholderval', placeholderValues);

                                        $('.locker-non-availability').data('placeholders', placeholder);

                                        // $('#modal_placeholders').val(placeholder);

                                        $('#school_request').removeClass('d-none')

                                    }

                                });

                                $('#school_request').removeClass('d-none');

                                $('#login_request').removeClass('d-none')

                            }else{


                                var str = $('.locker-non-availability').data('text');

                                var data_id = $('.locker-non-availability').data('id');

                                // var token = $("meta[name='csrf-token']").attr("content");
                                $.ajaxSetup({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    }
                                });
                                $.ajax({

                                    url:"/frontend/placeholders",

                                    method:"POST",

                                    data:{data_id:data_id},

                                    success:function(data){



                                        var placeholder = data.placeholder;

                                        var text = str.replace(placeholder, location);

                                        $('.locker-non-availability').text(text);

                                        var placeholderValues =location;

                                        $('.locker-non-availability').data('placeholderval', placeholderValues);

                                        $('.locker-non-availability').data('placeholders', placeholder);

                                        // $('#modal_placeholders').val(placeholder);

                                        $('#school_request').removeClass('d-none');


                                    }

                                });

                                var loggedUserEmail = $('#user_email').val();

                                var email = $('#email').val(loggedUserEmail);

                                var data_id = $('#email_msg').data('id');

                                // var token = $("meta[name='csrf-token']").attr("content");
                                $.ajaxSetup({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    }
                                });

                                $.ajax({

                                    url:"/frontend/placeholders",

                                    method:"POST",

                                    data:{data_id:data_id},

                                    success:function(data){

                                        var placeholder = data.placeholder;

                                        var str = $('#email_msg').data('text');

                                        var text = str.replace(placeholder , loggedUserEmail);

                                        $('#email_msg').text(text);

                                        var placeholderValues = loggedUserEmail;

                                        $('#email_msg').data('placeholderval', placeholderValues);

                                        $('#email_msg').data('placeholders', placeholder);

                                    }
                                });
                            }
                        }

                    },

                    error: function (jqXHR, textStatus, errorThrown) {



                    }

                });

            }

        });

        $('body').on('click','.radio-locations',function(e){

            var location_id = $(this).data('location_id');

            // var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $.ajax({

                url: '/frontend/getlocationgroup',

                type: 'POST',

                dataType: 'JSON',

                cache: false,

                data: {location_id: location_id},

                success: function (data) {

                    var str = '';

                    str+='<div class="card" >';

                    str+='<div class="card-body">';

                    str+='<h6 class="card-title text-left">'+ data.location_group.name +'</h6>';

                    str+='<h6 class="card-title text-left">'+data.name +'</h6>';

                    str+='<p class="text-left text-muted"  ><b>Address  </b><br>'+ data.street +' '+ data.city +','+ data.country +'</p>';

                    str+='</div>';

                    str+='</div>';

                    $('#next_stage_proceede').html(str);

                    if($('#edit_cms_switch').is(":checked")){



                        $('.get_text').prop("disabled", false);

                        $('#location_proceed_button').addClass("get_text");

                    }else{

                        $('.get_text').prop("disabled", true);

                        $('#location_proceed_button').removeClass("get_text");

                    }

                    $('#location_proceed_button').attr("disabled", false);

                    $("#location_proceed_button").css({"opacity": "1","cursor":"pointer"});
                    $('#location_proceed_button').attr('title','');
                    $('#location_proceed_button').attr('data-toggle','');

                    $("#location_proceed_button").hover(function() {

                        $(this).css("background", "#F5B54A");

                    }, function() {

                        $(this).css("background", "#F5B54A");

                    });



                },

                error: function (jqXHR, textStatus, errorThrown) {


                }

            });

        });

        $('body').on('click','#signIn-button',function(e){
            e.preventDefault();
            $('.validation_text').addClass('d-none');
            var email = $.trim($('#signInEmail').val());
            var email_validation = isEmail(email);

            var password = $.trim($('#signInPassword').val());

            if ( email_validation == false || password.length <= 4 ) {

                if (email_validation == false) {

                    $('#signInEmailValidation').removeClass('d-none');
                    $('#signInEmail').focus();

                }

                if (password.length <= 4) {

                    $('#signInPasswordValidation').removeClass('d-none');
                    $('#signInPassword').focus();

                }

                return false;
            }

            // var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({

                url: '/login',
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {email: email, password: password},
                success: function (response) {

                    $('meta[name="csrf-token"]').attr('content',response.token);
                    $('#csrf-schooltermbased').val(response.token);

                    if(response.auth == true) {

                        $('#user_id').val(response.user.id);
                        $('#user_email').val(response.user.email);
                        $('#login_request').addClass('d-none');
                        $('#submit_request').removeClass('d-none');

                        replaced_email_msg(response.user.email);
                        $('#email_msg').removeClass('d-none');
                    }
                },
                error: function (jqXHR) {

                    var response = $.parseJSON(jqXHR.responseText);
                    if(response.message) {
                        alert(response.message);
                    }
                }
            });

        });

        $('body').on('click','#init_confirm_email',function(e){
            e.preventDefault();
            var email = $('#email_for_confirmation').val();
            var check = isEmail(email);
            console.log(check, email);

            if(check){
                $('#emailconfirmvalidation').addClass('d-none');

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: '/frontend/resendEmail',
                    type: 'POST',
                    dataType: 'JSON',
                    cache: false,
                    data: {email: email},
                    success: function (response) {
                        console.log(response);
                        if(response){
                            $("#dismiss_modal_confirm").click();
                            $("#resend-confirmation-email-modal .close").click();
                            swal({
                                title: "Success.",
                                text: "You email has been sent\n Please check your spam and trash if you haven’t received the confirmation email",
                                type: "success",
                            });
                        } else {
                            $('#emailconfirmvalidation_not_found').removeClass('d-none');
                            $('#email_for_confirmation').focus();
                        }

                    },
                    error: function (jqXHR) {
                        console.log(jqXHR);
                        $('#emailconfirmvalidation_not_found').removeClass('d-none');
                        $('#email_for_confirmation').focus();
                    }
                });

            } else {
                $('#emailconfirmvalidation').removeClass('d-none');
                $('#email_for_confirmation').focus();
            }

        });

        $('body').on('click','#reset_email_btn',function(e){

            e.preventDefault();
            $('#reset_sms_validation_not_found').addClass('d-none');
            $('#resetemailconfirmvalidation').addClass('d-none');

            var email = $.trim($('#reset_email_for_confirmation').val());


            var phone_number = $.trim($('#reset_sms_for_confirmation').val());

            if(email.length > 0 ) {
                var check = isEmail(email);
                console.log(check, email);


                if (check) {



                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url: '/frontend/sendresetemail',
                        type: 'POST',
                        dataType: 'JSON',
                        cache: false,
                        data: {email: email},
                        success: function (response) {
                            console.log(response);


                        },
                        error: function (jqXHR) {
                            console.log(jqXHR);

                        }
                    });
                    setTimeout(
                        function()
                        {
                            $('#email_confirm_msg').removeClass('d-none');
                            $('#reset_email_for_confirmation').val('');
                            $('#reset_sms_for_confirmation').val('');
                        }, 2000);


                } else {
                    $('#resetemailconfirmvalidation').removeClass('d-none');
                    $('#reset_email_for_confirmation').focus();
                }
            }else  if(phone_number.length > 0){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: '/frontend/sendresetsms',
                    type: 'POST',
                    dataType: 'JSON',
                    cache: false,
                    data: {phone_number: phone_number},
                    success: function (response) {
                        if(response=='user_not_found'){

                            $('#reset_sms_validation_not_found').removeClass('d-none');

                        }else{
                            var dest_url = "{{ url('/logout') }}";
                            $.ajax({
                                type:"POST",
                                url:dest_url,
                                success: function(data){

                                },
                                error: function(data){

                                }
                            })
                            $('#reset_user_id').val(response.user_id);
                            $('#sms_confirm_msg').removeClass('d-none');
                            $('.sms-input-section').addClass('d-none');
                            $('.email-input-section').addClass('d-none');
                            $('.reset-btn-section').addClass('d-none');
                            $('.reset-radio-section').addClass('d-none');

                            $('.passcode-section').removeClass('d-none');
                        }



                    },
                    error: function (jqXHR) {
                        console.log(jqXHR);

                    }
                });


            }

        });


        $('body').on('click','#passcode_confirm_btn',function(e){

            e.preventDefault();

            $('#reset_passcode_validation_not_found').addClass('d-none');

            var reset_user_id = $.trim($('#reset_user_id').val());
            var reset_code = $.trim($('#reset_passcode_for_confirmation').val());

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: '/frontend/validatepasscode',
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {reset_user_id: reset_user_id,reset_code: reset_code},
                success: function (response) {
                    console.log(response);
                    if(response.status == 'matched'){


                        console.log(response.url,'url');
                        window.location.href = response.url;

                    }else{
                        $('#reset_passcode_validation_not_found').removeClass('d-none');
                    }



                },
                error: function (jqXHR) {
                    console.log(jqXHR);

                }
            });


        });

        $('body').on('click','.reset-clear',function(e){

            $('#reset_sms_for_confirmation').val('');
            $('#reset_email_for_confirmation').val('');
            $('#reset_passcode_for_confirmation').val('');
            $('.reset-radio-section').removeClass('d-none');
            $('.email-input-section').removeClass('d-none');
            $('.reset-btn-section').removeClass('d-none');
            $("#email_radio"). prop("checked", true)
            $('#email_confirm_msg').addClass('d-none');
            $('#sms_confirm_msg').addClass('d-none');
            $('.sms-input-section').addClass('d-none');
            $('.passcode-section').addClass('d-none');
            $('#reset_sms_validation_not_found').addClass('d-none');
            $('#resetemailconfirmvalidation').addClass('d-none');

        });

        $('body').on('click','#email_radio',function(e){

            $('#reset_sms_for_confirmation').val('');
            $('.email-input-section').removeClass('d-none');
            $('.sms-input-section').addClass('d-none');
            $('#reset_sms_validation_not_found').addClass('d-none');
            $('#resetemailconfirmvalidation').addClass('d-none');

        });

        $('body').on('click','#sms_radio',function(e){

            $('#reset_email_for_confirmation').val('');
            $('.email-input-section').addClass('d-none');
            $('.sms-input-section').removeClass('d-none');
            $('#reset_sms_validation_not_found').addClass('d-none');
            $('#resetemailconfirmvalidation').addClass('d-none');

        });

        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }

        $('body').on('click','#password_reset_button',function(e){
            e.preventDefault();
            $('.validation_text').addClass('d-none');
            var email = $.trim($('#passwordResetEmail').val());
            var email_validation = isEmail(email);

            if ( email_validation == false ) {

                if (email_validation == false) {

                    $('#passwordResetEmailValidation').removeClass('d-none');
                    $('#passwordResetEmail').focus();

                }


                return false;
            }

            // var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: '/frontend/userExist',
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {email: email},
                success: function (response) {


                    if(response == false) {

                        $('#emailExistValidation').removeClass('d-none');
                    }else{
                        $.ajax({
                            url: '/password/email',
                            type: 'POST',
                            dataType: 'JSON',
                            cache: false,
                            data: {email: email},
                            success: function (response) {


                            },
                            error: function (jqXHR) {

                                var response = $.parseJSON(jqXHR.responseText);
                                if(response.message) {


                                }
                            }
                        });
                        $('#emailSubmitValidation').removeClass('d-none');
                        $('#passwordResetEmail').val('');
                        // $('#ResetPassForm').submit();

                    }
                },
                error: function (jqXHR) {

                    var response = $.parseJSON(jqXHR.responseText);
                    if(response.message) {

                    }
                }
            });

        });

        $('body').on('click','#signUp-button',function(e){
            e.preventDefault();
            $('.validation_text').addClass('d-none');
            var signUpFirstName = $.trim($('#signUpFirstName').val());
            var signUpSurname = $.trim($('#signUpSurname').val());
            var email = $.trim($('#signUpEmail').val());
            var email_validation = isEmail(email);

            var password = $.trim($('#signUpPassword').val());
            var confirmPassword = $.trim($('#signUpConfirmPassword').val());
            var signUpPhoneNumber = $.trim($('#signUpPhoneNumber').val());
            var ajax = 1;

            if ( signUpFirstName.length == 0 || signUpSurname.length == 0 ||email_validation == false || password.length <= 4 || password != confirmPassword || signUpPhoneNumber.length == 0 ) {


                if (signUpFirstName.length == 0) {

                    $('#signUpFirstNameValidation').removeClass('d-none');
                    $('#signUpFirstName').focus();

                }
                if (signUpSurname.length == 0) {

                    $('#signUpSurnameValidation').removeClass('d-none');
                    $('#signUpSurname').focus();

                }


                if (email_validation == false) {

                    $('#signUpEmailValidation').removeClass('d-none');
                    $('#signUpEmail').focus();

                }

                var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
                if(!password.match(passw))
                {
                    $('#signUpPasswordValidation').removeClass('d-none');
                    $('#signUpPassword').focus();
                }

                // if (password.length <= 4) {

                //     $('#signUpPasswordValidation').removeClass('d-none');
                //     $('#signUpPassword').focus();

                // }

                if (password != confirmPassword) {

                    $('#signUpConfirmPasswordValidation').removeClass('d-none');
                    $('#signUpConfirmPassword').focus();
                }

                if (signUpPhoneNumber.length == 0) {

                    $('#signUpPhoneNumberValidation').removeClass('d-none');
                    $('#signUpPhoneNumber').focus();

                }

                return false;
            }

            // var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({

                url: '/frontend/createAjax',
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {name: signUpFirstName ,surname: signUpSurname , email: email, password: password, password_confirmation: confirmPassword,phone_number: signUpPhoneNumber,ajax : ajax },
                success: function (response) {

                    $('meta[name="csrf-token"]').attr('content',response.token);
                    $('#csrf-schooltermbased').val(response.token);

                    if(response.user_exist == true) {
                        $('#user_exist_msg').removeClass('d-none');
                        $('#signUpEmail').focus();
                    }
                    if(response.auth == true) {
                        $('#user_id').val(response.user.id);
                        $('#user_email').val(response.user.email);
                        $('#login_request').addClass('d-none');
                        $('#submit_request').removeClass('d-none');

                        replaced_email_msg(response.user.email);
                        $('#email_msg').removeClass('d-none');
                    }
                },
                error: function (jqXHR) {

                    var response = $.parseJSON(jqXHR.responseText);
                    if(response.message) {
                        alert(response.message);
                    }
                }
            });

        });

        function replaced_email_msg(loggedUserEmail){

            var data_id = $('#email_msg').data('id');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({

                url:"/frontend/placeholders",

                method:"POST",

                data:{data_id:data_id},

                success:function(data){

                    var placeholder = data.placeholder;

                    var str = $('#email_msg').data('text');

                    var text = str.replace(placeholder , loggedUserEmail);


                    $('#email_msg').text(text);

                    var placeholderValues = loggedUserEmail;

                    $('#email_msg').data('placeholderval', placeholderValues);

                    $('#email_msg').data('placeholders', placeholder);

                }

            });

        }

        $('body').on('click','#submit_request',function(e){

            e.preventDefault();
            $('.validation_text').addClass('d-none');
            var school_name =$.trim($('#school_name').val());
            var line_1_address =$.trim($('#line_1_address').val());
            var line_2_address =$.trim($('#line_2_address').val());
            var town = $.trim($('#town').val());
            var post_code =$.trim($('#post_code').val());
            var country =$.trim($('#country').val());
            // var name = $.trim($('#name').val());
            // var email = $.trim($('#email').val());
            // var email_validation = isEmail(email);
            // var password = $.trim($('#password').val());
            // var confirmPassword = $.trim($('#confirmPassword').val());
            // var user_id = $('#user_id').val();
            // if(user_id == 0) {
            //
            //     if (school_name.length == 0 || line_1_address.length == 0 || line_2_address.length == 0 || town.length == 0 || post_code.length == 0 || country.length == 0 ) {
            //
            //
            //         if (school_name.length == 0) {
            //
            //             $('#school_nameValidation').removeClass('d-none');
            //             $('#school_name').focus();
            //
            //         }
            //
            //
            //         if (line_1_address.length == 0) {
            //
            //             $('#line_1_addressValidation').removeClass('d-none');
            //             $('#line_1_address').focus();
            //
            //         }
            //
            //
            //         if (line_2_address.length == 0) {
            //
            //             $('#line_2_addressValidation').removeClass('d-none');
            //             $('#line2').focus();
            //
            //         }
            //
            //
            //         if (town.length == 0) {
            //
            //             $('#townValidation').removeClass('d-none');
            //             $('#town').focus();
            //
            //         }
            //
            //
            //         if (post_code.length == 0) {
            //
            //             $('#post_codeValidation').removeClass('d-none');
            //             $('#post_code').focus();
            //
            //         }
            //
            //
            //         if (country.length == 0) {
            //
            //             $('#countryValidation').removeClass('d-none');
            //             $('#country').focus();
            //
            //         }
            //
            //         // if (name.length == 0) {
            //         //
            //         //     $('#nameValidation').removeClass('d-none');
            //         //     $('#name').focus();
            //         //
            //         // }
            //         //
            //         // if (email_validation == false) {
            //         //
            //         //     $('#emailValidation').removeClass('d-none');
            //         //     $('#email').focus();
            //         //
            //         // }
            //         //
            //         // if (password.length <= 5) {
            //         //
            //         //     $('#passwordValidation').removeClass('d-none');
            //         //     $('#password').focus();
            //         //
            //         // }
            //         //
            //         // if (password != confirmPassword) {
            //         //
            //         //     $('#confirmPassValidation').removeClass('d-none');
            //         //     $('#confirmPass').focus();
            //         //
            //         // }
            //
            //
            //         return false;
            //     }
            //
            // }else{

            if (school_name.length == 0 || line_1_address.length == 0 || line_2_address.length == 0 || town.length == 0 || post_code.length == 0 || country.length == 0) {


                if (school_name.length == 0) {

                    $('#schoolNameValidation').removeClass('d-none');

                }


                if (line_1_address.length == 0) {

                    $('#line1AddressValidation').removeClass('d-none');

                }


                if (line_2_address.length == 0) {

                    $('#line2AddressValidation').removeClass('d-none');

                }

                if (town.length == 0) {

                    $('#townValidation').removeClass('d-none');

                }


                if (post_code.length == 0) {

                    $('#postCodeValidation').removeClass('d-none');

                }


                if (country.length == 0) {

                    $('#countryValidation').removeClass('d-none');

                }

                return false;
            }


            var data = $('#request_form').serialize();


            // var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            // alert(CSRF_TOKEN);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({

                url: '/frontend/schoolRequest',

                type: 'POST',

                dataType: 'JSON',

                cache: false,

                data: data,

                success: function (data) {


                    if(data.recapcha == true){

                        var other_req = '';

                        if (data.current_user_request.requests > 1) {

                            $('#school_request').addClass('d-none');

                            other_req += '<p>Thank you for your submitting Request for <b>' + data.current_user_request.school_name + '</b> Locker.You have already been ' + (data.current_user_request.requests - 1) + ' Request for <b> ' + data.current_user_request.school_name +'</b>';

                            if (data.others_saved_requests > 0) {



                                other_req += ' And other people have also submitted ' + data.others_saved_requests + ' Request for <b>' + data.current_user_request.school_name + '</b>';

                            }

                            other_req += '</p>';

                            $('#school_req_response').html(other_req);

                        } else {

                            $('#school_request').addClass('d-none');

                            other_req += '<p>Thank you for your submitting Request at <b>' + data.current_user_request.school_name + '</b>';

                            if (data.others_saved_requests > 0) {



                                other_req += ' And other people have also submitted ' + data.others_saved_requests + ' Request for <b>' + data.current_user_request.school_name + '</b>';

                            }

                            other_req += '</p>';

                            $('#school_req_response').html(other_req);

                        }

                        $('#location_search').val('');

                        // var str = '';

                        // str+='<div class="card" style="width: 18rem;">';

                        // str+='<div class="card-body">';

                        // str+='<h6 class="card-title text-left"><b>Location Group : </b>'+ data.location_group.name +'</h6>';

                        // str+='<h6 class="card-title text-left"><b>Company : </b>'+data.name +'</h6>';

                        // str+='<p class="card-text text-muted text-left" ><b>Address : </b>'+ data.address +'</p>';

                        // str+='</div>';

                        // str+='</div>';

                        // $('#next_stage_proceede').html(str);
                    }else{
                        $('#location_search').val('');
                        $('#school_request').addClass('d-none');
                        $('#capcha_error').html('<span style="color:red">Your are not human! refill request form!</span>');
                    }
                },

                error: function (jqXHR, textStatus, errorThrown) {



                }

            });

        });

        // function isEmail(email) {
        //
        //     var regex = /^([a-zA-Z0-9_.+-]{3,20})+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        //
        //     return regex.test(email);
        //
        // }

    </script>
@endsection