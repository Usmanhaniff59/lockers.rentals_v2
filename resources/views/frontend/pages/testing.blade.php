public function getlockers(Request $request){

if(Session::has('saleKey')) {
$start = Carbon::parse($request['start'])->format('Y-m-d H:i:s');
$end = Carbon::parse($request['end'])->format('Y-m-d H:i:s');

$locker_numbers = Locker::where('lockers.block_id', $request->block_id)
->where('active',1)
->where('display',1)
->orderBy('id', 'ASC')
->get();
$total_lockers = $locker_numbers->count();
$locker_numbers->toArray();

$output = '';

$blocks = Block::leftJoin('locker_block_image', 'locker_block_image.id', '=', 'blocks.locker_block_image_id')
->select('blocks.*', 'locker_block_image.file_name as locker_block_image')
->where('blocks.id', $request->block_id)->first();

$output .= '<div class="col-md-12" ><div class="row" style="overflow-x:auto;"><div class="col-md-12 lockers_table" ><table id="locker_table' . $request['counter'] . '">';

                $counter = 0;
                $counter_red = 0;
                for ($i = 1; $i <= $blocks->locker_blocks; $i++) {

                $red_column = 0;
                $output .= '<tr>';
                    for ($j = 1; $j <= $blocks->block_height; $j++) {
                    if ($counter_red <= $total_lockers - 1) {
                    if (Sale::where('locker_id',$locker_numbers[$counter_red]['id'])
                    ->where(function ($query) use ($start,$end) {
                    $query->where('end','>=',$start)
                    ->Where('start','<=',$end);
                    })
                    ->where(function ($query) {
                    $query->where('status', '=', 'b')
                    ->orWhere('status', '=', 'r');
                    })->count('id') > 0 ) {
                    $red_column++;

                    }else if( $locker_numbers[$counter]->maintenances->where( 'to' ,'>=', $start)
                    ->where('from','<=' ,$end)->where('active',1)->count('id') > 0){
                    $red_column++;
                    }

                    } else {
                    $red_column++;
                    }
                    $counter_red++;

                    }

                    for ($j = 1; $j <= $blocks->block_height; $j++) {

                    if ($counter <= $total_lockers - 1) {

                    if (Sale::where('locker_id',$locker_numbers[$counter]['id'])
                    ->where(function ($query) use ($start,$end) {
                    $query->where('end','>=',$start)
                    ->Where('start','<=',$end);
                    })
                    ->where(function ($query) {
                    $query->where('status', '=', 'b')
                    ->orWhere('status', '=', 'r');
                    })->count('id') > 0
                    ||

                    $locker_numbers[$counter]->maintenances->where( 'to' ,'>=', $start)
                    ->where('from','<=' ,$end)->where('active',1)->count('id') > 0) {


                    $output .= '<td ><input type = "radio" name = "locker_id' . $request['counter'] . '" class="invisible-radio' . $request['counter'] . '" checked>';
                        if ($red_column == $blocks->block_height) {
                        $output .= '<label for="radio' . $locker_numbers[$counter]['id'] . '" class="red-lockers" > ';
                            } else {
                            $output .= '<label for="radio' . $locker_numbers[$counter]['id'] . '" class="" > ';

                                }
                                if ($locker_numbers[$counter]['id'] != intval($request->selected_lockers)) {

                                $output .= '<div class="styled-radio-red' . $request['counter'] . ' text-center" style = "color: white">' . $locker_numbers[$counter]->locker_number . '<br ><i class="fa fa-times icon-cross" ></i ></div ></label ></td>';
                    } else {

                    $output .= '<div class="styled-radio' . $request['counter'] . ' text-center" style = "color: white; border: 5px solid #66ff66">' . $locker_numbers[$counter]->locker_number . '<br ><i class="fa fa-check rounded-circle" style="background-color:#66ff66; padding: 6px""></i ></div ></label ></td>';
                    // $output .= '<div class="styled-radio'.$request['counter'].' text-center" style = "color: white;">'.$locker_numbers[$counter]->locker_number.'<br ><i class="fa fa-check icon-tick" ></i ></div ></label ></td >';
                    }

                    } else {
                    $output .= '<td><input type = "radio" name = "locker_id' . $request['counter'] . '" id = "radio' . $locker_numbers[$counter]['id'] . '' . $request['counter'] . '" value="' . $locker_numbers[$counter]['id'] . '" data-sale_id = "' . $request->sale_id . '" data-block_id = "' . $locker_numbers[$counter]->block_id . '" data-locker_id = "' . $locker_numbers[$counter]['id'] . '" data-rate = "' . $request['rate'] . '" data-tax = "' . $request['tax'] . '" data-counter = "' . $request['counter'] . '" data-hold_locker="' . $request['hold_locker'] . '" class="invisible-radio' . $request['counter'] . ' choose-locker" >
                        <label for="radio' . $locker_numbers[$counter]['id'] . '' . $request['counter'] . '" class="puffOut">
                            <div class="styled-radio' . $request['counter'] . ' text-center" style = "color: white;">' . $locker_numbers[$counter]->locker_number . '<br ><i class="fa fa-check icon-tick" ></i ></div ></label ></td >';
                    }


                    } else {
                    //                                                    $output.='<td> <input type = "radio" name = "id'.$request['counter'].'" class="invisible-radio'.$request['counter'].'" >
                        //                                                        <label for="radio" >
                            //                                                            <div class="styled-radio-gray'.$request['counter'].' text-center" style = "color: white" ></div ></label >
                        //                                                    </td>';
                    }
                    $counter++;
                    }
                    $output .= '</tr>';
                }

                $output .= '</table></div><div style="display: none;" class="col-md-12 locker_image">
            <img style="width:100%;" src="' . url('uploads/' . $blocks->locker_block_image) . '" class=" img-responsive" alt="Block Image">
        </div></div></div>';
}else{
return redirect('/');
}

return response()->json($output);
}