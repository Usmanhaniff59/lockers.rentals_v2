@extends('frontend.layouts.app')
@section('style')
    <script src="https://code.jquery.com/jquery-3.4.1.js" crossorigin="anonymous"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>
    <style>
        .flash-messages{
            display: none ;
        }
    </style>
@endsection
@section('content')

    {{--@if(session()->has('locale'))--}}
    {{--{{ dd(session()->get('locale'))}}--}}
    {{--@endif--}}
    <main id="main">
        <!--========================== About Us Section   ============================-->
        <section id="about">
            <input type="hidden" id="ticketurl" value="https://tickets.locker.rentals/">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-success show_result_message alert-dismissible fade show" style="display: none;">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong id="result_message">Success! <span id="ticket_success" class="assign_class" data-id="ticket_successfully_created" data-value ="{{t('ticket_successfully_created')}}">{!! t('ticket_successfully_created') !!}</span></strong>
                        </div>
                    </div>
                </div>
                <header class="section-header">
                    <h3 class="assign_class" data-id="support" data-value ="{{t('support')}}">{!! t('support') !!}</h3>
                    <div class="text-center p-b-20"><span class="assign_class" data-id="support-review" data-value="{{t('support-review')}}">{!! t('support-review')!!}</span> </div>
                </header>
                <div class="row about-cols">
                    <div class="col-md-2"></div>
                    <div class="col-md-8 wow fadeInUp" data-wow-delay="0.1s">
                        <div class="about-col">
                            <div class="img">
                                <img  alt="" class="img-fluid">
                                <div class="icon"><i class="fad fa-question-circle"></i></div>
                            </div>
                            <br><br><br>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-10">
                                    <form action="javascript:;" id="generateTicket" method="POST">
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="ticket_type_id">
                                                        <b class="assign_class" data-id="ticket-type" data-value ="{{t('ticket-type')}}">{!! t('ticket-type') !!}</b>
                                                    </label>
                                                    <select name="ticket_type_id" class="form-control" id="ticket_type_id"></select>
                                                    <small class="text-danger" id="ticket_type_id_message"></small>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <!-- school list start -->


                                     <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="location" ><b class="assign_class" data-id="school-name" data-value ="{{t('school-name')}}">{!! t('school-name') !!}</b></label>
                                            <input type="text" name="location" class="form-control assign_class"  data-id = "location-input-place-holder" data-value = "{{t('location-input-place-holder')}}" id="location_search"  placeholder="@t('location-input-place-holder')"  data-inputplaceholder="yes" autocomplete="off"/>
                                        </div>
                                    </div>
                                    <input type="hidden" id="locationId" name="locationId" value="">
                                </div>
                                 <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group">
                                                <div id="display_locations"></div>
                                            </div>
                                        </div>
                                        <!-- <div class="col-md-6 col-sm-6">
                                            <div class="form-group" id="next_stage_proceede"></div>
                                        </div> -->
                                    </div>
                                </div>

                                            <!-- school list end -->
                                       
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="phone">
                                                        <b class="assign_class" data-id="phone" data-value ="{{t('phone')}}">{!! t('phone') !!}</b>
                                                    </label>
                                                    <input type="text" class="form-control" id="phone" name="phone" >
                                                    <small class="text-danger" id="phone_message"></small>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="phone">
                                                        <b class="assign_class" data-id="name" data-value ="{{t('name')}}">{!! t('name') !!}</b>
                                                    </label>
                                                    <input type="text" class="form-control" @if(Auth::check()) value="{{auth::user()->name}}" @endif id="name" name="name" >
                                                    <small class="text-danger" id="name_message"></small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="email">
                                                        <b class="assign_class" data-id="email" data-value ="{{t('email')}}">{!! t('email') !!} *</b>
                                                    </label>
                                                    {{--                                                    <input type="email" value="{{auth::user()->email}}" disabled class="form-control">--}}
                                                    <input type="text" @if(Auth::check()) value="{{auth::user()->email}}" @endif class="form-control"  id="email" name="email" >
                                                    <input type="hidden" @if(Auth::check())  value="{{auth::user()->password}}" @endif  id="password" name="password" >
                                                    <input type="hidden" @if(Auth::check())  value="{{auth::user()->response_group_id}}" @endif id="response_group_id" name="response_group_id" >
                                                    {{--                                                    <input type="hidden" @if(Auth::check())  value="{{auth::user()->name}}" @endif id="name" name="name" >--}}
                                                    <small class="text-danger" id="email_message"></small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="subject">
                                                        <b class="assign_class" data-id="subject" data-value ="{{t('subject')}}">{!! t('subject') !!} *</b>
                                                    </label>
                                                    <input type="text" class="form-control" id="overview" name="overview" >
                                                    <small class="text-danger" id="overview_message"></small>
                                                </div>
                                            </div>
                                        </div>
                                          <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="bookingId"> <b class="assign_class" data-id="refrence_Number" data-value ="{{t('refrence_Number')}}">{!! t('refrence_Number') !!} *</b></label>
                                                    <input type="text" class="form-control" id="bookingId" name="bookingId">
                                                    <small class="text-danger" id="bookingId_message"></small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="message">
                                                        <b class="assign_class" data-id="message" data-value ="{{t('message')}}">{!! t('message') !!} *</b>
                                                    </label>
                                                    <textarea name="message" class="form-control" id="message" cols="30" rows="10"></textarea>
                                                    <small class="text-danger" id="message_message"></small>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="details">
                                                        <b class="assign_class" data-id="additional_info" data-value ="{{t('additional_info')}}">{!! t('additional_info') !!} </b>
                                                    </label>
                                                    <textarea name="details" class="form-control" id="details" cols="30" rows="10" placeholder="Please provide your booking reference and school name"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row pb-5">
                                            <div class="col-md-12">
                                                <button class="green-button button_text submit_ticket_form assign_class confirm-purchase" type="submit">Submit</button>
                                                <span class="spinner-border" style="position: absolute;top: 15%;left: 16%"></span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </section>
        <!-- #about -->
    </main>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#sel1").select2();
            $(".spinner-border").hide();
            getTicketTypes();
        });
        function getTicketTypes(){
            var ticketurl=$("#ticketurl").val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "GET",
                dataType: 'json',
                data: 1,
                processData: false,
                contentType: 'application/json',
                CrossDomain:true,
                async: false,
                url: ticketurl+'api/general/getTicketTypes',
                success: function(response)
                {
                    var text="";
                    text+="<option value=''>Please Select</option>";
                    var data=response.data;
                    for (var i = 0; i <data.length; i++){
                        var id=JSON.stringify(response.data[i]['id']);
                        var name=JSON.stringify(response.data[i]['name']).replace('"'," ").replace('"'," ");
                        text+="<option value='"+id+"'>"+name+"</option>";
                    }
                    $("#ticket_type_id").html(text);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

        function getUserInfoByEmail(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                data: {email:$("#email").val(),name:$("#name").val()},
                url:'exist/users',
                success: function(response)
                {
                    $("#password").val(response.password);
                    $("#response_group_id").val(response.response_group_id);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

        $("body").on('submit','#generateTicket',function(event){
            if ($("#overview").val() =="" || $("#ticket_type_id").val() =="" || $("#email").val() =="" || $("#phone").val() =="" || $("#name").val() =="" || $("#message").val() ==""){
                if ($("#overview").val() ==""){
                    $("#overview_message").html('Subject required');
                }
                if ($("#message").val() ==""){
                    $("#message_message").html('Message required');
                }
                if ($("#ticket_type_id").val() ==""){
                    $("#ticket_type_id_message").html('Ticket Type required');
                }
                if ($("#phone").val() ==""){
                    $("#phone_message").html('Phone number required');
                }
                if ($("#name").val() ==""){
                    $("#name_message").html('Name required');
                }

                if ($("#email").val() ==""){
                    $("#email_message").html('Email required');
                }
                $(".spinner-border").hide();
                $(".submit_ticket_form").attr('disabled', false);
                return 0;
            }else{
                getUserInfoByEmail();
                $(".spinner-border").show();
                $(".submit_ticket_form").attr('disabled', true);
                setTimeout(function(){
                    var ticketurl=$("#ticketurl").val();
                
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        data:new FormData(document.getElementById('generateTicket')),
                        contentType:false,
                        cache:false,
                        processData:false,
                        url: ticketurl+'api/general/generateTicket',
                        success: function(response)
                        {
                            $(".show_result_message").show();
                            var ticket_success=$("#ticket_success").html();
                            if (ticket_success==""){
                                $("#ticket_success").html('Ticket Successfully Created');
                            }else{
                                $("#ticket_success").html(ticket_success);
                            }
                            $(".spinner-border").hide();
                            $(".submit_ticket_form").attr('disabled', false);
                            $("#generateTicket")[0].reset();
                            console.log(response);
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                },5000);
            }
        });


        $('body').on('keyup','#location_search',function(e){

            var location = $('#location_search').val();

            $('#display_locations').html('');

            // $('#locker-non-availability').html('');

            $('#next_stage_proceede').html('');

            // $('#school_req_response').html('');

            // $('#capcha_error').html('');

            // $('#request_form')[0].reset();

            // $('#school_request').addClass('d-none');

            // $('#login_request').addClass('d-none');

            // $('#location_proceed_button').attr("disabled", true);

            // $('#location_proceed_button').css({"opacity":"0.5","cursor":"no-drop"});
            // $('#location_proceed_button').attr('title','Please select location');
            // $('#location_proceed_button').attr('data-toggle','tooltip');

            // $("#location_proceed_button").hover(function() {

            //     $(this).css("background", "");

            // }, function() {

            //     $(this).css("background", "");

            // });

            if(location.length >= 3) {

                $('#next_stage_proceede').html('');

                // var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });


                $.ajax({

                    url: '/frontend/searchlocation',

                    type: 'POST',

                    dataType: 'JSON',

                    cache: false,

                    data: {location: location },

                    success: function (data) {

                        if(data.locations.length != 0) {

                            $('#display_locations').html(data.radio_locations);

                        }else{

                            $('#display_locations').html('');

                            var searchedLocation = $('#location_search').val();
                            // $('#school_name').val(searchedLocation);

                            var user_id = 0;

                            //if user not login
                            if(user_id == 0 ){

                                // alert('LOGIN');
                                $('#submit_request').addClass('d-none');

                                var str = $('.locker-non-availability').data('text');

                                var data_id = $('.locker-non-availability').data('id');

                                // var token = $("meta[name='csrf-token']").attr("content");
                                $.ajaxSetup({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    }
                                });

                                $.ajax({

                                    url:"/frontend/placeholders",

                                    method:"POST",

                                    data:{data_id:data_id},

                                    success:function(data){



                                        var placeholder = data.placeholder;

                                        var text = str.replace(placeholder, location);

                                        $('.locker-non-availability').text(text);

                                        var placeholderValues =location;

                                        $('.locker-non-availability').data('placeholderval', placeholderValues);

                                        $('.locker-non-availability').data('placeholders', placeholder);

                                        // $('#modal_placeholders').val(placeholder);

                                        $('#school_request').removeClass('d-none')

                                    }

                                });

                                $('#school_request').removeClass('d-none');

                                $('#login_request').removeClass('d-none')

                            }
                        }

                    },

                    error: function (jqXHR, textStatus, errorThrown) {



                    }

                });

            }

        });
 $('body').on('click','.radio-locations',function(e){

            var location_id = $(this).data('location_id');
            var val= $(this).data('location_name');
            // alert(val);
            $('#location_search').val(val);
            $('#locationId').val(location_id);
            $('#display_locations').html('');
            // var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            // $.ajaxSetup({
            //     headers: {
            //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //     }
            // });


            // $.ajax({

            //     url: '/frontend/getlocationgroup',

            //     type: 'POST',

            //     dataType: 'JSON',

            //     cache: false,

            //     data: {location_id: location_id},

            //     success: function (data) {

            //         var str = '';

            //         str+='<div class="card" >';

            //         str+='<div class="card-body">';

            //         str+='<h6 class="card-title text-left">'+ data.location_group.name +'</h6>';

            //         str+='<h6 class="card-title text-left">'+data.name +'</h6>';

            //         str+='<p class="text-left text-muted"  ><b>Address  </b><br>'+ data.street +' '+ data.city +','+ data.country +'</p>';

            //         str+='</div>';

            //         str+='</div>';

            //         $('#next_stage_proceede').html(str);

            //         if($('#edit_cms_switch').is(":checked")){



            //             $('.get_text').prop("disabled", false);

            //             $('#location_proceed_button').addClass("get_text");

            //         }else{

            //             $('.get_text').prop("disabled", true);

            //             $('#location_proceed_button').removeClass("get_text");

            //         }

            //         $('#location_proceed_button').attr("disabled", false);

            //         $("#location_proceed_button").css({"opacity": "1","cursor":"pointer"});
            //         $('#location_proceed_button').attr('title','');
            //         $('#location_proceed_button').attr('data-toggle','');

            //         $("#location_proceed_button").hover(function() {

            //             $(this).css("background", "#F5B54A");

            //         }, function() {

            //             $(this).css("background", "#F5B54A");

            //         });



            //     },

            //     error: function (jqXHR, textStatus, errorThrown) {


            //     }

            // });

        });

    </script>

@endsection