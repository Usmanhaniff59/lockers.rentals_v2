@extends('frontend.layouts.app')
@section('style')
    <script src="https://code.jquery.com/jquery-3.4.1.js" crossorigin="anonymous"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>
    <style>
        .section-header h3::after {

    content: '';

    position: absolute;

    display: block;

    width: 5%;

    height: 3px;

    background: red;

    bottom: 0;

    left: 35%;

}

    </style>
@endsection
    @section('content')

        {{--@if(session()->has('locale'))--}}
            {{--{{ dd(session()->get('locale'))}}--}}
        {{--@endif--}}
        <main id="main">
            <!--==========================
                 About Us Section
               ============================-->

            <section id="about">
                <div class="container">
                    <header class="section-header">
                        <h3 class="assign_class"   data-id = "booking-expired-heading" data-value = "{{t('booking-expired-heading')}}">{!! t('booking-expired-heading')!!}</h3>
                        <p class="assign_class" data-id = "booking-expired-text" data-value = "{{t('booking-expired-text')}}">{!! t('booking-expired-text')!!}</p>
                    </header>
                    <div class="row about-cols">
                        <div class="col-md-12 wow fadeInUp" data-wow-delay="0.1s">
                            <div class="about-col">
                                <div class="img">
                                    <img  alt="" class="img-fluid">
                                    <div class="icon" style="background-color: white"><i class="fad fa-exclamation-triangle" style="color:red"></i></div>
                                </div>
                                <h2 class="assign_class" />

                                <p class="assign_class" style="font-weight: 300" data-id = "booking-expired-section1-text" data-placeholders="{{$sale_desc_placeholder}}" data-placeholderval="{{$sale->hold_booking_date}},{{url('/login')}}" data-value = "{{t('booking-expired-section1-text')}}"  >{!!  str_replace(explode(',', $sale_desc_placeholder),[url('/login'), date("F jS, Y", strtotime($sale->hold_booking_date))],t('booking-expired-section1-text'))  !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- #about -->
        </main>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#sel1").select2();
        });
        $('#login_msg').click(function(){
            var base_url = window.location.origin;
            $(location).attr('href', base_url)
           
        });

    </script>

@endsection