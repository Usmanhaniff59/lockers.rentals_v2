@extends('frontend.layouts.app')
@section('style')
    <script src="https://code.jquery.com/jquery-3.4.1.js" crossorigin="anonymous"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>
    <style>
        .flash-messages{
            display: none ;
        }
    </style>
@endsection
@section('content')

    {{--@if(session()->has('locale'))--}}
    {{--{{ dd(session()->get('locale'))}}--}}
    {{--@endif--}}
    <main id="main">
        <!--========================== About Us Section   ============================-->
        <section id="about">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        @if ($errors->has('messageSuccess'))
                            <div class="alert alert-success alert-dismissible fade show">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong>Success!</strong> {{ $errors->first('messageSuccess') }}
                            </div>
                        @elseif($errors->has('messageError'))
                            <div class="alert alert-warning alert-dismissible fade show">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong>Warning!</strong> {{ $errors->first('messageError') }}
                            </div>
                        @endif
                    </div>
                </div>
                <header class="section-header">
                    <h3 class="assign_class" data-id ="contact-us-heading" data-value ="{{t('contact-us-heading')}}">{!! t('contact-us-heading') !!}</h3>
                    <div class="text-center p-b-20"><span class="assign_class" data-id="contact-policy-review" data-value="{{t('contact-policy-review')}}">{!! t('contact-policy-review')!!}</span> </div>
                </header>
                <div class="row about-cols">
                    <div class="col-md-2"></div>
                    <div class="col-md-8 wow fadeInUp" data-wow-delay="0.1s">
                        <div class="about-col">
                            <div class="img">
                                <img  alt="" class="img-fluid">
                                <div class="icon"><i class="fad fa-question-circle"></i></div>
                            </div>
                            <br><br><br>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-10">
                                    <form action="{{url('/send/contact/mail')}}" id="contactform" method="POST">
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="subject" class="text-blue"><b>Subject *</b></label>
                                                    <input type="text" class="form-control" id="subject" name="subject">
                                                    <small class="text-danger" id="subject_message"></small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="school" class="text-blue"><b>School Name</b></label>
                                                    <input type="text" class="form-control" id="school_name" name="school">
                                                    <small class="text-danger" id="school_message"></small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="bookingId" class="text-blue"><b>Booking ID/Reference Number</b></label>
                                                    <input type="text" class="form-control" id="bookingId" name="bookingId">
                                                    <small class="text-danger" id="bookingId_message"></small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="phone" class="text-blue"><b>Phone Number</b></label>
                                                    <input type="text" class="form-control" id="phone" name="phone" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="email" class="text-blue"><b>E-mail Address*</b></label>
                                                    <input type="email" class="form-control" id="email" name="email" >
                                                    <small class="text-danger" id="email_message"></small>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="message" class="text-blue"><b>Message</b></label>
                                                    <textarea name="message" class="form-control" id="message" cols="30" rows="10"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row pb-5">
                                            <div class="col-md-12">
                                                <button class="green-button button_text assign_class confirm-purchase">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </section>
        <!-- #about -->
    </main>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#sel1").select2();
        });
        $("body").on('submit','#contactform',function (event) {

            if ($("#subject").val() ==""){
                $("#subject_message").html('Subject required');
                return false;
                event.preventDefault();
            }
            if ($("#email").val() ==""){
                $("#email_message").html('Email required');
                return false;
                event.preventDefault();
            }
            return true;
        });
    </script>

@endsection