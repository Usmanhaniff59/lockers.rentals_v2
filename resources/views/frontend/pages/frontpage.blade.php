@extends('frontend.layouts.app')

@section('style')

<link rel="stylesheet" href="/frontend/summernote/summernote-bs4.css">
@endsection

@section('content')
<!--==========================
  Intro Section
============================-->

<main id="main">


    <!--==========================
      Skills Section
    ============================-->
    <section id="skills">
        <div class="container">

            <header class="section-header">
                <h3 >{{$page->title}}</h3>
                <div id="body-p">{!! $page->body !!} <button id="edit-but">Edit</button></div>

            </header>
            <div class="summernote" id="body-edit">{!! $page->body !!}</div><br>
            <button id="save-but" data-id = "{{$page->id}}" style="display: none;">Save</button>
        </div>
    </section>


</main>

@endsection

@section('script')

    <script type="text/javascript" src="/frontend/summernote/summernote-bs4.js"></script>
    <script type="text/javascript">
        $('#edit-but').click(function (){
            $('#body-edit').show();
            $('#save-but').show();
            $('#body-p').hide();
            $('.summernote').summernote({
                height: 300,
                tabsize: 2,
                followingToolbar: true,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['fontname', ['fontname']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['view', ['fullscreen', 'help']],
                ],
            });
        });
        $('#save-but').click(function (){
            var pageid = $(this).data("id");
            var pagecontent = $('.summernote').summernote('code');

            var token = $("meta[name='csrf-token']").attr("content");
            $.ajax({
                url:"{{ route('savepage') }}",
                method:"POST",
                data:{pageid:pageid, pagecontent:pagecontent, _token:token},
                success:function(data){
                    location.reload();
                }
            });
        });
        $(document).ready(function() {
            $('#body-edit').hide();

        });
    </script>
@endsection