@extends('frontend.layouts.app')

@section('style')


    <link rel="stylesheet" href="/frontend/vendors/animate/css/animate.min.css"/>
    <link rel="stylesheet" href="/frontend/vendors/hover/css/hover-min.css"/>
    <link rel="stylesheet" href="/frontend/vendors/wow/css/animate.css"/>
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.0.0/flatly/bootstrap.min.css">--}}
    <link rel="stylesheet" href="/css/bootstrap-datetimepicker.css">
    <link type="text/css" rel="stylesheet" href="/frontend/vendors/checkbox_css/css/checkbox.min.css" />
    <link type="text/css" rel="stylesheet" href="/frontend/css/pages/radio_checkbox.css" />
    <link type="text/css" rel="stylesheet" href="/frontend/css/pages/wizards.css"/>


    <link type="text/css" rel="stylesheet" href="/frontend/vendors/chosen/css/chosen.css"/>
    <link type="text/css" rel="stylesheet" href="/frontend/css/pages/form_elements.css"/>

    <!-- end of plugin styles-->
    <link rel="stylesheet" href="/frontend/css/pages/animations.css"/>
    <style>

        /*.school_header h3::after {*/
        /*content: '';*/
        /*position: absolute;*/
        /*display: block;*/
        /*width: 40px;*/
        /*height: 3px;*/
        /*background: #F5B54A;*/
        /*bottom: 0;*/
        /*left: calc(50% - 60px);*/
        /*}*/
        /*body {*/
        /*font-family: Ubuntu, Helvetica, Arial, sans-serif;*/
        /*}*/

        .flex-container {
            display: flex;
            justify-content: center;
            flex-wrap: wrap;
        }

        /*#rootwizard ul li, #rootwizard_no_val ul li {*/
        /*    display: block;*/
        /*    width: 100%;*/
        /*}*/
        .chosen-drop>ul.chosen-results>li {
            display: block !important;
            width: 100% !important;
        }

        .flex-container label {
            margin: 15px;
        }


        @foreach($childrens as $index=>$children)

        .styled-radio{{++$index}} {
            margin: 5px;
            padding-top: 5px;
            width: 75px;
            height: 100px;
            background: #94AACF;
            border: 5px solid #559DE4;
            border-radius: 10px;
            font-weight: bold;
            font-size: x-large;
            line-height: 40px;
            /*opacity:0.7;*/
            /*filter:alpha(opacity=70);*/

        }

        .styled-radio-red{{$index}} {
            margin: 5px;
            padding-top: 5px;
            width: 75px;
            height: 100px;
            background: #B5040C;
            border: 5px solid #ffb2b2;
            border-radius: 10px;
            font-weight: bold;
            font-size: x-large;
            line-height: 40px;
            /*opacity:0.7;*/
            /*filter:alpha(opacity=70);*/
            background-color: #B5040C;

        }
        .styled-radio-gray{{$index}} {
            margin: 5px;
            padding-top: 5px;
            width: 75px;
            height: 100px;
            background: gray;
            border: 5px solid #9d9d9d ;
            border-radius: 10px;
            font-weight: bold;
            font-size: x-large;
            line-height: 40px;
            /*opacity:0.7;*/
            /*filter:alpha(opacity=70);*/
            /*background-color: #00c0ef;*/

        }



        .gray{{$index}} { background: gray; }

        .invisible-radio{{$index}} {
            /* per https://a11yproject.com/posts/how-to-hide-content/ */
            position: absolute;
            height: 1px;
            width: 1px;
            overflow: hidden;
            clip: rect(1px 1px 1px 1px); /* IE6, IE7 */
            clip: rect(1px, 1px, 1px, 1px);
        }

        .invisible-radio{{$index}}:checked + label .styled-radio{{$index}}::after {

            font-size: 3em;
            display: inline-block;
            box-sizing: border-box;
            padding: 20px;
            width: 150px;
            text-align: center;
        }

        .invisible-radio{{$index}}:not(:checked) + label {
            cursor: pointer;
        }

        .invisible-radio{{$index}}:not(:checked) + label:hover .styled-radio{{$index}} {
            opacity: 0.7;
            background: #00A1FF;
            border: 5px solid #559DE4;
        }
        .invisible-radio{{$index}}:not(:checked) + label:hover .styled-radio{{$index}} .icon-tick {
            padding: 4px;
            border-radius: 100%;
            background-color: #559DE4;

        }

        .invisible-radio{{$index}}:checked + label .styled-radio{{$index}} {
            transform: scale(1, 1);
            background: #9CBBBD!important;
            border: 5px solid #66ff66;
            animation: pulsate 1s alternate ease-in-out infinite;
        }
        .invisible-radio{{$index}}:focus + label .styled-radio{{$index}} .icon-tick {
            padding: 4px;
            border-radius: 100%;
            background-color: #66ff66;
            opacity: 1;
            visibility: visible;
            /*background: white;*/
        }
        @endforeach

        @keyframes pulsate {
            from {
                transform: scale(1, 1);
                box-shadow: none;
            }
            to {
                transform: scale(1.05, 1.05);
                box-shadow: 1px 4px 8px 1px rgba(0, 255, 0, 0.6);
            }
        }

        .after-title-step2::after {
            background: #9ABDB7;
        }
        .after-title-step3::after {
            background: #9ABDB7;
        }
        .icon-tick{
            padding: 4px;
            border-radius: 100%;
            background-color: #559DE4;
            opacity: 0.6;
            visibility: hidden;
        }
        .icon-cross{
            padding: 4px 7px;
            border-radius: 100%;
            background-color: red;
            opacity: 0.3;
        }

        .cell{
            flex: 0 0 25%;
            max-width: 25%
        }

        i.red-circle {
            display: inline-block;
            border-radius: 100%;
            box-shadow: 0px 0px 2px #B5040C;
            padding: 0.5em 0.6em;
            background-color: #B5040C;
            color: white;


        }

        i.green-circle {
            display: inline-block;
            border-radius: 100%;
            box-shadow: 0px 0px 2px #9CBBBD;
            padding: 0.5em 0.6em;
            background-color: #9CBBBD;
            color: #9CBBBD;
        }

        i.blue-circle {
            display: inline-block;
            border-radius: 100%;
            box-shadow: 0px 0px 2px #66ff66;
            padding: 0.5em 0.6em;
            background-color: #66ff66;
            color: white;
        }

        .right_school_booking_block{
            margin-bottom: 10px !important;
        }

        #rootwizard ul li, #rootwizard_no_val ul li {
            display: contents ;
            margin-top: 10px ;
        }
        .block-nave-link{
            padding: .5rem .5rem !important;
            margin-top: 10px !important;
        }
        #left_arrow::after{
            z-index: 1 !important;
        }
        .child-name{
            color: white;
        }

        .nav-tabs .nav-link, .nav-tabs .nav-item .nav-link {
            color: white;
            background-color: #B5040C;
            border-color: #dee2e6 #dee2e6 #e9e9e9;
        }
        .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
         border: 1px solid transparent;
        color: white;
        border-color:  #000000;
}
      .loader {
            position: absolute;
            top: 120%;
            left: 45%;
            margin: auto;
            display: none;
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #00c0ef;
            width: 100px;
            height: 100px;
            -webkit-animation: spin 2s linear infinite; /* Safari */
            animation: spin 2s linear infinite;
            z-index: 100000000;
        }
        /* Safari */
        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
</style>
@endsection

@section('content')
<main>
    <section id="portfolio"  class="section-bg" >

        @if(Session::has('flash_message'))
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable float-right" style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-hidden="true" style="margin-top: 4px; margin-left: 15px;"> ×
                    </button>
                    {{ Session::get('flash_message') }}
                </div>
            </div>
        @endif

<div class="loader" ></div>
        <form id="SchoolBookingForm" action="{{route('payments')}}" method="post">
            <div class="container">
                <input type='hidden' class="form-control" id="hide_show_locker_status" name="hide_show_locker_status" value="{{Session::get('show_hide_lockers')}}" />
                <input type='hidden' class="form-control" id="timeDifference" name="timeDifference" value="{{$remainingTime}}" />
                <header class="section-header">
                    {{--<h3 class="section-title">Type of Booking for '{{$school->name}}'</h3>--}}
                    <h3 class="section-title  school_header">
                        <span class="assign_class text-blue-color" data-id = "choose-locker-title" data-value = "{{t('choose-locker-title')}}"  data-steps="yes">{!! t('choose-locker-title')!!}</span>
                        <div class="after-left-arrow arrows d-none" id="left_arrow"></div><div class="after-title-step2"></div><div class="after-title-step3"></div><div class="after-title-step4"></div><div class="after-title-step5"></div><div class="after-right-arrow arrows d-none" id="right_arrow"></div></h3>
                    {{--<h3 class="section-title assign_class school_header" data-id = "choose-locker-title" data-value = "{{t('choose-locker-title')}}"  data-steps="yes" >{!! t('choose-locker-title')!!}<div class="after-title-step2"></div><div class="after-title-step3"></div><div class="after-title-step4"></div><div class="after-title-step5"></div></h3>--}}
                </header>
                <div class="card">
                    <div class="card-body m-t-20" style="padding-top: 1.25rem !important;padding-bottom: 1.25rem !important;margin-top: 0px !important;">
                        <div id="rootwizard_no_val">
                            <div class="row">
                                <div class="col-md-8" style="border-right: 2px solid #f5b54a;">
                                    <div class="row" style="margin-top:-45px">
                                        <div class="col-lg-12 col-12 fa-icon" style="margin-top: 50px">
                                            <i class="fas fa-times-circle red-circle" style="margin: 3px"></i><span class="assign_class" style="margin-left: 5px" data-id = "choose-locker-red-key-text" data-value = "{{t('choose-locker-red-key-text')}}" >{!! t('choose-locker-red-key-text')!!}</span>
                                            <i class="fas fa-circle green-circle" style="margin: 3px"></i><span class="assign_class" style="margin-left: 5px" data-id = "choose-locker-green-key-text" data-value = "{{t('choose-locker-green-key-text')}}" >{!! t('choose-locker-green-key-text')!!}</span>
                                            <i class="fas fa-circle blue-circle" style="margin: 3px"></i><span class="assign_class" style="margin-left: 5px" data-id = "choose-locker-blue-key-text" data-value = "{{t('choose-locker-blue-key-text')}}" >{!! t('choose-locker-blue-key-text')!!}</span>
                                        </div>
                                    </div>

                                    {{--<div class="row" style="margin-top: 11px;">--}}
                                        {{--<div class="col-lg-12 col-12 fa-icon" style="margin-top: 30px">--}}
                                            {{--<ul class="nav nav-pills">--}}
                                                {{--@foreach($child_blocks as $index=>$child_block)--}}
                                                    {{--@if($index==0)--}}
                                                        {{--<li id="nav_links_for_child" class="nav-item" style="margin-top: -50px; margin-left: -12px">--}}
                                                            {{--<a class="nav-link block-nave-link tab-show active show selected-child"  href="#tab{{$index}}" id="tab-pills{{$index}}" data-toggle="tab"><span--}}
                                                                        {{--class="userprofile_tab">{{$index+1}}</span>Child {{$index+1}} </a>--}}
                                                        {{--</li>--}}
                                                    {{--@else--}}
                                                        {{--<li id="nav_links_for_child" class="nav-item" style="margin-top: -50px; margin-left: -12px">--}}
                                                            {{--<a class="nav-link block-nave-link tab-show selected-child" style="color: blue" href="#tab{{$index}}" id="tab-pills{{$index}}" data-toggle="tab"><span--}}
                                                                        {{--class="userprofile_tab">{{$index+1}}</span>Child {{$index+1}}   </a>--}}
                                                        {{--</li>--}}
                                                    {{--@endif--}}
                                                {{--@endforeach--}}
                                            {{--</ul>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    <div class="row">


                                        <div class="col-lg-12 col-12">
                                            @include('frontend.pages.choose_locker_render')

                                            {{--<div class="tab-content m-t-20">--}}

                                                {{--@foreach($child_blocks as $index=>$child_block)--}}

                                                    {{--@if($index==0)--}}

                                                        {{--<div class="tab-pane tab-show active show" id="tab{{$index}}">--}}
                                                            {{--<br>--}}
                                                           {{----}}

                                                            {{--<div class="form-group">--}}
                                                                {{--@if($location_group == 1)--}}

                                                                    {{--<p class="assign_class" data-id = "choose-locker-child-name-date-title" data-placeholders="{{$child_name_date_placeholders}}" data-placeholderval="{{$childrens[$index]->child_first_name}} {{$childrens[$index]->child_surname}},{{Carbon\Carbon::parse($childrens[$index]->start)->format('d M, Y')}} - {{Carbon\Carbon::parse($childrens[$index]->end)->format('d M, Y')}}" data-value = "{{t('choose-locker-child-name-date-title')}}" >{!! str_replace(explode(',', $child_name_date_placeholders),[$childrens[$index]->child_first_name.' '.$childrens[$index]->child_surname , Carbon\Carbon::parse($childrens[$index]->start)->format('d M, Y').' - '.Carbon\Carbon::parse($childrens[$index]->end)->format('d M, Y')],t('choose-locker-child-name-date-title')) !!} </p>--}}

                                                                {{--@elseif($location_group == 2)--}}
                                                                    {{--                                                        <p class="assign_class" data-id = "choose-locker-child-name-date-title" data-placeholders="%child_name%,%date%" data-placeholderval="{{$childrens[$index]->child_first_name}} {{$childrens[$index]->child_surname}},{{Carbon\Carbon::parse($childrens[$index]->start)->format('d-M-Y')}} - {{Carbon\Carbon::parse($childrens[$index]->end)->format('d-M-Y')}}" data-value = "{{t('choose-locker-child-name-date-title')}}" data-steps="yes" >{!!  str_replace(["%child_name%" ,"%date%"],[$childrens[$index]->child_first_name ,Carbon\Carbon::parse($childrens[$index]->start)->format('d-M-Y') - Carbon\Carbon::parse($childrens[$index]->end)->format('d-M-Y')],t('choose-locker-child-name-date-title'))  !!}</p>--}}
                                                                    {{--<p class="assign_class" data-id = "choose-locker-child-name-date-title" data-placeholders="{{$child_name_date_placeholders}}" data-placeholderval="{{$childrens[$index]->child_first_name}} {{$childrens[$index]->child_surname}},{{Carbon\Carbon::parse($childrens[$index]->start)->format('d M, Y')}} - {{Carbon\Carbon::parse($childrens[$index]->end)->format('d M, Y')}}" data-value = "{{t('choose-locker-child-name-date-title')}}" >{!! str_replace(explode(',', $child_name_date_placeholders),[$childrens[$index]->child_first_name.' '.$childrens[$index]->child_surname ,Carbon\Carbon::parse($childrens[$index]->start)->format('d M, Y').' - '.Carbon\Carbon::parse($childrens[$index]->end)->format('d M, Y')],t('choose-locker-child-name-date-title')) !!}</p>--}}

                                                                {{--@else--}}
                                                                    {{--<p class="assign_class" data-id = "choose-locker-child-name-date-title" data-placeholders="{{$child_name_date_placeholders}}" data-placeholderval="{{$childrens[$index]->child_first_name}} {{$childrens[$index]->child_surname}},{{Carbon\Carbon::parse($childrens[$index]->start)->format('d M, Y H:i')}} - {{Carbon\Carbon::parse($childrens[$index]->end)->format('d M, Y H:i')}}" data-value = "{{t('choose-locker-child-name-date-title')}}" >{!! str_replace(explode(',', $child_name_date_placeholders),[$childrens[$index]->child_first_name.' '.$childrens[$index]->child_surname ,Carbon\Carbon::parse($childrens[$index]->start)->format('d M, Y H:i').' - '.Carbon\Carbon::parse($childrens[$index]->end)->format('d M, Y H:i')],t('choose-locker-child-name-date-title')) !!} </p>--}}

                                                                {{--@endif--}}
                                                            {{--</div>--}}

                                        {{--<div class="card" style="margin-bottom: 20px;">--}}
                                            {{--<div class="card-body m-t-20">--}}
                                                {{--<div class="form-group">--}}
                                                    {{--<label for="blocks" class="assign_class" data-id = "choose-locker-select-block-title" data-value = "{{t('choose-locker-select-block-title')}}" >{!! t('choose-locker-select-block-title')!!}</label>--}}
                                                    {{--{!! $child_block !!}--}}
                                                {{--</div>--}}
                                                {{--<br>--}}
                                                {{--<div class="lockers_printing" style="text-align:left;" id="display_lockers{{$index+1}}"></div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                {{--@else--}}

                                    {{--<div class="tab-pane tab-show" id="tab{{$index}}">--}}
                                        {{--<br>--}}
                                        {{--<div id="tabValidation{{$index}}" style="color: red" class="assign_class d-none validation_text" data-id = "choose-locker-select-child-locker-validation" data-value = "{{t('choose-locker-select-child-locker-validation')}}" >{!! t('choose-locker-select-child-locker-validation')!!}</div>--}}

                                        {{--<div class="assign_class d-none" style="color: red" id="locker_already_exists_msg{{$index}}" data-id = "choose-locker-already-exists-locker" data-value = "{{t('choose-locker-already-exists-locker')}}" >{!! t('choose-locker-already-exists-locker')!!}</div>--}}

                                        {{--<div class="form-group">--}}

                                            {{--@if($location_group == 1)--}}

                                                {{--<p class="">{{$childrens[$index]->child_first_name}} {{$childrens[$index]->child_surname}}  ( {{Carbon\Carbon::parse($childrens[$index]->start)->format('d M, Y')}} - {{Carbon\Carbon::parse($childrens[$index]->end)->format('d M, Y')}} ) </p>--}}

                                            {{--@else--}}
                                                {{--<p class="">{{$childrens[$index]->child_first_name}} {{$childrens[$index]->child_surname}}  ( {{Carbon\Carbon::parse($childrens[$index]->start)->format('d M, Y')}} - {{Carbon\Carbon::parse($childrens[$index]->end)->format('d M, Y')}} )</p>--}}

                                            {{--@endif--}}
                                        {{--</div>--}}

                                        {{--<div class="form-group">--}}
                                            {{--<label for="blocks" class="assign_class" data-id = "choose-locker-select-block-title" data-value = "{{t('choose-locker-select-block-title')}}" >{!! t('choose-locker-select-block-title')!!}</label>--}}
                                            {{--{!! $child_block !!}--}}

                                        {{--</div>--}}
                                        {{--<br>--}}
                                        {{--<div class="row" style="text-align:center;padding-left: 10px;" id="display_lockers{{$index+1}}">--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--@endif--}}
                            {{--@endforeach--}}

                        {{--</div>--}}
                    </div>
                </div>
            </div>
                                <div class="col-md-4">

                                    <div class="row right_school_booking_block">
                                        <div class="col-lg-12 col-12" >
                                            @if(Session::get('show_hide_lockers') == 'Grid Show' || Session::has('show_hide_lockers') == false)
                                                <div class="show-hide-locker-box" style="background-color: black;border-radius: 2%">
                                                    <div class="row" style="margin-top: 10px">
                                                        <div class="col-lg-12 col-12" >
                                                            <div class="text-center assign_class d-none"  id="show_all_lockers_title" style="color: white;margin-top: 35px;font-size: 25px;padding: 5px 13px" data-id = "choose-locker-show-all-lockers" data-value = "{{t('choose-locker-show-all-lockers')}}" >{!! t('choose-locker-show-all-lockers')!!}</div>
                                                            <div class="text-center assign_class "  id="hide_booked_lockers_title" style="color: white;margin-top: 35px;font-size: 23px;padding: 9px 13px" data-id = "choose-locker-hide-booked-lockers-title" data-value = "{{t('choose-locker-hide-booked-lockers-title')}}" >{!! t('choose-locker-hide-booked-lockers-title')!!}</div>

                                                            {{--<div class="text-center" id="hide_show_title" style="color: white;margin-top: 35px;font-size: 23px;padding: 9px 13px">Hide lockers that are booked</div>--}}
                                                        </div>
                                                        <div class="col-lg-12 col-12 text-center d-none" style="margin-bottom: 35px" id="show_all_lockers">
                                                            <button  class="assign_class btn btn-outline-secondary" style="color: white" data-id = "choose-locker-grid-show-button" data-value = "{{t('choose-locker-grid-show-button')}}" >{!! t('choose-locker-grid-show-button')!!}</button>
                                                            {{--<button class="btn btn-outline-secondary" style="color: white" >Grid Show</button>--}}
                                                        </div>

                                                        <div class="col-lg-12 col-12 text-center" style="margin-bottom: 35px" id="hide_booked_lockers">
                                                            <button  class="assign_class btn btn-outline-danger" style="color: white" data-id = "choose-locker-grid-hidden-button" data-value = "{{t('choose-locker-grid-hidden-button')}}" >{!! t('choose-locker-grid-hidden-button')!!}</button>

                                                            {{--<button class="btn btn-outline-danger" style="color: white">Grid Hidden</button>--}}
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="show-hide-locker-box" style="background-color: #f5b54a;border-radius: 2%">
                                                    <div class="row" style="margin-top: 10px">
                                                        <div class="col-lg-12 col-12" >
                                                            <div class="text-center assign_class"  id="show_all_lockers_title" style="color: white;margin-top: 35px;font-size: 25px;padding: 5px 13px" data-id = "choose-locker-show-all-lockers" data-value = "{{t('choose-locker-show-all-lockers')}}" >{!! t('choose-locker-show-all-lockers')!!}</div>
                                                            <div class="text-center assign_class d-none"  id="hide_booked_lockers_title" style="color: white;margin-top: 35px;font-size: 23px;padding: 9px 13px" data-id = "choose-locker-hide-booked-lockers-title" data-value = "{{t('choose-locker-hide-booked-lockers-title')}}" >{!! t('choose-locker-hide-booked-lockers-title')!!}</div>

                                                            {{--<div class="text-center" id="hide_show_title" style="color: white;margin-top: 35px;font-size: 25px;padding: 5px 13px">Show all lockers</div>--}}
                                                        </div>
                                                        <div class="col-lg-12 col-12 text-center" style="margin-bottom:35px" id="show_all_lockers">
                                                            <button  class="assign_class btn btn-outline-secondary" style="color: white" data-id = "choose-locker-grid-show-button" data-value = "{{t('choose-locker-grid-show-button')}}" >{!! t('choose-locker-grid-show-button')!!}</button>
                                                            {{--<button class="btn btn-outline-secondary" style="color: white" >Grid Show</button>--}}
                                                        </div>

                                                        <div class="col-lg-12 col-12 text-center d-none" id="hide_booked_lockers" style="margin-bottom: 35px">
                                                            <button  class="assign_class btn btn-outline-danger" style="color: white" data-id = "choose-locker-grid-hidden-button" data-value = "{{t('choose-locker-grid-hidden-button')}}" >{!! t('choose-locker-grid-hidden-button')!!}</button>

                                                            {{--<button class="btn btn-outline-danger" style="color: white;" >Grid Hidden</button>--}}
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row right_school_booking_block">
                                        <div class="col-lg-12 col-12">
                                            <div class="m-t-10 header_align" id="timer-card" style="color: white;border-radius: 2%">
                                                <div class="row">
                                                    <div class="col-lg-12 col-12">
                                                        <div class="row">
                                                            <div class="col-12 float-left">
                                                                <div class="current-date assign_class" style="font-size: 25px;padding: 5px 13px" data-id = "choose-locker-timer-text" data-value = "{{t('choose-locker-timer-text')}}" >{!! t('choose-locker-timer-text')!!}</div>
                                                            </div>
                                                            <div class="col-12">
                                                                <span class="time float-right assign_class" style="font-size: 45px;padding: 2px 13px" id="timer" data-id = "choose-locker-minutes" data-placeholders="%minutes%"  data-value = "{{t('choose-locker-minutes')}}" >{!! t('choose-locker-minutes') !!}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row right_school_booking_block">
                                        <div class="col-lg-12 col-12">
                                            <div class="m-t-10 header_align" id="timer-card" style="color: white;border-radius: 2%">
                                                <div class="row">
                                                    <div class="col-lg-12 col-12 show_locker_image"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="container"  >
                    <p id="demo"></p>
                </div>

                <br>
                <div class="text-center">
                    <a href="javascript:void(0)" id="left_arrow_button"
                    class=" button_text green-button assign_class "
                    data-id="school-term-back-booking-button"
                    data-value="{{t('school-term-back-booking-button')}}" style="padding: 9px 10px;"> <span style="font-size: 20px;">&larr;</span> {!!
                    t('school-term-back-booking-button')!!}</a>
                    <a href="#." class="bg-blue-color button_text assign_class reset-booking" data-sale_id="{{$booking_id}}" data-id = "choose-locker-reset-button" data-value = "{{t('choose-locker-reset-button')}}"  style="padding: 10px 20px;">{!! t('choose-locker-reset-button')!!}</a>
                    <button  type="button"  class="green-button button_text assign_class" id="proceed_button" data-id = "choose-locker-proceed-button" data-value = "{{t('choose-locker-proceed-button')}}">{!! t('choose-locker-proceed-button')!!}</button>
                </div>

            </div>

            @csrf
        </form>
    </section><!-- #portfolio -->
</main>

@endsection

@section('script')

    {{--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>--}}
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>--}}
    {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>--}}

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/frontend/js/pages/radio_checkbox.js"></script>

    <script type="text/javascript" src="/frontend/vendors/chosen/js/chosen.jquery.js"></script>
    <script type="text/javascript" src="/frontend/js/pages/form_elements.js"></script>

    <script src="/js/bootstrap-datetimepicker.min.js"></script>
    <script src="/frontend/vendors/snabbt/js/snabbt.min.js"></script>
    <script src="/frontend/vendors/wow/js/wow.min.js"></script>

    <script src="/frontend/vendors/twitter-bootstrap-wizard/js/jquery.bootstrap.wizard.min.js"></script>

    <script type="text/javascript" src="/frontend/js/pages/wizard.js"></script>
    <script type="text/javascript" src="/frontend/js/bookings/choose_locker.js"></script>

    <script type="text/javascript">


        $(function () {

            var arrowPage = "{{Session::get('arrowsPage')}}";

            if (arrowPage == 3) {

                $("#left_arrow").removeClass('d-none');

            } else if (arrowPage > 3) {

                $("#left_arrow").removeClass('d-none');
                $("#right_arrow").removeClass('d-none');
            }

            $('body').on('click', '#left_arrow', function (e) {

                window.location.href = '{{ route('schooltermbasedback') }}';

            });

             $('body').on('click', '#left_arrow_button', function (e) {

                window.location.href = '{{ route('schooltermbasedback') }}';

            });

            $('body').on('click', '#right_arrow', function (e) {

                window.location.href = '{{ route('saveSchoolBookingForward') }}';

            });

            $('body').on('click', '.choose-locker', function (e) {
                
                var counter = $(this).data('counter');

                $('#tabValidation'+ counter).addClass('d-none');
                $('#locker_already_exists_msg'+counter).addClass('d-none');

                var sale_id = $(this).data('sale_id');

                var block_id = $(this).data('block_id');
                var locker_id = $(this).data('locker_id');
                var locker_number = $(this).data('locker_number');

                var rate = $(this).data('rate');
                var tax = $(this).data('tax');
                var hold_locker = $(this).data('hold_locker');


                var index_of_sale_ids = sessionStorage.getItem("selected_dropdown_current_tab");
                console.log(index_of_sale_ids,'index_of_sale_ids')

                // sessionStorage.removeItem("selected_lockers");
                var storedArray = JSON.parse(sessionStorage.getItem("selected_lockers"));
                console.log(storedArray,'storedArray')
                var testArray = [];
                var i;

                if (storedArray != null) {
                    for (i = 0; i < storedArray.length; i++) {
                        testArray[i] = storedArray[i];
                    }
                }

                testArray[index_of_sale_ids] = locker_id;
                console.log(typeof  testArray ,'testArray');

                sessionStorage.setItem("selected_lockers", JSON.stringify(testArray));
                var selected_lockers = JSON.parse(sessionStorage.getItem("selected_lockers"));

                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                $.ajax({

                    url: '/frontend/updateSale',

                    type: 'POST',

                    dataType: 'JSON',

                    cache: false,

                    data: {
                        sale_id: sale_id,
                        block_id: block_id,
                        locker_id: locker_id,
                        locker_number: locker_number,
                        rate: rate,
                        tax: tax,
                        hold_locker: hold_locker,
                        _token: CSRF_TOKEN
                    },

                    success: function (data) {
                     
                        if(data == 'home_page'){
                            document.location.href = "/";
                        }
                        if(data.status == 'already_exists'){
                            $('#locker_already_exists_msg'+counter).removeClass('d-none');
                            const  selected_lockers = JSON.parse(sessionStorage.getItem("selected_lockers"));

                            var  locker_id = parseInt(data.locker_id);

                            const index = selected_lockers.indexOf(locker_id);
                            if (index > -1) {
                                selected_lockers.splice(index, 1);
                            }
                            sessionStorage.setItem("selected_lockers", JSON.stringify(selected_lockers));
                            var reomoved_locker =  JSON.parse(sessionStorage.getItem("selected_lockers"));

                        }else if(data.status == 'available'){
                            $('.nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link').css("color", "black")
                        }
                        getNewLockers();

                    },

                    error: function (jqXHR, textStatus, errorThrown) {



                    }

                });


            });


        });

    </script>

@endsection