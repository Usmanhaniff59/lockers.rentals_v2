
 <section id="portfolio"  class="section-bg" >
            @if(Session::has('flash_message'))
                <div class="col-md-12">
                    <div class="alert alert-success alert-dismissable float-right" style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-hidden="true" style="margin-top: 4px; margin-left: 15px;"> ×
                        </button>
                        {{ Session::get('flash_message') }}
                    </div>
                </div>
            @endif

            <div class="container">
                <header class="section-header">
                    {{--<h3 class="section-title">Type of Booking for '{{$school->name}}'</h3>--}}
                    <h3 class="section-title assign_class school_header" data-id = "payments-confirmation-main-title" data-value = "{{t('payments-confirmation-main-title')}}"  data-steps="yes" >{!! t('payments-confirmation-main-title')!!}<div class="after-title-step2"></div><div class="after-title-step3"></div><div class="after-title-step4"></div><div class="after-title-step5"></div></h3>
                </header>
                <div class="outer">
                    {{--<div class="inner bg-container">--}}
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header bg-white assign_class" data-id = "payments-confirmation-card-main-title" data-value = "{{t('payments-confirmation-card-main-title')}}" >{!! t('payments-confirmation-card-main-title')!!}</div>
                                <div class="card-body cards_section_margin">
                                    <div class="row">
                                        <div class="col-lg m-t-35">
                                            <form id="paymentForm" action="{{route('savepayments')}}" method="post" >
                                                <div class="row">

                                                    <div class="col-md-7">
                                                        {{--{{dd()}}--}}
                                                        @if($booking_confirmation == 'Confirmed')
                                                            {{--{{dd($booking_confirmation)}}--}}
                                                            <div class="card card-inverse" style="background-color: #9BBFBF">
                                                                <div class="card-header" style="background-color: #9BBFBF"><h3 class="section-title assign_class" style="color: white" data-id = "payments-confirmation-card-success-title" data-value = "{{t('payments-confirmation-card-success-title')}}" >{!! t('payments-confirmation-card-success-title')!!}</h3> </div>
                                                                <div class="card-body">
                                                                    <p  class="text-center" style="color: white"><span class="assign_class" data-id = "payments-confirmation-card-success-msg" data-placeholders="{{$payment_success_msg_placeholders}}" data-placeholderval="12, {{$companyIfo->company_name}},{{$companyIfo->email}},{{$companyIfo->booking_id}}" data-value = "{{t('payments-confirmation-card-success-msg')}}" >{!! str_replace(explode(',', $payment_success_msg_placeholders),[12, $companyIfo->company_name,$companyIfo->email,$companyIfo->booking_id],t('payments-confirmation-card-success-msg')) !!}</span> </p><br>
                                                                    {{--<h3 class="text-center" style="color: white"><span >{{$companyIfo->booking_id}}</span></h3>--}}
                                                                    {{--<div class="text-center"><a href="/admin/payment" class="green-button button_text assign_class book-another-locker"  data-id = "payments-confirmation-check-your-order-button" data-value = "{{t('payments-confirmation-check-your-order-button')}}"  style="padding: 10px 20px;">{!! t('payments-confirmation-check-your-order-button')!!}</a>--}}
                                                                    <div class="text-center"><a href="{{route('payment.index')}}" class="green-button"  data-id = "payments-confirmation-check-your-order-button" data-value = "{{t('payments-confirmation-check-your-order-button')}}"  style="padding: 10px 20px;">{!! t('payments-confirmation-check-your-order-button')!!}</a>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        @else
                                                            <div class="card card-inverse"  style="background-color: #AD0A08">
                                                                <div class="card-header assign_class"  style="background-color: #AD0A08" data-id = "payments-confirmation-card-failed-title" data-value = "{{t('payments-confirmation-card-failed-title')}}" >{!! t('payments-confirmation-card-failed-title')!!}  </div>
                                                                <div class="card-body">
                                                                    <p  class="text-center" style="color: white"><span class="assign_class" data-id = "payments-confirmation-booking-already-exists-msg"   data-value = "{{t('payments-confirmation-booking-already-exists-msg')}}" >{!! t('payments-confirmation-booking-already-exists-msg') !!}</span> </p><br>

                                                                    {{--<h2  class="text-center">Booking Confirmation Number</h2><br>--}}
                                                                    {{--<h1 class="text-center">{{$payment->id}}</h1>--}}
                                                                    <div class="text-center"><a href="{{route('payment.index')}}" class="green-button"  data-id = "payments-confirmation-check-your-order-button" data-value = "{{t('payments-confirmation-check-your-order-button')}}"  style="padding: 10px 20px;">{!! t('payments-confirmation-check-your-order-button')!!}</a>


                                                                    </div>
                                                            </div>
                                                        @endif
                                                      
                                                        <!--Carousel Wrapper-->
                                                        <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails m-t-20" data-ride="carousel">
                                                          <!--Slides-->
                                                          <div class="carousel-inner" role="listbox">
                                                            <?php $isActive = false; ?>
                                                            @foreach ($marketing_pictures as $picture)
                                                            <div class="carousel-item {{$isActive ? 'hjg' : 'active'}} ">
                                                              <img class="d-block w-100" src="{{url('uploads/'.$picture->file)}}"
                                                                alt="First slide">
                                                            </div>
                                                            <?php $isActive = true; ?>
                                                            @endforeach
                                                          </div>
                                                          <!--/.Slides-->
                                                          <!--Controls-->
                                                          <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
                                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                            <span class="sr-only">Previous</span>
                                                          </a>
                                                          <a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
                                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                            <span class="sr-only">Next</span>
                                                          </a>
                                                          <!--/.Controls-->
                                                          <ol class="carousel-indicators">
                                                            <?php $i = 1; ?>
{{--                                                            @foreach ($marketing_pictures as $picture)--}}
{{--                                                            <li data-target="#carousel-thumb" data-slide-to="{{$i}}" class="active">--}}
{{--                                                              <img src="{{url('uploads/'.$picture->file)}}" width="100">--}}
{{--                                                            </li>--}}
{{--                                                            <?php $i++; ?>--}}
{{--                                                            @endforeach--}}
                                                          </ol>
                                                        </div>
                                                        <!--/.Carousel Wrapper-->
                                                    </div>

                                                    <div class="col-md-5">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                 <h3 class="assign_class" data-id = "payments-booking-info-company-title" data-value = "{{t('payments-booking-info-company-title')}}" >{!! t('payments-booking-info-company-title')!!}</h3>
                                                                 <div><span class="company-info assign_class" data-id = "payments-booking-info-name" data-value = "{{t('payments-booking-info-name')}}" >{!! t('payments-booking-info-name')!!} </span>{{$companyIfo->company_name}}</div>
                                                        <p><span class="company-info assign_class" data-id = "payments-booking-info-address" data-value = "{{t('payments-booking-info-address')}}" >{!! t('payments-booking-info-address')!!} </span>{{$companyIfo->company_street}} {{$companyIfo->company_city}} {{$companyIfo->company_country}}</p>
                                                            </div>
                                                        </div>
                                                       <br>
                                                         <div class="card">
                                                            <div class="card-body">
                                                                <h3 class="assign_class" data-id = "payments-booking-info-booked-title" data-value = "{{t('payments-booking-info-booked-title')}}" >{!! t('payments-booking-info-booked-title')!!}</h3>

                                                                <div><span class="company-info assign_class" data-id = "payments-user-info-reservation-id" data-value = "{{t('payments-user-info-reservation-id')}}" >{!! t('payments-user-info-reservation-id')!!} </span>{{$companyIfo->booking_id}}</div>

                                                                @if($currency->position == 'b')
                                                                    <div><span class="company-info assign_class fa" data-id = "payments-user-info-lockers-cost" data-value = "{{t('payments-user-info-lockers-cost')}}" >{!! t('payments-user-info-lockers-cost')!!}  </span>{{$currency->symbol}}{{$total_lockers_price}}</div>
                                                                    <div><span class="company-info assign_class fa" data-id = "payments-user-info-charity-cost" data-value = "{{t('payments-user-info-charity-cost')}}" >{!! t('payments-user-info-charity-cost')!!}  </span> {{$currency->symbol}}{{$charity_price}}</div>
                                                                    <p><span class="company-info assign_class fa" data-id = "payments-user-info-cost" data-value = "{{t('payments-user-info-cost')}}" >{!! t('payments-user-info-cost')!!}  </span>{{$currency->symbol}}{{$total_price}}</p>

                                                                @else
                                                                    <div><span class="company-info assign_class fa" data-id = "payments-user-info-lockers-cost" data-value = "{{t('payments-user-info-lockers-cost')}}" >{!! t('payments-user-info-lockers-cost')!!}  </span>{{$currency->symbol}}{{$total_lockers_price}}</div>
                                                                    <div><span class="company-info assign_class fa" data-id = "payments-user-info-charity-cost" data-value = "{{t('payments-user-info-charity-cost')}}" >{!! t('payments-user-info-charity-cost')!!}  </span> {{$currency->symbol}}{{$charity_price}}</div>
                                                                    <p><span class="company-info assign_class fa" data-id = "payments-user-info-cost" data-value = "{{t('payments-user-info-cost')}}" >{!! t('payments-user-info-cost')!!}  </span> {{$currency->symbol}}{{$total_price}}</p>
                                                                @endif

                                                                @foreach($lockerInfo as $locker)

                                                                    <div><span class="company-info assign_class" data-id = "payments-booking-info-child-name" data-value = "{{t('payments-booking-info-child-name')}}" >{!! t('payments-booking-info-child-name')!!}  </span> {{$locker->childName}}</div>
                                                                    @if($locker->child_email)
                                                                        <div><span class="company-info assign_class" data-id = "payments-booking-info-child-email" data-value = "{{t('payments-booking-info-child-email')}}" >{!! t('payments-booking-info-child-email')!!} </span> {{$locker->child_email}}</div>
                                                                    @endif
                                                                    <div><span class="company-info assign_class" data-id = "payments-booking-info-start-rent" data-value = "{{t('payments-booking-info-start-rent')}}" >{!! t('payments-booking-info-start-rent')!!} </span>@if($companyIfo->location_group_id == 1 || $companyIfo->location_group_id == 2){{date('d M, Y', strtotime($locker->start))}}@else{{date('d M, Y H:i', strtotime($locker->start))}}@endif</div>
                                                                    <div><span class="company-info assign_class" data-id = "payments-booking-info-end-rent" data-value = "{{t('payments-booking-info-end-rent')}}" >{!! t('payments-booking-info-end-rent')!!} </span>@if($companyIfo->location_group_id == 1 || $companyIfo->location_group_id == 2){{date('d M, Y', strtotime($locker->end))}}@else{{date('d M, Y H:i', strtotime($locker->end))}}@endif</div>
                                                                    <div><span class="company-info assign_class" data-id = "payments-booking-info-locker-block" data-value = "{{t('payments-booking-info-locker-block')}}" >{!! t('payments-booking-info-locker-block')!!} </span>{{$locker->block_name}}</div>
                                                                    <div><span class="company-info assign_class" data-id = "payments-booking-info-locker-number" data-value = "{{t('payments-booking-info-locker-number')}}" >{!! t('payments-booking-info-locker-number')!!} </span>{{$locker->locker_number}}</div>
                                                                    <div><span class="company-info assign_class" data-id = "payments-booking-info-locker-code" data-value = "{{t('payments-booking-info-locker-code')}}" >{!! t('payments-booking-info-locker-code')!!} </span>{{$locker->net_pass}}</div>

                                                                @if($currency->position == 'b')
                                                                        <p><span class="company-info assign_class fa" data-id = "payments-booking-info-locker-price" data-value = "{{t('payments-booking-info-locker-price')}}" >{!! t('payments-booking-info-locker-price')!!} </span>  {{$currency->symbol}}{{$locker->price}}</p>

                                                                    @else
                                                                        <p><span class="company-info assign_class fa" data-id = "payments-booking-info-locker-price" data-value = "{{t('payments-booking-info-locker-price')}}" >{!! t('payments-booking-info-locker-price')!!} </span> {{$currency->symbol}}{{$locker->price}}</p>
                                                                    @endif

                                                                @endforeach
                                                            </div>
                                                        </div>
                                                        <br>
                                                         <div class="card">
                                                            <div class="card-body">
                                                                <p><span class="company-info assign_class" data-id = "payments-booking-info-terms-msg" data-value = "{{t('payments-booking-info-terms-msg')}}" >{!! t('payments-booking-info-terms-msg')!!}  </span> <i class="fa fa-check icon-tick" style="color: #F5B54A;font-size: x-large;margin-left: 5px" ></i ></p>
                                                            </div>
                                                        </div>
                                                        <br>
                                                         <div class="card">
                                                            <div class="card-body">
                                                                <h3 style="margin-top: 15px" class="assign_class" data-id = "payments-booking-info-user-main-title" data-value = "{{t('payments-booking-info-user-main-title')}}" >{!! t('payments-booking-info-user-main-title')!!}</h3>
                                                                <div><span class="company-info assign_class" data-id = "payments-user-info-name" data-value = "{{t('payments-user-info-name')}}" >{!! t('payments-user-info-name')!!} </span>@if($companyIfo->name || $companyIfo->surname){{$companyIfo->name}} {{$companyIfo->surname}} @else {{$companyIfo->username }} @endif</div>
                                                                <div><span class="company-info assign_class" data-id = "payments-user-info-email" data-value = "{{t('payments-user-info-email')}}" >{!! t('payments-user-info-email')!!} </span>{{$companyIfo->email}}</div>


                                                                {{--<h2 style="margin-top: 37px">Company Info</h2>--}}
                                                                {{--<p style="margin-bottom: 10px"><span class="company-info">Name : </span>{{$lockerInfo->company_name}}</p>--}}
                                                                {{--<p style="margin-bottom: 10px"><span class="company-info">Address : </span>{{$lockerInfo->address}}</p>--}}
                                                                {{--<p style="margin-bottom: 10px"><span class="company-info">Start Rent : </span>{{$lockerInfo->start}}</p>--}}
                                                                {{--<p style="margin-bottom: 10px"><span class="company-info">End Rent : </span>{{$lockerInfo->end}}</p>--}}
                                                                {{--<p style="margin-bottom: 10px"><span class="company-info">Locker Block : </span>{{$lockerInfo->block_name}}</p>--}}
                                                                {{--<p style="margin-bottom: 10px"><span class="company-info">Locker Number : </span>{{$lockerInfo->locker_number}}</p>--}}
                                                                {{--<p style="margin-bottom: 10px"><span class="company-info">Agreed terms and conditions :   </span> <i class="fa fa-check icon-tick" style="color: #F5B54A;font-size: xx-large;margin-left: 5px" ></i ></p>--}}

                                                                {{--<h2 style="margin-top: 50px">Personal Info</h2>--}}
                                                                {{--<p style="margin-bottom: 10px"><span class="company-info">Name : </span>{{$lockerInfo->name}}</p>--}}
                                                                {{--<p style="margin-bottom: 10px"><span class="company-info">Email : </span>{{$lockerInfo->email}}</p>--}}
                                                                {{--<p style="margin-bottom: 10px"><span class="company-info">Cost : </span>{{$lockerInfo->price}}</p>--}}
                                                                {{--<p style="margin-bottom: 10px"><span class="company-info">Reservation ID : </span>{{$lockerInfo->sale_id}}</p>--}}
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                @csrf
                                            </form>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <br>
                <div class="text-center"><a href="/" class="green-button button_text assign_class book-another-locker"  data-id = "payments-confirmation-book-another-locker" data-value = "{{t('payments-confirmation-book-another-locker')}}"  style="padding: 10px 20px;">{!! t('payments-confirmation-book-another-locker')!!}</a>
                </div>
            </div>

        </section>










