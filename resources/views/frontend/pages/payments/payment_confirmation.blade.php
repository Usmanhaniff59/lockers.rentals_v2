@extends('frontend.layouts.app')

@section('style')

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />


<link type="text/css" rel="stylesheet" href="/frontend/css/custom.css" />

<link rel="stylesheet" href="/css/bootstrap-datetimepicker.css">
<link type="text/css" rel="stylesheet" href="/frontend/vendors/checkbox_css/css/checkbox.min.css" />
<link type="text/css" rel="stylesheet" href="/frontend/css/pages/radio_checkbox.css" />
<link rel="stylesheet" href="/frontend/vendors/swiper/css/swiper.min.css" />

<!--Page level styles-->
<link rel="stylesheet" href="/frontend/css/pages/general_components.css" />

<!-- end of plugin styles-->

<style>
    .after-title-step2::after {
        background: #9ABDB7;
    }

    .after-title-step3::after {
        background: #9ABDB7;
    }

    .after-title-step4::after {
        background: #9ABDB7;
    }

    .after-title-step5::after {
        background: #9ABDB7;
    }

    .company-info {
        color: #000;
    }

    .icon-tick:before {
        position: absolute !important;
        top: 16px !important;
    }
</style>
@endsection
@php

@endphp

@section('content')

@include('frontend.pages.payments.payment_confirmation_render')

@endsection

@section('script')

@endsection