@extends('frontend.layouts.app')
@section('style')
    <script src="https://code.jquery.com/jquery-3.4.1.js" crossorigin="anonymous"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>
    <style></style>
@endsection
    @section('content')

        {{--@if(session()->has('locale'))--}}
            {{--{{ dd(session()->get('locale'))}}--}}
        {{--@endif--}}
        <main id="main">
            <!--==========================
                 About Us Section
               ============================-->
            <section id="about">
                <div class="container">
                    <header class="section-header">
                        <h3 class="assign_class" data-id = "policy-heading" data-value = "{{t('policy-heading')}}">{!! t('policy-heading')!!}</h3>
                        <div class="assign_class text-center p-b-20" data-id = "policy-subheading-text" data-value = "{{t('policy-subheading-text')}}">{!! t('policy-subheading-text')!!}</div>
                    </header>
                    <div class="row about-cols">
                        <div class="col-md-12 wow fadeInUp" data-wow-delay="0.1s">
                            <div class="about-col">
                                <div class="img">
                                    <img  alt="" class="img-fluid">
                                    <div class="icon"><i class="fad fa-shield-check"></i></div>
                                </div>
                                <h2 class="assign_class" data-id = "policy-section1-heading" data-value = "{{t('policy-section1-heading')}}">{!! t('policy-section1-heading')!!}</h2>
                                <div class="assign_class" style="padding-left: 20px;padding-right:20px;" data-id = "policy-section1-text" data-value = "{{t('policy-section1-text')}}">{!! t('policy-section1-text')!!}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- #about -->
        </main>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#sel1").select2();
        });
    </script>

@endsection