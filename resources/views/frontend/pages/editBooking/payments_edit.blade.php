@extends('frontend.layouts.app')

@section('style')

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link type="text/css" rel="stylesheet" href="/frontend/css/custom.css" />

    <link rel="stylesheet" href="/css/bootstrap-datetimepicker.css">
    <link type="text/css" rel="stylesheet" href="/frontend/vendors/checkbox_css/css/checkbox.min.css" />
    <link type="text/css" rel="stylesheet" href="/frontend/css/pages/radio_checkbox.css" />
    <link rel="stylesheet" href="/frontend/vendors/swiper/css/swiper.min.css" />
    <link type="text/css" rel="stylesheet" href="/frontend/vendors/Buttons/css/buttons.min.css"/>

    <link type="text/css" rel="stylesheet" href="/frontend/vendors/chosen/css/chosen.css"/>
    <link type="text/css" rel="stylesheet" href="/frontend/css/pages/form_elements.css"/>

    <!--Page level styles-->
    <link rel="stylesheet" href="/frontend/css/pages/general_components.css" />


    <!-- end of plugin styles-->

    <style>
        .after-title-step2::after {
            background: #9ABDB7;
        }
        .after-title-step3::after {
            background: #9ABDB7;
        }
        .after-title-step4::after {
            background: #9ABDB7;
        }
        .after-title-step5::after {
            background: red;
        }
        .company-info{
            color: #000;
        }
        *.font-awesome .fa {
            font-family: "Font Awesome 5 Free", Open Sans;
        }

        .form-check-label{
            margin-left: 3px !important;
        }
        .form-check {
            padding-left: 1.50rem !important;
        }
        .icon-tick:before{
            position: absolute !important;
            top: 16px !important;
        }
        #div_for_charity{
            padding-left: 3px !important;
            padding-top: 20px !important;
        }

        .loader {
            position: absolute;
            top: 80%;
            left: 45%;
            margin: auto;
            display: none;
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #00c0ef;
            width: 100px;
            height: 100px;
            -webkit-animation: spin 2s linear infinite; /* Safari */
            animation: spin 2s linear infinite;
            z-index: 100000000;
        }
        /* Safari */
        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>
@endsection
<?php
if(\Illuminate\Support\Facades\Session::has('user_payments_form_info')){
    $form=\Illuminate\Support\Facades\Session::get('user_payments_form_info');

}else{
    $form =null;
}

?>

@section('content')

    <main id="main" >
        <section id="portfolio"  class="section-bg" >
            @if(Session::has('flash_message'))
                <div class="col-md-12">
                    <div class="alert alert-success alert-dismissable float-right" style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-hidden="true" style="margin-top: 4px; margin-left: 15px;"> ×
                        </button>
                        {{ Session::get('flash_message') }}
                    </div>
                </div>
            @endif

            <div class="container">
                <header class="section-header payments-opacity">
                    {{--<h3 class="section-title">Type of Booking for '{{$school->name}}'</h3>--}}
                    <h3 class="section-title school_header" >
                        <span class="assign_class" data-id = "payments-main-title" data-value = "{{t('payments-main-title')}}"  data-steps="yes">{!! t('payments-main-title')!!}</span>
                        <div class="after-left-arrow arrows d-none" id="left_arrow"></div><div class="after-title-step2"></div><div class="after-title-step3"></div><div class="after-title-step4"></div><div class="after-title-step5"></div><div class="after-right-arrow arrows d-none" id="right_arrow"></div></h3>
                </header>
                <div class="outer payments-opacity">
                    {{--<div class="inner bg-container">--}}
                    <div class="row">

                        <div class="col-lg-12">

                            <h3 class="assign_class" data-id = "school-term-sub-title" data-value = "{{t('school-term-sub-title')}}" >{!! t('school-term-sub-title')!!}</h3>

                            <div class="card">
                                <div class="loader" id="block_loader"></div>

                                <div class="card-header bg-white assign_class" data-id = "payments-card-main-title" data-value = "{{t('payments-card-main-title')}}" >{!! t('payments-card-main-title')!!}</div>

                                <div class="card-body cards_section_margin">

                                    <div class="row">

                                        <div class="col-lg m-t-35">

                                            <form id="paymentForm" action="{{route('savepayments')}}" method="post" >
                                                <div class="row">
                                                    <div class="col-md-8">

                                                        @if (session('status'))
                                                            <div class="alert alert-danger">
                                                                {{ session('status') }}
                                                            </div>
                                                        @endif

                                                        {{--<h3 class="assign_class" data-id = "payments-card-inner-title" data-value = "{{t('payments-card-inner-title')}}" >{!! t('payments-card-inner-title')!!}</h3>--}}

                                                        {{(session::put('payment',false))}}
                                                        <input type="hidden" class="form-control" id="sale_id" name="sale_id" value="{{$companyIfo->booking_id}}">
                                                        {{--<input type="hidden" class="form-control" id="first_name" name="first_name" value="{{$companyIfo->first_name}}">--}}
                                                        {{--<input type="hidden" class="form-control" id="surname" name="surname" value="{{$companyIfo->surname}}">--}}
                                                        <input type='hidden' class="form-control" id="timeDifference" name="timeDifference" value="{{$remainingTime}}" />

                                                        <div class="card">

                                                            <div class="card-body">
                                                                <div id="manual_address_fields">
                                                                    <div class="row">
                                                                        <div class="col-6">
                                                                            <div class="form-group">

                                                                                <label for="payername1" class="assign_class" data-id = "payments-payername1-label" data-value = "{{ t('payments-payername1-label') }}" >{!! t('payments-payername1-label') !!}</label>
                                                                                <input type="text" class="form-control assign_class" id="firstname" name="firstname"
                                                                                       value="@if($form) {{$form['firstname']}} @elseif(isset($companyIfo->name)) {{$companyIfo->name  }}@else @endif" placeholder="{!! t('payments-payername1-placeholder')!!}" data-id = "payments-payername1-placeholder" data-value = "{{t('payments-payername1-placeholder')}}" data-inputplaceholder="yes" >
                                                                                <small id="firstnameValidation" class="assign_class d-none validation_text" style="color: red" data-id = "payments-payername1-validation" data-value = "{{t('payments-payername1-validation')}}" >{!! t('payments-payername1-validation')!!}</small>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="form-group">
                                                                                <label for="payername1" class="assign_class" data-id = "payments-surname-label" data-value = "{{ t('payments-surname-label') }}" >{!! t('payments-surname-label') !!}</label>

                                                                                <input type="text" class="form-control assign_class" id="payer_surname" name="payer_surname" value="@if($form) {{$form['payer_surname']}} @elseif(isset($companyIfo->surname)) {{ $companyIfo->surname }}@else @endif"  data-id = "payments-surname-placeholder" data-value = "{{t('payments-surname-placeholder')}}" data-inputplaceholder="yes" >

                                                                                <small id="surnameValidation" class="assign_class d-none validation_text" style="color: red" data-id = "payments-surname-validation" data-value = "{{t('payments-surname-validation')}}" >{!! t('payments-surname-validation')!!}</small>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    {{--<input type='hidden' class="form-control"  name="address-radio-default" value="@if($form) @if(isset($form['address-radio-default'])) 1 @endif @else 1 @endif" />--}}

                                                                    <div class="form-group">
                                                                        {{--{{dd($companyIfo->address_2)}}--}}
                                                                        @if($companyIfo->address_1  && $companyIfo->address_2 &&  $companyIfo->user_city &&  $companyIfo->user_post_code &&  $companyIfo->user_state )

                                                                            <div class="form-check">
                                                                                <input type="radio" class="form-check-input radio-charity" id="address-radio-default" name="address-radio"  checked >
                                                                                <label class="form-check-label" for="materialUnchecked" style="margin-top: 0px">Use my address</label>
                                                                            </div>
                                                                        @else
                                                                            {{--<input type="radio" class="form-check-input radio-charity" id="address-radio-default" name="address-radio" @if($form)@if(isset($form['address-radio-default'])) checked @endif @endif>--}}

                                                                        @endif

                                                                        <div class="form-check">
                                                                            @if($companyIfo->address_1  && $companyIfo->address_2 &&  $companyIfo->user_city &&  $companyIfo->user_post_code &&  $companyIfo->user_state )

                                                                                <input type="radio" class="form-check-input radio-charity" id="address-radio-change" name="address-radio" @if($form)@if(isset($form['address-radio-change'])) checked @endif @endif/>
                                                                            @else

                                                                                <input type="radio" class="form-check-input radio-charity" id="address-radio-change" name="address-radio" @if($form)@if(isset($form['address-radio-change'])) checked @endif @else checked  @endif/>
                                                                            @endif
                                                                            <label class="form-check-label" for="materialUnchecked" style="margin-top: 0px">Change my address</label>
                                                                        </div>
                                                                        <input type="hidden" name="charity_id" value="{{$charity->id}}">
                                                                        {{--<input type='hidden' class="form-control"  name="address-radio-diff" value="@if($form)@if(isset($form['address-radio-diff'])) 1 @endif @else 1 @endif" />--}}

                                                                        <div class="form-check">
                                                                            <input type="radio" class="form-check-input radio-charity" id="address-radio-diff" name="address-radio" @if($form)@if(isset($form['address-radio-diff'])) checked @endif @endif />
                                                                            <label class="form-check-label" for="materialUnchecked" style="margin-top: 0px">Use Different Address</label>
                                                                        </div>
                                                                    </div>


                                                                    @if($companyIfo->address_1  && $companyIfo->address_2 &&  $companyIfo->user_city &&  $companyIfo->user_post_code &&  $companyIfo->user_state )
                                                                        <div class="card" id="use-my-address-text">
                                                                            @else
                                                                                <div class="card d-none" id="use-my-address-text">
                                                                                    @endif
                                                                                    <div class="card-body">
                                                                                        {{--<p>{{$companyIfo->}}</p>--}}
                                                                                        <div class="m-t-5 text-left "><span class="text-left text-muted assign_class" data-id = "payments-use-my-address1-text" data-value = "{{t('payments-use-my-address1-text')}}" >{!! t('payments-use-my-address1-text')!!}</span> </div>
                                                                                        <span class="m-b-20 text-left" > {{$companyIfo->address_1}} {{$companyIfo->address_2}} {{$companyIfo->user_city}} {{$companyIfo->user_post_code}} {{$companyIfo->user_state}}</span>

                                                                                    </div>
                                                                                </div>
                                                                                @if($companyIfo->address_1  && $companyIfo->address_2 &&  $companyIfo->user_city &&  $companyIfo->user_post_code &&  $companyIfo->user_state )
                                                                                    <div class="card d-none" id="use-my-address-inputs" >
                                                                                        @else
                                                                                            <div class="card" id="use-my-address-inputs" >
                                                                                                @endif

                                                                                                <div class="card-body">
                                                                                                    <div id="manual_address_fields">
                                                                                                        <div class="form-group">
                                                                                                            <label for="billingAddress1" class="assign_class" data-id = "payments-billingaddress1-label" data-value = "{{ t('payments-billingaddress1-label') }}" >{!! t('payments-billingaddress1-label') !!}</label>
                                                                                                            <input type="text" class="form-control assign_class" id="billing_address1" name="billing_address1" value="@if($form) {{$form['billing_address1']}} @else {{$companyIfo->address_1}} @endif" placeholder="{!! t('payments-billingaddress1-placeholder')!!}" data-id = "payments-billingaddress1-placeholder" data-value = "{{t('payments-billingaddress1-placeholder')}}" data-inputplaceholder="yes">
                                                                                                            <small id="billingAddress1Validation" class="assign_class d-none validation_text" style="color: red" data-id = "payments-billingaddress1-validation" data-value = "{{t('payments-billingaddress1-validation')}}" >{!! t('payments-billingaddress1-validation')!!}</small>
                                                                                                        </div>
                                                                                                        <div class="form-group">
                                                                                                            <label for="billingAddress2" class="assign_class" data-id = "payments-billingaddress2-label" data-value = "{{t('payments-billingaddress2-label')}}" >{!! t('payments-billingaddress2-label') !!}</label>
                                                                                                            <input type="text" class="form-control assign_class" id="billing_address2" name="billing_address2" value="@if($form) {{$form['billing_address2']}} @else {{$companyIfo->address_2}} @endif" placeholder="{!! t('payments-billingaddress2-placeholder')!!}" data-id = "payments-billingaddress2-placeholder" data-value = "{{t('payments-billingaddress2-placeholder')}}" data-inputplaceholder="yes">
                                                                                                            <small id="billingAddress2Validation" class="assign_class d-none validation_text" style="color: red" data-id = "payments-billingaddress2-validation" data-value = "{{t('payments-billingaddress2-validation')}}" >{!! t('payments-billingaddress2-validation')!!}</small>
                                                                                                        </div>
                                                                                                        <div class="form-group">
                                                                                                            <label for="exampleInputEmail1" class="assign_class" data-id = "payments-city-label" data-value = "{{t('payments-city-label')}}" >{!! t('payments-city-label') !!}</label>
                                                                                                            <input type="text" class="form-control assign_class" id="city" name="city" value="@if($form) {{$form['city']}}@else {{$companyIfo->user_city}} @endif" placeholder="{!! t('payments-city-placeholder')!!}" data-id = "payments-city-placeholder" data-value = "{{t('payments-city-placeholder')}}" data-inputplaceholder="yes">
                                                                                                            <small id="cityValidation" class="assign_class d-none validation_text" style="color: red" data-id = "payments-city-validation" data-value = "{{t('payments-city-validation')}}" >{!! t('payments-city-validation')!!}</small>
                                                                                                        </div>
                                                                                                        <div class="form-group">
                                                                                                            <label for="state" class="assign_class" data-id = "payments-state-label" data-value = "{{t('payments-state-label')}}" >{!! t('payments-state-label')!!}</label>
                                                                                                            <input type="text" class="form-control assign_class" id="state" name="state" value="@if($form) {{$form['state']}}@else {{$companyIfo->user_state}} @endif" placeholder="{!! t('payments-state-placeholder')!!}" data-id = "payments-state-placeholder" data-value = "{{t('payments-state-placeholder')}}" data-inputplaceholder="yes">
                                                                                                            <small id="stateValidation" class="assign_class d-none validation_text" style="color: red" data-id = "payments-state-validation" data-value = "{{t('payments-state-validation')}}" >{!! t('payments-state-validation')!!}</small>
                                                                                                        </div>
                                                                                                        <div class="form-group">
                                                                                                            <label for="exampleInputEmail1" class="assign_class" data-id = "payments-postcode-label" data-value = "{{t('payments-postcode-label')}}" >{!! t('payments-postcode-label')!!}</label>
                                                                                                            <input type="text" class="form-control assign_class" id="post_code" name="post_code" value="@if($form) {{$form['post_code']}}@else {{$companyIfo->user_post_code}} @endif" placeholder="{!! t('payments-postcode-placeholder')!!}" data-id = "payments-postcode-placeholder" data-value = "{{t('payments-postcode-placeholder')}}" data-inputplaceholder="yes">
                                                                                                            <small id="postCodeValidation" class="assign_class d-none validation_text" style="color: red" data-id = "payments-postcode-validation" data-value = "{{t('payments-postcode-validation')}}" >{!! t('payments-postcode-validation')!!}</small>
                                                                                                        </div>


                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>

                                                                                            {{--<div class="form-group">--}}
                                                                                                {{--<label for="Country" class="assign_class" data-id = "payments-country-label" data-value = "{{t('payments-country-label')}}" >{!! t('payments-country-label')!!}</label>--}}
                                                                                                {{--<input type="text" class="form-control assign_class" id="country" name="country" value="@if($form) {{$form['country']}} @endif" placeholder="{!! t('payments-country-placeholder')!!}" data-id = "payments-country-placeholder" data-value = "{{t('payments-country-placeholder')}}" data-inputplaceholder="yes">--}}

                                                                                                {{--<select tabindex="5" class="form-control chzn-select assign_class" id="country"   name="country" >--}}
                                                                                                    {{--<option value="" selected>Select Country</option>--}}
                                                                                                    {{--<option value="GB" @if($form) {{ "GB" == $form['country'] ? 'selected="selected"' : '' }} @endif >UK</option>--}}
                                                                                                    {{--<option value="FR" @if($form) {{ "FR" == $form['country'] ? 'selected="selected"' : '' }} @endif >France</option>--}}

                                                                                                {{--</select>--}}
                                                                                                {{--<small id="countryValidation" class="assign_class d-none validation_text" style="color: red" data-id = "payments-country-validation" data-value = "{{t('payments-country-validation')}}" >{!! t('payments-country-validation')!!}</small>--}}
                                                                                            {{--</div>--}}
                                                                                            {{--<div class="form-group d-none" id="phone_number_form">--}}
                                                                                                {{--<label for="exampleInputEmail1" class="assign_class" data-id = "payments-phon-number-label" data-value = "{{ t('payments-phon-number-label')}}" >{!! t('payments-phon-number-label')!!}</label>--}}
                                                                                                {{--<input type="text" class="form-control assign_class" id="phone_number" name="phone_number" value="@if($form) {{$form['phone_number']}} @endif" placeholder="{!! t('payments-phon-number-placeholder')!!}" data-id = "payments-phon-number-placeholder" data-value = "{{t('payments-phon-number-placeholder')}}" data-inputplaceholder="yes">--}}
                                                                                                {{--<small id="phoneNumberValidation" class="assign_class d-none validation_text" style="color: red" data-id = "payments-phon-number-validation" data-value = "{{t('payments-phon-number-validation')}}" >{!! t('payments-phon-number-validation')!!}</small>--}}
                                                                                            {{--</div>--}}

                                                                                            {{--<div class="checkbox" id="sms_ask">--}}

                                                                                                {{--<label>--}}
                                                                                                    {{--<input type="checkbox" class="hold_locker" name="is_sms_active" id="is_sms_active" >--}}
                                                                                                    {{--<span class="cr"><i class="cr-icon fa fa-check"></i></span>--}}
                                                                                                    {{--<span class="assign_class" data-id = "payment-recieve-confirmation-sms-checkbox-text" data-value = "{{t('payment-recieve-confirmation-sms-checkbox-text')}}"> {!! t('payment-recieve-confirmation-sms-checkbox-text')!!}</span><br>--}}

                                                                                                {{--</label>--}}

                                                                                            {{--</div>--}}
                                                                                    </div>
                                                                        </div>
                                                                </div>

                                                                <br>
                                                                {{--<div class="assign_class card-header" style="margin: 0px" data-id = "payments-charity-card-main-title" data-value = "{{t('payments-charity-card-main-title')}}" >{!! t('payments-charity-card-main-title')!!}</div>--}}
                                                                <br>


                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        {{--{{dd($payment->amount)}}--}}
                                                                        @if($payment->amount == $payment->account_balance )
                                                                            <div class="assign_class card-header" style="margin: 0px" data-id = "payments-edit-no-price-change-mode" data-value = "{{t('payments-edit-no-price-change-mode')}}" >{!! t('payments-edit-no-price-change-mode')!!}</div>

                                                                        @elseif($payment->amount > $payment->account_balance)
                                                                            <div> <span class="assign_class" id="login_msg" data-id = "payments-edit-add-another-transaction" data-placeholders="{{$add_another_transaction_placeholders}}" data-placeholderval="{{$currency->symbol}},{{$payment->amount}},{{$payment->account_balance}}" data-value = "{{t('payments-edit-add-another-transaction')}}"  >{!!  str_replace(explode(',', $add_another_transaction_placeholders),[$currency->symbol,$payment->amount,$payment->account_balance],t('payments-edit-add-another-transaction'))  !!}</span></div>
                                                                            <br>
                                                                            <a href=" {{url('admin/refundPaymentList/' .  $booking_id . '/list') }}" class="btn btn-warning pull-left button-style text-center" >Transaction Log</a>
                                                                            {{--<div class="assign_class card-header" style="margin: 0px" data-id = "payments-edit-add-another-transaction" data-value = "{{t('payments-edit-add-another-transaction')}}" >{!! t('payments-edit-add-another-transaction')!!}</div>--}}
                                                                        @endif

                                                                    </div>
                                                                </div>

                                                            </div>

                                                            <div class="col-md-4">
                                                                <div class="card" id="timer-card" style="color: white;border-radius: 2%">
                                                                    <div class="card-body">
                                                                        <div class="row">
                                                                            <div class="col-12 float-left">
                                                                                <div class="current-date assign_class" style="font-size: 25px;padding: 2px 7px" data-id = "choose-locker-timer-text" data-value = "{{t('choose-locker-timer-text')}}" >{!! t('choose-locker-timer-text')!!}</div>
                                                                            </div>
                                                                            <div class="col-12">
                                                                                <span class="time float-right assign_class" style="font-size: 45px;padding: 2px 7px" id="timer" data-id = "choose-locker-minutes" data-placeholders="%minutes%"  data-value = "{{t('choose-locker-minutes')}}" >{!! t('choose-locker-minutes') !!}</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <br>
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <h3 class="assign_class" data-id = "payments-booking-info-company-title" data-value = "{{t('payments-booking-info-company-title')}}" >{!! t('payments-booking-info-company-title')!!}</h3>
                                                                        <div><span class="company-info assign_class" data-id = "payments-booking-info-name" data-value = "{{t('payments-booking-info-name')}}" >{!! t('payments-booking-info-name')!!} </span>{{$companyIfo->company_name}}</div>
                                                                        <p class="m-t-10"><span class="company-info assign_class" data-id = "payments-booking-info-address" data-value = "{{t('payments-booking-info-address')}}" >{!! t('payments-booking-info-address')!!} </span>{{$companyIfo->company_street}} {{$companyIfo->company_city}} {{$companyIfo->company_country}}</p>
                                                                    </div>
                                                                </div>

                                                                <div class="card m-t-10">
                                                                    <div class="card-body">

                                                                        <h3 class="assign_class" data-id = "payments-booking-info-booked-title" data-value = "{{t('payments-booking-info-booked-title')}}" >{!! t('payments-booking-info-booked-title')!!}</h3>

                                                                        <div><span class="company-info assign_class" data-id = "payments-user-info-reservation-id" data-value = "{{t('payments-user-info-reservation-id')}}" >{!! t('payments-user-info-reservation-id')!!} </span>{{$companyIfo->booking_id}}</div>


                                                                        @if($currency->position == 'b')
                                                                            <p class="m-t-10"><span class="company-info assign_class" data-id = "payments-user-info-cost" data-value = "{{t('payments-user-info-cost')}}" >{!! t('payments-user-info-cost')!!}</span>{{$currency->symbol}}{{$total_price}}</p>

                                                                        @else
                                                                            <p class="m-t-10 m-b-0"><span class="company-info assign_class" data-id = "payments-user-info-cost" data-value = "{{t('payments-user-info-cost')}}" >{!! t('payments-user-info-cost')!!}  </span> {{$currency->symbol}}{{$total_price}}</p>
                                                                        @endif

                                                                        @foreach($lockerInfo as $locker)

                                                                            <div ><span class="company-info assign_class" data-id = "payments-booking-info-child-name" data-value = "{{t('payments-booking-info-child-name')}}" >{!! t('payments-booking-info-child-name')!!}  </span> {{$locker->childName}}</div>
                                                                            @if($locker->child_email)
                                                                                <div class="m-t-10"><span class="company-info assign_class" data-id = "payments-booking-info-child-email" data-value = "{{t('payments-booking-info-child-email')}}" >{!! t('payments-booking-info-child-email')!!} </span> {{$locker->child_email}}</div>
                                                                            @endif
                                                                            <div  class="m-t-10"><span class="company-info assign_class" data-id = "payments-booking-info-start-rent" data-value = "{{t('payments-booking-info-start-rent')}}" >{!! t('payments-booking-info-start-rent')!!} </span>@if($companyIfo->location_group_id == 1 || $companyIfo->location_group_id == 2){{date('d M, Y', strtotime($locker->start))}}@else{{date('d M, Y H:i', strtotime($locker->start))}}@endif</div>
                                                                            <div class="m-t-10"><span class="company-info assign_class" data-id = "payments-booking-info-end-rent" data-value = "{{t('payments-booking-info-end-rent')}}" >{!! t('payments-booking-info-end-rent')!!} </span>@if($companyIfo->location_group_id == 1 || $companyIfo->location_group_id == 2){{date('d M, Y', strtotime($locker->end))}}@else{{date('d M, Y H:i', strtotime($locker->end))}}@endif</div>
                                                                            <div class="m-t-10"><span class="company-info assign_class" data-id = "payments-booking-info-locker-block" data-value = "{{t('payments-booking-info-locker-block')}}" >{!! t('payments-booking-info-locker-block')!!} </span>{{$locker->block_name}}</div>
                                                                            <div class="m-t-10"><span class="company-info assign_class" data-id = "payments-booking-info-locker-number" data-value = "{{t('payments-booking-info-locker-number')}}" >{!! t('payments-booking-info-locker-number')!!} </span>{{$locker->locker_number}}</div>

                                                                            @if($currency->position == 'b')
                                                                                <p class="m-t-10"><span class="company-info assign_class" data-id = "payments-booking-info-locker-price" data-value = "{{t('payments-booking-info-locker-price')}}" >{!! t('payments-booking-info-locker-price')!!} </span>{{$currency->symbol}}{{$locker->price}}</p>

                                                                            @else
                                                                                <p class="m-t-10"><span class="company-info assign_class" data-id = "payments-booking-info-locker-price" data-value = "{{t('payments-booking-info-locker-price')}}" >{!! t('payments-booking-info-locker-price')!!} </span>{{$currency->symbol}}{{$locker->price}}</p>
                                                                            @endif

                                                                        @endforeach

                                                                    </div>
                                                                </div>

                                                                <div class="card m-t-10">
                                                                    <div class="card-body">

                                                                        <p><span class="company-info assign_class" data-id = "payments-booking-info-terms-msg" data-value = "{{t('payments-booking-info-terms-msg')}}" >{!! t('payments-booking-info-terms-msg')!!}  </span> <i class="fa fa-check icon-tick" style="color: #F5B54A;font-size: x-large;margin-left: 5px" ></i ></p>
                                                                    </div>
                                                                </div>

                                                                <div class="card m-t-10">
                                                                    <div class="card-body">

                                                                        <h3 style="margin-top: 15px" class="assign_class" data-id = "payments-booking-info-user-main-title" data-value = "{{t('payments-booking-info-user-main-title')}}" >{!! t('payments-booking-info-user-main-title')!!}</h3>
                                                                        <div class="m-t-10"><span class="company-info assign_class" data-id = "payments-user-info-name" data-value = "{{t('payments-user-info-name')}}" >{!! t('payments-user-info-name')!!} </span>@if($companyIfo->name || $companyIfo->surname){{$companyIfo->name}} {{$companyIfo->surname}} @else {{$companyIfo->username }} @endif</div>
                                                                        <div class="m-t-10"><span class="company-info assign_class" data-id = "payments-user-info-email" data-value = "{{t('payments-user-info-email')}}" >{!! t('payments-user-info-email')!!} </span>{{$companyIfo->email}}</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                @csrf
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <br>
                <div class="row" style="margin: auto; justify-content: center;">
                    <div class="text-center payments-opacity"><a href="{{route('payment.index')}}" class="green-button button_text assign_class" data-id = "edit-booking-orders-back-button" data-value = "{{t('edit-booking-orders-back-button')}}" style="padding: 9px 10px;">{!! t('edit-booking-orders-back-button')!!}</a></div>
                    {{--<div class="text-center payments-opacity" style="margin-left: 12px"><a class="green-button button_text assign_class confirm-purchase" id="confirm-purchase" data-id = "payments-confirm-button" data-value = "{{t('payments-confirm-button')}}" style="padding: 10px 20px;"><span style="color: #fff">{!! t('payments-confirm-button')!!}</span></a></div>--}}
                    {{--<div class="text-center" style="margin-left: 12px"><a class="green-button button_text assign_class d-none" id="refresh-page" data-id = "payments-refresh-page-button" data-value = "{{t('payments-refresh-page-button')}}" style="padding: 10px 20px;"><span style="color: #fff">{!! t('payments-refresh-page-button')!!}</span></a></div>--}}
                    `
                <!--   <button type="submit" class="btn btn-default assign_class confirm-purchase" style="background-color: #F5B54A" id="confirm-purchase" data-id = "payments-confirm-button" data-value = "{{t('payments-confirm-button')}}" >{!! t('payments-confirm-button')!!}</button> -->
                </div>
                <div class="form-group" id="pay-now"></div>
            </div>
        </section><!-- #portfolio -->
        <div class="d-none" id="3d_form"></div>

    </main>


@endsection

@section('script')


    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>

    <script src="/frontend/js/custom.js"></script>

    <script type="text/javascript" src="/frontend/vendors/Buttons/js/buttons.js"></script>

    <script src="/frontend/js/jquery.payform.min.js" charset="utf-8"></script>



    <script type="text/javascript" src="/frontend/js/pages/radio_checkbox.js"></script>

    <script src="/frontend/vendors/swiper/js/swiper.min.js"></script>

    <script src="/frontend/js/pages/cards.js"></script>


    <script type="text/javascript" src="/backend/vendors/chosen/js/chosen.jquery.js"></script>
    <script type="text/javascript" src="/frontend/js/pages/form_elements.js"></script>


    <script>

        $(function() {

            var owner = $('#owner');
            var cardNumber = $('#cardNumber');
            var cardNumberField = $('#card-number-field');
            var CVV = $("#cvv");
            var mastercard = $("#mastercard");
            var confirmButton = $('#confirm-purchase');
            var visa = $("#visa");
            var amex = $("#amex");

            // Use the payform library to format and validate
            // the payment fields.

            cardNumber.payform('formatCardNumber');
            CVV.payform('formatCardCVC');


            cardNumber.keyup(function() {

                amex.removeClass('transparent');
                visa.removeClass('transparent');
                mastercard.removeClass('transparent');

                if ($.payform.validateCardNumber(cardNumber.val()) == false) {
                    cardNumberField.addClass('has-error');
                } else {
                    cardNumberField.removeClass('has-error');
                    cardNumberField.addClass('has-success');
                }

                if ($.payform.parseCardType(cardNumber.val()) == 'visa') {
                    mastercard.addClass('transparent');
                    amex.addClass('transparent');
                } else if ($.payform.parseCardType(cardNumber.val()) == 'amex') {
                    mastercard.addClass('transparent');
                    visa.addClass('transparent');
                } else if ($.payform.parseCardType(cardNumber.val()) == 'mastercard') {
                    amex.addClass('transparent');
                    visa.addClass('transparent');
                }
            });

            $('body').on('click','.confirm-purchase',function(e) {
                if ($('#edit_cms_switch').is(":checked")) {

                    // alert('checked');

                    // $('.confirm-purchase').removeAttribute('id');

                    // $('#confirm-purchase').addClass("get_text");
                    // return false;

                } else {

                    e.preventDefault();
                    payment_validation();

                    // $('.confirm-purchase').addAttribute('id');

                    // $('#confirm-purchase').prop("disabled", false);
                    // $('.get_text').prop("disabled", true);
                    //
                    // $('#confirm-purchase').removeClass("get_text");

                }
            });


            function payment_validation() {

                $('#confirm-purchase').prop('disabled', true);


                $('.validation_text').addClass('d-none');
                var billing_address1 = $.trim($('#billing_address1').val());

                var city = $.trim($('#city').val());
                var state = $.trim($('#state').val());
                var post_code = $.trim($('#post_code').val());
                var country = $.trim($('#country').val());
                var firstname = $.trim($('#firstname').val());
                var surname = $.trim($('#payer_surname').val());

                var isCardValid = $.payform.validateCardNumber(cardNumber.val());
                var isCvvValid = $.payform.validateCardCVC(CVV.val());


                if (firstname.length == 0 || surname.length == 0 ||billing_address1.length == 0 ||  city == false || state.length == 0 || post_code.length == 0 || country.length == 0  || isCardValid == false || isCvvValid == false || owner.val().length < 5) {


                    if (firstname.length == 0) {

                        $('#firstnameValidation').removeClass('d-none');
                        $('#firstname').focus();

                    }
                    if (surname.length == 0) {

                        $('#surnameValidation').removeClass('d-none');
                        $('#surname').focus();

                    }
                    if (billing_address1.length == 0) {

                        $('#billingAddress1Validation').removeClass('d-none');
                        $('#billing_address1').focus();

                    }

                    if (city.length == 0) {

                        $('#cityValidation').removeClass('d-none');
                        $('#city').focus();

                    }

                    if (state.length == 0) {

                        $('#stateValidation').removeClass('d-none');
                        $('#state').focus();

                    }


                    if (post_code.length == 0) {

                        $('#postCodeValidation').removeClass('d-none');
                        $('#post_code').focus();

                    }
                    if (country.length == 0) {

                        $('#countryValidation').removeClass('d-none');
                        $('#country').focus();

                    }



                    if (owner.val().length < 5) {
                        $('#ownerValidation').removeClass('d-none');
                        $('#owner').focus();
                        // $('#ownerValidation').focus();
                        // alert("Wrong owner name");
                    }
                    if (!isCardValid) {
                        $('#cardNumberValidation').removeClass('d-none');
                        $('#cardNumber').focus();
                        // alert("Wrong card number");
                    }
                    if (!isCvvValid) {
                        $('#cvvValidation').removeClass('d-none');
                        $('#cvv').focus();

                    }
                    $('#confirm-purchase').prop('disabled', false);

                    return false;
                } else {
                    // $('#paymentForm').submit();
                    $(".loader").show();
                    var change = $("#address-radio-change").is(':checked');
                    var diff = $("#address-radio-diff").is(':checked');
                    $('#refresh-page').removeClass('d-none');

                    if(change || diff){
                        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                        $.ajax({

                            url: '/user/saveUserAddress',

                            type: 'post',

                            dataType: 'JSON',

                            cache: false,

                            data: {
                                firstname:  $('#firstname').val(),
                                payer_surname:  $('#payer_surname').val(),
                                address_1:  $('#billing_address1').val(),
                                address_2 : $('#billing_address2').val(),
                                city : $('#city').val(),
                                state : $('#state').val(),
                                post_code : $('#post_code').val(),
                                country : $('#country').val(),

                                _token: CSRF_TOKEN
                            },

                            success: function (data) {

                            },

                            error: function (jqXHR, textStatus, errorThrown) {



                            }

                        });
                    }


                    jQuery('.payments-opacity').css('opacity', '0.1');
                    // jQuery('#refresh-page').css('opacity', '1');


                    var form_data = $('#paymentForm').serialize();

                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                    $.ajax({

                        url: '/frontend/savepayments',

                        type: 'post',
                        data : form_data,
                        // dataType: 'JSON',
                        cache: false,
                        // data:  form_data,
                        success: function (data) {

                            $('#refresh-page').addClass('d-none');
                            if(data.status == 'redirect'){

                                window.location = data.url;
                            }else if(data.status == 'errors'){

                                var error_html = '';
                                error_html +='<ul>';
                                $.each(data.error_msgs ,function (index ,error) {
                                    error_html +='<li>'+error+'</li>';
                                });
                                error_html += '</ul>';
                                $('#error_msg').removeClass('d-none');
                                $('#error_msg').html(error_html);
                                $('#confirm-purchase').prop('disabled', false);
                                $(".loader").hide();
                                jQuery('.payments-opacity').css('opacity', '1');
                            }else if(data.status == 'confirmation'){
                                $('#main').html(data.view);
                                $(".loader").hide();
                                jQuery('.payments-opacity').css('opacity', '1');

                                window.scrollTo(0, 0);
                                // debugger;coord
                                // setTimeout(
                                //     function()
                                //     {
                                //         $('.after-title-step5:after').css('background', '#9ABDB7');
                                //     }, 2000);

                            }else if(data.status == '3D'){
                                $('#3d_form').html(data.view);
                                $('#pa-form').submit();
                                $(".loader").hide();
                                jQuery('.payments-opacity').css('opacity', '1');
                            }else if(data.status == 'wait'){
                                $('#confirm-purchase').prop('disabled', false);
                                $(".loader").hide();
                                jQuery('.payments-opacity').css('opacity', '1');
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $(".loader").hide();
                            jQuery('.payments-opacity').css('opacity', '1');

                        },
                        // complete: function (xhr, status) {
                        //     if (status === 'error' || !xhr.responseText) {
                        //         console.log(status);
                        //     }
                        //     else {
                        //         var data = xhr.responseText;
                        //         console.log(data);
                        //     }
                        // }
                    });


                }
            }

        });

        $('document').ready(function(){
            $.ajax({

                url: '/user/checkCredits',

                type: 'get',

                dataType: 'JSON',

                cache: false,

                success: function (data) {

                    var a = false;
                    if(!data){
                        $('#sms_ask').addClass('d-none');
                    }
                },

                error: function (jqXHR, textStatus, errorThrown) {



                }

            });
        })




        // $("#confirm-purchase").click(function() {
        //
        //     var change = $("#address-radio-change").is(':checked');
        //     var diff = $("#address-radio-diff").is(':checked');
        //      var form_data = $('#paymentForm').serialize();
        //      console.log(form_data);
        //     // if(change || diff){
        //     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        //      $.ajax({
        //
        //         url: '/user/saveUserAddress',
        //
        //         type: 'post',
        //         data : form_data,
        //         dataType: 'JSON',
        //
        //         cache: false,
        //
        //         data: {
        //             address_1:  $('#billing_address1').val(),
        //             address_2 : $('#billing_address2').val(),
        //             city : $('#city').val(),
        //             state : $('#state').val(),
        //             post_code : $('#post_code').val(),
        //             country : $('#country').val(),
        //             _token: CSRF_TOKEN
        //         },
        //
        //         success: function (data) {
        //             console.log(data);
        //         },
        //
        //         error: function (jqXHR, textStatus, errorThrown) {
        //
        //             console.log(jqXHR);
        //
        //         }
        //
        //     });
        //
        // });
        // $("#confirm-purchase").click(function() {
        //
        //     var change = $("#address-radio-change").is(':checked');
        //     var diff = $("#address-radio-diff").is(':checked');
        //
        //     if(change || diff){
        //     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        //      $.ajax({
        //
        //         url: '/user/saveUserAddress',
        //
        //         type: 'post',
        //
        //         dataType: 'JSON',
        //
        //         cache: false,
        //
        //         data: {
        //             address_1:  $('#billing_address1').val(),
        //             address_2 : $('#billing_address2').val(),
        //             city : $('#city').val(),
        //             state : $('#state').val(),
        //             post_code : $('#post_code').val(),
        //             country : $('#country').val(),
        //             _token: CSRF_TOKEN
        //         },
        //
        //         success: function (data) {
        //             console.log(data);
        //         },
        //
        //         error: function (jqXHR, textStatus, errorThrown) {
        //
        //             console.log(jqXHR);
        //
        //         }
        //
        //     });
        // }
        // });

        $('body').on('click','#refresh-page',function(e){

            location.reload(true);

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({

                url: '/frontend/refreshpayments',

                type: 'post',

                dataType: 'JSON',

                cache: false,

                data: { _token: CSRF_TOKEN},

                success: function (data) {



                },

                error: function (jqXHR, textStatus, errorThrown) {



                }

            });


        });

        $('body').on('click','#address-radio-default',function(e){

            $('#use-my-address-text').removeClass('d-none');
            $('#use-my-address-inputs').addClass('d-none');
            // $("input[name='address-radio-default']").val(1);
            // $("input[name='address-radio-change']").val('');
            // $("input[name='address-radio-diff']").val('');
            var checked = $(this).is(':checked');
            if (checked) {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                $.ajax({

                    url: '/user/getSaveAddress',

                    type: 'get',

                    dataType: 'JSON',

                    cache: false,

                    success: function (data) {



                        $('#billing_address1').val(data.address_1);
                        $('#billing_address2').val(data.address_2);
                        $('#city').val(data.city);
                        $('#country').val(data.country);
                        $('#state').val(data.state);
                        $('#post_code').val(data.post_code);

                    },

                    error: function (jqXHR, textStatus, errorThrown) {



                    }

                });
            } else {
                $("#phone_number_form").addClass('d-none');
            }
        });

        $('body').on('click','#address-radio-change',function(e){
            // $("input[name='address-radio-change']").val(1);
            // $("input[name='address-radio-default']").val('');
            // $("input[name='address-radio-diff']").val('');
            $('#use-my-address-text').addClass('d-none');
            $('#use-my-address-inputs').removeClass('d-none');

            var checked = $(this).is(':checked');
            if (checked) {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                $.ajax({

                    url: '/user/getSaveAddress',

                    type: 'get',

                    dataType: 'JSON',

                    cache: false,

                    success: function (data) {



                        $('#billing_address1').val(data.address_1);
                        $('#billing_address2').val(data.address_2);
                        $('#city').val(data.city);
                        $('#country').val(data.country);
                        $('#state').val(data.state);
                        $('#post_code').val(data.post_code);

                    },

                    error: function (jqXHR, textStatus, errorThrown) {



                    }

                });
            } else {
                $("#phone_number_form").addClass('d-none');
            }
        });

        $('#address-radio-diff').click(function() {
            // $("input[name='address-radio-diff']").val(1);
            // $("input[name='address-radio-default']").val('');
            // $("input[name='address-radio-change']").val('');
            $('#use-my-address-text').addClass('d-none');
            $('#use-my-address-inputs').removeClass('d-none');
            $('#billing_address1').val('');
            $('#billing_address2').val('');
            $('#city').val('');
            $('#state').val('');
            $('#post_code').val('');
            $('#country').val('');
        });

        $("#is_sms_active").click(function() {
            var checked = $(this).is(':checked');
            if (checked) {
                $("#phone_number_form").removeClass('d-none');
            } else {
                $("#phone_number_form").addClass('d-none');
            }
        });

        $('.radio-charity').click(function() {

            $('#charity-input-custom').val('');
            $('#charity-text-custom').addClass('d-none');




            if($('#charity-radio-custom').is(':checked')) {
                $('#charity-text-custom').removeClass('d-none')

            }

            if($('#charity-radio-default').is(':checked')){

                $('#charity-input-custom').val(1)

            }

            if ($('#charity-input-custom').val() !== '') {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                $.ajax({

                    url: '/charitywork',

                    type: 'post',

                    dataType: 'JSON',

                    cache: false,

                    data: {price:  $('#charity-input-custom').val(), lockerInfo : <?php echo count($lockerInfo); ?>,total_price : <?php echo $total_price;?>, _token: CSRF_TOKEN},

                    success: function (data) {

                        {{--var values ='<span class="assign_class" id="login_msg" data-id = "payments-charity-card-main-disclaimer" data-placeholders="{{$charity_declaimer_placeholder}}" data-placeholderval="{{count($lockerInfo)}},{{$total_price}},'+data+',logout" data-value = "{{t('payments-charity-card-main-disclaimer')}}"  >'+data+'</span>';--}}
                        {{--$('#div_for_charity').html(values);--}}
                        {{--console.log(values)--}}

                    },

                    error: function (jqXHR, textStatus, errorThrown) {



                    }

                });
            }

        });

        var count = $('#timeDifference').val();

        var counter = setInterval(timer, 1000); //1000 will  run it every 1 second

        function timer() {
            count = count - 1;
            if (count == -1) {

                clearInterval(counter);

                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                var sale_id = $('#sale_id').val();

                $.ajax({

                    url: '/frontend/expireBooking',

                    type: 'POST',

                    dataType: 'JSON',

                    cache: false,

                    data: {sale_id: sale_id, _token: CSRF_TOKEN},

                    success: function (data) {
                        // console.log(data);

                        document.location.href = "/";

                    },

                    error: function (jqXHR, textStatus, errorThrown) {



                    }

                });

                return;
            }

            var seconds = count % 60;
            var minutes = Math.floor(count / 60);

            // var hours = Math.floor(minutes / 60);
            minutes %= 60;

            // hours %= 60;
            if(minutes >= 10 ){

                $("#timer-card").css("background-color", "#99BDBC");
            }
            if(minutes < 10){
                $("#timer-card").css("background-color", "#F6B929");
            }
            if(minutes < 5){
                $("#timer-card").css("background-color", "#AA0911");
            }
            if(seconds < 10){
                seconds =  ('0' + seconds).slice(-2);
            }
            if(minutes < 10){
                minutes =  ('0' + minutes).slice(-2);
            }

            var placeholder = $('#timer').data('placeholders');
            // console.log(placeholder,'placeholder');
            var data = $('#timer').data('value');
            // console.log(data,'data');
            var time = minutes +":"+ seconds;
            // console.log(time,'time');
            replaced_data_value = data.replace(placeholder, time);
            // console.log(replaced_data_value,'replaced_data_value');

            document.getElementById("timer").innerHTML =  replaced_data_value; // watch for spelling

            $('#timer').data('placeholderval',time);
        }


    </script>

    <script type="text/javascript">

        var arrowPage = "{{Session::get('arrowsPage')}}";

        if(arrowPage == 4 ) {

            $("#left_arrow").removeClass('d-none');

        }else if(arrowPage > 4){

            $("#left_arrow").removeClass('d-none');
            $("#right_arrow").removeClass('d-none');
        }

        $('body').on('click','#left_arrow',function(e){

            window.location.href = '{{ route('edit.saveSchoolBookingBack') }}';

        });



        $(".styled-radio").on('click', function(event){
            // alert('hi');
            // return false;
            // $(this).click();
        });

        // $("#lookup_dropdown").on('click', function(event){
        //     $('#lookup_dropdown').addClass('show');
        //     $('.dropdown-menu').addClass('show');
        //     // $('#about-us1').attr('aria-haspopup',true);
        //     $('#about-us1').attr('aria-expanded',true);
        //     // alert('hi');
        //     // return false;
        //     // $(this).click();
        // });
        $('body').on('click','.reset-booking',function(e){

            // var  start = $('input('start_date').val();

            var sale_id = $(this).data('sale_id');

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({

                url: '/frontend/resetSale',

                type: 'POST',

                dataType: 'JSON',

                cache: false,

                data: {sale_id: sale_id, _token: CSRF_TOKEN},

                success: function (data) {

                    document.location.href="/";

                },

                error: function (jqXHR, textStatus, errorThrown) {


                }

            });


        });

        $('body').on('click','#post_code_search',function(e){

            // var  start = $('input('start_date').val();
            $('#postcodeLookupValidation').addClass('d-none');

            var post_code = $('#post_code_lookup').val();


            // alert(post_code);
            // alert(locker_id);


            // var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({

                url: '/frontend/postCodeSearch',

                type: 'POST',

                dataType: 'JSON',

                cache: false,

                data: {post_code: post_code},

                success: function (data) {

                    var obj = JSON.parse(data);
                    if(obj.status = 'match'){
                        // if(obj.match_type =='post_code_area'){
                        //   console.log(obj.match_type,'obj.match_type')
                        // }
                        // if(obj.match_type =='post_code_district'){
                        //   console.log(obj.match_type,'post_code_district')
                        // }
                        // if(obj.match_type =='post_code_sector'){
                        //   console.log(obj.match_type,'post_code_sector')
                        // }
                        if(obj.match_type =='unit_postcode'){
                            // console.log(obj.match_type,'unit_postcode');
                            // console.log(obj.data.country,'country');
                            $('#manual_address_fields').removeClass('d-none');
                            $('#city').val(obj.data.postcode_sector);
                            $('#state').val(obj.data.postcode_district);
                            $('#post_code').val(obj.data.postcode);
                            $('#country').val(obj.data.country);
                        }else{
                            $('#postcodeLookupValidation').removeClass('d-none');
                            $('#manual_address_fields').addClass('d-none');
                        }
                    }else{

                        $('#postcodeLookupValidation').removeClass('d-none');
                        $('#manual_address_fields').addClass('d-none');
                        // console.log(obj.error,'error')

                    }


                    // console.log(obj.status);

                },

                error: function (jqXHR, textStatus, errorThrown) {



                }

            });


        });

        $("#add_address_manually").on('click', function(event){

            event.preventDefault();
            $('.postcode-lookup').addClass('d-none');
            $('#manual_address_fields').removeClass('d-none');
            $('#lookup_dropdown').removeClass('d-none');
            // $("#paymentForm")[0].reset();
            // $('#paymentForm').reset();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({

                url: '/frontend/manualPostCodeSession',

                type: 'POST',

                dataType: 'JSON',

                cache: false,

                success: function (data) {

                },

                error: function (jqXHR, textStatus, errorThrown) {



                }

            });

        });

    </script>

@endsection