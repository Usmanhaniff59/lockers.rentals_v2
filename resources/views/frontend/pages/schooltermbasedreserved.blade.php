
@extends('frontend.layouts.app')



@section('style')
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">--}}

    {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>--}}

    {{--<link type="text/css" rel="stylesheet" href="/frontend/css/components.css" />--}}
    {{--<link type="text/css" rel="stylesheet" href="/frontend/css/custom.css" />--}}

    {{--<script src="https://co
    de.jquery.com/jquery-3.4.1.js" crossorigin="anonymous"></script>--}}

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>

    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.0.0/flatly/bootstrap.min.css">--}}

    <link rel="stylesheet" href="/css/bootstrap-datetimepicker.css">
    <link type="text/css" rel="stylesheet" href="/frontend/vendors/swiper/css/swiper.min.css" />
    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="/frontend/vendors/chosen/css/chosen.css"/>
    <link type="text/css" rel="stylesheet" href="/frontend/css/pages/form_elements.css"/>


    <link type="text/css" rel="stylesheet" href="/frontend/css/pages/general_components.css" />

    <link type="text/css" rel="stylesheet" href="/frontend/vendors/checkbox_css/css/checkbox.min.css" />

    <link type="text/css" rel="stylesheet" href="/frontend/css/pages/radio_checkbox.css" />
    {{--<link type="text/css" rel="stylesheet" href="/frontend/vendors/modal/css/component.css"/>--}}

    <!-- end of plugin styles -->


    <link rel="stylesheet" href="/frontend/css/pages/general_components.css" />
    <link rel="stylesheet" href="/frontend/vendors/swiper/css/swiper.min.css" />

    <style>

        .after-title-step2::after {
            background: #9ABDB7;
        }

        .bg-primary {
            background-color: #00c0ef!important;
        }
        .btn-primary {
            color: #fff;
            background-color: #00c0ef;
            border-color: #00c0ef;
        }
        .btn-primary:hover {
            color: #fff;
            background-color: #00a1c9;
            border-color: #0097bc;
        }
        .modal-header {
            display: flex;
            flex-direction: row ;
        }

    </style>

@endsection

@section('content')

    <?php

    use App\SchoolYear;

    if(\Illuminate\Support\Facades\Auth::check() == true){

        $user_name = \Illuminate\Support\Facades\Auth::user()->name;
        $user_surname = \Illuminate\Support\Facades\Auth::user()->surname;
        $user_email = \Illuminate\Support\Facades\Auth::user()->email;

        $user_id = \Illuminate\Support\Facades\Auth::user()->id;

    }else{

        $user_name = 0;
        $user_surname = 0;
        $user_email = 0;
        $user_id = 0;

    }


    ?>

    <main id="main">
        {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">--}}


        <section id="portfolio"  class="section-bg" >

            @if(Session::has('flash_message'))

                <div class="col-md-12">

                    <div class="alert alert-success alert-dismissable float-right" style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">

                        <button type="button" class="close" data-dismiss="alert"

                                aria-hidden="true" style="margin-top: 4px; margin-left: 15px;">

                        </button>

                        {{ Session::get('flash_message') }}

                    </div>

                </div>

            @endif

            <form id="SchoolBookingForm" action="{{route('saveSchoolBooking')}}" method="post">
                <input type="hidden" name="_token" class="csrf-token"  value="{{ Session::token() }}" />
                <div class="container">
                    <input type="hidden" name="user_id" class="form-control" id="user_id" value="{{ $user_id }}"/>
                    <input type="hidden" name="userName" class="form-control" id="user_name" value="{{ $user_name }}"/>
                    <input type="hidden" name="userSurname" class="form-control" id="user_surname" value="{{ $user_surname }}"/>
                    <input type="hidden" name="userEmail" class="form-control" id="user_email" value="{{ $user_email }}"/>
                    <input type="hidden"  class="form-control" id="form_submittion" value="false"/>

                    <input type='hidden' class="form-control" name="location_group" id="location_group" value="{{$school->location_group_id}}" />

                    <input type='hidden' class="form-control" name="company_id" value="{{$school->id}}" />

                    <input type='hidden' class="form-control" name="school_name" value="{{$school->name}}" />

                    <input type='hidden' class="form-control" name="street" value="{{$school->street}}" />

                    <input type='hidden' class="form-control" name="city" value="{{$school->city}}" />

                    <input type='hidden' class="form-control" name="post_code" value="{{$school->post_code}}" />

                    <input type='hidden' class="form-control" name="country" value="{{$school->country}}" />

                    <input type='hidden' class="form-control" name="school_academic_years_count" id="school_academic_years_count" value="{{count($years)}}" />

                    <input type="hidden"  class="form-control" id="form_submittion" value="false"/>




                    <header class="section-header">
                        {{--<h3 class="section-title">Type of Booking for '{{$school->name}}'</h3>--}}
                        @if($school->location_group_id == 1)
                            <h3 class="section-title assign_class school_header" data-id = "school-term-title" data-placeholders="%type_of_booking%,%school_name%" data-placeholderval="{{$school->name}},{{$school->locationGroup->name}}" data-value = "{{t('school-term-title')}}" data-steps="yes" >{!!  str_replace(["%type_of_booking%" ,"%school_name%"],[$school->locationGroup->name,$school->name],t('school-term-title'))  !!}<div class="after-left-arrow arrows " id="left_arrow"></div><div class="after-title-step2"></div><div class="after-title-step3"></div><div class="after-title-step4"></div><div class="after-title-step5"></div><div class="after-right-arrow arrows d-none" id="right_arrow"></div></h3>
                            {{--<h3 class="section-title assign_class school_header" data-id = "school-term-title" data-placeholders="%type_of_booking%,%school_name%" data-placeholderval="{{$school->name}},{{$school->locationGroup->name}}" data-value = "{{t('school-term-title')}}" data-steps="yes" >{!!  str_replace(["%type_of_booking%" ,"%school_name%"],[$school->locationGroup->name,$school->name],t('school-term-title'))  !!}<div class="after-title-step2"></div><div class="after-title-step3"></div><div class="after-title-step4"></div><div class="after-title-step5"></div></h3>--}}
                        @elseif($school->location_group_id == 2)
                            <h3 class="section-title assign_class school_header" data-id = "school-term-title-location-group2" data-placeholders="%type_of_booking%,%school_name%" data-placeholderval="{{$school->name}},{{$school->locationGroup->name}}" data-value = "{{t('school-term-title-location-group2')}}" data-steps="yes" >{!!  str_replace(["%type_of_booking%" ,"%school_name%"],[$school->locationGroup->name,$school->name],t('school-term-title-location-group2'))  !!}<div class="after-left-arrow arrows" id="left_arrow"></div><div class="after-title-step2"></div><div class="after-title-step3"></div><div class="after-title-step4"></div><div class="after-title-step5"></div><div class="after-right-arrow arrows d-none" id="right_arrow"></div></h3>

                        @elseif($school->location_group_id == 3)
                            <h3 class="section-title assign_class school_header" data-id = "school-term-title-location-group3" data-placeholders="%type_of_booking%,%school_name%" data-placeholderval="{{$school->name}},{{$school->locationGroup->name}}" data-value = "{{t('school-term-title-location-group3')}}" data-steps="yes" >{!!  str_replace(["%type_of_booking%" ,"%school_name%"],[$school->locationGroup->name,$school->name],t('school-term-title-location-group3'))  !!}<div class="after-left-arrow arrows" id="left_arrow"></div><div class="after-title-step2"></div><div class="after-title-step3"></div><div class="after-title-step4"></div><div class="after-title-step5"></div><div class="after-right-arrow arrows d-none" id="right_arrow"></div></h3>

                        @endif

                    </header>

                    <div class="col-md-8" style="margin: auto; padding: 12px; margin-top: 40px">
                        <input type="hidden" id="child_count" name="child_count" value="{{$sales_count}}">
                        <input type='hidden' class="form-control" name="booking_id" id="booking_id" value="@if(\Illuminate\Support\Facades\Session::get('page_number')>2){{$sales[0]->booking_id}} @else 0 @endif" />
                        <div class="card"  style="margin: 7px 0px; padding: 12px">
                            <div class="card-body">
                                @if(isset($sales) && isset($sales[0]->hold_booking_date))

                                    <span class="assign_class" data-id = "booking-locker-disclaimer" data-placeholders="{{$book_lock_desc_placeholders}}" data-placeholderval="{{$school->name}}" data-value = "{{t('booking-locker-disclaimer')}}"  >{!!  str_replace(explode(',', $book_lock_desc_placeholders),[$school->name, date("F jS, Y", strtotime($sales[0]->hold_booking_date)), "", ""],t('booking-locker-disclaimer'))  !!}</span>

                                @else

                                    <span class="assign_class" data-id = "booking-locker-disclaimer" data-placeholders="{{$book_lock_desc_placeholders}}" data-placeholderval="{{$school->name}}" data-value = "{{t('booking-locker-disclaimer')}}"  >{!!  str_replace(explode(',', $book_lock_desc_placeholders),[$school->name, "", "<div class='d-none'>", "</div>"],t('booking-locker-disclaimer'))  !!}</span>

                                @endif
                            </div>
                        </div>
                        <?php  $tabindex = 1 ?>
                        @for ($i = 1; $i <= 20; $i++)

                            <div class="card d-none" id="child{{$i}}" style="margin: 7px 0px">

                                {{--<div class="card-header bg-white assign_class"  data-id = "school-term-child-detail-title" data-value = "{{t('school-term-child-detail-title')}}" >{!! t('school-term-child-detail-title')!!}</div>--}}
                                <div class="card-body">
                                    <h3 class="assign_class" data-id = "school-term-child-detail-title" data-value = "{{t('school-term-child-detail-title')}}" >{!! t('school-term-child-detail-title')!!}</h3>

                                    <h3 class="assign_class d-none" id="child_number_title{{$i}}" data-id = "school-term-child-number-title" data-value = "{{t('school-term-child-number-title')}}" >{!! t('school-term-child-number-title')!!}</h3>

                                    <div class="row">

                                        <input type='hidden' class="form-control" name="sale_id{{$i}}" id="sale_id{{$i}}" value="@if(\Illuminate\Support\Facades\Session::get('page_number')>2)@if($i <= $sales_count ){{$sales[$i-1]->id}} @endif @else 0 @endif" />
                                        <input type='hidden' class="form-control" name="block_id{{$i}}" id="block_id{{$i}}" value="@if(\Illuminate\Support\Facades\Session::get('page_number')>2)@if($i <= $sales_count ){{$sales[$i-1]->block_id}} @endif @else 0 @endif" />
                                        <input type='hidden' class="form-control" name="locker_id{{$i}}" id="locker_id{{$i}}" value="@if(\Illuminate\Support\Facades\Session::get('page_number')>2)@if($i <= $sales_count ){{$sales[$i-1]->locker_id}}@endif @else 0 @endif" />
                                        <input type='hidden' class="form-control" name="hold_booking_date{{$i}}" id="hold_booking_date{{$i}}" value="@if(\Illuminate\Support\Facades\Session::get('page_number')>2)@if($i <= $sales_count ){{$sales[$i-1]->hold_booking_date}} @endif @endif" />
                                        <input type='hidden' class="form-control"  id="school_year_id{{$i}}" value="@if(\Illuminate\Support\Facades\Session::get('page_number')>2)@if($i <= $sales_count ){{$sales[$i-1]->school_year_id}}@endif @else @endif" />

                                        <div class="col-6">

                                            <div class="form-group">
                                                <input tabindex="{{$tabindex}}" type='text' class="form-control assign_class" id='child_first_name{{$i}}' name="child_first_name{{$i}}" value="@if(\Illuminate\Support\Facades\Session::get('page_number')>2)@if($i <= $sales_count )@if($sales[$i-1]->child_first_name){{$sales[$i-1]->child_first_name}}@endif @endif @endif"  placeholder="{!! t('school-term-child-first-name-placeholder')!!}"   data-id = "school-term-child-first-name-placeholder" data-value = "{{t('school-term-child-first-name-placeholder')}}" data-inputplaceholder="yes"  required/>
                                                <p id="child_first_nameValidation{{$i}}" style="color: red" class="assign_class d-none validation_text" data-id = "school-term-child-first-name-validation" data-value = "{{t('school-term-child-first-name-validation')}}" >{!! t('school-term-child-first-name-validation')!!}</p>

                                            </div>
                                            <div class="form-group">
                                                <input tabindex="{{$tabindex+2}}" type='text' class="form-control assign_class cookies-values" id='child_email{{$i}}' name="child_email{{$i}}" value="@if(\Illuminate\Support\Facades\Session::get('page_number') > 2 )@if($i <= $sales_count )@if($sales[$i-1]->child_email){{$sales[$i-1]->child_email}}@endif @endif @endif"  placeholder="{!! t('school-term-child-email-placeholder')!!}"   data-id = "school-term-child-email-placeholder" data-value = "{{t('school-term-child-email-placeholder')}}" data-inputplaceholder="yes"  required/>

                                                <p id="child_emailValidation{{$i}}" style="color: red" class="assign_class d-none validation_text" data-id = "school-term-child-email-validation" data-value = "{{t('school-term-child-email-validation')}}" >{!! t('school-term-child-email-validation')!!}</p>

                                            </div>
                                        </div>

                                        <div class="col-6">

                                            <div class="form-group">

                                                <input tabindex="{{$tabindex+1}}" type='text' class="form-control assign_class cookies-values" id='child_surname{{$i}}' name="child_surname{{$i}}" value="@if(\Illuminate\Support\Facades\Session::get('page_number')> 2)@if($i <= $sales_count )@if($sales[$i-1]->child_surname){{$sales[$i-1]->child_surname}}@endif @endif @endif"  placeholder="{!! t('school-term-child-surname-placeholder')!!}"   data-id = "school-term-child-surname-placeholder" data-value = "{{t('school-term-child-surname-placeholder')}}" data-inputplaceholder="yes" required/>

                                                <p id="child_surnameValidation{{$i}}" style="color: red" class="assign_class d-none validation_text" data-id = "school-term-child--surname-validation" data-value = "{{t('school-term-child--surname-validation')}}" >{!! t('school-term-child--surname-validation')!!}</p>

                                            </div>
                                            <div class="form-group">

                                                <input tabindex="{{$tabindex+3}}" type='text' class="form-control assign_class cookies-values" id='child_confirm_email{{$i}}' name="child_confirm_email{{$i}}" value="@if(\Illuminate\Support\Facades\Session::get('page_number') > 2 ) @if($i <= $sales_count ) @if($sales[$i-1]->child_email){{$sales[$i-1]->child_email}}@endif @endif @endif" placeholder="{!! t('school-term-child-confirm-email-placeholder')!!}"   data-id = "school-term-child-confirm-email-placeholder" data-value = "{{t('school-term-child-confirm-email-placeholder')}}" data-inputplaceholder="yes"  required/>

                                                <p id="child_confirm_emailValidation{{$i}}" style="color: red" class="assign_class d-none validation_text" data-id = "school-term-child-confirm-email-validation" data-value = "{{t('school-term-child-confirm-email-validation')}}" >{!! t('school-term-child-confirm-email-validation')!!}</p>

                                            </div>

                                        </div>

                                    </div>

                                    @if($school->location_group_id == 1)
                                        @include('frontend.pages.schooltermbased.reserved_location_group_1')
                                    @elseif($school->location_group_id == 2)

                                        @include('frontend.pages.schooltermbased.reserved_location_group_2')

                                    @else
                                        @include('frontend.pages.schooltermbased.reserved_location_group_3')

                                    @endif

                                    <br>

                                    {{--<h3 class="assign_class" data-id = "school-term-child-detail-title" data-value = "{{t('school-term-child-detail-title')}}" >{!! t('school-term-child-detail-title')!!}</h3>--}}

                                </div>
                                <br><br>

                            </div>

                        @endfor

                        <button tabindex="7" type="Button"  id="add_child_button" class="green-button button_text assign_class float-right validation-date holdlocker-validation" style="margin: 0px 3px;" data-id = "school-term-add-new-child-button" data-value = "{{t('school-term-add-new-child-button')}}" onclick="add_child(this)">{!! t('school-term-add-new-child-button')!!}</button>
                        <button  tabindex="8" type="Button" id="remove_child_button"  class="green-button button_text assign_class float-right validation-date holdLocker-validation" style="margin: 0px 3px;background-color: #BD2130" data-id = "school-term-remove-new-child-button" data-value = "{{t('school-term-remove-new-child-button')}}" onclick="remove_child(this)">{!! t('school-term-remove-new-child-button')!!}</button>

                        <br>
                        <br>
                        <br>
                        <br>

                        @include('frontend.pages.schooltermbased.login')

                        <br>
                        @include('frontend.pages.schooltermbased.footer_terms')

                    </div>
                    <br>
                    <br>
                    <br>

                    <div class="text-center validation-date holdLocker-validation"><a href="#." class="green-button button_text assign_class reset-booking" data-sale_id="@if(\Illuminate\Support\Facades\Session::get('page_number')>2){{$sales[0]->booking_id}} @endif" data-id = "school-term-restart-booking-button" data-value = "{{t('school-term-restart-booking-button')}}" style="padding: 9px 10px;">{!! t('school-term-restart-booking-button')!!}</a>
                        @if($school->location_group_id == 1)
                            <button  type="Button"  class="green-button button_text assign_class" data-id = "school-term-check-availability-button" data-value = "{{t('school-term-check-availability-button')}}" id="check_available_button" onclick="check_availability()">{!! t('school-term-check-availability-button')!!}</button>
                        @else
                            <button  type="Button"  class="green-button button_text assign_class" data-id = "school-term-check-availability-button" data-value = "{{t('school-term-check-availability-button')}}" id="check_available_button" onclick="form_validation()">{!! t('school-term-check-availability-button')!!}</button>
                        @endif
                    </div>

                </div>

                </div>

            </form>

                @include('frontend.pages.schooltermbased.school_academic_year_change_modal')
                @include('frontend.pages.schooltermbased.school_year_change_modal')
        </section><!-- #portfolio -->

    </main>

@endsection

@section('script')
{{--    <script type="text/javascript" src="/frontend/js/components.js"></script>--}}
{{--    <script type="text/javascript" src="/frontend/js/custom.js"></script>--}}
    <script src="/frontend/vendors/twitter-bootstrap-wizard/js/jquery.bootstrap.wizard.min.js"></script>
    <script src="/frontend/vendors/swiper/js/swiper.min.js"></script>


    {{--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>--}}

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>

    <script type="text/javascript" src="/frontend/js/pages/radio_checkbox.js"></script>

    <script src="/js/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript" src="/backend/vendors/chosen/js/chosen.jquery.js"></script>
    <script type="text/javascript" src="/frontend/js/pages/form_elements.js"></script>

    <script type="text/javascript" src="/frontend/vendors/swiper/js/swiper.min.js"></script>
    <script type="text/javascript" src="/frontend/js/pages/cards.js"></script>
    <script type="text/javascript" src="/frontend/js/pages/modals.js"></script>
    {{--<script type="text/javascript" src="{{asset('/frontend/js/js-cookie-master')}}"></script>--}}
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@rc/dist/js.cookie.min.js"></script>
    <script type="module" src="https://cdn.jsdelivr.net/npm/js-cookie@rc/dist/js.cookie.min.mjs"></script>
    <script type="text/javascript" src="/frontend/js/bookings/schooltermbased.js"></script>
    <script type="text/javascript" src="/frontend/js/bookings/shcoolterm_reserved_and_edit.js"></script>

    <script type="text/javascript">

        $(document).ready(function() {
            var hold_locker = '<?php echo $sales[0]->hold_locker; ?>';

            var subscription = '@if(auth()->user()){{auth()->user()->subscription}}@else{{"a"}}@endif';

            if(subscription == 1){

                $('#futureComunication').prop('checked', true);
            }


            var total_child = $('#child_count').val();

            for(var child_counter=1 ;child_counter<=total_child ; child_counter++){

                var block_id = $('#block_id'+child_counter).val();

                if($.trim(block_id) == ''){

                    $('#needBlockYearValidation'+child_counter).val(true);
                    shcoolYearValidation(child_counter);
                }else{

                    var shcoolAcademicYear = $.trim($('#shcoolAcademicYear'+child_counter).val());
                    if(shcoolAcademicYear == ''){

                        var sale_id = $('#shcoolAcademicYear'+child_counter).data('sale_id');
                        $('#changeDateStatus'+child_counter).val(true);
                        $('#changeShcoolAcademicYear'+child_counter).val(true);
                        $('#needBlockYearValidation'+child_counter).val(true);

                        lost_booking(sale_id);

                    }else {

                        check_need_validation(child_counter);
                    }
                }
            }
        });

        sessionStorage.removeItem("selected_dropdown_current_tab");
        sessionStorage.removeItem("selected_dropdown_start");
        sessionStorage.removeItem("selected_dropdown_end");
        sessionStorage.removeItem("selected_dropdown_rate");
        sessionStorage.removeItem("selected_dropdown_tax");
        sessionStorage.removeItem("selected_dropdown_block_id");
        sessionStorage.removeItem("selected_dropdown_sale_id");
        sessionStorage.removeItem("selected_dropdown_counter");
        sessionStorage.removeItem("selected_dropdown_hold_locker");
        sessionStorage.removeItem("selected_lockers");

        var locker_ids = "@json($locker_ids)";

        //remove '[' from start of string
        locker_ids = locker_ids.substr(1);

        //remove ']' from end of string
        locker_ids = locker_ids.slice(0, -1);

        //make array wit comma separated
        locker_ids = locker_ids.split(',');
        var locker_ids_array = [];
        $.each(locker_ids, function( index, value ) {

            if(value !== 'null')
            locker_ids_array.push(value);
            else
                locker_ids_array.push(null);
        });

        sessionStorage.setItem("selected_lockers", JSON.stringify(locker_ids_array));

        $(".school_academic_year").on('focus', function () {

            previous_school_academic_year = this.value;

        }).change(function() {

            var counter = $(this).data('counter');
            var sale_id = $.trim($(this).data('sale_id'));


            if($('#changeShcoolAcademicYear'+counter).val() == 'false') {
                $('#school-academic-year-modal').modal('toggle');
            }else{
                shcoolYearValidation(counter);
            }

            $('#school_academic_year_previous_value').val(previous_school_academic_year);
            $('#school_academic_year_changed_value').val(this.value);
            $('#school_academic_year_modal_counter').val(counter);
            $('#sale_academic_id_modal').val(sale_id);

        });
        var previous;

        $(".shcoolYear").on('focus', function () {

            previous = this.value;

        }).change(function() {

            var counter = $(this).data('counter');
            var sale_id = $.trim($(this).data('sale_id'));

            if($('#changeDateStatus'+counter).val() == 'false') {
                $('#school-year-modal').modal('toggle');
            }else{
                shcoolYearValidation(counter);
            }

            $('#school_year_previous_value').val(previous);
            $('#school_year_changed_value').val(this.value);
            $('#school_year_modal_counter').val(counter);
            $('#sale_id_modal').val(sale_id);

        });
        function remove_child() {

            var count = $('#child_count').val();

            var location_group = $.trim($('#location_group').val());

            if(count > 1) {

                var sale_id = $('#sale_id'+count).val();

                var page_number = "{{\Illuminate\Support\Facades\Session::get('page_number')}}";

                if(page_number>2){
                    if($.trim($('#sale_id'+count).val()).length != 0 || $('#sale_id'+count).val() != ' ' ) {

                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        $.ajax({

                            url: '/frontend/removechild',

                            type: 'POST',

                            dataType: 'JSON',

                            cache: false,

                            data: {sale_id: sale_id},

                            success: function (data) {

                                $('#child' + count).addClass('d-none');
                                count--;
                                $('#child_count').val(count);

                            },

                            error: function (jqXHR, textStatus, errorThrown) {

                            }

                        });
                    }else{
                        $('#child' + count).addClass('d-none');
                        count--;
                        $('#child_count').val(count);

                        if(location_group == 1){

                            var years = "{{count($years)}}";
                            if(years > 1){

                                $('#add_child_button').attr('tabindex',(count * 6)+1);
                                $('#remove_child_button').attr('tabindex',(count * 6)+2);

                            }else{

                                $('#add_child_button').attr('tabindex',(count * 5) +1);
                                $('#remove_child_button').attr('tabindex',(count * 5)+2);
                            }

                        }else{
                            $('#add_child_button').attr('tabindex',(count * 6)+1);
                            $('#remove_child_button').attr('tabindex',(count * 6)+2);
                        }
                    }

                }else {
                    $('#child' + count).addClass('d-none');
                    count--;
                    $('#child_count').val(count);

                    if(location_group == 1){

                        var years = "{{count($years)}}";
                        if(years > 1){

                            $('#add_child_button').attr('tabindex',(count * 6)+1);
                            $('#remove_child_button').attr('tabindex',(count * 6)+2);

                        }else{

                            $('#add_child_button').attr('tabindex',(count * 5) +1);
                            $('#remove_child_button').attr('tabindex',(count * 5)+2);
                        }

                    }else{
                        $('#add_child_button').attr('tabindex',(count * 6)+1);
                        $('#remove_child_button').attr('tabindex',(count * 6)+2);
                    }
                }


            }


        }

        // Ajax Logout
        $('#logout_btn').click(function(e){
            e.preventDefault();

            var dest_url = "{{ url('/logout') }}";
            $.ajax({
                type:"POST",
                url:dest_url,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(response){
                    window.location.reload();

                },
                error: function(response){

                }
            })
        });

        function check_availability() {
            // $('#check_available_button').prop('disabled', true);

            $('#form_submittion').val(true);

            var counter = $('#child_count').val();

            var location_group = $.trim($('#location_group').val());

            if(location_group == 1){


                for (var child_count = 1; child_count <= counter; child_count++) {
                    var school_year_id = $.trim($('#school_year_id'+child_count).val());

                    if($('#changeDateStatus'+child_count).val() == 'false' ) {

                        if(child_count == counter){

                            form_validation();

                        }


                    }else{

                        shcoolYearValidation(child_count);
                    }



                }
            }

        }

        function form_validation() {

            $('#check_available_button').prop('disabled', true);
            var counter = $('#child_count').val();

            $('.validation_text').addClass('d-none');
            var location_group = $.trim($('#location_group').val());

            if(location_group != 1){
                $('#form_submittion').val(true);
            }
            var validation_error = false;

            for (var i = 1 ; i <= counter ; i++) {

                var user_id = $.trim($('#user_id').val());
                var child_first_name = $.trim($('#child_first_name'+i).val());
                var child_surname = $.trim($('#child_surname'+i).val());
                var child_email = $.trim($('#child_email'+i).val());

                if(location_group == 1) {

                    var shcool_academic_year_id = $.trim($('#shcoolAcademicYear' + i).val());

                    var shcoolYear = $.trim($('#shcoolYear' + i).val());
                    var school_year_id = $.trim($('#school_year_id'+i).val());
                    var start_date =1;
                    var end_date= 1;
                    if($('#changeDateStatus'+i).val() == 'false' && $('#needBlockYearValidation'+i).val() == 'false' && $('#changeShcoolAcademicYear'+i).val() == 'false'  ) {

                        var daterange = true;
                        shcoolYear = 1

                    }else{
                        var daterange = $("#daterange"+i).val();
                    }

                }else if(location_group == 2){

                    var start_date = $.trim($("#start_date"+i).val());
                    var end_date = $.trim($("#end_date"+i).val());
                    var shcool_academic_year_id = 1;
                    var daterange = $("#daterange"+i).val();
                    var shcoolYear = 1;

                }else if(location_group == 3){
                    var start_date = $.trim($("#start_dateTime"+i).val());
                    var end_date = $.trim($("#end_dateTime"+i).val());
                    var shcool_academic_year_id = 1;
                    var daterange = $("#daterange"+i).val();
                    var shcoolYear = 1;


                }

                $('#endValidation'+i).addClass('d-none');

                $('#laterDateValidation'+i).addClass('d-none');

                if(child_email) {

                    var child_email_validation = isEmail(child_email);

                    var child_confirm_email = $.trim($('#child_confirm_email' + i).val());

                    if( child_email != child_confirm_email) {

                        child_confirm_email = false;
                    }else{
                        child_confirm_email = true;
                    }
                }else{
                    child_email_validation = true;

                    child_confirm_email = true;
                }

                var daterange_staus = '';


                if (daterange == 'false') {

                    daterange_staus = 'false';
                }

                if (child_first_name.length == 0) {

                    $('#child_first_nameValidation'+i).removeClass('d-none');
                    $('#child_first_name'+i).focus();
                    validation_error =true;

                }

                if (child_surname.length == 0) {

                    $('#child_surnameValidation'+i).removeClass('d-none');
                    $('#child_surname'+i).focus();
                    validation_error =true;
                }

                if (child_email_validation == false) {

                    $('#child_emailValidation'+i).removeClass('d-none');
                    $('#child_email'+i).focus();
                    validation_error =true;
                }

                if (child_confirm_email == false) {

                    $('#child_confirm_emailValidation'+i).removeClass('d-none');
                    $('#child_confirm_email'+i).focus();
                    validation_error =true;
                }

                if (start_date.length == 0 ) {

                    $('#startValidation'+i).removeClass('d-none');
                    $('#start_date'+i).focus();
                    validation_error =true;
                }

                if (end_date.length == 0 ) {

                    $('#endDateValidation'+i).removeClass('d-none');
                    $('#end_date'+i).focus();
                    validation_error =true;
                }

                if (user_id == 0) {

                    $('#signInValidation').removeClass('d-none');
                    $('#signInEmail').focus();
                    validation_error =true;
                }

                if (shcool_academic_year_id.length == 0) {

                    $('#shcoolAcademicYearValidation'+i).removeClass('d-none');
                    $('#shcoolAcademicYear'+i).focus();
                    validation_error =true;
                }
                if(start_date.length == 0 || end_date.length == 0){
                    validation_error =true;
                }else{

                    if (shcoolYear.length == 0) {

                        $('#shcoolYearValidation'+i).removeClass('d-none');
                        $('#shcoolYear'+i).focus();
                        validation_error =true;
                    }else if(shcool_academic_year_id.length == 0){
                        $('#shcoolAcademicYearValidation'+i).removeClass('d-none');
                        $('#shcoolAcademicYear'+i).focus();
                        validation_error =true;

                    }else if(daterange_staus == 'false') {

                        $('#endValidation'+i).removeClass('d-none');
                        validation_error =true;
                    }
                }

                if(i == counter){
                    if(validation_error == true) {
                        $('#check_available_button').prop('disabled', false);
                        $('#form_submittion').val(false);
                        return false;
                    }
                }
                if(counter == i){

                    if($('#terms_conditions').is( ":checked")==true){
                        $('#check_available_button').prop('disabled', false);
                        if($('#form_submittion').val() == 'true') {
                            var code = $.trim($('#voucher_code').val());
                            var voucher_code_id = $('#voucher_code_id').val('');
                            if( (code!='')){

                                voucher_code_validation(code);

                            }else {
                                $('#SchoolBookingForm').submit();
                            }
                        }

                    }else{
                        $('#check_available_button').prop('disabled', false);
                        $('#term_chk_msg').removeClass('d-none');
                        return false;

                    }
                }

            }

        }

        $(function () {

            var arrowPage = "{{ Session::get('arrowsPage') }}";

            if(arrowPage == 2){

                $("#left_arrow").removeClass('d-none');

            }else if(arrowPage > 2){

                $("#left_arrow").removeClass('d-none');
                $("#right_arrow").removeClass('d-none');

            }


            $('body').on('click','#left_arrow',function(e){

                $('.reset-booking').click();

            });

            $('body').on('click','#right_arrow',function(e){

                e.preventDefault();

                window.location.href = '{{ route('saveSchoolBookingBack') }}';

            });

        });


    </script>
@endsection
