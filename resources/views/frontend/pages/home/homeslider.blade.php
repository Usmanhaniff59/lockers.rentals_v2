@php
    $sliders = \App\Slider::all();
     $status = true;
@endphp
<section id="intro">
    <div class="intro-container">
        <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">

            <div class="carousel-inner" role="listbox">
                @if(count($sliders) > 0)
                    @foreach($sliders as $slider)
                        @if($status == true)
                            <div class="carousel-item active">
                                {{--                                <div class="carousel-background"><img src="/backend/img/sliders/{{$slider->img_url}}" alt=""></div>--}}
                                <div class="carousel-container">
                                    <div class="carousel-content">
                                        {{--                                        @if(\Illuminate\Support\Facades\Session::get('switchCMS') == 'true')--}}
                                        {{--                                            <h2 style="margin-top: 20px" class="text-white assign_class"  data-id = "{{$slider->title}}" data-value = "{{t($slider->title)}}" >{!! t($slider->title)!!}</h2>--}}
                                        {{--                                        @else--}}
                                        {{--                                            <h2 style="margin-top: 20px" class="slider-title assign_class"  data-id = "{{$slider->title}}" data-value = "{{t($slider->title)}}" >{!! t($slider->title)!!}</h2>--}}
                                        {{--                                        @endif--}}
                                        {{--@if($slider->open_in_new_tab == 1)--}}
                                        {{--<a href="{{$slider->button_link}}" class="btn-get-started scrollto assign_class" data-id = "{{$slider->button_text}}" data-value = "{{t($slider->button_text)}}"  >{!! t($slider->button_text)!!}</a>--}}
                                        {{--@else--}}
                                        {{--<a href="{{$slider->button_link}}" class="btn-get-started scrollto assign_class" target="_blank" data-id = "{{$slider->button_text}}" data-value = "{{t($slider->button_text)}}" >{!! t($slider->button_text)!!}</a>--}}
                                        {{--@endif--}}
                                    </div>
                                </div>
                            </div>
                            @php
                                //$status = false;
                            @endphp
                        @else
                            <div class="carousel-item">
                                {{--                                <div class="carousel-background"><img src="/backend/img/sliders/{{$slider->img_url}}" alt=""></div>--}}
                                <div class="carousel-container">
                                    <div class="carousel-content">
                                        @if(\Illuminate\Support\Facades\Session::get('switchCMS') == 'true')
                                            {{--                                            <h2 style="margin-top: 20px" class="text-white assign_class"  data-id = "{{$slider->title}}" data-value = "{{t($slider->title)}}" >{!! t($slider->title)!!}</h2>--}}
                                        @else
                                            {{--                                            <h2 style="margin-top: 20px" class="slider-title assign_class"  data-id = "{{$slider->title}}" data-value = "{{t($slider->title)}}" >{!! t($slider->title)!!}</h2>--}}
                                        @endif
                                        {{--@if($slider->open_in_new_tab == 1)--}}
                                        {{--<a href="{{$slider->button_link}}" class="btn-get-started scrollto assign_class"   data-id = "{{$slider->button_text}}" data-value = "{{t($slider->button_text)}}"  >{!! t($slider->button_text)!!}</a>--}}
                                        {{--@else--}}
                                        {{--<a href="{{$slider->button_link}}" class="btn-get-started scrollto assign_class"   target="_blank" data-id = "{{$slider->button_text}}" data-value = "{{t($slider->button_text)}}" >{!! t($slider->button_text)!!}</a>--}}
                                        {{--@endif--}}
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                @else
                    <div class="carousel-item active header_bottom_bg">
                        {{--                        <div class="carousel-background"></div>--}}
                        <div class="carousel-container">
                            <div class="carousel-content">
                                {{--                                <h2 style="margin-top: 120px" class="text-white assign_class"  data-id = "static-banner-title" data-value = "{{t('static-banner-title')}}" >{!! t('static-banner-title')!!}</h2>--}}
                                {{--<a href="/frontend/requestavailability" class="btn-get-started scrollto assign_class"  data-id = "static-banner-button" data-value = "{{t('static-banner-button')}}" >{!! t('static-banner-button')!!}</a>--}}
                            </div>
                        </div>
                    </div>
                @endif
            </div>

            {{--<a class="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">--}}
            {{--<span class="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>--}}
            {{--<span class="sr-only">Previous</span>--}}
            {{--</a>--}}

            {{--<a class="carousel-control-next" href="#introCarousel" role="button" data-slide="next">--}}
            {{--<span class="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>--}}
            {{--<span class="sr-only">Next</span>--}}
            {{--</a>--}}

        </div>
    </div>

    <div class="container pb-lg-5 intro-home">
        <div class="row">
            <div class="col-md-4 col-lg-4 col-sm-12 "><img class="header_banner" src="/frontend/img/banner/banner_1.png"/></div>
            <div class="col-md-4 col-lg-4 col-sm-12 "><img class="header_banner" src="/frontend/img/banner/banner_2.png"/></div>
            <div class="col-md-4 col-lg-4 col-sm-12 "><img class="header_banner" src="/frontend/img/banner/banner_3.png"/></div>
        </div>
    </div>
</section><!-- #intro -->