


<div class="modal" id="myModal">
    <div class="modal-dialog" style="max-width: 724px!important;">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Change Text</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <input type="hidden" id="modal_text_id">
                <input type="hidden" id="modal_placeholders">
                <input type="hidden" id="modal_placeholder_values">
                <input type="hidden" id="modal_input_placeholder">
                <input type="hidden" id="modal_steps">
                <div class="summernote" id="body-text"></div><br>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer cms-modal-footer">
                <div class="row">
                    <div class="col-6">
                        <div id="placeholders"></div>
                    </div>
                    <div class="col-6">
                        <button type="button" class="btn btn-success save-button float-right"   data-dismiss="modal">Save</button>
                    </div>
                </div>
            </div>




        </div>
    </div>
</div>

{{--<div class="modal" id="myModal">--}}
    {{--<div class="modal-dialog" style="max-width: 724px!important;">--}}
        {{--<div class="modal-content">--}}

            {{--<!-- Modal Header -->--}}
            {{--<div class="modal-header">--}}
                {{--<h4 class="modal-title">Change Text</h4>--}}
                {{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
            {{--</div>--}}

            {{--<!-- Modal body -->--}}
            {{--<div class="modal-body">--}}
                {{--<form id="cms-form">--}}
                    {{--<input type="hidden" name="total-lang"  id="total-lang">--}}
                    {{--<input type="hidden" id="modal_text_id" >--}}
                    {{--<div class="summernote" id="body-text-append"></div><br>--}}
                {{--<div class="summernote" id="body-text"></div><br>--}}
                {{--</form>--}}
                {{--<div class="summernote2" id="body-text2"></div><br>--}}
            {{--</div>--}}

            {{--<!-- Modal footer -->--}}
            {{--<div class="modal-footer">--}}
                {{--<button type="button" class="btn btn-success save-button" data-dismiss="modal">Save</button>--}}
            {{--</div>--}}

        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}