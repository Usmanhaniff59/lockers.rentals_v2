<div class="card validation-date holdLocker-validation">
    <div class="card-body">

        <div class="checkbox validation-date holdLocker-validation" >

            <label>

                <input type="checkbox" name="futureComunication" id="futureComunication" value="1">

                <span class="cr"><i class="cr-icon fa fa-check"></i></span>

                <span class="assign_class" data-id = "school-term-receive-future-checkbox-text" data-value = "{{t('school-term-receive-future-checkbox-text')}}"> {!! t('school-term-receive-future-checkbox-text')!!} </span><br><br>

            </label>

        </div>
        @if($school->location_group_id == 1)
            @if($school->keep_block != 3)
                <div class="form-group">

                    <div class="checkbox" id="checkbox_hold_locker" checked="true">
                        <label>
                            <input type="checkbox" class="hold_locker" name="hold_locker" id="hold_locker"  @if(\Illuminate\Support\Facades\Session::get('page_number') > 2 )@if(\Illuminate\Support\Facades\Session::get('hold_locker_page2_checked') == true ) checked @endif @endif>
                            <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                            <span class="assign_class" data-id = "school-term-hold-this-locker-checkbox-text" data-value = "{{t('school-term-hold-this-locker-checkbox-text')}}"> {!! t('school-term-hold-this-locker-checkbox-text')!!}</span><br>

                        </label>
                    </div>
                </div>

            @endif
        @endif

        {{--@can('Voucher Code')--}}
        <div class="form-group">

            <?php
            if(\Illuminate\Support\Facades\Session::has('voucher_code_id')){
                $voucher_code_id = \Illuminate\Support\Facades\Session::get('voucher_code_id');
                $voucher_code=\App\VoucherCode::find($voucher_code_id);
            }
            ?>
            <input   type='hidden' class="form-control assign_class" id='voucher_code_id' name="voucher_code_id" value="@if(\Illuminate\Support\Facades\Session::has('voucher_code_id')){{$voucher_code->id}} @endif"  />

            <label for="voucher_code" class="assign_class" data-id = "school-term-voucher-code-label" data-value = "{{t('school-term-voucher-code-label')}}" >{!! t('school-term-voucher-code-label')!!}</label>

            {{--<input   type='text' class="form-control assign_class"  id='voucher_code' value="@if(\Illuminate\Support\Facades\Session::has('voucher_code_id')){{$voucher_code->code}} @endif"  placeholder="{!! t('school-term-voucher-code-placeholder')!!}" data-id = "school-term-voucher-code-placeholder" data-value = "{{t('school-term-voucher-code-placeholder')}}"  data-inputplaceholder="yes"  />--}}
            <input   type='text' class="form-control assign_class"  id='voucher_code' value="@if(\Illuminate\Support\Facades\Session::has('voucher_code_id')){{$voucher_code->code}} @endif"  placeholder="Voucher Code"  />

            <p id="voucherCodeValidation" style="color: red" class="assign_class validation_text"> </p>
        </div>
        {{--@else--}}
        {{--<div class="form-group d-none">--}}

        {{--<input   type='hidden' class="form-control assign_class" id='voucher_code_id' />--}}

        {{--<label for="voucher_code" class="assign_class" data-id = "school-term-voucher-code-label" data-value = "{{t('school-term-voucher-code-label')}}" >{!! t('school-term-voucher-code-label')!!}</label>--}}

        {{--<input   type='text' class="form-control assign_class"  id='voucher_code'  placeholder="{!! t('school-term-voucher-code-placeholder')!!}" data-id = "school-term-voucher-code-placeholder" data-value = "{{t('school-term-voucher-code-placeholder')}}"  data-inputplaceholder="yes"  />--}}
        {{--<input   type='text' class="form-control assign_class"  id='voucher_code'  placeholder="Voucher Code"  />--}}

        {{--<p id="voucherCodeValidation" style="color: red" class="assign_class validation_text"> </p>--}}

        {{--</div>--}}
        {{--@endcan--}}


        <br>
        <div class="validation-date holdLocker-validation">

            <h5 class="assign_class " style="margin-left: 15px" data-id = "school-term-terms-conditions-title" data-value = "{{t('school-term-terms-conditions-title')}}" >{!! t('school-term-terms-conditions-title')!!}</h5><br>

            <div class="checkbox" style="margin-left: 15px">

                <span class="assign_class" data-id = "school-term-agree-text" data-value = "{{t('school-term-agree-text')}}" >{!! t('school-term-agree-text')!!} </span> &nbsp;

                <label>

                    <input type="checkbox" name="terms_conditions" id="terms_conditions" @if(\Illuminate\Support\Facades\Session::get('page_number') > 2 ) checked @endif  >

                    <span class="cr"><i class="cr-icon fa fa-check"></i></span>

                    <a class="green-button button_text terms_button assign_class" onclick="show_terms()" style="margin: 10px;background-color: #A9A9A9;color: white"  data-id = "school-term-terms-conditions-button" data-value = "{{t('school-term-terms-conditions-button')}}" >{!! t('school-term-terms-conditions-button')!!}</a>


                </label>

            </div>

            <label class="assign_class d-none" id="term_chk_msg" style="color: red;margin-left: 15px" data-id = "school-term-agree-terms-check-alert" data-value = "{{t('school-term-agree-terms-check-alert')}}" >{!! t('school-term-agree-terms-check-alert')!!}</label>

        </div>
        <div style="margin: 10px 15px; border: 2px solid darkgrey; border-radius: 10px; padding: 10px; display: none;" id="terms_div">

            <p class="assign_class" data-id = "school-term-terms-title" data-value = "{{t('school-term-terms-title')}}" >{!! t('school-term-terms-title')!!}  </p>

            <span class="assign_class" data-id = "school-term-terms-and-conditions" data-value = "{{t('school-term-terms-and-conditions')}}">{!! t('school-term-terms-and-conditions')!!}</span>

            <p class="assign_class" data-id = "school-term-terms-bottom-text" data-value = "{{t('school-term-terms-bottom-text')}}" >{!! t('school-term-terms-bottom-text')!!}</p>

        </div>
       
        <br>
    </div>
    
</div>