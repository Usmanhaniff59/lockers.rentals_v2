<div class="modal fade" id="school-year-modal" role="dialog" aria-labelledby="modalLabelprimary">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary float-left">
                <h4 class="modal-title text-white " id="modalLabelprimary">Booking Dates</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="school_year_previous_value">
                <input type="hidden" id="school_year_changed_value">
                <input type="hidden" id="school_year_modal_counter">
                <input type="hidden" id="sale_id_modal">
                <p>
                    If you change the dates your reserved locker will be released. Do you want to Continue
                </p>

            </div>
            <div class="modal-footer text-right">
                <button class="btn btn-success" id="school-year-modal-keep-my-dates-button" data-dismiss="modal">keep my dates</button>
                <button class="btn btn-primary" id="school-year-modal-change-date-button" data-dismiss="modal">Continue</button>

            </div>
        </div>
    </div>
</div>