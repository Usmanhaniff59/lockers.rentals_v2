<input type='hidden' class="form-control" name="daterange" id="daterange{{$i}}" value="@if(\Illuminate\Support\Facades\Session::get('page_number') > 2 )@if($i <= $sales_count ) true @else false @endif @else false @endif" />

<input type='hidden' class="form-control" id="child_number_date" value="{{$i}}" />

<div class="form-group">
    <label class="assign_class" data-id = "school-term-sub-title" data-value = "{{t('school-term-sub-title')}}" >{!! t('school-term-sub-title')!!}</label>

    <input tabindex="{{$tabindex+4}}" type='text' class="form-control assign_class datetimepicker_start" id="start_dateTime{{$i}}" name="start_dateTime{{$i}}" value="@if(\Illuminate\Support\Facades\Session::get('page_number')>2)@if($i <= $sales_count )@if($sales[$i-1]->start){{\Carbon\Carbon::parse($sales[$i-1]->start)->format('d M, Y H:i')}}@endif @endif @endif" data-counter="{{$i}}" placeholder="{!! t('school-term-start-date-placeholder')!!}" data-id = "school-term-start-date-placeholder" data-value = "{{t('school-term-start-date-placeholder')}}" data-inputplaceholder="yes"  />

    <p id="startValidation{{$i}}" style="color: red" class="assign_class d-none validation_text" data-id = "school-term-start-date-validation" data-value = "{{t('school-term-start-date-validation')}}" >{!! t('school-term-start-date-validation')!!}</p>

</div>

<div class="form-group">

    <input tabindex="{{$tabindex+5}}" type='text' class="form-control assign_class datetimepicker_end" id="end_dateTime{{$i}}" name="end_dateTime{{$i}}" value="@if(\Illuminate\Support\Facades\Session::get('page_number')>2)@if($i <= $sales_count )@if($sales[$i-1]->end){{\Carbon\Carbon::parse($sales[$i-1]->end)->format('d M, Y H:i')}}@endif @endif @endif" data-counter="{{$i}}" placeholder="{!! t('school-term-end-date-placeholder')!!}" data-id = "school-term-end-date-placeholder" data-value = "{{t('school-term-end-date-placeholder')}}" data-inputplaceholder="yes"  />

    <p id="endDateValidation{{$i}}" style="color: red" class="assign_class d-none validation_text" data-id = "school-term-end-date-validation" data-value = "{{t('school-term-end-date-validation')}}" >{!! t('school-term-end-date-validation')!!}</p>

</div>

<div class="form-group">

    <p id="endValidation{{$i}}" style="color: red" class="assign_class d-none" data-id = "school-term-locker-availability-validation" data-value = "{{t('school-term-locker-availability-validation')}}" data-placeholders="%fromDate%,%toDate%,%locationName%,%requestFormLINK%" >{!! t('school-term-locker-availability-validation')!!}</p>
    <p id="laterDateValidation{{$i}}" style="color: red" class="assign_class d-none" data-id = "school-term-end-later-date-validation" data-value = "{{t('school-term-end-later-date-validation')}}" data-placeholders="%fromDate%,%toDate%" >{!! t('school-term-end-later-date-validation')!!}</p>


</div>