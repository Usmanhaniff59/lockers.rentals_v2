<input type='hidden' class="form-control" name="daterange" id="daterange{{$i}}"
       value="@if(\Illuminate\Support\Facades\Session::get('page_number') > 2 )@if($i <= $sales_count ) true @else false @endif @else false @endif"/>

@if($i <= $sales_count )

    <div class="form-group">
        <label for="shcoolAcademicYear" class="assign_class" data-id="school-term-school-academic-year"
               data-value="{{t('school-term-school-academic-year')}}">{!! t('school-term-school-academic-year')!!}</label>

        @if(isset($sales[$i-1]->block_id))
            <input type="hidden" class="form-control" id="changeShcoolAcademicYear{{$i}}" value="false"/>
            <input type="hidden" class="form-control" id="needBlockYearValidation{{$i}}" value="false"/>
            <select tabindex="{{$tabindex+4}}" class="form-control cookies-values school_academic_year"
                    id="shcoolAcademicYear{{$i}}"
                    onchange="update_sales_log( '<?php echo $i ?>' , 'shcoolAcademicYear');" data-counter="{{$i}}"
                    data-sale_id="{{$sales[$i-1]->sale_id}}" data-block_id="{{$sales[$i-1]->block_id}}"
                    data-locker_id="{{$sales[$i-1]->locker_id}}" data-company_id="{{$sales[$i-1]->company_id}}"
                    name="school_academic_year_id{{$i}}">
                @else
                    <input type="hidden" class="form-control" id="needBlockYearValidation{{$i}}" value="true"/>
                    <input type="hidden" class="form-control" id="changeShcoolAcademicYear{{$i}}" value="true"/>
                    <select tabindex="{{$tabindex+4}}"
                            class="form-control cookies-values check-need-validation-without-block school_academic_year"
                            id="shcoolAcademicYear{{$i}}"
                            onchange="update_sales_log( '<?php echo $i ?>', 'shcoolAcademicYear'); shcoolYearValidation({{$i}})"
                            data-counter="{{$i}}" name="school_academic_year_id{{$i}}">
                        @endif
                        <option value="" selected>Choose School Academic Year</option>
                        @foreach($academic_years as $academic_year)
                            <option value="{{$academic_year['id']}}"
                                    @if(\Illuminate\Support\Facades\Session::get('page_number') > 2 )@if($i <= $sales_count )@if($sales[$i-1]->school_academic_year_id == $academic_year['id'] ) selected @endif @endif @endif>{{ $academic_year['school_year'] }}</option>
                        @endforeach

                    </select>

                    <p id="shcoolAcademicYearValidation{{$i}}" style="color: red"
                       class="assign_class validation_text d-none" data-id="school-term-school-academic-year-validation"
                       data-value="{{t('school-term-school-academic-year-validation')}}">{!! t('school-term-school-academic-year-validation')!!}</p>

    </div>

    <?php  $schoolYear = App\SchoolYear::where('id', $sales[$i - 1]->school_year_id)->first() ?>

    @if($schoolYear->status == 1)

        @if(count($years) > 1)
            @if($sales[$i-1]->block_id == null || $sales[$i-1]->block_id == 0)
                <input type="hidden" class="form-control" id="changeDateStatus{{$i}}" value="true"/>
            @else
                <input type="hidden" class="form-control" id="changeDateStatus{{$i}}" value="false"/>
            @endif

            <div class="form-group">
                <label for="shcoolYear" class="assign_class cookies-values" data-id="school-term-school-year"
                       data-value="{{t('school-term-school-year')}}">{!! t('school-term-school-year')!!}</label>

                <select tabindex="{{$tabindex+5}}" class="form-control shcoolYear" id="shcoolYear{{$i}}"
                        onchange="update_sales_log( '<?php echo $i ?>' , 'shcoolYear');" data-counter="{{$i}}"
                        data-sale_id="@if(\Illuminate\Support\Facades\Session::get('page_number')> 2)@if($i <= $sales_count ) {{$sales[$i-1]->sale_id}} @endif @endif"
                        name="school_year_id{{$i}}" value="{{ old('school_year_id') }}">
                    <option value="" selected>Choose School Year</option>

                    @foreach($years as $year)
                        <option value="{{$year['id']}}"
                                @if(\Illuminate\Support\Facades\Session::get('page_number') > 2 )@if($i <= $sales_count )@if($sales[$i-1]->school_year_id == $year['id'] ) selected @endif @endif @endif>{{ $year['title'] }}</option>
                    @endforeach
                </select>

                <p id="shcoolYearValidation{{$i}}" style="color: red" class="assign_class validation_text d-none"
                   data-id="school-term-school-year-validation"
                   data-value="{{t('school-term-school-year-validation')}}">{!! t('school-term-school-year-validation')!!}</p>

            </div>


            <?php  $tabindex = $tabindex + 6 ?>

        @else
            <input type="hidden" class="form-control" id="changeDateStatus{{$i}}" value="false"/>

            <div class="form-group">
                <input type="hidden" name="school_year_id{{$i}}" id="shcoolYear{{$i}}" value="{{$schoolYear->id}}">
                <span class="m-t-10"><b>School Year: </b>{!! $schoolYear->title !!}</span>
            </div>
            <?php  $tabindex = $tabindex + 5 ?>

        @endif
    @else
        <input type="hidden" class="form-control" id="changeDateStatus{{$i}}" value="false"/>

        <div class="form-group">
            <input type="hidden" name="school_year_id{{$i}}" id="shcoolYear{{$i}}" value="{{$schoolYear->id}}">
            <span class="m-t-10"><b>School Year: </b>{!! $schoolYear->title !!}</span>
        </div>

        <?php  $tabindex = $tabindex + 5 ?>

    @endif
@else
    <div class="form-group">
        <label for="shcoolAcademicYear" class="assign_class" data-id="school-term-school-academic-year"
               data-value="{{t('school-term-school-academic-year')}}">{!! t('school-term-school-academic-year')!!}</label>
        <input type="hidden" class="form-control" id="needBlockYearValidation{{$i}}" value="true"/>

        <select tabindex="{{$tabindex+4}}" class="form-control cookies-values" id="shcoolAcademicYear{{$i}}"
                onchange="update_sales_log('<?php echo $i ?>' , 'shcoolAcademicYear'); shcoolYearValidation({{$i}})"
                data-counter="{{$i}}" name="school_academic_year_id{{$i}}">
            <option value="" selected>Choose School Academic Year</option>
            @foreach($academic_years as $academic_year)
                <option value="{{$academic_year['id']}}"
                        @if(\Illuminate\Support\Facades\Session::get('page_number') > 2 )@if($i <= $sales_count )@if($sales[$i-1]->school_academic_year_id == $academic_year['id'] ) selected @endif @endif @endif>{{ $academic_year['school_year'] }}</option>
            @endforeach
        </select>
        <p id="shcoolAcademicYearValidation{{$i}}" style="color: red" class="assign_class validation_text d-none"
           data-id="school-term-school-academic-year-validation"
           data-value="{{t('school-term-school-academic-year-validation')}}">{!! t('school-term-school-academic-year-validation')!!}</p>

    </div>
    @if(count($years) > 1)
        <div class="form-group">
            <label for="shcoolYear" class="assign_class cookies-values" data-id="school-term-school-year"
                   data-value="{{t('school-term-school-year')}}">{!! t('school-term-school-year')!!}</label>
            <input type="hidden" class="form-control" id="changeDateStatus{{$i}}" value="true"/>

            <select tabindex="{{$tabindex+5}}" class="form-control" id="shcoolYear{{$i}}"
                    onchange="update_sales_log( '<?php echo $i ?>' , 'shcoolYear'); shcoolYearValidation({{$i}})"
                    data-counter="{{$i}}" name="school_year_id{{$i}}" value="{{ old('school_year_id') }}">
                <option value="" selected>Choose School Year</option>

                @foreach($years as $year)
                    <option value="{{$year['id']}}"
                            @if(\Illuminate\Support\Facades\Session::get('page_number') > 2 )@if($i <= $sales_count )@if($sales[$i-1]->school_year_id == $year['id'] ) selected @endif @endif @endif>{{ $year['title'] }}</option>
                @endforeach
            </select>

            <p id="shcoolYearValidation{{$i}}" style="color: red" class="assign_class validation_text d-none"
               data-id="school-term-school-year-validation"
               data-value="{{t('school-term-school-year-validation')}}">{!! t('school-term-school-year-validation')!!}</p>

        </div>


        <?php  $tabindex = $tabindex + 6 ?>

    @else
        <input type="hidden" class="form-control" id="changeDateStatus{{$i}}" value="true"/>
        <div class="form-group">
            <input type="hidden" name="school_year_id{{$i}}" id="shcoolYear{{$i}}" value="{{$years[0]['id']}}">
            <span class="m-t-10"><b>School Year: </b>{!! $years[0]['title'] !!}</span>
        </div>
        <?php  $tabindex = $tabindex + 5 ?>

    @endif
@endif


<div class="form-group">

    <p id="endValidation{{$i}}" style="color: red" class="assign_class d-none"
       data-id="school-term-locker-availability-validation"
       data-value="{{t('school-term-locker-availability-validation')}}"
       data-placeholders="%fromDate%,%toDate%,%locationName%,%requestFormLINK%">{!! t('school-term-locker-availability-validation')!!}</p>
    <p id="hold_lockerValidation{{$i}}" style="color: red" class="assign_class d-none"
       data-id="school-term-hold-locker-availability-validation"
       data-value="{{t('school-term-hold-locker-availability-validation')}}"
       data-placeholders="%fromDate%,%toDate%,%locationName%,%requestFormLINK%">{!! t('school-term-hold-locker-availability-validation')!!}</p>

    <p id="laterDateValidation{{$i}}" style="color: red" class="assign_class d-none"
       data-id="school-term-end-later-date-validation" data-value="{{t('school-term-end-later-date-validation')}}"
       data-placeholders="%fromDate%,%toDate%">{!! t('school-term-end-later-date-validation')!!}</p>
    {{--<p id="schoolYearUserGuideMsg{{$i}}" style="color:#F5B54A" class="assign_class @if(\Illuminate\Support\Facades\Session::get('page_number') > 2 )@if($i <= $sales_count ) d-none @endif @endif" data-id = "school-term-school-year-user-guide-msg" data-value = "{{t('school-term-school-year-user-guide-msg')}}" >{!! t('school-term-school-year-user-guide-msg')!!}</p>--}}

</div>
@if($school->keep_block == 1)

    <div class="form-group">

        <div class="checkbox validation-date">

            <label class="d-none">

                <input type="checkbox" name="hold_locker{{$i}}" id="hold_locker{{$i}}" data-counter="{{$i}}">
                <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                <span class="assign_class" data-id="school-term-hold-this-locker-checkbox-text"
                      data-value="{{t('school-term-hold-this-locker-checkbox-text')}}"> {!! t('school-term-hold-this-locker-checkbox-text')!!}</span><br>

            </label>

        </div>

    </div>

@else
    <div class="form-group">

        <div class="checkbox validation-date">

            <label class="d-none">

                <input type="checkbox" name="hold_locker{{$i}}" id="hold_locker{{$i}}" data-counter="{{$i}}" checked>
                <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                <span class="assign_class" data-id="school-term-hold-this-locker-checkbox-text"
                      data-value="{{t('school-term-hold-this-locker-checkbox-text')}}"> {!! t('school-term-hold-this-locker-checkbox-text')!!}</span><br>

            </label>

        </div>

    </div>

@endif

<p id="enterEndDateValidation{{$i}}" style="color: red" class="assign_class d-none"
   data-id="school-term-end-date-validation" data-value="{{t('school-term-end-date-validation')}}"
   data-placeholders="%fromDate%,%toDate%,%locationName%,%requestFormLINK%">{!! t('school-term-end-date-validation')!!}</p>
