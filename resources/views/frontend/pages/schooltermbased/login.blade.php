<div class="card validation-date holdLocker-validation">

    <div class="card-body">
        <h3 class="assign_class text-blue" data-id = "school-term-my-detail-title" data-value = "{{t('school-term-my-detail-title')}}" >{!! t('school-term-my-detail-title')!!}</h3>

        <div id="login_request" class="d-none">

            <div class="card" id="login_card">

                <div class="card-header bg-white">

                    <ul class="nav nav-tabs card-header-tabs float-left">
                        <li class="nav-item">
                            <a class="nav-link active button_text assign_class get_text" href="#tab1" data-toggle="tab" data-id = "home-request-sign-in-tab-text" data-value = "{{t('home-request-sign-in-tab-text')}}" >{!! t('home-request-sign-in-tab-text') !!}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link button_text assign_class get_text" href="#tab2" data-toggle="tab" data-id = "home-request-create-account-tab-text" data-value = "{{t('home-request-create-account-tab-text')}}" >{!! t('home-request-create-account-tab-text') !!}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link button_text assign_class get_text" href="#tab3" data-toggle="tab">Forgot password</a>
                        </li>
                    </ul>

                </div>

                <div class="card-body">

                    <div class="tab-content text-justify">
                        <div class="tab-pane active" id="tab1">
                            <h2 class="card-title text-center text-blue assign_class" style="margin: 20px"  data-id = "home-request-sign-in-title" data-value = "{{t('home-request-sign-in-title')}}" >{!! t('home-request-sign-in-title') !!}</h2>
                            <form class="login" action="{{ url('/login') }}" method="post" data-type="json" id="signInForm">

                                <div class="form-group">
                                    <input type="email" name="email" class="form-control assign_class" id="signInEmail" placeholder="{!! t('home-request-sign-in-email-placeholder')!!}"   data-id = "home-request-sign-in-email-placeholder" data-value = "{{t('home-request-sign-in-email-placeholder')}}" data-inputplaceholder="yes"/>
                                    <div id="signInEmailValidation" style="color: red" class="assign_class d-none validation_text" data-id = "home-request-sign-in-email-validation" data-value = "{{t('home-request-sign-in-email-validation')}}" >{!! t('home-request-sign-in-email-validation')!!}</div>

                                </div>

                                <div class="form-group">
                                    <input type="password" name="password" class="form-control assign_class" id="signInPassword" placeholder="{!! t('home-request-sign-in-password-placeholder')!!}" data-id = "home-request-sign-in-password-placeholder" data-value = "{{t('home-request-sign-in-password-placeholder')}}" data-inputplaceholder="yes"/>
                                    <div id="signInPasswordValidation" style="color: red" class="assign_class d-none validation_text" data-id = "home-request-sign-in-password-validation" data-value = "{{t('home-request-sign-in-password-validation')}}" >{!! t('home-request-sign-in-password-validation')!!}</div>

                                </div>
                                <div id="signInIncorrectValidation" style="color: red" class="assign_class d-none validation_text" data-id = "home-request-sign-in-incorrect-validation" data-value = "{{t('home-request-sign-in-incorrect-validation')}}" >{!! t('home-request-sign-in-incorrect-validation')!!}</div>

                                <div id="signInValidation" style="color: red" class="assign_class d-none validation_text" data-id = "home-request-sign-in-validation" data-value = "{{t('home-request-sign-in-validation')}}" >{!! t('home-request-sign-in-validation')!!}</div>

                                <div class="form-group text-center">
                                    <button type="button" class="green-button button_text assign_class" style="margin: 20px;padding: 7px 70px;" id="signIn-button" data-id = "home-request-sign-in-button" data-value = "{{t('home-request-sign-in-button')}}" >{!! t('home-request-sign-in-button')!!}</button>
                                </div>

                            </form>
                        </div>
                        <div class="tab-pane" id="tab2">

                            <h2 class="card-title text-center text-blue assign_class assign_class" style="margin: 20px"  data-id = "home-request-sign-up-title" data-value = "{{t('home-request-sign-up-title')}}" >{!! t('home-request-sign-up-title') !!}</h2>
                            <p class="card-title text-center assign_class assign_class" style="margin: 20px"  data-id = "home-request-sign-up-sub-title-msg" data-value = "{{t('home-request-sign-up-sub-title-msg')}}" >{!! t('home-request-sign-up-sub-title-msg') !!}</p>



                            <div class="form-group">
                                <input type="text" name="first_name" class="form-control assign_class" id="signUpFirstName" placeholder="{!! t('home-request-sign-up-first-name-placeholder')!!}"   data-id = "home-request-sign-up-first-name-placeholder" data-value = "{{t('home-request-sign-up-first-name-placeholder')}}" data-inputplaceholder="yes"/>
                                <div id="signUpFirstNameValidation" style="color: red" class="assign_class d-none validation_text" data-id = "home-request-sign-up-first-name-validation" data-value = "{{t('home-request-sign-up-first-name-validation')}}" >{!! t('home-request-sign-up-first-name-validation')!!}</div>

                            </div>
                            <div class="form-group">
                                <input type="text" name="surname" class="form-control assign_class" id="signUpSurname" placeholder="{!! t('home-request-sign-up-surname-placeholder')!!}"   data-id = "home-request-sign-up-surname-placeholder" data-value = "{{t('home-request-sign-up-surname-placeholder')}}" data-inputplaceholder="yes"/>
                                <div id="signUpSurnameValidation" style="color: red" class="assign_class d-none validation_text" data-id = "home-request-sign-up-surname-validation" data-value = "{{t('home-request-sign-up-surname-validation')}}" >{!! t('home-request-sign-up-surname-validation')!!}</div>

                            </div>
                            <div class="form-group">
                                <input type="text" name="email" class="form-control assign_class" id="signUpEmail" placeholder="{!! t('home-request-sign-up-email-placeholder')!!}"   data-id = "home-request-sign-up-email-placeholder" data-value = "{{t('home-request-sign-up-email-placeholder')}}" data-inputplaceholder="yes"/>
                                <div id="signUpEmailValidation" style="color: red" class="assign_class d-none validation_text" data-id = "home-request-sign-up-email-validation" data-value = "{{t('home-request-sign-up-email-validation')}}" >{!! t('home-request-sign-up-email-validation')!!}</div>
                                <div class="card-title text-center assign_class assign_class validation_text d-none" id="user_exist_msg" style="margin: 20px;color: red"  data-id = "home-request-sign-up-user-exist-msg" data-value = "{{t('home-request-sign-up-user-exist-msg')}}" >{!! t('home-request-sign-up-user-exist-msg') !!}</div>

                            </div>

                            <div class="form-group">
                                <input type="password" name="password" class="form-control assign_class" id="signUpPassword" placeholder="{!! t('home-request-sign-up-password-placeholder')!!}" data-id = "home-request-sign-up-password-placeholder" data-value = "{{t('home-request-sign-up-password-placeholder')}}" data-inputplaceholder="yes"/>
                                <div id="signUpPasswordValidation" style="color: red" class="assign_class d-none validation_text" data-id = "home-request-sign-up-password-validation" data-value = "{{t('home-request-sign-up-password-validation')}}" >{!! t('home-request-sign-up-password-validation')!!}</div>

                            </div>
                            <div class="form-group">
                                <input type="password" name="password_confirmation" class="form-control assign_class" id="signUpConfirmPassword" placeholder="{!! t('home-request-sign-up-confirm-password-placeholder')!!}" data-id = "home-request-sign-up-confirm-password-placeholder" data-value = "{{t('home-request-sign-up-confirm-password-placeholder')}}" data-inputplaceholder="yes"/>
                                <div id="signUpConfirmPasswordValidation" style="color: red" class="assign_class d-none validation_text" data-id = "home-request-sign-up-confirm-password-validation" data-value = "{{t('home-request-sign-up-confirm-password-validation')}}" >{!! t('home-request-sign-up-confirm-password-validation')!!}</div>

                            </div>
                            <div class="form-group">
                                <input type="text" name="phone_number" class="form-control assign_class" id="signUpPhoneNumber" placeholder="{!! t('home-request-sign-up-phone-number-placeholder')!!}"   data-id = "home-request-sign-up-phone-number-placeholder" data-value = "{{t('home-request-sign-up-phone-number-placeholder')}}" data-inputplaceholder="yes"/>
                                <div id="signUpPhoneNumberValidation" style="color: red" class="assign_class d-none validation_text" data-id = "home-request-sign-up-phone-number-validation" data-value = "{{t('home-request-sign-up-phone-number-validation')}}" >{!! t('home-request-sign-up-phone-number-validation')!!}</div>

                            </div>

                            <div class="form-group text-center">
                                <button type="button" class="green-button button_text assign_class" style="margin: 20px;padding: 7px 70px;" id="signUp-button" data-id = "home-request-sign-up-button" data-value = "{{t('home-request-sign-up-button')}}" >{!! t('home-request-sign-up-button')!!}</button>
                            </div>

                        </div>
                        <div class="tab-pane" id="tab3">

                            <h2 class="card-title text-center assign_class assign_class text-blue" style="margin: 20px"  data-id = "home-request-forget-password-title" data-value = "{{t('home-request-forget-password-title')}}" >{!! t('home-request-forget-password-title') !!}</h2>
                            <p class="card-title text-center assign_class assign_class" style="margin: 20px"  data-id = "home-request-forget-password-sub-title-msg" data-value = "{{t('home-request-forget-password-sub-title-msg')}}" >{!! t('home-request-forget-password-sub-title-msg') !!}</p>


                            <form class="form-horizontal" id="ResetPassForm" role="form" method="POST" action="{{ route('password.email') }}">

                                <div class="form-group">
                                    <input type="email" name="email" id="passwordResetEmail" class="form-control assign_class" value="{{ old('email') }}" required autofocus id="signUpEmail" placeholder="{!! t('home-request-forget-password-email-placeholder')!!}"   data-id = "home-request-forget-password-email-placeholder" data-value = "{{t('home-request-forget-password-email-placeholder')}}" data-inputplaceholder="yes"/>
                                    <div id="passwordResetEmailValidation" style="color: red" class="assign_class d-none validation_text" data-id = "home-request-forget-password-email-validation" data-value = "{{t('home-request-forget-password-email-validation')}}" >{!! t('home-request-forget-password-email-validation')!!}</div>
                                    <div id="emailExistValidation" style="color: red" class="assign_class d-none validation_text" data-id = "home-request-forget-password-email-exist-validation" data-value = "{{t('home-request-forget-password-email-exist-validation')}}" >{!! t('home-request-forget-password-email-exist-validation')!!}</div>
                                    <div id="emailSubmitValidation" style="color: red" class="assign_class d-none validation_text" data-id = "home-request-forget-password-send-email-validation" data-value = "{{t('home-request-forget-password-send-email-validation')}}" >{!! t('home-request-forget-password-send-email-validation')!!}</div>

                                </div>
                                {{--<input type="email" class="form-control b_r_20" id="email" name="email" value="{{ old('email') }}" required autofocus>--}}
                                @if ($errors->has('email'))
                                    <span class="help-block" style="color: white">
                                                                               <strong>{{ $errors->first('email') }}</strong>
                                                                            </span>
                                @endif
                                <div class="form-group text-center">
                                    <button type="button" class="green-button button_text assign_class" style="margin: 20px;padding: 7px 70px;" id="password_reset_button" data-id = "home-request-forget-password-button" data-value = "{{t('home-request-forget-password-button')}}" >{!! t('home-request-forget-password-button')!!}</button>
                                </div>

                            </form>

                        </div>
                    </div>

                </div>
            </div>

        </div>

        <span class="assign_class d-none login_msg" id="login_msg" data-id = "school-term-login-user-msg" data-placeholders="{{$login_user_msg_placeholders}}" data-placeholderval="{{$user_name}},{{$user_surname}},{{$user_email}},logout" data-value = "{{t('school-term-login-user-msg')}}"  >{!!  str_replace(explode(',', $login_user_msg_placeholders),[$user_name,$user_surname,$user_email,'logout'],t('school-term-login-user-msg'))  !!}</span>
        <br>
        <div class="text-right">
            <input type="hidden" id="csrf_token" value="<?php echo csrf_token(); ?>">
            <button  type="Button" id="logout_btn" class="green-button assign_class d-none login_msg" style="margin: 10px;background-color: #BD2130;padding: 7px 30px" data-id = "school-term-log-out-button" data-value = "{{t('school-term-log-out-button')}}">{!! t('school-term-log-out-button')!!}</button>
        </div>
        <br>
        <br>


    </div>
</div>