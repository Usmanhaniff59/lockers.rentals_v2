@extends('frontend.layouts.apphome')
@section('style')
    <link rel="stylesheet" href="/frontend/vendors/bootstrapvalidator/css/bootstrapValidator.min.css"/>
    <!--page level styles-->
    <link rel="stylesheet" href="/frontend/vendors/swiper/css/swiper.min.css" />
    <!--Page level styles-->
    <link rel="stylesheet" href="/frontend/css/pages/general_components.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />

    <style>
        .form-title{
            color: #b30510;
        }
    </style>
@endsection
@section('content')
    <main id="main">
        <section id="about">
            <div class="container">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="file" name="file" class="form-control">
                            <br>
                            <button class="btn btn-success">Import User Data</button>
                            <a class="btn btn-warning" href="{{ route('export') }}">Export User Data</a>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection