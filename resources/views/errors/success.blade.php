<div class="col-md-12">
    <div class="alert alert-success alert-dismissable float-right print-success-message d-none" style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
        <button type="button" class="close" data-dismiss="alert"
                aria-hidden="true" style="margin-top: 4px; margin-left: 15px;"> ×
        </button>
        <span class="success-message"></span>

    </div>
</div>