<div class="col-md-12">
    <div class="alert alert-danger alert-dismissable print-error-message float-right d-none" style="padding: 0.25rem 1.25rem; margin-bottom: 0rem;">
        <button type="button" class="close" data-dismiss="alert"
                aria-hidden="true" style="margin-top: 4px; margin-left: 15px;"> ×
        </button>
        <span class="error-message"></span>

    </div>
</div>