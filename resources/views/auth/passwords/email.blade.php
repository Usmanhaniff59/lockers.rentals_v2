@extends('backend.layouts.app-signin')
@section('title', 'Lockers | Forgot')

@section('header-content')
    <a href="#." class="text-success m-r-20 font_18">Forgot Password</a>
@stop
@section('content')
    <div class="m-t-15">
        <form class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}
            <div class="radio disabled  reset-radio-section pl-3">
                <label class="text-warning">
                    <input type="radio"  id="email_radio" name="reset-radio"  checked >
                    <span class="cr"><i class="cr-icon fa fa-circle"></i></span>

                    <span style="color: black;">Email</span>
                </label>
            </div>
            <div class="radio disabled  reset-radio-section pl-3">
                <label class="text-warning">
                    <input type="radio"   id="sms_radio" name="reset-radio"  >
                    <span class="cr"><i class="cr-icon fa fa-circle"></i></span>
                    <span style="color: black;">SMS</span>
                </label>
            </div>
            <div class="email-section">
                <label for="email" class="col-form-label custom-control-description"> E-mail Address</label>
                <input type="email" class="form-control b_r_20" id="email" name="email" value="{{ old('email') }}" required autofocus>
                @if ($errors->has('email'))
                    <span class="help-block" style="color: white">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
                <div class="assign_class custom-control-description email-section" data-id = "password-reset-text"   data-value = "{{t('password-reset-text')}}" data-text="{!! t('password-reset-text') !!}">{!! t('password-reset-text') !!}</div>

            </div>
            <div class="sms-section d-none">
                <label for="phone_number" class="col-form-label custom-control-description"> Phone Number</label>
                <input type="text" class="form-control b_r_20" id="phone_number"  value="{{ old('phone_number') }}"  >
                <span class="help-block d-none" id="reset_sms_validation_not_found"  style="color: red">
                        <strong>Phone Number does not match our Records</strong>
                    </span>

            </div>

            <input type="hidden"  id="reset_user_id">
            <div class="passcode-section d-none">
                <label for="phone_number" class="col-form-label custom-control-description"> Passcode</label>
                <input type="text" class="form-control b_r_20" id="reset_passcode_for_confirmation"  placeholder="Enter Your Code"  >
                <span class="help-block"  style="color: red">
                        <strong id="reset_passcode_validation_not_found" class="d-none">Code does not match our Records</strong>
                    </span>
                {{--<div id="reset_passcode_validation_not_found" style="color: red" class="assign_class d-none" >Code does not match our Records</div>--}}
                <p id="sms_confirm_msg" class="d-none assign_class"   data-id = "home-sms-confirmation-msg" data-value = "{{t('home-sms-confirmation-msg')}}" > {!! t('home-sms-confirmation-msg')!!}</p>

            </div>

            <div class="text-center login_bottom email-section">
                <button type="submit" class="btn btn-mint btn-block b_r_20 m-t-10 m-r-20">Send Password Reset Link</button>
            </div>

        </form>

        <br>
        <div class="text-center login_bottom sms-section d-none">
            <button type="submit" class="btn btn-mint btn-block b_r_20 m-t-10 m-r-20 sms_send_btn">Send SMS</button>
        </div>
        <br>
        <div class="text-center login_bottom passcode-section d-none">
            <button type="submit" class="btn btn-mint btn-block b_r_20 m-t-10 m-r-20 passcode_confirm_btn">Verify Passcode</button>
        </div>
    </div>
@endsection
