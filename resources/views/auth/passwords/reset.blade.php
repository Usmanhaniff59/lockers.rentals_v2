@extends('backend.layouts.app-signin')
@section('title', 'Lockers | Forgot')
@section('header-content')
    <a href="javascript:;" class="text-success m-r-20 font_18">Reset Password</a>
@stop
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form class="form-horizontal" role="form" method="POST" action="{{ route('password.request') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="token" value="{{ $token }}">
                            {{--{{dd(Illuminate\Support\Facades\Session::get('reset_password_email'))}}--}}
                            @if(Illuminate\Support\Facades\Session::get('reset_password_using_phone_number') == true)
                                <input type="hidden"  id="email" name="email" value="{{ Illuminate\Support\Facades\Session::get('reset_password_email') ?? old('email') }}" required autocomplete="email" >

                                <label for="email" class="col-form-label custom-control-description"> Phone Number</label>

                                <input type="text" class="form-control @error('phone_number') is-invalid @enderror" id="phone_number" name="phone_number" value="{{ Illuminate\Support\Facades\Session::get('reset_password_number') ?? old('phone_number') }}" required autocomplete="email" autofocus>
                                {{--<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>--}}
                                {{--@if ($errors->has('phone_number'))--}}
                                    @error('phone_number')
                                        <span class="help-block" style="color: #ff6666">
                                            <strong>{{ $errors->first('phone_number') }}</strong>
                                        </span>
                                    @enderror
                                {{--@endif--}}
                            @else
                                <label for="email" class="col-form-label custom-control-description"> E-mail Address</label>
                                <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                                {{--<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>--}}
                                {{--@if ($errors->has('email'))--}}
                                    @error('email')
                                        <span class="help-block " style="color: #ff6666">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @enderror
                                {{--@endif--}}

                            @endif


                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-form-label text-left custom-control-description">Password</label>
                                <input id="password" type="password" class="form-control b-r-20" name="password" required>
                                {{--@if ($errors->has('password'))--}}
                                @error('password')
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @enderror
                                {{--@endif--}}
                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label for="password-confirm" class="col-form-label text-left custom-control-description">Confirm Password</label>
                                <input id="password-confirm" type="password" class="form-control b-r-20" name="password_confirmation" required>
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-md-offset-4 text-center">
                                    <button type="submit" class="btn btn-primary">
                                        Reset Password
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
