@extends('backend.layouts.app-signin')
@section('title', 'Lockers | Sign Up')
@section('header-content')
    <a href="login" class="text-success m-r-20 font_18">LOG IN</a>
    <!-- <a href="register" class="text-success m-r-20 font_18">SIGN UP</a> -->
@stop
@section('content')
    <div class="m-t-15">
        <form class="form-horizontal" id="register_valid" action="{{ route('register') }}" method="post">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="username" class="col-form-label custom-control-description">Name *</label>
                <input type="text" placeholder="Full Name" class="form-control b_r_20" name="name" id="name" value="{{ old('name') }}" required autofocus>
                @if ($errors->has('name'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-form-label custom-control-description">Email *</label>
                <input type="text" placeholder="Email Address" name="email" id="email" class="form-control b_r_20" value="{{ old('email') }}" required>
                @if ($errors->has('email'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-form-label custom-control-description">Password *</label>
                <input type="password" placeholder="Password" id="password" name="password" class="form-control b_r_20" required>


                @if ($errors->has('password'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="confirmpassword" class="col-form-label custom-control-description">Confirm Password *</label>
                <input type="password" placeholder="Confirm Password" name="password_confirmation" id="password-confirm" class="form-control b_r_20" required>

            </div>

            <div class="form-group row">
                <div class="col-6">
                    <button type="submit" class="btn btn-block btn-primary login_button b_r_20">Submit</button>
                </div>
                <div class="col-6">
                    <button type="reset" class="btn btn-block btn-danger b_r_20">Reset</button>
                </div>

            </div>
        </form>
    </div>

@endsection
