@extends('backend.layouts.app-signin')
@section('title', 'Lockers | Login')
@section('header-content')
    <!-- <a href="login" class="text-success m-r-20 font_18">LOG IN</a> -->
    <a href="register" class="text-success m-r-20 font_18">SIGN UP</a>
@stop
@section('content')

    <div class="m-t-15">
        <form action="{{ route('login') }}" id="login_validator" method="post">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-form-label custom-control-description"> E-mail</label>
                <input type="email" class="form-control b_r_20" id="email" name="email" value="{{ old('email') }}" required autofocus>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-form-label custom-control-description">Password</label>
                <input type="password" class="form-control b_r_20" id="password" name="password">
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="row m-t-15">
                <div class="col-12">
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input form-control" name="remember" {{ old('remember') ? 'checked' : '' }}>
                        <span class="custom-control-label"></span>
                        <a class="custom-control-description text-white">Keep me logged in</a>
                    </label>
                    {{--<div class="assign_class p-b-20 custom-control-description" data-id = "ofline-login-form-text" data-value = "{{t('ofline-login-form-text')}}" style="text-align: center;color: white;">--}}
                        {{--{!! t('ofline-login-form-text')!!}--}}
                    {{--</div>--}}
                </div>
            </div>
            <div class="text-center login_bottom">
                <button type="submit" class="btn btn-mint btn-block b_r_20 m-t-10 m-r-20">LOG IN</button>
            </div>
            <div class="m-t-15 text-center">
                <a href="{{ route('password.request') }}" class="custom-control-description">Forgot password ?</a>
            </div>
        </form>
    </div>
@endsection
