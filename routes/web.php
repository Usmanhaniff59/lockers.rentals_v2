<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use App\Locker;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

//

//Route::get('/logerror', function(){
//    Log::listen(function($level, $message, $context)
//    {
//        dd($message); // fail
//    });
////   dd( Log::error('fail'));
//});
Route::group(['middleware' => ['auth']], function () {

    Route::get('script/fixCode/{id}', 'Backend\AdminController@fixCodes')->name('fixCodes');

    Route::get('/user/checkCredits', 'Frontend\LockersController@checkSms');

    Route::post('admin/changeSalePrice', 'Backend\paymentController@changeSalePrice')->name('changeSalePrice');

    Route::get('user/getSaveAddress', 'Frontend\AddressController@getUserAddress')->name('getSaveAddress');
    Route::post('user/saveUserAddress', 'Frontend\AddressController@saveUserAddress')->name('saveUserAddress');

    Route::resource('admin/emailtemplates', 'Backend\EmailTemplateController')->middleware('EmailTemplateManagement');
    Route::post('admin/getemailtemplate', 'Backend\EmailTemplateController@getemailtemplate')->name('getemailtemplate');
    Route::post('admin/updateemailtemplate', 'Backend\EmailTemplateController@updateemailtemplate')->name('updateemailtemplate');

    Route::get('/admin', 'Backend\AdminController@index');

    Route::resource('admin/pages', 'Backend\PageController');


    //users
    Route::group(['prefix' => 'admin/users', 'as' => 'users.' ,'middleware' => ['UserManagment']], function () {
        Route::get('', 'Backend\UserController@index')->name('index');
        Route::post('store', 'Backend\UserController@store')->name('store');
        Route::put('update/{id}', 'Backend\UserController@update')->name('update');
        Route::delete('destroy/{id}', 'Backend\UserController@destroy')->name('destroy');
        Route::get('show/all', 'Backend\UserController@show_all')->name('show.all');

    });
     Route::post('admin/userCompanyFranchise', 'Backend\UserController@companyFranchise')->name('user.compnay.companyFranchise');
    Route::post('admin/getuserroles', 'Backend\UserController@getuserroles')->name('getuserroles');
    Route::post('admin/companygetuserroles', 'Backend\Company\CompanyUserController@getuserroles')->name('compnay.getuserroles');

     Route::post('admin/companyFranchise', 'Backend\Company\CompanyUserController@companyFranchise')->name('compnay.companyFranchise');

    Route::get('admin/editprofile', 'Backend\UserController@editprofile');
    Route::post('admin/saveprofile', 'Backend\UserController@saveprofile')->name('saveprofile');

    //roles
    Route::resource('admin/roles', 'Backend\RoleController')->middleware('RollManagement');
    Route::post('admin/getroles', 'Backend\RoleController@getroles')->name('getroles');

    // BlockGroup
    Route::group(['prefix' => 'admin/block-group', 'as' => 'blockgroup.','middleware' => ['BlockGroup']], function () {
         Route::get('', 'Backend\BlockgroupController@index')->name('index');
         Route::post('', 'Backend\BlockgroupController@store')->name('store');
       Route::post('show/all', 'Backend\BlockgroupController@show_all')->name('show.all');
       Route::post('update/sort', 'Backend\BlockgroupController@updateOrder')->name('update.order');
       Route::post('update/status', 'Backend\BlockgroupController@updateStatus')->name('update.status');
       Route::post('update/name', 'Backend\BlockgroupController@updateName')->name('update.name');
       Route::post('update/delete', 'Backend\BlockgroupController@deleteBlock')->name('update.deleteBlock');




    });
   
    // locker Description
    Route::group(['prefix' => 'admin/locker-description', 'as' => 'locker_description.','middleware' => ['LockerDescription'] ], function () {
         Route::get('', 'Backend\LockerDescription@index')->name('index');
         Route::post('', 'Backend\LockerDescription@store')->name('store');
       Route::post('show/all', 'Backend\LockerDescription@show_all')->name('show.all');
       // Route::post('update/sort', 'Backend\LockerDescription@updateOrder')->name('update.order');
       Route::post('update/status', 'Backend\LockerDescription@updateStatus')->name('update.status');
       Route::post('update/delete', 'Backend\LockerDescription@deleteBlock')->name('update.deleteBlock');
       Route::post('update/active', 'Backend\LockerDescription@updateActive')->name('update.active');




    });

    //Companies
    Route::group(['prefix' => 'admin/company', 'as' => 'company.','middleware' => ['BlockLinkforUser']], function () {
        Route::get('', 'Backend\CompanyController@index')->name('index');
        Route::get('create', 'Backend\CompanyController@create')->name('create');
        Route::post('store', 'Backend\CompanyController@store')->name('store');
        Route::put('update/{id}', 'Backend\CompanyController@update')->name('update');
        Route::delete('destroy/{id}', 'Backend\CompanyController@destroy')->name('destroy');
        Route::get('show/all', 'Backend\CompanyController@show_all')->name('show.all');
        Route::get('show/deleted', 'Backend\CompanyController@show_deleted')->name('show.deleted');
        Route::get('show/{id}', 'Backend\CompanyController@show')->name('show');
        Route::get('info', 'Backend\CompanyController@info')->name('info');

    });


    // Current Locker Offsale
    Route::group(['prefix' => 'admin/reports/currentlocker', 'as' => 'company.currentlocker.','middleware' => ['MaintanceOffsaleReport']], function () {
        Route::get('index', 'Backend\CurrentMaintenaceController@index');
        Route::get('current_show', 'Backend\CurrentMaintenaceController@currentlocker_show');
        Route::post('/count', 'Backend\CurrentMaintenaceController@offsaleCount')->name('offsaleCount');
        Route::post('get/data', 'Backend\CurrentMaintenaceController@getData')->name('getData');
    });

    //Company Document routes
    Route::group(['prefix' => 'admin/company/documents', 'as' => 'company.documents.'], function () {
        Route::get('', 'Backend\CompanyDocumentController@index')->name('index');
        Route::get('show/all', 'Backend\CompanyDocumentController@show_all')->name('show.all');
        Route::get('download/{id}', 'Backend\CompanyDocumentController@download')->name('download');
        Route::post('upload', 'Backend\CompanyDocumentController@upload')->name('storefile');
        Route::post('update', 'Backend\CompanyDocumentController@update')->name('update');
        Route::post('show', 'Backend\CompanyDocumentController@show')->name('show');
        Route::get('destroy/{id}', 'Backend\CompanyDocumentController@destroy')->name('delete');
    });
    //Document routes
    Route::group(['prefix' => 'admin/documents', 'as' => 'documents.','middleware' => ['DocumentManagment']], function () {
        Route::get('', 'Backend\DocumentController@index')->name('index');
        Route::get('show/all', 'Backend\DocumentController@show_all')->name('show.all');
        Route::get('download/{id}', 'Backend\DocumentController@download')->name('download');
        Route::post('upload', 'Backend\DocumentController@upload')->name('store');
        Route::post('update', 'Backend\DocumentController@update')->name('update');
        Route::post('show', 'Backend\DocumentController@show')->name('show');


    });
    // Company Outer Blocks Routs
    Route::group(['prefix' => 'admin/company/outer/blocks', 'as' => 'company.outer.blocks.'], function () {
        Route::get('', 'Backend\OuterBlockController@index')->name('index');
        Route::get('create', 'Backend\OuterBlockController@create')->name('create');
        Route::delete('destroy/{id}', 'Backend\OuterBlockController@destroy')->name('destroy');
        Route::get('show/all', 'Backend\OuterBlockController@show_all')->name('show.all');
        Route::get('show/delete', 'Backend\OuterBlockController@show_deleted')->name('show.delete');
        Route::get('show/{id}', 'Backend\OuterBlockController@show')->name('show');
    });


    // Company Outer Locker Blocks
    Route::group(['prefix' => 'admin/company/locker/blocks/images', 'as' => 'company.locker.blocks.images.' ,'middleware' => ['LockerBlockImages']], function () {
        Route::get('', 'Backend\Company\LockerBlockImagesController@index')->name('index');
        Route::get('show/all', 'Backend\Company\LockerBlockImagesController@show_all')->name('show.all');
        Route::post('upload', 'Backend\Company\LockerBlockImagesController@upload')->name('upload');
        Route::post('/update', 'Backend\Company\LockerBlockImagesController@update')->name('update');
        Route::post('/editabledata', 'Backend\Company\LockerBlockImagesController@editabledata')->name('editabledata');

    });

    //company blocks in Company
    Route::get('blocks/getUpdated', 'Backend\CompanyBlockController@getUpdated');
    Route::any('blocks/update/{id}', 'Backend\CompanyBlockController@update');
    Route::any('blocksouter/update/{id}', 'Backend\OuterBlockController@update');
    Route::post('blocks/store', 'Backend\CompanyBlockController@store')->name('store');
    Route::post('blocks/tiers', 'Backend\CompanyBlockController@findTier');

     Route::post('status/go_live', 'Backend\CompanyController@addLiveStatus');

    Route::get('getSchoolYear', 'Backend\CompanyBlockController@getSchoolYear')->name('getSchoolYear');
    Route::get('updatetheSchoolyearCheck', 'Backend\CompanyBlockController@updateSchoolYears')->name('updatetheSchoolyearCheck');


    Route::get('getBlockImages', 'Backend\CompanyBlockController@getBlockImages')->name('getBlockImages');
    Route::get('getBlockDetails/{id}', 'Backend\CompanyBlockController@getBlockDetails')->name('getBlockDetails');
    Route::post('getBlockDetailsouter/', 'Backend\OuterBlockController@getBlockDetails')->name('getBlockDetails');
    Route::post('admin/getcompany', 'Backend\CompanyController@getcompany')->name('getcompany');
    Route::post('admin/updatecompany', 'Backend\CompanyController@updatecompany')->name('updatecompany');
    Route::get('/Backend/changeLockerOrder', 'Backend\CompanyController@changeLockerOrder');

    //get lockers after selected block in dropdown
    Route::get('/admin/backend/getlockersblock', 'Backend\CompanyController@getlockersBlock')->name('getlockersblock');

    Route::post('/admin/backend/posteditlocker', 'Backend\CompanyController@posteditlocker')->name('posteditlocker');
    Route::post('/admin/backend/maintenancelocker', 'Backend\CompanyController@maintenancelocker')->name('maintenancelocker');
    Route::post('/admin/backend/onOffSale', 'Backend\CompanyController@onOffSale')->name('onOffSale');
    Route::post('/admin/backend/moveWithInBlock', 'Backend\CompanyController@moveWithInBlock')->name('moveWithInBlock');
    Route::post('/admin/backend/moveOutSideBlock', 'Backend\CompanyController@moveOutSideBlock')->name('moveOutSideBlock');
    Route::post('/admin/backend/add/lockers/column', 'Backend\CompanyController@addLockerColumn')->name('addLockerColumn');


    Route::group(['prefix' => 'admin/reserved/lockers', 'as' => 'reserved.lockers.' ,'middleware' => ['OrderManagment']], function () {
        Route::get('', 'Backend\ReservedLockersController@index')->name('index');
        Route::get('create', 'Backend\ReservedLockersController@create')->name('create');
        Route::post('store', 'Backend\ReservedLockersController@store')->name('store');
        Route::put('update/{id}', 'Backend\ReservedLockersController@update')->name('update');
        Route::get('{id}/edit', 'Backend\ReservedLockersController@edit')->name('edit');
        Route::delete('destroy/{id}', 'Backend\ReservedLockersController@destroy')->name('destroy');
        Route::get('show/all', 'Backend\ReservedLockersController@show_all')->name('show.all');
        Route::get('show/all/search', 'Backend\ReservedLockersController@show_all_search')->name('show.all.search');
        Route::get('delete/all', 'Backend\ReservedLockersController@delete_all')->name('delete.all');
        Route::get('show/{id}', 'Backend\ReservedLockersController@show')->name('show');
        Route::get('schooltermbasedreserved/{id}', 'Backend\ReservedLockersController@schooltermreserved')->name('schooltermbasedreserved');
        Route::post('dateChangeLostLocker', 'Backend\ReservedLockersController@dateChangeLostLocker')->name('dateChangeLostLocker');
        Route::post('check/same/school/year', 'Backend\ReservedLockersController@checkSameSchoolYear')->name('checkSameSchoolYear');
        Route::post('restore/booking', 'Backend\ReservedLockersController@restoreBooking')->name('restoreBooking');

    });


    Route::post('admin/get_sales_count', 'Backend\RoleController@get_sales_count')->name('get_sales_count');
    Route::post('admin/add_in_sales_autdit', 'Backend\RoleController@add_in_sales_autdit')->name('add_in_sales_autdit');


    Route::resource('admin/permissions', 'Backend\PermissionController')->middleware('PermissionManagement');

    Route::resource('admin/sliders', 'Backend\SlidersController')->middleware('SliderManagement');


    Route::group(['prefix' => 'admin/locker/codes', 'as' => 'locker.codes.'], function () {
        Route::get('', 'Backend\ManualLockerCodeController@index')->name('index');

        Route::get('show/all', 'Backend\ManualLockerCodeController@show_all')->name('show.all');


        // delete the codes
        Route::get('delete/all', 'Backend\ManualLockerCodeController@deleteAllcodes');
        Route::get('delete/duplicates', 'Backend\ManualLockerCodeController@deleteduplicates');

        Route::post('excel/export', 'Backend\ManualLockerCodeController@codes_excel')->name('excel.export');
        Route::post('get/locker/code', 'Backend\ManualLockerCodeController@getLockerCode')->name('get.locker.code');
        Route::post('update/code', 'Backend\ManualLockerCodeController@updateCode')->name('update.code');

    });

    //Report Booked Route for Admin Reports
    Route::group(['prefix' => 'admin/reports', 'as' => 'reports.'], function () {
        Route::get('sales/', 'Backend\ReportSales@index')->name('sales')->middleware('SalesReport');
        Route::get('show/all/sales', 'Backend\ReportSales@show_all')->name('show.all.sales');
        Route::any('show/all/availability', 'Backend\ReportAvailability@show_all')->name('show.all.availability');
        Route::get('availability/', 'Backend\ReportAvailability@index')->name('availability')->middleware('AvailabilityReport');

      
        Route::any('show/all/booked', 'Backend\ReportBooked@show_all')->name('show.all.booked');
        Route::get('booked/', 'Backend\ReportBooked@index')->name('booked')->middleware('BookingReport');

        Route::post('availability/excel/export', 'Backend\ReportAvailability@availability_excel')->name('availability.excel.export');
        Route::post('availability', 'Backend\ReportAvailability@getavailabilityreport')->name('getavailabilityreport');
        Route::post('availability/count', 'Backend\ReportAvailability@getavailabilityreportcount')->name('getavailabilitybookedreportcount');

        Route::post('booked/excel/export', 'Backend\ReportBooked@booked_excel')->name('booked.excel.export');
        Route::post('booked/report', 'Backend\ReportBooked@getbookedreport')->name('getbookedreport');
        Route::post('booked/report/count', 'Backend\ReportBooked@getbookedreportcount')->name('getbookedreportcount');


          // locker number report start
        Route::get('locker-numbers', 'Backend\Reports\LockerNumberReportController@index')->name('locker.numbers')->middleware('LockerNumberReport');
         Route::post('show/all/lockernumbers', 'Backend\Reports\LockerNumberReportController@show_all')->name('show.all.locker.numbers');
          Route::post('lockernumbers/count', 'Backend\Reports\LockerNumberReportController@getavailabilityreportcount')->name('lockernumbers.getavailabilitybookedreportcount');

          Route::POST('lockernumbers_excelFile/', 'Backend\Reports\LockerNumberReportController@getavailabilityreport')->name('lockernumber.excelFile');
        // locker number report end

    });

    Route::group(['prefix' => 'academic/year/report', 'as' => 'academic.year.report.','middleware' => ['AcademicYearReport']], function () {
        Route::get('', 'Backend\AcademicYearController@index')->name('index');
        Route::get('show/all', 'Backend\AcademicYearController@show_all')->name('show.all');

        Route::post('booked/excel/export', 'Backend\AcademicYearController@booked_excel')->name('booked.excel.export');
        Route::post('/data', 'Backend\AcademicYearController@get_data')->name('data');
        Route::post('/count', 'Backend\AcademicYearController@get_count')->name('count');

    });

    //VoucherPaymentsController
    Route::group(['prefix' => 'voucher/payment/report', 'as' => 'voucher.payment.report.','middleware' => ['VoucherPaymentsReport']], function () {
        Route::get('', 'Backend\Reports\VoucherPaymentsReportController@index')->name('index');
        Route::get('show/all', 'Backend\Reports\VoucherPaymentsReportController@show_all')->name('show.all');
        Route::post('/data', 'Backend\Reports\VoucherPaymentsReportController@get_data')->name('data');
        Route::post('/count', 'Backend\Reports\VoucherPaymentsReportController@get_count')->name('count');

    });
    //Charity Report Controller
    Route::group(['prefix' => 'charity/report', 'as' => 'charity.report.','middleware' => ['CharityReport']], function () {
        Route::get('', 'Backend\Reports\CharityReportController@index')->name('index');
        Route::get('show/all', 'Backend\Reports\CharityReportController@show_all')->name('show.all');
        Route::post('/data', 'Backend\Reports\CharityReportController@get_data')->name('data');
        Route::post('/count', 'Backend\Reports\CharityReportController@get_count')->name('count');
        Route::post('/update/checkbox', 'Backend\Reports\CharityReportController@update')->name('update');


    });
    //Pupil Premium Report Controller
    Route::group(['prefix' => 'pupil/premium/report', 'as' => 'pupil.premium.report.','middleware' => ['PupilPremium']], function () {
        Route::get('', 'Backend\Reports\PupilPremiumReportController@index')->name('index');
        Route::get('show/all', 'Backend\Reports\PupilPremiumReportController@show_all')->name('show.all');
        Route::post('/data', 'Backend\Reports\PupilPremiumReportController@get_data')->name('data');
        Route::post('/count', 'Backend\Reports\PupilPremiumReportController@get_count')->name('count');

    });
    //CompanyBookingReportController
    Route::group(['prefix' => 'company/booking/report', 'as' => 'company.booking.report.','middleware' => ['CompanyBookingReport']], function () {
        Route::get('', 'Backend\Reports\CompanyBookingsReportController@index')
            ->name('index');
        Route::get('show/all', 'Backend\Reports\CompanyBookingsReportController@show_all')
            ->name('show.all');
        Route::post('/data', 'Backend\Reports\CompanyBookingsReportController@get_data')
            ->name('data');
        Route::post('/count', 'Backend\Reports\CompanyBookingsReportController@get_count')
            ->name('count');
    });
    //lost booking
    Route::group(['prefix' => 'admin/lost/booking', 'as' => 'lost.booking.'], function () {
        Route::get('', 'Backend\LostBookingController@index')->name('index');
        Route::get('create', 'Backend\LostBookingController@create')->name('create');
        Route::post('store', 'Backend\LostBookingController@store')->name('store');
        Route::put('update/{id}', 'Backend\LostBookingController@update')->name('update');
        Route::delete('destroy/{id}', 'Backend\LostBookingController@destroy')->name('destroy');
        Route::get('show/all', 'Backend\LostBookingController@show_all')->name('show.all');
    });
    // Company Blocks Routs
    Route::group(['prefix' => 'admin/company/blocks', 'as' => 'company.blocks.'], function () {
        Route::get('', 'Backend\CompanyBlockController@index')->name('index');
        Route::get('create', 'Backend\CompanyBlockController@create')->name('create');
        Route::get('destroy/{id}', 'Backend\CompanyBlockController@destroy')->name('destroy');
        Route::get('show/all', 'Backend\CompanyBlockController@show_all')->name('show.all');
        Route::get('show/delete', 'Backend\CompanyBlockController@show_deleted')->name('show.delete');
        Route::get('show/{id}', 'Backend\CompanyBlockController@show')->name('show');
    });

    // Company chat routes
    Route::group(['prefix' => 'admin/company/chats', 'as' => 'company.chats.'], function () {
        Route::get('', 'Backend\CompanyChatController@index')->name('index');
        Route::get('show/all', 'Backend\CompanyChatController@show_all')->name('show.all');
        Route::post('store', 'Backend\CompanyChatController@store')->name('store');
        Route::get('excel/report', 'Backend\CompanyChatController@chatreport')->name('chatreport');
        Route::get('chatajax', 'Backend\CompanyChatController@chatreportshow');
        Route::post('/count', 'Backend\CompanyChatController@chatCount')->name('chatCount');
        Route::post('get/data', 'Backend\CompanyChatController@getData')->name('getData');
        Route::post('view', 'Backend\CompanyChatController@view')->name('view');

    });
    // Company Vouchers routes
    Route::group(['prefix' => 'admin/company/vouchers', 'as' => 'company.vouchers.'], function () {
        Route::get('', 'Backend\Company\VoucherCodesController@index')->name('index');
        Route::get('show/all', 'Backend\Company\VoucherCodesController@show_all')->name('show.all');
        Route::post('store', 'Backend\Company\VoucherCodesController@store')->name('store');
        Route::get('excel/report', 'Backend\Company\VoucherCodesController@chatreport')->name('chatreport');
        Route::post('send/voucher/code/status', 'Backend\Company\VoucherCodesController@send_voucher_code');
        Route::post('updateIssuedStatus', 'Backend\Company\VoucherCodesController@updateIssuedStatus');
        Route::post('assignCode', 'Backend\Company\VoucherCodesController@assignCode');
        Route::post('/count', 'Backend\Company\VoucherCodesController@chatCount')->name('chatCount');
        Route::post('get/data', 'Backend\Company\VoucherCodesController@getData')->name('getData');
        Route::post('showMessages', 'Backend\Company\VoucherCodesController@showMessages')->name('showMessages');


    });

    // Assign Users to Company
      Route::group(['prefix' => 'admin/company/users', 'as' => 'company.users.'], function () {
        // Route::get('', 'Backend\Company\PupilPremiumController@index')->name('index');
        Route::get('show/all', 'Backend\Company\CompanyUserController@show_all')->name('show.all');
        Route::put('update', 'Backend\Company\CompanyUserController@update')->name('update');
        Route::post('store', 'Backend\Company\CompanyUserController@store')->name('store');
        // Route::post('delete', 'Backend\Company\CompanyUserController@store')->name('store');
        Route::post('destroy', 'Backend\Company\CompanyUserController@delete')->name('destroy');

        // Route::post('store', 'Backend\Company\PupilPremiumController@store')->name('store');
        // Route::post('get/blocks', 'Backend\Company\PupilPremiumController@get_blocks');
        // Route::post('get/franchise', 'Backend\Company\PupilPremiumController@get_franchise');
        // Route::post('assignCode', 'Backend\Company\PupilPremiumController@assignCode');
        // Route::post('/count', 'Backend\Company\PupilPremiumController@chatCount')->name('chatCount');
        // Route::post('get/data', 'Backend\Company\PupilPremiumController@getData')->name('getData');
        // Route::post('showMessages', 'Backend\Company\PupilPremiumController@showMessages')->name('showMessages');
        // Route::post('updateName', 'Backend\Company\PupilPremiumController@updateName')->name('updateName');


    });

    // Company Pupil Premium routes
    Route::group(['prefix' => 'admin/company/pupil/premium', 'as' => 'company.pupil.premium.'], function () {
        Route::get('', 'Backend\Company\PupilPremiumController@index')->name('index');
        Route::get('show/all', 'Backend\Company\PupilPremiumController@show_all')->name('show.all');
        Route::post('store', 'Backend\Company\PupilPremiumController@store')->name('store');
        Route::post('get/blocks', 'Backend\Company\PupilPremiumController@get_blocks');
        Route::post('get/franchise', 'Backend\Company\PupilPremiumController@get_franchise');
        Route::post('assignCode', 'Backend\Company\PupilPremiumController@assignCode');
        Route::post('/count', 'Backend\Company\PupilPremiumController@chatCount')->name('chatCount');
        Route::post('get/data', 'Backend\Company\PupilPremiumController@getData')->name('getData');
        Route::post('showMessages', 'Backend\Company\PupilPremiumController@showMessages')->name('showMessages');
        Route::post('updateName', 'Backend\Company\PupilPremiumController@updateName')->name('updateName');


    });

    // Franchisee Report
    Route::group(['prefix' => 'admin/company/franchisee', 'as' => 'company.franchisee.','middleware' => ['FranchiseReport']], function () {
        Route::get('franchisee_index', 'Backend\FranchiseeReportController@franchisee_index');
        Route::post('get/data', 'Backend\FranchiseeReportController@getData')->name('getData');
        Route::get('franchisee/ajax', 'Backend\FranchiseeReportController@query_data');
        Route::post('/count', 'Backend\FranchiseeReportController@franchiseeCount')->name('franchiseeCount');
        Route::get('franchisee_show', 'Backend\FranchiseeReportController@franshisee_show');
    });

    // Offsale Report
    Route::group(['prefix' => 'admin/reports/offsale', 'as' => 'company.offsale.'], function () {
        Route::get('index', 'Backend\OffsaleReportController@index');
        Route::get('offsale_show', 'Backend\OffsaleReportController@OffsaleReport_show');
        Route::post('/count', 'Backend\OffsaleReportController@offsaleCount')->name('offsaleCount');
        Route::post('get/data', 'Backend\OffsaleReportController@getData')->name('getData');
    });

    Route::group(['prefix' => 'admin/payment', 'as' => 'payment.','middleware' => ['OrderManagment']], function () {
        Route::get('', 'Backend\paymentController@index')->name('index');
        Route::get('create', 'Backend\paymentController@create')->name('create');
        Route::post('store', 'Backend\paymentController@store')->name('store');
        Route::put('update/{id}', 'Backend\paymentController@update')->name('update');
        Route::put('updateUser/{id}', 'Backend\paymentController@updateUser')->name('updateUser');
        Route::any('edit', 'Backend\paymentController@edit')->name('edit');
        Route::delete('destroy/{id}', 'Backend\paymentController@destroy')->name('destroy');
        Route::get('show/all', 'Backend\paymentController@show_all')->name('show.all');
        Route::get('show/incorrect', 'Backend\paymentController@show_incorrect')->name('show.incorrect');
        Route::get('show/all/search', 'Backend\paymentController@show_all_search')->name('show.all.search');
        Route::get('show/{id}', 'Backend\paymentController@show')->name('show');
        Route::get('getPaymentTypes/{id}/{type}', 'Backend\paymentController@getPaymentTypes');
        Route::post('changePrice', 'Backend\paymentController@changePrice')->name('changePrice');
        Route::post('resendEmail', 'Backend\paymentController@resendEmail')->name('resendEmail');
        Route::post('changeStatus', 'Backend\paymentController@changeStatus')->name('changeStatus');
        Route::post('getBlocks', 'Backend\paymentController@getBlocks')->name('getBlocks');
        Route::post('updateNextYearBooking', 'Backend\paymentController@updateNextYearBooking')->name('updateNextYearBooking');
        Route::post('updateCheckedStatus', 'Backend\paymentController@updateCheckedStatus')->name('updateCheckedStatus');
        

        Route::post('clearAllChecked', 'Backend\paymentController@clearAllChecked')->name('clearAllChecked');
        Route::post('updateSale', 'Backend\paymentController@updateSale')->name('updateSale');
        Route::post('getlockers', 'Backend\paymentController@getlockers')->name('getlockers');
        Route::post('getuserroles', 'Backend\paymentController@getuserroles')->name('getuserroles');


    });

    Route::group(['prefix' => 'admin/edit/payment', 'as' => 'payment.edit.','middleware' => ['EditOrders','OrderManagment']], function () {
        Route::get('', 'Backend\EditOrdersController@index')->name('index');
        Route::get('create', 'Backend\EditOrdersController@create')->name('create');
        Route::post('store', 'Backend\EditOrdersController@store')->name('store');
        Route::put('update/{id}', 'Backend\EditOrdersController@update')->name('update');
        Route::put('updateUser/{id}', 'Backend\EditOrdersController@updateUser')->name('updateUser');
        Route::any('edit', 'Backend\EditOrdersController@edit')->name('edit');
        Route::delete('destroy/{id}', 'Backend\EditOrdersController@destroy')->name('destroy');
        Route::get('show/all', 'Backend\EditOrdersController@show_all')->name('show.all');
        Route::get('show/incorrect', 'Backend\EditOrdersController@show_incorrect')->name('show.incorrect');
        Route::get('show/all/search', 'Backend\EditOrdersController@show_all_search')->name('show.all.search');
        Route::get('show/{id}', 'Backend\EditOrdersController@show')->name('show');
        Route::get('getPaymentTypes/{id}/{type}', 'Backend\EditOrdersController@getPaymentTypes');
        Route::post('changePrice', 'Backend\EditOrdersController@changePrice')->name('changePrice');
        Route::post('resendEmail', 'Backend\EditOrdersController@resendEmail')->name('resendEmail');
        Route::post('changeStatus', 'Backend\EditOrdersController@changeStatus')->name('changeStatus');
        Route::post('getBlocks', 'Backend\EditOrdersController@getBlocks')->name('getBlocks');
        Route::post('updateNextYearBooking', 'Backend\EditOrdersController@updateNextYearBooking')->name('updateNextYearBooking');
        Route::post('updateCheckedStatus', 'Backend\EditOrdersController@updateCheckedStatus')->name('updateCheckedStatus');
        Route::post('updateEmailStatus', 'Backend\EditOrdersController@updateEmailStatus')->name('updateEmailStatus');
        Route::post('clearAllChecked', 'Backend\EditOrdersController@clearAllChecked')->name('clearAllChecked');
        Route::post('updateSale', 'Backend\EditOrdersController@updateSale')->name('updateSale');
        Route::post('getlockers', 'Backend\EditOrdersController@getlockers')->name('getlockers');
        Route::post('showMoveableBlocks', 'Backend\EditOrdersController@showMoveableBlocks')->name('showMoveableBlocks');
        Route::post('saveMultiLockers', 'Backend\EditOrdersController@saveMultiLockers')->name('saveMultiLockers');
        Route::post('setSession', 'Backend\EditOrdersController@setSession')->name('setSession');
        Route::post('blockValidation', 'Backend\EditOrdersController@blockValidation')->name('blockValidation');
        Route::post('updateMultiDone', 'Backend\EditOrdersController@updateMultiDone')->name('updateMultiDone');
    });

    Route::group(['prefix' => 'admin/edit/student', 'as' => 'student.edit.','middleware' => ['Students']], function () {
        Route::get('', 'Backend\StudentController@index')->name('index');
        Route::get('create', 'Backend\StudentController@create')->name('create');
        Route::post('store', 'Backend\StudentController@store')->name('store');
        Route::put('update/{id}', 'Backend\StudentController@update')->name('update');
        Route::put('updateUser/{id}', 'Backend\StudentController@updateUser')->name('updateUser');
        Route::any('edit', 'Backend\StudentController@edit')->name('edit');
        Route::delete('destroy/{id}', 'Backend\StudentController@destroy')->name('destroy');
        Route::get('show/all', 'Backend\StudentController@show_all')->name('show.all');
        Route::get('show/incorrect', 'Backend\StudentController@show_incorrect')->name('show.incorrect');
        Route::get('show/all/search', 'Backend\StudentController@show_all_search')->name('show.all.search');
        Route::get('show/{id}', 'Backend\StudentController@show')->name('show');
        Route::get('getStudentTypes/{id}/{type}', 'Backend\StudentController@getStudentTypes');
        Route::post('changePrice', 'Backend\StudentController@changePrice')->name('changePrice');
        Route::post('resendEmail', 'Backend\StudentController@resendEmail')->name('resendEmail');
        Route::post('changeStatus', 'Backend\StudentController@changeStatus')->name('changeStatus');
        Route::post('getBlocks', 'Backend\StudentController@getBlocks')->name('getBlocks');
        Route::post('updateNextYearBooking', 'Backend\StudentController@updateNextYearBooking')->name('updateNextYearBooking');
        Route::post('updateCheckedStatus', 'Backend\StudentController@updateCheckedStatus')->name('updateCheckedStatus');
        // Route::post('updateEmailStatus', 'Backend\EditOrdersController@updateEmailStatus')->name('updateEmailStatus');
        Route::post('clearAllChecked', 'Backend\StudentController@clearAllChecked')->name('clearAllChecked');
        Route::post('updateSale', 'Backend\StudentController@updateSale')->name('updateSale');
        Route::post('getlockers', 'Backend\StudentController@getlockers')->name('getlockers');
        Route::post('showMoveableBlocks', 'Backend\StudentController@showMoveableBlocks')->name('showMoveableBlocks');
        Route::post('saveMultiLockers', 'Backend\StudentController@saveMultiLockers')->name('saveMultiLockers');
        Route::post('setSession', 'Backend\StudentController@setSession')->name('setSession');
        Route::post('blockValidation', 'Backend\StudentController@blockValidation')->name('blockValidation');
        Route::post('updateMultiDone', 'Backend\StudentController@updateMultiDone')->name('updateMultiDone');
    });

    Route::group(['prefix' => 'admin/company/errors', 'as' => 'company.errors.','middleware' => ['CompanyError']], function () {
        Route::get('', 'Backend\CompanyErrorsController@index')->name('index');
        Route::get('create', 'Backend\CompanyErrorsController@create')->name('create');
        Route::post('store', 'Backend\CompanyErrorsController@store')->name('store');
        Route::put('update/{id}', 'Backend\CompanyErrorsController@update')->name('update');
        Route::put('updateUser/{id}', 'Backend\CompanyErrorsController@updateUser')->name('updateUser');
        Route::any('edit', 'Backend\CompanyErrorsController@edit')->name('edit');
        Route::delete('destroy/{id}', 'Backend\CompanyErrorsController@destroy')->name('destroy');
        Route::get('show/all', 'Backend\CompanyErrorsController@show_all')->name('show.all');
        Route::post('resolved', 'Backend\CompanyErrorsController@resolved')->name('resolved');
    });

    Route::post('admin/getpayment', 'Backend\paymentController@getpayment')->name('getpayment');
    Route::post('admin/updatepayment', 'Backend\paymentController@updatepayment')->name('updatepayment');
    Route::post('admin/refundPayment', 'Backend\paymentController@refundPayment')->name('refundPayment');
    Route::get('admin/refundPayment/fix', 'Backend\paymentController@fixPayments');
    Route::get('admin/refundPayment/fixdate', 'Backend\paymentController@fixPaymentsCreatedAt');
    Route::get('admin/refundPayment/fixtransaction', 'Backend\paymentController@fixPaymentTransactions');

    Route::post('admin/payments/filter', 'Backend\paymentController@filter')->name('filter');
    Route::any('admin/refundPaymentList', 'Backend\paymentController@refundPaymentList')->name('refundPaymentList');


    Route::resource('admin/bookingtype', 'Backend\BookingTypeController')->middleware('BookingTypeManagement');


    Route::post('admin/getEmailtemplateAlt', 'Backend\EmailTemplateController@getEmailtemplateAlt')->name('getEmailtemplateAlt');
    Route::post('admin/sendtestemail', 'Backend\EmailTemplateController@sendtestemail')->name('sendtestemail');
    Route::post('admin/updateSystemFunction', 'Backend\EmailTemplateController@updateSystemFunction')->name('updateSystemFunction');

    Route::post('admin/templatedistributionusers', 'Backend\EmailTemplateController@templatedistributionusers')->name('templatedistributionusers');
    Route::get('admin/templatedistributionusers/{slug}', 'Backend\EmailTemplateController@paginate');
    Route::post('admin/getsales', 'Backend\EmailTemplateController@getsales')->name('getsales');

    Route::resource('admin/emailtemplateplaceholder', 'Backend\EmailTemplatePlaceholderController');

    Route::resource('admin/emailtemplatedistributionlist', 'Backend\EmailTemplateDistributionListController');
    Route::post('/sendemail', 'Backend\EmailManagementController@send_email')->name('sendemail');
    Route::get('/email_delete/{id}', 'Backend\EmailManagementController@email_delete')->name('emaildelete');
    Route::get('/email_trash/{id}', 'Backend\EmailManagementController@email_trash');
    Route::get('admin/emailinbox', 'Backend\EmailManagementController@inbox')->name('emailinbox');
    Route::get('admin/emailsent', 'Backend\EmailManagementController@sent')->name('emailsent');
    Route::post('admin/emailsentstar', 'Backend\EmailManagementController@emailsentstar')->name('emailsentstar');
    Route::get('admin/emaildraft', 'Backend\EmailManagementController@draft');
    Route::get('admin/emailtrash', 'Backend\EmailManagementController@trash')->name('emailtrash');
    Route::get('admin/emailspam', 'Backend\EmailManagementController@spam');
    Route::get('admin/emailcompose', 'Backend\EmailManagementController@compose');
    Route::get('admin/emailview/{id}', 'Backend\EmailManagementController@view');
    Route::get('admin/emailmove/{email_id}/{folder_id}', 'Backend\EmailManagementController@move');

    Route::resource('admin/distributionlists', 'Backend\DistributionlistGroupController')->middleware('DistributionListGroupManagement');
    Route::post('admin/getnondistributionusers', 'Backend\DistributionlistGroupController@getnondistributionusers')->name('getnondistributionusers');
    Route::post('admin/getdistributionusers', 'Backend\DistributionlistGroupController@getdistributionusers')->name('getdistributionusers');
    Route::post('admin/distributionlists/view', 'Backend\DistributionlistGroupController@getdistributionusersview')->name('getdistributionusersview');

    Route::resource('admin/distributionlistmembers', 'Backend\DistributionListGroupMemberController');



    Route::resource('admin/helps', 'Backend\HelpController')->middleware('HelpManagment');

    Route::resource('admin/labels', 'Backend\LabelController')->middleware('LanguageManagment');
    Route::get('/admin/editlabel/{key}/{lang}', 'Backend\LabelController@editlabel')->name('editlabel');
    Route::get('/admin/destroylabel/{key}/{lang}', 'Backend\LabelController@destroylabel')->name('destroylabel');

    Route::resource('admin/languages', 'Backend\LanguagesController')->middleware('LanguageManagment');
    Route::get('/admin/destroylanguage/{key}', 'Backend\LanguagesController@destroylanguage')->name('destroylanguage');

    Route::post('/admin/help-ajax', 'Backend\HelpController@updateHelpAjax')->name('updateHelpAjax');


    Route::post('frontend/switchcms', 'Frontend\PageController@switchcms')->name('switchcms');
    Route::post('frontend/savepage', 'Frontend\PageController@savepage')->name('savepage');
    Route::post('frontend/changeText', 'Frontend\PageController@changeText')->name('changeText');
    Route::post('frontend/placeholders', 'Frontend\PageController@placeholders')->name('placeholders');
    Route::post('frontend/multiplaceholders', 'Frontend\PageController@multiplaceholders')->name('multiplaceholders');


//Route::middleware(['IpCheckMiddleware'])->group(function () {


    Route::post('frontend/getlockers', 'Frontend\LockersController@getlockers')->name('getlockers');

    Route::post('frontend/postCodeSearch', 'Frontend\LockersController@postCodeSearch')->name('postCodeSearch');
    Route::post('frontend/manualPostCodeSession', 'Frontend\LockersController@manualPostCodeSession')->name('manualPostCodeSession');
    Route::post('frontend/showLockersStatus', 'Frontend\LockersController@showLockersStatus')->name('showLockersStatus');

    Route::match(array('GET', 'POST'), 'frontend/chooselocker', 'Frontend\LockersController@chooselocker')->name('chooselocker');
    Route::post('frontend/saveSchoolBooking', 'Frontend\LockersController@saveSchoolBooking')->name('saveSchoolBooking');
    Route::get('frontend/saveSchoolBookingBack', 'Frontend\LockersController@saveSchoolBookingBack')->name('saveSchoolBookingBack');

    Route::get('frontend/saveSchoolBookingf', 'Frontend\LockersController@saveSchoolBookingforward')->name('saveSchoolBookingForward');
    Route::post('frontend/updateSale', 'Frontend\LockersController@updateSale')->name('updateSale');

    Route::get('frontend/payments', 'Frontend\LockersController@paymentsBack')->name('payments');
    Route::post('frontend/payments', 'Frontend\LockersController@payments')->name('payments')->middleware('timer');
    Route::post('frontend/savepayments', 'Frontend\LockersController@savepayments')->name('savepayments')->middleware('timer');
    Route::post('frontend/payments3D', 'Frontend\LockersController@payments_3D')->name('payments3D');
    Route::post('frontend/refreshpayments', 'Frontend\LockersController@refresh_payments')->name('refreshpayments');


    Route::post('charitywork', 'Frontend\LockersController@charitywork');

    Route::group(['prefix' => 'admin/edit', 'as' => 'edit.'], function () {

        Route::get('', 'Frontend\EditBookingController@index')->name('index');
        Route::get('{id}/schoolterm/booking', 'Frontend\EditBookingController@editBooking')->name('schoolTerm');
        Route::get('frontend/schooltermbasedback', 'Frontend\EditBookingController@schooltermbasedback')->name('schooltermbasedback');

        Route::post('saveSchoolBooking', 'Frontend\EditBookingController@saveSchoolBooking')->name('saveSchoolBooking');
        Route::get('frontend/saveSchoolBookingBack', 'Frontend\EditBookingController@saveSchoolBookingBack')->name('saveSchoolBookingBack');

        Route::get('frontend/saveSchoolBookingf', 'Frontend\EditBookingController@saveSchoolBookingforward')->name('saveSchoolBookingForward');

        Route::post('payments', 'Frontend\EditBookingController@payments')->name('payments')->middleware('timer');

        Route::post('frontend/updateSale', 'Frontend\EditBookingController@updateSale')->name('updateSale');
        Route::post('sales/log', 'Frontend\EditBookingController@updateSaleLog')->name('updateSaleLog');


    });


//    });
});

Route::post('/custom/error/log', 'Frontend\LockersController@customErrorLog');
Route::get('frontend/schooltermbasedback', 'Frontend\LockersController@schooltermbasedback')->name('schooltermbasedback');
Route::post('frontend/schooltermbased', 'Frontend\LockersController@schooltermbased')->name('schooltermbased');
Route::get('frontend/schooltermbased/{id}', 'Frontend\LockersController@schooltermbasedAfterLogout')->name('schooltermbasedAfterLogout');
Route::post('frontend/voucherCodeValidation', 'Frontend\SchoolTermBasedController@voucherCodeValidation')->name('voucherCodeValidation');

Route::get('frontend/reissueLockercode', 'Frontend\LockersController@reissueLockercode')->name('reissueLockercode');
Route::post('frontend/lockerAvailabilityCheck', 'Frontend\LockersController@lockerAvailabilityCheck')->name('lockerAvailabilityCheck');
Route::post('frontend/lockerAvailabilityCheckSchoolYear', 'Frontend\LockersController@lockerAvailabilityCheckSchoolYear')->name('lockerAvailabilityCheckSchoolYear');
Route::post('frontend/holdlocker', 'Frontend\LockersController@holdlocker')->name('holdlocker');
Route::post('frontend/validateSale', 'Frontend\LockersController@validateSale')->name('validateSale');
Route::post('frontend/removechild', 'Frontend\LockersController@removechild')->name('removechild');

Route::post('frontend/resetSale', 'Frontend\LockersController@resetSale')->name('resetSale');

// Route::resource('frontend', 'Frontend\PageController');

Route::get('/test', 'Frontend\HomeController@test')->name('test');


Route::get('/booking-expired/{id}', 'Frontend\PageController@bookingExpired');

Route::post('frontend/getLanguages', 'Frontend\PageController@getLanguages')->name('getLanguages');


Route::resource('/', 'Frontend\HomeController');


Route::post('frontend/expireBooking', 'Frontend\LockersController@expireBooking')->name('expireBooking');
//location search
Route::post('/frontend/searchlocation', 'Frontend\LocationController@searchlocation')->name('searchlocation');
Route::post('/frontend/getlocationgroup', 'Frontend\LocationController@getlocationgroup')->name('getlocationgroup');
Route::post('/frontend/schoolRequest', 'Frontend\LocationController@schoolRequest')->name('schoolRequest');

Route::get('/maintenance', 'RequestAvailabilityController@maintenance')->name('maintenance');
Route::get('frontend/requestavailability', 'Frontend\LockersController@requestavailability')->name('requestavailability');
Route::post('frontend/saverequestavailability', 'Frontend\LockersController@saverequestavailability')->name('saverequestavailability');

Route::get('/staticMainView', 'MainView\MainViewController@index')->name('staticMainView');

Route::get('getPhp', 'MainView\MainViewController@getPHP');

Route::post('changelocale', ['as' => 'changelocale', 'uses' => 'Frontend\TranslationController@changeLocale']);

Auth::routes();
Route::post('frontend/createAjax', 'Auth\RegisterController@createAjax')->name('createAjax');
Route::post('frontend/userExist', 'Frontend\HomeController@userExist')->name('userExist');
Route::post('frontend/resendEmail', 'Frontend\HomeController@resendEmail')->name('resendEmail');
Route::post('frontend/sendresetsms', 'Frontend\HomeController@sendresetsms')->name('sendresetsms');
Route::post('frontend/validatepasscode', 'Frontend\HomeController@validatepasscode')->name('validatepasscode');
Route::post('frontend/sendresetemail', 'Frontend\HomeController@sendresetemail')->name('sendresetemail');


//Route::post('frontend/resendEmail', 'Frontend\HomeController@resendEmail')->name('resendEmail');
Route::post('exist/users', 'Frontend\PageController@getUserInfoByEmail');
Route::get('/fronten/', 'Frontend\HomeController@logout');
Route::get('/contact-us', 'Frontend\HomeController@support');
Route::get('/aboutUs', 'Frontend\PageController@aboutUs');
Route::get('/faq', 'Frontend\PageController@faq');
Route::get('/policy', 'Frontend\PageController@policy');
Route::get('/contactUs', 'Frontend\PageController@contactUs');
Route::post('/send/contact/mail', 'Frontend\PageController@sendContactMail');
Route::get('/unsubscribe/{id}', 'Frontend\PageController@unsubcribe');
Route::get('export', 'CompanyProspectsController@export')->name('export');
Route::get('importExportView', 'CompanyProspectsController@importExportView');
Route::post('import', 'CompanyProspectsController@import')->name('import');


// Route::get('/reset/form/{token}', 'ForgetPasswordController@showPasswordResetForm');

// Route::post('password/email', 'ForgetPasswordController@forgot');
// Route::view('forgot_password', 'auth.reset_password')->name('password.reset');
// Route::post('password/reset', 'ForgetPasswordController@reset');
// Route::get('/forget_password/form', 'ForgetPasswordController@view');
// Route::get('/forget_password/update', 'ForgetPasswordController@update');




//Static Pages Routes