<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndex2ToCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->index('location_group_id');
            $table->index('currency');
            $table->index('keep_block');
            $table->index('status_type');
            $table->index('send_voucher_code');
            $table->index('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->dropIndex(['location_group_id']);
            $table->dropIndex(['currency']);
            $table->dropIndex(['keep_block']);
            $table->dropIndex(['status_type']);
            $table->dropIndex(['send_voucher_code']);
            $table->dropIndex(['status']);
        });
    }
}
