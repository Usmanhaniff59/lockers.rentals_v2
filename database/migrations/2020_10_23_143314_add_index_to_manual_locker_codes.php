<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToManualLockerCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manual_locker_codes', function (Blueprint $table) {

            $table->index('code');
            $table->index('start');
            $table->index('end');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manual_locker_codes', function (Blueprint $table) {

            $table->dropIndex(['code']);
            $table->dropIndex(['start']);
            $table->dropIndex(['end']);
        });
    }
}
