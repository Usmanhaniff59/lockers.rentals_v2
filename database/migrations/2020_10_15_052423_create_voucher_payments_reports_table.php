<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoucherPaymentsReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucher_payments_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->biginteger('company_id')->index()->nullable();
            $table->biginteger('locker_id')->index()->nullable();
            $table->biginteger('voucher_code_id')->index()->nullable();
            $table->biginteger('user_id')->index()->nullable();
            $table->biginteger('booking_id')->index()->nullable();
            $table->datetime('start')->index()->nullable();
            $table->datetime('end')->index()->nullable();
            $table->decimal('booking_cost', 18,2)->nullable();
            $table->decimal('voucher_cost', 18,2)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voucher_payments_reports');
    }
}
