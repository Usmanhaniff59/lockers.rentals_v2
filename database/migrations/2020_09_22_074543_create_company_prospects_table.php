<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyProspectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_prospects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('company_id')->nullable();
            $table->string('territory_code')->nullable();
            $table->string('short_post_code')->nullable();
            $table->string('buzz_code')->nullable();
            $table->string('establishment_name')->nullable();
            $table->string('town')->nullable();
            $table->string('country')->nullable();
            $table->string('post_code')->nullable();
            $table->string('telephone')->nullable();
            $table->string('gender')->nullable();
            $table->string('la')->nullable();
            $table->string('school_lea_code')->nullable();
            $table->string('low_age')->nullable();
            $table->string('high_age')->nullable();
            $table->string('open_date')->nullable();
            $table->string('type_of_establishment')->nullable();
            $table->string('sixth_form')->nullable();
            $table->string('phase_of_education')->nullable();
            $table->string('boarding_school')->nullable();
            $table->string('academy_trust')->nullable();
            $table->string('free_school_meals_percentage')->nullable();
            $table->string('religious_affiliation')->nullable();
            $table->string('number_of_pupils')->nullable();
            $table->string('area')->nullable();
            $table->string('website_address')->nullable();
            $table->string('general_email')->nullable();
            $table->string('head_title')->nullable();
            $table->string('head_first')->nullable();
            $table->string('head_surname')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_prospects');
    }
}