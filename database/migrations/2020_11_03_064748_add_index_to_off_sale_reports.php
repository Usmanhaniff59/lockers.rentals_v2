<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToOffSaleReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('off_sale_reports', function (Blueprint $table) {
            $table->index('month');
            $table->index('year');
            $table->index('company_id');
            $table->index('franchisee_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('off_sale_reports', function (Blueprint $table) {
            $table->dropIndex(['month']);
            $table->dropIndex(['year']);
            $table->dropIndex(['company_id']);
            $table->dropIndex(['franchisee_id']);
        });
    }
}
