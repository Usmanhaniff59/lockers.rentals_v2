<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToEmailTemplateDistributionLists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('email_template_distribution_lists', function (Blueprint $table) {
            $table->index('email_template_id');
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('email_template_distribution_lists', function (Blueprint $table) {
            $table->dropIndex(['email_template_id']);
            $table->dropIndex(['user_id']);
        });
    }
}
