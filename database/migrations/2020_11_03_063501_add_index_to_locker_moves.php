<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToLockerMoves extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('locker_moves', function (Blueprint $table) {
            $table->index('sale_id');
            $table->index('booking_id');
            $table->index('old_locker_id');
            $table->index('new_locker_id');
            $table->index('complete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('locker_moves', function (Blueprint $table) {
            $table->dropIndex(['sale_id']);
            $table->dropIndex(['booking_id']);
            $table->dropIndex(['old_locker_id']);
            $table->dropIndex(['new_locker_id']);
            $table->dropIndex(['complete']);
        });
    }
}
