<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToDistributionListGroupMembers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('distribution_list_group_members', function (Blueprint $table) {
            $table->index('distribution_list_group_id');
            $table->index('user_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('distribution_list_group_members', function (Blueprint $table) {
            $table->dropIndex(['distribution_list_group_id']);
            $table->dropIndex(['user_id']);

        });
    }
}
