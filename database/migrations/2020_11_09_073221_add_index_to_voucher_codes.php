<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToVoucherCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('voucher_codes', function (Blueprint $table) {
            $table->index('company_id');
            $table->index('type');
            $table->index('code');
            $table->index('start');
            $table->index('end');
            $table->index('email');
            $table->index('issued');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('voucher_codes', function (Blueprint $table) {
            $table->dropIndex(['company_id']);
            $table->dropIndex(['type']);
            $table->dropIndex(['code']);
            $table->dropIndex(['start']);
            $table->dropIndex(['end']);
            $table->dropIndex(['email']);
            $table->dropIndex(['issued']);
        });
    }
}
