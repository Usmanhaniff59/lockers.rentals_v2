<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transactions', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->bigInteger('booking_id')->nullable()->index();
			$table->bigInteger('payment_id')->nullable()->index();
			$table->bigInteger('user_id')->nullable()->index();
            $table->string('vendor_tx_code', 40)->nullable();
            $table->string('currency', 191)->nullable();
			$table->string('last4Digits', 20)->nullable();
			$table->decimal('amount', 19,2)->nullable();
			$table->string('type', 2);
			$table->integer('amount_change')->nullable();
			$table->integer('refund')->nullable();
			$table->string('status', 20)->nullable();
			$table->string('status_message', 191)->nullable();
            $table->tinyInteger('void')->nullable();
            $table->boolean('fraud_score')->default(0);
			$table->string('transaction_id', 100)->nullable();
			$table->text('notes')->nullable();
			$table->tinyInteger('waiting_response_sagepay')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transactions');
	}

}
