<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToMassRefunds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mass_refunds', function (Blueprint $table) {
            $table->index('booking_id');
            $table->index('completed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mass_refunds', function (Blueprint $table) {
            $table->dropIndex(['booking_id']);
            $table->dropIndex(['completed']);

        });
    }
}
