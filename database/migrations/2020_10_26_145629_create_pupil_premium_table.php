<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePupilPremiumTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pupil_premium', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('booking_id')->index();
            $table->bigInteger('user_id')->index();
            $table->bigInteger('company_id')->index();
            $table->bigInteger('block_id')->index();
            $table->bigInteger('locker_id')->index();
            $table->bigInteger('school_year_id')->index();
            $table->dateTime('start')->index();
            $table->dateTime('end')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pupil_premium');
    }
}
