<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToLockerOffSales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('locker_off_sales', function (Blueprint $table) {
            $table->index('locker_id');
            $table->index('active');
            $table->index('delete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('locker_off_sales', function (Blueprint $table) {
            $table->dropIndex(['locker_id']);
            $table->dropIndex(['active']);
            $table->dropIndex(['delete']);
        });
    }
}
