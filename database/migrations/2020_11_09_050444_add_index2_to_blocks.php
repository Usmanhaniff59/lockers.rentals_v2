<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndex2ToBlocks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blocks', function (Blueprint $table) {
            $table->index('previous_block_id');
            $table->index('lock_type');
            $table->index('order_list');
             $table->index('location_group_id');
            $table->index('locker_block_image_id');
            $table->index('resend_instruction');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blocks', function (Blueprint $table) {
            $table->dropIndex(['previous_block_id']);
            $table->dropIndex(['lock_type']);
            $table->dropIndex(['order_list']);
             $table->dropIndex(['location_group_id']);
            $table->dropIndex(['locker_block_image_id']);
            $table->dropIndex(['resend_instruction']);
        });
    }
}
