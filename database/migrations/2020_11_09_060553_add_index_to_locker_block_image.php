<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToLockerBlockImage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('locker_block_image', function (Blueprint $table) {
            $table->index('file_name');
            $table->index('active');
            $table->index('deleted');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('locker_block_image', function (Blueprint $table) {
            $table->dropIndex(['file_name']);
            $table->dropIndex(['active']);
            $table->dropIndex(['deleted']);

        });
    }
}
