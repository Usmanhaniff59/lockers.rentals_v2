<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSagePayBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sage_pay_balances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('date')->index();
            $table->integer('start_transaction_no');
            $table->integer('end_transaction_no');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sage_pay_balances');
    }
}
