<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToRequestAvailabilities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_availabilities', function (Blueprint $table) {
            $table->index('location');
            $table->index('email');
            $table->index('emailed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_availabilities', function (Blueprint $table) {
            $table->dropIndex(['location']);
            $table->dropIndex(['email']);
            $table->dropIndex(['emailed']);
        });
    }
}
