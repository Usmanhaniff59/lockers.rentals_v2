<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSchoolRequestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('school_requests', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->integer('user_id')->unsigned();
			$table->string('school_name', 191);
			$table->text('line_1_address', 65535);
			$table->text('line_2_address', 65535);
			$table->string('town', 191);
			$table->string('post_code', 191);
			$table->string('country', 191);
			$table->date('date_requested');
			$table->string('requests', 191);
			$table->string('response', 191);
			$table->date('date_responded')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('school_requests');
	}

}
