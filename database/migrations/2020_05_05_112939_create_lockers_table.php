<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLockersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lockers', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->bigInteger('block_id')->unsigned()->nullable()->index();
			$table->bigInteger('company_id')->unsigned()->nullable()->index();
			$table->integer('locker_number')->nullable();
			$table->string('location_info', 191)->nullable();
			$table->dateTime('date_added')->nullable();
			$table->integer('locker_order')->nullable();
			$table->boolean('active')->default(1);
			$table->boolean('display')->default(1);
			$table->boolean('updated')->default(1);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lockers');
	}

}
