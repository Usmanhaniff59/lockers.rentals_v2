<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->string('territory_code')->nullable();
            $table->string('short_post_code')->nullable();
            $table->string('buzz_code')->nullable();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('address3')->nullable();
            $table->string('town')->nullable();
            $table->string('telephone')->nullable();
            $table->string('gender')->nullable();
            $table->string('la')->nullable();
            $table->string('school_lea_code')->nullable();
            $table->string('low_age')->nullable();
            $table->string('high_age')->nullable();
            $table->date('open_date')->nullable();
            $table->string('type_of_establishment')->nullable();
            $table->string('sixth_form')->nullable();
            $table->string('phase_of_education')->nullable();
            $table->string('boarding_school')->nullable();
            $table->string('academy_trust')->nullable();
            $table->string('free_school_meals_percentage')->nullable();
            $table->string('religious_affiliation')->nullable();
            $table->string('number_of_pupils')->nullable();
            $table->string('area')->nullable();
            $table->string('website_address')->nullable();
            $table->string('general_email')->nullable();
            $table->string('head_title')->nullable();
            $table->string('head_first')->nullable();
            $table->string('head_surname')->nullable();
            $table->string('status')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {

            $table->dropColumn('territory_code');
            $table->dropColumn('short_post_code');
            $table->dropColumn('buzz_code');
            $table->dropColumn('address1');
            $table->dropColumn('address2');
            $table->dropColumn('address3');
            $table->dropColumn('town');
            $table->dropColumn('telephone');
            $table->dropColumn('gender');
            $table->dropColumn('la');
            $table->dropColumn('school_lea_code');
            $table->dropColumn('low_age');
            $table->dropColumn('high_age');
            $table->dropColumn('open_date');
            $table->dropColumn('type_of_establishment');
            $table->dropColumn('sixth_form');
            $table->dropColumn('phase_of_education');
            $table->dropColumn('boarding_school');
            $table->dropColumn('academy_trust');
            $table->dropColumn('free_school_meals_percentage');
            $table->dropColumn('religious_affiliation');
            $table->dropColumn('number_of_pupils');
            $table->dropColumn('area');
            $table->dropColumn('website_address');
            $table->dropColumn('general_email');
            $table->dropColumn('head_title');
            $table->dropColumn('head_first');
            $table->dropColumn('head_surname');
            $table->dropColumn('status');
        });
    }
}