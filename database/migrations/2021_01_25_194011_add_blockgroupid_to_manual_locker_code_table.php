<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBlockgroupidToManualLockerCodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manual_locker_codes', function (Blueprint $table) {
            $table->integer('block_group_id')->nullable()->index()->after('block_id'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manual_locker_codes', function (Blueprint $table) {
            // $table->dropColumn('code');
            $table->dropColumn('block_id');
            
        });
    }
}
