<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToBlockYears extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('block_years', function (Blueprint $table) {

            $table->index('company_id');
            $table->index('block_id');
            $table->index('school_academic_year_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('block_years', function (Blueprint $table) {

            $table->dropIndex(['company_id']);
            $table->dropIndex(['block_id']);
            $table->dropIndex(['school_academic_year_id']);

        });
    }
}
