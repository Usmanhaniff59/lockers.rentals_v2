<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTextsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('texts', function(Blueprint $table)
		{
			$table->char('key', 191);
			$table->char('lang', 2)->default('en');
			$table->text('value');
			$table->enum('scope', array('admin','site','global'))->default('global');
			$table->timestamps();
			$table->primary(['key','lang','scope']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('texts');
	}

}
