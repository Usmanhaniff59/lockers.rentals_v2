<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_reports', function(Blueprint $table)
        {
            $table->bigInteger('id', true)->unsigned();
            $table->string('currency', 30)->nullable();
            $table->string('date', 30)->nullable();
            $table->integer('sales_count')->nullable();
            $table->integer('amount')->nullable();
            $table->integer('refund')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_reports');
    }
}
