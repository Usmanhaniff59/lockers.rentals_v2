<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToEmailTemplateAlts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('email_template_alts', function (Blueprint $table) {
            $table->index('email_template_id');
            $table->index('lang');
            $table->index('tab_name');
            $table->index('link_text');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('email_template_alts', function (Blueprint $table) {
            $table->dropIndex(['email_template_id']);
            $table->dropIndex(['lang']);
            $table->dropIndex(['tab_name']);
            $table->dropIndex(['link_text']);
        });
    }
}
