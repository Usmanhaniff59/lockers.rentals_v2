<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToSales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales', function (Blueprint $table) {

            $table->index('number');
            $table->index('net_pass');
            $table->index('block_name');
            $table->index('confirmation_sent');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales', function (Blueprint $table) {

            $table->dropIndex(['number']);
            $table->dropIndex(['net_pass']);
            $table->dropIndex(['block_name']);
            $table->dropIndex(['confirmation_sent']);

        });
    }
}
