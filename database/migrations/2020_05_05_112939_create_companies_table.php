<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompaniesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('companies', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->bigInteger('location_group_id')->unsigned()->nullable();
			$table->enum('currency', array('GBP','EUR'))->nullable();
			$table->string('name', 191)->nullable();
			$table->string('stop_auto_rebook', 191)->nullable();
			$table->string('name_number', 191)->nullable();
			$table->string('street', 191)->nullable();
			$table->string('city', 191)->nullable();
			$table->string('country', 191)->nullable();
			$table->string('post_code', 191)->nullable();
			$table->string('active', 191)->nullable();
			$table->string('display', 191)->nullable();
			$table->string('keep_block', 191)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('companies');
	}

}
