<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndex2ToSales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales', function (Blueprint $table) {
            $table->index('lang');
            $table->index('title');
            $table->index('booking_type');
            $table->index('hold_booking_date');
            $table->index('school_year_id');
            $table->index('school_academic_year_id');
            $table->index('hold_locker');
            $table->index('booking_expiry');
            $table->index('checked');
            $table->index('resend_request');
            $table->index('pupil_premium');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales', function (Blueprint $table) {
            $table->dropIndex(['lang']);
            $table->dropIndex(['title']);
            $table->dropIndex(['booking_type']);
            $table->dropIndex(['hold_booking_date']);
            $table->dropIndex(['school_year_id']);
            $table->dropIndex(['school_academic_year_id']);
            $table->dropIndex(['hold_locker']);
            $table->dropIndex(['booking_expiry']);
            $table->dropIndex(['checked']);
            $table->dropIndex(['resend_request']);
            $table->dropIndex(['pupil_premium']);
        });
    }
}
