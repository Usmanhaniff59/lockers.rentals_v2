<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCronSchedulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cron_schedules', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('class', 191);
			$table->string('controller', 191);
			$table->string('where_check', 191);
			$table->dateTime('due');
			$table->string('completed', 191);
			$table->string('status', 191);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cron_schedules');
	}

}
