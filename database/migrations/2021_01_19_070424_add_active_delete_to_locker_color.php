<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActiveDeleteToLockerColor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('locker_color', function (Blueprint $table) {
             $table->renameColumn('name', 'color');
          $table->boolean('active')->default(1);
          $table->boolean('delete')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('locker_color', function (Blueprint $table) {
            $table->renameColumn('color', 'name');
             $table->dropColumn('active');
             $table->dropColumn('delete');
        });
    }
}
