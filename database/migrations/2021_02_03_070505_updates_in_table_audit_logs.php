<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatesInTableAuditLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('audit_logs', function (Blueprint $table) {
           $table->text('field')->change(); //
           $table->text('old_val')->change(); //
           $table->text('updated_val')->change()->nullable(); //
           $table->bigInteger('table_id')->after('table_name');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('audit_logs', function (Blueprint $table) {
          $table->string('field')->change(); //
           $table->string('old_val')->change(); //
           $table->string('updated_val')->change(); //
            $table->dropColumn('table_id');
           
        });
    }
}
