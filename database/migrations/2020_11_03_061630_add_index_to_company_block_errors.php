<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToCompanyBlockErrors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_block_errors', function (Blueprint $table) {
            $table->index('company_id');
            $table->index('block_id');
            $table->index('error_type');
            $table->index('resolved');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_block_errors', function (Blueprint $table) {
            $table->dropIndex(['company_id']);
            $table->dropIndex(['block_id']);
            $table->dropIndex(['error_type']);
            $table->dropIndex(['resolved']);

        });
    }
}
