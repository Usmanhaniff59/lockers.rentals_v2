<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToCompanyYears extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_years', function (Blueprint $table) {
            $table->index('school_academic_year_id');
            $table->index('company_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_years', function (Blueprint $table) {
            $table->dropIndex(['school_academic_year_id']);
            $table->dropIndex(['company_id']);
        });
    }
}
