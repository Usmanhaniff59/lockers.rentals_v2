<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentDiscountsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payment_discounts', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->bigInteger('rate_id')->unsigned();
			$table->boolean('sept');
			$table->boolean('oct');
			$table->boolean('nov');
			$table->boolean('dec');
			$table->boolean('jan');
			$table->boolean('feb');
			$table->boolean('mar');
			$table->boolean('april');
			$table->boolean('may');
			$table->boolean('jun');
			$table->boolean('july');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payment_discounts');
	}

}
