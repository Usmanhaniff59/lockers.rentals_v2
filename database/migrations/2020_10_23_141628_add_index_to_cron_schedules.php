<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToCronSchedules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cron_schedules', function (Blueprint $table) {

            $table->index('class');
            $table->index('controller');
            $table->index('due');
            $table->index('completed');
            $table->index('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cron_schedules', function (Blueprint $table) {

            $table->dropIndex(['class']);
            $table->dropIndex(['controller']);
            $table->dropIndex(['due']);
            $table->dropIndex(['completed']);
            $table->dropIndex(['status']);
        });
    }
}
