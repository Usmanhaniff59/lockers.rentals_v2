<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmailTemplatePlaceHoldersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('email_template_place_holders', function(Blueprint $table)
		{
			$table->foreign('email_template_id', 'email_template_place_holders_emailtemplateid_foreign')->references('id')->on('email_templates')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('email_template_place_holders', function(Blueprint $table)
		{
			$table->dropForeign('email_template_place_holders_emailtemplateid_foreign');
		});
	}

}
