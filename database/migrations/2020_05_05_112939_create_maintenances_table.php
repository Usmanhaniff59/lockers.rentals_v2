<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMaintenancesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('maintenances', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->bigInteger('locker_id')->unsigned()->nullable()->index();
			$table->bigInteger('company_id')->unsigned()->nullable()->index();
			$table->bigInteger('block_id')->unsigned()->nullable()->index();
			$table->dateTime('from')->nullable();
			$table->dateTime('to')->nullable();
			$table->boolean('active')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('maintenances');
	}

}
