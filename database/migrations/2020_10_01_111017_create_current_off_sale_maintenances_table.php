<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrentOffSaleMaintenancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('current_off_sale_maintenances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedbiginteger('company_id');
            $table->unsignedbiginteger('franchise_id')->nullable();
            $table->unsignedbiginteger('block_id');
            $table->unsignedbiginteger('locker_id');
            $table->date('start_date')->nullable();
            $table->string('type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('current_off_sale_maintenances');
    }
}
