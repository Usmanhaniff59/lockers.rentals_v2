<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffSaleReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('off_sale_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('month');
            $table->year('year');
            $table->unsignedbiginteger('company_id');
            $table->unsignedbiginteger('franchisee_id')->nullable();
            $table->integer('maintenance_cleaning')->defaul(0);
            $table->integer('maintenance_fixing')->defaul(0);
            $table->integer('maintenance_late')->defaul(0);
            $table->integer('total_off_sale')->defaul(0);
            $table->integer('offsale_late')->defaul(0);
            $table->integer('total_active_lockers')->defaul(0);
            $table->integer('total_lockers')->defaul(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('off_sale_reports');
    }
}
