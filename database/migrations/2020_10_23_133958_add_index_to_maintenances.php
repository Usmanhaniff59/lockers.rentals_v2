<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToMaintenances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('maintenances', function (Blueprint $table) {

            $table->index('type');
            $table->index('from');
            $table->index('to');
            $table->index('active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('maintenances', function (Blueprint $table) {

            $table->dropIndex(['type']);
            $table->dropIndex(['from']);
            $table->dropIndex(['to']);
            $table->dropIndex(['active']);
        });
    }
}
