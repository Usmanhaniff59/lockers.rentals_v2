<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToAcademicYearReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('academic_year_reports', function (Blueprint $table) {
            $table->index('year');
            $table->index('company_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('academic_year_reports', function (Blueprint $table) {
            $table->dropIndex(['year']);
            $table->dropIndex(['company_id']);
        });
    }
}
