<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCharityTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('charity_transactions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->bigInteger('charity_id')->unsigned();
			$table->string('amount', 191);
			$table->bigInteger('booking_id')->unsigned();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('charity_transactions');
	}

}
