<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBlocksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('blocks', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->bigInteger('previous_block_id')->unsigned()->nullable();
			$table->bigInteger('company_id')->unsigned()->nullable()->index();
			$table->string('lock_type', 1)->nullable();
			$table->bigInteger('order_list')->unsigned()->nullable();
			$table->bigInteger('location_group_id')->unsigned()->nullable();
			$table->bigInteger('rate_id')->unsigned()->nullable();
			$table->string('block_height', 191)->nullable();
			$table->string('locker_blocks', 191)->nullable();
			$table->string('name', 191)->nullable();
			$table->string('street', 50)->nullable();
			$table->string('city', 50)->nullable();
			$table->string('country', 50)->nullable();
			$table->string('post_code', 50)->nullable();
			$table->bigInteger('locker_block_image_id')->unsigned()->nullable();
			$table->boolean('active')->nullable();
			$table->boolean('display')->nullable();
			$table->boolean('resend_instruction')->nullable();
			$table->integer('school_academic_year_id')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('blocks');
	}

}
