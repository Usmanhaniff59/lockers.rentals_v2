<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToVoucherCodesIssueds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('voucher_codes_issueds', function (Blueprint $table) {
            $table->index('voucher_code_id');
            $table->index('name');
            $table->index('email');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('voucher_codes_issueds', function (Blueprint $table) {
            $table->dropIndex(['voucher_code_id']);
            $table->dropIndex(['name']);
            $table->dropIndex(['email']);

        });
    }
}
