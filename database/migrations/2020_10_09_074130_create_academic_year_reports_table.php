<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcademicYearReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('academic_year_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->year('year');
            $table->unsignedbiginteger('company_id');
            $table->integer('total_lockers')->defaul(0);
            $table->integer('total_lockers_sold')->defaul(0);
            $table->integer('total_lockers_available')->defaul(0);
            $table->integer('lockers_sold_percentage')->defaul(0);
            $table->integer('sold_last_year')->defaul(0);
            $table->integer('change_from_last_year_percentage')->defaul(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('academic_year_reports');
    }
}
