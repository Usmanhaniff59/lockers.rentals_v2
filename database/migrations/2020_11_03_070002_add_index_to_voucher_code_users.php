<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToVoucherCodeUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('voucher_code_users', function (Blueprint $table) {
            $table->index('user_id');
            $table->index('code');
            $table->index('booking_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('voucher_code_users', function (Blueprint $table) {
            $table->dropIndex(['user_id']);
            $table->dropIndex(['code']);
            $table->dropIndex(['booking_id']);
        });
    }
}
