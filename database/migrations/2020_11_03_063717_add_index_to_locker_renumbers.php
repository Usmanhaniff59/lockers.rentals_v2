<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToLockerRenumbers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('locker_renumbers', function (Blueprint $table) {
            $table->index('locker_id');
            $table->index('user_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('locker_renumbers', function (Blueprint $table) {
            $table->dropIndex(['locker_id']);
            $table->dropIndex(['user_id']);
        });
    }
}
