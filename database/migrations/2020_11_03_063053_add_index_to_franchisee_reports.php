<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToFranchiseeReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('franchisee_reports', function (Blueprint $table) {
            $table->index('franchisee_id');
            $table->index('month');
            $table->index('year');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('franchisee_reports', function (Blueprint $table) {
            $table->dropIndex(['franchisee_id']);
            $table->dropIndex(['month']);
            $table->dropIndex(['year']);

        });
    }
}
