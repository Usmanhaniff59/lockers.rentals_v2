<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToRefundTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('refund_transactions', function (Blueprint $table) {
            $table->index('transaction_id');
            $table->index('status');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('refund_transactions', function (Blueprint $table) {
            $table->dropIndex(['transaction_id']);
            $table->dropIndex(['status']);
         });
    }
}
