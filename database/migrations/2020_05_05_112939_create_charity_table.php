<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCharityTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('charity', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('charity_name', 191);
			$table->string('default_amount', 191);
			$table->string('charity_logo', 191);
			$table->string('charity_video', 191);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('charity');
	}

}
