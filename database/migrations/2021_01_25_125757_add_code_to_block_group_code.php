<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodeToBlockGroupCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('block_group_code', function (Blueprint $table) {
             $table->string('code')->after('block_group_id');
             $table->integer('locker_number')->charset(null)->collation(null)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('block_group_code', function (Blueprint $table) {
             $table->dropColumn('code');
             $table->string('locker_number')->change();
        });
    }
}
