<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHoldSalesIdToSalesAudits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_audits', function (Blueprint $table) {
            $table->bigInteger('hold_sales_id')->after('school_academic_year_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_audits', function (Blueprint $table) {
            $table->dropColumn('hold_sales_id');

        });
    }
}
