<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharityReportsTable extends Migration
{
    public function up()
    {
        Schema::create('charity_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('month');
            $table->year('year');
            $table->string('currency')->nullable();
            $table->integer('bookings')->default(0);
            $table->decimal('payment', 18,2)->default(0);
            $table->integer('charity_transaction')->default(0);
            $table->decimal('charity_revenue', 18,2)->default(0);
            $table->decimal('total_payment', 18,2)->default(0);
            $table->integer('charity_revenue_percentage')->default(0);
            $table->boolean('charity_processed')->default(0);
             $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('charity_reports');
    }
}
