<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUpdatedColToManualLockerCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manual_locker_codes', function (Blueprint $table) {
            //
            $table->boolean('updated')->after('end')->default(0);
            // zero is the case => when no changes have done 
            // one is the case => when we have made some updates in the codes
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manual_locker_codes', function (Blueprint $table) {
            //
            $table->dropColumn('updated');
        });
    }
}
