<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 191)->nullable();
			$table->string('surname', 191)->nullable();
			$table->string('username', 191)->nullable();
			$table->string('email', 191)->unique();
			$table->string('password', 191);
			$table->string('phone_number', 191)->nullable();
			$table->string('address_1', 100)->nullable();
			$table->string('address_2', 100)->nullable();
			$table->string('city', 50)->nullable();
			$table->string('post_code', 20)->nullable();
			$table->string('state', 30)->nullable();
			$table->string('country', 20)->nullable();
			$table->bigInteger('franchisee_id')->nullable();
			$table->bigInteger('company_id')->nullable();
			$table->boolean('active')->default(1);
			$table->boolean('subscription')->default(0);
			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
