<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {

            $table->index('amount');
            $table->index('type');
            $table->index('amount_change');
            $table->index('refund');
            $table->index('status');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {

            $table->dropIndex(['amount']);
            $table->dropIndex(['type']);
            $table->dropIndex(['amount_change']);
            $table->dropIndex(['refund']);
            $table->dropIndex(['status']);

        });
    }
}
