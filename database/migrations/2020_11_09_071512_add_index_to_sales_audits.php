<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToSalesAudits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_audits', function (Blueprint $table) {
            $table->index('net_pass');
            $table->index('number');
            $table->index('school_year_id');
            $table->index('school_academic_year_id');
            $table->index('hold_sales_id');
            $table->index('type');
            $table->index('hold_locker');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_audits', function (Blueprint $table) {
            $table->dropIndex(['net_pass']);
            $table->dropIndex(['number']);
            $table->dropIndex(['school_year_id']);
            $table->dropIndex(['school_academic_year_id']);
            $table->dropIndex(['hold_sales_id']);
            $table->dropIndex(['type']);
            $table->dropIndex(['hold_locker']);
        });
    }
}
