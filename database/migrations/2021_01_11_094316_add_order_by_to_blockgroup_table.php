<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderByToBlockgroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('block_groups', function (Blueprint $table) {
            $table->bigInteger('order_by')->after('name')->default(0);
          $table->boolean('status')->after('order_by')->default(1);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('block_groups', function (Blueprint $table) {
            $table->dropColumn('order_by');
            $table->dropColumn('status');
            
        });
    }
}
