<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToCharityTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('charity_transactions', function (Blueprint $table) {

            $table->index('amount');
            $table->index('booking_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('charity_transactions', function (Blueprint $table) {

            $table->dropIndex(['amount']);
            $table->dropIndex(['booking_id']);

        });
    }
}
