<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToStatusLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('status_logs', function (Blueprint $table) {
            $table->index('company_id');
            $table->index('user_id');
            $table->index('status_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('status_logs', function (Blueprint $table) {
            $table->dropIndex(['company_id']);
            $table->dropIndex(['user_id']);
            $table->dropIndex(['status_type']);
        });
    }
}
