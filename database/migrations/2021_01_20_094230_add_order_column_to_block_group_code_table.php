<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderColumnToBlockGroupCodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('block_group_code', function (Blueprint $table) {
                 $table->renameColumn('code', 'locker_number');
                 $table->renameColumn('start', 'start_date');
                 $table->renameColumn('end', 'end_date');
                 $table->integer('order')->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('block_group_code', function (Blueprint $table) {
            $table->renameColumn('locker_number', 'code');
            $table->renameColumn('start_date', 'start');
            $table->renameColumn('end_date', 'end');
            $table->dropColumn('order');
        });
    }
}
