<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmailTemplatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('email_templates', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->boolean('system_function')->default(0);
			$table->string('system_task_name', 191)->nullable();
			$table->text('template_name', 65535);
			$table->string('subject', 191);
			$table->text('template_text', 65535);
			$table->string('sms_message', 180)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('email_templates');
	}

}
