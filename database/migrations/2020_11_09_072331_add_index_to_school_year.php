<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToSchoolYear extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('school_year', function (Blueprint $table) {
            $table->index('title');
            $table->index('from_date');
            $table->index('to_date');
            $table->index('status');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('school_year', function (Blueprint $table) {
            $table->dropIndex(['title']);
            $table->dropIndex(['from_date']);
            $table->dropIndex(['to_date']);
            $table->dropIndex(['status']);

        });
    }
}
