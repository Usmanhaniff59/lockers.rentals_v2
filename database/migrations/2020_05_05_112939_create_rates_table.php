<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rates', function(Blueprint $table)
		{
			$table->bigInteger('key', true)->unsigned();
			$table->integer('id')->index();
			$table->string('name', 50)->nullable();
			$table->decimal('hour', 5)->nullable();
			$table->decimal('day', 10)->nullable();
			$table->decimal('week', 10)->nullable();
			$table->decimal('month', 10)->nullable();
			$table->decimal('year', 10)->nullable();
			$table->decimal('unit', 10)->nullable();
			$table->decimal('vat', 10);
			$table->date('valid_from')->nullable();
			$table->date('valid_too')->nullable();
			$table->date('date_added')->nullable();
			$table->date('date_updated')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rates');
	}

}
