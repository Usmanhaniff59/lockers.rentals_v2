<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LockerNumberReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
           Schema::create('locker_number_report', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('order')->nullable();
            // $table->string('code', 10)->nullable();
            $table->string('locker_number', 10)->nullable();
             $table->integer('block_group_id')->nullable()->index();
            // $table->integer('block_id')->nullable()->index();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locker_number_report');
    }
}
