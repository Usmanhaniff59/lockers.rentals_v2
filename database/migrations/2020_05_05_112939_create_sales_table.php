<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSalesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sales', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->integer('user_id')->nullable()->index();
			$table->integer('booking_id')->nullable()->index();
			$table->string('lang', 5)->nullable();
			$table->boolean('booking_type')->nullable();
			$table->string('title', 191)->nullable();
			$table->string('number', 100)->nullable();
			$table->string('net_pass', 12)->nullable();
			$table->string('lock_type', 1)->nullable();
			$table->bigInteger('locker_id')->unsigned()->nullable()->index();
			$table->bigInteger('block_id')->unsigned()->nullable()->index();
			$table->bigInteger('company_id')->unsigned()->nullable()->index();
			$table->dateTime('start')->nullable()->index();
			$table->dateTime('end')->nullable()->index();
			$table->decimal('price', 7,2)->nullable();
			$table->decimal('price_original', 7,2)->nullable();
			$table->integer('discount_percent')->nullable();
			$table->decimal('tax', 7,2)->nullable();
			$table->string('email', 191)->nullable()->index();
			$table->string('phone_number', 40)->nullable();
			$table->string('status', 1)->nullable()->index();
			$table->dateTime('hold_booking_date')->nullable();
			$table->boolean('confirmation_sent')->nullable();
			$table->integer('rebooking_sent')->nullable();
			$table->dateTime('rebooking_email_date')->nullable();
			$table->boolean('sms')->nullable();
			$table->integer('sms_code')->nullable();
			$table->boolean('stop_rebook')->nullable();
			$table->boolean('delete')->nullable();
			$table->string('street', 191)->nullable();
			$table->string('city', 191)->nullable();
			$table->string('post_code', 191)->nullable();
			$table->string('country', 191)->nullable();
			$table->string('child_first_name', 191)->nullable();
			$table->string('child_surname', 191)->nullable();
			$table->string('child_email', 191)->nullable();
			$table->bigInteger('school_year_id')->unsigned()->nullable();
			$table->integer('school_academic_year_id')->nullable();
			$table->boolean('hold_locker')->default(0);
			$table->integer('hold_sales_id')->nullable();
			$table->dateTime('booking_expiry')->nullable();
			$table->timestamps();
            $table->tinyInteger('resend_request')->default(0);

        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sales');
	}

}
