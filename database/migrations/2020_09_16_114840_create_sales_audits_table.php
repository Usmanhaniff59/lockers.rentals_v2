<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_audits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('sale_id')->nullable()->index();
            $table->bigInteger('user_id')->nullable()->index();
            $table->bigInteger('booking_id')->nullable()->index();
            $table->string('number', 100)->nullable();
            $table->string('net_pass', 12)->nullable();
            $table->bigInteger('locker_id')->unsigned()->nullable()->index();
            $table->bigInteger('block_id')->unsigned()->nullable()->index();
            $table->string('block_name')->nullable();
            $table->bigInteger('company_id')->unsigned()->nullable()->index();
            $table->dateTime('start')->nullable()->index();
            $table->dateTime('end')->nullable()->index();
            $table->decimal('price', 7,2)->nullable();
            $table->string('email', 191)->nullable()->index();
            $table->string('status', 1)->nullable()->index();
            $table->string('street', 191)->nullable();
            $table->string('city', 191)->nullable();
            $table->string('post_code', 191)->nullable();
            $table->string('country', 191)->nullable();
            $table->string('child_first_name', 191)->nullable();
            $table->string('child_surname', 191)->nullable();
            $table->string('child_email', 191)->nullable();
            $table->bigInteger('school_year_id')->unsigned()->nullable();
            $table->bigInteger('school_academic_year_id')->nullable();
            $table->string('type',11)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_audits');
    }
}
