<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->index('name');
            $table->index('surname');
            $table->index('username');
            $table->index('franchisee_id');
            $table->index('company_id');
            $table->index('active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->dropIndex(['name']);
            $table->dropIndex(['surname']);
            $table->dropIndex(['username']);
            $table->dropIndex(['franchisee_id']);
            $table->dropIndex(['company_id']);
            $table->dropIndex(['active']);
        });
    }
}
