<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmailsTable extends Migration {
	public function up()
	{
		Schema::create('emails', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('email', 191)->nullable();
            $table->string('phone_number', 40)->nullable();
			$table->enum('action', array('inbox','sent'));
			$table->string('subject', 191);
			$table->text('mail_body');
			$table->text('sms_body')->nullable();
			$table->date('date_added');
			$table->enum('type', array('email','sms'));
			$table->string('result_code', 191);
			$table->string('star', 191);
			$table->string('send', 191);
			$table->string('complete', 191);
			$table->string('deleted', 191);
			$table->tinyInteger('fail')->default(0);
			$table->bigInteger('email_folder_id')->unsigned()->default(0);
			$table->timestamps();

		});
	}
	public function down()
	{
		Schema::drop('emails');
	}
}
