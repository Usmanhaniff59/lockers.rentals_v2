<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payments', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->decimal('amount', 18,2)->nullable();
			$table->decimal('account_balance', 18,2)->nullable();
			$table->bigInteger('user_id')->unsigned()->nullable()->index();
			$table->bigInteger('booking_id')->unsigned()->nullable();
			$table->string('billing_address1', 100)->nullable();
			$table->string('billing_address2', 100)->nullable();
			$table->string('billing_city', 40)->nullable();
			$table->string('billing_country', 100)->nullable();
			$table->string('billing_first_name', 20)->nullable();
			$table->string('billing_phone', 20)->nullable();
			$table->string('billing_post_code', 10)->nullable();
			$table->string('billing_state', 100)->nullable();
			$table->string('billing_surname', 20)->nullable();
			$table->dateTime('created')->nullable();
			$table->string('currency', 3)->nullable();
			$table->string('customer_email', 200)->nullable();
			$table->boolean('deleted')->nullable();
            $table->tinyInteger('waiting_response_sagepay')->default(0);
            $table->tinyInteger('has_transaction')->default(0);
 			$table->timestamps();

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payments');
	}

}