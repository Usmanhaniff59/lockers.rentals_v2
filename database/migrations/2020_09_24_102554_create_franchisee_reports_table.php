<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFranchiseeReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('franchisee_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('month');
            $table->integer('year');
            $table->bigInteger('franchisee_id');
            $table->integer('new_customer')->default(0);
            $table->integer('customer')->default(0);
            $table->integer('new_lead')->default(0);
            $table->integer('lead')->default(0);
            $table->integer('new_prospective')->default(0);
            $table->integer('prospective')->default(0);
            $table->integer('total')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('franchisee_reports');
    }
}