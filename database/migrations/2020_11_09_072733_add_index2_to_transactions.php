<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndex2ToTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->index('vendor_tx_code');
            $table->index('currency');
            $table->index('transaction_id');
            $table->index('waiting_response_sagepay');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropIndex(['vendor_tx_code']);
            $table->dropIndex(['currency']);
            $table->dropIndex(['transaction_id']);
            $table->dropIndex(['waiting_response_sagepay']);
        });
    }
}
