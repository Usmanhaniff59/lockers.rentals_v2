<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSagePayTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sage_pay_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('sage_pay_balance_id')->index();
            $table->string('transaction_id')->index();
            $table->tinyInteger('exist')->default('0')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sage_pay_transactions');
    }
}
