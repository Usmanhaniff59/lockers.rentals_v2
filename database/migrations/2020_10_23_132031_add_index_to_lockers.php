<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToLockers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lockers', function (Blueprint $table) {

            $table->index('locker_number');
            $table->index('locker_order');
            $table->index('active');
            $table->index('display');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lockers', function (Blueprint $table) {

            $table->dropIndex(['locker_number']);
            $table->dropIndex(['locker_order']);
            $table->dropIndex(['active']);
            $table->dropIndex(['display']);
        });
    }
}
