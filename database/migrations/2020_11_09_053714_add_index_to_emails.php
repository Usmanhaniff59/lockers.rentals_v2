<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToEmails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('emails', function (Blueprint $table) {
            $table->index('email');
            $table->index('send');
            $table->index('complete');
             $table->index('date_added');
            $table->index('type');
            $table->index('email_folder_id');
            $table->index('fail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('emails', function (Blueprint $table) {
            $table->dropIndex(['email']);
            $table->dropIndex(['send']);
            $table->dropIndex(['complete']);
             $table->dropIndex(['date_added']);
            $table->dropIndex(['type']);
            $table->dropIndex(['email_folder_id']);
            $table->dropIndex(['fail']);
        });
    }
}
