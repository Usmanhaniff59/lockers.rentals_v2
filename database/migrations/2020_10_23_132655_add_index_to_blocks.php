<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToBlocks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blocks', function (Blueprint $table) {

            $table->index('rate_id');
            $table->index('name');
            $table->index('school_academic_year_id');
            $table->index('active');
            $table->index('display');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blocks', function (Blueprint $table) {

            $table->dropIndex(['rate_id']);
            $table->dropIndex(['name']);
            $table->dropIndex(['school_academic_year_id']);
            $table->dropIndex(['active']);
            $table->dropIndex(['display']);
        });
    }
}
