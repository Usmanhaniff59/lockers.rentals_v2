<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToMarketingPictures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('marketing_pictures', function (Blueprint $table) {
            $table->index('active');
            $table->index('deleted');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('marketing_pictures', function (Blueprint $table) {
            $table->dropIndex(['active']);
            $table->dropIndex(['deleted']);

        });
    }
}
