<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BlockGroupCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('block_group_code', function (Blueprint $table) {
            $table->bigIncrements('id');
             $table->string('code', 10)->nullable();
            $table->integer('block_group_id')->nullable()->index();
            // $table->integer('block_id')->nullable()->index();
            $table->dateTime('start')->nullable();
            $table->dateTime('end')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('block_group_code');
    }
}
