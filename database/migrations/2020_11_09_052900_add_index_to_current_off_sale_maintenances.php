<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToCurrentOffSaleMaintenances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('current_off_sale_maintenances', function (Blueprint $table) {
            $table->index('company_id');
            $table->index('franchise_id');
            $table->index('block_id');
            $table->index('locker_id');
            $table->index('start_date');
            $table->index('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('current_off_sale_maintenances', function (Blueprint $table) {
            $table->dropIndex(['company_id']);
            $table->dropIndex(['franchise_id']);
            $table->dropIndex(['block_id']);
            $table->dropIndex(['locker_id']);
            $table->dropIndex(['start_date']);
            $table->dropIndex(['type']);
        });
    }
}
