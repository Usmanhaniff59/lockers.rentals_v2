<?php

use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('languages')->delete();
        
        \DB::table('languages')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'English',
                'native_name' => 'English',
                'flag' => 'gb.svg',
                'locale' => 'en',
                'canonical_locale' => 'en_GB',
                'full_locale' => 'en_GB.UTF-8',
                'status' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'French',
                'native_name' => 'French',
                'flag' => 'gb.svg',
                'locale' => 'fr',
                'canonical_locale' => 'fr_FR',
                'full_locale' => 'fr_FR.UTF-8',
                'status' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Italian',
                'native_name' => 'Italian',
                'flag' => 'it.svg',
                'locale' => 'it',
                'canonical_locale' => 'it_IT',
                'full_locale' => 'it_IT.UTF-8',
                'status' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}