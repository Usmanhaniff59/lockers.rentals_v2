<?php

use Illuminate\Database\Seeder;

class RequestAvailabilitiesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('request_availabilities')->delete();
        
        \DB::table('request_availabilities')->insert(array (
            0 => 
            array (
                'id' => 1,
                'type' => 'Schools',
                'location' => 'lahore',
                'town' => 'lahore',
                'first_name' => 'sufyna',
                'surname' => 'aslam',
                'email' => 'sufyanhashmi301@gmail.com',
                'notes' => 'qq',
                'emailed' => 0,
                'created_at' => '2019-09-23 02:34:05',
                'updated_at' => '2019-09-23 02:34:05',
            ),
            1 => 
            array (
                'id' => 2,
                'type' => 'Schools',
                'location' => 'dsf',
                'town' => 'lahore',
                'first_name' => 'sufyna',
                'surname' => 'aslam',
                'email' => 'sufyanhashmi301@gmail.com',
                'notes' => 'aa',
                'emailed' => 0,
                'created_at' => '2019-10-17 06:55:26',
                'updated_at' => '2019-10-17 06:55:26',
            ),
            2 => 
            array (
                'id' => 3,
                'type' => 'Schools',
                'location' => 'lahore',
                'town' => 'lahore',
                'first_name' => 'sufyna',
                'surname' => 'aslam',
                'email' => 'sufyanhashmi301@gmail.com',
                'notes' => 'aaa',
                'emailed' => 0,
                'created_at' => '2019-10-23 02:18:31',
                'updated_at' => '2019-10-23 02:18:31',
            ),
            3 => 
            array (
                'id' => 4,
                'type' => 'Schools',
                'location' => 'lahore',
                'town' => 'lahore',
                'first_name' => 'sufyna',
                'surname' => 'aslam',
                'email' => 'sufyanhashmi301@gmail.com',
                'notes' => 'aa',
                'emailed' => 0,
                'created_at' => '2019-10-23 02:18:46',
                'updated_at' => '2019-10-23 02:18:46',
            ),
            4 => 
            array (
                'id' => 5,
                'type' => 'Schools',
                'location' => 'lahore',
                'town' => 'lahore',
                'first_name' => 'sufyna',
                'surname' => 'aslam',
                'email' => 'sufyanhashmi301@gmail.com',
                'notes' => 'aaa',
                'emailed' => 0,
                'created_at' => '2019-11-25 02:35:24',
                'updated_at' => '2019-11-25 02:35:24',
            ),
            5 => 
            array (
                'id' => 6,
                'type' => 'Schools',
                'location' => 'lahore',
                'town' => 'lahore',
                'first_name' => 'sufyna',
                'surname' => 'aslam',
                'email' => 'sufyanhashmi301@gmail.com',
                'notes' => 'aaa',
                'emailed' => 0,
                'created_at' => '2019-11-25 02:40:03',
                'updated_at' => '2019-11-25 02:40:03',
            ),
            6 => 
            array (
                'id' => 7,
                'type' => 'Schools',
                'location' => 'lahores',
                'town' => 'lahore',
                'first_name' => 'sufyna',
                'surname' => 'aslam',
                'email' => 'sufyanhashmi301@gmail.com',
                'notes' => 'ss',
                'emailed' => 0,
                'created_at' => '2019-11-25 03:25:44',
                'updated_at' => '2019-11-25 03:25:44',
            ),
            7 => 
            array (
                'id' => 8,
                'type' => 'Schools',
                'location' => 'Copthall School',
                'town' => 'lahore',
                'first_name' => 'aa',
                'surname' => 'aa',
                'email' => 'sufyanhashmi301@gmail.com',
                'notes' => 'aaa',
                'emailed' => 0,
                'created_at' => '2020-01-17 07:56:47',
                'updated_at' => '2020-01-17 07:56:47',
            ),
            8 => 
            array (
                'id' => 9,
                'type' => 'Schools',
                'location' => 'Copthall School',
                'town' => 'lahore',
                'first_name' => 'sss',
                'surname' => 'aslam',
                'email' => 'sufyanhashmi301@gmail.com',
                'notes' => 'ss',
                'emailed' => 0,
                'created_at' => '2020-01-17 08:05:51',
                'updated_at' => '2020-01-17 08:05:51',
            ),
            9 => 
            array (
                'id' => 10,
                'type' => 'Schools',
                'location' => 'aa',
                'town' => 'dd',
                'first_name' => 'dd',
                'surname' => 'dd',
                'email' => 'sufyanhashmi301@gmail.com',
                'notes' => 'adfas',
                'emailed' => 0,
                'created_at' => '2020-03-19 07:03:03',
                'updated_at' => '2020-03-19 07:03:03',
            ),
            10 => 
            array (
                'id' => 11,
                'type' => 'Schools',
                'location' => 'aa',
                'town' => 'dd',
                'first_name' => 'dd',
                'surname' => 'dd',
                'email' => 'sufyanhashmi301@gmail.com',
                'notes' => 'adfas',
                'emailed' => 0,
                'created_at' => '2020-03-19 07:07:11',
                'updated_at' => '2020-03-19 07:07:11',
            ),
            11 => 
            array (
                'id' => 12,
                'type' => 'Schools',
                'location' => 'aa',
                'town' => 'dd',
                'first_name' => 'dd',
                'surname' => 'dd',
                'email' => 'sufyanhashmi301@gmail.com',
                'notes' => 'adfas',
                'emailed' => 0,
                'created_at' => '2020-03-19 07:35:19',
                'updated_at' => '2020-03-19 07:35:19',
            ),
            12 => 
            array (
                'id' => 13,
                'type' => 'Schools',
                'location' => 'England',
                'town' => 'Oldham',
                'first_name' => 'Hadia',
                'surname' => 'Naveed',
                'email' => 'hadiaaanvd@gmail.com',
                'notes' => 'asjdbasb',
                'emailed' => 0,
                'created_at' => '2020-09-15 11:49:32',
                'updated_at' => '2020-09-15 11:49:32',
            ),
        ));
        
        
    }
}