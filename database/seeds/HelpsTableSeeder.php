<?php

use Illuminate\Database\Seeder;

class HelpsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('helps')->delete();
        
        \DB::table('helps')->insert(array (
            0 => 
            array (
                'id' => 4,
                'title' => 'Email Templates',
                'detail' => 'Ability to alter standard systems templates or create templates to mass email users',
                'page_url' => '/admin/emailtemplates',
                'created_at' => '2019-08-20 13:09:51',
                'updated_at' => '2020-03-23 21:16:42',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 5,
                'title' => 'Default Help',
                'detail' => 'This is default help which is shown if there is no help entered for this page.',
                'page_url' => 'default',
                'created_at' => '2019-08-20 13:10:52',
                'updated_at' => '2019-08-20 13:10:52',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 6,
                'title' => 'Admin Dashboard Help',
                'detail' => 'Dashboard HElp2',
                'page_url' => '/admin',
                'created_at' => '2019-08-21 10:44:40',
                'updated_at' => '2019-08-21 11:02:06',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 7,
                'title' => 'Permission Help',
                'detail' => 'This is default help which is shown if there is no help entered for this page.',
                'page_url' => '/admin/permissions',
                'created_at' => '2019-08-21 11:13:02',
                'updated_at' => '2019-08-21 11:13:02',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 8,
                'title' => 'Default Help Create',
                'detail' => 'This is default help which is shown if there is no help entered for this page.',
                'page_url' => '/admin/helps',
                'created_at' => '2019-08-22 05:35:34',
                'updated_at' => '2019-08-22 05:35:34',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 10,
                'title' => 'Default Help edit',
                'detail' => 'This is default help which is shown if there is no help entered for this page.11',
                'page_url' => '/admin/pages/edit',
                'created_at' => '2019-08-22 05:44:52',
                'updated_at' => '2019-09-21 11:17:18',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 11,
                'title' => 'Default Help Page Create',
                'detail' => 'This is default help which is shown if there is no help entered for this page.2',
                'page_url' => '/admin/pages/create',
                'created_at' => '2019-08-22 05:48:13',
                'updated_at' => '2019-08-22 05:57:33',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 13,
                'title' => 'Company Management',
                'detail' => 'List of companies that includes the ability to manage the lockers',
                'page_url' => '/admin/company',
                'created_at' => '2020-03-23 21:12:44',
                'updated_at' => '2020-03-23 21:12:44',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 14,
                'title' => 'Orders',
                'detail' => 'All order history including the lockers booked and the account transactions',
                'page_url' => '/admin/payment',
                'created_at' => '2020-03-23 21:13:34',
                'updated_at' => '2020-03-23 21:13:34',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 15,
                'title' => 'User',
                'detail' => 'List of all users that have with made bookings or have been assigned portal administer access',
                'page_url' => '/admin/users',
                'created_at' => '2020-03-23 21:14:35',
                'updated_at' => '2020-03-23 21:14:46',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 16,
                'title' => 'Booking Groups',
                'detail' => 'Names of four in the system that enable different system functions',
                'page_url' => '/admin/bookingtype',
                'created_at' => '2020-03-23 21:16:03',
                'updated_at' => '2020-03-23 21:16:03',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 17,
                'title' => 'Distribution Lists',
                'detail' => 'List of  user groups that can be used to add groups of users to templates.',
                'page_url' => '/admin/distributionlists',
                'created_at' => '2020-03-23 21:17:55',
                'updated_at' => '2020-03-23 21:17:55',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}