<?php

use Illuminate\Database\Seeder;

class DeveloperErrorsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('developer_errors')->delete();
        
        \DB::table('developer_errors')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 18649,
                'sale_id' => NULL,
                'booking_id' => 131000,
                'error_type' => 'Not Equal Account Balance',
                'details' => 'Booking id 131000 has paid 22.00 and refunded 0 but account balance is not equal to current account_balance column in payments. Current account balance : 22.00 new account balance 0',
                'resolved' => 0,
                'created_at' => '2020-08-21 08:46:35',
                'updated_at' => '2020-08-21 08:46:35',
            ),
            1 => 
            array (
                'id' => 2,
                'user_id' => 18649,
                'sale_id' => NULL,
                'booking_id' => 131000,
                'error_type' => 'Not Equal Account Balance',
                'details' => 'Booking id 131000 has paid 0 and refunded 0 but account balance is not equal to current account_balance column in payments. Current account balance : 22.00 new account balance 0.00',
                'resolved' => 0,
                'created_at' => '2020-08-21 13:11:51',
                'updated_at' => '2020-08-21 13:11:51',
            ),
            2 => 
            array (
                'id' => 3,
                'user_id' => 2420,
                'sale_id' => NULL,
                'booking_id' => 131002,
                'error_type' => 'Not Equal Account Balance',
                'details' => 'Booking id 131002 has paid 22.00 and refunded 0 but account balance is not equal to current account_balance column in payments. Current account balance : 0.00 new account balance 22.00',
                'resolved' => 0,
                'created_at' => '2020-08-21 13:11:51',
                'updated_at' => '2020-08-21 13:11:51',
            ),
        ));
        
        
    }
}