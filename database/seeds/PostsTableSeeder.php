<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('posts')->delete();
        
        \DB::table('posts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'aaa',
                'body' => 'aaaa',
                'created_at' => '2019-09-23 02:39:29',
                'updated_at' => '2019-09-23 02:39:29',
            ),
        ));
        
        
    }
}