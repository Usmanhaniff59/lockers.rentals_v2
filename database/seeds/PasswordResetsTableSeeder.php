<?php

use Illuminate\Database\Seeder;

class PasswordResetsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('password_resets')->delete();
        
        \DB::table('password_resets')->insert(array (
            0 => 
            array (
                'email' => 'richard@etchintime.com',
                'token' => '$2y$10$iQcCDYIBNPJ1c5ZSyRT85e262pHslPe9NJn4BwKpss1eGR1wf/QGO',
                'created_at' => '2020-04-09 19:17:25',
            ),
            1 => 
            array (
                'email' => 'rperris@xweb4u.com',
                'token' => '$2y$10$66xJ/.pCNEKKxqoRqZ2ThOrtw8UDcG78Va1iXhy.VyminoBNu9sOm',
                'created_at' => '2020-04-14 20:58:06',
            ),
            2 => 
            array (
                'email' => 'superadmin@xweb4u.com',
                'token' => '$2y$10$mkCnbXIF7jiGN7GiTmkutOVQwYPcp0jntFa4KjAY1vzOZ53.2ye/i',
                'created_at' => '2020-09-09 14:15:11',
            ),
            3 => 
            array (
                'email' => 'admin@xweb4u.com',
                'token' => '$2y$10$oQjxz3DTR3x45PLyqUt4G.mxjlGOEVmt9.LPCPnbO3f83UXEeOD76',
                'created_at' => '2020-09-10 19:05:30',
            ),
        ));
        
        
    }
}