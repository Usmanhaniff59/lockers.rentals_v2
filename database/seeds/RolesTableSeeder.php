<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Super Admin',
                'created_at' => '2019-07-17 02:53:33',
                'updated_at' => '2020-03-19 09:47:38',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Admin',
                'created_at' => '2019-07-17 02:58:06',
                'updated_at' => '2020-03-20 00:53:16',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Company',
                'created_at' => '2019-07-28 21:38:26',
                'updated_at' => '2020-03-19 10:15:40',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'User',
                'created_at' => '2019-07-30 02:26:21',
                'updated_at' => '2020-03-20 00:53:31',
            ),
            4 => 
            array (
                'id' => 10,
                'name' => 'Franchisee',
                'created_at' => '2019-07-30 02:26:21',
                'updated_at' => '2020-03-20 00:53:31',
            ),
        ));
        
        
    }
}