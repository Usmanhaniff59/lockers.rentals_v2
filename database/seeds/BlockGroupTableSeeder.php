<?php

use Illuminate\Database\Seeder;

class BlockGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('block_groups')->truncate();
        \DB::table('block_group_code')->truncate();
        \DB::table('manual_locker_codes')->whereNotNull('block_group_id')->delete();
        \DB::table('blocks')->whereNotNull('block_group')->update(array('block_group' => NULL));
    }
}
