<?php

use Illuminate\Database\Seeder;

class DistributionListGroupMembersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('distribution_list_group_members')->delete();
        
        \DB::table('distribution_list_group_members')->insert(array (
            0 => 
            array (
                'id' => 1,
                'distribution_list_group_id' => 4,
                'user_id' => 1,
                'description' => NULL,
                'created_at' => '2020-01-17 13:20:28',
                'updated_at' => '2020-01-17 13:20:28',
            ),
            1 => 
            array (
                'id' => 2,
                'distribution_list_group_id' => 4,
                'user_id' => 36,
                'description' => NULL,
                'created_at' => '2020-01-17 13:20:28',
                'updated_at' => '2020-01-17 13:20:28',
            ),
            2 => 
            array (
                'id' => 3,
                'distribution_list_group_id' => 2,
                'user_id' => 1,
                'description' => NULL,
                'created_at' => '2020-02-27 07:02:46',
                'updated_at' => '2020-02-27 07:02:46',
            ),
            3 => 
            array (
                'id' => 4,
                'distribution_list_group_id' => 2,
                'user_id' => 36,
                'description' => NULL,
                'created_at' => '2020-02-27 07:02:46',
                'updated_at' => '2020-02-27 07:02:46',
            ),
            4 => 
            array (
                'id' => 5,
                'distribution_list_group_id' => 2,
                'user_id' => 38,
                'description' => NULL,
                'created_at' => '2020-02-27 07:02:46',
                'updated_at' => '2020-02-27 07:02:46',
            ),
        ));
        
        
    }
}