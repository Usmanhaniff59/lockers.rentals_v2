<?php

use Illuminate\Database\Seeder;

class CharityTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('charity')->delete();
        
        \DB::table('charity')->insert(array (
            0 => 
            array (
                'id' => 1,
                'charity_name' => ' ',
                'default_amount' => '1',
                'charity_logo' => 'wwf.png',
                'charity_video' => 'https://www.youtube.com/embed/ZsTT-Sy56ks',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}