<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LocationGroupsTableSeeder::class);
        $this->call(AttachmentsTableSeeder::class);
        $this->call(CompaniesTableSeeder::class);
        $this->call(CompanyYearsTableSeeder::class);
        $this->call(BlockYearsTableSeeder::class);
        $this->call(BlocksTableSeeder::class);
        $this->call(LockersTableSeeder::class);
        $this->call(FranchiseeCompaniesTableSeeder::class);
        $this->call(FranchiseesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CharityTableSeeder::class);
        $this->call(CharityTransactionsTableSeeder::class);
        $this->call(CronSchedulesTableSeeder::class);
        $this->call(CurrenciesTableSeeder::class);
        $this->call(DistributionListGroupMembersTableSeeder::class);
        $this->call(DistributionListGroupsTableSeeder::class);
        $this->call(EmailFoldersTableSeeder::class);
        $this->call(EmailTemplateAltsTableSeeder::class);
        $this->call(EmailTemplateDistributionListsTableSeeder::class);
        $this->call(EmailTemplatesTableSeeder::class);
        $this->call(FrontendPlaceholdersTableSeeder::class);
        $this->call(EmailTemplatePlaceHoldersTableSeeder::class);
        $this->call(HelpsTableSeeder::class);
        $this->call(LanguagesTableSeeder::class);
        $this->call(LockerBlockImageTableSeeder::class);
        $this->call(MaintenancesTableSeeder::class);
        $this->call(MarketingPicturesTableSeeder::class);
        $this->call(PagesTableSeeder::class);
        $this->call(PasswordResetsTableSeeder::class);
        $this->call(PaymentDiscountsTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(RatesTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(RoleHasPermissionsTableSeeder::class);
        $this->call(UserHasPermissionsTableSeeder::class);
        $this->call(UserHasRolesTableSeeder::class);
        $this->call(SalesTableSeeder::class);
        $this->call(TextsTableSeeder::class);
        $this->call(SchoolAcademicYearsTableSeeder::class);
        $this->call(SchoolRequestsTableSeeder::class);
        $this->call(SchoolYearTableSeeder::class);
        $this->call(ManualLockerCodesTableSeeder::class);
        $this->call(CompanyBlockErrorsTableSeeder::class);
        $this->call(DeveloperErrorsTableSeeder::class);
        $this->call(EmailsTableSeeder::class);
        //$this->call(JobsTableSeeder::class);
        $this->call(LockerMovesTableSeeder::class);
        $this->call(LockerOffSalesTableSeeder::class);
        $this->call(LockerRenumbersTableSeeder::class);
        $this->call(SalesReportsTableSeeder::class);
        $this->call(PaymentsTableSeeder::class);
        $this->call(RequestAvailabilitiesTableSeeder::class);
        $this->call(SlidersTableSeeder::class);
        $this->call(TransactionsTableSeeder::class);
        $this->call(VoucherCodesTableSeeder::class);
        $this->call(VoucherCodeUsersTableSeeder::class);
        $this->call(SalesAuditsTableSeeder::class);
        
        $this->call(ChatTypesTableSeeder::class);
        $this->call(DocumentTypesTableSeeder::class);
        // after 16th Feb 2021

        $this->call(BlockGroupTableSeeder::class);
        $this->call(SalesUpdateTableSeeder::class);
        
        
    }
}