<?php

use Illuminate\Database\Seeder;

class EmailsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('emails')->delete();
        
        \DB::table('emails')->insert(array (
            0 => 
            array (
                'id' => 20,
                'email' => 'admin@xweb4u.com',
                'phone_number' => NULL,
                'action' => 'sent',
                'subject' => 'Confirmation Email',
                'mail_body' => '<div style="line-height: 1.5rem;">Dear admin </div>
<div style="line-height: 1.5rem;">&nbsp;</div>
<div style="line-height: 1.5rem;"><strong>Your booked</strong></div>
<div style="line-height: 1.5rem;">
<table style="border-collapse: collapse; width: 629px; height: 86px;" border="1">
<tbody>
<tr>
<td style="width: 200px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">Reservation ID</td>
<td style="width: 422px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">156239</td>
</tr>
<tr>
<td style="width: 200px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">Email</td>
<td style="width: 422px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">admin@xweb4u.com</td>
</tr>
<tr>
<td style="width: 200px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">Total Cost</td>
<td style="width: 422px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">19.00</td>
</tr>
</tbody>
</table>
</div>
<div style="line-height: 1.5rem;"> <br />
<div style="line-height: 1.5rem;"><strong>aa aa Locker Details</strong></div>
<div style="line-height: 1.5rem;">
<table style="border-collapse: collapse; width: 632px; height: 220px;" border="1">
<tbody>
<tr>
<td style="width: 200px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">Start</td>
<td style="width: 422px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">01 Sep, 2020</td>
</tr>
<tr>
<td style="width: 200px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">End</td>
<td style="width: 422px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">01 Aug, 2021</td>
</tr>
<tr>
<td style="width: 200px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">Schools Bookings</td>
<td style="width: 422px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">Copthall School</td>
</tr>
<tr>
<td style="width: 200px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">Block</td>
<td style="width: 422px; border: 1px solid lightgray !important; padding: 7px;">Block A</td>
</tr>
<tr>
<td style="width: 200px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">Locker Number</td>
<td style="width: 422px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">1</td>
</tr>
<tr>
<td style="width: 200px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">Locker Code</td>
<td style="width: 422px; border: 1px solid lightgray !important; padding: 7px;">7140</td>
</tr>
<tr>
<td style="width: 200px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">Cost</td>
<td style="width: 422px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">18.00</td>
</tr>
</tbody>
</table>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
<div style="line-height: 1.5rem;"> </div>
<div style="line-height: 1.5rem;"><strong>HOW TO REPLACE YOUR LOCKER CODE:</strong></div>
<div style="line-height: 1.5rem;">Go to the locker 1</div>
<div style="line-height: 1.5rem;">Enter the code (7140) and open the lock</div>
<div style="line-height: 1.5rem;">1. With lock unlocked press and hold the button on the side of the lock</div>
<div style="line-height: 1.5rem;">2. Whilst still holding the button turn the dials to the desired code</div>
<div style="line-height: 1.5rem;">3. Release the button on the side of the lock</div>
<div style="line-height: 1.5rem;">&nbsp;</div>
<div style="line-height: 1.5rem;">This rental agreement is between Prefect Rentals and the lessee</div>
<div style="line-height: 1.5rem;">&nbsp;</div>
<div style="line-height: 1.5rem;">The rental period is from the point of rental (regardless of date) until the end of the</div>
<div style="line-height: 1.5rem;">current school year. Reduced rates for short date ranges are not available</div>
<div style="line-height: 1.5rem;">Rental fees are non-refundable</div>
<div style="line-height: 1.5rem;">Rental of lockers is non-transferrable</div>
<div style="line-height: 1.5rem;">All locker components including the electronic lock and batteries remain the property of</div>
<div style="line-height: 1.5rem;">Prefect Rentals and cannot be removed from site</div>
<div style="line-height: 1.5rem;">The locker can only be used by the lessee and lockers cannot be shared</div>
<div style="line-height: 1.5rem;">Lockers can only be used for the storage of personal items that belong to the lessee and</div>
<div style="line-height: 1.5rem;">must not include illegal items or any item prohibited by the school</div>
<div style="line-height: 1.5rem;">Prefect will not be held liable for any loss, damage or theft of items kept in the lockers.</div>
<div style="line-height: 1.5rem;">The school has the right to inspect the contents of the locker without prior notice</div>
<div style="line-height: 1.5rem;">At the end of the school year all items must be removed from the locker and the locker</div>
<div style="line-height: 1.5rem;">left empty. Any items left in the locker will be removed and disposed of without exception</div>
<div style="line-height: 1.5rem;">The locker should be left clean and in good condition</div>
<div style="line-height: 1.5rem;">Any damage or vandalism must be reported to the school immediately</div>
<div style="line-height: 1.5rem;">Codes can be reset or forgotten codes reissued by visiting Locker.Rentals</div>
<div style="line-height: 1.5rem;">For assistance or support issues (other than to retrieve your code) please contact us on</div>
<div style="line-height: 1.5rem;">0330 311 1003 or rentals@prefectlockers.com</div>
<div style="line-height: 1.5rem;">&nbsp;</div>
<div style="line-height: 1.5rem;">&nbsp;</div>
</div>',
                'sms_body' => NULL,
                'date_added' => '2020-05-05',
                'type' => 'email',
                'result_code' => '0',
                'star' => '0',
                'send' => '0',
                'complete' => '0',
                'deleted' => '0',
                'email_folder_id' => 0,
                'created_at' => '2020-05-05 22:52:14',
                'updated_at' => '2020-05-05 22:52:14',
            ),
        ));
        
        
    }
}