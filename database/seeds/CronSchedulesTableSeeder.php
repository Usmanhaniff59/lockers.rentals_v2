<?php

use Illuminate\Database\Seeder;

class CronSchedulesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cron_schedules')->delete();
        
        \DB::table('cron_schedules')->insert(array (
            0 => 
            array (
                'id' => 13,
                'class' => 'lockerBooking',
                'controller' => 'LockersController',
                'where_check' => '0',
                'due' => '2019-11-28 15:16:09',
                'completed' => '0',
                'status' => 'Pending',
                'created_at' => '2019-11-28 15:16:09',
                'updated_at' => '2019-11-28 15:16:09',
            ),
            1 => 
            array (
                'id' => 14,
                'class' => 'Email',
                'controller' => 'LockersController',
                'where_check' => '0',
                'due' => '2020-01-17 12:56:47',
                'completed' => '0',
                'status' => 'Pending',
                'created_at' => '2020-01-17 12:56:47',
                'updated_at' => '2020-01-17 12:56:47',
            ),
        ));
        
        
    }
}