<?php

use Illuminate\Database\Seeder;

class BlockYearsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('block_years')->delete();
        
        \DB::table('block_years')->insert(array (
            0 => 
            array (
                'id' => 1,
                'company_id' => 1,
                'block_id' => 308,
                'school_academic_year_id' => 2,
            ),
            1 => 
            array (
                'id' => 2,
                'company_id' => 1,
                'block_id' => 308,
                'school_academic_year_id' => 3,
            ),
            2 => 
            array (
                'id' => 3,
                'company_id' => 1,
                'block_id' => 308,
                'school_academic_year_id' => 4,
            ),
            3 => 
            array (
                'id' => 4,
                'company_id' => 1,
                'block_id' => 308,
                'school_academic_year_id' => 5,
            ),
            4 => 
            array (
                'id' => 5,
                'company_id' => 1,
                'block_id' => 308,
                'school_academic_year_id' => 6,
            ),
            5 => 
            array (
                'id' => 11,
                'company_id' => 5,
                'block_id' => 5,
                'school_academic_year_id' => 2,
            ),
            6 => 
            array (
                'id' => 12,
                'company_id' => 5,
                'block_id' => 5,
                'school_academic_year_id' => 3,
            ),
            7 => 
            array (
                'id' => 13,
                'company_id' => 5,
                'block_id' => 5,
                'school_academic_year_id' => 4,
            ),
            8 => 
            array (
                'id' => 14,
                'company_id' => 5,
                'block_id' => 5,
                'school_academic_year_id' => 5,
            ),
            9 => 
            array (
                'id' => 15,
                'company_id' => 5,
                'block_id' => 5,
                'school_academic_year_id' => 6,
            ),
            10 => 
            array (
                'id' => 16,
                'company_id' => 6,
                'block_id' => 6,
                'school_academic_year_id' => 2,
            ),
            11 => 
            array (
                'id' => 17,
                'company_id' => 6,
                'block_id' => 50,
                'school_academic_year_id' => 2,
            ),
            12 => 
            array (
                'id' => 18,
                'company_id' => 6,
                'block_id' => 6,
                'school_academic_year_id' => 3,
            ),
            13 => 
            array (
                'id' => 19,
                'company_id' => 6,
                'block_id' => 50,
                'school_academic_year_id' => 3,
            ),
            14 => 
            array (
                'id' => 20,
                'company_id' => 6,
                'block_id' => 6,
                'school_academic_year_id' => 4,
            ),
            15 => 
            array (
                'id' => 21,
                'company_id' => 6,
                'block_id' => 50,
                'school_academic_year_id' => 4,
            ),
            16 => 
            array (
                'id' => 22,
                'company_id' => 6,
                'block_id' => 6,
                'school_academic_year_id' => 5,
            ),
            17 => 
            array (
                'id' => 23,
                'company_id' => 6,
                'block_id' => 50,
                'school_academic_year_id' => 5,
            ),
            18 => 
            array (
                'id' => 24,
                'company_id' => 6,
                'block_id' => 6,
                'school_academic_year_id' => 6,
            ),
            19 => 
            array (
                'id' => 25,
                'company_id' => 6,
                'block_id' => 50,
                'school_academic_year_id' => 6,
            ),
            20 => 
            array (
                'id' => 26,
                'company_id' => 7,
                'block_id' => 7,
                'school_academic_year_id' => 2,
            ),
            21 => 
            array (
                'id' => 27,
                'company_id' => 7,
                'block_id' => 7,
                'school_academic_year_id' => 3,
            ),
            22 => 
            array (
                'id' => 28,
                'company_id' => 7,
                'block_id' => 7,
                'school_academic_year_id' => 4,
            ),
            23 => 
            array (
                'id' => 29,
                'company_id' => 7,
                'block_id' => 7,
                'school_academic_year_id' => 5,
            ),
            24 => 
            array (
                'id' => 30,
                'company_id' => 7,
                'block_id' => 7,
                'school_academic_year_id' => 6,
            ),
            25 => 
            array (
                'id' => 31,
                'company_id' => 8,
                'block_id' => 9,
                'school_academic_year_id' => 2,
            ),
            26 => 
            array (
                'id' => 32,
                'company_id' => 8,
                'block_id' => 10,
                'school_academic_year_id' => 2,
            ),
            27 => 
            array (
                'id' => 33,
                'company_id' => 8,
                'block_id' => 9,
                'school_academic_year_id' => 3,
            ),
            28 => 
            array (
                'id' => 34,
                'company_id' => 8,
                'block_id' => 10,
                'school_academic_year_id' => 3,
            ),
            29 => 
            array (
                'id' => 35,
                'company_id' => 8,
                'block_id' => 9,
                'school_academic_year_id' => 4,
            ),
            30 => 
            array (
                'id' => 36,
                'company_id' => 8,
                'block_id' => 10,
                'school_academic_year_id' => 4,
            ),
            31 => 
            array (
                'id' => 37,
                'company_id' => 8,
                'block_id' => 9,
                'school_academic_year_id' => 5,
            ),
            32 => 
            array (
                'id' => 38,
                'company_id' => 8,
                'block_id' => 10,
                'school_academic_year_id' => 5,
            ),
            33 => 
            array (
                'id' => 39,
                'company_id' => 8,
                'block_id' => 9,
                'school_academic_year_id' => 6,
            ),
            34 => 
            array (
                'id' => 40,
                'company_id' => 8,
                'block_id' => 10,
                'school_academic_year_id' => 6,
            ),
            35 => 
            array (
                'id' => 41,
                'company_id' => 9,
                'block_id' => 11,
                'school_academic_year_id' => 2,
            ),
            36 => 
            array (
                'id' => 42,
                'company_id' => 9,
                'block_id' => 11,
                'school_academic_year_id' => 3,
            ),
            37 => 
            array (
                'id' => 43,
                'company_id' => 9,
                'block_id' => 11,
                'school_academic_year_id' => 4,
            ),
            38 => 
            array (
                'id' => 44,
                'company_id' => 9,
                'block_id' => 11,
                'school_academic_year_id' => 5,
            ),
            39 => 
            array (
                'id' => 45,
                'company_id' => 9,
                'block_id' => 11,
                'school_academic_year_id' => 6,
            ),
            40 => 
            array (
                'id' => 46,
                'company_id' => 10,
                'block_id' => 12,
                'school_academic_year_id' => 2,
            ),
            41 => 
            array (
                'id' => 47,
                'company_id' => 10,
                'block_id' => 19,
                'school_academic_year_id' => 2,
            ),
            42 => 
            array (
                'id' => 48,
                'company_id' => 10,
                'block_id' => 12,
                'school_academic_year_id' => 3,
            ),
            43 => 
            array (
                'id' => 49,
                'company_id' => 10,
                'block_id' => 19,
                'school_academic_year_id' => 3,
            ),
            44 => 
            array (
                'id' => 50,
                'company_id' => 10,
                'block_id' => 12,
                'school_academic_year_id' => 4,
            ),
            45 => 
            array (
                'id' => 51,
                'company_id' => 10,
                'block_id' => 19,
                'school_academic_year_id' => 4,
            ),
            46 => 
            array (
                'id' => 52,
                'company_id' => 10,
                'block_id' => 12,
                'school_academic_year_id' => 5,
            ),
            47 => 
            array (
                'id' => 53,
                'company_id' => 10,
                'block_id' => 19,
                'school_academic_year_id' => 5,
            ),
            48 => 
            array (
                'id' => 54,
                'company_id' => 10,
                'block_id' => 12,
                'school_academic_year_id' => 6,
            ),
            49 => 
            array (
                'id' => 55,
                'company_id' => 10,
                'block_id' => 19,
                'school_academic_year_id' => 6,
            ),
            50 => 
            array (
                'id' => 56,
                'company_id' => 11,
                'block_id' => 15,
                'school_academic_year_id' => 2,
            ),
            51 => 
            array (
                'id' => 57,
                'company_id' => 11,
                'block_id' => 17,
                'school_academic_year_id' => 2,
            ),
            52 => 
            array (
                'id' => 58,
                'company_id' => 11,
                'block_id' => 18,
                'school_academic_year_id' => 2,
            ),
            53 => 
            array (
                'id' => 59,
                'company_id' => 11,
                'block_id' => 20,
                'school_academic_year_id' => 2,
            ),
            54 => 
            array (
                'id' => 60,
                'company_id' => 11,
                'block_id' => 21,
                'school_academic_year_id' => 2,
            ),
            55 => 
            array (
                'id' => 61,
                'company_id' => 11,
                'block_id' => 22,
                'school_academic_year_id' => 2,
            ),
            56 => 
            array (
                'id' => 62,
                'company_id' => 11,
                'block_id' => 23,
                'school_academic_year_id' => 2,
            ),
            57 => 
            array (
                'id' => 63,
                'company_id' => 11,
                'block_id' => 81,
                'school_academic_year_id' => 2,
            ),
            58 => 
            array (
                'id' => 64,
                'company_id' => 11,
                'block_id' => 84,
                'school_academic_year_id' => 2,
            ),
            59 => 
            array (
                'id' => 65,
                'company_id' => 11,
                'block_id' => 87,
                'school_academic_year_id' => 2,
            ),
            60 => 
            array (
                'id' => 66,
                'company_id' => 11,
                'block_id' => 89,
                'school_academic_year_id' => 2,
            ),
            61 => 
            array (
                'id' => 67,
                'company_id' => 11,
                'block_id' => 15,
                'school_academic_year_id' => 3,
            ),
            62 => 
            array (
                'id' => 68,
                'company_id' => 11,
                'block_id' => 17,
                'school_academic_year_id' => 3,
            ),
            63 => 
            array (
                'id' => 69,
                'company_id' => 11,
                'block_id' => 18,
                'school_academic_year_id' => 3,
            ),
            64 => 
            array (
                'id' => 70,
                'company_id' => 11,
                'block_id' => 20,
                'school_academic_year_id' => 3,
            ),
            65 => 
            array (
                'id' => 71,
                'company_id' => 11,
                'block_id' => 21,
                'school_academic_year_id' => 3,
            ),
            66 => 
            array (
                'id' => 72,
                'company_id' => 11,
                'block_id' => 22,
                'school_academic_year_id' => 3,
            ),
            67 => 
            array (
                'id' => 73,
                'company_id' => 11,
                'block_id' => 23,
                'school_academic_year_id' => 3,
            ),
            68 => 
            array (
                'id' => 74,
                'company_id' => 11,
                'block_id' => 81,
                'school_academic_year_id' => 3,
            ),
            69 => 
            array (
                'id' => 75,
                'company_id' => 11,
                'block_id' => 84,
                'school_academic_year_id' => 3,
            ),
            70 => 
            array (
                'id' => 76,
                'company_id' => 11,
                'block_id' => 87,
                'school_academic_year_id' => 3,
            ),
            71 => 
            array (
                'id' => 77,
                'company_id' => 11,
                'block_id' => 89,
                'school_academic_year_id' => 3,
            ),
            72 => 
            array (
                'id' => 78,
                'company_id' => 11,
                'block_id' => 15,
                'school_academic_year_id' => 4,
            ),
            73 => 
            array (
                'id' => 79,
                'company_id' => 11,
                'block_id' => 17,
                'school_academic_year_id' => 4,
            ),
            74 => 
            array (
                'id' => 80,
                'company_id' => 11,
                'block_id' => 18,
                'school_academic_year_id' => 4,
            ),
            75 => 
            array (
                'id' => 81,
                'company_id' => 11,
                'block_id' => 20,
                'school_academic_year_id' => 4,
            ),
            76 => 
            array (
                'id' => 82,
                'company_id' => 11,
                'block_id' => 21,
                'school_academic_year_id' => 4,
            ),
            77 => 
            array (
                'id' => 83,
                'company_id' => 11,
                'block_id' => 22,
                'school_academic_year_id' => 4,
            ),
            78 => 
            array (
                'id' => 84,
                'company_id' => 11,
                'block_id' => 23,
                'school_academic_year_id' => 4,
            ),
            79 => 
            array (
                'id' => 85,
                'company_id' => 11,
                'block_id' => 81,
                'school_academic_year_id' => 4,
            ),
            80 => 
            array (
                'id' => 86,
                'company_id' => 11,
                'block_id' => 84,
                'school_academic_year_id' => 4,
            ),
            81 => 
            array (
                'id' => 87,
                'company_id' => 11,
                'block_id' => 87,
                'school_academic_year_id' => 4,
            ),
            82 => 
            array (
                'id' => 88,
                'company_id' => 11,
                'block_id' => 89,
                'school_academic_year_id' => 4,
            ),
            83 => 
            array (
                'id' => 89,
                'company_id' => 11,
                'block_id' => 15,
                'school_academic_year_id' => 5,
            ),
            84 => 
            array (
                'id' => 90,
                'company_id' => 11,
                'block_id' => 17,
                'school_academic_year_id' => 5,
            ),
            85 => 
            array (
                'id' => 91,
                'company_id' => 11,
                'block_id' => 18,
                'school_academic_year_id' => 5,
            ),
            86 => 
            array (
                'id' => 92,
                'company_id' => 11,
                'block_id' => 20,
                'school_academic_year_id' => 5,
            ),
            87 => 
            array (
                'id' => 93,
                'company_id' => 11,
                'block_id' => 21,
                'school_academic_year_id' => 5,
            ),
            88 => 
            array (
                'id' => 94,
                'company_id' => 11,
                'block_id' => 22,
                'school_academic_year_id' => 5,
            ),
            89 => 
            array (
                'id' => 95,
                'company_id' => 11,
                'block_id' => 23,
                'school_academic_year_id' => 5,
            ),
            90 => 
            array (
                'id' => 96,
                'company_id' => 11,
                'block_id' => 81,
                'school_academic_year_id' => 5,
            ),
            91 => 
            array (
                'id' => 97,
                'company_id' => 11,
                'block_id' => 84,
                'school_academic_year_id' => 5,
            ),
            92 => 
            array (
                'id' => 98,
                'company_id' => 11,
                'block_id' => 87,
                'school_academic_year_id' => 5,
            ),
            93 => 
            array (
                'id' => 99,
                'company_id' => 11,
                'block_id' => 89,
                'school_academic_year_id' => 5,
            ),
            94 => 
            array (
                'id' => 100,
                'company_id' => 11,
                'block_id' => 15,
                'school_academic_year_id' => 6,
            ),
            95 => 
            array (
                'id' => 101,
                'company_id' => 11,
                'block_id' => 17,
                'school_academic_year_id' => 6,
            ),
            96 => 
            array (
                'id' => 102,
                'company_id' => 11,
                'block_id' => 18,
                'school_academic_year_id' => 6,
            ),
            97 => 
            array (
                'id' => 103,
                'company_id' => 11,
                'block_id' => 20,
                'school_academic_year_id' => 6,
            ),
            98 => 
            array (
                'id' => 104,
                'company_id' => 11,
                'block_id' => 21,
                'school_academic_year_id' => 6,
            ),
            99 => 
            array (
                'id' => 105,
                'company_id' => 11,
                'block_id' => 22,
                'school_academic_year_id' => 6,
            ),
            100 => 
            array (
                'id' => 106,
                'company_id' => 11,
                'block_id' => 23,
                'school_academic_year_id' => 6,
            ),
            101 => 
            array (
                'id' => 107,
                'company_id' => 11,
                'block_id' => 81,
                'school_academic_year_id' => 6,
            ),
            102 => 
            array (
                'id' => 108,
                'company_id' => 11,
                'block_id' => 84,
                'school_academic_year_id' => 6,
            ),
            103 => 
            array (
                'id' => 109,
                'company_id' => 11,
                'block_id' => 87,
                'school_academic_year_id' => 6,
            ),
            104 => 
            array (
                'id' => 110,
                'company_id' => 11,
                'block_id' => 89,
                'school_academic_year_id' => 6,
            ),
            105 => 
            array (
                'id' => 111,
                'company_id' => 12,
                'block_id' => 24,
                'school_academic_year_id' => 2,
            ),
            106 => 
            array (
                'id' => 112,
                'company_id' => 12,
                'block_id' => 180,
                'school_academic_year_id' => 2,
            ),
            107 => 
            array (
                'id' => 113,
                'company_id' => 12,
                'block_id' => 24,
                'school_academic_year_id' => 3,
            ),
            108 => 
            array (
                'id' => 114,
                'company_id' => 12,
                'block_id' => 180,
                'school_academic_year_id' => 3,
            ),
            109 => 
            array (
                'id' => 115,
                'company_id' => 12,
                'block_id' => 24,
                'school_academic_year_id' => 4,
            ),
            110 => 
            array (
                'id' => 116,
                'company_id' => 12,
                'block_id' => 180,
                'school_academic_year_id' => 4,
            ),
            111 => 
            array (
                'id' => 117,
                'company_id' => 12,
                'block_id' => 24,
                'school_academic_year_id' => 5,
            ),
            112 => 
            array (
                'id' => 118,
                'company_id' => 12,
                'block_id' => 180,
                'school_academic_year_id' => 5,
            ),
            113 => 
            array (
                'id' => 119,
                'company_id' => 12,
                'block_id' => 24,
                'school_academic_year_id' => 6,
            ),
            114 => 
            array (
                'id' => 120,
                'company_id' => 12,
                'block_id' => 180,
                'school_academic_year_id' => 6,
            ),
            115 => 
            array (
                'id' => 121,
                'company_id' => 14,
                'block_id' => 313,
                'school_academic_year_id' => 2,
            ),
            116 => 
            array (
                'id' => 122,
                'company_id' => 14,
                'block_id' => 314,
                'school_academic_year_id' => 2,
            ),
            117 => 
            array (
                'id' => 123,
                'company_id' => 14,
                'block_id' => 315,
                'school_academic_year_id' => 2,
            ),
            118 => 
            array (
                'id' => 124,
                'company_id' => 14,
                'block_id' => 316,
                'school_academic_year_id' => 2,
            ),
            119 => 
            array (
                'id' => 125,
                'company_id' => 14,
                'block_id' => 313,
                'school_academic_year_id' => 3,
            ),
            120 => 
            array (
                'id' => 126,
                'company_id' => 14,
                'block_id' => 314,
                'school_academic_year_id' => 3,
            ),
            121 => 
            array (
                'id' => 127,
                'company_id' => 14,
                'block_id' => 315,
                'school_academic_year_id' => 3,
            ),
            122 => 
            array (
                'id' => 128,
                'company_id' => 14,
                'block_id' => 316,
                'school_academic_year_id' => 3,
            ),
            123 => 
            array (
                'id' => 129,
                'company_id' => 14,
                'block_id' => 313,
                'school_academic_year_id' => 4,
            ),
            124 => 
            array (
                'id' => 130,
                'company_id' => 14,
                'block_id' => 314,
                'school_academic_year_id' => 4,
            ),
            125 => 
            array (
                'id' => 131,
                'company_id' => 14,
                'block_id' => 315,
                'school_academic_year_id' => 4,
            ),
            126 => 
            array (
                'id' => 132,
                'company_id' => 14,
                'block_id' => 316,
                'school_academic_year_id' => 4,
            ),
            127 => 
            array (
                'id' => 133,
                'company_id' => 14,
                'block_id' => 313,
                'school_academic_year_id' => 5,
            ),
            128 => 
            array (
                'id' => 134,
                'company_id' => 14,
                'block_id' => 314,
                'school_academic_year_id' => 5,
            ),
            129 => 
            array (
                'id' => 135,
                'company_id' => 14,
                'block_id' => 315,
                'school_academic_year_id' => 5,
            ),
            130 => 
            array (
                'id' => 136,
                'company_id' => 14,
                'block_id' => 316,
                'school_academic_year_id' => 5,
            ),
            131 => 
            array (
                'id' => 137,
                'company_id' => 14,
                'block_id' => 313,
                'school_academic_year_id' => 6,
            ),
            132 => 
            array (
                'id' => 138,
                'company_id' => 14,
                'block_id' => 314,
                'school_academic_year_id' => 6,
            ),
            133 => 
            array (
                'id' => 139,
                'company_id' => 14,
                'block_id' => 315,
                'school_academic_year_id' => 6,
            ),
            134 => 
            array (
                'id' => 140,
                'company_id' => 14,
                'block_id' => 316,
                'school_academic_year_id' => 6,
            ),
            135 => 
            array (
                'id' => 141,
                'company_id' => 15,
                'block_id' => 31,
                'school_academic_year_id' => 2,
            ),
            136 => 
            array (
                'id' => 142,
                'company_id' => 15,
                'block_id' => 32,
                'school_academic_year_id' => 2,
            ),
            137 => 
            array (
                'id' => 143,
                'company_id' => 15,
                'block_id' => 33,
                'school_academic_year_id' => 2,
            ),
            138 => 
            array (
                'id' => 145,
                'company_id' => 15,
                'block_id' => 31,
                'school_academic_year_id' => 3,
            ),
            139 => 
            array (
                'id' => 146,
                'company_id' => 15,
                'block_id' => 32,
                'school_academic_year_id' => 3,
            ),
            140 => 
            array (
                'id' => 147,
                'company_id' => 15,
                'block_id' => 33,
                'school_academic_year_id' => 3,
            ),
            141 => 
            array (
                'id' => 149,
                'company_id' => 15,
                'block_id' => 31,
                'school_academic_year_id' => 4,
            ),
            142 => 
            array (
                'id' => 150,
                'company_id' => 15,
                'block_id' => 32,
                'school_academic_year_id' => 4,
            ),
            143 => 
            array (
                'id' => 151,
                'company_id' => 15,
                'block_id' => 33,
                'school_academic_year_id' => 4,
            ),
            144 => 
            array (
                'id' => 153,
                'company_id' => 15,
                'block_id' => 31,
                'school_academic_year_id' => 5,
            ),
            145 => 
            array (
                'id' => 154,
                'company_id' => 15,
                'block_id' => 32,
                'school_academic_year_id' => 5,
            ),
            146 => 
            array (
                'id' => 155,
                'company_id' => 15,
                'block_id' => 33,
                'school_academic_year_id' => 5,
            ),
            147 => 
            array (
                'id' => 157,
                'company_id' => 15,
                'block_id' => 31,
                'school_academic_year_id' => 6,
            ),
            148 => 
            array (
                'id' => 158,
                'company_id' => 15,
                'block_id' => 32,
                'school_academic_year_id' => 6,
            ),
            149 => 
            array (
                'id' => 159,
                'company_id' => 15,
                'block_id' => 33,
                'school_academic_year_id' => 6,
            ),
            150 => 
            array (
                'id' => 161,
                'company_id' => 16,
                'block_id' => 35,
                'school_academic_year_id' => 2,
            ),
            151 => 
            array (
                'id' => 162,
                'company_id' => 16,
                'block_id' => 36,
                'school_academic_year_id' => 2,
            ),
            152 => 
            array (
                'id' => 163,
                'company_id' => 16,
                'block_id' => 37,
                'school_academic_year_id' => 2,
            ),
            153 => 
            array (
                'id' => 164,
                'company_id' => 16,
                'block_id' => 88,
                'school_academic_year_id' => 2,
            ),
            154 => 
            array (
                'id' => 165,
                'company_id' => 16,
                'block_id' => 163,
                'school_academic_year_id' => 2,
            ),
            155 => 
            array (
                'id' => 166,
                'company_id' => 16,
                'block_id' => 35,
                'school_academic_year_id' => 3,
            ),
            156 => 
            array (
                'id' => 167,
                'company_id' => 16,
                'block_id' => 36,
                'school_academic_year_id' => 3,
            ),
            157 => 
            array (
                'id' => 168,
                'company_id' => 16,
                'block_id' => 37,
                'school_academic_year_id' => 3,
            ),
            158 => 
            array (
                'id' => 169,
                'company_id' => 16,
                'block_id' => 88,
                'school_academic_year_id' => 3,
            ),
            159 => 
            array (
                'id' => 170,
                'company_id' => 16,
                'block_id' => 163,
                'school_academic_year_id' => 3,
            ),
            160 => 
            array (
                'id' => 171,
                'company_id' => 16,
                'block_id' => 35,
                'school_academic_year_id' => 4,
            ),
            161 => 
            array (
                'id' => 172,
                'company_id' => 16,
                'block_id' => 36,
                'school_academic_year_id' => 4,
            ),
            162 => 
            array (
                'id' => 173,
                'company_id' => 16,
                'block_id' => 37,
                'school_academic_year_id' => 4,
            ),
            163 => 
            array (
                'id' => 174,
                'company_id' => 16,
                'block_id' => 88,
                'school_academic_year_id' => 4,
            ),
            164 => 
            array (
                'id' => 175,
                'company_id' => 16,
                'block_id' => 163,
                'school_academic_year_id' => 4,
            ),
            165 => 
            array (
                'id' => 176,
                'company_id' => 16,
                'block_id' => 35,
                'school_academic_year_id' => 5,
            ),
            166 => 
            array (
                'id' => 177,
                'company_id' => 16,
                'block_id' => 36,
                'school_academic_year_id' => 5,
            ),
            167 => 
            array (
                'id' => 178,
                'company_id' => 16,
                'block_id' => 37,
                'school_academic_year_id' => 5,
            ),
            168 => 
            array (
                'id' => 179,
                'company_id' => 16,
                'block_id' => 88,
                'school_academic_year_id' => 5,
            ),
            169 => 
            array (
                'id' => 180,
                'company_id' => 16,
                'block_id' => 163,
                'school_academic_year_id' => 5,
            ),
            170 => 
            array (
                'id' => 181,
                'company_id' => 16,
                'block_id' => 35,
                'school_academic_year_id' => 6,
            ),
            171 => 
            array (
                'id' => 182,
                'company_id' => 16,
                'block_id' => 36,
                'school_academic_year_id' => 6,
            ),
            172 => 
            array (
                'id' => 183,
                'company_id' => 16,
                'block_id' => 37,
                'school_academic_year_id' => 6,
            ),
            173 => 
            array (
                'id' => 184,
                'company_id' => 16,
                'block_id' => 88,
                'school_academic_year_id' => 6,
            ),
            174 => 
            array (
                'id' => 185,
                'company_id' => 16,
                'block_id' => 163,
                'school_academic_year_id' => 6,
            ),
            175 => 
            array (
                'id' => 186,
                'company_id' => 17,
                'block_id' => 38,
                'school_academic_year_id' => 2,
            ),
            176 => 
            array (
                'id' => 187,
                'company_id' => 17,
                'block_id' => 39,
                'school_academic_year_id' => 2,
            ),
            177 => 
            array (
                'id' => 188,
                'company_id' => 17,
                'block_id' => 40,
                'school_academic_year_id' => 2,
            ),
            178 => 
            array (
                'id' => 189,
                'company_id' => 17,
                'block_id' => 41,
                'school_academic_year_id' => 2,
            ),
            179 => 
            array (
                'id' => 190,
                'company_id' => 17,
                'block_id' => 49,
                'school_academic_year_id' => 2,
            ),
            180 => 
            array (
                'id' => 191,
                'company_id' => 17,
                'block_id' => 38,
                'school_academic_year_id' => 3,
            ),
            181 => 
            array (
                'id' => 192,
                'company_id' => 17,
                'block_id' => 39,
                'school_academic_year_id' => 3,
            ),
            182 => 
            array (
                'id' => 193,
                'company_id' => 17,
                'block_id' => 40,
                'school_academic_year_id' => 3,
            ),
            183 => 
            array (
                'id' => 194,
                'company_id' => 17,
                'block_id' => 41,
                'school_academic_year_id' => 3,
            ),
            184 => 
            array (
                'id' => 195,
                'company_id' => 17,
                'block_id' => 49,
                'school_academic_year_id' => 3,
            ),
            185 => 
            array (
                'id' => 196,
                'company_id' => 17,
                'block_id' => 38,
                'school_academic_year_id' => 4,
            ),
            186 => 
            array (
                'id' => 197,
                'company_id' => 17,
                'block_id' => 39,
                'school_academic_year_id' => 4,
            ),
            187 => 
            array (
                'id' => 198,
                'company_id' => 17,
                'block_id' => 40,
                'school_academic_year_id' => 4,
            ),
            188 => 
            array (
                'id' => 199,
                'company_id' => 17,
                'block_id' => 41,
                'school_academic_year_id' => 4,
            ),
            189 => 
            array (
                'id' => 200,
                'company_id' => 17,
                'block_id' => 49,
                'school_academic_year_id' => 4,
            ),
            190 => 
            array (
                'id' => 201,
                'company_id' => 17,
                'block_id' => 38,
                'school_academic_year_id' => 5,
            ),
            191 => 
            array (
                'id' => 202,
                'company_id' => 17,
                'block_id' => 39,
                'school_academic_year_id' => 5,
            ),
            192 => 
            array (
                'id' => 203,
                'company_id' => 17,
                'block_id' => 40,
                'school_academic_year_id' => 5,
            ),
            193 => 
            array (
                'id' => 204,
                'company_id' => 17,
                'block_id' => 41,
                'school_academic_year_id' => 5,
            ),
            194 => 
            array (
                'id' => 205,
                'company_id' => 17,
                'block_id' => 49,
                'school_academic_year_id' => 5,
            ),
            195 => 
            array (
                'id' => 206,
                'company_id' => 17,
                'block_id' => 38,
                'school_academic_year_id' => 6,
            ),
            196 => 
            array (
                'id' => 207,
                'company_id' => 17,
                'block_id' => 39,
                'school_academic_year_id' => 6,
            ),
            197 => 
            array (
                'id' => 208,
                'company_id' => 17,
                'block_id' => 40,
                'school_academic_year_id' => 6,
            ),
            198 => 
            array (
                'id' => 209,
                'company_id' => 17,
                'block_id' => 41,
                'school_academic_year_id' => 6,
            ),
            199 => 
            array (
                'id' => 210,
                'company_id' => 17,
                'block_id' => 49,
                'school_academic_year_id' => 6,
            ),
            200 => 
            array (
                'id' => 211,
                'company_id' => 19,
                'block_id' => 44,
                'school_academic_year_id' => 2,
            ),
            201 => 
            array (
                'id' => 212,
                'company_id' => 19,
                'block_id' => 114,
                'school_academic_year_id' => 2,
            ),
            202 => 
            array (
                'id' => 213,
                'company_id' => 19,
                'block_id' => 115,
                'school_academic_year_id' => 2,
            ),
            203 => 
            array (
                'id' => 214,
                'company_id' => 19,
                'block_id' => 221,
                'school_academic_year_id' => 2,
            ),
            204 => 
            array (
                'id' => 215,
                'company_id' => 19,
                'block_id' => 228,
                'school_academic_year_id' => 2,
            ),
            205 => 
            array (
                'id' => 216,
                'company_id' => 19,
                'block_id' => 229,
                'school_academic_year_id' => 2,
            ),
            206 => 
            array (
                'id' => 217,
                'company_id' => 19,
                'block_id' => 230,
                'school_academic_year_id' => 2,
            ),
            207 => 
            array (
                'id' => 218,
                'company_id' => 19,
                'block_id' => 370,
                'school_academic_year_id' => 2,
            ),
            208 => 
            array (
                'id' => 219,
                'company_id' => 19,
                'block_id' => 371,
                'school_academic_year_id' => 2,
            ),
            209 => 
            array (
                'id' => 220,
                'company_id' => 19,
                'block_id' => 44,
                'school_academic_year_id' => 3,
            ),
            210 => 
            array (
                'id' => 221,
                'company_id' => 19,
                'block_id' => 114,
                'school_academic_year_id' => 3,
            ),
            211 => 
            array (
                'id' => 222,
                'company_id' => 19,
                'block_id' => 115,
                'school_academic_year_id' => 3,
            ),
            212 => 
            array (
                'id' => 223,
                'company_id' => 19,
                'block_id' => 221,
                'school_academic_year_id' => 3,
            ),
            213 => 
            array (
                'id' => 224,
                'company_id' => 19,
                'block_id' => 228,
                'school_academic_year_id' => 3,
            ),
            214 => 
            array (
                'id' => 225,
                'company_id' => 19,
                'block_id' => 229,
                'school_academic_year_id' => 3,
            ),
            215 => 
            array (
                'id' => 226,
                'company_id' => 19,
                'block_id' => 230,
                'school_academic_year_id' => 3,
            ),
            216 => 
            array (
                'id' => 227,
                'company_id' => 19,
                'block_id' => 370,
                'school_academic_year_id' => 3,
            ),
            217 => 
            array (
                'id' => 228,
                'company_id' => 19,
                'block_id' => 371,
                'school_academic_year_id' => 3,
            ),
            218 => 
            array (
                'id' => 229,
                'company_id' => 19,
                'block_id' => 44,
                'school_academic_year_id' => 4,
            ),
            219 => 
            array (
                'id' => 230,
                'company_id' => 19,
                'block_id' => 114,
                'school_academic_year_id' => 4,
            ),
            220 => 
            array (
                'id' => 231,
                'company_id' => 19,
                'block_id' => 115,
                'school_academic_year_id' => 4,
            ),
            221 => 
            array (
                'id' => 232,
                'company_id' => 19,
                'block_id' => 221,
                'school_academic_year_id' => 4,
            ),
            222 => 
            array (
                'id' => 233,
                'company_id' => 19,
                'block_id' => 228,
                'school_academic_year_id' => 4,
            ),
            223 => 
            array (
                'id' => 234,
                'company_id' => 19,
                'block_id' => 229,
                'school_academic_year_id' => 4,
            ),
            224 => 
            array (
                'id' => 235,
                'company_id' => 19,
                'block_id' => 230,
                'school_academic_year_id' => 4,
            ),
            225 => 
            array (
                'id' => 236,
                'company_id' => 19,
                'block_id' => 370,
                'school_academic_year_id' => 4,
            ),
            226 => 
            array (
                'id' => 237,
                'company_id' => 19,
                'block_id' => 371,
                'school_academic_year_id' => 4,
            ),
            227 => 
            array (
                'id' => 238,
                'company_id' => 19,
                'block_id' => 44,
                'school_academic_year_id' => 5,
            ),
            228 => 
            array (
                'id' => 239,
                'company_id' => 19,
                'block_id' => 114,
                'school_academic_year_id' => 5,
            ),
            229 => 
            array (
                'id' => 240,
                'company_id' => 19,
                'block_id' => 115,
                'school_academic_year_id' => 5,
            ),
            230 => 
            array (
                'id' => 241,
                'company_id' => 19,
                'block_id' => 221,
                'school_academic_year_id' => 5,
            ),
            231 => 
            array (
                'id' => 242,
                'company_id' => 19,
                'block_id' => 228,
                'school_academic_year_id' => 5,
            ),
            232 => 
            array (
                'id' => 243,
                'company_id' => 19,
                'block_id' => 229,
                'school_academic_year_id' => 5,
            ),
            233 => 
            array (
                'id' => 244,
                'company_id' => 19,
                'block_id' => 230,
                'school_academic_year_id' => 5,
            ),
            234 => 
            array (
                'id' => 245,
                'company_id' => 19,
                'block_id' => 370,
                'school_academic_year_id' => 5,
            ),
            235 => 
            array (
                'id' => 246,
                'company_id' => 19,
                'block_id' => 371,
                'school_academic_year_id' => 5,
            ),
            236 => 
            array (
                'id' => 247,
                'company_id' => 19,
                'block_id' => 44,
                'school_academic_year_id' => 6,
            ),
            237 => 
            array (
                'id' => 248,
                'company_id' => 19,
                'block_id' => 114,
                'school_academic_year_id' => 6,
            ),
            238 => 
            array (
                'id' => 249,
                'company_id' => 19,
                'block_id' => 115,
                'school_academic_year_id' => 6,
            ),
            239 => 
            array (
                'id' => 250,
                'company_id' => 19,
                'block_id' => 221,
                'school_academic_year_id' => 6,
            ),
            240 => 
            array (
                'id' => 251,
                'company_id' => 19,
                'block_id' => 228,
                'school_academic_year_id' => 6,
            ),
            241 => 
            array (
                'id' => 252,
                'company_id' => 19,
                'block_id' => 229,
                'school_academic_year_id' => 6,
            ),
            242 => 
            array (
                'id' => 253,
                'company_id' => 19,
                'block_id' => 230,
                'school_academic_year_id' => 6,
            ),
            243 => 
            array (
                'id' => 254,
                'company_id' => 19,
                'block_id' => 370,
                'school_academic_year_id' => 6,
            ),
            244 => 
            array (
                'id' => 255,
                'company_id' => 19,
                'block_id' => 371,
                'school_academic_year_id' => 6,
            ),
            245 => 
            array (
                'id' => 256,
                'company_id' => 21,
                'block_id' => 47,
                'school_academic_year_id' => 2,
            ),
            246 => 
            array (
                'id' => 257,
                'company_id' => 21,
                'block_id' => 178,
                'school_academic_year_id' => 2,
            ),
            247 => 
            array (
                'id' => 258,
                'company_id' => 21,
                'block_id' => 47,
                'school_academic_year_id' => 3,
            ),
            248 => 
            array (
                'id' => 259,
                'company_id' => 21,
                'block_id' => 178,
                'school_academic_year_id' => 3,
            ),
            249 => 
            array (
                'id' => 260,
                'company_id' => 21,
                'block_id' => 47,
                'school_academic_year_id' => 4,
            ),
            250 => 
            array (
                'id' => 261,
                'company_id' => 21,
                'block_id' => 178,
                'school_academic_year_id' => 4,
            ),
            251 => 
            array (
                'id' => 262,
                'company_id' => 21,
                'block_id' => 47,
                'school_academic_year_id' => 5,
            ),
            252 => 
            array (
                'id' => 263,
                'company_id' => 21,
                'block_id' => 178,
                'school_academic_year_id' => 5,
            ),
            253 => 
            array (
                'id' => 264,
                'company_id' => 21,
                'block_id' => 47,
                'school_academic_year_id' => 6,
            ),
            254 => 
            array (
                'id' => 265,
                'company_id' => 21,
                'block_id' => 178,
                'school_academic_year_id' => 6,
            ),
            255 => 
            array (
                'id' => 266,
                'company_id' => 22,
                'block_id' => 48,
                'school_academic_year_id' => 2,
            ),
            256 => 
            array (
                'id' => 267,
                'company_id' => 22,
                'block_id' => 425,
                'school_academic_year_id' => 2,
            ),
            257 => 
            array (
                'id' => 268,
                'company_id' => 22,
                'block_id' => 48,
                'school_academic_year_id' => 3,
            ),
            258 => 
            array (
                'id' => 269,
                'company_id' => 22,
                'block_id' => 425,
                'school_academic_year_id' => 3,
            ),
            259 => 
            array (
                'id' => 270,
                'company_id' => 22,
                'block_id' => 48,
                'school_academic_year_id' => 4,
            ),
            260 => 
            array (
                'id' => 271,
                'company_id' => 22,
                'block_id' => 425,
                'school_academic_year_id' => 4,
            ),
            261 => 
            array (
                'id' => 272,
                'company_id' => 22,
                'block_id' => 48,
                'school_academic_year_id' => 5,
            ),
            262 => 
            array (
                'id' => 273,
                'company_id' => 22,
                'block_id' => 425,
                'school_academic_year_id' => 5,
            ),
            263 => 
            array (
                'id' => 274,
                'company_id' => 22,
                'block_id' => 48,
                'school_academic_year_id' => 6,
            ),
            264 => 
            array (
                'id' => 275,
                'company_id' => 22,
                'block_id' => 425,
                'school_academic_year_id' => 6,
            ),
            265 => 
            array (
                'id' => 276,
                'company_id' => 23,
                'block_id' => 51,
                'school_academic_year_id' => 2,
            ),
            266 => 
            array (
                'id' => 277,
                'company_id' => 23,
                'block_id' => 51,
                'school_academic_year_id' => 3,
            ),
            267 => 
            array (
                'id' => 278,
                'company_id' => 23,
                'block_id' => 51,
                'school_academic_year_id' => 4,
            ),
            268 => 
            array (
                'id' => 279,
                'company_id' => 23,
                'block_id' => 51,
                'school_academic_year_id' => 5,
            ),
            269 => 
            array (
                'id' => 280,
                'company_id' => 23,
                'block_id' => 51,
                'school_academic_year_id' => 6,
            ),
            270 => 
            array (
                'id' => 281,
                'company_id' => 24,
                'block_id' => 52,
                'school_academic_year_id' => 2,
            ),
            271 => 
            array (
                'id' => 282,
                'company_id' => 24,
                'block_id' => 52,
                'school_academic_year_id' => 3,
            ),
            272 => 
            array (
                'id' => 283,
                'company_id' => 24,
                'block_id' => 52,
                'school_academic_year_id' => 4,
            ),
            273 => 
            array (
                'id' => 284,
                'company_id' => 24,
                'block_id' => 52,
                'school_academic_year_id' => 5,
            ),
            274 => 
            array (
                'id' => 285,
                'company_id' => 24,
                'block_id' => 52,
                'school_academic_year_id' => 6,
            ),
            275 => 
            array (
                'id' => 286,
                'company_id' => 25,
                'block_id' => 53,
                'school_academic_year_id' => 2,
            ),
            276 => 
            array (
                'id' => 287,
                'company_id' => 25,
                'block_id' => 176,
                'school_academic_year_id' => 2,
            ),
            277 => 
            array (
                'id' => 288,
                'company_id' => 25,
                'block_id' => 177,
                'school_academic_year_id' => 2,
            ),
            278 => 
            array (
                'id' => 289,
                'company_id' => 25,
                'block_id' => 53,
                'school_academic_year_id' => 3,
            ),
            279 => 
            array (
                'id' => 290,
                'company_id' => 25,
                'block_id' => 176,
                'school_academic_year_id' => 3,
            ),
            280 => 
            array (
                'id' => 291,
                'company_id' => 25,
                'block_id' => 177,
                'school_academic_year_id' => 3,
            ),
            281 => 
            array (
                'id' => 292,
                'company_id' => 25,
                'block_id' => 53,
                'school_academic_year_id' => 4,
            ),
            282 => 
            array (
                'id' => 293,
                'company_id' => 25,
                'block_id' => 176,
                'school_academic_year_id' => 4,
            ),
            283 => 
            array (
                'id' => 294,
                'company_id' => 25,
                'block_id' => 177,
                'school_academic_year_id' => 4,
            ),
            284 => 
            array (
                'id' => 295,
                'company_id' => 25,
                'block_id' => 53,
                'school_academic_year_id' => 5,
            ),
            285 => 
            array (
                'id' => 296,
                'company_id' => 25,
                'block_id' => 176,
                'school_academic_year_id' => 5,
            ),
            286 => 
            array (
                'id' => 297,
                'company_id' => 25,
                'block_id' => 177,
                'school_academic_year_id' => 5,
            ),
            287 => 
            array (
                'id' => 298,
                'company_id' => 25,
                'block_id' => 53,
                'school_academic_year_id' => 6,
            ),
            288 => 
            array (
                'id' => 299,
                'company_id' => 25,
                'block_id' => 176,
                'school_academic_year_id' => 6,
            ),
            289 => 
            array (
                'id' => 300,
                'company_id' => 25,
                'block_id' => 177,
                'school_academic_year_id' => 6,
            ),
            290 => 
            array (
                'id' => 301,
                'company_id' => 26,
                'block_id' => 54,
                'school_academic_year_id' => 2,
            ),
            291 => 
            array (
                'id' => 302,
                'company_id' => 26,
                'block_id' => 54,
                'school_academic_year_id' => 3,
            ),
            292 => 
            array (
                'id' => 303,
                'company_id' => 26,
                'block_id' => 54,
                'school_academic_year_id' => 4,
            ),
            293 => 
            array (
                'id' => 304,
                'company_id' => 26,
                'block_id' => 54,
                'school_academic_year_id' => 5,
            ),
            294 => 
            array (
                'id' => 305,
                'company_id' => 26,
                'block_id' => 54,
                'school_academic_year_id' => 6,
            ),
            295 => 
            array (
                'id' => 306,
                'company_id' => 27,
                'block_id' => 55,
                'school_academic_year_id' => 2,
            ),
            296 => 
            array (
                'id' => 307,
                'company_id' => 27,
                'block_id' => 55,
                'school_academic_year_id' => 3,
            ),
            297 => 
            array (
                'id' => 308,
                'company_id' => 27,
                'block_id' => 55,
                'school_academic_year_id' => 4,
            ),
            298 => 
            array (
                'id' => 309,
                'company_id' => 27,
                'block_id' => 55,
                'school_academic_year_id' => 5,
            ),
            299 => 
            array (
                'id' => 310,
                'company_id' => 27,
                'block_id' => 55,
                'school_academic_year_id' => 6,
            ),
            300 => 
            array (
                'id' => 311,
                'company_id' => 28,
                'block_id' => 56,
                'school_academic_year_id' => 2,
            ),
            301 => 
            array (
                'id' => 312,
                'company_id' => 28,
                'block_id' => 56,
                'school_academic_year_id' => 3,
            ),
            302 => 
            array (
                'id' => 313,
                'company_id' => 28,
                'block_id' => 56,
                'school_academic_year_id' => 4,
            ),
            303 => 
            array (
                'id' => 314,
                'company_id' => 28,
                'block_id' => 56,
                'school_academic_year_id' => 5,
            ),
            304 => 
            array (
                'id' => 315,
                'company_id' => 28,
                'block_id' => 56,
                'school_academic_year_id' => 6,
            ),
            305 => 
            array (
                'id' => 316,
                'company_id' => 29,
                'block_id' => 57,
                'school_academic_year_id' => 2,
            ),
            306 => 
            array (
                'id' => 317,
                'company_id' => 29,
                'block_id' => 57,
                'school_academic_year_id' => 3,
            ),
            307 => 
            array (
                'id' => 318,
                'company_id' => 29,
                'block_id' => 57,
                'school_academic_year_id' => 4,
            ),
            308 => 
            array (
                'id' => 319,
                'company_id' => 29,
                'block_id' => 57,
                'school_academic_year_id' => 5,
            ),
            309 => 
            array (
                'id' => 320,
                'company_id' => 29,
                'block_id' => 57,
                'school_academic_year_id' => 6,
            ),
            310 => 
            array (
                'id' => 326,
                'company_id' => 31,
                'block_id' => 60,
                'school_academic_year_id' => 2,
            ),
            311 => 
            array (
                'id' => 327,
                'company_id' => 31,
                'block_id' => 60,
                'school_academic_year_id' => 3,
            ),
            312 => 
            array (
                'id' => 328,
                'company_id' => 31,
                'block_id' => 60,
                'school_academic_year_id' => 4,
            ),
            313 => 
            array (
                'id' => 329,
                'company_id' => 31,
                'block_id' => 60,
                'school_academic_year_id' => 5,
            ),
            314 => 
            array (
                'id' => 330,
                'company_id' => 31,
                'block_id' => 60,
                'school_academic_year_id' => 6,
            ),
            315 => 
            array (
                'id' => 331,
                'company_id' => 33,
                'block_id' => 127,
                'school_academic_year_id' => 2,
            ),
            316 => 
            array (
                'id' => 332,
                'company_id' => 33,
                'block_id' => 128,
                'school_academic_year_id' => 2,
            ),
            317 => 
            array (
                'id' => 333,
                'company_id' => 33,
                'block_id' => 129,
                'school_academic_year_id' => 2,
            ),
            318 => 
            array (
                'id' => 334,
                'company_id' => 33,
                'block_id' => 130,
                'school_academic_year_id' => 2,
            ),
            319 => 
            array (
                'id' => 335,
                'company_id' => 33,
                'block_id' => 224,
                'school_academic_year_id' => 2,
            ),
            320 => 
            array (
                'id' => 336,
                'company_id' => 33,
                'block_id' => 225,
                'school_academic_year_id' => 2,
            ),
            321 => 
            array (
                'id' => 337,
                'company_id' => 33,
                'block_id' => 422,
                'school_academic_year_id' => 2,
            ),
            322 => 
            array (
                'id' => 338,
                'company_id' => 33,
                'block_id' => 423,
                'school_academic_year_id' => 2,
            ),
            323 => 
            array (
                'id' => 339,
                'company_id' => 33,
                'block_id' => 127,
                'school_academic_year_id' => 3,
            ),
            324 => 
            array (
                'id' => 340,
                'company_id' => 33,
                'block_id' => 128,
                'school_academic_year_id' => 3,
            ),
            325 => 
            array (
                'id' => 341,
                'company_id' => 33,
                'block_id' => 129,
                'school_academic_year_id' => 3,
            ),
            326 => 
            array (
                'id' => 342,
                'company_id' => 33,
                'block_id' => 130,
                'school_academic_year_id' => 3,
            ),
            327 => 
            array (
                'id' => 343,
                'company_id' => 33,
                'block_id' => 224,
                'school_academic_year_id' => 3,
            ),
            328 => 
            array (
                'id' => 344,
                'company_id' => 33,
                'block_id' => 225,
                'school_academic_year_id' => 3,
            ),
            329 => 
            array (
                'id' => 345,
                'company_id' => 33,
                'block_id' => 422,
                'school_academic_year_id' => 3,
            ),
            330 => 
            array (
                'id' => 346,
                'company_id' => 33,
                'block_id' => 423,
                'school_academic_year_id' => 3,
            ),
            331 => 
            array (
                'id' => 347,
                'company_id' => 33,
                'block_id' => 127,
                'school_academic_year_id' => 4,
            ),
            332 => 
            array (
                'id' => 348,
                'company_id' => 33,
                'block_id' => 128,
                'school_academic_year_id' => 4,
            ),
            333 => 
            array (
                'id' => 349,
                'company_id' => 33,
                'block_id' => 129,
                'school_academic_year_id' => 4,
            ),
            334 => 
            array (
                'id' => 350,
                'company_id' => 33,
                'block_id' => 130,
                'school_academic_year_id' => 4,
            ),
            335 => 
            array (
                'id' => 351,
                'company_id' => 33,
                'block_id' => 224,
                'school_academic_year_id' => 4,
            ),
            336 => 
            array (
                'id' => 352,
                'company_id' => 33,
                'block_id' => 225,
                'school_academic_year_id' => 4,
            ),
            337 => 
            array (
                'id' => 353,
                'company_id' => 33,
                'block_id' => 422,
                'school_academic_year_id' => 4,
            ),
            338 => 
            array (
                'id' => 354,
                'company_id' => 33,
                'block_id' => 423,
                'school_academic_year_id' => 4,
            ),
            339 => 
            array (
                'id' => 355,
                'company_id' => 33,
                'block_id' => 127,
                'school_academic_year_id' => 5,
            ),
            340 => 
            array (
                'id' => 356,
                'company_id' => 33,
                'block_id' => 128,
                'school_academic_year_id' => 5,
            ),
            341 => 
            array (
                'id' => 357,
                'company_id' => 33,
                'block_id' => 129,
                'school_academic_year_id' => 5,
            ),
            342 => 
            array (
                'id' => 358,
                'company_id' => 33,
                'block_id' => 130,
                'school_academic_year_id' => 5,
            ),
            343 => 
            array (
                'id' => 359,
                'company_id' => 33,
                'block_id' => 224,
                'school_academic_year_id' => 5,
            ),
            344 => 
            array (
                'id' => 360,
                'company_id' => 33,
                'block_id' => 225,
                'school_academic_year_id' => 5,
            ),
            345 => 
            array (
                'id' => 361,
                'company_id' => 33,
                'block_id' => 422,
                'school_academic_year_id' => 5,
            ),
            346 => 
            array (
                'id' => 362,
                'company_id' => 33,
                'block_id' => 423,
                'school_academic_year_id' => 5,
            ),
            347 => 
            array (
                'id' => 363,
                'company_id' => 33,
                'block_id' => 127,
                'school_academic_year_id' => 6,
            ),
            348 => 
            array (
                'id' => 364,
                'company_id' => 33,
                'block_id' => 128,
                'school_academic_year_id' => 6,
            ),
            349 => 
            array (
                'id' => 365,
                'company_id' => 33,
                'block_id' => 129,
                'school_academic_year_id' => 6,
            ),
            350 => 
            array (
                'id' => 366,
                'company_id' => 33,
                'block_id' => 130,
                'school_academic_year_id' => 6,
            ),
            351 => 
            array (
                'id' => 367,
                'company_id' => 33,
                'block_id' => 224,
                'school_academic_year_id' => 6,
            ),
            352 => 
            array (
                'id' => 368,
                'company_id' => 33,
                'block_id' => 225,
                'school_academic_year_id' => 6,
            ),
            353 => 
            array (
                'id' => 369,
                'company_id' => 33,
                'block_id' => 422,
                'school_academic_year_id' => 6,
            ),
            354 => 
            array (
                'id' => 370,
                'company_id' => 33,
                'block_id' => 423,
                'school_academic_year_id' => 6,
            ),
            355 => 
            array (
                'id' => 371,
                'company_id' => 34,
                'block_id' => 73,
                'school_academic_year_id' => 2,
            ),
            356 => 
            array (
                'id' => 372,
                'company_id' => 34,
                'block_id' => 74,
                'school_academic_year_id' => 2,
            ),
            357 => 
            array (
                'id' => 373,
                'company_id' => 34,
                'block_id' => 75,
                'school_academic_year_id' => 2,
            ),
            358 => 
            array (
                'id' => 374,
                'company_id' => 34,
                'block_id' => 76,
                'school_academic_year_id' => 2,
            ),
            359 => 
            array (
                'id' => 375,
                'company_id' => 34,
                'block_id' => 73,
                'school_academic_year_id' => 3,
            ),
            360 => 
            array (
                'id' => 376,
                'company_id' => 34,
                'block_id' => 74,
                'school_academic_year_id' => 3,
            ),
            361 => 
            array (
                'id' => 377,
                'company_id' => 34,
                'block_id' => 75,
                'school_academic_year_id' => 3,
            ),
            362 => 
            array (
                'id' => 378,
                'company_id' => 34,
                'block_id' => 76,
                'school_academic_year_id' => 3,
            ),
            363 => 
            array (
                'id' => 379,
                'company_id' => 34,
                'block_id' => 73,
                'school_academic_year_id' => 4,
            ),
            364 => 
            array (
                'id' => 380,
                'company_id' => 34,
                'block_id' => 74,
                'school_academic_year_id' => 4,
            ),
            365 => 
            array (
                'id' => 381,
                'company_id' => 34,
                'block_id' => 75,
                'school_academic_year_id' => 4,
            ),
            366 => 
            array (
                'id' => 382,
                'company_id' => 34,
                'block_id' => 76,
                'school_academic_year_id' => 4,
            ),
            367 => 
            array (
                'id' => 383,
                'company_id' => 34,
                'block_id' => 73,
                'school_academic_year_id' => 5,
            ),
            368 => 
            array (
                'id' => 384,
                'company_id' => 34,
                'block_id' => 74,
                'school_academic_year_id' => 5,
            ),
            369 => 
            array (
                'id' => 385,
                'company_id' => 34,
                'block_id' => 75,
                'school_academic_year_id' => 5,
            ),
            370 => 
            array (
                'id' => 386,
                'company_id' => 34,
                'block_id' => 76,
                'school_academic_year_id' => 5,
            ),
            371 => 
            array (
                'id' => 387,
                'company_id' => 34,
                'block_id' => 73,
                'school_academic_year_id' => 6,
            ),
            372 => 
            array (
                'id' => 388,
                'company_id' => 34,
                'block_id' => 74,
                'school_academic_year_id' => 6,
            ),
            373 => 
            array (
                'id' => 389,
                'company_id' => 34,
                'block_id' => 75,
                'school_academic_year_id' => 6,
            ),
            374 => 
            array (
                'id' => 390,
                'company_id' => 34,
                'block_id' => 76,
                'school_academic_year_id' => 6,
            ),
            375 => 
            array (
                'id' => 396,
                'company_id' => 36,
                'block_id' => 79,
                'school_academic_year_id' => 2,
            ),
            376 => 
            array (
                'id' => 397,
                'company_id' => 36,
                'block_id' => 79,
                'school_academic_year_id' => 3,
            ),
            377 => 
            array (
                'id' => 398,
                'company_id' => 36,
                'block_id' => 79,
                'school_academic_year_id' => 4,
            ),
            378 => 
            array (
                'id' => 399,
                'company_id' => 36,
                'block_id' => 79,
                'school_academic_year_id' => 5,
            ),
            379 => 
            array (
                'id' => 400,
                'company_id' => 36,
                'block_id' => 79,
                'school_academic_year_id' => 6,
            ),
            380 => 
            array (
                'id' => 401,
                'company_id' => 37,
                'block_id' => 78,
                'school_academic_year_id' => 2,
            ),
            381 => 
            array (
                'id' => 402,
                'company_id' => 37,
                'block_id' => 273,
                'school_academic_year_id' => 2,
            ),
            382 => 
            array (
                'id' => 403,
                'company_id' => 37,
                'block_id' => 78,
                'school_academic_year_id' => 3,
            ),
            383 => 
            array (
                'id' => 404,
                'company_id' => 37,
                'block_id' => 273,
                'school_academic_year_id' => 3,
            ),
            384 => 
            array (
                'id' => 405,
                'company_id' => 37,
                'block_id' => 78,
                'school_academic_year_id' => 4,
            ),
            385 => 
            array (
                'id' => 406,
                'company_id' => 37,
                'block_id' => 273,
                'school_academic_year_id' => 4,
            ),
            386 => 
            array (
                'id' => 407,
                'company_id' => 37,
                'block_id' => 78,
                'school_academic_year_id' => 5,
            ),
            387 => 
            array (
                'id' => 408,
                'company_id' => 37,
                'block_id' => 273,
                'school_academic_year_id' => 5,
            ),
            388 => 
            array (
                'id' => 409,
                'company_id' => 37,
                'block_id' => 78,
                'school_academic_year_id' => 6,
            ),
            389 => 
            array (
                'id' => 410,
                'company_id' => 37,
                'block_id' => 273,
                'school_academic_year_id' => 6,
            ),
            390 => 
            array (
                'id' => 411,
                'company_id' => 38,
                'block_id' => 90,
                'school_academic_year_id' => 2,
            ),
            391 => 
            array (
                'id' => 412,
                'company_id' => 38,
                'block_id' => 90,
                'school_academic_year_id' => 3,
            ),
            392 => 
            array (
                'id' => 413,
                'company_id' => 38,
                'block_id' => 90,
                'school_academic_year_id' => 4,
            ),
            393 => 
            array (
                'id' => 414,
                'company_id' => 38,
                'block_id' => 90,
                'school_academic_year_id' => 5,
            ),
            394 => 
            array (
                'id' => 415,
                'company_id' => 38,
                'block_id' => 90,
                'school_academic_year_id' => 6,
            ),
            395 => 
            array (
                'id' => 416,
                'company_id' => 39,
                'block_id' => 91,
                'school_academic_year_id' => 2,
            ),
            396 => 
            array (
                'id' => 417,
                'company_id' => 39,
                'block_id' => 91,
                'school_academic_year_id' => 3,
            ),
            397 => 
            array (
                'id' => 418,
                'company_id' => 39,
                'block_id' => 91,
                'school_academic_year_id' => 4,
            ),
            398 => 
            array (
                'id' => 419,
                'company_id' => 39,
                'block_id' => 91,
                'school_academic_year_id' => 5,
            ),
            399 => 
            array (
                'id' => 420,
                'company_id' => 39,
                'block_id' => 91,
                'school_academic_year_id' => 6,
            ),
            400 => 
            array (
                'id' => 421,
                'company_id' => 40,
                'block_id' => 93,
                'school_academic_year_id' => 2,
            ),
            401 => 
            array (
                'id' => 422,
                'company_id' => 40,
                'block_id' => 94,
                'school_academic_year_id' => 2,
            ),
            402 => 
            array (
                'id' => 423,
                'company_id' => 40,
                'block_id' => 95,
                'school_academic_year_id' => 2,
            ),
            403 => 
            array (
                'id' => 424,
                'company_id' => 40,
                'block_id' => 96,
                'school_academic_year_id' => 2,
            ),
            404 => 
            array (
                'id' => 425,
                'company_id' => 40,
                'block_id' => 97,
                'school_academic_year_id' => 2,
            ),
            405 => 
            array (
                'id' => 426,
                'company_id' => 40,
                'block_id' => 98,
                'school_academic_year_id' => 2,
            ),
            406 => 
            array (
                'id' => 427,
                'company_id' => 40,
                'block_id' => 99,
                'school_academic_year_id' => 2,
            ),
            407 => 
            array (
                'id' => 428,
                'company_id' => 40,
                'block_id' => 93,
                'school_academic_year_id' => 3,
            ),
            408 => 
            array (
                'id' => 429,
                'company_id' => 40,
                'block_id' => 94,
                'school_academic_year_id' => 3,
            ),
            409 => 
            array (
                'id' => 430,
                'company_id' => 40,
                'block_id' => 95,
                'school_academic_year_id' => 3,
            ),
            410 => 
            array (
                'id' => 431,
                'company_id' => 40,
                'block_id' => 96,
                'school_academic_year_id' => 3,
            ),
            411 => 
            array (
                'id' => 432,
                'company_id' => 40,
                'block_id' => 97,
                'school_academic_year_id' => 3,
            ),
            412 => 
            array (
                'id' => 433,
                'company_id' => 40,
                'block_id' => 98,
                'school_academic_year_id' => 3,
            ),
            413 => 
            array (
                'id' => 434,
                'company_id' => 40,
                'block_id' => 99,
                'school_academic_year_id' => 3,
            ),
            414 => 
            array (
                'id' => 435,
                'company_id' => 40,
                'block_id' => 93,
                'school_academic_year_id' => 4,
            ),
            415 => 
            array (
                'id' => 436,
                'company_id' => 40,
                'block_id' => 94,
                'school_academic_year_id' => 4,
            ),
            416 => 
            array (
                'id' => 437,
                'company_id' => 40,
                'block_id' => 95,
                'school_academic_year_id' => 4,
            ),
            417 => 
            array (
                'id' => 438,
                'company_id' => 40,
                'block_id' => 96,
                'school_academic_year_id' => 4,
            ),
            418 => 
            array (
                'id' => 439,
                'company_id' => 40,
                'block_id' => 97,
                'school_academic_year_id' => 4,
            ),
            419 => 
            array (
                'id' => 440,
                'company_id' => 40,
                'block_id' => 98,
                'school_academic_year_id' => 4,
            ),
            420 => 
            array (
                'id' => 441,
                'company_id' => 40,
                'block_id' => 99,
                'school_academic_year_id' => 4,
            ),
            421 => 
            array (
                'id' => 442,
                'company_id' => 40,
                'block_id' => 93,
                'school_academic_year_id' => 5,
            ),
            422 => 
            array (
                'id' => 443,
                'company_id' => 40,
                'block_id' => 94,
                'school_academic_year_id' => 5,
            ),
            423 => 
            array (
                'id' => 444,
                'company_id' => 40,
                'block_id' => 95,
                'school_academic_year_id' => 5,
            ),
            424 => 
            array (
                'id' => 445,
                'company_id' => 40,
                'block_id' => 96,
                'school_academic_year_id' => 5,
            ),
            425 => 
            array (
                'id' => 446,
                'company_id' => 40,
                'block_id' => 97,
                'school_academic_year_id' => 5,
            ),
            426 => 
            array (
                'id' => 447,
                'company_id' => 40,
                'block_id' => 98,
                'school_academic_year_id' => 5,
            ),
            427 => 
            array (
                'id' => 448,
                'company_id' => 40,
                'block_id' => 99,
                'school_academic_year_id' => 5,
            ),
            428 => 
            array (
                'id' => 449,
                'company_id' => 40,
                'block_id' => 93,
                'school_academic_year_id' => 6,
            ),
            429 => 
            array (
                'id' => 450,
                'company_id' => 40,
                'block_id' => 94,
                'school_academic_year_id' => 6,
            ),
            430 => 
            array (
                'id' => 451,
                'company_id' => 40,
                'block_id' => 95,
                'school_academic_year_id' => 6,
            ),
            431 => 
            array (
                'id' => 452,
                'company_id' => 40,
                'block_id' => 96,
                'school_academic_year_id' => 6,
            ),
            432 => 
            array (
                'id' => 453,
                'company_id' => 40,
                'block_id' => 97,
                'school_academic_year_id' => 6,
            ),
            433 => 
            array (
                'id' => 454,
                'company_id' => 40,
                'block_id' => 98,
                'school_academic_year_id' => 6,
            ),
            434 => 
            array (
                'id' => 455,
                'company_id' => 40,
                'block_id' => 99,
                'school_academic_year_id' => 6,
            ),
            435 => 
            array (
                'id' => 456,
                'company_id' => 41,
                'block_id' => 100,
                'school_academic_year_id' => 2,
            ),
            436 => 
            array (
                'id' => 457,
                'company_id' => 41,
                'block_id' => 100,
                'school_academic_year_id' => 3,
            ),
            437 => 
            array (
                'id' => 458,
                'company_id' => 41,
                'block_id' => 100,
                'school_academic_year_id' => 4,
            ),
            438 => 
            array (
                'id' => 459,
                'company_id' => 41,
                'block_id' => 100,
                'school_academic_year_id' => 5,
            ),
            439 => 
            array (
                'id' => 460,
                'company_id' => 41,
                'block_id' => 100,
                'school_academic_year_id' => 6,
            ),
            440 => 
            array (
                'id' => 461,
                'company_id' => 42,
                'block_id' => 101,
                'school_academic_year_id' => 2,
            ),
            441 => 
            array (
                'id' => 462,
                'company_id' => 42,
                'block_id' => 101,
                'school_academic_year_id' => 3,
            ),
            442 => 
            array (
                'id' => 463,
                'company_id' => 42,
                'block_id' => 101,
                'school_academic_year_id' => 4,
            ),
            443 => 
            array (
                'id' => 464,
                'company_id' => 42,
                'block_id' => 101,
                'school_academic_year_id' => 5,
            ),
            444 => 
            array (
                'id' => 465,
                'company_id' => 42,
                'block_id' => 101,
                'school_academic_year_id' => 6,
            ),
            445 => 
            array (
                'id' => 466,
                'company_id' => 43,
                'block_id' => 102,
                'school_academic_year_id' => 2,
            ),
            446 => 
            array (
                'id' => 467,
                'company_id' => 43,
                'block_id' => 102,
                'school_academic_year_id' => 3,
            ),
            447 => 
            array (
                'id' => 468,
                'company_id' => 43,
                'block_id' => 102,
                'school_academic_year_id' => 4,
            ),
            448 => 
            array (
                'id' => 469,
                'company_id' => 43,
                'block_id' => 102,
                'school_academic_year_id' => 5,
            ),
            449 => 
            array (
                'id' => 470,
                'company_id' => 43,
                'block_id' => 102,
                'school_academic_year_id' => 6,
            ),
            450 => 
            array (
                'id' => 471,
                'company_id' => 44,
                'block_id' => 103,
                'school_academic_year_id' => 2,
            ),
            451 => 
            array (
                'id' => 472,
                'company_id' => 44,
                'block_id' => 103,
                'school_academic_year_id' => 3,
            ),
            452 => 
            array (
                'id' => 473,
                'company_id' => 44,
                'block_id' => 103,
                'school_academic_year_id' => 4,
            ),
            453 => 
            array (
                'id' => 474,
                'company_id' => 44,
                'block_id' => 103,
                'school_academic_year_id' => 5,
            ),
            454 => 
            array (
                'id' => 475,
                'company_id' => 44,
                'block_id' => 103,
                'school_academic_year_id' => 6,
            ),
            455 => 
            array (
                'id' => 476,
                'company_id' => 45,
                'block_id' => 104,
                'school_academic_year_id' => 2,
            ),
            456 => 
            array (
                'id' => 477,
                'company_id' => 45,
                'block_id' => 104,
                'school_academic_year_id' => 3,
            ),
            457 => 
            array (
                'id' => 478,
                'company_id' => 45,
                'block_id' => 104,
                'school_academic_year_id' => 4,
            ),
            458 => 
            array (
                'id' => 479,
                'company_id' => 45,
                'block_id' => 104,
                'school_academic_year_id' => 5,
            ),
            459 => 
            array (
                'id' => 480,
                'company_id' => 45,
                'block_id' => 104,
                'school_academic_year_id' => 6,
            ),
            460 => 
            array (
                'id' => 481,
                'company_id' => 46,
                'block_id' => 105,
                'school_academic_year_id' => 2,
            ),
            461 => 
            array (
                'id' => 482,
                'company_id' => 46,
                'block_id' => 105,
                'school_academic_year_id' => 3,
            ),
            462 => 
            array (
                'id' => 483,
                'company_id' => 46,
                'block_id' => 105,
                'school_academic_year_id' => 4,
            ),
            463 => 
            array (
                'id' => 484,
                'company_id' => 46,
                'block_id' => 105,
                'school_academic_year_id' => 5,
            ),
            464 => 
            array (
                'id' => 485,
                'company_id' => 46,
                'block_id' => 105,
                'school_academic_year_id' => 6,
            ),
            465 => 
            array (
                'id' => 486,
                'company_id' => 47,
                'block_id' => 106,
                'school_academic_year_id' => 2,
            ),
            466 => 
            array (
                'id' => 487,
                'company_id' => 47,
                'block_id' => 107,
                'school_academic_year_id' => 2,
            ),
            467 => 
            array (
                'id' => 488,
                'company_id' => 47,
                'block_id' => 234,
                'school_academic_year_id' => 2,
            ),
            468 => 
            array (
                'id' => 489,
                'company_id' => 47,
                'block_id' => 455,
                'school_academic_year_id' => 2,
            ),
            469 => 
            array (
                'id' => 490,
                'company_id' => 47,
                'block_id' => 106,
                'school_academic_year_id' => 3,
            ),
            470 => 
            array (
                'id' => 491,
                'company_id' => 47,
                'block_id' => 107,
                'school_academic_year_id' => 3,
            ),
            471 => 
            array (
                'id' => 492,
                'company_id' => 47,
                'block_id' => 234,
                'school_academic_year_id' => 3,
            ),
            472 => 
            array (
                'id' => 493,
                'company_id' => 47,
                'block_id' => 455,
                'school_academic_year_id' => 3,
            ),
            473 => 
            array (
                'id' => 494,
                'company_id' => 47,
                'block_id' => 106,
                'school_academic_year_id' => 4,
            ),
            474 => 
            array (
                'id' => 495,
                'company_id' => 47,
                'block_id' => 107,
                'school_academic_year_id' => 4,
            ),
            475 => 
            array (
                'id' => 496,
                'company_id' => 47,
                'block_id' => 234,
                'school_academic_year_id' => 4,
            ),
            476 => 
            array (
                'id' => 497,
                'company_id' => 47,
                'block_id' => 455,
                'school_academic_year_id' => 4,
            ),
            477 => 
            array (
                'id' => 498,
                'company_id' => 47,
                'block_id' => 106,
                'school_academic_year_id' => 5,
            ),
            478 => 
            array (
                'id' => 499,
                'company_id' => 47,
                'block_id' => 107,
                'school_academic_year_id' => 5,
            ),
            479 => 
            array (
                'id' => 500,
                'company_id' => 47,
                'block_id' => 234,
                'school_academic_year_id' => 5,
            ),
            480 => 
            array (
                'id' => 501,
                'company_id' => 47,
                'block_id' => 455,
                'school_academic_year_id' => 5,
            ),
            481 => 
            array (
                'id' => 502,
                'company_id' => 47,
                'block_id' => 106,
                'school_academic_year_id' => 6,
            ),
            482 => 
            array (
                'id' => 503,
                'company_id' => 47,
                'block_id' => 107,
                'school_academic_year_id' => 6,
            ),
            483 => 
            array (
                'id' => 504,
                'company_id' => 47,
                'block_id' => 234,
                'school_academic_year_id' => 6,
            ),
            484 => 
            array (
                'id' => 505,
                'company_id' => 47,
                'block_id' => 455,
                'school_academic_year_id' => 6,
            ),
            485 => 
            array (
                'id' => 506,
                'company_id' => 48,
                'block_id' => 108,
                'school_academic_year_id' => 2,
            ),
            486 => 
            array (
                'id' => 507,
                'company_id' => 48,
                'block_id' => 109,
                'school_academic_year_id' => 2,
            ),
            487 => 
            array (
                'id' => 508,
                'company_id' => 48,
                'block_id' => 110,
                'school_academic_year_id' => 2,
            ),
            488 => 
            array (
                'id' => 509,
                'company_id' => 48,
                'block_id' => 111,
                'school_academic_year_id' => 2,
            ),
            489 => 
            array (
                'id' => 510,
                'company_id' => 48,
                'block_id' => 112,
                'school_academic_year_id' => 2,
            ),
            490 => 
            array (
                'id' => 511,
                'company_id' => 48,
                'block_id' => 113,
                'school_academic_year_id' => 2,
            ),
            491 => 
            array (
                'id' => 512,
                'company_id' => 48,
                'block_id' => 117,
                'school_academic_year_id' => 2,
            ),
            492 => 
            array (
                'id' => 513,
                'company_id' => 48,
                'block_id' => 118,
                'school_academic_year_id' => 2,
            ),
            493 => 
            array (
                'id' => 514,
                'company_id' => 48,
                'block_id' => 119,
                'school_academic_year_id' => 2,
            ),
            494 => 
            array (
                'id' => 515,
                'company_id' => 48,
                'block_id' => 120,
                'school_academic_year_id' => 2,
            ),
            495 => 
            array (
                'id' => 516,
                'company_id' => 48,
                'block_id' => 121,
                'school_academic_year_id' => 2,
            ),
            496 => 
            array (
                'id' => 517,
                'company_id' => 48,
                'block_id' => 122,
                'school_academic_year_id' => 2,
            ),
            497 => 
            array (
                'id' => 518,
                'company_id' => 48,
                'block_id' => 123,
                'school_academic_year_id' => 2,
            ),
            498 => 
            array (
                'id' => 519,
                'company_id' => 48,
                'block_id' => 124,
                'school_academic_year_id' => 2,
            ),
            499 => 
            array (
                'id' => 520,
                'company_id' => 48,
                'block_id' => 164,
                'school_academic_year_id' => 2,
            ),
        ));
        \DB::table('block_years')->insert(array (
            0 => 
            array (
                'id' => 521,
                'company_id' => 48,
                'block_id' => 227,
                'school_academic_year_id' => 2,
            ),
            1 => 
            array (
                'id' => 522,
                'company_id' => 48,
                'block_id' => 108,
                'school_academic_year_id' => 3,
            ),
            2 => 
            array (
                'id' => 523,
                'company_id' => 48,
                'block_id' => 109,
                'school_academic_year_id' => 3,
            ),
            3 => 
            array (
                'id' => 524,
                'company_id' => 48,
                'block_id' => 110,
                'school_academic_year_id' => 3,
            ),
            4 => 
            array (
                'id' => 525,
                'company_id' => 48,
                'block_id' => 111,
                'school_academic_year_id' => 3,
            ),
            5 => 
            array (
                'id' => 526,
                'company_id' => 48,
                'block_id' => 112,
                'school_academic_year_id' => 3,
            ),
            6 => 
            array (
                'id' => 527,
                'company_id' => 48,
                'block_id' => 113,
                'school_academic_year_id' => 3,
            ),
            7 => 
            array (
                'id' => 528,
                'company_id' => 48,
                'block_id' => 117,
                'school_academic_year_id' => 3,
            ),
            8 => 
            array (
                'id' => 529,
                'company_id' => 48,
                'block_id' => 118,
                'school_academic_year_id' => 3,
            ),
            9 => 
            array (
                'id' => 530,
                'company_id' => 48,
                'block_id' => 119,
                'school_academic_year_id' => 3,
            ),
            10 => 
            array (
                'id' => 531,
                'company_id' => 48,
                'block_id' => 120,
                'school_academic_year_id' => 3,
            ),
            11 => 
            array (
                'id' => 532,
                'company_id' => 48,
                'block_id' => 121,
                'school_academic_year_id' => 3,
            ),
            12 => 
            array (
                'id' => 533,
                'company_id' => 48,
                'block_id' => 122,
                'school_academic_year_id' => 3,
            ),
            13 => 
            array (
                'id' => 534,
                'company_id' => 48,
                'block_id' => 123,
                'school_academic_year_id' => 3,
            ),
            14 => 
            array (
                'id' => 535,
                'company_id' => 48,
                'block_id' => 124,
                'school_academic_year_id' => 3,
            ),
            15 => 
            array (
                'id' => 536,
                'company_id' => 48,
                'block_id' => 164,
                'school_academic_year_id' => 3,
            ),
            16 => 
            array (
                'id' => 537,
                'company_id' => 48,
                'block_id' => 227,
                'school_academic_year_id' => 3,
            ),
            17 => 
            array (
                'id' => 538,
                'company_id' => 48,
                'block_id' => 108,
                'school_academic_year_id' => 4,
            ),
            18 => 
            array (
                'id' => 539,
                'company_id' => 48,
                'block_id' => 109,
                'school_academic_year_id' => 4,
            ),
            19 => 
            array (
                'id' => 540,
                'company_id' => 48,
                'block_id' => 110,
                'school_academic_year_id' => 4,
            ),
            20 => 
            array (
                'id' => 541,
                'company_id' => 48,
                'block_id' => 111,
                'school_academic_year_id' => 4,
            ),
            21 => 
            array (
                'id' => 542,
                'company_id' => 48,
                'block_id' => 112,
                'school_academic_year_id' => 4,
            ),
            22 => 
            array (
                'id' => 543,
                'company_id' => 48,
                'block_id' => 113,
                'school_academic_year_id' => 4,
            ),
            23 => 
            array (
                'id' => 544,
                'company_id' => 48,
                'block_id' => 117,
                'school_academic_year_id' => 4,
            ),
            24 => 
            array (
                'id' => 545,
                'company_id' => 48,
                'block_id' => 118,
                'school_academic_year_id' => 4,
            ),
            25 => 
            array (
                'id' => 546,
                'company_id' => 48,
                'block_id' => 119,
                'school_academic_year_id' => 4,
            ),
            26 => 
            array (
                'id' => 547,
                'company_id' => 48,
                'block_id' => 120,
                'school_academic_year_id' => 4,
            ),
            27 => 
            array (
                'id' => 548,
                'company_id' => 48,
                'block_id' => 121,
                'school_academic_year_id' => 4,
            ),
            28 => 
            array (
                'id' => 549,
                'company_id' => 48,
                'block_id' => 122,
                'school_academic_year_id' => 4,
            ),
            29 => 
            array (
                'id' => 550,
                'company_id' => 48,
                'block_id' => 123,
                'school_academic_year_id' => 4,
            ),
            30 => 
            array (
                'id' => 551,
                'company_id' => 48,
                'block_id' => 124,
                'school_academic_year_id' => 4,
            ),
            31 => 
            array (
                'id' => 552,
                'company_id' => 48,
                'block_id' => 164,
                'school_academic_year_id' => 4,
            ),
            32 => 
            array (
                'id' => 553,
                'company_id' => 48,
                'block_id' => 227,
                'school_academic_year_id' => 4,
            ),
            33 => 
            array (
                'id' => 554,
                'company_id' => 48,
                'block_id' => 108,
                'school_academic_year_id' => 5,
            ),
            34 => 
            array (
                'id' => 555,
                'company_id' => 48,
                'block_id' => 109,
                'school_academic_year_id' => 5,
            ),
            35 => 
            array (
                'id' => 556,
                'company_id' => 48,
                'block_id' => 110,
                'school_academic_year_id' => 5,
            ),
            36 => 
            array (
                'id' => 557,
                'company_id' => 48,
                'block_id' => 111,
                'school_academic_year_id' => 5,
            ),
            37 => 
            array (
                'id' => 558,
                'company_id' => 48,
                'block_id' => 112,
                'school_academic_year_id' => 5,
            ),
            38 => 
            array (
                'id' => 559,
                'company_id' => 48,
                'block_id' => 113,
                'school_academic_year_id' => 5,
            ),
            39 => 
            array (
                'id' => 560,
                'company_id' => 48,
                'block_id' => 117,
                'school_academic_year_id' => 5,
            ),
            40 => 
            array (
                'id' => 561,
                'company_id' => 48,
                'block_id' => 118,
                'school_academic_year_id' => 5,
            ),
            41 => 
            array (
                'id' => 562,
                'company_id' => 48,
                'block_id' => 119,
                'school_academic_year_id' => 5,
            ),
            42 => 
            array (
                'id' => 563,
                'company_id' => 48,
                'block_id' => 120,
                'school_academic_year_id' => 5,
            ),
            43 => 
            array (
                'id' => 564,
                'company_id' => 48,
                'block_id' => 121,
                'school_academic_year_id' => 5,
            ),
            44 => 
            array (
                'id' => 565,
                'company_id' => 48,
                'block_id' => 122,
                'school_academic_year_id' => 5,
            ),
            45 => 
            array (
                'id' => 566,
                'company_id' => 48,
                'block_id' => 123,
                'school_academic_year_id' => 5,
            ),
            46 => 
            array (
                'id' => 567,
                'company_id' => 48,
                'block_id' => 124,
                'school_academic_year_id' => 5,
            ),
            47 => 
            array (
                'id' => 568,
                'company_id' => 48,
                'block_id' => 164,
                'school_academic_year_id' => 5,
            ),
            48 => 
            array (
                'id' => 569,
                'company_id' => 48,
                'block_id' => 227,
                'school_academic_year_id' => 5,
            ),
            49 => 
            array (
                'id' => 570,
                'company_id' => 48,
                'block_id' => 108,
                'school_academic_year_id' => 6,
            ),
            50 => 
            array (
                'id' => 571,
                'company_id' => 48,
                'block_id' => 109,
                'school_academic_year_id' => 6,
            ),
            51 => 
            array (
                'id' => 572,
                'company_id' => 48,
                'block_id' => 110,
                'school_academic_year_id' => 6,
            ),
            52 => 
            array (
                'id' => 573,
                'company_id' => 48,
                'block_id' => 111,
                'school_academic_year_id' => 6,
            ),
            53 => 
            array (
                'id' => 574,
                'company_id' => 48,
                'block_id' => 112,
                'school_academic_year_id' => 6,
            ),
            54 => 
            array (
                'id' => 575,
                'company_id' => 48,
                'block_id' => 113,
                'school_academic_year_id' => 6,
            ),
            55 => 
            array (
                'id' => 576,
                'company_id' => 48,
                'block_id' => 117,
                'school_academic_year_id' => 6,
            ),
            56 => 
            array (
                'id' => 577,
                'company_id' => 48,
                'block_id' => 118,
                'school_academic_year_id' => 6,
            ),
            57 => 
            array (
                'id' => 578,
                'company_id' => 48,
                'block_id' => 119,
                'school_academic_year_id' => 6,
            ),
            58 => 
            array (
                'id' => 579,
                'company_id' => 48,
                'block_id' => 120,
                'school_academic_year_id' => 6,
            ),
            59 => 
            array (
                'id' => 580,
                'company_id' => 48,
                'block_id' => 121,
                'school_academic_year_id' => 6,
            ),
            60 => 
            array (
                'id' => 581,
                'company_id' => 48,
                'block_id' => 122,
                'school_academic_year_id' => 6,
            ),
            61 => 
            array (
                'id' => 582,
                'company_id' => 48,
                'block_id' => 123,
                'school_academic_year_id' => 6,
            ),
            62 => 
            array (
                'id' => 583,
                'company_id' => 48,
                'block_id' => 124,
                'school_academic_year_id' => 6,
            ),
            63 => 
            array (
                'id' => 584,
                'company_id' => 48,
                'block_id' => 164,
                'school_academic_year_id' => 6,
            ),
            64 => 
            array (
                'id' => 585,
                'company_id' => 48,
                'block_id' => 227,
                'school_academic_year_id' => 6,
            ),
            65 => 
            array (
                'id' => 586,
                'company_id' => 49,
                'block_id' => 116,
                'school_academic_year_id' => 2,
            ),
            66 => 
            array (
                'id' => 587,
                'company_id' => 49,
                'block_id' => 226,
                'school_academic_year_id' => 2,
            ),
            67 => 
            array (
                'id' => 588,
                'company_id' => 49,
                'block_id' => 116,
                'school_academic_year_id' => 3,
            ),
            68 => 
            array (
                'id' => 589,
                'company_id' => 49,
                'block_id' => 226,
                'school_academic_year_id' => 3,
            ),
            69 => 
            array (
                'id' => 590,
                'company_id' => 49,
                'block_id' => 116,
                'school_academic_year_id' => 4,
            ),
            70 => 
            array (
                'id' => 591,
                'company_id' => 49,
                'block_id' => 226,
                'school_academic_year_id' => 4,
            ),
            71 => 
            array (
                'id' => 592,
                'company_id' => 49,
                'block_id' => 116,
                'school_academic_year_id' => 5,
            ),
            72 => 
            array (
                'id' => 593,
                'company_id' => 49,
                'block_id' => 226,
                'school_academic_year_id' => 5,
            ),
            73 => 
            array (
                'id' => 594,
                'company_id' => 49,
                'block_id' => 116,
                'school_academic_year_id' => 6,
            ),
            74 => 
            array (
                'id' => 595,
                'company_id' => 49,
                'block_id' => 226,
                'school_academic_year_id' => 6,
            ),
            75 => 
            array (
                'id' => 596,
                'company_id' => 50,
                'block_id' => 125,
                'school_academic_year_id' => 2,
            ),
            76 => 
            array (
                'id' => 597,
                'company_id' => 50,
                'block_id' => 306,
                'school_academic_year_id' => 2,
            ),
            77 => 
            array (
                'id' => 598,
                'company_id' => 50,
                'block_id' => 307,
                'school_academic_year_id' => 2,
            ),
            78 => 
            array (
                'id' => 599,
                'company_id' => 50,
                'block_id' => 310,
                'school_academic_year_id' => 2,
            ),
            79 => 
            array (
                'id' => 600,
                'company_id' => 50,
                'block_id' => 125,
                'school_academic_year_id' => 3,
            ),
            80 => 
            array (
                'id' => 601,
                'company_id' => 50,
                'block_id' => 306,
                'school_academic_year_id' => 3,
            ),
            81 => 
            array (
                'id' => 602,
                'company_id' => 50,
                'block_id' => 307,
                'school_academic_year_id' => 3,
            ),
            82 => 
            array (
                'id' => 603,
                'company_id' => 50,
                'block_id' => 310,
                'school_academic_year_id' => 3,
            ),
            83 => 
            array (
                'id' => 604,
                'company_id' => 50,
                'block_id' => 125,
                'school_academic_year_id' => 4,
            ),
            84 => 
            array (
                'id' => 605,
                'company_id' => 50,
                'block_id' => 306,
                'school_academic_year_id' => 4,
            ),
            85 => 
            array (
                'id' => 606,
                'company_id' => 50,
                'block_id' => 307,
                'school_academic_year_id' => 4,
            ),
            86 => 
            array (
                'id' => 607,
                'company_id' => 50,
                'block_id' => 310,
                'school_academic_year_id' => 4,
            ),
            87 => 
            array (
                'id' => 608,
                'company_id' => 50,
                'block_id' => 125,
                'school_academic_year_id' => 5,
            ),
            88 => 
            array (
                'id' => 609,
                'company_id' => 50,
                'block_id' => 306,
                'school_academic_year_id' => 5,
            ),
            89 => 
            array (
                'id' => 610,
                'company_id' => 50,
                'block_id' => 307,
                'school_academic_year_id' => 5,
            ),
            90 => 
            array (
                'id' => 611,
                'company_id' => 50,
                'block_id' => 310,
                'school_academic_year_id' => 5,
            ),
            91 => 
            array (
                'id' => 612,
                'company_id' => 50,
                'block_id' => 125,
                'school_academic_year_id' => 6,
            ),
            92 => 
            array (
                'id' => 613,
                'company_id' => 50,
                'block_id' => 306,
                'school_academic_year_id' => 6,
            ),
            93 => 
            array (
                'id' => 614,
                'company_id' => 50,
                'block_id' => 307,
                'school_academic_year_id' => 6,
            ),
            94 => 
            array (
                'id' => 615,
                'company_id' => 50,
                'block_id' => 310,
                'school_academic_year_id' => 6,
            ),
            95 => 
            array (
                'id' => 616,
                'company_id' => 51,
                'block_id' => 126,
                'school_academic_year_id' => 2,
            ),
            96 => 
            array (
                'id' => 617,
                'company_id' => 51,
                'block_id' => 126,
                'school_academic_year_id' => 3,
            ),
            97 => 
            array (
                'id' => 618,
                'company_id' => 51,
                'block_id' => 126,
                'school_academic_year_id' => 4,
            ),
            98 => 
            array (
                'id' => 619,
                'company_id' => 51,
                'block_id' => 126,
                'school_academic_year_id' => 5,
            ),
            99 => 
            array (
                'id' => 620,
                'company_id' => 51,
                'block_id' => 126,
                'school_academic_year_id' => 6,
            ),
            100 => 
            array (
                'id' => 621,
                'company_id' => 52,
                'block_id' => 136,
                'school_academic_year_id' => 2,
            ),
            101 => 
            array (
                'id' => 622,
                'company_id' => 52,
                'block_id' => 381,
                'school_academic_year_id' => 2,
            ),
            102 => 
            array (
                'id' => 623,
                'company_id' => 52,
                'block_id' => 382,
                'school_academic_year_id' => 2,
            ),
            103 => 
            array (
                'id' => 624,
                'company_id' => 52,
                'block_id' => 136,
                'school_academic_year_id' => 3,
            ),
            104 => 
            array (
                'id' => 625,
                'company_id' => 52,
                'block_id' => 381,
                'school_academic_year_id' => 3,
            ),
            105 => 
            array (
                'id' => 626,
                'company_id' => 52,
                'block_id' => 382,
                'school_academic_year_id' => 3,
            ),
            106 => 
            array (
                'id' => 627,
                'company_id' => 52,
                'block_id' => 136,
                'school_academic_year_id' => 4,
            ),
            107 => 
            array (
                'id' => 628,
                'company_id' => 52,
                'block_id' => 381,
                'school_academic_year_id' => 4,
            ),
            108 => 
            array (
                'id' => 629,
                'company_id' => 52,
                'block_id' => 382,
                'school_academic_year_id' => 4,
            ),
            109 => 
            array (
                'id' => 630,
                'company_id' => 52,
                'block_id' => 136,
                'school_academic_year_id' => 5,
            ),
            110 => 
            array (
                'id' => 631,
                'company_id' => 52,
                'block_id' => 381,
                'school_academic_year_id' => 5,
            ),
            111 => 
            array (
                'id' => 632,
                'company_id' => 52,
                'block_id' => 382,
                'school_academic_year_id' => 5,
            ),
            112 => 
            array (
                'id' => 633,
                'company_id' => 52,
                'block_id' => 136,
                'school_academic_year_id' => 6,
            ),
            113 => 
            array (
                'id' => 634,
                'company_id' => 52,
                'block_id' => 381,
                'school_academic_year_id' => 6,
            ),
            114 => 
            array (
                'id' => 635,
                'company_id' => 52,
                'block_id' => 382,
                'school_academic_year_id' => 6,
            ),
            115 => 
            array (
                'id' => 636,
                'company_id' => 53,
                'block_id' => 131,
                'school_academic_year_id' => 2,
            ),
            116 => 
            array (
                'id' => 637,
                'company_id' => 53,
                'block_id' => 132,
                'school_academic_year_id' => 2,
            ),
            117 => 
            array (
                'id' => 638,
                'company_id' => 53,
                'block_id' => 133,
                'school_academic_year_id' => 2,
            ),
            118 => 
            array (
                'id' => 639,
                'company_id' => 53,
                'block_id' => 134,
                'school_academic_year_id' => 2,
            ),
            119 => 
            array (
                'id' => 640,
                'company_id' => 53,
                'block_id' => 135,
                'school_academic_year_id' => 2,
            ),
            120 => 
            array (
                'id' => 641,
                'company_id' => 53,
                'block_id' => 297,
                'school_academic_year_id' => 2,
            ),
            121 => 
            array (
                'id' => 642,
                'company_id' => 53,
                'block_id' => 298,
                'school_academic_year_id' => 2,
            ),
            122 => 
            array (
                'id' => 643,
                'company_id' => 53,
                'block_id' => 433,
                'school_academic_year_id' => 2,
            ),
            123 => 
            array (
                'id' => 644,
                'company_id' => 53,
                'block_id' => 131,
                'school_academic_year_id' => 3,
            ),
            124 => 
            array (
                'id' => 645,
                'company_id' => 53,
                'block_id' => 132,
                'school_academic_year_id' => 3,
            ),
            125 => 
            array (
                'id' => 646,
                'company_id' => 53,
                'block_id' => 133,
                'school_academic_year_id' => 3,
            ),
            126 => 
            array (
                'id' => 647,
                'company_id' => 53,
                'block_id' => 134,
                'school_academic_year_id' => 3,
            ),
            127 => 
            array (
                'id' => 648,
                'company_id' => 53,
                'block_id' => 135,
                'school_academic_year_id' => 3,
            ),
            128 => 
            array (
                'id' => 649,
                'company_id' => 53,
                'block_id' => 297,
                'school_academic_year_id' => 3,
            ),
            129 => 
            array (
                'id' => 650,
                'company_id' => 53,
                'block_id' => 298,
                'school_academic_year_id' => 3,
            ),
            130 => 
            array (
                'id' => 651,
                'company_id' => 53,
                'block_id' => 433,
                'school_academic_year_id' => 3,
            ),
            131 => 
            array (
                'id' => 652,
                'company_id' => 53,
                'block_id' => 131,
                'school_academic_year_id' => 4,
            ),
            132 => 
            array (
                'id' => 653,
                'company_id' => 53,
                'block_id' => 132,
                'school_academic_year_id' => 4,
            ),
            133 => 
            array (
                'id' => 654,
                'company_id' => 53,
                'block_id' => 133,
                'school_academic_year_id' => 4,
            ),
            134 => 
            array (
                'id' => 655,
                'company_id' => 53,
                'block_id' => 134,
                'school_academic_year_id' => 4,
            ),
            135 => 
            array (
                'id' => 656,
                'company_id' => 53,
                'block_id' => 135,
                'school_academic_year_id' => 4,
            ),
            136 => 
            array (
                'id' => 657,
                'company_id' => 53,
                'block_id' => 297,
                'school_academic_year_id' => 4,
            ),
            137 => 
            array (
                'id' => 658,
                'company_id' => 53,
                'block_id' => 298,
                'school_academic_year_id' => 4,
            ),
            138 => 
            array (
                'id' => 659,
                'company_id' => 53,
                'block_id' => 433,
                'school_academic_year_id' => 4,
            ),
            139 => 
            array (
                'id' => 660,
                'company_id' => 53,
                'block_id' => 131,
                'school_academic_year_id' => 5,
            ),
            140 => 
            array (
                'id' => 661,
                'company_id' => 53,
                'block_id' => 132,
                'school_academic_year_id' => 5,
            ),
            141 => 
            array (
                'id' => 662,
                'company_id' => 53,
                'block_id' => 133,
                'school_academic_year_id' => 5,
            ),
            142 => 
            array (
                'id' => 663,
                'company_id' => 53,
                'block_id' => 134,
                'school_academic_year_id' => 5,
            ),
            143 => 
            array (
                'id' => 664,
                'company_id' => 53,
                'block_id' => 135,
                'school_academic_year_id' => 5,
            ),
            144 => 
            array (
                'id' => 665,
                'company_id' => 53,
                'block_id' => 297,
                'school_academic_year_id' => 5,
            ),
            145 => 
            array (
                'id' => 666,
                'company_id' => 53,
                'block_id' => 298,
                'school_academic_year_id' => 5,
            ),
            146 => 
            array (
                'id' => 667,
                'company_id' => 53,
                'block_id' => 433,
                'school_academic_year_id' => 5,
            ),
            147 => 
            array (
                'id' => 668,
                'company_id' => 53,
                'block_id' => 131,
                'school_academic_year_id' => 6,
            ),
            148 => 
            array (
                'id' => 669,
                'company_id' => 53,
                'block_id' => 132,
                'school_academic_year_id' => 6,
            ),
            149 => 
            array (
                'id' => 670,
                'company_id' => 53,
                'block_id' => 133,
                'school_academic_year_id' => 6,
            ),
            150 => 
            array (
                'id' => 671,
                'company_id' => 53,
                'block_id' => 134,
                'school_academic_year_id' => 6,
            ),
            151 => 
            array (
                'id' => 672,
                'company_id' => 53,
                'block_id' => 135,
                'school_academic_year_id' => 6,
            ),
            152 => 
            array (
                'id' => 673,
                'company_id' => 53,
                'block_id' => 297,
                'school_academic_year_id' => 6,
            ),
            153 => 
            array (
                'id' => 674,
                'company_id' => 53,
                'block_id' => 298,
                'school_academic_year_id' => 6,
            ),
            154 => 
            array (
                'id' => 675,
                'company_id' => 53,
                'block_id' => 433,
                'school_academic_year_id' => 6,
            ),
            155 => 
            array (
                'id' => 676,
                'company_id' => 54,
                'block_id' => 137,
                'school_academic_year_id' => 2,
            ),
            156 => 
            array (
                'id' => 677,
                'company_id' => 54,
                'block_id' => 256,
                'school_academic_year_id' => 2,
            ),
            157 => 
            array (
                'id' => 678,
                'company_id' => 54,
                'block_id' => 386,
                'school_academic_year_id' => 2,
            ),
            158 => 
            array (
                'id' => 679,
                'company_id' => 54,
                'block_id' => 137,
                'school_academic_year_id' => 3,
            ),
            159 => 
            array (
                'id' => 680,
                'company_id' => 54,
                'block_id' => 256,
                'school_academic_year_id' => 3,
            ),
            160 => 
            array (
                'id' => 681,
                'company_id' => 54,
                'block_id' => 386,
                'school_academic_year_id' => 3,
            ),
            161 => 
            array (
                'id' => 682,
                'company_id' => 54,
                'block_id' => 137,
                'school_academic_year_id' => 4,
            ),
            162 => 
            array (
                'id' => 683,
                'company_id' => 54,
                'block_id' => 256,
                'school_academic_year_id' => 4,
            ),
            163 => 
            array (
                'id' => 684,
                'company_id' => 54,
                'block_id' => 386,
                'school_academic_year_id' => 4,
            ),
            164 => 
            array (
                'id' => 685,
                'company_id' => 54,
                'block_id' => 137,
                'school_academic_year_id' => 5,
            ),
            165 => 
            array (
                'id' => 686,
                'company_id' => 54,
                'block_id' => 256,
                'school_academic_year_id' => 5,
            ),
            166 => 
            array (
                'id' => 687,
                'company_id' => 54,
                'block_id' => 386,
                'school_academic_year_id' => 5,
            ),
            167 => 
            array (
                'id' => 688,
                'company_id' => 54,
                'block_id' => 137,
                'school_academic_year_id' => 6,
            ),
            168 => 
            array (
                'id' => 689,
                'company_id' => 54,
                'block_id' => 256,
                'school_academic_year_id' => 6,
            ),
            169 => 
            array (
                'id' => 690,
                'company_id' => 54,
                'block_id' => 386,
                'school_academic_year_id' => 6,
            ),
            170 => 
            array (
                'id' => 691,
                'company_id' => 55,
                'block_id' => 138,
                'school_academic_year_id' => 2,
            ),
            171 => 
            array (
                'id' => 692,
                'company_id' => 55,
                'block_id' => 219,
                'school_academic_year_id' => 2,
            ),
            172 => 
            array (
                'id' => 693,
                'company_id' => 55,
                'block_id' => 138,
                'school_academic_year_id' => 3,
            ),
            173 => 
            array (
                'id' => 694,
                'company_id' => 55,
                'block_id' => 219,
                'school_academic_year_id' => 3,
            ),
            174 => 
            array (
                'id' => 695,
                'company_id' => 55,
                'block_id' => 138,
                'school_academic_year_id' => 4,
            ),
            175 => 
            array (
                'id' => 696,
                'company_id' => 55,
                'block_id' => 219,
                'school_academic_year_id' => 4,
            ),
            176 => 
            array (
                'id' => 697,
                'company_id' => 55,
                'block_id' => 138,
                'school_academic_year_id' => 5,
            ),
            177 => 
            array (
                'id' => 698,
                'company_id' => 55,
                'block_id' => 219,
                'school_academic_year_id' => 5,
            ),
            178 => 
            array (
                'id' => 699,
                'company_id' => 55,
                'block_id' => 138,
                'school_academic_year_id' => 6,
            ),
            179 => 
            array (
                'id' => 700,
                'company_id' => 55,
                'block_id' => 219,
                'school_academic_year_id' => 6,
            ),
            180 => 
            array (
                'id' => 701,
                'company_id' => 56,
                'block_id' => 139,
                'school_academic_year_id' => 2,
            ),
            181 => 
            array (
                'id' => 702,
                'company_id' => 56,
                'block_id' => 235,
                'school_academic_year_id' => 2,
            ),
            182 => 
            array (
                'id' => 703,
                'company_id' => 56,
                'block_id' => 336,
                'school_academic_year_id' => 2,
            ),
            183 => 
            array (
                'id' => 704,
                'company_id' => 56,
                'block_id' => 432,
                'school_academic_year_id' => 2,
            ),
            184 => 
            array (
                'id' => 705,
                'company_id' => 56,
                'block_id' => 139,
                'school_academic_year_id' => 3,
            ),
            185 => 
            array (
                'id' => 706,
                'company_id' => 56,
                'block_id' => 235,
                'school_academic_year_id' => 3,
            ),
            186 => 
            array (
                'id' => 707,
                'company_id' => 56,
                'block_id' => 336,
                'school_academic_year_id' => 3,
            ),
            187 => 
            array (
                'id' => 708,
                'company_id' => 56,
                'block_id' => 432,
                'school_academic_year_id' => 3,
            ),
            188 => 
            array (
                'id' => 709,
                'company_id' => 56,
                'block_id' => 139,
                'school_academic_year_id' => 4,
            ),
            189 => 
            array (
                'id' => 710,
                'company_id' => 56,
                'block_id' => 235,
                'school_academic_year_id' => 4,
            ),
            190 => 
            array (
                'id' => 711,
                'company_id' => 56,
                'block_id' => 336,
                'school_academic_year_id' => 4,
            ),
            191 => 
            array (
                'id' => 712,
                'company_id' => 56,
                'block_id' => 432,
                'school_academic_year_id' => 4,
            ),
            192 => 
            array (
                'id' => 713,
                'company_id' => 56,
                'block_id' => 139,
                'school_academic_year_id' => 5,
            ),
            193 => 
            array (
                'id' => 714,
                'company_id' => 56,
                'block_id' => 235,
                'school_academic_year_id' => 5,
            ),
            194 => 
            array (
                'id' => 715,
                'company_id' => 56,
                'block_id' => 336,
                'school_academic_year_id' => 5,
            ),
            195 => 
            array (
                'id' => 716,
                'company_id' => 56,
                'block_id' => 432,
                'school_academic_year_id' => 5,
            ),
            196 => 
            array (
                'id' => 717,
                'company_id' => 56,
                'block_id' => 139,
                'school_academic_year_id' => 6,
            ),
            197 => 
            array (
                'id' => 718,
                'company_id' => 56,
                'block_id' => 235,
                'school_academic_year_id' => 6,
            ),
            198 => 
            array (
                'id' => 719,
                'company_id' => 56,
                'block_id' => 336,
                'school_academic_year_id' => 6,
            ),
            199 => 
            array (
                'id' => 720,
                'company_id' => 56,
                'block_id' => 432,
                'school_academic_year_id' => 6,
            ),
            200 => 
            array (
                'id' => 721,
                'company_id' => 57,
                'block_id' => 140,
                'school_academic_year_id' => 2,
            ),
            201 => 
            array (
                'id' => 722,
                'company_id' => 57,
                'block_id' => 140,
                'school_academic_year_id' => 3,
            ),
            202 => 
            array (
                'id' => 723,
                'company_id' => 57,
                'block_id' => 140,
                'school_academic_year_id' => 4,
            ),
            203 => 
            array (
                'id' => 724,
                'company_id' => 57,
                'block_id' => 140,
                'school_academic_year_id' => 5,
            ),
            204 => 
            array (
                'id' => 725,
                'company_id' => 57,
                'block_id' => 140,
                'school_academic_year_id' => 6,
            ),
            205 => 
            array (
                'id' => 726,
                'company_id' => 58,
                'block_id' => 141,
                'school_academic_year_id' => 2,
            ),
            206 => 
            array (
                'id' => 727,
                'company_id' => 58,
                'block_id' => 142,
                'school_academic_year_id' => 2,
            ),
            207 => 
            array (
                'id' => 728,
                'company_id' => 58,
                'block_id' => 143,
                'school_academic_year_id' => 2,
            ),
            208 => 
            array (
                'id' => 729,
                'company_id' => 58,
                'block_id' => 144,
                'school_academic_year_id' => 2,
            ),
            209 => 
            array (
                'id' => 730,
                'company_id' => 58,
                'block_id' => 148,
                'school_academic_year_id' => 2,
            ),
            210 => 
            array (
                'id' => 731,
                'company_id' => 58,
                'block_id' => 242,
                'school_academic_year_id' => 2,
            ),
            211 => 
            array (
                'id' => 732,
                'company_id' => 58,
                'block_id' => 141,
                'school_academic_year_id' => 3,
            ),
            212 => 
            array (
                'id' => 733,
                'company_id' => 58,
                'block_id' => 142,
                'school_academic_year_id' => 3,
            ),
            213 => 
            array (
                'id' => 734,
                'company_id' => 58,
                'block_id' => 143,
                'school_academic_year_id' => 3,
            ),
            214 => 
            array (
                'id' => 735,
                'company_id' => 58,
                'block_id' => 144,
                'school_academic_year_id' => 3,
            ),
            215 => 
            array (
                'id' => 736,
                'company_id' => 58,
                'block_id' => 148,
                'school_academic_year_id' => 3,
            ),
            216 => 
            array (
                'id' => 737,
                'company_id' => 58,
                'block_id' => 242,
                'school_academic_year_id' => 3,
            ),
            217 => 
            array (
                'id' => 738,
                'company_id' => 58,
                'block_id' => 141,
                'school_academic_year_id' => 4,
            ),
            218 => 
            array (
                'id' => 739,
                'company_id' => 58,
                'block_id' => 142,
                'school_academic_year_id' => 4,
            ),
            219 => 
            array (
                'id' => 740,
                'company_id' => 58,
                'block_id' => 143,
                'school_academic_year_id' => 4,
            ),
            220 => 
            array (
                'id' => 741,
                'company_id' => 58,
                'block_id' => 144,
                'school_academic_year_id' => 4,
            ),
            221 => 
            array (
                'id' => 742,
                'company_id' => 58,
                'block_id' => 148,
                'school_academic_year_id' => 4,
            ),
            222 => 
            array (
                'id' => 743,
                'company_id' => 58,
                'block_id' => 242,
                'school_academic_year_id' => 4,
            ),
            223 => 
            array (
                'id' => 744,
                'company_id' => 58,
                'block_id' => 141,
                'school_academic_year_id' => 5,
            ),
            224 => 
            array (
                'id' => 745,
                'company_id' => 58,
                'block_id' => 142,
                'school_academic_year_id' => 5,
            ),
            225 => 
            array (
                'id' => 746,
                'company_id' => 58,
                'block_id' => 143,
                'school_academic_year_id' => 5,
            ),
            226 => 
            array (
                'id' => 747,
                'company_id' => 58,
                'block_id' => 144,
                'school_academic_year_id' => 5,
            ),
            227 => 
            array (
                'id' => 748,
                'company_id' => 58,
                'block_id' => 148,
                'school_academic_year_id' => 5,
            ),
            228 => 
            array (
                'id' => 749,
                'company_id' => 58,
                'block_id' => 242,
                'school_academic_year_id' => 5,
            ),
            229 => 
            array (
                'id' => 750,
                'company_id' => 58,
                'block_id' => 141,
                'school_academic_year_id' => 6,
            ),
            230 => 
            array (
                'id' => 751,
                'company_id' => 58,
                'block_id' => 142,
                'school_academic_year_id' => 6,
            ),
            231 => 
            array (
                'id' => 752,
                'company_id' => 58,
                'block_id' => 143,
                'school_academic_year_id' => 6,
            ),
            232 => 
            array (
                'id' => 753,
                'company_id' => 58,
                'block_id' => 144,
                'school_academic_year_id' => 6,
            ),
            233 => 
            array (
                'id' => 754,
                'company_id' => 58,
                'block_id' => 148,
                'school_academic_year_id' => 6,
            ),
            234 => 
            array (
                'id' => 755,
                'company_id' => 58,
                'block_id' => 242,
                'school_academic_year_id' => 6,
            ),
            235 => 
            array (
                'id' => 756,
                'company_id' => 59,
                'block_id' => 145,
                'school_academic_year_id' => 2,
            ),
            236 => 
            array (
                'id' => 757,
                'company_id' => 59,
                'block_id' => 147,
                'school_academic_year_id' => 2,
            ),
            237 => 
            array (
                'id' => 758,
                'company_id' => 59,
                'block_id' => 145,
                'school_academic_year_id' => 3,
            ),
            238 => 
            array (
                'id' => 759,
                'company_id' => 59,
                'block_id' => 147,
                'school_academic_year_id' => 3,
            ),
            239 => 
            array (
                'id' => 760,
                'company_id' => 59,
                'block_id' => 145,
                'school_academic_year_id' => 4,
            ),
            240 => 
            array (
                'id' => 761,
                'company_id' => 59,
                'block_id' => 147,
                'school_academic_year_id' => 4,
            ),
            241 => 
            array (
                'id' => 762,
                'company_id' => 59,
                'block_id' => 145,
                'school_academic_year_id' => 5,
            ),
            242 => 
            array (
                'id' => 763,
                'company_id' => 59,
                'block_id' => 147,
                'school_academic_year_id' => 5,
            ),
            243 => 
            array (
                'id' => 764,
                'company_id' => 59,
                'block_id' => 145,
                'school_academic_year_id' => 6,
            ),
            244 => 
            array (
                'id' => 765,
                'company_id' => 59,
                'block_id' => 147,
                'school_academic_year_id' => 6,
            ),
            245 => 
            array (
                'id' => 766,
                'company_id' => 60,
                'block_id' => 149,
                'school_academic_year_id' => 2,
            ),
            246 => 
            array (
                'id' => 767,
                'company_id' => 60,
                'block_id' => 150,
                'school_academic_year_id' => 2,
            ),
            247 => 
            array (
                'id' => 768,
                'company_id' => 60,
                'block_id' => 151,
                'school_academic_year_id' => 2,
            ),
            248 => 
            array (
                'id' => 769,
                'company_id' => 60,
                'block_id' => 152,
                'school_academic_year_id' => 2,
            ),
            249 => 
            array (
                'id' => 770,
                'company_id' => 60,
                'block_id' => 149,
                'school_academic_year_id' => 3,
            ),
            250 => 
            array (
                'id' => 771,
                'company_id' => 60,
                'block_id' => 150,
                'school_academic_year_id' => 3,
            ),
            251 => 
            array (
                'id' => 772,
                'company_id' => 60,
                'block_id' => 151,
                'school_academic_year_id' => 3,
            ),
            252 => 
            array (
                'id' => 773,
                'company_id' => 60,
                'block_id' => 152,
                'school_academic_year_id' => 3,
            ),
            253 => 
            array (
                'id' => 774,
                'company_id' => 60,
                'block_id' => 149,
                'school_academic_year_id' => 4,
            ),
            254 => 
            array (
                'id' => 775,
                'company_id' => 60,
                'block_id' => 150,
                'school_academic_year_id' => 4,
            ),
            255 => 
            array (
                'id' => 776,
                'company_id' => 60,
                'block_id' => 151,
                'school_academic_year_id' => 4,
            ),
            256 => 
            array (
                'id' => 777,
                'company_id' => 60,
                'block_id' => 152,
                'school_academic_year_id' => 4,
            ),
            257 => 
            array (
                'id' => 778,
                'company_id' => 60,
                'block_id' => 149,
                'school_academic_year_id' => 5,
            ),
            258 => 
            array (
                'id' => 779,
                'company_id' => 60,
                'block_id' => 150,
                'school_academic_year_id' => 5,
            ),
            259 => 
            array (
                'id' => 780,
                'company_id' => 60,
                'block_id' => 151,
                'school_academic_year_id' => 5,
            ),
            260 => 
            array (
                'id' => 781,
                'company_id' => 60,
                'block_id' => 152,
                'school_academic_year_id' => 5,
            ),
            261 => 
            array (
                'id' => 782,
                'company_id' => 60,
                'block_id' => 149,
                'school_academic_year_id' => 6,
            ),
            262 => 
            array (
                'id' => 783,
                'company_id' => 60,
                'block_id' => 150,
                'school_academic_year_id' => 6,
            ),
            263 => 
            array (
                'id' => 784,
                'company_id' => 60,
                'block_id' => 151,
                'school_academic_year_id' => 6,
            ),
            264 => 
            array (
                'id' => 785,
                'company_id' => 60,
                'block_id' => 152,
                'school_academic_year_id' => 6,
            ),
            265 => 
            array (
                'id' => 786,
                'company_id' => 61,
                'block_id' => 153,
                'school_academic_year_id' => 2,
            ),
            266 => 
            array (
                'id' => 787,
                'company_id' => 61,
                'block_id' => 154,
                'school_academic_year_id' => 2,
            ),
            267 => 
            array (
                'id' => 788,
                'company_id' => 61,
                'block_id' => 155,
                'school_academic_year_id' => 2,
            ),
            268 => 
            array (
                'id' => 789,
                'company_id' => 61,
                'block_id' => 156,
                'school_academic_year_id' => 2,
            ),
            269 => 
            array (
                'id' => 790,
                'company_id' => 61,
                'block_id' => 157,
                'school_academic_year_id' => 2,
            ),
            270 => 
            array (
                'id' => 791,
                'company_id' => 61,
                'block_id' => 158,
                'school_academic_year_id' => 2,
            ),
            271 => 
            array (
                'id' => 792,
                'company_id' => 61,
                'block_id' => 274,
                'school_academic_year_id' => 2,
            ),
            272 => 
            array (
                'id' => 793,
                'company_id' => 61,
                'block_id' => 279,
                'school_academic_year_id' => 2,
            ),
            273 => 
            array (
                'id' => 794,
                'company_id' => 61,
                'block_id' => 280,
                'school_academic_year_id' => 2,
            ),
            274 => 
            array (
                'id' => 795,
                'company_id' => 61,
                'block_id' => 281,
                'school_academic_year_id' => 2,
            ),
            275 => 
            array (
                'id' => 796,
                'company_id' => 61,
                'block_id' => 282,
                'school_academic_year_id' => 2,
            ),
            276 => 
            array (
                'id' => 797,
                'company_id' => 61,
                'block_id' => 283,
                'school_academic_year_id' => 2,
            ),
            277 => 
            array (
                'id' => 798,
                'company_id' => 61,
                'block_id' => 284,
                'school_academic_year_id' => 2,
            ),
            278 => 
            array (
                'id' => 799,
                'company_id' => 61,
                'block_id' => 286,
                'school_academic_year_id' => 2,
            ),
            279 => 
            array (
                'id' => 800,
                'company_id' => 61,
                'block_id' => 288,
                'school_academic_year_id' => 2,
            ),
            280 => 
            array (
                'id' => 801,
                'company_id' => 61,
                'block_id' => 289,
                'school_academic_year_id' => 2,
            ),
            281 => 
            array (
                'id' => 802,
                'company_id' => 61,
                'block_id' => 290,
                'school_academic_year_id' => 2,
            ),
            282 => 
            array (
                'id' => 803,
                'company_id' => 61,
                'block_id' => 291,
                'school_academic_year_id' => 2,
            ),
            283 => 
            array (
                'id' => 804,
                'company_id' => 61,
                'block_id' => 426,
                'school_academic_year_id' => 2,
            ),
            284 => 
            array (
                'id' => 805,
                'company_id' => 61,
                'block_id' => 427,
                'school_academic_year_id' => 2,
            ),
            285 => 
            array (
                'id' => 806,
                'company_id' => 61,
                'block_id' => 428,
                'school_academic_year_id' => 2,
            ),
            286 => 
            array (
                'id' => 807,
                'company_id' => 61,
                'block_id' => 429,
                'school_academic_year_id' => 2,
            ),
            287 => 
            array (
                'id' => 808,
                'company_id' => 61,
                'block_id' => 430,
                'school_academic_year_id' => 2,
            ),
            288 => 
            array (
                'id' => 809,
                'company_id' => 61,
                'block_id' => 431,
                'school_academic_year_id' => 2,
            ),
            289 => 
            array (
                'id' => 810,
                'company_id' => 61,
                'block_id' => 456,
                'school_academic_year_id' => 2,
            ),
            290 => 
            array (
                'id' => 811,
                'company_id' => 61,
                'block_id' => 153,
                'school_academic_year_id' => 3,
            ),
            291 => 
            array (
                'id' => 812,
                'company_id' => 61,
                'block_id' => 154,
                'school_academic_year_id' => 3,
            ),
            292 => 
            array (
                'id' => 813,
                'company_id' => 61,
                'block_id' => 155,
                'school_academic_year_id' => 3,
            ),
            293 => 
            array (
                'id' => 814,
                'company_id' => 61,
                'block_id' => 156,
                'school_academic_year_id' => 3,
            ),
            294 => 
            array (
                'id' => 815,
                'company_id' => 61,
                'block_id' => 157,
                'school_academic_year_id' => 3,
            ),
            295 => 
            array (
                'id' => 816,
                'company_id' => 61,
                'block_id' => 158,
                'school_academic_year_id' => 3,
            ),
            296 => 
            array (
                'id' => 817,
                'company_id' => 61,
                'block_id' => 274,
                'school_academic_year_id' => 3,
            ),
            297 => 
            array (
                'id' => 818,
                'company_id' => 61,
                'block_id' => 279,
                'school_academic_year_id' => 3,
            ),
            298 => 
            array (
                'id' => 819,
                'company_id' => 61,
                'block_id' => 280,
                'school_academic_year_id' => 3,
            ),
            299 => 
            array (
                'id' => 820,
                'company_id' => 61,
                'block_id' => 281,
                'school_academic_year_id' => 3,
            ),
            300 => 
            array (
                'id' => 821,
                'company_id' => 61,
                'block_id' => 282,
                'school_academic_year_id' => 3,
            ),
            301 => 
            array (
                'id' => 822,
                'company_id' => 61,
                'block_id' => 283,
                'school_academic_year_id' => 3,
            ),
            302 => 
            array (
                'id' => 823,
                'company_id' => 61,
                'block_id' => 284,
                'school_academic_year_id' => 3,
            ),
            303 => 
            array (
                'id' => 824,
                'company_id' => 61,
                'block_id' => 286,
                'school_academic_year_id' => 3,
            ),
            304 => 
            array (
                'id' => 825,
                'company_id' => 61,
                'block_id' => 288,
                'school_academic_year_id' => 3,
            ),
            305 => 
            array (
                'id' => 826,
                'company_id' => 61,
                'block_id' => 289,
                'school_academic_year_id' => 3,
            ),
            306 => 
            array (
                'id' => 827,
                'company_id' => 61,
                'block_id' => 290,
                'school_academic_year_id' => 3,
            ),
            307 => 
            array (
                'id' => 828,
                'company_id' => 61,
                'block_id' => 291,
                'school_academic_year_id' => 3,
            ),
            308 => 
            array (
                'id' => 829,
                'company_id' => 61,
                'block_id' => 426,
                'school_academic_year_id' => 3,
            ),
            309 => 
            array (
                'id' => 830,
                'company_id' => 61,
                'block_id' => 427,
                'school_academic_year_id' => 3,
            ),
            310 => 
            array (
                'id' => 831,
                'company_id' => 61,
                'block_id' => 428,
                'school_academic_year_id' => 3,
            ),
            311 => 
            array (
                'id' => 832,
                'company_id' => 61,
                'block_id' => 429,
                'school_academic_year_id' => 3,
            ),
            312 => 
            array (
                'id' => 833,
                'company_id' => 61,
                'block_id' => 430,
                'school_academic_year_id' => 3,
            ),
            313 => 
            array (
                'id' => 834,
                'company_id' => 61,
                'block_id' => 431,
                'school_academic_year_id' => 3,
            ),
            314 => 
            array (
                'id' => 835,
                'company_id' => 61,
                'block_id' => 456,
                'school_academic_year_id' => 3,
            ),
            315 => 
            array (
                'id' => 836,
                'company_id' => 61,
                'block_id' => 153,
                'school_academic_year_id' => 4,
            ),
            316 => 
            array (
                'id' => 837,
                'company_id' => 61,
                'block_id' => 154,
                'school_academic_year_id' => 4,
            ),
            317 => 
            array (
                'id' => 838,
                'company_id' => 61,
                'block_id' => 155,
                'school_academic_year_id' => 4,
            ),
            318 => 
            array (
                'id' => 839,
                'company_id' => 61,
                'block_id' => 156,
                'school_academic_year_id' => 4,
            ),
            319 => 
            array (
                'id' => 840,
                'company_id' => 61,
                'block_id' => 157,
                'school_academic_year_id' => 4,
            ),
            320 => 
            array (
                'id' => 841,
                'company_id' => 61,
                'block_id' => 158,
                'school_academic_year_id' => 4,
            ),
            321 => 
            array (
                'id' => 842,
                'company_id' => 61,
                'block_id' => 274,
                'school_academic_year_id' => 4,
            ),
            322 => 
            array (
                'id' => 843,
                'company_id' => 61,
                'block_id' => 279,
                'school_academic_year_id' => 4,
            ),
            323 => 
            array (
                'id' => 844,
                'company_id' => 61,
                'block_id' => 280,
                'school_academic_year_id' => 4,
            ),
            324 => 
            array (
                'id' => 845,
                'company_id' => 61,
                'block_id' => 281,
                'school_academic_year_id' => 4,
            ),
            325 => 
            array (
                'id' => 846,
                'company_id' => 61,
                'block_id' => 282,
                'school_academic_year_id' => 4,
            ),
            326 => 
            array (
                'id' => 847,
                'company_id' => 61,
                'block_id' => 283,
                'school_academic_year_id' => 4,
            ),
            327 => 
            array (
                'id' => 848,
                'company_id' => 61,
                'block_id' => 284,
                'school_academic_year_id' => 4,
            ),
            328 => 
            array (
                'id' => 849,
                'company_id' => 61,
                'block_id' => 286,
                'school_academic_year_id' => 4,
            ),
            329 => 
            array (
                'id' => 850,
                'company_id' => 61,
                'block_id' => 288,
                'school_academic_year_id' => 4,
            ),
            330 => 
            array (
                'id' => 851,
                'company_id' => 61,
                'block_id' => 289,
                'school_academic_year_id' => 4,
            ),
            331 => 
            array (
                'id' => 852,
                'company_id' => 61,
                'block_id' => 290,
                'school_academic_year_id' => 4,
            ),
            332 => 
            array (
                'id' => 853,
                'company_id' => 61,
                'block_id' => 291,
                'school_academic_year_id' => 4,
            ),
            333 => 
            array (
                'id' => 854,
                'company_id' => 61,
                'block_id' => 426,
                'school_academic_year_id' => 4,
            ),
            334 => 
            array (
                'id' => 855,
                'company_id' => 61,
                'block_id' => 427,
                'school_academic_year_id' => 4,
            ),
            335 => 
            array (
                'id' => 856,
                'company_id' => 61,
                'block_id' => 428,
                'school_academic_year_id' => 4,
            ),
            336 => 
            array (
                'id' => 857,
                'company_id' => 61,
                'block_id' => 429,
                'school_academic_year_id' => 4,
            ),
            337 => 
            array (
                'id' => 858,
                'company_id' => 61,
                'block_id' => 430,
                'school_academic_year_id' => 4,
            ),
            338 => 
            array (
                'id' => 859,
                'company_id' => 61,
                'block_id' => 431,
                'school_academic_year_id' => 4,
            ),
            339 => 
            array (
                'id' => 860,
                'company_id' => 61,
                'block_id' => 456,
                'school_academic_year_id' => 4,
            ),
            340 => 
            array (
                'id' => 861,
                'company_id' => 61,
                'block_id' => 153,
                'school_academic_year_id' => 5,
            ),
            341 => 
            array (
                'id' => 862,
                'company_id' => 61,
                'block_id' => 154,
                'school_academic_year_id' => 5,
            ),
            342 => 
            array (
                'id' => 863,
                'company_id' => 61,
                'block_id' => 155,
                'school_academic_year_id' => 5,
            ),
            343 => 
            array (
                'id' => 864,
                'company_id' => 61,
                'block_id' => 156,
                'school_academic_year_id' => 5,
            ),
            344 => 
            array (
                'id' => 865,
                'company_id' => 61,
                'block_id' => 157,
                'school_academic_year_id' => 5,
            ),
            345 => 
            array (
                'id' => 866,
                'company_id' => 61,
                'block_id' => 158,
                'school_academic_year_id' => 5,
            ),
            346 => 
            array (
                'id' => 867,
                'company_id' => 61,
                'block_id' => 274,
                'school_academic_year_id' => 5,
            ),
            347 => 
            array (
                'id' => 868,
                'company_id' => 61,
                'block_id' => 279,
                'school_academic_year_id' => 5,
            ),
            348 => 
            array (
                'id' => 869,
                'company_id' => 61,
                'block_id' => 280,
                'school_academic_year_id' => 5,
            ),
            349 => 
            array (
                'id' => 870,
                'company_id' => 61,
                'block_id' => 281,
                'school_academic_year_id' => 5,
            ),
            350 => 
            array (
                'id' => 871,
                'company_id' => 61,
                'block_id' => 282,
                'school_academic_year_id' => 5,
            ),
            351 => 
            array (
                'id' => 872,
                'company_id' => 61,
                'block_id' => 283,
                'school_academic_year_id' => 5,
            ),
            352 => 
            array (
                'id' => 873,
                'company_id' => 61,
                'block_id' => 284,
                'school_academic_year_id' => 5,
            ),
            353 => 
            array (
                'id' => 874,
                'company_id' => 61,
                'block_id' => 286,
                'school_academic_year_id' => 5,
            ),
            354 => 
            array (
                'id' => 875,
                'company_id' => 61,
                'block_id' => 288,
                'school_academic_year_id' => 5,
            ),
            355 => 
            array (
                'id' => 876,
                'company_id' => 61,
                'block_id' => 289,
                'school_academic_year_id' => 5,
            ),
            356 => 
            array (
                'id' => 877,
                'company_id' => 61,
                'block_id' => 290,
                'school_academic_year_id' => 5,
            ),
            357 => 
            array (
                'id' => 878,
                'company_id' => 61,
                'block_id' => 291,
                'school_academic_year_id' => 5,
            ),
            358 => 
            array (
                'id' => 879,
                'company_id' => 61,
                'block_id' => 426,
                'school_academic_year_id' => 5,
            ),
            359 => 
            array (
                'id' => 880,
                'company_id' => 61,
                'block_id' => 427,
                'school_academic_year_id' => 5,
            ),
            360 => 
            array (
                'id' => 881,
                'company_id' => 61,
                'block_id' => 428,
                'school_academic_year_id' => 5,
            ),
            361 => 
            array (
                'id' => 882,
                'company_id' => 61,
                'block_id' => 429,
                'school_academic_year_id' => 5,
            ),
            362 => 
            array (
                'id' => 883,
                'company_id' => 61,
                'block_id' => 430,
                'school_academic_year_id' => 5,
            ),
            363 => 
            array (
                'id' => 884,
                'company_id' => 61,
                'block_id' => 431,
                'school_academic_year_id' => 5,
            ),
            364 => 
            array (
                'id' => 885,
                'company_id' => 61,
                'block_id' => 456,
                'school_academic_year_id' => 5,
            ),
            365 => 
            array (
                'id' => 886,
                'company_id' => 61,
                'block_id' => 153,
                'school_academic_year_id' => 6,
            ),
            366 => 
            array (
                'id' => 887,
                'company_id' => 61,
                'block_id' => 154,
                'school_academic_year_id' => 6,
            ),
            367 => 
            array (
                'id' => 888,
                'company_id' => 61,
                'block_id' => 155,
                'school_academic_year_id' => 6,
            ),
            368 => 
            array (
                'id' => 889,
                'company_id' => 61,
                'block_id' => 156,
                'school_academic_year_id' => 6,
            ),
            369 => 
            array (
                'id' => 890,
                'company_id' => 61,
                'block_id' => 157,
                'school_academic_year_id' => 6,
            ),
            370 => 
            array (
                'id' => 891,
                'company_id' => 61,
                'block_id' => 158,
                'school_academic_year_id' => 6,
            ),
            371 => 
            array (
                'id' => 892,
                'company_id' => 61,
                'block_id' => 274,
                'school_academic_year_id' => 6,
            ),
            372 => 
            array (
                'id' => 893,
                'company_id' => 61,
                'block_id' => 279,
                'school_academic_year_id' => 6,
            ),
            373 => 
            array (
                'id' => 894,
                'company_id' => 61,
                'block_id' => 280,
                'school_academic_year_id' => 6,
            ),
            374 => 
            array (
                'id' => 895,
                'company_id' => 61,
                'block_id' => 281,
                'school_academic_year_id' => 6,
            ),
            375 => 
            array (
                'id' => 896,
                'company_id' => 61,
                'block_id' => 282,
                'school_academic_year_id' => 6,
            ),
            376 => 
            array (
                'id' => 897,
                'company_id' => 61,
                'block_id' => 283,
                'school_academic_year_id' => 6,
            ),
            377 => 
            array (
                'id' => 898,
                'company_id' => 61,
                'block_id' => 284,
                'school_academic_year_id' => 6,
            ),
            378 => 
            array (
                'id' => 899,
                'company_id' => 61,
                'block_id' => 286,
                'school_academic_year_id' => 6,
            ),
            379 => 
            array (
                'id' => 900,
                'company_id' => 61,
                'block_id' => 288,
                'school_academic_year_id' => 6,
            ),
            380 => 
            array (
                'id' => 901,
                'company_id' => 61,
                'block_id' => 289,
                'school_academic_year_id' => 6,
            ),
            381 => 
            array (
                'id' => 902,
                'company_id' => 61,
                'block_id' => 290,
                'school_academic_year_id' => 6,
            ),
            382 => 
            array (
                'id' => 903,
                'company_id' => 61,
                'block_id' => 291,
                'school_academic_year_id' => 6,
            ),
            383 => 
            array (
                'id' => 904,
                'company_id' => 61,
                'block_id' => 426,
                'school_academic_year_id' => 6,
            ),
            384 => 
            array (
                'id' => 905,
                'company_id' => 61,
                'block_id' => 427,
                'school_academic_year_id' => 6,
            ),
            385 => 
            array (
                'id' => 906,
                'company_id' => 61,
                'block_id' => 428,
                'school_academic_year_id' => 6,
            ),
            386 => 
            array (
                'id' => 907,
                'company_id' => 61,
                'block_id' => 429,
                'school_academic_year_id' => 6,
            ),
            387 => 
            array (
                'id' => 908,
                'company_id' => 61,
                'block_id' => 430,
                'school_academic_year_id' => 6,
            ),
            388 => 
            array (
                'id' => 909,
                'company_id' => 61,
                'block_id' => 431,
                'school_academic_year_id' => 6,
            ),
            389 => 
            array (
                'id' => 910,
                'company_id' => 61,
                'block_id' => 456,
                'school_academic_year_id' => 6,
            ),
            390 => 
            array (
                'id' => 911,
                'company_id' => 62,
                'block_id' => 159,
                'school_academic_year_id' => 2,
            ),
            391 => 
            array (
                'id' => 912,
                'company_id' => 62,
                'block_id' => 167,
                'school_academic_year_id' => 2,
            ),
            392 => 
            array (
                'id' => 913,
                'company_id' => 62,
                'block_id' => 159,
                'school_academic_year_id' => 3,
            ),
            393 => 
            array (
                'id' => 914,
                'company_id' => 62,
                'block_id' => 167,
                'school_academic_year_id' => 3,
            ),
            394 => 
            array (
                'id' => 915,
                'company_id' => 62,
                'block_id' => 159,
                'school_academic_year_id' => 4,
            ),
            395 => 
            array (
                'id' => 916,
                'company_id' => 62,
                'block_id' => 167,
                'school_academic_year_id' => 4,
            ),
            396 => 
            array (
                'id' => 917,
                'company_id' => 62,
                'block_id' => 159,
                'school_academic_year_id' => 5,
            ),
            397 => 
            array (
                'id' => 918,
                'company_id' => 62,
                'block_id' => 167,
                'school_academic_year_id' => 5,
            ),
            398 => 
            array (
                'id' => 919,
                'company_id' => 62,
                'block_id' => 159,
                'school_academic_year_id' => 6,
            ),
            399 => 
            array (
                'id' => 920,
                'company_id' => 62,
                'block_id' => 167,
                'school_academic_year_id' => 6,
            ),
            400 => 
            array (
                'id' => 921,
                'company_id' => 63,
                'block_id' => 160,
                'school_academic_year_id' => 2,
            ),
            401 => 
            array (
                'id' => 922,
                'company_id' => 63,
                'block_id' => 161,
                'school_academic_year_id' => 2,
            ),
            402 => 
            array (
                'id' => 923,
                'company_id' => 63,
                'block_id' => 162,
                'school_academic_year_id' => 2,
            ),
            403 => 
            array (
                'id' => 924,
                'company_id' => 63,
                'block_id' => 160,
                'school_academic_year_id' => 3,
            ),
            404 => 
            array (
                'id' => 925,
                'company_id' => 63,
                'block_id' => 161,
                'school_academic_year_id' => 3,
            ),
            405 => 
            array (
                'id' => 926,
                'company_id' => 63,
                'block_id' => 162,
                'school_academic_year_id' => 3,
            ),
            406 => 
            array (
                'id' => 927,
                'company_id' => 63,
                'block_id' => 160,
                'school_academic_year_id' => 4,
            ),
            407 => 
            array (
                'id' => 928,
                'company_id' => 63,
                'block_id' => 161,
                'school_academic_year_id' => 4,
            ),
            408 => 
            array (
                'id' => 929,
                'company_id' => 63,
                'block_id' => 162,
                'school_academic_year_id' => 4,
            ),
            409 => 
            array (
                'id' => 930,
                'company_id' => 63,
                'block_id' => 160,
                'school_academic_year_id' => 5,
            ),
            410 => 
            array (
                'id' => 931,
                'company_id' => 63,
                'block_id' => 161,
                'school_academic_year_id' => 5,
            ),
            411 => 
            array (
                'id' => 932,
                'company_id' => 63,
                'block_id' => 162,
                'school_academic_year_id' => 5,
            ),
            412 => 
            array (
                'id' => 933,
                'company_id' => 63,
                'block_id' => 160,
                'school_academic_year_id' => 6,
            ),
            413 => 
            array (
                'id' => 934,
                'company_id' => 63,
                'block_id' => 161,
                'school_academic_year_id' => 6,
            ),
            414 => 
            array (
                'id' => 935,
                'company_id' => 63,
                'block_id' => 162,
                'school_academic_year_id' => 6,
            ),
            415 => 
            array (
                'id' => 936,
                'company_id' => 64,
                'block_id' => 165,
                'school_academic_year_id' => 2,
            ),
            416 => 
            array (
                'id' => 937,
                'company_id' => 64,
                'block_id' => 165,
                'school_academic_year_id' => 3,
            ),
            417 => 
            array (
                'id' => 938,
                'company_id' => 64,
                'block_id' => 165,
                'school_academic_year_id' => 4,
            ),
            418 => 
            array (
                'id' => 939,
                'company_id' => 64,
                'block_id' => 165,
                'school_academic_year_id' => 5,
            ),
            419 => 
            array (
                'id' => 940,
                'company_id' => 64,
                'block_id' => 165,
                'school_academic_year_id' => 6,
            ),
            420 => 
            array (
                'id' => 941,
                'company_id' => 65,
                'block_id' => 166,
                'school_academic_year_id' => 2,
            ),
            421 => 
            array (
                'id' => 942,
                'company_id' => 65,
                'block_id' => 166,
                'school_academic_year_id' => 3,
            ),
            422 => 
            array (
                'id' => 943,
                'company_id' => 65,
                'block_id' => 166,
                'school_academic_year_id' => 4,
            ),
            423 => 
            array (
                'id' => 944,
                'company_id' => 65,
                'block_id' => 166,
                'school_academic_year_id' => 5,
            ),
            424 => 
            array (
                'id' => 945,
                'company_id' => 65,
                'block_id' => 166,
                'school_academic_year_id' => 6,
            ),
            425 => 
            array (
                'id' => 946,
                'company_id' => 66,
                'block_id' => 168,
                'school_academic_year_id' => 2,
            ),
            426 => 
            array (
                'id' => 947,
                'company_id' => 66,
                'block_id' => 172,
                'school_academic_year_id' => 2,
            ),
            427 => 
            array (
                'id' => 948,
                'company_id' => 66,
                'block_id' => 174,
                'school_academic_year_id' => 2,
            ),
            428 => 
            array (
                'id' => 949,
                'company_id' => 66,
                'block_id' => 175,
                'school_academic_year_id' => 2,
            ),
            429 => 
            array (
                'id' => 950,
                'company_id' => 66,
                'block_id' => 168,
                'school_academic_year_id' => 3,
            ),
            430 => 
            array (
                'id' => 951,
                'company_id' => 66,
                'block_id' => 172,
                'school_academic_year_id' => 3,
            ),
            431 => 
            array (
                'id' => 952,
                'company_id' => 66,
                'block_id' => 174,
                'school_academic_year_id' => 3,
            ),
            432 => 
            array (
                'id' => 953,
                'company_id' => 66,
                'block_id' => 175,
                'school_academic_year_id' => 3,
            ),
            433 => 
            array (
                'id' => 954,
                'company_id' => 66,
                'block_id' => 168,
                'school_academic_year_id' => 4,
            ),
            434 => 
            array (
                'id' => 955,
                'company_id' => 66,
                'block_id' => 172,
                'school_academic_year_id' => 4,
            ),
            435 => 
            array (
                'id' => 956,
                'company_id' => 66,
                'block_id' => 174,
                'school_academic_year_id' => 4,
            ),
            436 => 
            array (
                'id' => 957,
                'company_id' => 66,
                'block_id' => 175,
                'school_academic_year_id' => 4,
            ),
            437 => 
            array (
                'id' => 958,
                'company_id' => 66,
                'block_id' => 168,
                'school_academic_year_id' => 5,
            ),
            438 => 
            array (
                'id' => 959,
                'company_id' => 66,
                'block_id' => 172,
                'school_academic_year_id' => 5,
            ),
            439 => 
            array (
                'id' => 960,
                'company_id' => 66,
                'block_id' => 174,
                'school_academic_year_id' => 5,
            ),
            440 => 
            array (
                'id' => 961,
                'company_id' => 66,
                'block_id' => 175,
                'school_academic_year_id' => 5,
            ),
            441 => 
            array (
                'id' => 962,
                'company_id' => 66,
                'block_id' => 168,
                'school_academic_year_id' => 6,
            ),
            442 => 
            array (
                'id' => 963,
                'company_id' => 66,
                'block_id' => 172,
                'school_academic_year_id' => 6,
            ),
            443 => 
            array (
                'id' => 964,
                'company_id' => 66,
                'block_id' => 174,
                'school_academic_year_id' => 6,
            ),
            444 => 
            array (
                'id' => 965,
                'company_id' => 66,
                'block_id' => 175,
                'school_academic_year_id' => 6,
            ),
            445 => 
            array (
                'id' => 966,
                'company_id' => 67,
                'block_id' => 179,
                'school_academic_year_id' => 2,
            ),
            446 => 
            array (
                'id' => 967,
                'company_id' => 67,
                'block_id' => 179,
                'school_academic_year_id' => 3,
            ),
            447 => 
            array (
                'id' => 968,
                'company_id' => 67,
                'block_id' => 179,
                'school_academic_year_id' => 4,
            ),
            448 => 
            array (
                'id' => 969,
                'company_id' => 67,
                'block_id' => 179,
                'school_academic_year_id' => 5,
            ),
            449 => 
            array (
                'id' => 970,
                'company_id' => 67,
                'block_id' => 179,
                'school_academic_year_id' => 6,
            ),
            450 => 
            array (
                'id' => 971,
                'company_id' => 68,
                'block_id' => 183,
                'school_academic_year_id' => 2,
            ),
            451 => 
            array (
                'id' => 972,
                'company_id' => 68,
                'block_id' => 184,
                'school_academic_year_id' => 2,
            ),
            452 => 
            array (
                'id' => 973,
                'company_id' => 68,
                'block_id' => 185,
                'school_academic_year_id' => 2,
            ),
            453 => 
            array (
                'id' => 974,
                'company_id' => 68,
                'block_id' => 186,
                'school_academic_year_id' => 2,
            ),
            454 => 
            array (
                'id' => 975,
                'company_id' => 68,
                'block_id' => 187,
                'school_academic_year_id' => 2,
            ),
            455 => 
            array (
                'id' => 976,
                'company_id' => 68,
                'block_id' => 192,
                'school_academic_year_id' => 2,
            ),
            456 => 
            array (
                'id' => 977,
                'company_id' => 68,
                'block_id' => 193,
                'school_academic_year_id' => 2,
            ),
            457 => 
            array (
                'id' => 978,
                'company_id' => 68,
                'block_id' => 194,
                'school_academic_year_id' => 2,
            ),
            458 => 
            array (
                'id' => 979,
                'company_id' => 68,
                'block_id' => 195,
                'school_academic_year_id' => 2,
            ),
            459 => 
            array (
                'id' => 980,
                'company_id' => 68,
                'block_id' => 196,
                'school_academic_year_id' => 2,
            ),
            460 => 
            array (
                'id' => 981,
                'company_id' => 68,
                'block_id' => 199,
                'school_academic_year_id' => 2,
            ),
            461 => 
            array (
                'id' => 982,
                'company_id' => 68,
                'block_id' => 200,
                'school_academic_year_id' => 2,
            ),
            462 => 
            array (
                'id' => 983,
                'company_id' => 68,
                'block_id' => 201,
                'school_academic_year_id' => 2,
            ),
            463 => 
            array (
                'id' => 984,
                'company_id' => 68,
                'block_id' => 202,
                'school_academic_year_id' => 2,
            ),
            464 => 
            array (
                'id' => 985,
                'company_id' => 68,
                'block_id' => 203,
                'school_academic_year_id' => 2,
            ),
            465 => 
            array (
                'id' => 986,
                'company_id' => 68,
                'block_id' => 204,
                'school_academic_year_id' => 2,
            ),
            466 => 
            array (
                'id' => 987,
                'company_id' => 68,
                'block_id' => 205,
                'school_academic_year_id' => 2,
            ),
            467 => 
            array (
                'id' => 988,
                'company_id' => 68,
                'block_id' => 206,
                'school_academic_year_id' => 2,
            ),
            468 => 
            array (
                'id' => 989,
                'company_id' => 68,
                'block_id' => 207,
                'school_academic_year_id' => 2,
            ),
            469 => 
            array (
                'id' => 990,
                'company_id' => 68,
                'block_id' => 208,
                'school_academic_year_id' => 2,
            ),
            470 => 
            array (
                'id' => 991,
                'company_id' => 68,
                'block_id' => 183,
                'school_academic_year_id' => 3,
            ),
            471 => 
            array (
                'id' => 992,
                'company_id' => 68,
                'block_id' => 184,
                'school_academic_year_id' => 3,
            ),
            472 => 
            array (
                'id' => 993,
                'company_id' => 68,
                'block_id' => 185,
                'school_academic_year_id' => 3,
            ),
            473 => 
            array (
                'id' => 994,
                'company_id' => 68,
                'block_id' => 186,
                'school_academic_year_id' => 3,
            ),
            474 => 
            array (
                'id' => 995,
                'company_id' => 68,
                'block_id' => 187,
                'school_academic_year_id' => 3,
            ),
            475 => 
            array (
                'id' => 996,
                'company_id' => 68,
                'block_id' => 192,
                'school_academic_year_id' => 3,
            ),
            476 => 
            array (
                'id' => 997,
                'company_id' => 68,
                'block_id' => 193,
                'school_academic_year_id' => 3,
            ),
            477 => 
            array (
                'id' => 998,
                'company_id' => 68,
                'block_id' => 194,
                'school_academic_year_id' => 3,
            ),
            478 => 
            array (
                'id' => 999,
                'company_id' => 68,
                'block_id' => 195,
                'school_academic_year_id' => 3,
            ),
            479 => 
            array (
                'id' => 1000,
                'company_id' => 68,
                'block_id' => 196,
                'school_academic_year_id' => 3,
            ),
            480 => 
            array (
                'id' => 1001,
                'company_id' => 68,
                'block_id' => 199,
                'school_academic_year_id' => 3,
            ),
            481 => 
            array (
                'id' => 1002,
                'company_id' => 68,
                'block_id' => 200,
                'school_academic_year_id' => 3,
            ),
            482 => 
            array (
                'id' => 1003,
                'company_id' => 68,
                'block_id' => 201,
                'school_academic_year_id' => 3,
            ),
            483 => 
            array (
                'id' => 1004,
                'company_id' => 68,
                'block_id' => 202,
                'school_academic_year_id' => 3,
            ),
            484 => 
            array (
                'id' => 1005,
                'company_id' => 68,
                'block_id' => 203,
                'school_academic_year_id' => 3,
            ),
            485 => 
            array (
                'id' => 1006,
                'company_id' => 68,
                'block_id' => 204,
                'school_academic_year_id' => 3,
            ),
            486 => 
            array (
                'id' => 1007,
                'company_id' => 68,
                'block_id' => 205,
                'school_academic_year_id' => 3,
            ),
            487 => 
            array (
                'id' => 1008,
                'company_id' => 68,
                'block_id' => 206,
                'school_academic_year_id' => 3,
            ),
            488 => 
            array (
                'id' => 1009,
                'company_id' => 68,
                'block_id' => 207,
                'school_academic_year_id' => 3,
            ),
            489 => 
            array (
                'id' => 1010,
                'company_id' => 68,
                'block_id' => 208,
                'school_academic_year_id' => 3,
            ),
            490 => 
            array (
                'id' => 1011,
                'company_id' => 68,
                'block_id' => 183,
                'school_academic_year_id' => 4,
            ),
            491 => 
            array (
                'id' => 1012,
                'company_id' => 68,
                'block_id' => 184,
                'school_academic_year_id' => 4,
            ),
            492 => 
            array (
                'id' => 1013,
                'company_id' => 68,
                'block_id' => 185,
                'school_academic_year_id' => 4,
            ),
            493 => 
            array (
                'id' => 1014,
                'company_id' => 68,
                'block_id' => 186,
                'school_academic_year_id' => 4,
            ),
            494 => 
            array (
                'id' => 1015,
                'company_id' => 68,
                'block_id' => 187,
                'school_academic_year_id' => 4,
            ),
            495 => 
            array (
                'id' => 1016,
                'company_id' => 68,
                'block_id' => 192,
                'school_academic_year_id' => 4,
            ),
            496 => 
            array (
                'id' => 1017,
                'company_id' => 68,
                'block_id' => 193,
                'school_academic_year_id' => 4,
            ),
            497 => 
            array (
                'id' => 1018,
                'company_id' => 68,
                'block_id' => 194,
                'school_academic_year_id' => 4,
            ),
            498 => 
            array (
                'id' => 1019,
                'company_id' => 68,
                'block_id' => 195,
                'school_academic_year_id' => 4,
            ),
            499 => 
            array (
                'id' => 1020,
                'company_id' => 68,
                'block_id' => 196,
                'school_academic_year_id' => 4,
            ),
        ));
        \DB::table('block_years')->insert(array (
            0 => 
            array (
                'id' => 1021,
                'company_id' => 68,
                'block_id' => 199,
                'school_academic_year_id' => 4,
            ),
            1 => 
            array (
                'id' => 1022,
                'company_id' => 68,
                'block_id' => 200,
                'school_academic_year_id' => 4,
            ),
            2 => 
            array (
                'id' => 1023,
                'company_id' => 68,
                'block_id' => 201,
                'school_academic_year_id' => 4,
            ),
            3 => 
            array (
                'id' => 1024,
                'company_id' => 68,
                'block_id' => 202,
                'school_academic_year_id' => 4,
            ),
            4 => 
            array (
                'id' => 1025,
                'company_id' => 68,
                'block_id' => 203,
                'school_academic_year_id' => 4,
            ),
            5 => 
            array (
                'id' => 1026,
                'company_id' => 68,
                'block_id' => 204,
                'school_academic_year_id' => 4,
            ),
            6 => 
            array (
                'id' => 1027,
                'company_id' => 68,
                'block_id' => 205,
                'school_academic_year_id' => 4,
            ),
            7 => 
            array (
                'id' => 1028,
                'company_id' => 68,
                'block_id' => 206,
                'school_academic_year_id' => 4,
            ),
            8 => 
            array (
                'id' => 1029,
                'company_id' => 68,
                'block_id' => 207,
                'school_academic_year_id' => 4,
            ),
            9 => 
            array (
                'id' => 1030,
                'company_id' => 68,
                'block_id' => 208,
                'school_academic_year_id' => 4,
            ),
            10 => 
            array (
                'id' => 1031,
                'company_id' => 68,
                'block_id' => 183,
                'school_academic_year_id' => 5,
            ),
            11 => 
            array (
                'id' => 1032,
                'company_id' => 68,
                'block_id' => 184,
                'school_academic_year_id' => 5,
            ),
            12 => 
            array (
                'id' => 1033,
                'company_id' => 68,
                'block_id' => 185,
                'school_academic_year_id' => 5,
            ),
            13 => 
            array (
                'id' => 1034,
                'company_id' => 68,
                'block_id' => 186,
                'school_academic_year_id' => 5,
            ),
            14 => 
            array (
                'id' => 1035,
                'company_id' => 68,
                'block_id' => 187,
                'school_academic_year_id' => 5,
            ),
            15 => 
            array (
                'id' => 1036,
                'company_id' => 68,
                'block_id' => 192,
                'school_academic_year_id' => 5,
            ),
            16 => 
            array (
                'id' => 1037,
                'company_id' => 68,
                'block_id' => 193,
                'school_academic_year_id' => 5,
            ),
            17 => 
            array (
                'id' => 1038,
                'company_id' => 68,
                'block_id' => 194,
                'school_academic_year_id' => 5,
            ),
            18 => 
            array (
                'id' => 1039,
                'company_id' => 68,
                'block_id' => 195,
                'school_academic_year_id' => 5,
            ),
            19 => 
            array (
                'id' => 1040,
                'company_id' => 68,
                'block_id' => 196,
                'school_academic_year_id' => 5,
            ),
            20 => 
            array (
                'id' => 1041,
                'company_id' => 68,
                'block_id' => 199,
                'school_academic_year_id' => 5,
            ),
            21 => 
            array (
                'id' => 1042,
                'company_id' => 68,
                'block_id' => 200,
                'school_academic_year_id' => 5,
            ),
            22 => 
            array (
                'id' => 1043,
                'company_id' => 68,
                'block_id' => 201,
                'school_academic_year_id' => 5,
            ),
            23 => 
            array (
                'id' => 1044,
                'company_id' => 68,
                'block_id' => 202,
                'school_academic_year_id' => 5,
            ),
            24 => 
            array (
                'id' => 1045,
                'company_id' => 68,
                'block_id' => 203,
                'school_academic_year_id' => 5,
            ),
            25 => 
            array (
                'id' => 1046,
                'company_id' => 68,
                'block_id' => 204,
                'school_academic_year_id' => 5,
            ),
            26 => 
            array (
                'id' => 1047,
                'company_id' => 68,
                'block_id' => 205,
                'school_academic_year_id' => 5,
            ),
            27 => 
            array (
                'id' => 1048,
                'company_id' => 68,
                'block_id' => 206,
                'school_academic_year_id' => 5,
            ),
            28 => 
            array (
                'id' => 1049,
                'company_id' => 68,
                'block_id' => 207,
                'school_academic_year_id' => 5,
            ),
            29 => 
            array (
                'id' => 1050,
                'company_id' => 68,
                'block_id' => 208,
                'school_academic_year_id' => 5,
            ),
            30 => 
            array (
                'id' => 1051,
                'company_id' => 68,
                'block_id' => 183,
                'school_academic_year_id' => 6,
            ),
            31 => 
            array (
                'id' => 1052,
                'company_id' => 68,
                'block_id' => 184,
                'school_academic_year_id' => 6,
            ),
            32 => 
            array (
                'id' => 1053,
                'company_id' => 68,
                'block_id' => 185,
                'school_academic_year_id' => 6,
            ),
            33 => 
            array (
                'id' => 1054,
                'company_id' => 68,
                'block_id' => 186,
                'school_academic_year_id' => 6,
            ),
            34 => 
            array (
                'id' => 1055,
                'company_id' => 68,
                'block_id' => 187,
                'school_academic_year_id' => 6,
            ),
            35 => 
            array (
                'id' => 1056,
                'company_id' => 68,
                'block_id' => 192,
                'school_academic_year_id' => 6,
            ),
            36 => 
            array (
                'id' => 1057,
                'company_id' => 68,
                'block_id' => 193,
                'school_academic_year_id' => 6,
            ),
            37 => 
            array (
                'id' => 1058,
                'company_id' => 68,
                'block_id' => 194,
                'school_academic_year_id' => 6,
            ),
            38 => 
            array (
                'id' => 1059,
                'company_id' => 68,
                'block_id' => 195,
                'school_academic_year_id' => 6,
            ),
            39 => 
            array (
                'id' => 1060,
                'company_id' => 68,
                'block_id' => 196,
                'school_academic_year_id' => 6,
            ),
            40 => 
            array (
                'id' => 1061,
                'company_id' => 68,
                'block_id' => 199,
                'school_academic_year_id' => 6,
            ),
            41 => 
            array (
                'id' => 1062,
                'company_id' => 68,
                'block_id' => 200,
                'school_academic_year_id' => 6,
            ),
            42 => 
            array (
                'id' => 1063,
                'company_id' => 68,
                'block_id' => 201,
                'school_academic_year_id' => 6,
            ),
            43 => 
            array (
                'id' => 1064,
                'company_id' => 68,
                'block_id' => 202,
                'school_academic_year_id' => 6,
            ),
            44 => 
            array (
                'id' => 1065,
                'company_id' => 68,
                'block_id' => 203,
                'school_academic_year_id' => 6,
            ),
            45 => 
            array (
                'id' => 1066,
                'company_id' => 68,
                'block_id' => 204,
                'school_academic_year_id' => 6,
            ),
            46 => 
            array (
                'id' => 1067,
                'company_id' => 68,
                'block_id' => 205,
                'school_academic_year_id' => 6,
            ),
            47 => 
            array (
                'id' => 1068,
                'company_id' => 68,
                'block_id' => 206,
                'school_academic_year_id' => 6,
            ),
            48 => 
            array (
                'id' => 1069,
                'company_id' => 68,
                'block_id' => 207,
                'school_academic_year_id' => 6,
            ),
            49 => 
            array (
                'id' => 1070,
                'company_id' => 68,
                'block_id' => 208,
                'school_academic_year_id' => 6,
            ),
            50 => 
            array (
                'id' => 1071,
                'company_id' => 69,
                'block_id' => 212,
                'school_academic_year_id' => 2,
            ),
            51 => 
            array (
                'id' => 1072,
                'company_id' => 69,
                'block_id' => 213,
                'school_academic_year_id' => 2,
            ),
            52 => 
            array (
                'id' => 1073,
                'company_id' => 69,
                'block_id' => 214,
                'school_academic_year_id' => 2,
            ),
            53 => 
            array (
                'id' => 1074,
                'company_id' => 69,
                'block_id' => 212,
                'school_academic_year_id' => 3,
            ),
            54 => 
            array (
                'id' => 1075,
                'company_id' => 69,
                'block_id' => 213,
                'school_academic_year_id' => 3,
            ),
            55 => 
            array (
                'id' => 1076,
                'company_id' => 69,
                'block_id' => 214,
                'school_academic_year_id' => 3,
            ),
            56 => 
            array (
                'id' => 1077,
                'company_id' => 69,
                'block_id' => 212,
                'school_academic_year_id' => 4,
            ),
            57 => 
            array (
                'id' => 1078,
                'company_id' => 69,
                'block_id' => 213,
                'school_academic_year_id' => 4,
            ),
            58 => 
            array (
                'id' => 1079,
                'company_id' => 69,
                'block_id' => 214,
                'school_academic_year_id' => 4,
            ),
            59 => 
            array (
                'id' => 1080,
                'company_id' => 69,
                'block_id' => 212,
                'school_academic_year_id' => 5,
            ),
            60 => 
            array (
                'id' => 1081,
                'company_id' => 69,
                'block_id' => 213,
                'school_academic_year_id' => 5,
            ),
            61 => 
            array (
                'id' => 1082,
                'company_id' => 69,
                'block_id' => 214,
                'school_academic_year_id' => 5,
            ),
            62 => 
            array (
                'id' => 1083,
                'company_id' => 69,
                'block_id' => 212,
                'school_academic_year_id' => 6,
            ),
            63 => 
            array (
                'id' => 1084,
                'company_id' => 69,
                'block_id' => 213,
                'school_academic_year_id' => 6,
            ),
            64 => 
            array (
                'id' => 1085,
                'company_id' => 69,
                'block_id' => 214,
                'school_academic_year_id' => 6,
            ),
            65 => 
            array (
                'id' => 1086,
                'company_id' => 70,
                'block_id' => 215,
                'school_academic_year_id' => 2,
            ),
            66 => 
            array (
                'id' => 1087,
                'company_id' => 70,
                'block_id' => 216,
                'school_academic_year_id' => 2,
            ),
            67 => 
            array (
                'id' => 1088,
                'company_id' => 70,
                'block_id' => 217,
                'school_academic_year_id' => 2,
            ),
            68 => 
            array (
                'id' => 1089,
                'company_id' => 70,
                'block_id' => 215,
                'school_academic_year_id' => 3,
            ),
            69 => 
            array (
                'id' => 1090,
                'company_id' => 70,
                'block_id' => 216,
                'school_academic_year_id' => 3,
            ),
            70 => 
            array (
                'id' => 1091,
                'company_id' => 70,
                'block_id' => 217,
                'school_academic_year_id' => 3,
            ),
            71 => 
            array (
                'id' => 1092,
                'company_id' => 70,
                'block_id' => 215,
                'school_academic_year_id' => 4,
            ),
            72 => 
            array (
                'id' => 1093,
                'company_id' => 70,
                'block_id' => 216,
                'school_academic_year_id' => 4,
            ),
            73 => 
            array (
                'id' => 1094,
                'company_id' => 70,
                'block_id' => 217,
                'school_academic_year_id' => 4,
            ),
            74 => 
            array (
                'id' => 1095,
                'company_id' => 70,
                'block_id' => 215,
                'school_academic_year_id' => 5,
            ),
            75 => 
            array (
                'id' => 1096,
                'company_id' => 70,
                'block_id' => 216,
                'school_academic_year_id' => 5,
            ),
            76 => 
            array (
                'id' => 1097,
                'company_id' => 70,
                'block_id' => 217,
                'school_academic_year_id' => 5,
            ),
            77 => 
            array (
                'id' => 1098,
                'company_id' => 70,
                'block_id' => 215,
                'school_academic_year_id' => 6,
            ),
            78 => 
            array (
                'id' => 1099,
                'company_id' => 70,
                'block_id' => 216,
                'school_academic_year_id' => 6,
            ),
            79 => 
            array (
                'id' => 1100,
                'company_id' => 70,
                'block_id' => 217,
                'school_academic_year_id' => 6,
            ),
            80 => 
            array (
                'id' => 1101,
                'company_id' => 72,
                'block_id' => 220,
                'school_academic_year_id' => 2,
            ),
            81 => 
            array (
                'id' => 1102,
                'company_id' => 72,
                'block_id' => 278,
                'school_academic_year_id' => 2,
            ),
            82 => 
            array (
                'id' => 1103,
                'company_id' => 72,
                'block_id' => 347,
                'school_academic_year_id' => 2,
            ),
            83 => 
            array (
                'id' => 1104,
                'company_id' => 72,
                'block_id' => 220,
                'school_academic_year_id' => 3,
            ),
            84 => 
            array (
                'id' => 1105,
                'company_id' => 72,
                'block_id' => 278,
                'school_academic_year_id' => 3,
            ),
            85 => 
            array (
                'id' => 1106,
                'company_id' => 72,
                'block_id' => 347,
                'school_academic_year_id' => 3,
            ),
            86 => 
            array (
                'id' => 1107,
                'company_id' => 72,
                'block_id' => 220,
                'school_academic_year_id' => 4,
            ),
            87 => 
            array (
                'id' => 1108,
                'company_id' => 72,
                'block_id' => 278,
                'school_academic_year_id' => 4,
            ),
            88 => 
            array (
                'id' => 1109,
                'company_id' => 72,
                'block_id' => 347,
                'school_academic_year_id' => 4,
            ),
            89 => 
            array (
                'id' => 1110,
                'company_id' => 72,
                'block_id' => 220,
                'school_academic_year_id' => 5,
            ),
            90 => 
            array (
                'id' => 1111,
                'company_id' => 72,
                'block_id' => 278,
                'school_academic_year_id' => 5,
            ),
            91 => 
            array (
                'id' => 1112,
                'company_id' => 72,
                'block_id' => 347,
                'school_academic_year_id' => 5,
            ),
            92 => 
            array (
                'id' => 1113,
                'company_id' => 72,
                'block_id' => 220,
                'school_academic_year_id' => 6,
            ),
            93 => 
            array (
                'id' => 1114,
                'company_id' => 72,
                'block_id' => 278,
                'school_academic_year_id' => 6,
            ),
            94 => 
            array (
                'id' => 1115,
                'company_id' => 72,
                'block_id' => 347,
                'school_academic_year_id' => 6,
            ),
            95 => 
            array (
                'id' => 1116,
                'company_id' => 73,
                'block_id' => 222,
                'school_academic_year_id' => 2,
            ),
            96 => 
            array (
                'id' => 1117,
                'company_id' => 73,
                'block_id' => 223,
                'school_academic_year_id' => 2,
            ),
            97 => 
            array (
                'id' => 1118,
                'company_id' => 73,
                'block_id' => 233,
                'school_academic_year_id' => 2,
            ),
            98 => 
            array (
                'id' => 1119,
                'company_id' => 73,
                'block_id' => 222,
                'school_academic_year_id' => 3,
            ),
            99 => 
            array (
                'id' => 1120,
                'company_id' => 73,
                'block_id' => 223,
                'school_academic_year_id' => 3,
            ),
            100 => 
            array (
                'id' => 1121,
                'company_id' => 73,
                'block_id' => 233,
                'school_academic_year_id' => 3,
            ),
            101 => 
            array (
                'id' => 1122,
                'company_id' => 73,
                'block_id' => 222,
                'school_academic_year_id' => 4,
            ),
            102 => 
            array (
                'id' => 1123,
                'company_id' => 73,
                'block_id' => 223,
                'school_academic_year_id' => 4,
            ),
            103 => 
            array (
                'id' => 1124,
                'company_id' => 73,
                'block_id' => 233,
                'school_academic_year_id' => 4,
            ),
            104 => 
            array (
                'id' => 1125,
                'company_id' => 73,
                'block_id' => 222,
                'school_academic_year_id' => 5,
            ),
            105 => 
            array (
                'id' => 1126,
                'company_id' => 73,
                'block_id' => 223,
                'school_academic_year_id' => 5,
            ),
            106 => 
            array (
                'id' => 1127,
                'company_id' => 73,
                'block_id' => 233,
                'school_academic_year_id' => 5,
            ),
            107 => 
            array (
                'id' => 1128,
                'company_id' => 73,
                'block_id' => 222,
                'school_academic_year_id' => 6,
            ),
            108 => 
            array (
                'id' => 1129,
                'company_id' => 73,
                'block_id' => 223,
                'school_academic_year_id' => 6,
            ),
            109 => 
            array (
                'id' => 1130,
                'company_id' => 73,
                'block_id' => 233,
                'school_academic_year_id' => 6,
            ),
            110 => 
            array (
                'id' => 1131,
                'company_id' => 74,
                'block_id' => 231,
                'school_academic_year_id' => 2,
            ),
            111 => 
            array (
                'id' => 1132,
                'company_id' => 74,
                'block_id' => 232,
                'school_academic_year_id' => 2,
            ),
            112 => 
            array (
                'id' => 1133,
                'company_id' => 74,
                'block_id' => 231,
                'school_academic_year_id' => 3,
            ),
            113 => 
            array (
                'id' => 1134,
                'company_id' => 74,
                'block_id' => 232,
                'school_academic_year_id' => 3,
            ),
            114 => 
            array (
                'id' => 1135,
                'company_id' => 74,
                'block_id' => 231,
                'school_academic_year_id' => 4,
            ),
            115 => 
            array (
                'id' => 1136,
                'company_id' => 74,
                'block_id' => 232,
                'school_academic_year_id' => 4,
            ),
            116 => 
            array (
                'id' => 1137,
                'company_id' => 74,
                'block_id' => 231,
                'school_academic_year_id' => 5,
            ),
            117 => 
            array (
                'id' => 1138,
                'company_id' => 74,
                'block_id' => 232,
                'school_academic_year_id' => 5,
            ),
            118 => 
            array (
                'id' => 1139,
                'company_id' => 74,
                'block_id' => 231,
                'school_academic_year_id' => 6,
            ),
            119 => 
            array (
                'id' => 1140,
                'company_id' => 74,
                'block_id' => 232,
                'school_academic_year_id' => 6,
            ),
            120 => 
            array (
                'id' => 1141,
                'company_id' => 75,
                'block_id' => 236,
                'school_academic_year_id' => 2,
            ),
            121 => 
            array (
                'id' => 1142,
                'company_id' => 75,
                'block_id' => 237,
                'school_academic_year_id' => 2,
            ),
            122 => 
            array (
                'id' => 1143,
                'company_id' => 75,
                'block_id' => 238,
                'school_academic_year_id' => 2,
            ),
            123 => 
            array (
                'id' => 1144,
                'company_id' => 75,
                'block_id' => 239,
                'school_academic_year_id' => 2,
            ),
            124 => 
            array (
                'id' => 1145,
                'company_id' => 75,
                'block_id' => 240,
                'school_academic_year_id' => 2,
            ),
            125 => 
            array (
                'id' => 1146,
                'company_id' => 75,
                'block_id' => 241,
                'school_academic_year_id' => 2,
            ),
            126 => 
            array (
                'id' => 1147,
                'company_id' => 75,
                'block_id' => 236,
                'school_academic_year_id' => 3,
            ),
            127 => 
            array (
                'id' => 1148,
                'company_id' => 75,
                'block_id' => 237,
                'school_academic_year_id' => 3,
            ),
            128 => 
            array (
                'id' => 1149,
                'company_id' => 75,
                'block_id' => 238,
                'school_academic_year_id' => 3,
            ),
            129 => 
            array (
                'id' => 1150,
                'company_id' => 75,
                'block_id' => 239,
                'school_academic_year_id' => 3,
            ),
            130 => 
            array (
                'id' => 1151,
                'company_id' => 75,
                'block_id' => 240,
                'school_academic_year_id' => 3,
            ),
            131 => 
            array (
                'id' => 1152,
                'company_id' => 75,
                'block_id' => 241,
                'school_academic_year_id' => 3,
            ),
            132 => 
            array (
                'id' => 1153,
                'company_id' => 75,
                'block_id' => 236,
                'school_academic_year_id' => 4,
            ),
            133 => 
            array (
                'id' => 1154,
                'company_id' => 75,
                'block_id' => 237,
                'school_academic_year_id' => 4,
            ),
            134 => 
            array (
                'id' => 1155,
                'company_id' => 75,
                'block_id' => 238,
                'school_academic_year_id' => 4,
            ),
            135 => 
            array (
                'id' => 1156,
                'company_id' => 75,
                'block_id' => 239,
                'school_academic_year_id' => 4,
            ),
            136 => 
            array (
                'id' => 1157,
                'company_id' => 75,
                'block_id' => 240,
                'school_academic_year_id' => 4,
            ),
            137 => 
            array (
                'id' => 1158,
                'company_id' => 75,
                'block_id' => 241,
                'school_academic_year_id' => 4,
            ),
            138 => 
            array (
                'id' => 1159,
                'company_id' => 75,
                'block_id' => 236,
                'school_academic_year_id' => 5,
            ),
            139 => 
            array (
                'id' => 1160,
                'company_id' => 75,
                'block_id' => 237,
                'school_academic_year_id' => 5,
            ),
            140 => 
            array (
                'id' => 1161,
                'company_id' => 75,
                'block_id' => 238,
                'school_academic_year_id' => 5,
            ),
            141 => 
            array (
                'id' => 1162,
                'company_id' => 75,
                'block_id' => 239,
                'school_academic_year_id' => 5,
            ),
            142 => 
            array (
                'id' => 1163,
                'company_id' => 75,
                'block_id' => 240,
                'school_academic_year_id' => 5,
            ),
            143 => 
            array (
                'id' => 1164,
                'company_id' => 75,
                'block_id' => 241,
                'school_academic_year_id' => 5,
            ),
            144 => 
            array (
                'id' => 1165,
                'company_id' => 75,
                'block_id' => 236,
                'school_academic_year_id' => 6,
            ),
            145 => 
            array (
                'id' => 1166,
                'company_id' => 75,
                'block_id' => 237,
                'school_academic_year_id' => 6,
            ),
            146 => 
            array (
                'id' => 1167,
                'company_id' => 75,
                'block_id' => 238,
                'school_academic_year_id' => 6,
            ),
            147 => 
            array (
                'id' => 1168,
                'company_id' => 75,
                'block_id' => 239,
                'school_academic_year_id' => 6,
            ),
            148 => 
            array (
                'id' => 1169,
                'company_id' => 75,
                'block_id' => 240,
                'school_academic_year_id' => 6,
            ),
            149 => 
            array (
                'id' => 1170,
                'company_id' => 75,
                'block_id' => 241,
                'school_academic_year_id' => 6,
            ),
            150 => 
            array (
                'id' => 1171,
                'company_id' => 76,
                'block_id' => 244,
                'school_academic_year_id' => 2,
            ),
            151 => 
            array (
                'id' => 1172,
                'company_id' => 76,
                'block_id' => 245,
                'school_academic_year_id' => 2,
            ),
            152 => 
            array (
                'id' => 1173,
                'company_id' => 76,
                'block_id' => 246,
                'school_academic_year_id' => 2,
            ),
            153 => 
            array (
                'id' => 1174,
                'company_id' => 76,
                'block_id' => 244,
                'school_academic_year_id' => 3,
            ),
            154 => 
            array (
                'id' => 1175,
                'company_id' => 76,
                'block_id' => 245,
                'school_academic_year_id' => 3,
            ),
            155 => 
            array (
                'id' => 1176,
                'company_id' => 76,
                'block_id' => 246,
                'school_academic_year_id' => 3,
            ),
            156 => 
            array (
                'id' => 1177,
                'company_id' => 76,
                'block_id' => 244,
                'school_academic_year_id' => 4,
            ),
            157 => 
            array (
                'id' => 1178,
                'company_id' => 76,
                'block_id' => 245,
                'school_academic_year_id' => 4,
            ),
            158 => 
            array (
                'id' => 1179,
                'company_id' => 76,
                'block_id' => 246,
                'school_academic_year_id' => 4,
            ),
            159 => 
            array (
                'id' => 1180,
                'company_id' => 76,
                'block_id' => 244,
                'school_academic_year_id' => 5,
            ),
            160 => 
            array (
                'id' => 1181,
                'company_id' => 76,
                'block_id' => 245,
                'school_academic_year_id' => 5,
            ),
            161 => 
            array (
                'id' => 1182,
                'company_id' => 76,
                'block_id' => 246,
                'school_academic_year_id' => 5,
            ),
            162 => 
            array (
                'id' => 1183,
                'company_id' => 76,
                'block_id' => 244,
                'school_academic_year_id' => 6,
            ),
            163 => 
            array (
                'id' => 1184,
                'company_id' => 76,
                'block_id' => 245,
                'school_academic_year_id' => 6,
            ),
            164 => 
            array (
                'id' => 1185,
                'company_id' => 76,
                'block_id' => 246,
                'school_academic_year_id' => 6,
            ),
            165 => 
            array (
                'id' => 1186,
                'company_id' => 77,
                'block_id' => 247,
                'school_academic_year_id' => 2,
            ),
            166 => 
            array (
                'id' => 1187,
                'company_id' => 77,
                'block_id' => 248,
                'school_academic_year_id' => 2,
            ),
            167 => 
            array (
                'id' => 1188,
                'company_id' => 77,
                'block_id' => 247,
                'school_academic_year_id' => 3,
            ),
            168 => 
            array (
                'id' => 1189,
                'company_id' => 77,
                'block_id' => 248,
                'school_academic_year_id' => 3,
            ),
            169 => 
            array (
                'id' => 1190,
                'company_id' => 77,
                'block_id' => 247,
                'school_academic_year_id' => 4,
            ),
            170 => 
            array (
                'id' => 1191,
                'company_id' => 77,
                'block_id' => 248,
                'school_academic_year_id' => 4,
            ),
            171 => 
            array (
                'id' => 1192,
                'company_id' => 77,
                'block_id' => 247,
                'school_academic_year_id' => 5,
            ),
            172 => 
            array (
                'id' => 1193,
                'company_id' => 77,
                'block_id' => 248,
                'school_academic_year_id' => 5,
            ),
            173 => 
            array (
                'id' => 1194,
                'company_id' => 77,
                'block_id' => 247,
                'school_academic_year_id' => 6,
            ),
            174 => 
            array (
                'id' => 1195,
                'company_id' => 77,
                'block_id' => 248,
                'school_academic_year_id' => 6,
            ),
            175 => 
            array (
                'id' => 1196,
                'company_id' => 78,
                'block_id' => 249,
                'school_academic_year_id' => 2,
            ),
            176 => 
            array (
                'id' => 1197,
                'company_id' => 78,
                'block_id' => 249,
                'school_academic_year_id' => 3,
            ),
            177 => 
            array (
                'id' => 1198,
                'company_id' => 78,
                'block_id' => 249,
                'school_academic_year_id' => 4,
            ),
            178 => 
            array (
                'id' => 1199,
                'company_id' => 78,
                'block_id' => 249,
                'school_academic_year_id' => 5,
            ),
            179 => 
            array (
                'id' => 1200,
                'company_id' => 78,
                'block_id' => 249,
                'school_academic_year_id' => 6,
            ),
            180 => 
            array (
                'id' => 1201,
                'company_id' => 79,
                'block_id' => 250,
                'school_academic_year_id' => 2,
            ),
            181 => 
            array (
                'id' => 1202,
                'company_id' => 79,
                'block_id' => 250,
                'school_academic_year_id' => 3,
            ),
            182 => 
            array (
                'id' => 1203,
                'company_id' => 79,
                'block_id' => 250,
                'school_academic_year_id' => 4,
            ),
            183 => 
            array (
                'id' => 1204,
                'company_id' => 79,
                'block_id' => 250,
                'school_academic_year_id' => 5,
            ),
            184 => 
            array (
                'id' => 1205,
                'company_id' => 79,
                'block_id' => 250,
                'school_academic_year_id' => 6,
            ),
            185 => 
            array (
                'id' => 1206,
                'company_id' => 80,
                'block_id' => 265,
                'school_academic_year_id' => 2,
            ),
            186 => 
            array (
                'id' => 1207,
                'company_id' => 80,
                'block_id' => 266,
                'school_academic_year_id' => 2,
            ),
            187 => 
            array (
                'id' => 1208,
                'company_id' => 80,
                'block_id' => 267,
                'school_academic_year_id' => 2,
            ),
            188 => 
            array (
                'id' => 1209,
                'company_id' => 80,
                'block_id' => 265,
                'school_academic_year_id' => 3,
            ),
            189 => 
            array (
                'id' => 1210,
                'company_id' => 80,
                'block_id' => 266,
                'school_academic_year_id' => 3,
            ),
            190 => 
            array (
                'id' => 1211,
                'company_id' => 80,
                'block_id' => 267,
                'school_academic_year_id' => 3,
            ),
            191 => 
            array (
                'id' => 1212,
                'company_id' => 80,
                'block_id' => 265,
                'school_academic_year_id' => 4,
            ),
            192 => 
            array (
                'id' => 1213,
                'company_id' => 80,
                'block_id' => 266,
                'school_academic_year_id' => 4,
            ),
            193 => 
            array (
                'id' => 1214,
                'company_id' => 80,
                'block_id' => 267,
                'school_academic_year_id' => 4,
            ),
            194 => 
            array (
                'id' => 1215,
                'company_id' => 80,
                'block_id' => 265,
                'school_academic_year_id' => 5,
            ),
            195 => 
            array (
                'id' => 1216,
                'company_id' => 80,
                'block_id' => 266,
                'school_academic_year_id' => 5,
            ),
            196 => 
            array (
                'id' => 1217,
                'company_id' => 80,
                'block_id' => 267,
                'school_academic_year_id' => 5,
            ),
            197 => 
            array (
                'id' => 1218,
                'company_id' => 80,
                'block_id' => 265,
                'school_academic_year_id' => 6,
            ),
            198 => 
            array (
                'id' => 1219,
                'company_id' => 80,
                'block_id' => 266,
                'school_academic_year_id' => 6,
            ),
            199 => 
            array (
                'id' => 1220,
                'company_id' => 80,
                'block_id' => 267,
                'school_academic_year_id' => 6,
            ),
            200 => 
            array (
                'id' => 1221,
                'company_id' => 81,
                'block_id' => 251,
                'school_academic_year_id' => 2,
            ),
            201 => 
            array (
                'id' => 1222,
                'company_id' => 81,
                'block_id' => 252,
                'school_academic_year_id' => 2,
            ),
            202 => 
            array (
                'id' => 1223,
                'company_id' => 81,
                'block_id' => 253,
                'school_academic_year_id' => 2,
            ),
            203 => 
            array (
                'id' => 1224,
                'company_id' => 81,
                'block_id' => 254,
                'school_academic_year_id' => 2,
            ),
            204 => 
            array (
                'id' => 1225,
                'company_id' => 81,
                'block_id' => 255,
                'school_academic_year_id' => 2,
            ),
            205 => 
            array (
                'id' => 1226,
                'company_id' => 81,
                'block_id' => 251,
                'school_academic_year_id' => 3,
            ),
            206 => 
            array (
                'id' => 1227,
                'company_id' => 81,
                'block_id' => 252,
                'school_academic_year_id' => 3,
            ),
            207 => 
            array (
                'id' => 1228,
                'company_id' => 81,
                'block_id' => 253,
                'school_academic_year_id' => 3,
            ),
            208 => 
            array (
                'id' => 1229,
                'company_id' => 81,
                'block_id' => 254,
                'school_academic_year_id' => 3,
            ),
            209 => 
            array (
                'id' => 1230,
                'company_id' => 81,
                'block_id' => 255,
                'school_academic_year_id' => 3,
            ),
            210 => 
            array (
                'id' => 1231,
                'company_id' => 81,
                'block_id' => 251,
                'school_academic_year_id' => 4,
            ),
            211 => 
            array (
                'id' => 1232,
                'company_id' => 81,
                'block_id' => 252,
                'school_academic_year_id' => 4,
            ),
            212 => 
            array (
                'id' => 1233,
                'company_id' => 81,
                'block_id' => 253,
                'school_academic_year_id' => 4,
            ),
            213 => 
            array (
                'id' => 1234,
                'company_id' => 81,
                'block_id' => 254,
                'school_academic_year_id' => 4,
            ),
            214 => 
            array (
                'id' => 1235,
                'company_id' => 81,
                'block_id' => 255,
                'school_academic_year_id' => 4,
            ),
            215 => 
            array (
                'id' => 1236,
                'company_id' => 81,
                'block_id' => 251,
                'school_academic_year_id' => 5,
            ),
            216 => 
            array (
                'id' => 1237,
                'company_id' => 81,
                'block_id' => 252,
                'school_academic_year_id' => 5,
            ),
            217 => 
            array (
                'id' => 1238,
                'company_id' => 81,
                'block_id' => 253,
                'school_academic_year_id' => 5,
            ),
            218 => 
            array (
                'id' => 1239,
                'company_id' => 81,
                'block_id' => 254,
                'school_academic_year_id' => 5,
            ),
            219 => 
            array (
                'id' => 1240,
                'company_id' => 81,
                'block_id' => 255,
                'school_academic_year_id' => 5,
            ),
            220 => 
            array (
                'id' => 1241,
                'company_id' => 81,
                'block_id' => 251,
                'school_academic_year_id' => 6,
            ),
            221 => 
            array (
                'id' => 1242,
                'company_id' => 81,
                'block_id' => 252,
                'school_academic_year_id' => 6,
            ),
            222 => 
            array (
                'id' => 1243,
                'company_id' => 81,
                'block_id' => 253,
                'school_academic_year_id' => 6,
            ),
            223 => 
            array (
                'id' => 1244,
                'company_id' => 81,
                'block_id' => 254,
                'school_academic_year_id' => 6,
            ),
            224 => 
            array (
                'id' => 1245,
                'company_id' => 81,
                'block_id' => 255,
                'school_academic_year_id' => 6,
            ),
            225 => 
            array (
                'id' => 1246,
                'company_id' => 82,
                'block_id' => 257,
                'school_academic_year_id' => 2,
            ),
            226 => 
            array (
                'id' => 1247,
                'company_id' => 82,
                'block_id' => 258,
                'school_academic_year_id' => 2,
            ),
            227 => 
            array (
                'id' => 1248,
                'company_id' => 82,
                'block_id' => 259,
                'school_academic_year_id' => 2,
            ),
            228 => 
            array (
                'id' => 1249,
                'company_id' => 82,
                'block_id' => 260,
                'school_academic_year_id' => 2,
            ),
            229 => 
            array (
                'id' => 1250,
                'company_id' => 82,
                'block_id' => 261,
                'school_academic_year_id' => 2,
            ),
            230 => 
            array (
                'id' => 1251,
                'company_id' => 82,
                'block_id' => 262,
                'school_academic_year_id' => 2,
            ),
            231 => 
            array (
                'id' => 1252,
                'company_id' => 82,
                'block_id' => 263,
                'school_academic_year_id' => 2,
            ),
            232 => 
            array (
                'id' => 1253,
                'company_id' => 82,
                'block_id' => 264,
                'school_academic_year_id' => 2,
            ),
            233 => 
            array (
                'id' => 1254,
                'company_id' => 82,
                'block_id' => 335,
                'school_academic_year_id' => 2,
            ),
            234 => 
            array (
                'id' => 1255,
                'company_id' => 82,
                'block_id' => 257,
                'school_academic_year_id' => 3,
            ),
            235 => 
            array (
                'id' => 1256,
                'company_id' => 82,
                'block_id' => 258,
                'school_academic_year_id' => 3,
            ),
            236 => 
            array (
                'id' => 1257,
                'company_id' => 82,
                'block_id' => 259,
                'school_academic_year_id' => 3,
            ),
            237 => 
            array (
                'id' => 1258,
                'company_id' => 82,
                'block_id' => 260,
                'school_academic_year_id' => 3,
            ),
            238 => 
            array (
                'id' => 1259,
                'company_id' => 82,
                'block_id' => 261,
                'school_academic_year_id' => 3,
            ),
            239 => 
            array (
                'id' => 1260,
                'company_id' => 82,
                'block_id' => 262,
                'school_academic_year_id' => 3,
            ),
            240 => 
            array (
                'id' => 1261,
                'company_id' => 82,
                'block_id' => 263,
                'school_academic_year_id' => 3,
            ),
            241 => 
            array (
                'id' => 1262,
                'company_id' => 82,
                'block_id' => 264,
                'school_academic_year_id' => 3,
            ),
            242 => 
            array (
                'id' => 1263,
                'company_id' => 82,
                'block_id' => 335,
                'school_academic_year_id' => 3,
            ),
            243 => 
            array (
                'id' => 1264,
                'company_id' => 82,
                'block_id' => 257,
                'school_academic_year_id' => 4,
            ),
            244 => 
            array (
                'id' => 1265,
                'company_id' => 82,
                'block_id' => 258,
                'school_academic_year_id' => 4,
            ),
            245 => 
            array (
                'id' => 1266,
                'company_id' => 82,
                'block_id' => 259,
                'school_academic_year_id' => 4,
            ),
            246 => 
            array (
                'id' => 1267,
                'company_id' => 82,
                'block_id' => 260,
                'school_academic_year_id' => 4,
            ),
            247 => 
            array (
                'id' => 1268,
                'company_id' => 82,
                'block_id' => 261,
                'school_academic_year_id' => 4,
            ),
            248 => 
            array (
                'id' => 1269,
                'company_id' => 82,
                'block_id' => 262,
                'school_academic_year_id' => 4,
            ),
            249 => 
            array (
                'id' => 1270,
                'company_id' => 82,
                'block_id' => 263,
                'school_academic_year_id' => 4,
            ),
            250 => 
            array (
                'id' => 1271,
                'company_id' => 82,
                'block_id' => 264,
                'school_academic_year_id' => 4,
            ),
            251 => 
            array (
                'id' => 1272,
                'company_id' => 82,
                'block_id' => 335,
                'school_academic_year_id' => 4,
            ),
            252 => 
            array (
                'id' => 1273,
                'company_id' => 82,
                'block_id' => 257,
                'school_academic_year_id' => 5,
            ),
            253 => 
            array (
                'id' => 1274,
                'company_id' => 82,
                'block_id' => 258,
                'school_academic_year_id' => 5,
            ),
            254 => 
            array (
                'id' => 1275,
                'company_id' => 82,
                'block_id' => 259,
                'school_academic_year_id' => 5,
            ),
            255 => 
            array (
                'id' => 1276,
                'company_id' => 82,
                'block_id' => 260,
                'school_academic_year_id' => 5,
            ),
            256 => 
            array (
                'id' => 1277,
                'company_id' => 82,
                'block_id' => 261,
                'school_academic_year_id' => 5,
            ),
            257 => 
            array (
                'id' => 1278,
                'company_id' => 82,
                'block_id' => 262,
                'school_academic_year_id' => 5,
            ),
            258 => 
            array (
                'id' => 1279,
                'company_id' => 82,
                'block_id' => 263,
                'school_academic_year_id' => 5,
            ),
            259 => 
            array (
                'id' => 1280,
                'company_id' => 82,
                'block_id' => 264,
                'school_academic_year_id' => 5,
            ),
            260 => 
            array (
                'id' => 1281,
                'company_id' => 82,
                'block_id' => 335,
                'school_academic_year_id' => 5,
            ),
            261 => 
            array (
                'id' => 1282,
                'company_id' => 82,
                'block_id' => 257,
                'school_academic_year_id' => 6,
            ),
            262 => 
            array (
                'id' => 1283,
                'company_id' => 82,
                'block_id' => 258,
                'school_academic_year_id' => 6,
            ),
            263 => 
            array (
                'id' => 1284,
                'company_id' => 82,
                'block_id' => 259,
                'school_academic_year_id' => 6,
            ),
            264 => 
            array (
                'id' => 1285,
                'company_id' => 82,
                'block_id' => 260,
                'school_academic_year_id' => 6,
            ),
            265 => 
            array (
                'id' => 1286,
                'company_id' => 82,
                'block_id' => 261,
                'school_academic_year_id' => 6,
            ),
            266 => 
            array (
                'id' => 1287,
                'company_id' => 82,
                'block_id' => 262,
                'school_academic_year_id' => 6,
            ),
            267 => 
            array (
                'id' => 1288,
                'company_id' => 82,
                'block_id' => 263,
                'school_academic_year_id' => 6,
            ),
            268 => 
            array (
                'id' => 1289,
                'company_id' => 82,
                'block_id' => 264,
                'school_academic_year_id' => 6,
            ),
            269 => 
            array (
                'id' => 1290,
                'company_id' => 82,
                'block_id' => 335,
                'school_academic_year_id' => 6,
            ),
            270 => 
            array (
                'id' => 1291,
                'company_id' => 83,
                'block_id' => 268,
                'school_academic_year_id' => 2,
            ),
            271 => 
            array (
                'id' => 1292,
                'company_id' => 83,
                'block_id' => 269,
                'school_academic_year_id' => 2,
            ),
            272 => 
            array (
                'id' => 1293,
                'company_id' => 83,
                'block_id' => 270,
                'school_academic_year_id' => 2,
            ),
            273 => 
            array (
                'id' => 1294,
                'company_id' => 83,
                'block_id' => 272,
                'school_academic_year_id' => 2,
            ),
            274 => 
            array (
                'id' => 1295,
                'company_id' => 83,
                'block_id' => 268,
                'school_academic_year_id' => 3,
            ),
            275 => 
            array (
                'id' => 1296,
                'company_id' => 83,
                'block_id' => 269,
                'school_academic_year_id' => 3,
            ),
            276 => 
            array (
                'id' => 1297,
                'company_id' => 83,
                'block_id' => 270,
                'school_academic_year_id' => 3,
            ),
            277 => 
            array (
                'id' => 1298,
                'company_id' => 83,
                'block_id' => 272,
                'school_academic_year_id' => 3,
            ),
            278 => 
            array (
                'id' => 1299,
                'company_id' => 83,
                'block_id' => 268,
                'school_academic_year_id' => 4,
            ),
            279 => 
            array (
                'id' => 1300,
                'company_id' => 83,
                'block_id' => 269,
                'school_academic_year_id' => 4,
            ),
            280 => 
            array (
                'id' => 1301,
                'company_id' => 83,
                'block_id' => 270,
                'school_academic_year_id' => 4,
            ),
            281 => 
            array (
                'id' => 1302,
                'company_id' => 83,
                'block_id' => 272,
                'school_academic_year_id' => 4,
            ),
            282 => 
            array (
                'id' => 1303,
                'company_id' => 83,
                'block_id' => 268,
                'school_academic_year_id' => 5,
            ),
            283 => 
            array (
                'id' => 1304,
                'company_id' => 83,
                'block_id' => 269,
                'school_academic_year_id' => 5,
            ),
            284 => 
            array (
                'id' => 1305,
                'company_id' => 83,
                'block_id' => 270,
                'school_academic_year_id' => 5,
            ),
            285 => 
            array (
                'id' => 1306,
                'company_id' => 83,
                'block_id' => 272,
                'school_academic_year_id' => 5,
            ),
            286 => 
            array (
                'id' => 1307,
                'company_id' => 83,
                'block_id' => 268,
                'school_academic_year_id' => 6,
            ),
            287 => 
            array (
                'id' => 1308,
                'company_id' => 83,
                'block_id' => 269,
                'school_academic_year_id' => 6,
            ),
            288 => 
            array (
                'id' => 1309,
                'company_id' => 83,
                'block_id' => 270,
                'school_academic_year_id' => 6,
            ),
            289 => 
            array (
                'id' => 1310,
                'company_id' => 83,
                'block_id' => 272,
                'school_academic_year_id' => 6,
            ),
            290 => 
            array (
                'id' => 1311,
                'company_id' => 84,
                'block_id' => 276,
                'school_academic_year_id' => 2,
            ),
            291 => 
            array (
                'id' => 1312,
                'company_id' => 84,
                'block_id' => 277,
                'school_academic_year_id' => 2,
            ),
            292 => 
            array (
                'id' => 1313,
                'company_id' => 84,
                'block_id' => 276,
                'school_academic_year_id' => 3,
            ),
            293 => 
            array (
                'id' => 1314,
                'company_id' => 84,
                'block_id' => 277,
                'school_academic_year_id' => 3,
            ),
            294 => 
            array (
                'id' => 1315,
                'company_id' => 84,
                'block_id' => 276,
                'school_academic_year_id' => 4,
            ),
            295 => 
            array (
                'id' => 1316,
                'company_id' => 84,
                'block_id' => 277,
                'school_academic_year_id' => 4,
            ),
            296 => 
            array (
                'id' => 1317,
                'company_id' => 84,
                'block_id' => 276,
                'school_academic_year_id' => 5,
            ),
            297 => 
            array (
                'id' => 1318,
                'company_id' => 84,
                'block_id' => 277,
                'school_academic_year_id' => 5,
            ),
            298 => 
            array (
                'id' => 1319,
                'company_id' => 84,
                'block_id' => 276,
                'school_academic_year_id' => 6,
            ),
            299 => 
            array (
                'id' => 1320,
                'company_id' => 84,
                'block_id' => 277,
                'school_academic_year_id' => 6,
            ),
            300 => 
            array (
                'id' => 1321,
                'company_id' => 85,
                'block_id' => 292,
                'school_academic_year_id' => 2,
            ),
            301 => 
            array (
                'id' => 1322,
                'company_id' => 85,
                'block_id' => 292,
                'school_academic_year_id' => 3,
            ),
            302 => 
            array (
                'id' => 1323,
                'company_id' => 85,
                'block_id' => 292,
                'school_academic_year_id' => 4,
            ),
            303 => 
            array (
                'id' => 1324,
                'company_id' => 85,
                'block_id' => 292,
                'school_academic_year_id' => 5,
            ),
            304 => 
            array (
                'id' => 1325,
                'company_id' => 85,
                'block_id' => 292,
                'school_academic_year_id' => 6,
            ),
            305 => 
            array (
                'id' => 1326,
                'company_id' => 86,
                'block_id' => 293,
                'school_academic_year_id' => 2,
            ),
            306 => 
            array (
                'id' => 1327,
                'company_id' => 86,
                'block_id' => 294,
                'school_academic_year_id' => 2,
            ),
            307 => 
            array (
                'id' => 1328,
                'company_id' => 86,
                'block_id' => 295,
                'school_academic_year_id' => 2,
            ),
            308 => 
            array (
                'id' => 1329,
                'company_id' => 86,
                'block_id' => 296,
                'school_academic_year_id' => 2,
            ),
            309 => 
            array (
                'id' => 1330,
                'company_id' => 86,
                'block_id' => 329,
                'school_academic_year_id' => 2,
            ),
            310 => 
            array (
                'id' => 1331,
                'company_id' => 86,
                'block_id' => 448,
                'school_academic_year_id' => 2,
            ),
            311 => 
            array (
                'id' => 1332,
                'company_id' => 86,
                'block_id' => 449,
                'school_academic_year_id' => 2,
            ),
            312 => 
            array (
                'id' => 1333,
                'company_id' => 86,
                'block_id' => 293,
                'school_academic_year_id' => 3,
            ),
            313 => 
            array (
                'id' => 1334,
                'company_id' => 86,
                'block_id' => 294,
                'school_academic_year_id' => 3,
            ),
            314 => 
            array (
                'id' => 1335,
                'company_id' => 86,
                'block_id' => 295,
                'school_academic_year_id' => 3,
            ),
            315 => 
            array (
                'id' => 1336,
                'company_id' => 86,
                'block_id' => 296,
                'school_academic_year_id' => 3,
            ),
            316 => 
            array (
                'id' => 1337,
                'company_id' => 86,
                'block_id' => 329,
                'school_academic_year_id' => 3,
            ),
            317 => 
            array (
                'id' => 1338,
                'company_id' => 86,
                'block_id' => 448,
                'school_academic_year_id' => 3,
            ),
            318 => 
            array (
                'id' => 1339,
                'company_id' => 86,
                'block_id' => 449,
                'school_academic_year_id' => 3,
            ),
            319 => 
            array (
                'id' => 1340,
                'company_id' => 86,
                'block_id' => 293,
                'school_academic_year_id' => 4,
            ),
            320 => 
            array (
                'id' => 1341,
                'company_id' => 86,
                'block_id' => 294,
                'school_academic_year_id' => 4,
            ),
            321 => 
            array (
                'id' => 1342,
                'company_id' => 86,
                'block_id' => 295,
                'school_academic_year_id' => 4,
            ),
            322 => 
            array (
                'id' => 1343,
                'company_id' => 86,
                'block_id' => 296,
                'school_academic_year_id' => 4,
            ),
            323 => 
            array (
                'id' => 1344,
                'company_id' => 86,
                'block_id' => 329,
                'school_academic_year_id' => 4,
            ),
            324 => 
            array (
                'id' => 1345,
                'company_id' => 86,
                'block_id' => 448,
                'school_academic_year_id' => 4,
            ),
            325 => 
            array (
                'id' => 1346,
                'company_id' => 86,
                'block_id' => 449,
                'school_academic_year_id' => 4,
            ),
            326 => 
            array (
                'id' => 1347,
                'company_id' => 86,
                'block_id' => 293,
                'school_academic_year_id' => 5,
            ),
            327 => 
            array (
                'id' => 1348,
                'company_id' => 86,
                'block_id' => 294,
                'school_academic_year_id' => 5,
            ),
            328 => 
            array (
                'id' => 1349,
                'company_id' => 86,
                'block_id' => 295,
                'school_academic_year_id' => 5,
            ),
            329 => 
            array (
                'id' => 1350,
                'company_id' => 86,
                'block_id' => 296,
                'school_academic_year_id' => 5,
            ),
            330 => 
            array (
                'id' => 1351,
                'company_id' => 86,
                'block_id' => 329,
                'school_academic_year_id' => 5,
            ),
            331 => 
            array (
                'id' => 1352,
                'company_id' => 86,
                'block_id' => 448,
                'school_academic_year_id' => 5,
            ),
            332 => 
            array (
                'id' => 1353,
                'company_id' => 86,
                'block_id' => 449,
                'school_academic_year_id' => 5,
            ),
            333 => 
            array (
                'id' => 1354,
                'company_id' => 86,
                'block_id' => 293,
                'school_academic_year_id' => 6,
            ),
            334 => 
            array (
                'id' => 1355,
                'company_id' => 86,
                'block_id' => 294,
                'school_academic_year_id' => 6,
            ),
            335 => 
            array (
                'id' => 1356,
                'company_id' => 86,
                'block_id' => 295,
                'school_academic_year_id' => 6,
            ),
            336 => 
            array (
                'id' => 1357,
                'company_id' => 86,
                'block_id' => 296,
                'school_academic_year_id' => 6,
            ),
            337 => 
            array (
                'id' => 1358,
                'company_id' => 86,
                'block_id' => 329,
                'school_academic_year_id' => 6,
            ),
            338 => 
            array (
                'id' => 1359,
                'company_id' => 86,
                'block_id' => 448,
                'school_academic_year_id' => 6,
            ),
            339 => 
            array (
                'id' => 1360,
                'company_id' => 86,
                'block_id' => 449,
                'school_academic_year_id' => 6,
            ),
            340 => 
            array (
                'id' => 1361,
                'company_id' => 87,
                'block_id' => 299,
                'school_academic_year_id' => 2,
            ),
            341 => 
            array (
                'id' => 1362,
                'company_id' => 87,
                'block_id' => 300,
                'school_academic_year_id' => 2,
            ),
            342 => 
            array (
                'id' => 1363,
                'company_id' => 87,
                'block_id' => 301,
                'school_academic_year_id' => 2,
            ),
            343 => 
            array (
                'id' => 1364,
                'company_id' => 87,
                'block_id' => 302,
                'school_academic_year_id' => 2,
            ),
            344 => 
            array (
                'id' => 1365,
                'company_id' => 87,
                'block_id' => 303,
                'school_academic_year_id' => 2,
            ),
            345 => 
            array (
                'id' => 1366,
                'company_id' => 87,
                'block_id' => 304,
                'school_academic_year_id' => 2,
            ),
            346 => 
            array (
                'id' => 1367,
                'company_id' => 87,
                'block_id' => 305,
                'school_academic_year_id' => 2,
            ),
            347 => 
            array (
                'id' => 1368,
                'company_id' => 87,
                'block_id' => 299,
                'school_academic_year_id' => 3,
            ),
            348 => 
            array (
                'id' => 1369,
                'company_id' => 87,
                'block_id' => 300,
                'school_academic_year_id' => 3,
            ),
            349 => 
            array (
                'id' => 1370,
                'company_id' => 87,
                'block_id' => 301,
                'school_academic_year_id' => 3,
            ),
            350 => 
            array (
                'id' => 1371,
                'company_id' => 87,
                'block_id' => 302,
                'school_academic_year_id' => 3,
            ),
            351 => 
            array (
                'id' => 1372,
                'company_id' => 87,
                'block_id' => 303,
                'school_academic_year_id' => 3,
            ),
            352 => 
            array (
                'id' => 1373,
                'company_id' => 87,
                'block_id' => 304,
                'school_academic_year_id' => 3,
            ),
            353 => 
            array (
                'id' => 1374,
                'company_id' => 87,
                'block_id' => 305,
                'school_academic_year_id' => 3,
            ),
            354 => 
            array (
                'id' => 1375,
                'company_id' => 87,
                'block_id' => 299,
                'school_academic_year_id' => 4,
            ),
            355 => 
            array (
                'id' => 1376,
                'company_id' => 87,
                'block_id' => 300,
                'school_academic_year_id' => 4,
            ),
            356 => 
            array (
                'id' => 1377,
                'company_id' => 87,
                'block_id' => 301,
                'school_academic_year_id' => 4,
            ),
            357 => 
            array (
                'id' => 1378,
                'company_id' => 87,
                'block_id' => 302,
                'school_academic_year_id' => 4,
            ),
            358 => 
            array (
                'id' => 1379,
                'company_id' => 87,
                'block_id' => 303,
                'school_academic_year_id' => 4,
            ),
            359 => 
            array (
                'id' => 1380,
                'company_id' => 87,
                'block_id' => 304,
                'school_academic_year_id' => 4,
            ),
            360 => 
            array (
                'id' => 1381,
                'company_id' => 87,
                'block_id' => 305,
                'school_academic_year_id' => 4,
            ),
            361 => 
            array (
                'id' => 1382,
                'company_id' => 87,
                'block_id' => 299,
                'school_academic_year_id' => 5,
            ),
            362 => 
            array (
                'id' => 1383,
                'company_id' => 87,
                'block_id' => 300,
                'school_academic_year_id' => 5,
            ),
            363 => 
            array (
                'id' => 1384,
                'company_id' => 87,
                'block_id' => 301,
                'school_academic_year_id' => 5,
            ),
            364 => 
            array (
                'id' => 1385,
                'company_id' => 87,
                'block_id' => 302,
                'school_academic_year_id' => 5,
            ),
            365 => 
            array (
                'id' => 1386,
                'company_id' => 87,
                'block_id' => 303,
                'school_academic_year_id' => 5,
            ),
            366 => 
            array (
                'id' => 1387,
                'company_id' => 87,
                'block_id' => 304,
                'school_academic_year_id' => 5,
            ),
            367 => 
            array (
                'id' => 1388,
                'company_id' => 87,
                'block_id' => 305,
                'school_academic_year_id' => 5,
            ),
            368 => 
            array (
                'id' => 1389,
                'company_id' => 87,
                'block_id' => 299,
                'school_academic_year_id' => 6,
            ),
            369 => 
            array (
                'id' => 1390,
                'company_id' => 87,
                'block_id' => 300,
                'school_academic_year_id' => 6,
            ),
            370 => 
            array (
                'id' => 1391,
                'company_id' => 87,
                'block_id' => 301,
                'school_academic_year_id' => 6,
            ),
            371 => 
            array (
                'id' => 1392,
                'company_id' => 87,
                'block_id' => 302,
                'school_academic_year_id' => 6,
            ),
            372 => 
            array (
                'id' => 1393,
                'company_id' => 87,
                'block_id' => 303,
                'school_academic_year_id' => 6,
            ),
            373 => 
            array (
                'id' => 1394,
                'company_id' => 87,
                'block_id' => 304,
                'school_academic_year_id' => 6,
            ),
            374 => 
            array (
                'id' => 1395,
                'company_id' => 87,
                'block_id' => 305,
                'school_academic_year_id' => 6,
            ),
            375 => 
            array (
                'id' => 1396,
                'company_id' => 89,
                'block_id' => 309,
                'school_academic_year_id' => 2,
            ),
            376 => 
            array (
                'id' => 1397,
                'company_id' => 89,
                'block_id' => 309,
                'school_academic_year_id' => 3,
            ),
            377 => 
            array (
                'id' => 1398,
                'company_id' => 89,
                'block_id' => 309,
                'school_academic_year_id' => 4,
            ),
            378 => 
            array (
                'id' => 1399,
                'company_id' => 89,
                'block_id' => 309,
                'school_academic_year_id' => 5,
            ),
            379 => 
            array (
                'id' => 1400,
                'company_id' => 89,
                'block_id' => 309,
                'school_academic_year_id' => 6,
            ),
            380 => 
            array (
                'id' => 1401,
                'company_id' => 90,
                'block_id' => 311,
                'school_academic_year_id' => 2,
            ),
            381 => 
            array (
                'id' => 1402,
                'company_id' => 90,
                'block_id' => 311,
                'school_academic_year_id' => 3,
            ),
            382 => 
            array (
                'id' => 1403,
                'company_id' => 90,
                'block_id' => 311,
                'school_academic_year_id' => 4,
            ),
            383 => 
            array (
                'id' => 1404,
                'company_id' => 90,
                'block_id' => 311,
                'school_academic_year_id' => 5,
            ),
            384 => 
            array (
                'id' => 1405,
                'company_id' => 90,
                'block_id' => 311,
                'school_academic_year_id' => 6,
            ),
            385 => 
            array (
                'id' => 1406,
                'company_id' => 91,
                'block_id' => 312,
                'school_academic_year_id' => 2,
            ),
            386 => 
            array (
                'id' => 1407,
                'company_id' => 91,
                'block_id' => 312,
                'school_academic_year_id' => 3,
            ),
            387 => 
            array (
                'id' => 1408,
                'company_id' => 91,
                'block_id' => 312,
                'school_academic_year_id' => 4,
            ),
            388 => 
            array (
                'id' => 1409,
                'company_id' => 91,
                'block_id' => 312,
                'school_academic_year_id' => 5,
            ),
            389 => 
            array (
                'id' => 1410,
                'company_id' => 91,
                'block_id' => 312,
                'school_academic_year_id' => 6,
            ),
            390 => 
            array (
                'id' => 1411,
                'company_id' => 92,
                'block_id' => 317,
                'school_academic_year_id' => 2,
            ),
            391 => 
            array (
                'id' => 1412,
                'company_id' => 92,
                'block_id' => 438,
                'school_academic_year_id' => 2,
            ),
            392 => 
            array (
                'id' => 1413,
                'company_id' => 92,
                'block_id' => 439,
                'school_academic_year_id' => 2,
            ),
            393 => 
            array (
                'id' => 1414,
                'company_id' => 92,
                'block_id' => 317,
                'school_academic_year_id' => 3,
            ),
            394 => 
            array (
                'id' => 1415,
                'company_id' => 92,
                'block_id' => 438,
                'school_academic_year_id' => 3,
            ),
            395 => 
            array (
                'id' => 1416,
                'company_id' => 92,
                'block_id' => 439,
                'school_academic_year_id' => 3,
            ),
            396 => 
            array (
                'id' => 1417,
                'company_id' => 92,
                'block_id' => 317,
                'school_academic_year_id' => 4,
            ),
            397 => 
            array (
                'id' => 1418,
                'company_id' => 92,
                'block_id' => 438,
                'school_academic_year_id' => 4,
            ),
            398 => 
            array (
                'id' => 1419,
                'company_id' => 92,
                'block_id' => 439,
                'school_academic_year_id' => 4,
            ),
            399 => 
            array (
                'id' => 1420,
                'company_id' => 92,
                'block_id' => 317,
                'school_academic_year_id' => 5,
            ),
            400 => 
            array (
                'id' => 1421,
                'company_id' => 92,
                'block_id' => 438,
                'school_academic_year_id' => 5,
            ),
            401 => 
            array (
                'id' => 1422,
                'company_id' => 92,
                'block_id' => 439,
                'school_academic_year_id' => 5,
            ),
            402 => 
            array (
                'id' => 1423,
                'company_id' => 92,
                'block_id' => 317,
                'school_academic_year_id' => 6,
            ),
            403 => 
            array (
                'id' => 1424,
                'company_id' => 92,
                'block_id' => 438,
                'school_academic_year_id' => 6,
            ),
            404 => 
            array (
                'id' => 1425,
                'company_id' => 92,
                'block_id' => 439,
                'school_academic_year_id' => 6,
            ),
            405 => 
            array (
                'id' => 1426,
                'company_id' => 93,
                'block_id' => 318,
                'school_academic_year_id' => 2,
            ),
            406 => 
            array (
                'id' => 1427,
                'company_id' => 93,
                'block_id' => 318,
                'school_academic_year_id' => 3,
            ),
            407 => 
            array (
                'id' => 1428,
                'company_id' => 93,
                'block_id' => 318,
                'school_academic_year_id' => 4,
            ),
            408 => 
            array (
                'id' => 1429,
                'company_id' => 93,
                'block_id' => 318,
                'school_academic_year_id' => 5,
            ),
            409 => 
            array (
                'id' => 1430,
                'company_id' => 93,
                'block_id' => 318,
                'school_academic_year_id' => 6,
            ),
            410 => 
            array (
                'id' => 1431,
                'company_id' => 94,
                'block_id' => 322,
                'school_academic_year_id' => 2,
            ),
            411 => 
            array (
                'id' => 1432,
                'company_id' => 94,
                'block_id' => 337,
                'school_academic_year_id' => 2,
            ),
            412 => 
            array (
                'id' => 1433,
                'company_id' => 94,
                'block_id' => 338,
                'school_academic_year_id' => 2,
            ),
            413 => 
            array (
                'id' => 1434,
                'company_id' => 94,
                'block_id' => 339,
                'school_academic_year_id' => 2,
            ),
            414 => 
            array (
                'id' => 1435,
                'company_id' => 94,
                'block_id' => 340,
                'school_academic_year_id' => 2,
            ),
            415 => 
            array (
                'id' => 1436,
                'company_id' => 94,
                'block_id' => 341,
                'school_academic_year_id' => 2,
            ),
            416 => 
            array (
                'id' => 1437,
                'company_id' => 94,
                'block_id' => 342,
                'school_academic_year_id' => 2,
            ),
            417 => 
            array (
                'id' => 1438,
                'company_id' => 94,
                'block_id' => 344,
                'school_academic_year_id' => 2,
            ),
            418 => 
            array (
                'id' => 1439,
                'company_id' => 94,
                'block_id' => 322,
                'school_academic_year_id' => 3,
            ),
            419 => 
            array (
                'id' => 1440,
                'company_id' => 94,
                'block_id' => 337,
                'school_academic_year_id' => 3,
            ),
            420 => 
            array (
                'id' => 1441,
                'company_id' => 94,
                'block_id' => 338,
                'school_academic_year_id' => 3,
            ),
            421 => 
            array (
                'id' => 1442,
                'company_id' => 94,
                'block_id' => 339,
                'school_academic_year_id' => 3,
            ),
            422 => 
            array (
                'id' => 1443,
                'company_id' => 94,
                'block_id' => 340,
                'school_academic_year_id' => 3,
            ),
            423 => 
            array (
                'id' => 1444,
                'company_id' => 94,
                'block_id' => 341,
                'school_academic_year_id' => 3,
            ),
            424 => 
            array (
                'id' => 1445,
                'company_id' => 94,
                'block_id' => 342,
                'school_academic_year_id' => 3,
            ),
            425 => 
            array (
                'id' => 1446,
                'company_id' => 94,
                'block_id' => 344,
                'school_academic_year_id' => 3,
            ),
            426 => 
            array (
                'id' => 1447,
                'company_id' => 94,
                'block_id' => 322,
                'school_academic_year_id' => 4,
            ),
            427 => 
            array (
                'id' => 1448,
                'company_id' => 94,
                'block_id' => 337,
                'school_academic_year_id' => 4,
            ),
            428 => 
            array (
                'id' => 1449,
                'company_id' => 94,
                'block_id' => 338,
                'school_academic_year_id' => 4,
            ),
            429 => 
            array (
                'id' => 1450,
                'company_id' => 94,
                'block_id' => 339,
                'school_academic_year_id' => 4,
            ),
            430 => 
            array (
                'id' => 1451,
                'company_id' => 94,
                'block_id' => 340,
                'school_academic_year_id' => 4,
            ),
            431 => 
            array (
                'id' => 1452,
                'company_id' => 94,
                'block_id' => 341,
                'school_academic_year_id' => 4,
            ),
            432 => 
            array (
                'id' => 1453,
                'company_id' => 94,
                'block_id' => 342,
                'school_academic_year_id' => 4,
            ),
            433 => 
            array (
                'id' => 1454,
                'company_id' => 94,
                'block_id' => 344,
                'school_academic_year_id' => 4,
            ),
            434 => 
            array (
                'id' => 1455,
                'company_id' => 94,
                'block_id' => 322,
                'school_academic_year_id' => 5,
            ),
            435 => 
            array (
                'id' => 1456,
                'company_id' => 94,
                'block_id' => 337,
                'school_academic_year_id' => 5,
            ),
            436 => 
            array (
                'id' => 1457,
                'company_id' => 94,
                'block_id' => 338,
                'school_academic_year_id' => 5,
            ),
            437 => 
            array (
                'id' => 1458,
                'company_id' => 94,
                'block_id' => 339,
                'school_academic_year_id' => 5,
            ),
            438 => 
            array (
                'id' => 1459,
                'company_id' => 94,
                'block_id' => 340,
                'school_academic_year_id' => 5,
            ),
            439 => 
            array (
                'id' => 1460,
                'company_id' => 94,
                'block_id' => 341,
                'school_academic_year_id' => 5,
            ),
            440 => 
            array (
                'id' => 1461,
                'company_id' => 94,
                'block_id' => 342,
                'school_academic_year_id' => 5,
            ),
            441 => 
            array (
                'id' => 1462,
                'company_id' => 94,
                'block_id' => 344,
                'school_academic_year_id' => 5,
            ),
            442 => 
            array (
                'id' => 1463,
                'company_id' => 94,
                'block_id' => 322,
                'school_academic_year_id' => 6,
            ),
            443 => 
            array (
                'id' => 1464,
                'company_id' => 94,
                'block_id' => 337,
                'school_academic_year_id' => 6,
            ),
            444 => 
            array (
                'id' => 1465,
                'company_id' => 94,
                'block_id' => 338,
                'school_academic_year_id' => 6,
            ),
            445 => 
            array (
                'id' => 1466,
                'company_id' => 94,
                'block_id' => 339,
                'school_academic_year_id' => 6,
            ),
            446 => 
            array (
                'id' => 1467,
                'company_id' => 94,
                'block_id' => 340,
                'school_academic_year_id' => 6,
            ),
            447 => 
            array (
                'id' => 1468,
                'company_id' => 94,
                'block_id' => 341,
                'school_academic_year_id' => 6,
            ),
            448 => 
            array (
                'id' => 1469,
                'company_id' => 94,
                'block_id' => 342,
                'school_academic_year_id' => 6,
            ),
            449 => 
            array (
                'id' => 1470,
                'company_id' => 94,
                'block_id' => 344,
                'school_academic_year_id' => 6,
            ),
            450 => 
            array (
                'id' => 1471,
                'company_id' => 95,
                'block_id' => 323,
                'school_academic_year_id' => 2,
            ),
            451 => 
            array (
                'id' => 1472,
                'company_id' => 95,
                'block_id' => 330,
                'school_academic_year_id' => 2,
            ),
            452 => 
            array (
                'id' => 1473,
                'company_id' => 95,
                'block_id' => 331,
                'school_academic_year_id' => 2,
            ),
            453 => 
            array (
                'id' => 1474,
                'company_id' => 95,
                'block_id' => 332,
                'school_academic_year_id' => 2,
            ),
            454 => 
            array (
                'id' => 1475,
                'company_id' => 95,
                'block_id' => 323,
                'school_academic_year_id' => 3,
            ),
            455 => 
            array (
                'id' => 1476,
                'company_id' => 95,
                'block_id' => 330,
                'school_academic_year_id' => 3,
            ),
            456 => 
            array (
                'id' => 1477,
                'company_id' => 95,
                'block_id' => 331,
                'school_academic_year_id' => 3,
            ),
            457 => 
            array (
                'id' => 1478,
                'company_id' => 95,
                'block_id' => 332,
                'school_academic_year_id' => 3,
            ),
            458 => 
            array (
                'id' => 1479,
                'company_id' => 95,
                'block_id' => 323,
                'school_academic_year_id' => 4,
            ),
            459 => 
            array (
                'id' => 1480,
                'company_id' => 95,
                'block_id' => 330,
                'school_academic_year_id' => 4,
            ),
            460 => 
            array (
                'id' => 1481,
                'company_id' => 95,
                'block_id' => 331,
                'school_academic_year_id' => 4,
            ),
            461 => 
            array (
                'id' => 1482,
                'company_id' => 95,
                'block_id' => 332,
                'school_academic_year_id' => 4,
            ),
            462 => 
            array (
                'id' => 1483,
                'company_id' => 95,
                'block_id' => 323,
                'school_academic_year_id' => 5,
            ),
            463 => 
            array (
                'id' => 1484,
                'company_id' => 95,
                'block_id' => 330,
                'school_academic_year_id' => 5,
            ),
            464 => 
            array (
                'id' => 1485,
                'company_id' => 95,
                'block_id' => 331,
                'school_academic_year_id' => 5,
            ),
            465 => 
            array (
                'id' => 1486,
                'company_id' => 95,
                'block_id' => 332,
                'school_academic_year_id' => 5,
            ),
            466 => 
            array (
                'id' => 1487,
                'company_id' => 95,
                'block_id' => 323,
                'school_academic_year_id' => 6,
            ),
            467 => 
            array (
                'id' => 1488,
                'company_id' => 95,
                'block_id' => 330,
                'school_academic_year_id' => 6,
            ),
            468 => 
            array (
                'id' => 1489,
                'company_id' => 95,
                'block_id' => 331,
                'school_academic_year_id' => 6,
            ),
            469 => 
            array (
                'id' => 1490,
                'company_id' => 95,
                'block_id' => 332,
                'school_academic_year_id' => 6,
            ),
            470 => 
            array (
                'id' => 1491,
                'company_id' => 96,
                'block_id' => 325,
                'school_academic_year_id' => 2,
            ),
            471 => 
            array (
                'id' => 1492,
                'company_id' => 96,
                'block_id' => 326,
                'school_academic_year_id' => 2,
            ),
            472 => 
            array (
                'id' => 1493,
                'company_id' => 96,
                'block_id' => 327,
                'school_academic_year_id' => 2,
            ),
            473 => 
            array (
                'id' => 1494,
                'company_id' => 96,
                'block_id' => 328,
                'school_academic_year_id' => 2,
            ),
            474 => 
            array (
                'id' => 1495,
                'company_id' => 96,
                'block_id' => 325,
                'school_academic_year_id' => 3,
            ),
            475 => 
            array (
                'id' => 1496,
                'company_id' => 96,
                'block_id' => 326,
                'school_academic_year_id' => 3,
            ),
            476 => 
            array (
                'id' => 1497,
                'company_id' => 96,
                'block_id' => 327,
                'school_academic_year_id' => 3,
            ),
            477 => 
            array (
                'id' => 1498,
                'company_id' => 96,
                'block_id' => 328,
                'school_academic_year_id' => 3,
            ),
            478 => 
            array (
                'id' => 1499,
                'company_id' => 96,
                'block_id' => 325,
                'school_academic_year_id' => 4,
            ),
            479 => 
            array (
                'id' => 1500,
                'company_id' => 96,
                'block_id' => 326,
                'school_academic_year_id' => 4,
            ),
            480 => 
            array (
                'id' => 1501,
                'company_id' => 96,
                'block_id' => 327,
                'school_academic_year_id' => 4,
            ),
            481 => 
            array (
                'id' => 1502,
                'company_id' => 96,
                'block_id' => 328,
                'school_academic_year_id' => 4,
            ),
            482 => 
            array (
                'id' => 1503,
                'company_id' => 96,
                'block_id' => 325,
                'school_academic_year_id' => 5,
            ),
            483 => 
            array (
                'id' => 1504,
                'company_id' => 96,
                'block_id' => 326,
                'school_academic_year_id' => 5,
            ),
            484 => 
            array (
                'id' => 1505,
                'company_id' => 96,
                'block_id' => 327,
                'school_academic_year_id' => 5,
            ),
            485 => 
            array (
                'id' => 1506,
                'company_id' => 96,
                'block_id' => 328,
                'school_academic_year_id' => 5,
            ),
            486 => 
            array (
                'id' => 1507,
                'company_id' => 96,
                'block_id' => 325,
                'school_academic_year_id' => 6,
            ),
            487 => 
            array (
                'id' => 1508,
                'company_id' => 96,
                'block_id' => 326,
                'school_academic_year_id' => 6,
            ),
            488 => 
            array (
                'id' => 1509,
                'company_id' => 96,
                'block_id' => 327,
                'school_academic_year_id' => 6,
            ),
            489 => 
            array (
                'id' => 1510,
                'company_id' => 96,
                'block_id' => 328,
                'school_academic_year_id' => 6,
            ),
            490 => 
            array (
                'id' => 1511,
                'company_id' => 97,
                'block_id' => 333,
                'school_academic_year_id' => 2,
            ),
            491 => 
            array (
                'id' => 1512,
                'company_id' => 97,
                'block_id' => 334,
                'school_academic_year_id' => 2,
            ),
            492 => 
            array (
                'id' => 1513,
                'company_id' => 97,
                'block_id' => 333,
                'school_academic_year_id' => 3,
            ),
            493 => 
            array (
                'id' => 1514,
                'company_id' => 97,
                'block_id' => 334,
                'school_academic_year_id' => 3,
            ),
            494 => 
            array (
                'id' => 1515,
                'company_id' => 97,
                'block_id' => 333,
                'school_academic_year_id' => 4,
            ),
            495 => 
            array (
                'id' => 1516,
                'company_id' => 97,
                'block_id' => 334,
                'school_academic_year_id' => 4,
            ),
            496 => 
            array (
                'id' => 1517,
                'company_id' => 97,
                'block_id' => 333,
                'school_academic_year_id' => 5,
            ),
            497 => 
            array (
                'id' => 1518,
                'company_id' => 97,
                'block_id' => 334,
                'school_academic_year_id' => 5,
            ),
            498 => 
            array (
                'id' => 1519,
                'company_id' => 97,
                'block_id' => 333,
                'school_academic_year_id' => 6,
            ),
            499 => 
            array (
                'id' => 1520,
                'company_id' => 97,
                'block_id' => 334,
                'school_academic_year_id' => 6,
            ),
        ));
        \DB::table('block_years')->insert(array (
            0 => 
            array (
                'id' => 1521,
                'company_id' => 98,
                'block_id' => 345,
                'school_academic_year_id' => 2,
            ),
            1 => 
            array (
                'id' => 1522,
                'company_id' => 98,
                'block_id' => 346,
                'school_academic_year_id' => 2,
            ),
            2 => 
            array (
                'id' => 1523,
                'company_id' => 98,
                'block_id' => 349,
                'school_academic_year_id' => 2,
            ),
            3 => 
            array (
                'id' => 1524,
                'company_id' => 98,
                'block_id' => 457,
                'school_academic_year_id' => 2,
            ),
            4 => 
            array (
                'id' => 1525,
                'company_id' => 98,
                'block_id' => 345,
                'school_academic_year_id' => 3,
            ),
            5 => 
            array (
                'id' => 1526,
                'company_id' => 98,
                'block_id' => 346,
                'school_academic_year_id' => 3,
            ),
            6 => 
            array (
                'id' => 1527,
                'company_id' => 98,
                'block_id' => 349,
                'school_academic_year_id' => 3,
            ),
            7 => 
            array (
                'id' => 1528,
                'company_id' => 98,
                'block_id' => 457,
                'school_academic_year_id' => 3,
            ),
            8 => 
            array (
                'id' => 1529,
                'company_id' => 98,
                'block_id' => 345,
                'school_academic_year_id' => 4,
            ),
            9 => 
            array (
                'id' => 1530,
                'company_id' => 98,
                'block_id' => 346,
                'school_academic_year_id' => 4,
            ),
            10 => 
            array (
                'id' => 1531,
                'company_id' => 98,
                'block_id' => 349,
                'school_academic_year_id' => 4,
            ),
            11 => 
            array (
                'id' => 1532,
                'company_id' => 98,
                'block_id' => 457,
                'school_academic_year_id' => 4,
            ),
            12 => 
            array (
                'id' => 1533,
                'company_id' => 98,
                'block_id' => 345,
                'school_academic_year_id' => 5,
            ),
            13 => 
            array (
                'id' => 1534,
                'company_id' => 98,
                'block_id' => 346,
                'school_academic_year_id' => 5,
            ),
            14 => 
            array (
                'id' => 1535,
                'company_id' => 98,
                'block_id' => 349,
                'school_academic_year_id' => 5,
            ),
            15 => 
            array (
                'id' => 1536,
                'company_id' => 98,
                'block_id' => 457,
                'school_academic_year_id' => 5,
            ),
            16 => 
            array (
                'id' => 1537,
                'company_id' => 98,
                'block_id' => 345,
                'school_academic_year_id' => 6,
            ),
            17 => 
            array (
                'id' => 1538,
                'company_id' => 98,
                'block_id' => 346,
                'school_academic_year_id' => 6,
            ),
            18 => 
            array (
                'id' => 1539,
                'company_id' => 98,
                'block_id' => 349,
                'school_academic_year_id' => 6,
            ),
            19 => 
            array (
                'id' => 1540,
                'company_id' => 98,
                'block_id' => 457,
                'school_academic_year_id' => 6,
            ),
            20 => 
            array (
                'id' => 1541,
                'company_id' => 99,
                'block_id' => 350,
                'school_academic_year_id' => 2,
            ),
            21 => 
            array (
                'id' => 1542,
                'company_id' => 99,
                'block_id' => 350,
                'school_academic_year_id' => 3,
            ),
            22 => 
            array (
                'id' => 1543,
                'company_id' => 99,
                'block_id' => 350,
                'school_academic_year_id' => 4,
            ),
            23 => 
            array (
                'id' => 1544,
                'company_id' => 99,
                'block_id' => 350,
                'school_academic_year_id' => 5,
            ),
            24 => 
            array (
                'id' => 1545,
                'company_id' => 99,
                'block_id' => 350,
                'school_academic_year_id' => 6,
            ),
            25 => 
            array (
                'id' => 1546,
                'company_id' => 100,
                'block_id' => 351,
                'school_academic_year_id' => 2,
            ),
            26 => 
            array (
                'id' => 1547,
                'company_id' => 100,
                'block_id' => 351,
                'school_academic_year_id' => 3,
            ),
            27 => 
            array (
                'id' => 1548,
                'company_id' => 100,
                'block_id' => 351,
                'school_academic_year_id' => 4,
            ),
            28 => 
            array (
                'id' => 1549,
                'company_id' => 100,
                'block_id' => 351,
                'school_academic_year_id' => 5,
            ),
            29 => 
            array (
                'id' => 1550,
                'company_id' => 100,
                'block_id' => 351,
                'school_academic_year_id' => 6,
            ),
            30 => 
            array (
                'id' => 1551,
                'company_id' => 101,
                'block_id' => 352,
                'school_academic_year_id' => 2,
            ),
            31 => 
            array (
                'id' => 1552,
                'company_id' => 101,
                'block_id' => 353,
                'school_academic_year_id' => 2,
            ),
            32 => 
            array (
                'id' => 1553,
                'company_id' => 101,
                'block_id' => 354,
                'school_academic_year_id' => 2,
            ),
            33 => 
            array (
                'id' => 1554,
                'company_id' => 101,
                'block_id' => 355,
                'school_academic_year_id' => 2,
            ),
            34 => 
            array (
                'id' => 1555,
                'company_id' => 101,
                'block_id' => 368,
                'school_academic_year_id' => 2,
            ),
            35 => 
            array (
                'id' => 1556,
                'company_id' => 101,
                'block_id' => 352,
                'school_academic_year_id' => 3,
            ),
            36 => 
            array (
                'id' => 1557,
                'company_id' => 101,
                'block_id' => 353,
                'school_academic_year_id' => 3,
            ),
            37 => 
            array (
                'id' => 1558,
                'company_id' => 101,
                'block_id' => 354,
                'school_academic_year_id' => 3,
            ),
            38 => 
            array (
                'id' => 1559,
                'company_id' => 101,
                'block_id' => 355,
                'school_academic_year_id' => 3,
            ),
            39 => 
            array (
                'id' => 1560,
                'company_id' => 101,
                'block_id' => 368,
                'school_academic_year_id' => 3,
            ),
            40 => 
            array (
                'id' => 1561,
                'company_id' => 101,
                'block_id' => 352,
                'school_academic_year_id' => 4,
            ),
            41 => 
            array (
                'id' => 1562,
                'company_id' => 101,
                'block_id' => 353,
                'school_academic_year_id' => 4,
            ),
            42 => 
            array (
                'id' => 1563,
                'company_id' => 101,
                'block_id' => 354,
                'school_academic_year_id' => 4,
            ),
            43 => 
            array (
                'id' => 1564,
                'company_id' => 101,
                'block_id' => 355,
                'school_academic_year_id' => 4,
            ),
            44 => 
            array (
                'id' => 1565,
                'company_id' => 101,
                'block_id' => 368,
                'school_academic_year_id' => 4,
            ),
            45 => 
            array (
                'id' => 1566,
                'company_id' => 101,
                'block_id' => 352,
                'school_academic_year_id' => 5,
            ),
            46 => 
            array (
                'id' => 1567,
                'company_id' => 101,
                'block_id' => 353,
                'school_academic_year_id' => 5,
            ),
            47 => 
            array (
                'id' => 1568,
                'company_id' => 101,
                'block_id' => 354,
                'school_academic_year_id' => 5,
            ),
            48 => 
            array (
                'id' => 1569,
                'company_id' => 101,
                'block_id' => 355,
                'school_academic_year_id' => 5,
            ),
            49 => 
            array (
                'id' => 1570,
                'company_id' => 101,
                'block_id' => 368,
                'school_academic_year_id' => 5,
            ),
            50 => 
            array (
                'id' => 1571,
                'company_id' => 101,
                'block_id' => 352,
                'school_academic_year_id' => 6,
            ),
            51 => 
            array (
                'id' => 1572,
                'company_id' => 101,
                'block_id' => 353,
                'school_academic_year_id' => 6,
            ),
            52 => 
            array (
                'id' => 1573,
                'company_id' => 101,
                'block_id' => 354,
                'school_academic_year_id' => 6,
            ),
            53 => 
            array (
                'id' => 1574,
                'company_id' => 101,
                'block_id' => 355,
                'school_academic_year_id' => 6,
            ),
            54 => 
            array (
                'id' => 1575,
                'company_id' => 101,
                'block_id' => 368,
                'school_academic_year_id' => 6,
            ),
            55 => 
            array (
                'id' => 1576,
                'company_id' => 102,
                'block_id' => 356,
                'school_academic_year_id' => 2,
            ),
            56 => 
            array (
                'id' => 1577,
                'company_id' => 102,
                'block_id' => 369,
                'school_academic_year_id' => 2,
            ),
            57 => 
            array (
                'id' => 1578,
                'company_id' => 102,
                'block_id' => 356,
                'school_academic_year_id' => 3,
            ),
            58 => 
            array (
                'id' => 1579,
                'company_id' => 102,
                'block_id' => 369,
                'school_academic_year_id' => 3,
            ),
            59 => 
            array (
                'id' => 1580,
                'company_id' => 102,
                'block_id' => 356,
                'school_academic_year_id' => 4,
            ),
            60 => 
            array (
                'id' => 1581,
                'company_id' => 102,
                'block_id' => 369,
                'school_academic_year_id' => 4,
            ),
            61 => 
            array (
                'id' => 1582,
                'company_id' => 102,
                'block_id' => 356,
                'school_academic_year_id' => 5,
            ),
            62 => 
            array (
                'id' => 1583,
                'company_id' => 102,
                'block_id' => 369,
                'school_academic_year_id' => 5,
            ),
            63 => 
            array (
                'id' => 1584,
                'company_id' => 102,
                'block_id' => 356,
                'school_academic_year_id' => 6,
            ),
            64 => 
            array (
                'id' => 1585,
                'company_id' => 102,
                'block_id' => 369,
                'school_academic_year_id' => 6,
            ),
            65 => 
            array (
                'id' => 1586,
                'company_id' => 103,
                'block_id' => 357,
                'school_academic_year_id' => 2,
            ),
            66 => 
            array (
                'id' => 1587,
                'company_id' => 103,
                'block_id' => 357,
                'school_academic_year_id' => 3,
            ),
            67 => 
            array (
                'id' => 1588,
                'company_id' => 103,
                'block_id' => 357,
                'school_academic_year_id' => 4,
            ),
            68 => 
            array (
                'id' => 1589,
                'company_id' => 103,
                'block_id' => 357,
                'school_academic_year_id' => 5,
            ),
            69 => 
            array (
                'id' => 1590,
                'company_id' => 103,
                'block_id' => 357,
                'school_academic_year_id' => 6,
            ),
            70 => 
            array (
                'id' => 1591,
                'company_id' => 104,
                'block_id' => 358,
                'school_academic_year_id' => 2,
            ),
            71 => 
            array (
                'id' => 1592,
                'company_id' => 104,
                'block_id' => 359,
                'school_academic_year_id' => 2,
            ),
            72 => 
            array (
                'id' => 1593,
                'company_id' => 104,
                'block_id' => 360,
                'school_academic_year_id' => 2,
            ),
            73 => 
            array (
                'id' => 1594,
                'company_id' => 104,
                'block_id' => 361,
                'school_academic_year_id' => 2,
            ),
            74 => 
            array (
                'id' => 1595,
                'company_id' => 104,
                'block_id' => 362,
                'school_academic_year_id' => 2,
            ),
            75 => 
            array (
                'id' => 1596,
                'company_id' => 104,
                'block_id' => 358,
                'school_academic_year_id' => 3,
            ),
            76 => 
            array (
                'id' => 1597,
                'company_id' => 104,
                'block_id' => 359,
                'school_academic_year_id' => 3,
            ),
            77 => 
            array (
                'id' => 1598,
                'company_id' => 104,
                'block_id' => 360,
                'school_academic_year_id' => 3,
            ),
            78 => 
            array (
                'id' => 1599,
                'company_id' => 104,
                'block_id' => 361,
                'school_academic_year_id' => 3,
            ),
            79 => 
            array (
                'id' => 1600,
                'company_id' => 104,
                'block_id' => 362,
                'school_academic_year_id' => 3,
            ),
            80 => 
            array (
                'id' => 1601,
                'company_id' => 104,
                'block_id' => 358,
                'school_academic_year_id' => 4,
            ),
            81 => 
            array (
                'id' => 1602,
                'company_id' => 104,
                'block_id' => 359,
                'school_academic_year_id' => 4,
            ),
            82 => 
            array (
                'id' => 1603,
                'company_id' => 104,
                'block_id' => 360,
                'school_academic_year_id' => 4,
            ),
            83 => 
            array (
                'id' => 1604,
                'company_id' => 104,
                'block_id' => 361,
                'school_academic_year_id' => 4,
            ),
            84 => 
            array (
                'id' => 1605,
                'company_id' => 104,
                'block_id' => 362,
                'school_academic_year_id' => 4,
            ),
            85 => 
            array (
                'id' => 1606,
                'company_id' => 104,
                'block_id' => 358,
                'school_academic_year_id' => 5,
            ),
            86 => 
            array (
                'id' => 1607,
                'company_id' => 104,
                'block_id' => 359,
                'school_academic_year_id' => 5,
            ),
            87 => 
            array (
                'id' => 1608,
                'company_id' => 104,
                'block_id' => 360,
                'school_academic_year_id' => 5,
            ),
            88 => 
            array (
                'id' => 1609,
                'company_id' => 104,
                'block_id' => 361,
                'school_academic_year_id' => 5,
            ),
            89 => 
            array (
                'id' => 1610,
                'company_id' => 104,
                'block_id' => 362,
                'school_academic_year_id' => 5,
            ),
            90 => 
            array (
                'id' => 1611,
                'company_id' => 104,
                'block_id' => 358,
                'school_academic_year_id' => 6,
            ),
            91 => 
            array (
                'id' => 1612,
                'company_id' => 104,
                'block_id' => 359,
                'school_academic_year_id' => 6,
            ),
            92 => 
            array (
                'id' => 1613,
                'company_id' => 104,
                'block_id' => 360,
                'school_academic_year_id' => 6,
            ),
            93 => 
            array (
                'id' => 1614,
                'company_id' => 104,
                'block_id' => 361,
                'school_academic_year_id' => 6,
            ),
            94 => 
            array (
                'id' => 1615,
                'company_id' => 104,
                'block_id' => 362,
                'school_academic_year_id' => 6,
            ),
            95 => 
            array (
                'id' => 1616,
                'company_id' => 105,
                'block_id' => 363,
                'school_academic_year_id' => 2,
            ),
            96 => 
            array (
                'id' => 1617,
                'company_id' => 105,
                'block_id' => 363,
                'school_academic_year_id' => 3,
            ),
            97 => 
            array (
                'id' => 1618,
                'company_id' => 105,
                'block_id' => 363,
                'school_academic_year_id' => 4,
            ),
            98 => 
            array (
                'id' => 1619,
                'company_id' => 105,
                'block_id' => 363,
                'school_academic_year_id' => 5,
            ),
            99 => 
            array (
                'id' => 1620,
                'company_id' => 105,
                'block_id' => 363,
                'school_academic_year_id' => 6,
            ),
            100 => 
            array (
                'id' => 1621,
                'company_id' => 106,
                'block_id' => 364,
                'school_academic_year_id' => 2,
            ),
            101 => 
            array (
                'id' => 1622,
                'company_id' => 106,
                'block_id' => 365,
                'school_academic_year_id' => 2,
            ),
            102 => 
            array (
                'id' => 1623,
                'company_id' => 106,
                'block_id' => 366,
                'school_academic_year_id' => 2,
            ),
            103 => 
            array (
                'id' => 1624,
                'company_id' => 106,
                'block_id' => 367,
                'school_academic_year_id' => 2,
            ),
            104 => 
            array (
                'id' => 1625,
                'company_id' => 106,
                'block_id' => 364,
                'school_academic_year_id' => 3,
            ),
            105 => 
            array (
                'id' => 1626,
                'company_id' => 106,
                'block_id' => 365,
                'school_academic_year_id' => 3,
            ),
            106 => 
            array (
                'id' => 1627,
                'company_id' => 106,
                'block_id' => 366,
                'school_academic_year_id' => 3,
            ),
            107 => 
            array (
                'id' => 1628,
                'company_id' => 106,
                'block_id' => 367,
                'school_academic_year_id' => 3,
            ),
            108 => 
            array (
                'id' => 1629,
                'company_id' => 106,
                'block_id' => 364,
                'school_academic_year_id' => 4,
            ),
            109 => 
            array (
                'id' => 1630,
                'company_id' => 106,
                'block_id' => 365,
                'school_academic_year_id' => 4,
            ),
            110 => 
            array (
                'id' => 1631,
                'company_id' => 106,
                'block_id' => 366,
                'school_academic_year_id' => 4,
            ),
            111 => 
            array (
                'id' => 1632,
                'company_id' => 106,
                'block_id' => 367,
                'school_academic_year_id' => 4,
            ),
            112 => 
            array (
                'id' => 1633,
                'company_id' => 106,
                'block_id' => 364,
                'school_academic_year_id' => 5,
            ),
            113 => 
            array (
                'id' => 1634,
                'company_id' => 106,
                'block_id' => 365,
                'school_academic_year_id' => 5,
            ),
            114 => 
            array (
                'id' => 1635,
                'company_id' => 106,
                'block_id' => 366,
                'school_academic_year_id' => 5,
            ),
            115 => 
            array (
                'id' => 1636,
                'company_id' => 106,
                'block_id' => 367,
                'school_academic_year_id' => 5,
            ),
            116 => 
            array (
                'id' => 1637,
                'company_id' => 106,
                'block_id' => 364,
                'school_academic_year_id' => 6,
            ),
            117 => 
            array (
                'id' => 1638,
                'company_id' => 106,
                'block_id' => 365,
                'school_academic_year_id' => 6,
            ),
            118 => 
            array (
                'id' => 1639,
                'company_id' => 106,
                'block_id' => 366,
                'school_academic_year_id' => 6,
            ),
            119 => 
            array (
                'id' => 1640,
                'company_id' => 106,
                'block_id' => 367,
                'school_academic_year_id' => 6,
            ),
            120 => 
            array (
                'id' => 1641,
                'company_id' => 107,
                'block_id' => 372,
                'school_academic_year_id' => 2,
            ),
            121 => 
            array (
                'id' => 1642,
                'company_id' => 107,
                'block_id' => 373,
                'school_academic_year_id' => 2,
            ),
            122 => 
            array (
                'id' => 1643,
                'company_id' => 107,
                'block_id' => 372,
                'school_academic_year_id' => 3,
            ),
            123 => 
            array (
                'id' => 1644,
                'company_id' => 107,
                'block_id' => 373,
                'school_academic_year_id' => 3,
            ),
            124 => 
            array (
                'id' => 1645,
                'company_id' => 107,
                'block_id' => 372,
                'school_academic_year_id' => 4,
            ),
            125 => 
            array (
                'id' => 1646,
                'company_id' => 107,
                'block_id' => 373,
                'school_academic_year_id' => 4,
            ),
            126 => 
            array (
                'id' => 1647,
                'company_id' => 107,
                'block_id' => 372,
                'school_academic_year_id' => 5,
            ),
            127 => 
            array (
                'id' => 1648,
                'company_id' => 107,
                'block_id' => 373,
                'school_academic_year_id' => 5,
            ),
            128 => 
            array (
                'id' => 1649,
                'company_id' => 107,
                'block_id' => 372,
                'school_academic_year_id' => 6,
            ),
            129 => 
            array (
                'id' => 1650,
                'company_id' => 107,
                'block_id' => 373,
                'school_academic_year_id' => 6,
            ),
            130 => 
            array (
                'id' => 1651,
                'company_id' => 108,
                'block_id' => 374,
                'school_academic_year_id' => 2,
            ),
            131 => 
            array (
                'id' => 1652,
                'company_id' => 108,
                'block_id' => 375,
                'school_academic_year_id' => 2,
            ),
            132 => 
            array (
                'id' => 1653,
                'company_id' => 108,
                'block_id' => 374,
                'school_academic_year_id' => 3,
            ),
            133 => 
            array (
                'id' => 1654,
                'company_id' => 108,
                'block_id' => 375,
                'school_academic_year_id' => 3,
            ),
            134 => 
            array (
                'id' => 1655,
                'company_id' => 108,
                'block_id' => 374,
                'school_academic_year_id' => 4,
            ),
            135 => 
            array (
                'id' => 1656,
                'company_id' => 108,
                'block_id' => 375,
                'school_academic_year_id' => 4,
            ),
            136 => 
            array (
                'id' => 1657,
                'company_id' => 108,
                'block_id' => 374,
                'school_academic_year_id' => 5,
            ),
            137 => 
            array (
                'id' => 1658,
                'company_id' => 108,
                'block_id' => 375,
                'school_academic_year_id' => 5,
            ),
            138 => 
            array (
                'id' => 1659,
                'company_id' => 108,
                'block_id' => 374,
                'school_academic_year_id' => 6,
            ),
            139 => 
            array (
                'id' => 1660,
                'company_id' => 108,
                'block_id' => 375,
                'school_academic_year_id' => 6,
            ),
            140 => 
            array (
                'id' => 1661,
                'company_id' => 109,
                'block_id' => 376,
                'school_academic_year_id' => 2,
            ),
            141 => 
            array (
                'id' => 1662,
                'company_id' => 109,
                'block_id' => 377,
                'school_academic_year_id' => 2,
            ),
            142 => 
            array (
                'id' => 1663,
                'company_id' => 109,
                'block_id' => 378,
                'school_academic_year_id' => 2,
            ),
            143 => 
            array (
                'id' => 1664,
                'company_id' => 109,
                'block_id' => 379,
                'school_academic_year_id' => 2,
            ),
            144 => 
            array (
                'id' => 1665,
                'company_id' => 109,
                'block_id' => 380,
                'school_academic_year_id' => 2,
            ),
            145 => 
            array (
                'id' => 1666,
                'company_id' => 109,
                'block_id' => 376,
                'school_academic_year_id' => 3,
            ),
            146 => 
            array (
                'id' => 1667,
                'company_id' => 109,
                'block_id' => 377,
                'school_academic_year_id' => 3,
            ),
            147 => 
            array (
                'id' => 1668,
                'company_id' => 109,
                'block_id' => 378,
                'school_academic_year_id' => 3,
            ),
            148 => 
            array (
                'id' => 1669,
                'company_id' => 109,
                'block_id' => 379,
                'school_academic_year_id' => 3,
            ),
            149 => 
            array (
                'id' => 1670,
                'company_id' => 109,
                'block_id' => 380,
                'school_academic_year_id' => 3,
            ),
            150 => 
            array (
                'id' => 1671,
                'company_id' => 109,
                'block_id' => 376,
                'school_academic_year_id' => 4,
            ),
            151 => 
            array (
                'id' => 1672,
                'company_id' => 109,
                'block_id' => 377,
                'school_academic_year_id' => 4,
            ),
            152 => 
            array (
                'id' => 1673,
                'company_id' => 109,
                'block_id' => 378,
                'school_academic_year_id' => 4,
            ),
            153 => 
            array (
                'id' => 1674,
                'company_id' => 109,
                'block_id' => 379,
                'school_academic_year_id' => 4,
            ),
            154 => 
            array (
                'id' => 1675,
                'company_id' => 109,
                'block_id' => 380,
                'school_academic_year_id' => 4,
            ),
            155 => 
            array (
                'id' => 1676,
                'company_id' => 109,
                'block_id' => 376,
                'school_academic_year_id' => 5,
            ),
            156 => 
            array (
                'id' => 1677,
                'company_id' => 109,
                'block_id' => 377,
                'school_academic_year_id' => 5,
            ),
            157 => 
            array (
                'id' => 1678,
                'company_id' => 109,
                'block_id' => 378,
                'school_academic_year_id' => 5,
            ),
            158 => 
            array (
                'id' => 1679,
                'company_id' => 109,
                'block_id' => 379,
                'school_academic_year_id' => 5,
            ),
            159 => 
            array (
                'id' => 1680,
                'company_id' => 109,
                'block_id' => 380,
                'school_academic_year_id' => 5,
            ),
            160 => 
            array (
                'id' => 1681,
                'company_id' => 109,
                'block_id' => 376,
                'school_academic_year_id' => 6,
            ),
            161 => 
            array (
                'id' => 1682,
                'company_id' => 109,
                'block_id' => 377,
                'school_academic_year_id' => 6,
            ),
            162 => 
            array (
                'id' => 1683,
                'company_id' => 109,
                'block_id' => 378,
                'school_academic_year_id' => 6,
            ),
            163 => 
            array (
                'id' => 1684,
                'company_id' => 109,
                'block_id' => 379,
                'school_academic_year_id' => 6,
            ),
            164 => 
            array (
                'id' => 1685,
                'company_id' => 109,
                'block_id' => 380,
                'school_academic_year_id' => 6,
            ),
            165 => 
            array (
                'id' => 1686,
                'company_id' => 110,
                'block_id' => 383,
                'school_academic_year_id' => 2,
            ),
            166 => 
            array (
                'id' => 1687,
                'company_id' => 110,
                'block_id' => 384,
                'school_academic_year_id' => 2,
            ),
            167 => 
            array (
                'id' => 1688,
                'company_id' => 110,
                'block_id' => 385,
                'school_academic_year_id' => 2,
            ),
            168 => 
            array (
                'id' => 1689,
                'company_id' => 110,
                'block_id' => 383,
                'school_academic_year_id' => 3,
            ),
            169 => 
            array (
                'id' => 1690,
                'company_id' => 110,
                'block_id' => 384,
                'school_academic_year_id' => 3,
            ),
            170 => 
            array (
                'id' => 1691,
                'company_id' => 110,
                'block_id' => 385,
                'school_academic_year_id' => 3,
            ),
            171 => 
            array (
                'id' => 1692,
                'company_id' => 110,
                'block_id' => 383,
                'school_academic_year_id' => 4,
            ),
            172 => 
            array (
                'id' => 1693,
                'company_id' => 110,
                'block_id' => 384,
                'school_academic_year_id' => 4,
            ),
            173 => 
            array (
                'id' => 1694,
                'company_id' => 110,
                'block_id' => 385,
                'school_academic_year_id' => 4,
            ),
            174 => 
            array (
                'id' => 1695,
                'company_id' => 110,
                'block_id' => 383,
                'school_academic_year_id' => 5,
            ),
            175 => 
            array (
                'id' => 1696,
                'company_id' => 110,
                'block_id' => 384,
                'school_academic_year_id' => 5,
            ),
            176 => 
            array (
                'id' => 1697,
                'company_id' => 110,
                'block_id' => 385,
                'school_academic_year_id' => 5,
            ),
            177 => 
            array (
                'id' => 1698,
                'company_id' => 110,
                'block_id' => 383,
                'school_academic_year_id' => 6,
            ),
            178 => 
            array (
                'id' => 1699,
                'company_id' => 110,
                'block_id' => 384,
                'school_academic_year_id' => 6,
            ),
            179 => 
            array (
                'id' => 1700,
                'company_id' => 110,
                'block_id' => 385,
                'school_academic_year_id' => 6,
            ),
            180 => 
            array (
                'id' => 1701,
                'company_id' => 111,
                'block_id' => 387,
                'school_academic_year_id' => 2,
            ),
            181 => 
            array (
                'id' => 1702,
                'company_id' => 111,
                'block_id' => 388,
                'school_academic_year_id' => 2,
            ),
            182 => 
            array (
                'id' => 1703,
                'company_id' => 111,
                'block_id' => 389,
                'school_academic_year_id' => 2,
            ),
            183 => 
            array (
                'id' => 1704,
                'company_id' => 111,
                'block_id' => 390,
                'school_academic_year_id' => 2,
            ),
            184 => 
            array (
                'id' => 1705,
                'company_id' => 111,
                'block_id' => 391,
                'school_academic_year_id' => 2,
            ),
            185 => 
            array (
                'id' => 1706,
                'company_id' => 111,
                'block_id' => 392,
                'school_academic_year_id' => 2,
            ),
            186 => 
            array (
                'id' => 1707,
                'company_id' => 111,
                'block_id' => 393,
                'school_academic_year_id' => 2,
            ),
            187 => 
            array (
                'id' => 1708,
                'company_id' => 111,
                'block_id' => 387,
                'school_academic_year_id' => 3,
            ),
            188 => 
            array (
                'id' => 1709,
                'company_id' => 111,
                'block_id' => 388,
                'school_academic_year_id' => 3,
            ),
            189 => 
            array (
                'id' => 1710,
                'company_id' => 111,
                'block_id' => 389,
                'school_academic_year_id' => 3,
            ),
            190 => 
            array (
                'id' => 1711,
                'company_id' => 111,
                'block_id' => 390,
                'school_academic_year_id' => 3,
            ),
            191 => 
            array (
                'id' => 1712,
                'company_id' => 111,
                'block_id' => 391,
                'school_academic_year_id' => 3,
            ),
            192 => 
            array (
                'id' => 1713,
                'company_id' => 111,
                'block_id' => 392,
                'school_academic_year_id' => 3,
            ),
            193 => 
            array (
                'id' => 1714,
                'company_id' => 111,
                'block_id' => 393,
                'school_academic_year_id' => 3,
            ),
            194 => 
            array (
                'id' => 1715,
                'company_id' => 111,
                'block_id' => 387,
                'school_academic_year_id' => 4,
            ),
            195 => 
            array (
                'id' => 1716,
                'company_id' => 111,
                'block_id' => 388,
                'school_academic_year_id' => 4,
            ),
            196 => 
            array (
                'id' => 1717,
                'company_id' => 111,
                'block_id' => 389,
                'school_academic_year_id' => 4,
            ),
            197 => 
            array (
                'id' => 1718,
                'company_id' => 111,
                'block_id' => 390,
                'school_academic_year_id' => 4,
            ),
            198 => 
            array (
                'id' => 1719,
                'company_id' => 111,
                'block_id' => 391,
                'school_academic_year_id' => 4,
            ),
            199 => 
            array (
                'id' => 1720,
                'company_id' => 111,
                'block_id' => 392,
                'school_academic_year_id' => 4,
            ),
            200 => 
            array (
                'id' => 1721,
                'company_id' => 111,
                'block_id' => 393,
                'school_academic_year_id' => 4,
            ),
            201 => 
            array (
                'id' => 1722,
                'company_id' => 111,
                'block_id' => 387,
                'school_academic_year_id' => 5,
            ),
            202 => 
            array (
                'id' => 1723,
                'company_id' => 111,
                'block_id' => 388,
                'school_academic_year_id' => 5,
            ),
            203 => 
            array (
                'id' => 1724,
                'company_id' => 111,
                'block_id' => 389,
                'school_academic_year_id' => 5,
            ),
            204 => 
            array (
                'id' => 1725,
                'company_id' => 111,
                'block_id' => 390,
                'school_academic_year_id' => 5,
            ),
            205 => 
            array (
                'id' => 1726,
                'company_id' => 111,
                'block_id' => 391,
                'school_academic_year_id' => 5,
            ),
            206 => 
            array (
                'id' => 1727,
                'company_id' => 111,
                'block_id' => 392,
                'school_academic_year_id' => 5,
            ),
            207 => 
            array (
                'id' => 1728,
                'company_id' => 111,
                'block_id' => 393,
                'school_academic_year_id' => 5,
            ),
            208 => 
            array (
                'id' => 1729,
                'company_id' => 111,
                'block_id' => 387,
                'school_academic_year_id' => 6,
            ),
            209 => 
            array (
                'id' => 1730,
                'company_id' => 111,
                'block_id' => 388,
                'school_academic_year_id' => 6,
            ),
            210 => 
            array (
                'id' => 1731,
                'company_id' => 111,
                'block_id' => 389,
                'school_academic_year_id' => 6,
            ),
            211 => 
            array (
                'id' => 1732,
                'company_id' => 111,
                'block_id' => 390,
                'school_academic_year_id' => 6,
            ),
            212 => 
            array (
                'id' => 1733,
                'company_id' => 111,
                'block_id' => 391,
                'school_academic_year_id' => 6,
            ),
            213 => 
            array (
                'id' => 1734,
                'company_id' => 111,
                'block_id' => 392,
                'school_academic_year_id' => 6,
            ),
            214 => 
            array (
                'id' => 1735,
                'company_id' => 111,
                'block_id' => 393,
                'school_academic_year_id' => 6,
            ),
            215 => 
            array (
                'id' => 1736,
                'company_id' => 112,
                'block_id' => 394,
                'school_academic_year_id' => 2,
            ),
            216 => 
            array (
                'id' => 1737,
                'company_id' => 112,
                'block_id' => 395,
                'school_academic_year_id' => 2,
            ),
            217 => 
            array (
                'id' => 1738,
                'company_id' => 112,
                'block_id' => 396,
                'school_academic_year_id' => 2,
            ),
            218 => 
            array (
                'id' => 1739,
                'company_id' => 112,
                'block_id' => 405,
                'school_academic_year_id' => 2,
            ),
            219 => 
            array (
                'id' => 1740,
                'company_id' => 112,
                'block_id' => 406,
                'school_academic_year_id' => 2,
            ),
            220 => 
            array (
                'id' => 1741,
                'company_id' => 112,
                'block_id' => 410,
                'school_academic_year_id' => 2,
            ),
            221 => 
            array (
                'id' => 1742,
                'company_id' => 112,
                'block_id' => 440,
                'school_academic_year_id' => 2,
            ),
            222 => 
            array (
                'id' => 1743,
                'company_id' => 112,
                'block_id' => 394,
                'school_academic_year_id' => 3,
            ),
            223 => 
            array (
                'id' => 1744,
                'company_id' => 112,
                'block_id' => 395,
                'school_academic_year_id' => 3,
            ),
            224 => 
            array (
                'id' => 1745,
                'company_id' => 112,
                'block_id' => 396,
                'school_academic_year_id' => 3,
            ),
            225 => 
            array (
                'id' => 1746,
                'company_id' => 112,
                'block_id' => 405,
                'school_academic_year_id' => 3,
            ),
            226 => 
            array (
                'id' => 1747,
                'company_id' => 112,
                'block_id' => 406,
                'school_academic_year_id' => 3,
            ),
            227 => 
            array (
                'id' => 1748,
                'company_id' => 112,
                'block_id' => 410,
                'school_academic_year_id' => 3,
            ),
            228 => 
            array (
                'id' => 1749,
                'company_id' => 112,
                'block_id' => 440,
                'school_academic_year_id' => 3,
            ),
            229 => 
            array (
                'id' => 1750,
                'company_id' => 112,
                'block_id' => 394,
                'school_academic_year_id' => 4,
            ),
            230 => 
            array (
                'id' => 1751,
                'company_id' => 112,
                'block_id' => 395,
                'school_academic_year_id' => 4,
            ),
            231 => 
            array (
                'id' => 1752,
                'company_id' => 112,
                'block_id' => 396,
                'school_academic_year_id' => 4,
            ),
            232 => 
            array (
                'id' => 1753,
                'company_id' => 112,
                'block_id' => 405,
                'school_academic_year_id' => 4,
            ),
            233 => 
            array (
                'id' => 1754,
                'company_id' => 112,
                'block_id' => 406,
                'school_academic_year_id' => 4,
            ),
            234 => 
            array (
                'id' => 1755,
                'company_id' => 112,
                'block_id' => 410,
                'school_academic_year_id' => 4,
            ),
            235 => 
            array (
                'id' => 1756,
                'company_id' => 112,
                'block_id' => 440,
                'school_academic_year_id' => 4,
            ),
            236 => 
            array (
                'id' => 1757,
                'company_id' => 112,
                'block_id' => 394,
                'school_academic_year_id' => 5,
            ),
            237 => 
            array (
                'id' => 1758,
                'company_id' => 112,
                'block_id' => 395,
                'school_academic_year_id' => 5,
            ),
            238 => 
            array (
                'id' => 1759,
                'company_id' => 112,
                'block_id' => 396,
                'school_academic_year_id' => 5,
            ),
            239 => 
            array (
                'id' => 1760,
                'company_id' => 112,
                'block_id' => 405,
                'school_academic_year_id' => 5,
            ),
            240 => 
            array (
                'id' => 1761,
                'company_id' => 112,
                'block_id' => 406,
                'school_academic_year_id' => 5,
            ),
            241 => 
            array (
                'id' => 1762,
                'company_id' => 112,
                'block_id' => 410,
                'school_academic_year_id' => 5,
            ),
            242 => 
            array (
                'id' => 1763,
                'company_id' => 112,
                'block_id' => 440,
                'school_academic_year_id' => 5,
            ),
            243 => 
            array (
                'id' => 1764,
                'company_id' => 112,
                'block_id' => 394,
                'school_academic_year_id' => 6,
            ),
            244 => 
            array (
                'id' => 1765,
                'company_id' => 112,
                'block_id' => 395,
                'school_academic_year_id' => 6,
            ),
            245 => 
            array (
                'id' => 1766,
                'company_id' => 112,
                'block_id' => 396,
                'school_academic_year_id' => 6,
            ),
            246 => 
            array (
                'id' => 1767,
                'company_id' => 112,
                'block_id' => 405,
                'school_academic_year_id' => 6,
            ),
            247 => 
            array (
                'id' => 1768,
                'company_id' => 112,
                'block_id' => 406,
                'school_academic_year_id' => 6,
            ),
            248 => 
            array (
                'id' => 1769,
                'company_id' => 112,
                'block_id' => 410,
                'school_academic_year_id' => 6,
            ),
            249 => 
            array (
                'id' => 1770,
                'company_id' => 112,
                'block_id' => 440,
                'school_academic_year_id' => 6,
            ),
            250 => 
            array (
                'id' => 1771,
                'company_id' => 113,
                'block_id' => 413,
                'school_academic_year_id' => 2,
            ),
            251 => 
            array (
                'id' => 1772,
                'company_id' => 113,
                'block_id' => 414,
                'school_academic_year_id' => 2,
            ),
            252 => 
            array (
                'id' => 1773,
                'company_id' => 113,
                'block_id' => 415,
                'school_academic_year_id' => 2,
            ),
            253 => 
            array (
                'id' => 1774,
                'company_id' => 113,
                'block_id' => 416,
                'school_academic_year_id' => 2,
            ),
            254 => 
            array (
                'id' => 1775,
                'company_id' => 113,
                'block_id' => 413,
                'school_academic_year_id' => 3,
            ),
            255 => 
            array (
                'id' => 1776,
                'company_id' => 113,
                'block_id' => 414,
                'school_academic_year_id' => 3,
            ),
            256 => 
            array (
                'id' => 1777,
                'company_id' => 113,
                'block_id' => 415,
                'school_academic_year_id' => 3,
            ),
            257 => 
            array (
                'id' => 1778,
                'company_id' => 113,
                'block_id' => 416,
                'school_academic_year_id' => 3,
            ),
            258 => 
            array (
                'id' => 1779,
                'company_id' => 113,
                'block_id' => 413,
                'school_academic_year_id' => 4,
            ),
            259 => 
            array (
                'id' => 1780,
                'company_id' => 113,
                'block_id' => 414,
                'school_academic_year_id' => 4,
            ),
            260 => 
            array (
                'id' => 1781,
                'company_id' => 113,
                'block_id' => 415,
                'school_academic_year_id' => 4,
            ),
            261 => 
            array (
                'id' => 1782,
                'company_id' => 113,
                'block_id' => 416,
                'school_academic_year_id' => 4,
            ),
            262 => 
            array (
                'id' => 1783,
                'company_id' => 113,
                'block_id' => 413,
                'school_academic_year_id' => 5,
            ),
            263 => 
            array (
                'id' => 1784,
                'company_id' => 113,
                'block_id' => 414,
                'school_academic_year_id' => 5,
            ),
            264 => 
            array (
                'id' => 1785,
                'company_id' => 113,
                'block_id' => 415,
                'school_academic_year_id' => 5,
            ),
            265 => 
            array (
                'id' => 1786,
                'company_id' => 113,
                'block_id' => 416,
                'school_academic_year_id' => 5,
            ),
            266 => 
            array (
                'id' => 1787,
                'company_id' => 113,
                'block_id' => 413,
                'school_academic_year_id' => 6,
            ),
            267 => 
            array (
                'id' => 1788,
                'company_id' => 113,
                'block_id' => 414,
                'school_academic_year_id' => 6,
            ),
            268 => 
            array (
                'id' => 1789,
                'company_id' => 113,
                'block_id' => 415,
                'school_academic_year_id' => 6,
            ),
            269 => 
            array (
                'id' => 1790,
                'company_id' => 113,
                'block_id' => 416,
                'school_academic_year_id' => 6,
            ),
            270 => 
            array (
                'id' => 1791,
                'company_id' => 114,
                'block_id' => 417,
                'school_academic_year_id' => 2,
            ),
            271 => 
            array (
                'id' => 1792,
                'company_id' => 114,
                'block_id' => 418,
                'school_academic_year_id' => 2,
            ),
            272 => 
            array (
                'id' => 1793,
                'company_id' => 114,
                'block_id' => 419,
                'school_academic_year_id' => 2,
            ),
            273 => 
            array (
                'id' => 1794,
                'company_id' => 114,
                'block_id' => 420,
                'school_academic_year_id' => 2,
            ),
            274 => 
            array (
                'id' => 1795,
                'company_id' => 114,
                'block_id' => 421,
                'school_academic_year_id' => 2,
            ),
            275 => 
            array (
                'id' => 1796,
                'company_id' => 114,
                'block_id' => 417,
                'school_academic_year_id' => 3,
            ),
            276 => 
            array (
                'id' => 1797,
                'company_id' => 114,
                'block_id' => 418,
                'school_academic_year_id' => 3,
            ),
            277 => 
            array (
                'id' => 1798,
                'company_id' => 114,
                'block_id' => 419,
                'school_academic_year_id' => 3,
            ),
            278 => 
            array (
                'id' => 1799,
                'company_id' => 114,
                'block_id' => 420,
                'school_academic_year_id' => 3,
            ),
            279 => 
            array (
                'id' => 1800,
                'company_id' => 114,
                'block_id' => 421,
                'school_academic_year_id' => 3,
            ),
            280 => 
            array (
                'id' => 1801,
                'company_id' => 114,
                'block_id' => 417,
                'school_academic_year_id' => 4,
            ),
            281 => 
            array (
                'id' => 1802,
                'company_id' => 114,
                'block_id' => 418,
                'school_academic_year_id' => 4,
            ),
            282 => 
            array (
                'id' => 1803,
                'company_id' => 114,
                'block_id' => 419,
                'school_academic_year_id' => 4,
            ),
            283 => 
            array (
                'id' => 1804,
                'company_id' => 114,
                'block_id' => 420,
                'school_academic_year_id' => 4,
            ),
            284 => 
            array (
                'id' => 1805,
                'company_id' => 114,
                'block_id' => 421,
                'school_academic_year_id' => 4,
            ),
            285 => 
            array (
                'id' => 1806,
                'company_id' => 114,
                'block_id' => 417,
                'school_academic_year_id' => 5,
            ),
            286 => 
            array (
                'id' => 1807,
                'company_id' => 114,
                'block_id' => 418,
                'school_academic_year_id' => 5,
            ),
            287 => 
            array (
                'id' => 1808,
                'company_id' => 114,
                'block_id' => 419,
                'school_academic_year_id' => 5,
            ),
            288 => 
            array (
                'id' => 1809,
                'company_id' => 114,
                'block_id' => 420,
                'school_academic_year_id' => 5,
            ),
            289 => 
            array (
                'id' => 1810,
                'company_id' => 114,
                'block_id' => 421,
                'school_academic_year_id' => 5,
            ),
            290 => 
            array (
                'id' => 1811,
                'company_id' => 114,
                'block_id' => 417,
                'school_academic_year_id' => 6,
            ),
            291 => 
            array (
                'id' => 1812,
                'company_id' => 114,
                'block_id' => 418,
                'school_academic_year_id' => 6,
            ),
            292 => 
            array (
                'id' => 1813,
                'company_id' => 114,
                'block_id' => 419,
                'school_academic_year_id' => 6,
            ),
            293 => 
            array (
                'id' => 1814,
                'company_id' => 114,
                'block_id' => 420,
                'school_academic_year_id' => 6,
            ),
            294 => 
            array (
                'id' => 1815,
                'company_id' => 114,
                'block_id' => 421,
                'school_academic_year_id' => 6,
            ),
            295 => 
            array (
                'id' => 1816,
                'company_id' => 115,
                'block_id' => 424,
                'school_academic_year_id' => 2,
            ),
            296 => 
            array (
                'id' => 1817,
                'company_id' => 115,
                'block_id' => 424,
                'school_academic_year_id' => 3,
            ),
            297 => 
            array (
                'id' => 1818,
                'company_id' => 115,
                'block_id' => 424,
                'school_academic_year_id' => 4,
            ),
            298 => 
            array (
                'id' => 1819,
                'company_id' => 115,
                'block_id' => 424,
                'school_academic_year_id' => 5,
            ),
            299 => 
            array (
                'id' => 1820,
                'company_id' => 115,
                'block_id' => 424,
                'school_academic_year_id' => 6,
            ),
            300 => 
            array (
                'id' => 1821,
                'company_id' => 116,
                'block_id' => 434,
                'school_academic_year_id' => 2,
            ),
            301 => 
            array (
                'id' => 1822,
                'company_id' => 116,
                'block_id' => 435,
                'school_academic_year_id' => 2,
            ),
            302 => 
            array (
                'id' => 1823,
                'company_id' => 116,
                'block_id' => 436,
                'school_academic_year_id' => 2,
            ),
            303 => 
            array (
                'id' => 1824,
                'company_id' => 116,
                'block_id' => 434,
                'school_academic_year_id' => 3,
            ),
            304 => 
            array (
                'id' => 1825,
                'company_id' => 116,
                'block_id' => 435,
                'school_academic_year_id' => 3,
            ),
            305 => 
            array (
                'id' => 1826,
                'company_id' => 116,
                'block_id' => 436,
                'school_academic_year_id' => 3,
            ),
            306 => 
            array (
                'id' => 1827,
                'company_id' => 116,
                'block_id' => 434,
                'school_academic_year_id' => 4,
            ),
            307 => 
            array (
                'id' => 1828,
                'company_id' => 116,
                'block_id' => 435,
                'school_academic_year_id' => 4,
            ),
            308 => 
            array (
                'id' => 1829,
                'company_id' => 116,
                'block_id' => 436,
                'school_academic_year_id' => 4,
            ),
            309 => 
            array (
                'id' => 1830,
                'company_id' => 116,
                'block_id' => 434,
                'school_academic_year_id' => 5,
            ),
            310 => 
            array (
                'id' => 1831,
                'company_id' => 116,
                'block_id' => 435,
                'school_academic_year_id' => 5,
            ),
            311 => 
            array (
                'id' => 1832,
                'company_id' => 116,
                'block_id' => 436,
                'school_academic_year_id' => 5,
            ),
            312 => 
            array (
                'id' => 1833,
                'company_id' => 116,
                'block_id' => 434,
                'school_academic_year_id' => 6,
            ),
            313 => 
            array (
                'id' => 1834,
                'company_id' => 116,
                'block_id' => 435,
                'school_academic_year_id' => 6,
            ),
            314 => 
            array (
                'id' => 1835,
                'company_id' => 116,
                'block_id' => 436,
                'school_academic_year_id' => 6,
            ),
            315 => 
            array (
                'id' => 1836,
                'company_id' => 117,
                'block_id' => 441,
                'school_academic_year_id' => 2,
            ),
            316 => 
            array (
                'id' => 1837,
                'company_id' => 117,
                'block_id' => 442,
                'school_academic_year_id' => 2,
            ),
            317 => 
            array (
                'id' => 1838,
                'company_id' => 117,
                'block_id' => 443,
                'school_academic_year_id' => 2,
            ),
            318 => 
            array (
                'id' => 1839,
                'company_id' => 117,
                'block_id' => 444,
                'school_academic_year_id' => 2,
            ),
            319 => 
            array (
                'id' => 1840,
                'company_id' => 117,
                'block_id' => 445,
                'school_academic_year_id' => 2,
            ),
            320 => 
            array (
                'id' => 1841,
                'company_id' => 117,
                'block_id' => 441,
                'school_academic_year_id' => 3,
            ),
            321 => 
            array (
                'id' => 1842,
                'company_id' => 117,
                'block_id' => 442,
                'school_academic_year_id' => 3,
            ),
            322 => 
            array (
                'id' => 1843,
                'company_id' => 117,
                'block_id' => 443,
                'school_academic_year_id' => 3,
            ),
            323 => 
            array (
                'id' => 1844,
                'company_id' => 117,
                'block_id' => 444,
                'school_academic_year_id' => 3,
            ),
            324 => 
            array (
                'id' => 1845,
                'company_id' => 117,
                'block_id' => 445,
                'school_academic_year_id' => 3,
            ),
            325 => 
            array (
                'id' => 1846,
                'company_id' => 117,
                'block_id' => 441,
                'school_academic_year_id' => 4,
            ),
            326 => 
            array (
                'id' => 1847,
                'company_id' => 117,
                'block_id' => 442,
                'school_academic_year_id' => 4,
            ),
            327 => 
            array (
                'id' => 1848,
                'company_id' => 117,
                'block_id' => 443,
                'school_academic_year_id' => 4,
            ),
            328 => 
            array (
                'id' => 1849,
                'company_id' => 117,
                'block_id' => 444,
                'school_academic_year_id' => 4,
            ),
            329 => 
            array (
                'id' => 1850,
                'company_id' => 117,
                'block_id' => 445,
                'school_academic_year_id' => 4,
            ),
            330 => 
            array (
                'id' => 1851,
                'company_id' => 117,
                'block_id' => 441,
                'school_academic_year_id' => 5,
            ),
            331 => 
            array (
                'id' => 1852,
                'company_id' => 117,
                'block_id' => 442,
                'school_academic_year_id' => 5,
            ),
            332 => 
            array (
                'id' => 1853,
                'company_id' => 117,
                'block_id' => 443,
                'school_academic_year_id' => 5,
            ),
            333 => 
            array (
                'id' => 1854,
                'company_id' => 117,
                'block_id' => 444,
                'school_academic_year_id' => 5,
            ),
            334 => 
            array (
                'id' => 1855,
                'company_id' => 117,
                'block_id' => 445,
                'school_academic_year_id' => 5,
            ),
            335 => 
            array (
                'id' => 1856,
                'company_id' => 117,
                'block_id' => 441,
                'school_academic_year_id' => 6,
            ),
            336 => 
            array (
                'id' => 1857,
                'company_id' => 117,
                'block_id' => 442,
                'school_academic_year_id' => 6,
            ),
            337 => 
            array (
                'id' => 1858,
                'company_id' => 117,
                'block_id' => 443,
                'school_academic_year_id' => 6,
            ),
            338 => 
            array (
                'id' => 1859,
                'company_id' => 117,
                'block_id' => 444,
                'school_academic_year_id' => 6,
            ),
            339 => 
            array (
                'id' => 1860,
                'company_id' => 117,
                'block_id' => 445,
                'school_academic_year_id' => 6,
            ),
            340 => 
            array (
                'id' => 1861,
                'company_id' => 118,
                'block_id' => 450,
                'school_academic_year_id' => 2,
            ),
            341 => 
            array (
                'id' => 1862,
                'company_id' => 118,
                'block_id' => 451,
                'school_academic_year_id' => 2,
            ),
            342 => 
            array (
                'id' => 1863,
                'company_id' => 118,
                'block_id' => 452,
                'school_academic_year_id' => 2,
            ),
            343 => 
            array (
                'id' => 1864,
                'company_id' => 118,
                'block_id' => 450,
                'school_academic_year_id' => 3,
            ),
            344 => 
            array (
                'id' => 1865,
                'company_id' => 118,
                'block_id' => 451,
                'school_academic_year_id' => 3,
            ),
            345 => 
            array (
                'id' => 1866,
                'company_id' => 118,
                'block_id' => 452,
                'school_academic_year_id' => 3,
            ),
            346 => 
            array (
                'id' => 1867,
                'company_id' => 118,
                'block_id' => 450,
                'school_academic_year_id' => 4,
            ),
            347 => 
            array (
                'id' => 1868,
                'company_id' => 118,
                'block_id' => 451,
                'school_academic_year_id' => 4,
            ),
            348 => 
            array (
                'id' => 1869,
                'company_id' => 118,
                'block_id' => 452,
                'school_academic_year_id' => 4,
            ),
            349 => 
            array (
                'id' => 1870,
                'company_id' => 118,
                'block_id' => 450,
                'school_academic_year_id' => 5,
            ),
            350 => 
            array (
                'id' => 1871,
                'company_id' => 118,
                'block_id' => 451,
                'school_academic_year_id' => 5,
            ),
            351 => 
            array (
                'id' => 1872,
                'company_id' => 118,
                'block_id' => 452,
                'school_academic_year_id' => 5,
            ),
            352 => 
            array (
                'id' => 1873,
                'company_id' => 118,
                'block_id' => 450,
                'school_academic_year_id' => 6,
            ),
            353 => 
            array (
                'id' => 1874,
                'company_id' => 118,
                'block_id' => 451,
                'school_academic_year_id' => 6,
            ),
            354 => 
            array (
                'id' => 1875,
                'company_id' => 118,
                'block_id' => 452,
                'school_academic_year_id' => 6,
            ),
            355 => 
            array (
                'id' => 1876,
                'company_id' => 119,
                'block_id' => 453,
                'school_academic_year_id' => 2,
            ),
            356 => 
            array (
                'id' => 1877,
                'company_id' => 119,
                'block_id' => 454,
                'school_academic_year_id' => 2,
            ),
            357 => 
            array (
                'id' => 1878,
                'company_id' => 119,
                'block_id' => 453,
                'school_academic_year_id' => 3,
            ),
            358 => 
            array (
                'id' => 1879,
                'company_id' => 119,
                'block_id' => 454,
                'school_academic_year_id' => 3,
            ),
            359 => 
            array (
                'id' => 1880,
                'company_id' => 119,
                'block_id' => 453,
                'school_academic_year_id' => 4,
            ),
            360 => 
            array (
                'id' => 1881,
                'company_id' => 119,
                'block_id' => 454,
                'school_academic_year_id' => 4,
            ),
            361 => 
            array (
                'id' => 1882,
                'company_id' => 119,
                'block_id' => 453,
                'school_academic_year_id' => 5,
            ),
            362 => 
            array (
                'id' => 1883,
                'company_id' => 119,
                'block_id' => 454,
                'school_academic_year_id' => 5,
            ),
            363 => 
            array (
                'id' => 1884,
                'company_id' => 119,
                'block_id' => 453,
                'school_academic_year_id' => 6,
            ),
            364 => 
            array (
                'id' => 1885,
                'company_id' => 119,
                'block_id' => 454,
                'school_academic_year_id' => 6,
            ),
            365 => 
            array (
                'id' => 1886,
                'company_id' => 120,
                'block_id' => 458,
                'school_academic_year_id' => 2,
            ),
            366 => 
            array (
                'id' => 1887,
                'company_id' => 120,
                'block_id' => 459,
                'school_academic_year_id' => 2,
            ),
            367 => 
            array (
                'id' => 1888,
                'company_id' => 120,
                'block_id' => 458,
                'school_academic_year_id' => 3,
            ),
            368 => 
            array (
                'id' => 1889,
                'company_id' => 120,
                'block_id' => 459,
                'school_academic_year_id' => 3,
            ),
            369 => 
            array (
                'id' => 1890,
                'company_id' => 120,
                'block_id' => 458,
                'school_academic_year_id' => 4,
            ),
            370 => 
            array (
                'id' => 1891,
                'company_id' => 120,
                'block_id' => 459,
                'school_academic_year_id' => 4,
            ),
            371 => 
            array (
                'id' => 1892,
                'company_id' => 120,
                'block_id' => 458,
                'school_academic_year_id' => 5,
            ),
            372 => 
            array (
                'id' => 1893,
                'company_id' => 120,
                'block_id' => 459,
                'school_academic_year_id' => 5,
            ),
            373 => 
            array (
                'id' => 1894,
                'company_id' => 120,
                'block_id' => 458,
                'school_academic_year_id' => 6,
            ),
            374 => 
            array (
                'id' => 1895,
                'company_id' => 120,
                'block_id' => 459,
                'school_academic_year_id' => 6,
            ),
            375 => 
            array (
                'id' => 1896,
                'company_id' => 121,
                'block_id' => 460,
                'school_academic_year_id' => 2,
            ),
            376 => 
            array (
                'id' => 1897,
                'company_id' => 121,
                'block_id' => 460,
                'school_academic_year_id' => 3,
            ),
            377 => 
            array (
                'id' => 1898,
                'company_id' => 121,
                'block_id' => 460,
                'school_academic_year_id' => 4,
            ),
            378 => 
            array (
                'id' => 1899,
                'company_id' => 121,
                'block_id' => 460,
                'school_academic_year_id' => 5,
            ),
            379 => 
            array (
                'id' => 1900,
                'company_id' => 121,
                'block_id' => 460,
                'school_academic_year_id' => 6,
            ),
            380 => 
            array (
                'id' => 1901,
                'company_id' => 122,
                'block_id' => 461,
                'school_academic_year_id' => 2,
            ),
            381 => 
            array (
                'id' => 1902,
                'company_id' => 122,
                'block_id' => 463,
                'school_academic_year_id' => 2,
            ),
            382 => 
            array (
                'id' => 1903,
                'company_id' => 122,
                'block_id' => 466,
                'school_academic_year_id' => 2,
            ),
            383 => 
            array (
                'id' => 1904,
                'company_id' => 122,
                'block_id' => 467,
                'school_academic_year_id' => 2,
            ),
            384 => 
            array (
                'id' => 1905,
                'company_id' => 122,
                'block_id' => 468,
                'school_academic_year_id' => 2,
            ),
            385 => 
            array (
                'id' => 1906,
                'company_id' => 122,
                'block_id' => 469,
                'school_academic_year_id' => 2,
            ),
            386 => 
            array (
                'id' => 1907,
                'company_id' => 122,
                'block_id' => 470,
                'school_academic_year_id' => 2,
            ),
            387 => 
            array (
                'id' => 1908,
                'company_id' => 122,
                'block_id' => 471,
                'school_academic_year_id' => 2,
            ),
            388 => 
            array (
                'id' => 1909,
                'company_id' => 122,
                'block_id' => 472,
                'school_academic_year_id' => 2,
            ),
            389 => 
            array (
                'id' => 1910,
                'company_id' => 122,
                'block_id' => 473,
                'school_academic_year_id' => 2,
            ),
            390 => 
            array (
                'id' => 1911,
                'company_id' => 122,
                'block_id' => 474,
                'school_academic_year_id' => 2,
            ),
            391 => 
            array (
                'id' => 1912,
                'company_id' => 122,
                'block_id' => 475,
                'school_academic_year_id' => 2,
            ),
            392 => 
            array (
                'id' => 1913,
                'company_id' => 122,
                'block_id' => 476,
                'school_academic_year_id' => 2,
            ),
            393 => 
            array (
                'id' => 1914,
                'company_id' => 122,
                'block_id' => 477,
                'school_academic_year_id' => 2,
            ),
            394 => 
            array (
                'id' => 1915,
                'company_id' => 122,
                'block_id' => 478,
                'school_academic_year_id' => 2,
            ),
            395 => 
            array (
                'id' => 1916,
                'company_id' => 122,
                'block_id' => 479,
                'school_academic_year_id' => 2,
            ),
            396 => 
            array (
                'id' => 1917,
                'company_id' => 122,
                'block_id' => 480,
                'school_academic_year_id' => 2,
            ),
            397 => 
            array (
                'id' => 1918,
                'company_id' => 122,
                'block_id' => 481,
                'school_academic_year_id' => 2,
            ),
            398 => 
            array (
                'id' => 1919,
                'company_id' => 122,
                'block_id' => 461,
                'school_academic_year_id' => 3,
            ),
            399 => 
            array (
                'id' => 1920,
                'company_id' => 122,
                'block_id' => 463,
                'school_academic_year_id' => 3,
            ),
            400 => 
            array (
                'id' => 1921,
                'company_id' => 122,
                'block_id' => 466,
                'school_academic_year_id' => 3,
            ),
            401 => 
            array (
                'id' => 1922,
                'company_id' => 122,
                'block_id' => 467,
                'school_academic_year_id' => 3,
            ),
            402 => 
            array (
                'id' => 1923,
                'company_id' => 122,
                'block_id' => 468,
                'school_academic_year_id' => 3,
            ),
            403 => 
            array (
                'id' => 1924,
                'company_id' => 122,
                'block_id' => 469,
                'school_academic_year_id' => 3,
            ),
            404 => 
            array (
                'id' => 1925,
                'company_id' => 122,
                'block_id' => 470,
                'school_academic_year_id' => 3,
            ),
            405 => 
            array (
                'id' => 1926,
                'company_id' => 122,
                'block_id' => 471,
                'school_academic_year_id' => 3,
            ),
            406 => 
            array (
                'id' => 1927,
                'company_id' => 122,
                'block_id' => 472,
                'school_academic_year_id' => 3,
            ),
            407 => 
            array (
                'id' => 1928,
                'company_id' => 122,
                'block_id' => 473,
                'school_academic_year_id' => 3,
            ),
            408 => 
            array (
                'id' => 1929,
                'company_id' => 122,
                'block_id' => 474,
                'school_academic_year_id' => 3,
            ),
            409 => 
            array (
                'id' => 1930,
                'company_id' => 122,
                'block_id' => 475,
                'school_academic_year_id' => 3,
            ),
            410 => 
            array (
                'id' => 1931,
                'company_id' => 122,
                'block_id' => 476,
                'school_academic_year_id' => 3,
            ),
            411 => 
            array (
                'id' => 1932,
                'company_id' => 122,
                'block_id' => 477,
                'school_academic_year_id' => 3,
            ),
            412 => 
            array (
                'id' => 1933,
                'company_id' => 122,
                'block_id' => 478,
                'school_academic_year_id' => 3,
            ),
            413 => 
            array (
                'id' => 1934,
                'company_id' => 122,
                'block_id' => 479,
                'school_academic_year_id' => 3,
            ),
            414 => 
            array (
                'id' => 1935,
                'company_id' => 122,
                'block_id' => 480,
                'school_academic_year_id' => 3,
            ),
            415 => 
            array (
                'id' => 1936,
                'company_id' => 122,
                'block_id' => 481,
                'school_academic_year_id' => 3,
            ),
            416 => 
            array (
                'id' => 1937,
                'company_id' => 122,
                'block_id' => 461,
                'school_academic_year_id' => 4,
            ),
            417 => 
            array (
                'id' => 1938,
                'company_id' => 122,
                'block_id' => 463,
                'school_academic_year_id' => 4,
            ),
            418 => 
            array (
                'id' => 1939,
                'company_id' => 122,
                'block_id' => 466,
                'school_academic_year_id' => 4,
            ),
            419 => 
            array (
                'id' => 1940,
                'company_id' => 122,
                'block_id' => 467,
                'school_academic_year_id' => 4,
            ),
            420 => 
            array (
                'id' => 1941,
                'company_id' => 122,
                'block_id' => 468,
                'school_academic_year_id' => 4,
            ),
            421 => 
            array (
                'id' => 1942,
                'company_id' => 122,
                'block_id' => 469,
                'school_academic_year_id' => 4,
            ),
            422 => 
            array (
                'id' => 1943,
                'company_id' => 122,
                'block_id' => 470,
                'school_academic_year_id' => 4,
            ),
            423 => 
            array (
                'id' => 1944,
                'company_id' => 122,
                'block_id' => 471,
                'school_academic_year_id' => 4,
            ),
            424 => 
            array (
                'id' => 1945,
                'company_id' => 122,
                'block_id' => 472,
                'school_academic_year_id' => 4,
            ),
            425 => 
            array (
                'id' => 1946,
                'company_id' => 122,
                'block_id' => 473,
                'school_academic_year_id' => 4,
            ),
            426 => 
            array (
                'id' => 1947,
                'company_id' => 122,
                'block_id' => 474,
                'school_academic_year_id' => 4,
            ),
            427 => 
            array (
                'id' => 1948,
                'company_id' => 122,
                'block_id' => 475,
                'school_academic_year_id' => 4,
            ),
            428 => 
            array (
                'id' => 1949,
                'company_id' => 122,
                'block_id' => 476,
                'school_academic_year_id' => 4,
            ),
            429 => 
            array (
                'id' => 1950,
                'company_id' => 122,
                'block_id' => 477,
                'school_academic_year_id' => 4,
            ),
            430 => 
            array (
                'id' => 1951,
                'company_id' => 122,
                'block_id' => 478,
                'school_academic_year_id' => 4,
            ),
            431 => 
            array (
                'id' => 1952,
                'company_id' => 122,
                'block_id' => 479,
                'school_academic_year_id' => 4,
            ),
            432 => 
            array (
                'id' => 1953,
                'company_id' => 122,
                'block_id' => 480,
                'school_academic_year_id' => 4,
            ),
            433 => 
            array (
                'id' => 1954,
                'company_id' => 122,
                'block_id' => 481,
                'school_academic_year_id' => 4,
            ),
            434 => 
            array (
                'id' => 1955,
                'company_id' => 122,
                'block_id' => 461,
                'school_academic_year_id' => 5,
            ),
            435 => 
            array (
                'id' => 1956,
                'company_id' => 122,
                'block_id' => 463,
                'school_academic_year_id' => 5,
            ),
            436 => 
            array (
                'id' => 1957,
                'company_id' => 122,
                'block_id' => 466,
                'school_academic_year_id' => 5,
            ),
            437 => 
            array (
                'id' => 1958,
                'company_id' => 122,
                'block_id' => 467,
                'school_academic_year_id' => 5,
            ),
            438 => 
            array (
                'id' => 1959,
                'company_id' => 122,
                'block_id' => 468,
                'school_academic_year_id' => 5,
            ),
            439 => 
            array (
                'id' => 1960,
                'company_id' => 122,
                'block_id' => 469,
                'school_academic_year_id' => 5,
            ),
            440 => 
            array (
                'id' => 1961,
                'company_id' => 122,
                'block_id' => 470,
                'school_academic_year_id' => 5,
            ),
            441 => 
            array (
                'id' => 1962,
                'company_id' => 122,
                'block_id' => 471,
                'school_academic_year_id' => 5,
            ),
            442 => 
            array (
                'id' => 1963,
                'company_id' => 122,
                'block_id' => 472,
                'school_academic_year_id' => 5,
            ),
            443 => 
            array (
                'id' => 1964,
                'company_id' => 122,
                'block_id' => 473,
                'school_academic_year_id' => 5,
            ),
            444 => 
            array (
                'id' => 1965,
                'company_id' => 122,
                'block_id' => 474,
                'school_academic_year_id' => 5,
            ),
            445 => 
            array (
                'id' => 1966,
                'company_id' => 122,
                'block_id' => 475,
                'school_academic_year_id' => 5,
            ),
            446 => 
            array (
                'id' => 1967,
                'company_id' => 122,
                'block_id' => 476,
                'school_academic_year_id' => 5,
            ),
            447 => 
            array (
                'id' => 1968,
                'company_id' => 122,
                'block_id' => 477,
                'school_academic_year_id' => 5,
            ),
            448 => 
            array (
                'id' => 1969,
                'company_id' => 122,
                'block_id' => 478,
                'school_academic_year_id' => 5,
            ),
            449 => 
            array (
                'id' => 1970,
                'company_id' => 122,
                'block_id' => 479,
                'school_academic_year_id' => 5,
            ),
            450 => 
            array (
                'id' => 1971,
                'company_id' => 122,
                'block_id' => 480,
                'school_academic_year_id' => 5,
            ),
            451 => 
            array (
                'id' => 1972,
                'company_id' => 122,
                'block_id' => 481,
                'school_academic_year_id' => 5,
            ),
            452 => 
            array (
                'id' => 1973,
                'company_id' => 122,
                'block_id' => 461,
                'school_academic_year_id' => 6,
            ),
            453 => 
            array (
                'id' => 1974,
                'company_id' => 122,
                'block_id' => 463,
                'school_academic_year_id' => 6,
            ),
            454 => 
            array (
                'id' => 1975,
                'company_id' => 122,
                'block_id' => 466,
                'school_academic_year_id' => 6,
            ),
            455 => 
            array (
                'id' => 1976,
                'company_id' => 122,
                'block_id' => 467,
                'school_academic_year_id' => 6,
            ),
            456 => 
            array (
                'id' => 1977,
                'company_id' => 122,
                'block_id' => 468,
                'school_academic_year_id' => 6,
            ),
            457 => 
            array (
                'id' => 1978,
                'company_id' => 122,
                'block_id' => 469,
                'school_academic_year_id' => 6,
            ),
            458 => 
            array (
                'id' => 1979,
                'company_id' => 122,
                'block_id' => 470,
                'school_academic_year_id' => 6,
            ),
            459 => 
            array (
                'id' => 1980,
                'company_id' => 122,
                'block_id' => 471,
                'school_academic_year_id' => 6,
            ),
            460 => 
            array (
                'id' => 1981,
                'company_id' => 122,
                'block_id' => 472,
                'school_academic_year_id' => 6,
            ),
            461 => 
            array (
                'id' => 1982,
                'company_id' => 122,
                'block_id' => 473,
                'school_academic_year_id' => 6,
            ),
            462 => 
            array (
                'id' => 1983,
                'company_id' => 122,
                'block_id' => 474,
                'school_academic_year_id' => 6,
            ),
            463 => 
            array (
                'id' => 1984,
                'company_id' => 122,
                'block_id' => 475,
                'school_academic_year_id' => 6,
            ),
            464 => 
            array (
                'id' => 1985,
                'company_id' => 122,
                'block_id' => 476,
                'school_academic_year_id' => 6,
            ),
            465 => 
            array (
                'id' => 1986,
                'company_id' => 122,
                'block_id' => 477,
                'school_academic_year_id' => 6,
            ),
            466 => 
            array (
                'id' => 1987,
                'company_id' => 122,
                'block_id' => 478,
                'school_academic_year_id' => 6,
            ),
            467 => 
            array (
                'id' => 1988,
                'company_id' => 122,
                'block_id' => 479,
                'school_academic_year_id' => 6,
            ),
            468 => 
            array (
                'id' => 1989,
                'company_id' => 122,
                'block_id' => 480,
                'school_academic_year_id' => 6,
            ),
            469 => 
            array (
                'id' => 1990,
                'company_id' => 122,
                'block_id' => 481,
                'school_academic_year_id' => 6,
            ),
            470 => 
            array (
                'id' => 1991,
                'company_id' => 123,
                'block_id' => 482,
                'school_academic_year_id' => 2,
            ),
            471 => 
            array (
                'id' => 1992,
                'company_id' => 123,
                'block_id' => 482,
                'school_academic_year_id' => 3,
            ),
            472 => 
            array (
                'id' => 1993,
                'company_id' => 123,
                'block_id' => 482,
                'school_academic_year_id' => 4,
            ),
            473 => 
            array (
                'id' => 1994,
                'company_id' => 123,
                'block_id' => 482,
                'school_academic_year_id' => 5,
            ),
            474 => 
            array (
                'id' => 1995,
                'company_id' => 123,
                'block_id' => 482,
                'school_academic_year_id' => 6,
            ),
            475 => 
            array (
                'id' => 1996,
                'company_id' => 124,
                'block_id' => 483,
                'school_academic_year_id' => 2,
            ),
            476 => 
            array (
                'id' => 1997,
                'company_id' => 124,
                'block_id' => 483,
                'school_academic_year_id' => 3,
            ),
            477 => 
            array (
                'id' => 1998,
                'company_id' => 124,
                'block_id' => 483,
                'school_academic_year_id' => 4,
            ),
            478 => 
            array (
                'id' => 1999,
                'company_id' => 124,
                'block_id' => 483,
                'school_academic_year_id' => 5,
            ),
            479 => 
            array (
                'id' => 2000,
                'company_id' => 124,
                'block_id' => 483,
                'school_academic_year_id' => 6,
            ),
            480 => 
            array (
                'id' => 2002,
                'company_id' => 125,
                'block_id' => 492,
                'school_academic_year_id' => 2,
            ),
            481 => 
            array (
                'id' => 2003,
                'company_id' => 125,
                'block_id' => 493,
                'school_academic_year_id' => 2,
            ),
            482 => 
            array (
                'id' => 2004,
                'company_id' => 125,
                'block_id' => 494,
                'school_academic_year_id' => 2,
            ),
            483 => 
            array (
                'id' => 2005,
                'company_id' => 125,
                'block_id' => 495,
                'school_academic_year_id' => 2,
            ),
            484 => 
            array (
                'id' => 2006,
                'company_id' => 125,
                'block_id' => 496,
                'school_academic_year_id' => 2,
            ),
            485 => 
            array (
                'id' => 2007,
                'company_id' => 125,
                'block_id' => 497,
                'school_academic_year_id' => 2,
            ),
            486 => 
            array (
                'id' => 2009,
                'company_id' => 125,
                'block_id' => 492,
                'school_academic_year_id' => 3,
            ),
            487 => 
            array (
                'id' => 2010,
                'company_id' => 125,
                'block_id' => 493,
                'school_academic_year_id' => 3,
            ),
            488 => 
            array (
                'id' => 2011,
                'company_id' => 125,
                'block_id' => 494,
                'school_academic_year_id' => 3,
            ),
            489 => 
            array (
                'id' => 2012,
                'company_id' => 125,
                'block_id' => 495,
                'school_academic_year_id' => 3,
            ),
            490 => 
            array (
                'id' => 2013,
                'company_id' => 125,
                'block_id' => 496,
                'school_academic_year_id' => 3,
            ),
            491 => 
            array (
                'id' => 2014,
                'company_id' => 125,
                'block_id' => 497,
                'school_academic_year_id' => 3,
            ),
            492 => 
            array (
                'id' => 2016,
                'company_id' => 125,
                'block_id' => 492,
                'school_academic_year_id' => 4,
            ),
            493 => 
            array (
                'id' => 2017,
                'company_id' => 125,
                'block_id' => 493,
                'school_academic_year_id' => 4,
            ),
            494 => 
            array (
                'id' => 2018,
                'company_id' => 125,
                'block_id' => 494,
                'school_academic_year_id' => 4,
            ),
            495 => 
            array (
                'id' => 2019,
                'company_id' => 125,
                'block_id' => 495,
                'school_academic_year_id' => 4,
            ),
            496 => 
            array (
                'id' => 2020,
                'company_id' => 125,
                'block_id' => 496,
                'school_academic_year_id' => 4,
            ),
            497 => 
            array (
                'id' => 2021,
                'company_id' => 125,
                'block_id' => 497,
                'school_academic_year_id' => 4,
            ),
            498 => 
            array (
                'id' => 2023,
                'company_id' => 125,
                'block_id' => 492,
                'school_academic_year_id' => 5,
            ),
            499 => 
            array (
                'id' => 2024,
                'company_id' => 125,
                'block_id' => 493,
                'school_academic_year_id' => 5,
            ),
        ));
        \DB::table('block_years')->insert(array (
            0 => 
            array (
                'id' => 2025,
                'company_id' => 125,
                'block_id' => 494,
                'school_academic_year_id' => 5,
            ),
            1 => 
            array (
                'id' => 2026,
                'company_id' => 125,
                'block_id' => 495,
                'school_academic_year_id' => 5,
            ),
            2 => 
            array (
                'id' => 2027,
                'company_id' => 125,
                'block_id' => 496,
                'school_academic_year_id' => 5,
            ),
            3 => 
            array (
                'id' => 2028,
                'company_id' => 125,
                'block_id' => 497,
                'school_academic_year_id' => 5,
            ),
            4 => 
            array (
                'id' => 2030,
                'company_id' => 125,
                'block_id' => 492,
                'school_academic_year_id' => 6,
            ),
            5 => 
            array (
                'id' => 2031,
                'company_id' => 125,
                'block_id' => 493,
                'school_academic_year_id' => 6,
            ),
            6 => 
            array (
                'id' => 2032,
                'company_id' => 125,
                'block_id' => 494,
                'school_academic_year_id' => 6,
            ),
            7 => 
            array (
                'id' => 2033,
                'company_id' => 125,
                'block_id' => 495,
                'school_academic_year_id' => 6,
            ),
            8 => 
            array (
                'id' => 2034,
                'company_id' => 125,
                'block_id' => 496,
                'school_academic_year_id' => 6,
            ),
            9 => 
            array (
                'id' => 2035,
                'company_id' => 125,
                'block_id' => 497,
                'school_academic_year_id' => 6,
            ),
            10 => 
            array (
                'id' => 2041,
                'company_id' => NULL,
                'block_id' => 497,
                'school_academic_year_id' => 2,
            ),
            11 => 
            array (
                'id' => 2042,
                'company_id' => NULL,
                'block_id' => 497,
                'school_academic_year_id' => 3,
            ),
            12 => 
            array (
                'id' => 2043,
                'company_id' => NULL,
                'block_id' => 497,
                'school_academic_year_id' => 4,
            ),
            13 => 
            array (
                'id' => 2044,
                'company_id' => NULL,
                'block_id' => 497,
                'school_academic_year_id' => 5,
            ),
            14 => 
            array (
                'id' => 2045,
                'company_id' => NULL,
                'block_id' => 497,
                'school_academic_year_id' => 6,
            ),
            15 => 
            array (
                'id' => 2046,
                'company_id' => NULL,
                'block_id' => 496,
                'school_academic_year_id' => 2,
            ),
            16 => 
            array (
                'id' => 2047,
                'company_id' => NULL,
                'block_id' => 496,
                'school_academic_year_id' => 3,
            ),
            17 => 
            array (
                'id' => 2048,
                'company_id' => NULL,
                'block_id' => 496,
                'school_academic_year_id' => 4,
            ),
            18 => 
            array (
                'id' => 2049,
                'company_id' => NULL,
                'block_id' => 496,
                'school_academic_year_id' => 5,
            ),
            19 => 
            array (
                'id' => 2050,
                'company_id' => NULL,
                'block_id' => 496,
                'school_academic_year_id' => 6,
            ),
            20 => 
            array (
                'id' => 2066,
                'company_id' => 125,
                'block_id' => 491,
                'school_academic_year_id' => 2,
            ),
            21 => 
            array (
                'id' => 2067,
                'company_id' => 125,
                'block_id' => 491,
                'school_academic_year_id' => 3,
            ),
            22 => 
            array (
                'id' => 2068,
                'company_id' => 125,
                'block_id' => 491,
                'school_academic_year_id' => 4,
            ),
            23 => 
            array (
                'id' => 2069,
                'company_id' => 125,
                'block_id' => 491,
                'school_academic_year_id' => 5,
            ),
            24 => 
            array (
                'id' => 2070,
                'company_id' => 125,
                'block_id' => 491,
                'school_academic_year_id' => 6,
            ),
            25 => 
            array (
                'id' => 2073,
                'company_id' => 126,
                'block_id' => 499,
                'school_academic_year_id' => 2,
            ),
            26 => 
            array (
                'id' => 2074,
                'company_id' => 126,
                'block_id' => 499,
                'school_academic_year_id' => 3,
            ),
            27 => 
            array (
                'id' => 2075,
                'company_id' => 126,
                'block_id' => 499,
                'school_academic_year_id' => 5,
            ),
            28 => 
            array (
                'id' => 2086,
                'company_id' => 126,
                'block_id' => 500,
                'school_academic_year_id' => 2,
            ),
            29 => 
            array (
                'id' => 2087,
                'company_id' => 126,
                'block_id' => 500,
                'school_academic_year_id' => 3,
            ),
            30 => 
            array (
                'id' => 2088,
                'company_id' => 126,
                'block_id' => 500,
                'school_academic_year_id' => 4,
            ),
            31 => 
            array (
                'id' => 2089,
                'company_id' => 126,
                'block_id' => 500,
                'school_academic_year_id' => 5,
            ),
            32 => 
            array (
                'id' => 2090,
                'company_id' => 126,
                'block_id' => 500,
                'school_academic_year_id' => 6,
            ),
            33 => 
            array (
                'id' => 2174,
                'company_id' => 126,
                'block_id' => 498,
                'school_academic_year_id' => 2,
            ),
            34 => 
            array (
                'id' => 2175,
                'company_id' => 126,
                'block_id' => 498,
                'school_academic_year_id' => 3,
            ),
            35 => 
            array (
                'id' => 2176,
                'company_id' => 126,
                'block_id' => 498,
                'school_academic_year_id' => 4,
            ),
            36 => 
            array (
                'id' => 2177,
                'company_id' => 126,
                'block_id' => 498,
                'school_academic_year_id' => 5,
            ),
            37 => 
            array (
                'id' => 2178,
                'company_id' => 126,
                'block_id' => 498,
                'school_academic_year_id' => 6,
            ),
            38 => 
            array (
                'id' => 2179,
                'company_id' => 126,
                'block_id' => 501,
                'school_academic_year_id' => 2,
            ),
            39 => 
            array (
                'id' => 2180,
                'company_id' => 126,
                'block_id' => 501,
                'school_academic_year_id' => 3,
            ),
            40 => 
            array (
                'id' => 2181,
                'company_id' => 126,
                'block_id' => 501,
                'school_academic_year_id' => 4,
            ),
            41 => 
            array (
                'id' => 2191,
                'company_id' => 126,
                'block_id' => 502,
                'school_academic_year_id' => 2,
            ),
            42 => 
            array (
                'id' => 2192,
                'company_id' => 126,
                'block_id' => 502,
                'school_academic_year_id' => 3,
            ),
            43 => 
            array (
                'id' => 2193,
                'company_id' => 126,
                'block_id' => 502,
                'school_academic_year_id' => 4,
            ),
            44 => 
            array (
                'id' => 2194,
                'company_id' => 126,
                'block_id' => 502,
                'school_academic_year_id' => 5,
            ),
            45 => 
            array (
                'id' => 2195,
                'company_id' => 126,
                'block_id' => 502,
                'school_academic_year_id' => 6,
            ),
            46 => 
            array (
                'id' => 2196,
                'company_id' => 116,
                'block_id' => 503,
                'school_academic_year_id' => 2,
            ),
            47 => 
            array (
                'id' => 2197,
                'company_id' => 126,
                'block_id' => 505,
                'school_academic_year_id' => 2,
            ),
            48 => 
            array (
                'id' => 2198,
                'company_id' => 126,
                'block_id' => 505,
                'school_academic_year_id' => 3,
            ),
            49 => 
            array (
                'id' => 2199,
                'company_id' => 126,
                'block_id' => 511,
                'school_academic_year_id' => 2,
            ),
            50 => 
            array (
                'id' => 2200,
                'company_id' => 126,
                'block_id' => 512,
                'school_academic_year_id' => 2,
            ),
            51 => 
            array (
                'id' => 2201,
                'company_id' => 126,
                'block_id' => 512,
                'school_academic_year_id' => 3,
            ),
            52 => 
            array (
                'id' => 2202,
                'company_id' => 126,
                'block_id' => 513,
                'school_academic_year_id' => 2,
            ),
            53 => 
            array (
                'id' => 2203,
                'company_id' => 126,
                'block_id' => 514,
                'school_academic_year_id' => 3,
            ),
            54 => 
            array (
                'id' => 2204,
                'company_id' => 126,
                'block_id' => 515,
                'school_academic_year_id' => 2,
            ),
            55 => 
            array (
                'id' => 2205,
                'company_id' => 126,
                'block_id' => 516,
                'school_academic_year_id' => 2,
            ),
            56 => 
            array (
                'id' => 2206,
                'company_id' => 126,
                'block_id' => 517,
                'school_academic_year_id' => 2,
            ),
            57 => 
            array (
                'id' => 2207,
                'company_id' => 126,
                'block_id' => 518,
                'school_academic_year_id' => 3,
            ),
            58 => 
            array (
                'id' => 2208,
                'company_id' => 126,
                'block_id' => 520,
                'school_academic_year_id' => 2,
            ),
            59 => 
            array (
                'id' => 2212,
                'company_id' => 126,
                'block_id' => 523,
                'school_academic_year_id' => 3,
            ),
            60 => 
            array (
                'id' => 2213,
                'company_id' => 126,
                'block_id' => 524,
                'school_academic_year_id' => 2,
            ),
            61 => 
            array (
                'id' => 2214,
                'company_id' => 126,
                'block_id' => 525,
                'school_academic_year_id' => 2,
            ),
            62 => 
            array (
                'id' => 2216,
                'company_id' => 126,
                'block_id' => 528,
                'school_academic_year_id' => 2,
            ),
            63 => 
            array (
                'id' => 2218,
                'company_id' => 126,
                'block_id' => 529,
                'school_academic_year_id' => 2,
            ),
            64 => 
            array (
                'id' => 2219,
                'company_id' => 126,
                'block_id' => 531,
                'school_academic_year_id' => 2,
            ),
            65 => 
            array (
                'id' => 2220,
                'company_id' => 126,
                'block_id' => 532,
                'school_academic_year_id' => 3,
            ),
            66 => 
            array (
                'id' => 2221,
                'company_id' => 126,
                'block_id' => 533,
                'school_academic_year_id' => 3,
            ),
            67 => 
            array (
                'id' => 2222,
                'company_id' => 126,
                'block_id' => 534,
                'school_academic_year_id' => 3,
            ),
            68 => 
            array (
                'id' => 2223,
                'company_id' => 126,
                'block_id' => 535,
                'school_academic_year_id' => 3,
            ),
            69 => 
            array (
                'id' => 2224,
                'company_id' => 126,
                'block_id' => 536,
                'school_academic_year_id' => 3,
            ),
            70 => 
            array (
                'id' => 2225,
                'company_id' => 126,
                'block_id' => 537,
                'school_academic_year_id' => 3,
            ),
            71 => 
            array (
                'id' => 2226,
                'company_id' => 126,
                'block_id' => 538,
                'school_academic_year_id' => 3,
            ),
            72 => 
            array (
                'id' => 2227,
                'company_id' => 126,
                'block_id' => 539,
                'school_academic_year_id' => 3,
            ),
            73 => 
            array (
                'id' => 2228,
                'company_id' => 126,
                'block_id' => 540,
                'school_academic_year_id' => 3,
            ),
            74 => 
            array (
                'id' => 2229,
                'company_id' => 126,
                'block_id' => 541,
                'school_academic_year_id' => 3,
            ),
            75 => 
            array (
                'id' => 2230,
                'company_id' => 126,
                'block_id' => 542,
                'school_academic_year_id' => 2,
            ),
            76 => 
            array (
                'id' => 2231,
                'company_id' => 126,
                'block_id' => 543,
                'school_academic_year_id' => 2,
            ),
            77 => 
            array (
                'id' => 2232,
                'company_id' => 126,
                'block_id' => 544,
                'school_academic_year_id' => 2,
            ),
            78 => 
            array (
                'id' => 2233,
                'company_id' => 126,
                'block_id' => 545,
                'school_academic_year_id' => 2,
            ),
            79 => 
            array (
                'id' => 2234,
                'company_id' => 122,
                'block_id' => 546,
                'school_academic_year_id' => 2,
            ),
            80 => 
            array (
                'id' => 2235,
                'company_id' => 122,
                'block_id' => 547,
                'school_academic_year_id' => 2,
            ),
            81 => 
            array (
                'id' => 2236,
                'company_id' => 124,
                'block_id' => 548,
                'school_academic_year_id' => 2,
            ),
            82 => 
            array (
                'id' => 2237,
                'company_id' => 124,
                'block_id' => 549,
                'school_academic_year_id' => 2,
            ),
            83 => 
            array (
                'id' => 2238,
                'company_id' => 124,
                'block_id' => 550,
                'school_academic_year_id' => 2,
            ),
            84 => 
            array (
                'id' => 2239,
                'company_id' => 126,
                'block_id' => 552,
                'school_academic_year_id' => 2,
            ),
            85 => 
            array (
                'id' => 2240,
                'company_id' => 126,
                'block_id' => 553,
                'school_academic_year_id' => 2,
            ),
            86 => 
            array (
                'id' => 2241,
                'company_id' => 126,
                'block_id' => 554,
                'school_academic_year_id' => 2,
            ),
            87 => 
            array (
                'id' => 2248,
                'company_id' => 127,
                'block_id' => 555,
                'school_academic_year_id' => 12,
            ),
            88 => 
            array (
                'id' => 2249,
                'company_id' => 127,
                'block_id' => 555,
                'school_academic_year_id' => 1,
            ),
            89 => 
            array (
                'id' => 2250,
                'company_id' => 129,
                'block_id' => 556,
                'school_academic_year_id' => 2,
            ),
            90 => 
            array (
                'id' => 2251,
                'company_id' => 129,
                'block_id' => 557,
                'school_academic_year_id' => 12,
            ),
            91 => 
            array (
                'id' => 2252,
                'company_id' => 2,
                'block_id' => 2,
                'school_academic_year_id' => 2,
            ),
            92 => 
            array (
                'id' => 2253,
                'company_id' => 2,
                'block_id' => 2,
                'school_academic_year_id' => 3,
            ),
            93 => 
            array (
                'id' => 2254,
                'company_id' => 2,
                'block_id' => 2,
                'school_academic_year_id' => 4,
            ),
            94 => 
            array (
                'id' => 2255,
                'company_id' => 2,
                'block_id' => 2,
                'school_academic_year_id' => 5,
            ),
            95 => 
            array (
                'id' => 2256,
                'company_id' => 2,
                'block_id' => 2,
                'school_academic_year_id' => 6,
            ),
            96 => 
            array (
                'id' => 2257,
                'company_id' => 15,
                'block_id' => 34,
                'school_academic_year_id' => 2,
            ),
            97 => 
            array (
                'id' => 2258,
                'company_id' => 15,
                'block_id' => 34,
                'school_academic_year_id' => 3,
            ),
            98 => 
            array (
                'id' => 2259,
                'company_id' => 15,
                'block_id' => 34,
                'school_academic_year_id' => 4,
            ),
            99 => 
            array (
                'id' => 2260,
                'company_id' => 15,
                'block_id' => 34,
                'school_academic_year_id' => 5,
            ),
            100 => 
            array (
                'id' => 2261,
                'company_id' => 15,
                'block_id' => 34,
                'school_academic_year_id' => 6,
            ),
            101 => 
            array (
                'id' => 2262,
                'company_id' => 30,
                'block_id' => 58,
                'school_academic_year_id' => 2,
            ),
            102 => 
            array (
                'id' => 2263,
                'company_id' => 30,
                'block_id' => 58,
                'school_academic_year_id' => 3,
            ),
            103 => 
            array (
                'id' => 2264,
                'company_id' => 30,
                'block_id' => 58,
                'school_academic_year_id' => 4,
            ),
            104 => 
            array (
                'id' => 2265,
                'company_id' => 30,
                'block_id' => 58,
                'school_academic_year_id' => 5,
            ),
            105 => 
            array (
                'id' => 2266,
                'company_id' => 30,
                'block_id' => 58,
                'school_academic_year_id' => 6,
            ),
            106 => 
            array (
                'id' => 2267,
                'company_id' => 35,
                'block_id' => 77,
                'school_academic_year_id' => 2,
            ),
            107 => 
            array (
                'id' => 2268,
                'company_id' => 35,
                'block_id' => 77,
                'school_academic_year_id' => 3,
            ),
            108 => 
            array (
                'id' => 2269,
                'company_id' => 35,
                'block_id' => 77,
                'school_academic_year_id' => 4,
            ),
            109 => 
            array (
                'id' => 2270,
                'company_id' => 35,
                'block_id' => 77,
                'school_academic_year_id' => 5,
            ),
            110 => 
            array (
                'id' => 2271,
                'company_id' => 35,
                'block_id' => 77,
                'school_academic_year_id' => 6,
            ),
            111 => 
            array (
                'id' => 2272,
                'company_id' => NULL,
                'block_id' => 46,
                'school_academic_year_id' => 3,
            ),
            112 => 
            array (
                'id' => 2273,
                'company_id' => 1,
                'block_id' => 1,
                'school_academic_year_id' => 5,
            ),
        ));
        
        
    }
}