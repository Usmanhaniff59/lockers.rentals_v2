<?php

use Illuminate\Database\Seeder;

class FranchiseesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('franchisees')->delete();
        
        \DB::table('franchisees')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Franchisee Group 1',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 6,
                'name' => 'aaa',
                'created_at' => '2020-02-24 12:26:09',
                'updated_at' => '2020-02-24 12:26:09',
            ),
            2 => 
            array (
                'id' => 7,
                'name' => 'aaabbb1',
                'created_at' => '2020-02-24 12:28:10',
                'updated_at' => '2020-02-24 12:28:10',
            ),
            3 => 
            array (
                'id' => 8,
                'name' => 'test group 2',
                'created_at' => '2020-03-12 11:15:08',
                'updated_at' => '2020-03-12 11:15:08',
            ),
            4 => 
            array (
                'id' => 9,
                'name' => 'Perris Franchisee',
                'created_at' => '2020-09-16 07:32:12',
                'updated_at' => '2020-09-16 07:32:12',
            ),
        ));
        
        
    }
}