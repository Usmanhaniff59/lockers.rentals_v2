<?php

use Illuminate\Database\Seeder;

class LockerOffSalesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('locker_off_sales')->delete();
        
        \DB::table('locker_off_sales')->insert(array (
            0 => 
            array (
                'id' => 21,
                'locker_id' => 39076,
                'active' => 1,
                'delete' => 0,
                'created_at' => '2020-08-18 07:34:10',
                'updated_at' => '2020-08-18 07:34:10',
            ),
            1 => 
            array (
                'id' => 22,
                'locker_id' => 45456,
                'active' => 1,
                'delete' => 0,
                'created_at' => '2020-09-16 06:50:10',
                'updated_at' => '2020-09-16 06:50:10',
            ),
            2 => 
            array (
                'id' => 23,
                'locker_id' => 35566,
                'active' => 1,
                'delete' => 0,
                'created_at' => '2020-09-17 11:14:53',
                'updated_at' => '2020-09-17 11:14:53',
            ),
        ));
        
        
    }
}