<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\paymentConfig;


class DataImportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    // refund cients in sage pay
    // update all tx numbers in trasactions table
    // CONFIRM BOOKINGS
    // correct broken locker numbers

	//php artisan confirmation:send
    //php artisan send:checkhold
    //php artisan send:emails
    ///opt/cpanel/ea-php72/root/usr/bin/php artisan optimize
    //
    public function run()
    {

        $mysql_locker_rentals = DB::connection('mysql_locker_rentals');
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

		
		
		/*
	echo "resend bult confs \n";
		
		
		$errors = 0;
		$sales = DB::table('sales')
                        ->where('status','b')
                        ->where('start','2020-09-01 00:00:00')
                        //->where('company_id',47)                   
                  		->get();
                  		
                foreach($sales as $s)
        		{
        
        			$fix_possible = 0;
    
        			$lockers = DB::table('lockers')
                        ->where('id',$s->locker_id)                   
                        ->first();
            		
            			if(empty($lockers->id))
						{
							$errors++;
							echo "Locker Number is Blank";
            				echo " --- ";
            				
            				
	            			echo "ID: ".$s->id;
	            			echo "\n";
	            			
						}
						else
						{
            		
	            			if($s->number != $lockers->locker_number || $s->block_id != $lockers->block_id )
	            			{
	            			
	            				$errors++;
	            				$fix_possible = 1;
    
	            				
							 	if($s->number != $lockers->locker_number )
		            			{
		            				echo "Locker Number:".$s->number;
		            				echo "should be: ".$lockers->locker_number;
		            				echo " --- ";
		            			}
		            			
		            			if($s->block_id != $lockers->block_id )
		            			{
		            				echo "Block is:".$s->block_id;
		            				echo "should be: ".$lockers->block_id;
		            				echo " --- ";
		            			}
		            				
	            			
	            			
	            			$blocks = DB::table('blocks')
                        		->where('id',$s->block_id)                   
                        		->first();
                        	if($blocks->active == 0 || $blocks->display == 0){
                        			echo "Sales Block not Active:";
		            				echo " --- ";
                        	}
            		
	            			$blocksL = DB::table('blocks')
                        		->where('id',$lockers->block_id)                   
                        		->first();
                        	if($blocksL->active == 0 || $blocksL->display == 0){
                        			echo "Lockers Block not Active:";
		            				echo " --- ";
		            				$fix_possible = 0;
                        	}
                        	
                        	
                        	$salesCheck = DB::table('sales')
                        		->where('status','b')
                        		->where('start','2020-09-01 00:00:00')
                        		->where('locker_id',$lockers->id)
                        		->where('id','NOT',$s->id)                   
                  				->first();
                  			if(!empty($salesCheck->id)){
                  				echo "Double Booking";
		            			echo " --- ";
		            			$fix_possible = 0;
		            					
                  			}
        
							                        
	        				
	            			if($fix_possible == 1){
	            				echo " ---- Fixing ---- ";
	            					$arrayFix = [
		            				'number' => $lockers->locker_number,
		            				'block_id' => $lockers->block_id,
		            				'block_name' => $blocksL->name,
		            				'fix_db' => date('Y-m-d H:i:s')		
	            					];
	            					
	            					print_r($arrayFix);
	            			
	            					DB::table('sales')->where('id', $s->id)->update($arrayFix);
	        				
	            			
	            				
	        				}
	            			
	            			echo "ID: ".$s->id;
	            			echo "\n";
	            			
	            		
	            		}	
            			
        			}
        
        
        
        		}


		echo "\n Total Errors: ".$errors."\n";
		
		*/


/*

		
        
		
		
		
		
		$count = 0;
		$sales = DB::table('sales')
                        ->where('status','b')
                        ->where('start','2020-09-01 00:00:00')
                        ->where('company_id',125)                   
                  		->get();
                  		
                foreach($sales as $s)
        		{
        	
        			$companies = DB::table('companies')
			                        ->where('id', $s->company_id)
			                        ->first();
			       if(!empty($companies->name))        
		 			echo  "Company: ".$companies->name." ---\m";
		 			
		 			$count++;
		 			$arraysales = [
                    	'confirmation_sent' => 0,
                	];
                	DB::table('sales')->where('id', $s->id)->update($arraysales);

		              			
		              			
        		}


		echo "Total:".$count;
		
		*/
/*
		
		//manual Locker Code Check
		$count = 0;
		$mlc = DB::table('manual_locker_codes')
                        ->where(function ($status) {
			            	$status->where('start','2020-09-01 00:00:00')
			               ->orWhere('start','2020-09-01 23:59:59');
			             })     
			            ->orderBy('locker_id','ASC')                                                		
                        ->get();
    	foreach ($mlc as $m){
    		
    		
    		$lockers = DB::table('lockers')
                        ->where('id',$m->locker_id)                   
                        ->where('company_id',143)
                        ->where('active',1)
                        ->where('display',1)
                  		->first();
                if(!empty($lockers->id)) {
                
        	
        			$block = DB::table('blocks')
                        ->where('id',$lockers->block_id)                   
                        ->where('active',1)
                        ->where('display',1)
                  		->first();
                	if(!empty($block->id)) {
                
    		
			    		$code = 0;
			    		if(!empty($m->code))
			    			$code =  $m->code;
			    		
			    		$mlc_c = DB::table('manual_locker_codes_bk3')
			                        ->where('locker_id',$m->locker_id)
			                        ->where('id','!=',$m->id)
			                        ->where(function ($status) {
						            	$status->where('start','2020-08-31 00:00:00')
						               ->orWhere('start','2020-09-01 23:59:59');
						       })                                                     		
			                  ->first();
			            
			            
			            $code2 = 0;
			    		if(!empty($mlc_c->code))
			    			$code2 =  $mlc_c->code;
			    		
			    		            
			            if($code != $code2){
			            	
            				
        
                			                			
                			
			            	
			            	$sales = DB::table('sales')
			                        ->where('status','b')
			                        ->where('start','2020-09-01 00:00:00')
			                        ->where('locker_id',$m->locker_id)                   
			                        ->where('net_pass','!=',$code2)
			                  		->first();
			                if(!empty($sales->id))
			                {
			                	
			                	$companies = DB::table('companies')
			                        ->where('id', $lockers->company_id)
			                        ->first();
			                	if(!empty($companies->name))        
		              			echo  "Company: ".$companies->name." ---";
			            	
			            	
			                	echo "Booking ID: ".$sales->id." --- Block ID: ".$sales->block_id." --  locker Code:".$sales->net_pass."\n"; 
								$count++;
            		
            					echo $m->id." -- ";
                				echo "Locker Number: ".$lockers->locker_number." --- "; 
                				echo "Locker ID: ".$lockers->id." --- "; 
                				echo "CODE: ".$code2." --- "; 
                				echo "MLC: ".$m->id." --- "; 
                				
                			//	
                			//	DB::table('sales')->where('id', $sales->id)->update(
            	 			//	[
                          	//	  	'net_pass' => $code2, 
                          	///	  	'confirmation_sent' => 0,
                            	//]
               					//);
               					
               					echo "\n\n";
                			
							
			                
							}
							
							//DB::table('manual_locker_codes')->where('id', $m->id)->update([
                          	//	  	'code' => $code2, 
                            //	]);
               					
							
				

			            
	            
	            	}
	            	
				}

            	
            	
            	
            	
            	
            	
            	$count++;
            }
        	
        	            
    	

    	}
    	
    	echo "Total: ".$count."\n\n\n";
          	  
         
         */
         

		/*
		//manual Locker Code Check
		$count = 0;
		$mlc = DB::table('manual_locker_codes')
                        ->where(function ($status) {
			            	$status->where('start','2020-09-01 00:00:00')
			               ->orWhere('start','2020-09-01 23:59:59');
			             })     
			            ->orderBy('locker_id','ASC')                                                		
                        ->get();
    	foreach ($mlc as $m){
    		
    		$mlc_c = DB::table('manual_locker_codes')
                        ->where('id','!=',$m->id)
                        ->where('locker_id',$m->locker_id)
                        ->where(function ($status) {
			            	$status->where('start','2020-09-01 00:00:00')
			               ->orWhere('start','2020-09-01 23:59:59');
			             })                                                     		
                        ->first();
            if(!empty($mlc_c->locker_id)){
            	
            	echo $mlc_c->locker_id."\n";
            	
            	
            	$sales = DB::table('sales')
                        ->where('status','b')
                        ->where('start','2020-09-01 00:00:00')
                        ->where('locker_id',$m->locker_id)                   
                  		->first();
                if(!empty($sales->id))
                	echo "Booking ID: ".$sales->id." --- Block ID: ".$s->block_id." --  locker Code:".$sales->net_pass."\n"; 
		
				
            	$lockers = DB::table('lockers')
                        ->where('id',$m->locker_id)                   
                  		->first();
                if(!empty($lockers->id)) {
                	echo "Locker: ".$lockers->locker_number."\n"; 
                	
                	$companies = DB::table('companies')
	                        ->where('id', $lockers->company_id)
	                        ->first();
	                if(!empty($companies->name))        
	            		echo  "Company: ".$companies->name."\n";
	            
	            
				}
				
				
            	
            	echo "\n\n";
            	
            	
            	$count++;
            }
        	
        	            
    	

    	}
    	
    	echo "Total: ".$count."\n\n\n";
         */ 	  
		


		/*
		Sybil Andrews= 90
		Keswick 33
		Arden
		Kingsmead
		The Windsor Boys School – You have sent this already
		St Catherine’s
		*/
		
		/*
		$arrayNotSend = [
		'90',
		'33',
		'27',
		'139',
		'99',
		'47',
		];
		
		$count=0;
		$array = array();
		$sales = DB::table('sales')
                        ->where('status','b')
                        ->where('start','2020-09-01 00:00:00')
                        //->where(function ($status) {
			            //	$status->where('active', '0')
			              //  ->orWhere('display', '0');
			             //})                                               
                  		->get();
		foreach ($sales as $s){
		
			
            
			$error = 1;
			$mlc = DB::table('manual_locker_codes')
                        ->where('locker_id',$s->locker_id)
                        ->where(function ($status) {
			            	$status->where('start','2020-09-01 00:00:00')
			               ->orWhere('start','2020-09-01 23:59:59');
			             })                                                     		
                        ->first();
            
                        
                        
            $net_pass =0;    
            $code = 0;
                    
            if(!empty($mlc->code))            
            	$code = $mlc->code;
            
            if(!empty($s->net_pass))
            	$net_pass = $s->net_pass;
           
           if(!empty($net_pass)){
           	
           	$code = ltrim($code, '0');
            $net_pass = ltrim($net_pass, '0');
        	
            $code =  substr(sprintf("%04d",$code),  -4);
            $net_pass =  substr(sprintf("%04d",$net_pass),  -4);
        
           } 
        
      		
      
          if($code == $net_pass || $code == 0000)
			$error = 0;
				
			if(!empty($error))
			{
				
				$s->net_pass = $code;	
				
				echo "ID:".$s->id;
				echo "Code:".$code." / ".$net_pass;
				echo "-- booking Type:".$s->booking_type;	
				echo "\n";		
			//$a = date("Y-m-d", strtotime($s->created_at));
				
				
				
			if (in_array($s->company_id, $arrayNotSend)) {
				   	
					$companies = DB::table('companies')
	                        ->where('id', $s->company_id)
	                        ->first();
	            $a = $companies->name;
	            
					if(empty($array[$a]))
	            		$array[$a] = 1;
	            	else
	            		$array[$a]++;
	            		
	            		//DB::table('sales')->where('id', $s->id)->update(
            	 		//	[
                          ///  	'net_pass' => $code, 
                            //]
                        //);
	            
			} else {
				$count++;
				
				//if($count ==500)
             	
            	 //	DB::table('sales')->where('id', $s->id)->update(
            	 	//		[
                      //      	'confirmation_sent' => 0,
                        //    	'net_pass' => $code, 
                          //  ]
            //            );
				}
						
			}
			
			    
            
			
			
			
		}    

		
		echo "------ Total".$count;

		
		print_r($array);







/*
		$blocks = DB::table('blocks')
                            ->where('company_id', 139)
                            ->get();
        foreach ($blocks as $b){
        	
        	    $codesw = DB::table('manual_locker_codes_today')
            		->where('block_id', $b->id)
                	->get();
                foreach ($codesw as $c){
                	
                	   $array_locker = [
				              'code'=> $c->code,
			     	   ];
				      
				     echo $c->id. "- ". $c->code."\n";
                	DB::table('manual_locker_codes')->where('id', $c->id)->update($array_locker);
				
                }        
            
        }
        */

        //fix user has role



/*
		
		Lostock Hall Academy = 78
		Ulverston Victoria High School – In Recycle Bin = 75
		St Catherine’s  - Year 8 = 47
		Beth’s Grammar School =49
		Langley Park School for Girls = 135
		Haileybury Turnford = 108
		Sybil Andrews = 90
 
 */
 
		
		
		/*
		
		$array_school = [
		//'75',
	//	'135',
		];
		
		$array_school = [
		////'78',
		//'75',
		////'47',
		////'49',
		//'135',
		////'108',
		////'90',
		99
		];
		
		
		
		
		foreach ($array_school as $as)
		{
			
			
			$companies = DB::table('companies')
                        ->where('id', $as)
                        ->first();
            echo $as." - ";        
            echo $companies->name."\n";
            
            $inactive_blocks = DB::table('blocks')
                        ->where('company_id', $as)
                        ->where('display',0)
                        //->where(function ($status) {
			            //	$status->where('active', '0')
			              //  ->orWhere('display', '0');
			             //})                                               
                  		->get();
            	foreach ($inactive_blocks as $ib){
            		
            		echo $ib->name;
            		
            		
            		$lockers_old = DB::table('lockers')
                        ->where('block_id', $ib->id)	
                        ->where('active',1)
                        ->where('display',1)
                        ->get();
                    foreach ($lockers_old as $lo){
            
            			        	
            	
            
                    $new_blocks = DB::table('blocks')
                        ->where('company_id', $as)
                        ->where('previous_block_id', $ib->id)
                        ->get();
                    foreach ($new_blocks as $nb){
                    	
                            		        	
		                    $c =0;
		                    $old_locker_id = $lo->id;	
		                    $lockers_check = DB::table('lockers')
		                        ->where('company_id', $as)
		                        ->where('locker_number',$lo->locker_number)
		                        ->where('block_id',$nb->id)
		                        ->where('id','!=',$lo->id)
		                        ->where('active',1)
		                        ->where('display',1)
		                        ->where('id','>',$lo->id)
		                        ->get();
		                   foreach ($lockers_check as $lc){
		                	$c++;	
		                	$new_locker_id = $lc->id;
		                   }
                   
		                   if ($c == 1){
		                   	
		                
		                   	
			                   	$man_code = DB::table('manual_locker_codes')
			                        ->where('locker_id', $old_locker_id)
			                        ->where('start','2020-09-01 00:00:00')
			                        ->first();
			                   if(empty($man_code->code)){
			                   	
			                 		 echo $lo->id.") -- Old ID: ".$old_locker_id." -- New Locker ID:".$new_locker_id." ERROR NO MAN CODE\n";
			                   	
			                   } else {
	                   	
	                   	
			                   		$new_man_code = DB::table('manual_locker_codes')
			                        	->where('locker_id', $new_locker_id)
			                        	->where('start','2020-09-01 00:00:00')
			                        	->first();
			                   		if(!empty($new_man_code->id)){
			                   	
			                   			//		echo "great";
			                   			echo $new_man_code->id.")-- OLD Code:".$new_man_code->code."\n";
			                   			echo $man_code->id.") -- New Code: ".$man_code->code." NEW MAN CODE\n";
										        
			                   
				                   		$array_locker = [
				                   			'code'=> $man_code->code,
				                   		];
				                   	
				                    	DB::table('manual_locker_codes')->where('id', $new_man_code->id)->update($array_locker);
				
									}	
									else
									echo "error no code";
				                   	
				                   	
				              }
                   	
                   	
                   		} else if($c > 1)
                   		echo $lo->id.") -- Company ID: ".$lo->company_id." -- Locker Number:".$lo->locker_number." we have dup".$c."\n";
                   
                    	
                    
                    
                    }	
                    }
            	
            		
            		
            		
            	}
            	
            echo "\n";
            
            
            
		}
		

*/

        /*
                $users = DB::table('users')
                    ->get();
                foreach ($users as $u) {

                    $user_has_roles = DB::table('user_has_roles')
                        ->where('user_id', $u->id)
                        ->first();
                    if (empty($user_has_roles->role_id)) {
                        echo "No Role: " . $u->id . " \n";
                        echo "Email: " . $u->email;
                        echo "\n\n";
                        //4

                        DB::table('user_has_roles')->insert([
                            'role_id' => 4,
                            'user_id' => $u->id,
                        ]);

                        DB::table('users')->where('id', $u->id)->update([
                            'bug' => $u->bug . " ---- NO ROLE: " . date("Y-m-d H:i:s") . " ---- ",
                        ]);


                    }
                }

*/


        /*
        //SELECT * FROM `sales` WHERE company_id = 27 && (status = 'r' || (status ='r' && (user_id = 2 || user_id = 319))) and start = '2020-09-01 00:00:00'
        // booking status incorrect
        //Move locker and email
        $richards_booking = '156214';
        $customer_booking_move = '157944';
        echo 'id = '.$richards_booking.' || '.$customer_booking_move."\n";
        $sales = DB::table('sales')
            ->where('id',$customer_booking_move )
            ->first();
        if (!empty($sales->id)){



            $salesOther = DB::table('sales')
                ->where('id',$richards_booking)
                ->first();
            if(!empty($salesOther->id) && ($salesOther->user_id==319 || $salesOther->user_id == 2)) {


                $com = DB::table('companies')
                    ->where('id',$sales->company_id)
                    ->first();

                $html = "<p>Good Afternoon</p>";

                $html.= "<p>Due to a issue with your reservation we have had to change your locker number to <b>".$salesOther->number."</b> at ".$com->name.", 
                you will recieve a confirmation email shortly.</p>";

                $html.= "<p>We are sorry for any inconvinience caused.</p>";

                $html.= "<p>Kind Regards</p>";

                $html.= "<p>Web Admin<br>Locker.Rentals</p>";



                $arrayEmail = [
                    'email' => $sales->email,
                    'action' => 'sent',
                    'subject' => 'Your Locker has been Moved',
                    'mail_body' => $html,
                    'date_added' => date("Y-m-d H:i:s"),
                    'type' => 'email',
                    'result_code' => '0',
                    'priority' => '2',
                    'star' => '0',
                    'send' => '0',
                    'complete' => '0',
                    'deleted' => '0',
                ];
                $mass_change_id = DB::table('emails')->insertGetId($arrayEmail);



                $arraysales = [
                    'locker_id' => $salesOther->locker_id,
                    'number' => $salesOther->number,
                    'net_pass' => null,
                    'confirmation_sent' => 0,
                ];
                DB::table('sales')->where('id', $sales->id)->update($arraysales);

                $arraysales = [
                    'status' => 'l',
                ];

                DB::table('sales')->where('id', $salesOther->id)->update(['status' => 'l']);
            }


        }
        */




        // booking status incorrect
        /*
        $c = 0;
        $sales = DB::table('sales')
            ->where('start', '2020-09-01 00:00:00')
            ->where('price', null)
            ->where('status', 'b')
            ->get();
        foreach ($sales as $s) {


            $dontProcess = 0;
            $transactions = DB::table('transactions')
                ->where('booking_id', $s->booking_id)
                ->where('status', '0000')
                ->first();
            if (!empty($transactions->id))
                $dontProcess = 1;


            if (!empty($dontProcess))
                continue;

            $c++;



            $man_locker_codes = DB::table('manual_locker_codes')
                ->where('locker_id', $s->locker_id)
                ->where('start', date("Y-08-31 00:00:00", strtotime($s->start)))
                ->first();
            if (!empty($man_locker_codes->id)) {


                $new_code = rand(1000, 9999);
                $man_locker_codes->code = $new_code;
                echo "New Code: " . $new_code . "\n";

                 DB::table('manual_locker_codes')->where('id', $man_locker_codes->id)->update([
                    'code'=> $new_code,
                ]);







            }


            echo "\n";

            $com = DB::table('companies')
                ->where('id',$s->company_id)
                ->first();


            $purchase_link = '<a style="color:green;" href="'.url('/admin/reserved/lockers/schooltermbasedreserved/'.$s->booking_id).'"><u>Purchase</u></a>';

            $html = "<p>Good Morning</p>";

            $html.= "<p>You have recently attempted to purchase locker number ".$s->number." at <b>".$com->name."</b>, however we have not recieved the payment. We have changed your booking from the status 'Booked' to 'Reserved'. 
            You have until Saturday 13th June 2020 to <b>".$purchase_link."</b> this locker. Please complete the
                payment by Saturday 13th June 2020 otherwise the locker will be available for other people to purchase and this link will no longer work.</p>";

            $html.= "<p>If you think you have paid, plesae proivide the date of the payment and thae last 4 digits of your card number by reply email and we will update your booking.</p>";

            $html.= "<p>We are sorry for any inconvinience caused.</p>";

            $html.= "<p>Kind Regards</p>";

            $html.= "<p>Web Admin<br>Locker.Rentals</p>";



            $arrayEmail = [
                'email' => $s->email,
                'action' => 'sent',
                'subject' => 'Refund from Locker Rentals and New Booking Link',
                'mail_body' => $html,
                'date_added' => date("Y-m-d H:i:s"),
                'type' => 'email',
                'result_code' => '0',
                'priority' => '2',
                'star' => '0',
                'send' => '0',
                'complete' => '0',
                'deleted' => '0',
            ];

            echo "Booking ID " . $s->booking_id . "\n";
            echo "Booking ID " . $new_code . "\n";
            $mass_change_id = DB::table('emails')->insertGetId($arrayEmail);


            $arraysales = [
                'hold_booking_date' => '2020-06-13 23:59:59',
                'net_pass' => null,
                'status' => 'r',
            ];
            DB::table('sales')->where('id', $s->id)->update($arraysales);

        }

        */


        /*
         *
        send email out to peope
        $c=0;
        $sales = DB::table('sales')
            ->where('start','2020-09-01 00:00:00')
            ->where('price',null)
            ->where('status','b')
            ->get();
        foreach ($sales as $s){

            if($s->company_id == 86)
                continue;

            $paymnets = 0;
            $refunds = 0;
            $payment_id = 0;
            $transactions = DB::table('transactions')
                ->where('booking_id',$s->booking_id)
                ->where('status','0000')
                ->get();
            foreach ($transactions as $t) {

                $payment_id = $t->payment_id;

                if($t->status == '0000') {
                    $paymnets = $paymnets + $t->amount;
                    $refunds = $refunds + $t->refund;
                }

            }





            $total = $paymnets-$refunds;

            if(!empty($total)){

                $c++;
                $mass_refunds = DB::table('mass_refunds')
                    ->where('booking_id',$s->booking_id)
                    ->where('completed',0)
                    ->first();
                if(empty($mass_refunds->id)){

                    $mass_refund = DB::table('mass_refunds')->insertGetId([
                        'booking_id' => $s->booking_id,
                        'completed' => 0,
                    ]);

                }


                $mass_change = DB::table('transactions')
                    ->where('booking_id',$s->booking_id)
                    ->where('notes','Refund in Bulk')
                    ->first();
                if(empty($mass_change->id)){

                    $array = [
                        'booking_id' => $s->booking_id,
                        'payment_id' =>  $payment_id,
                        'user_id' =>  2,
                        'type' =>  '05',
                        'amount_change' => $total,
                        'notes' => 'Refund in Bulk',
                        'void' =>0,
                        'waiting_response_sagepay' =>0,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ];

                    $mass_change_id = DB::table('transactions')->insertGetId($array);

                }


                $com = DB::table('companies')
                    ->where('id',$s->company_id)
                    ->first();


                echo "Sale".$s->id."\n";
                echo "comp".$com->name."\n";
                echo "total:".$total."\n";

                echo "\n";

                $purchase_link = '<a style="color:green;" href="'.url('/admin/reserved/lockers/schooltermbasedreserved/'.$s->booking_id).'"><u>Purchase</u></a>';
                $html = "<p>Good Evening</p>";

                $html.= "<p>You have recently purchased locker number ".$s->number." at <b>".$com->name."</b> however due to administration error you have been charged the incorrect amount.
                We have refunded your payment in full and and created you a new reservation for the locker. If you wish to <b>".$purchase_link."</b> this locker please complete the
                payment by Friday 12th June 2020 otherwise the locker will be available for other people to purchase and this link will no longer work.</p>";

                $html.= "<p>We are sorry for any inconvinience caused.</p>";

                $html.= "<p>Kind Regards</p>";

                $html.= "<p>Web Admin<br>Locker.Rentals</p>";



                $arrayEmail = [
                    'email' => $s->email,
                    'action' => 'sent',
                    'subject' => 'Refund from Locker Rentals and New Booking Link',
                    'mail_body' => $html,
                    'date_added' => date("Y-m-d H:i:s"),
                    'type' => 'email',
                    'result_code' => '0',
                    'priority' => '2',
                    'star' => '0',
                    'send' => '0',
                    'complete' => '0',
                    'deleted' => '0',
                ];


                $mass_change_id = DB::table('emails')->insertGetId($arrayEmail);

                $man_locker_codes = DB::table('manual_locker_codes')
                    ->where('locker_id', $s->locker_id)
                    ->where('start',date("Y-08-31 00:00:00",strtotime($s->start)))
                    ->first();
                if(!empty($man_locker_codes->id)){

                    $new_code = rand(1000,9999);
                    $man_locker_codes->code = $new_code;
                    echo "New Code: ".$new_code . "\n";


                        DB::table('manual_locker_codes')->where('id', $man_locker_codes->id)->update([
                        'code'=> $new_code,
                        ]);

                   }

                $arraysales = [
                    'hold_booking_date' => '2020-06-13 23:59:59',
                    'net_pass' => null,
                    'status' => 'r',
                ];
                DB::table('sales')->where('id', $s->id)->update($arraysales);





            }



        }



        echo "\nCount:".$c."\n";

        */



/*
        $c=0;
        $sales = DB::table('sales')
            ->where('title','RICH_REFUND')
            ->get();
        foreach ($sales as $s){

                $html = "<p>Hello</p>";

                $html.= "<p>We sent you a e-mail recently giving you the option to renew your original locker in Year 7. Unfortunately this was sent in error as these lockers are allocated to Year 7 students only. Please forgive the confusion this may haves caused.</p>";

                $html.= "<p>We have issued you a full refund and invite to visit website www.locker.renatls to look at booking an alternative locker for Year 8.</p>";

                $html.= "<p>I trust this is in order.</p>";

                $html.= "<p>Kind Regards</p>";

                $html.= "<p>Web Admin<br>Locker.Rentals</p>";

                $arrayEmail = [
                    'email' => $s->email,
                    'action' => 'sent',
                    'subject' => 'Refund from Locker Rentals',
                    'mail_body' => $html,
                    'date_added' => date("Y-m-d H:i:s"),
                    'type' => 'email',
                    'result_code' => '0',
                    'priority' => '2',
                    'star' => '0',
                    'send' => '0',
                    'complete' => '0',
                    'deleted' => '0',
                ];
                $mass_change_id = DB::table('emails')->insertGetId($arrayEmail);

                $arraysales = [
                    'title' => 'LOCKER',
                ];
                DB::table('sales')->where('id', $s->id)->update($arraysales);

                $c++;


            }



*/





        /*
        $arraydup = array();
        $where = '';
        $sales = DB::table('sales')
            ->where('status','b')
            ->where('start','2020-09-01 00:00:00')
            ->get();
        foreach ($sales as $s){

            if (!in_array($s->id, $arraydup)) {

                $salesC = DB::table('sales')
                    ->where('locker_id', $s->locker_id)
                    ->where('start', '2020-09-01 00:00:00')
                    ->where('status', 'b')
                    ->where('id', 'NOT LIKE', $s->id)
                    ->first();
                if (!empty($salesC->id)) {

                    if (in_array($salesC->id, $arraydup))
                        continue;

                    array_push($arraydup, $s->id, $salesC->id);


                    $companies = DB::table('companies')
                        ->where('id', $salesC->company_id)
                        ->first();

                    echo "dup:" . $s->booking_id . "\n";
                    echo "dup:" . $salesC->booking_id . "\n";
                    echo "dup:" . $companies->name . "\n";

                    echo "where booking_id=" . $s->booking_id . " || booking_id =", $salesC->booking_id, "\n";
                    echo "\n";


                }
            }

        }

          print_r( $arraydup);
        */




        // check sales with no payments
        // check sales with double booked reservations
        //






        /*
        $c = 0;
        $sales = DB::table('sales')
            ->where('company_id', 125)
            ->where('status','r')
            ->where('title','LOCKER_M')
            ->where('start','2020-09-01 00:00:00')
            //->limit('1')
            ->get();
        foreach ($sales as $s) {
          $c++;

            $booking_id = $s->booking_id;
            $user_id = $s->user_id;
            $currency = 'GBP';
            $payment = DB::table('payments')
                ->where('booking_id',$s->booking_id)
                ->first();
            if(empty($payment->id)){


                echo "no ID\n";

                $payment_id = DB::table('payments')->insertGetId([
                    'user_id' => $user_id,
                    'booking_id' => $booking_id,
                    'currency' => $currency,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                 ]);





            } else
                $payment_id = $payment->id;


            if(empty($payment_id))
                continue;

            $amount = DB::table('sales')
                ->where('booking_id',$s->booking_id)
                ->sum('price');

            if(empty($amount) && $s->company_id == 121)
                $amount = 30;
            if(empty($amount) && $s->company_id == 122)
                $amount = 18;

            if(empty($amount)) {
                echo "ID" . $s->booking_id . "\n";
                echo "Price" . $amount . "\n\n";
            }

            continue;

            $paymentArray = [
                'amount' => $amount ,
                'account_balance' => $amount ,
                'has_transaction' => 1,
            ];

            DB::table('payments')->where('id', $payment_id)->update($paymentArray);



            $tax = round($amount*0.2,2);
            $transactions = DB::table('transactions')
                ->where('booking_id',$s->booking_id)
                ->where('status','0000')
                ->first();
            if(empty($transactions->id)){

                $transactionArray = [
                    'booking_id' => $booking_id,
                    'payment_id' => $payment_id,
                    'user_id' => $user_id,
                    'vendor_tx_code' => "lockers_".date("YmdHis")."_".$booking_id,
                    'currency' => $currency,
                    //'last4Digits' => $last4Digits,
                    'amount' => $amount,
                    'type' => '07',
                    'status' => '0000',
                    'status_message' => 'Manual Invoice',
                    'fraud_score' => 0,
                    // 'transaction_id' => $transactionId,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                ];
                print_r($transactionArray);
                $transaction_id = DB::table('transactions')->insertGetId($transactionArray);

            } else
                $transaction_id = $transactions->id;



            if(!empty($transactions->id) && !empty($payment->id)){

                DB::table('sales')->where('id', $s->id)->update([
                    'status' => 'b',
                    'price' => $amount,
                    'tax' => $tax,
                    'updated_at' => date("Y-m-d H:i:s"),
                    'confermation_sent' =>1,
                ]);

            }



           // echo "sale ID :".$s->id."\n";
          //  echo "Payment ID :".$payment->id."\n";



        }

        echo "COUNT:".$c;

        */



        /*

                        //import missed payments
                        $command = 'getTransactionList';
                        $vendor = 'lockers';
                        $user = 'developer';
                        $password = 'Prefect0207!';
                        $Integration_key = 'SG6RCBX2ZcdvfGUdjBxHJfITrMkL6TLyvNDAcrxqhaIz4QpSkt';
                        $Integration_password = 'CJrFR7I1Ss9MpjrJyqQj4JUcFWVueDZX9sZmwOKvowzu3GdQfHRyiNDUAq0R7KBzP';
                        $Authorization = $Integration_key . ':' . $Integration_password;
                        $Authorization = base64_encode($Authorization);


                        //in cron just do a couple of days
                        $startdate = date("d/m/Y 00:00:00", strtotime("-3 day"));
                        $enddate = date("d/m/Y 00:00:00");

                        $string = '<command>'.$command.'</command><vendor>'.$vendor.'</vendor><user>'.$user.'</user><startdate>'.$startdate.'</startdate><enddate>'.$enddate.'</enddate>';
                        $crypt = MD5($string . '<password>' . $password . '</password>');
                        $xml = 'XML=<vspaccess>'.$string.'<signature>'.$crypt.'</signature></vspaccess>';

                        $curl = curl_init('https://live.sagepay.com/access/access.htm');
                        curl_setopt($curl, CURLOPT_FAILONERROR, true);
                        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($curl, CURLOPT_POST, true);
                        curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);
                        $result = curl_exec($curl);
                        curl_close($curl);
                        $rxml = new SimpleXMLElement($result);

                        $start_row = $rxml->transactions->startrow;
                        $end_row = $rxml->transactions->endrow;
                        $totalrows = $rxml->transactions->totalrows;
                        $loops = ceil($totalrows/50);

                        $error_log = array();

                        for ($x = 0; $x < $loops; $x++) {


                            if (!empty($x)) {
                                $start_row = $end_row+1;
                                $end_row = $start_row + 49;

                                $string = '<command>'.$command.'</command><vendor>'.$vendor.'</vendor><user>'.$user.'</user><startdate>'.$startdate.'</startdate><enddate>'.$enddate.'</enddate><startrow>'.$start_row.'</startrow><endrow>'.$end_row.'</endrow>';
                                $crypt = MD5($string . '<password>' . $password . '</password>');
                                $xml = 'XML=<vspaccess>' . $string . '<signature>' . $crypt . '</signature></vspaccess>';

                                $curl = curl_init('https://live.sagepay.com/access/access.htm');
                                curl_setopt($curl, CURLOPT_FAILONERROR, true);
                                curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
                                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                                curl_setopt($curl, CURLOPT_POST, true);
                                curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);
                                $result = curl_exec($curl);
                                curl_close($curl);
                                $rxml = new SimpleXMLElement($result);

                            }

                            echo "\n\n\n";
                            echo $x . "/" . $loops . "\n";
                            echo  "row_number: ".$start_row . " - " . $end_row . " Total " . $totalrows . "\n\n\n\n\n\n";



                            foreach ($rxml->transactions->transaction as $transx) {

                                $error = 0;
                                //$row = $transx->rownumber;
                                //echo "row:".$row."\n";
                               // print_r($transx);
                                //continue;


                                //Tidy up arrays
                                $con = json_encode($transx->vpstxid);
                                $newArr = json_decode($con, true);
                                $transactionId = $newArr[0];

                                $con = json_encode($transx->vendortxcode);
                                $newArr = json_decode($con, true);
                                $vendor_tx_code = $newArr[0];

                                $str_array = explode("_", $vendor_tx_code);
                                $booking_id = $str_array[2];

                                if (empty($booking_id)) {

                                    $failStatus = 'no_booking';
                                    if (empty($error_log[$failStatus]))
                                        $error_log[$failStatus] = array($vendor_tx_code);
                                    else
                                        array_push($error_log[$failStatus], $vendor_tx_code);
                                    $error = 1;

                                }

                                $con = json_encode($transx->vendortxcode);
                                $newArr = json_decode($con, true);
                                $vendor_tx_code = $newArr[0];

                                $con = json_encode($transx->currency);
                                $newArr = json_decode($con, true);
                                $currency = $newArr[0];

                                $con = json_encode($transx->last4digits);
                                $newArr = json_decode($con, true);
                                $last4Digits = $newArr[0];

                                $con = json_encode($transx->amount);
                                $newArr = json_decode($con, true);
                                $amount = $newArr[0];
                                $amount_b = $newArr[0];

                                $con = json_encode($transx->t3mscore);
                                $newArr = json_decode($con, true);
                                $fraud_score = '';
                                if (!empty($fraud_score))
                                    $fraud_score = $newArr[0];

                                $date = $transx->started;
                                $date = substr($date, 0, strpos($date, "."));
                                $date = str_replace("/", "_", $date);
                                $date = str_replace(":", "_", $date);
                                $date = str_replace(" ", "_", $date);
                                $dateArray = explode('_', $date);
                                $date = $dateArray[2]."-".$dateArray[1]."-".$dateArray[0]." ".$dateArray[3].":".$dateArray[4].":".$dateArray[5];
                                $transaction_date = date("Y-m-d H:i:s", strtotime($date));

                                $user_id = '';
                                $sales = DB::table('sales')
                                    ->where('booking_id', $booking_id)
                                    ->get();
                                foreach ($sales as $s) {

                                    if (!empty($s->user_id)) {
                                        $user_id = $s->user_id;

                                    }

                                }


                                if(empty($user_id)){

                                    $failStatus = 'no_users';
                                    if (empty($error_log[$failStatus]))
                                        $error_log[$failStatus] = array($vendor_tx_code);
                                    else
                                        array_push( $error_log[$failStatus],$vendor_tx_code);

                                    $error = 1;
                                }





                                if (!empty($user_id) && !empty($booking_id)) {

                                    $payments = DB::table('payments')
                                        ->where('booking_id', $booking_id)
                                        ->first();
                                    if (!empty($payments->id)) {
                                        $payment_id = $payments->id;

                                    } else {

                                        $payment_id = DB::table('payments')->insertGetId([
                                            'user_id' => $user_id,
                                            'booking_id' => $booking_id,
                                            'currency' => $currency,
                                            'created_at' => $transaction_date,
                                            'updated_at' => $transaction_date,

                                        ]);

                                    }
                                }

                                if (empty($payment_id)) {

                                    $failStatus = 'no_payments';
                                    if (empty($error_log[$failStatus]))
                                        $error_log[$failStatus] = array($vendor_tx_code);
                                    else
                                        array_push($error_log[$failStatus], $vendor_tx_code);
                                    $error = 1;

                                }


                                $retrieve_transaction_url = 'https://pi-live.sagepay.com/api/v1/transactions/<transactionId>';
                                $retrieve_transaction_url = str_replace("<transactionId>", $transactionId, "$retrieve_transaction_url");
                                $curl_p = curl_init();
                                curl_setopt_array($curl_p, array(
                                    CURLOPT_URL => $retrieve_transaction_url,
                                    CURLOPT_RETURNTRANSFER => true,
                                    CURLOPT_HTTPHEADER => array(
                                        "Authorization: Basic $Authorization",
                                        "Cache-Control: no-cache"
                                    ),
                                ));

                                $retrieve_transaction_response = curl_exec($curl_p);
                                $retrieve_transaction_response = json_decode($retrieve_transaction_response);
                                curl_close($curl_p);

                                //echo "Trans: ".$transactionId."\n";
                                //echo "Vendor: ".$vendor_tx_code."\n";

                                $status = '2000.xweb';
                                $status_message = 'No Response';


                                if (!empty($retrieve_transaction_response->statusCode))
                                    $status = $retrieve_transaction_response->statusCode;

                                $status_message = 'No Response';
                                if (!empty($retrieve_transaction_response->statusDetail))
                                    $status_message = $retrieve_transaction_response->statusDetail;

                                if (empty($retrieve_transaction_response->{'3DSecure'}->status) || $retrieve_transaction_response->{'3DSecure'}->status == 'NotChecked')
                                    $type = '02';
                                else
                                    $type = '01';





                                if(empty($retrieve_transaction_response->transactionType) || $status != 0000) {
                                    $amount = null;
                                    $refund = null;
                                }
                                else if ($retrieve_transaction_response->transactionType == 'Refund')
                                {
                                    $amount = null;
                                    $refund = $amount;
                                    $type = '06';

                                } else {
                                    $refund = null;
                                }


                                //Get Charity value
                                $charity = null;
                                if($type == '01' || $type == '02') {

                                    if (!empty($booking_id) && !empty($amount)) {

                                        $salesValue = DB::table('sales')
                                            ->where('booking_id', $booking_id)
                                            ->sum('price');
                                        if (!empty($salesValue)) {


                                            $totalAmount = $amount;
                                            $transactionsValue = DB::table('transactions')
                                                ->where('booking_id', $booking_id)
                                                ->where('transaction_id','NOT LIKE',$transactionId)
                                                ->sum('amount');
                                            if (!empty($transactionsValue)) {
                                                $totalAmount = $transactionsValue + $totalAmount;
                                            }


                                            //charity_value
                                            if ($salesValue > $totalAmount) {

                                                $failStatus = 'charity_value_error';
                                                $postArray = [
                                                    'vendor_tx_code' => $vendor_tx_code,
                                                    'salesValue' => $salesValue,
                                                    'totalAmountPaid' => $totalAmount
                                                ];

                                                if (empty($error_log[$failStatus]))
                                                    $error_log[$failStatus] = array($postArray);
                                                else
                                                    array_push($error_log[$failStatus], $postArray);

                                                $error = 1;

                                            } else if ($totalAmount != $salesValue) {

                                                $charity = $amount-$salesValue;
                                                $amount = $amount-$charity;

                                            }




                                        } else {

                                            //run log that will be checked regularly to review errors
                                            $failStatus = 'no_sales';
                                            if (empty($error_log[$failStatus]))
                                                $error_log[$failStatus] = array($vendor_tx_code);
                                            else
                                                array_push($error_log[$failStatus], $vendor_tx_code);

                                            $error = 1;

                                        }


                                    }

                                }



                                if (empty($error)) {



                                    $transactionsExists = DB::table('transactions')
                                        ->where('transaction_id', $transactionId)
                                        ->first();
                                    if(empty($transactionsExists->id)) {

                                            if(empty($fraud_score))
                                                $fraud_score = 0;

                                            $failStatus = 'no_transaction_'.$status;
                                            if (empty($error_log[$failStatus]))
                                                $error_log[$failStatus] = 1;
                                            else
                                                $error_log[$failStatus] = $error_log[$failStatus] + 1;

                                            $add = 0;
                                            //if($status == '2000.xweb')
                                                $add = 1;

                                            if(!empty($charity)) {

                                                $transactionArrayCharity = [
                                                    'booking_id' => $booking_id,
                                                    'payment_id' => $payment_id,
                                                    'user_id' => $user_id,
                                                    'vendor_tx_code' => $vendor_tx_code,
                                                    'currency' => $currency,
                                                    'last4Digits' => $last4Digits,
                                                    'amount' => $charity,
                                                    'type' => '03',
                                                    'refund' => $refund,
                                                    'status' => $status,
                                                    'status_message' => $status_message,
                                                    'fraud_score' => $fraud_score,
                                                    'transaction_id' => $transactionId,
                                                    'created_at' => $transaction_date,
                                                    'updated_at' => $transaction_date,
                                                ];

                                                if($add == 1) {
                                                    DB::table('transactions')->insert($transactionArrayCharity);
                                                }
                                                print_r($transactionArrayCharity);


                                            }


                                            $transactionArray = [
                                                'booking_id' => $booking_id,
                                                'payment_id' => $payment_id,
                                                'user_id' => $user_id,
                                                'vendor_tx_code' => $vendor_tx_code,
                                                'currency' => $currency,
                                                'last4Digits' => $last4Digits,
                                                'amount' => $amount,
                                                'type' => $type,
                                                'refund' => $refund,
                                                'status' => $status,
                                                'status_message' => $status_message,
                                                'fraud_score' => $fraud_score,
                                                'transaction_id' => $transactionId,
                                                'created_at' => $transaction_date,
                                                'updated_at' => $transaction_date,
                                            ];

                                            print_r($transactionArray);
                                            if($add == 1) {
                                                DB::table('transactions')->insert($transactionArray);
                                            }



                                        }


                                    if(!empty($transactionsExists->id)){

                                        $transactionsExistsAmount = DB::table('transactions')
                                            ->where('transaction_id', $transactionId)
                                            ->sum('amount');

                                        if($transactionsExists->status != $status)
                                        {

                                            echo "Status does not match\n";
                                            echo "Vend:".$vendor_tx_code." \n";
                                            echo "Status:".$transactionsExists->status." \n";
                                            echo "Sage:".$status." \n";

                                            echo "Status M:".$transactionsExists->status_message." \n";
                                            echo "Sage M:".$status_message." \n";

                                            echo "Amount:".$transactionsExistsAmount." \n";
                                            echo "Sage A:".$amount." \n";

                                            echo "Refund:".$transactionsExists->refund." \n";
                                            echo "Sage R:".$refund." \n";

                                            echo "Type:".$transactionsExists->type." \n";
                                            echo "Sage T:".$type." \n";

                                            if(empty($amount))
                                                $amount = null;

                                            if(empty($refund))
                                                $refund = null;

                                            DB::table('transactions')->where('id', $transactionsExists->id)->update([
                                                'status' => $status,
                                                'status_message' => $status_message,
                                                'amount' => $amount,
                                                'refund' => $refund,

                                            ]);
                                        }


                                        else if($status == '0000' && round($transactionsExistsAmount,2) != round($amount_b,2)  )
                                        {

                                            $check =0;
                                            if($transactionsExists->type == '01' )
                                                $check =1;

                                            if($transactionsExists->type == '02' )
                                                $check =1;

                                            if($transactionsExists->type == '03' )
                                                $check =1;

                                            if(!empty($check)) {


                                                echo "Status does not match\n";
                                                echo "Vend:" . $vendor_tx_code . " \n";
                                                echo "Status:" . $transactionsExists->status . " \n";
                                                echo "Sage:" . $status . " \n";

                                                echo "Status M:" . $transactionsExists->status_message . " \n";
                                                echo "Sage M:" . $status_message . " \n";

                                                echo "Amount:" . $transactionsExistsAmount . " \n";
                                                echo "Sage A:" . $amount_b . " \n";

                                                echo "Refund:" . $transactionsExists->refund . " \n";
                                                echo "Sage R:" . $refund . " \n";

                                                echo "Type:" . $transactionsExists->type . " \n";
                                                echo "Sage T:" . $type . " \n";

                                                echo "\n\n";

                                                if (empty($amount))
                                                    $amount = null;

                                                if (empty($refund))
                                                    $refund = null;

                                                //DB::table('transactions')->where('id', $transactionsExists->status)->update([
                                                //   'last4Digits' => $last4Digits,
                                                //'amount' => $amount,
                                                //'type' => $type,
                                                //'refund' => $refund,
                                                //'status' => $status,
                                                //'status_message' => $status_message,

                                                // ]);

                                            }
                                        }

                                    }



                                        //print_r($retrieve_transaction_response);





                                }



                            }



                        }



                        //run log that will be checked regularly to review errors
                        if(!empty($error_log))
                        {
                            print_r($error_log);
                        }


        */




        /*
                //check to see
                        $payments = DB::table('payments')
                            ->get();
                        foreach ($payments as $p) {

                            $paid = 0;
                            $booked =0;
                            $transactions = DB::table('transactions')
                                ->where('booking_id',$p->booking_id)
                                ->get();
                            foreach ($transactions as $t) {

                                if($t->status == '0000')
                                    $paid = 1;

                            }


                            $sales = DB::table('sales')
                                ->where('booking_id', $p->booking_id)
                                ->where('status','b')
                                ->first();
                            if(!empty($sales->id)){
                                $booked =1;
                            }


                            if($paid == 1 && $booked == 0)
                            {

                                echo "Not Booked \n";
                                echo "Booking ID ".$p->booking_id."\n";


                                $salesR = DB::table('sales')
                                    ->where('booking_id', $p->booking_id)
                                    ->where('status','r')
                                    ->first();
                                if(!empty($salesR->id)){

                                    echo "Convert Booking \n";

                                    DB::table('sales')->where('booking_id', $p->booking_id)->update([
                                        'status'=> 'b',
                                    ]);

                                }
                                else {

                                    echo "Booking Error \n";

                                    $fixed = 0;
                                    $salesR = DB::table('sales')
                                        ->where('booking_id', $p->booking_id)
                                        ->where('status','l')
                                        ->first();
                                    if(!empty($salesR->id)) {

                                        echo "Found Lost Booking \n";


                                        if (empty($fixed)) {

                                            echo "Bookings Found as \n";
                                            echo "email:" . $salesR->email . "\n";
                                            echo "booking_id:" . $p->booking_id . "\n";
                                            echo "booking_id:" . $p->booking_id . "\n\n";


                                            $salesIssue = DB::table('sales')
                                                ->where('booking_id', $p->booking_id)
                                                ->get();
                                            foreach ($salesIssue as $si) {

                                                echo "email booking:" . $si->email . "\n";
                                                echo "start:" . $si->start . "\n";
                                                echo "status:" . $si->status . "\n";
                                                echo "booking_id:" . $si->booking_id . "\n";

                                                $lockerCheck = DB::table('sales')
                                                    ->where('locker_id', $si->locker_id)
                                                    ->where('status', 'b')
                                                    ->first();
                                                if (empty($lockerCheck->id)) {
                                                    echo "Available Locker \n\n";

                                                    DB::table('sales')->where('id', $si->id)->update([
                                                        'status' => 'b',
                                                    ]);
                                                    $fixed = 0;
                                                } else {

                                                    $found = 0;
                                                    $lockerCheckAll = DB::table('lockers')
                                                        ->where('block_id', $si->block_id)
                                                        ->where('active', 1)
                                                        ->where('display', 1)
                                                        ->get();
                                                    foreach ($lockerCheckAll as $lcA) {

                                                        if (!empty($found))
                                                            continue;

                                                        if (empty($lcA->locker_number))
                                                            continue;

                                                        $lockerCheck = DB::table('sales')
                                                            ->where('locker_id', $lcA->id)
                                                            ->where(function ($status) {
                                                                $status->where('status', 'b')
                                                                    ->orWhere('status', 'r');
                                                            })
                                                            ->where('start', $si->start)
                                                            ->first();
                                                        if (empty($lockerCheck->id)) {
                                                            echo "Available Locker \n\n";
                                                            $found = 1;

                                                            DB::table('sales')->where('id', $si->id)->update([
                                                                'status' => 'b',
                                                                'locker_id' => $lcA->id,
                                                                'number' => $lcA->locker_number,
                                                            ]);
                                                        }

                                                    }


                                                    if (empty($found)) {

                                                        $lockerCheckAll = DB::table('lockers')
                                                            ->where('company_id', $si->company_id)
                                                            ->where('active', 1)
                                                            ->where('display', 1)
                                                            ->get();
                                                        foreach ($lockerCheckAll as $lcA) {

                                                            if (empty($lcA->locker_number))
                                                                continue;

                                                            if (!empty($found))
                                                                continue;

                                                            $lockerCheck = DB::table('sales')
                                                                ->where('locker_id', $lcA->id)
                                                                ->where('start', $si->start)
                                                                ->where(function ($status) {
                                                                    $status->where('status', 'b')
                                                                        ->orWhere('status', 'r');
                                                                })
                                                                ->first();
                                                            if (empty($lockerCheck->id)) {
                                                                echo "Available Locker \n\n";
                                                                $found = 1;

                                                                DB::table('sales')->where('id', $si->id)->update([
                                                                    'status' => 'b',
                                                                    'locker_id' => $lcA->id,
                                                                    'number' => $lcA->locker_number,
                                                                ]);
                                                            }

                                                        }

                                                    }


                                                }


                                            }

                                        }


                                    }





                                }





                                echo "\n";

                            }



                        }

        */




        /*
                //remove any transaction that has been
                // check for over bookings
                $count_errors = 0;
                $where_error = '';
                $payments = DB::table('payments')
                    ->get();
                foreach ($payments as $p) {


                    $payments_paid = 0;
                    $charity_paid =0;
                    $payments_refunded = 0;
                    $payments_refunded_charity = 0;
                    $payments_price_change = 0;
                    $sales_paid = 0;
                    $date = '';

                    $date_created = '';
                    if(!empty($p->created_at))
                        $date_created = $p->created_at;

                    if(!empty($p->updated_at) && empty($date_created))
                        $date_created = $p->updated_at;

                    if(empty($date_created)){

                        $sale_check_old = $mysql_locker_rentals->table('product')
                            ->where('id', $p->booking_id)
                            ->first();
                        if (!empty($sale_check_old->date_added)){
                            $date_created = $sale_check_old->date_added;
                        }
                    }



                    $sales = DB::table('sales')
                        ->where('booking_id', $p->booking_id)
                        ->where('status','b')
                        ->get();
                    foreach ($sales as $s) {
                        $sales_paid = $sales_paid + $s->price;
                        $date = $s->start;

                        if(!empty($s->created_at) && empty($date_created))
                            $date_created = $s->created_at;

                        if(!empty($s->updated_at) && empty($date_created))
                            $date_created = $s->updated_at;

                    }

                    $actuallyPaid = 0;
                    $transactions = DB::table('transactions')
                        ->where('booking_id', $p->booking_id)
                        ->get();
                    foreach ($transactions as $t) {

                        if($t->status == 0000)
                            $actuallyPaid = 1;

                        if(!empty($t->updated_at) && empty($date_created))
                            $date_created = $t->updated_at;

                        if(!empty($t->created_at) && empty($date_created))
                            $date_created = $t->created_at;

                        $payments_paid = $payments_paid + $t->amount;

                        if($t->type == 03)
                            $charity_paid = $charity_paid + $t->amount;

                        if($t->type == 04 && $t->status == 0000)
                        $payments_refunded_charity = $payments_refunded_charity + $t->amount_change;

                        if($t->status == 0000)
                        $payments_refunded = $payments_refunded + $t->refund;

                        $payments_price_change = $payments_price_change + $t->amount_change;

                        if((empty($t->created_at) || empty($t->updated_at)) && !empty($date_created)){

                            echo $t->id. "update".$date_created."\n";
                            DB::table('transactions')->where('id', $t->id)->update([
                                'updated_at'=> $date_created,
                               'created_at'=> $date_created,
                            ]);

                        }



                    }

                    $charity_paid = $charity_paid-$payments_refunded_charity;


                         //SELECT * FROM `transactions`where `type` = '02' and `status` NOT LIKE '0000' ORDER BY `transactions`.`amount` DESC


              //        <td>Locker Payment</td>
                //                                            @elseif($transaction->type == '02')
                  //                                            <td>3D Payment</td>
                    //                                        @elseif($transaction->type == '03')
                      //                                        <td>Charity Payment</td>
                        //                                    @elseif($transaction->type == '04')
                          //                                      <td>Charity Adjustment</td>
                            //                                @elseif($transaction->type == '05')
                              //                                  <td>Locker Adjustment</td>
                                //                            @elseif($transaction->type == '06')
                                  //                              <td>Refund</td>
                                    //                        @elseif($transaction->type == '07')
                                      //                          <td>Invoice Paid</td>


                    $sales_paid = $sales_paid+$charity_paid;

                    DB::table('payments')->where('id', $p->id)->update([
                        'amount'=> $sales_paid,
                    ]);




                    $sales = DB::table('sales')
                        ->where('booking_id', $p->booking_id)
                        ->where('status','b')
                        ->get();
                    foreach ($sales as $s) {

                        if(empty($actuallyPaid )){

                            $count_errors++;
                            $new_code = rand(1000,9999);
                            echo $s->booking_id . ") confirmed in error \n";
                            echo "Start: ".$s->start . "\n";
                            echo "Confirmation Sent: ".$s->confirmation_sent . "\n";
                            echo "CHold Booking Date: ".$s->hold_booking_date . "\n";
                            echo "Code: ".$s->net_pass . "\n";

                            //2018-08-31 00:00:0//2018-08-31 00:00:00
                            $man_locker_codes = DB::table('manual_locker_codes')
                                ->where('locker_id', $s->locker_id)
                                ->where('start',date("Y-08-31 00:00:00",strtotime($s->start)))
                                ->first();
                            if(!empty($man_locker_codes->id)){
                                $man_locker_codes->code = $new_code;
                                echo "New Code: ".$new_code . "\n";



                           //     DB::table('sales')->where('id', $s->id)->update([
                         //           'status'=> 'r',
                           //         'confirmation_sent'=>0,
                           //         'net_pass' => null,
                            //    ]);

                                //
                           //     DB::table('manual_locker_codes')->where('id', $man_locker_codes->id)->update([
                            //        'code'=> $new_code,
                           //    ]);





                                $where_error.="booking_id = ".$s->booking_id." || ";
                            }


                          //  $payment_id = DB::table('payments')->insertGetId([
                            //    'user_id' => $user_id,
                              //  'booking_id' => $booking_id,
                             //  'currency' => $currency,
                              //  'created_at' => $transaction_date,
                              //  'updated_at' => $transaction_date,

                           // ]);


                            echo "\n";
                        }


                    }


                    $payments_paid = $payments_paid-$payments_refunded;
                    if (round($sales_paid, 2) != round($payments_paid, 2)) {

                        echo $p->booking_id . ")" . $sales_paid . " != " . $payments_paid . "\n";

                        if($date  < '2019-09-01 00:00:00'){
                          echo $p->booking_id . ") old so remove " . $date."\n";

                            $count_t =0;
                            $salesU = DB::table('sales')
                                ->where('booking_id', $p->booking_id)
                                ->get();
                            foreach ($salesU as $sp){
                                $count_t++;
                            }

                            if($count_t ==1){
                                echo "Updating Sales Price\n";
                                DB::table('sales')->where('booking_id', $p->booking_id)->update([
                                    'price'=> $payments_paid,
                                    'tax' => round($payments_paid*0.2)
                                ]);
                            } else
                                echo $count_t."Count\n";

                        }




                    }





                }

                echo "\n\n\n".$where_error."\n\n\n";
                echo "errors".$count_errors;




        */





        /*

        //generate Codes
        for ($x = 0; $x <= 3; $x++) {

            $start = date("Y-m-d 00:00:00", strtotime("+" . $x . " years", strtotime('2019-08-31')));
            $end = date("Y-m-d 00:00:00", strtotime("+" . $x . " years", strtotime('2020-09-01')));

            $lockers = DB::table('lockers')
                ->where('active',1)
                ->where('display',1)
                ->orderBy('block_id','ASC')
                ->orderBy('locker_order','ASC')
                ->get();
            foreach ($lockers as $l) {

                $man_locker_codes = DB::table('manual_locker_codes')
                    ->where('locker_id', $l->id)
                    ->where('block_id', $l->block_id)
                    ->where('start', $start)
                    ->first();
                if (empty($man_locker_codes->id)) {

                    $code = rand(1000, 9999);

                    echo $l->locker_number . " Code Created --- ";
                    echo $code . " --- \n";


                    DB::table('manual_locker_codes')->insert([
                        'code' => $code,
                        'locker_id' => $l->id,
                        'block_id' => $l->block_id,
                        'start' => $start,
                        'end' => $end,

                    ]);

                }

            }



        }


        */










        //manual Locker Codes






        /*

        echo " Start man locker codes \n\n";

        $mysql_locker_rentals->table('locker_code_report')->truncate();
        $companies = DB::table('companies')
            //->where('id',129)
            ->where('active',1)
            ->where('display',1)
            ->get();
        foreach ($companies as $c) {

            $lockers = DB::table('lockers')
                ->where('company_id', $c->id)
                ->where('active', 1)
                ->where('display', 1)
                ->orderBy('block_id', 'ASC')
                ->orderBy('locker_order', 'ASC')
                ->get();
            foreach ($lockers as $l) {

                $block_name = '';
                if (!empty($l->block_id)) {

                    $blocks = DB::table('blocks')
                        ->where('id', $l->block_id)
                        ->where('active',1)
                        ->where('display',1)
                        ->first();
                    if(!empty($blocks->id))
                        $block_name = $blocks->name;

                }


                if(!empty($block_name)) {

                    $start = date("2020-08-31 00:00:00");
                    $man_locker_codes = DB::table('manual_locker_codes')
                        ->where('locker_id', $l->id)
                        ->where('block_id', $l->block_id)
                        ->where('start', $start)
                        ->first();
                    if (!empty($man_locker_codes->id)) {

                        $company_name = $c->name;

                        $used = 'Not Used Last Year';
                        $last_year_code = '';
                        $salesCheck = DB::table('sales')
                            ->where('locker_id', $l->id)
                            ->where('start', '2019-09-01 00:00:00')
                            ->where('status', 'b')
                            ->first();
                        if (!empty($salesCheck->id)) {

                            $salesCheckThis = DB::table('sales')
                                ->where('locker_id', $l->id)
                                ->where('start', '2020-09-01 00:00:00')
                                ->where('status', 'b')
                                ->where('user_id', $salesCheck->user_id)
                                ->first();
                            if (!empty($salesCheckThis->id)) {
                                $used = 'Used by the same Person - If you want I can set the same code. This must be a decision yes or no for all schools. Provided old code';
                                $last_year_code = $salesCheckThis->net_pass;
                            } else
                                $used = 'Used last year by someone else';

                        } else {

                            $man_locker_codes_last = DB::table('manual_locker_codes')
                                ->where('locker_id', $l->id)
                                ->where('block_id', $l->block_id)
                                ->where('start', "2019-08-31 00:00:00")
                                ->first();
                            $used = 'No Reservation Last Year - can set last years code but must be a decision for all schools';
                            $last_year_code = $man_locker_codes_last->code;

                        }

                        if (!empty($l->locker_number)) {

                            // echo $l->locker_number . " Code: " . $man_locker_codes->code . "\n";

                            $mysql_locker_rentals->table('locker_code_report')->insert([
                                'school_name' => $company_name,
                                'block_name' => $block_name,
                                'locker_number' => $l->locker_number,
                                'used_last_year' => $used,
                                'code' => $man_locker_codes->code,
                                'last_year_code' => $last_year_code,
                            ]);

                        } else{

                            echo "error with locker" . $l->id . "\n";
                            echo "Company Name".$company_name."\n";
                        }


                    }

                }

            }

        }

        echo " end man locker codes \n\n";

        */















        //Pupual Premium


        /*

         Arden - 20
         Holy Trinity - Telford - 168 - £30
         Woodcote High School - 55 18
         Downlands Community School  101, block 352
          *
          * */


        /*
        $company_id = 101;
        $block_id = 0;//352;
        $count =240;
        $cost = 18;

        $c=0;
        $array = array();
        $companies = DB::table('companies')
            ->where('id',$company_id)
            ->get();
        foreach ($companies as $data) {

            $found =0;


            if($data->id == $company_id) {

                $countBooked = 0;

                $sales = DB::table('sales')
                    ->where('company_id', $data->id)
                    ->where('title','LOCKER_M')
                    ->where('start', '=', '2020-09-01 00:00:00')
                    ->get();
                foreach ($sales as $s){
                    $countBooked++;
                }



                if($count>$countBooked)
                    $required_booking = $count-$countBooked;
                else
                    $required_booking = 0;


                if($count >= $countBooked)
                {
                    echo $data->id."- ".$count . " NEED MORE : " . $countBooked . "\n";

                    for ($x = 1; $x <= $required_booking; $x++) {

                        $locker_id = 0;
                        $locker_number = 0;
                        $block =0;

                        if(!empty($block_id)){
                            $lockers = DB::table('lockers')
                                ->where('company_id',$data->id)
                                //->where('block_id',$block_id)
                                ->where('active',1)
                                ->where('display',1)
                                ->get();
                        } else {
                            $lockers = DB::table('lockers')
                                ->where('company_id',$data->id)
                                ->where('active',1)
                                ->where('display',1)
                                ->get();
                        }


                        foreach ($lockers as $l) {

                            $salesFind = DB::table('sales')
                                ->where('locker_id', $l->id)
                                ->where('start', '=', '2020-09-01 00:00:00')
                                ->where(function($status) {
                                    $status->where('status','r')
                                        ->orWhere('status','b');
                                })
                                ->first();
                            if (empty($salesFind->id)){

                                if(empty($locker_id) && !empty($l->id)){
                                    $locker_id = $l->id;
                                    $locker_number = $l->locker_number;
                                    $block = $l->block_id;

                                }

                            }

                        }

                        if(!empty($locker_id)){


                               $sales_id = DB::table('sales')->insertGetId([
                                   'lang' => 'EN',
                                   'title' => 'LOCKER_M',
                                   'number' => $locker_number,
                                   'lock_type' => 'm',
                                   'locker_id' => $locker_id,
                                   'block_id' => $block,
                                   'company_id' => $data->id,
                                   'start' => '2020-09-01 00:00:00',
                                   'end' => '2021-08-01 00:00:00',
                                   'status' => 'r',
                                   'hold_booking_date' => '2021-08-01 00:00:00',
                                   'booking_expiry' => '2021-08-01 00:00:00',
                                   'price' => $cost,
                                   'tax' => $cost * 0.2
                               ]);

                               $payment_id = DB::table('payments')->insertGetId([
                                   'amount' => $cost,
                                   'amount_balance' => $cost,
                                   'user_id' => $data->user_id,
                                   'booking_id' => $sales_id,
                                   'created' => date("Y-m-d H:i:s"),
                                   'currency' => 'GBP',
                                   'deleted' => 0,
                                   'waiting_response_sagepay'=>0,
                                   'has_transaction' =>1,

                               ]);

                               if (!empty($payment_id)) {
                                   DB::table('sales')->where('id', $sales_id)->update(['booking_id' => $sales_id]);
                                   echo "create" . $sales_id . " --- " . $payment_id . "\n";
                               }

                            echo "Locker Number " . $locker_id . " Needs Booking \n";
                        } else
                            echo "No Locker Available " . $x . "  \n";


                    }

                }
                else {


                    echo $data->id." -- ".$count ." All Locker booked " . $countBooked . "\n";

                }


            }

        }



        echo "Student Premium End \n";
        */


















        //Pupual Premium 2
        // from import sheet


        //SELECT `id`, `first_name`, `surname`, `school_year`, `school`, `done` FROM `sales_import` WHERE 1

		/*
		$cost = 8;
		$user_admin = 98;        
        $data = DB::table('sales_import')
            ->where('done',2)
            ->first();
        if(!empty($data)){ 
        	//($import as $data) {

            $found =0;

			$companies = DB::table('companies')
            ->where('id',$data->school)
            ->first();
                      
            echo "\n\n";          
            echo $companies->name."-- ";
            
    
    		$found =0;
    		$block_years = DB::table('block_years')
	           ->where('company_id',$data->school)
	           ->where('school_academic_year_id',$data->school_year)
	           ->get();
	        foreach ($block_years as $by)
	        {
	        	
	        	if(!empty($found))
	        	continue;
	        	

	        	
	        	
	        	$block = DB::table('blocks')
            		->where('id',$by->block_id)
            		->where('active',1)
	           		->where('display',1)
            		->first();
            		if(!empty($block->id))
            		{
            		
            			//for glen more
	        			// girls 5 boys 6
			        		
			        	$lookfor ='';			
			        	if($data->school_year == 5){ // girls {
			        		$lookfor ='GLB1B';//'GM Block';
			        	} else
			        		$lookfor ='WT Block 21 (Red, Sports)';
			        	
						$blockOK = 0;
						if (strpos($block->name, $lookfor) !== false) {
						    $blockOK = 1;
						}
						
		            		
		            	if(empty($blockOK))
            			continue;		
            		
            		
            			if(!empty($found))
	        			continue;
	        
            		echo "\n";
	            	echo "Block.".$block->name."-- lookfor: ".$lookfor." -- ";	  
	            			
	            	
	            	
	            	$lockers = DB::table('lockers')
		           ->where('company_id',$block->company_id)
		           ->where('block_id',$block->id)
		           ->where('active',1)
		           ->where('display',1)
		           ->get();
		           foreach ($lockers as $l)
		           {
		           	
		           		if(!empty($found))
	        			continue;
	        
		           		if(empty($found)){
		           		
		           			
		           			$salesFind = DB::table('sales')
	                           ->where('locker_id', $l->id)
	                            ->where('start', '=', '2020-09-01 00:00:00')
	                            ->where(function($status) {
	                                    $status->where('status','r')
	                                        ->orWhere('status','b');
	                                })
	                            ->first();
		           			if(!empty($salesFind)){
		           				echo $block->company_id.") Locker.".$salesFind->number." Booked -- \n";
		           				continue;
	            		  
		           			} 
		           			else
		           			{
		           				$found =1;
		           				echo $block->company_id.") Locker Available -- \n";
		           
		           		
		           				$user_email = '';
		           				$userFind = DB::table('users')
	                           		->where('id', $user_admin)
	                            	->first();
		           				if(!empty($userFind))
		           					$user_email = $userFind->email;
		           				
	            	
	            				$arraySales = [
	                                   'lang' => 'EN',
	                                   'title' => 'LOCKER_M',
	                                   'booking_type' =>1,
	                                   'block_name' => $block->name, 
	                                   'user_id'=>$user_admin,
	                                   'number' => $l->locker_number,
	                                   'lock_type' => 'm',
	                                   'locker_id' => $l->id,
	                                   'block_id' => $block->id,
	                                   'company_id' => $block->company_id,
	                                   'child_first_name'=>$data->first_name,
	                                   'child_surname'=>$data->surname,
	                                   'start' => '2020-09-01 00:00:00',
	                                   'end' => '2021-08-01 00:00:00',
	                                   'email'=> $user_email,
	                                   'status' => 'b',
	                                   'hold_booking_date' => '2021-08-01 00:00:00',
	                                   'booking_expiry' => '2021-08-01 00:00:00',
	                                   'price' => $cost,
	                                   'tax' => $cost * 0.2,
	                                   'school_year_id' => 2,
	                                   'school_academic_year_id'=>$data->school_year,
	                               ];
		           				
		           				print_r($arraySales);
		           				
		           				
		           				$sales_id = DB::table('sales')->insertGetId($arraySales);
		           				DB::table('sales_import')->where('id', $data->id)->update(['done' => 1]);
	                                   
	
	
	                               $payment_id = DB::table('payments')->insertGetId([
	                                   'amount' => $cost,
	                                   'account_balance' =>$cost,
	                                   'user_id' => $user_admin,
	                                   'booking_id' => $sales_id,
	                                   'created' => date("Y-m-d H:i:s"),
	                                   'currency' => 'GBP',
	                                   'customer_email'=> $user_email,
	                                   'deleted' => 0,
	                                   'has_transaction' => 1,
	
	                               ]);
	                               
	                               $transactionArray = [
				                    'booking_id' => $sales_id,
				                    'payment_id' => $payment_id,
				                    'user_id' => $user_admin,
				                    'vendor_tx_code' => "lockers_".date("YmdHis")."_".$sales_id,
				                    'currency' => 'GBP',
				                    //'last4Digits' => $last4Digits,
				                    'amount' => $cost,
				                    'type' => '07',
				                    'status' => '0000',
				                    'status_message' => 'Manual Invoice',
				                    'fraud_score' => 0,
				                    // 'transaction_id' => $transactionId,
				                    'created_at' => date("Y-m-d H:i:s"),
				                    'updated_at' => date("Y-m-d H:i:s"),
				                ];
                				//print_r($transactionArray);
                					$transaction_id = DB::table('transactions')->insertGetId($transactionArray);

	
	                               if (!empty($payment_id)) {
	                                   DB::table('sales')->where('id', $sales_id)->update(['booking_id' => $sales_id]);
	                                   //echo "create" . $sales_id . " --- " . $payment_id . "\n";
	                               }
		           				
		           				
		           			}
	           			}
            		}
            	}	
			}
			
			
		}
		
		*/
         



        echo "Student Premium End \n";
        



        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

}
