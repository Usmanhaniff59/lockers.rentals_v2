<?php

use Illuminate\Database\Seeder;

class EmailTemplatePlaceHoldersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('email_template_place_holders')->delete();
        
        \DB::table('email_template_place_holders')->insert(array (
            0 => 
            array (
                'id' => 7,
                'email_template_id' => NULL,
                'placeholder' => '%to_email%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 8,
                'email_template_id' => NULL,
                'placeholder' => '%to_first_name%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 9,
                'email_template_id' => NULL,
                'placeholder' => '%to_surname%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 10,
                'email_template_id' => NULL,
                'placeholder' => ' ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 11,
                'email_template_id' => NULL,
                'placeholder' => ' ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 12,
                'email_template_id' => NULL,
                'placeholder' => ' ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 13,
                'email_template_id' => NULL,
                'placeholder' => '',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 14,
                'email_template_id' => NULL,
                'placeholder' => '',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 15,
                'email_template_id' => NULL,
                'placeholder' => '',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 16,
                'email_template_id' => NULL,
                'placeholder' => '',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 17,
                'email_template_id' => NULL,
                'placeholder' => '',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 21,
                'email_template_id' => 4,
                'placeholder' => '%firs_name%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 22,
                'email_template_id' => 4,
                'placeholder' => '%surname%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 23,
                'email_template_id' => 4,
                'placeholder' => '%link%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 24,
                'email_template_id' => 5,
                'placeholder' => '%account_first_name%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 25,
                'email_template_id' => 5,
                'placeholder' => '%account_surname%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 26,
                'email_template_id' => 5,
                'placeholder' => '%reservation_id%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 27,
                'email_template_id' => 5,
                'placeholder' => '%email%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 28,
                'email_template_id' => 5,
                'placeholder' => '%total_cost%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 29,
                'email_template_id' => 5,
                'placeholder' => '%start_date%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 30,
                'email_template_id' => 5,
                'placeholder' => '%end_date%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 31,
                'email_template_id' => 8,
                'placeholder' => '%reset_link%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 32,
                'email_template_id' => NULL,
                'placeholder' => '%company_email%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 33,
                'email_template_id' => NULL,
                'placeholder' => '%company_phone_number%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 34,
                'email_template_id' => 5,
                'placeholder' => '%name%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 35,
                'email_template_id' => 5,
                'placeholder' => '%school_name%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 36,
                'email_template_id' => 5,
                'placeholder' => '%block%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 37,
                'email_template_id' => 5,
                'placeholder' => '%number%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => 38,
                'email_template_id' => 5,
                'placeholder' => '%code%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => 39,
                'email_template_id' => 5,
                'placeholder' => '%cost%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => 40,
                'email_template_id' => 5,
                'placeholder' => '%child_first_name%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => 41,
                'email_template_id' => 5,
                'placeholder' => '%child_surname%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => 42,
                'email_template_id' => 7,
                'placeholder' => '%account_first_name%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => 43,
                'email_template_id' => 7,
                'placeholder' => '%account_surname%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => 44,
                'email_template_id' => 7,
                'placeholder' => '%child_first_name%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => 45,
                'email_template_id' => 7,
                'placeholder' => '%child_surname%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => 46,
                'email_template_id' => 7,
                'placeholder' => '%reservation_id%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => 47,
                'email_template_id' => 7,
                'placeholder' => '%email%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => 48,
                'email_template_id' => 7,
                'placeholder' => '%total_cost%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => 49,
                'email_template_id' => 7,
                'placeholder' => '%start_date%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => 50,
                'email_template_id' => 7,
                'placeholder' => '%end_date%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => 51,
                'email_template_id' => 7,
                'placeholder' => '%name%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => 52,
                'email_template_id' => 7,
                'placeholder' => '%school_name%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => 53,
                'email_template_id' => 7,
                'placeholder' => '%block%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => 54,
                'email_template_id' => 7,
                'placeholder' => '%number%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => 55,
                'email_template_id' => 7,
                'placeholder' => '%code%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => 56,
                'email_template_id' => 7,
                'placeholder' => '%cost%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => 57,
                'email_template_id' => 10,
                'placeholder' => '%balance%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => 58,
                'email_template_id' => 10,
                'placeholder' => '%date%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => 59,
                'email_template_id' => 10,
                'placeholder' => '%amount%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'id' => 60,
                'email_template_id' => 10,
                'placeholder' => '%booking_id%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'id' => 61,
                'email_template_id' => 10,
                'placeholder' => '%name%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}