<?php

use Illuminate\Database\Seeder;

class FrontendPlaceholdersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('frontend_placeholders')->delete();
        
        \DB::table('frontend_placeholders')->insert(array (
            0 => 
            array (
                'id' => 1,
                'key' => 'home-non-availability-text',
                'placeholder' => '%school_name%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'key' => 'home-logged-email-text',
                'placeholder' => '%email%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'key' => 'school-term-title',
                'placeholder' => '%school_name%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'key' => 'school-term-title',
                'placeholder' => '%type_of_booking%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'key' => 'school-term-locker-availability-validation',
                'placeholder' => '%fromDate%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'key' => 'school-term-locker-availability-validation',
                'placeholder' => '%toDate%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'key' => 'school-term-locker-availability-validation',
                'placeholder' => '%locationName%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'key' => 'school-term-locker-availability-validation',
                'placeholder' => '%requestFormLINK%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'key' => 'school-term-title-location-group2',
                'placeholder' => '%school_name%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'key' => 'school-term-title-location-group2',
                'placeholder' => '%type_of_booking%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'key' => 'school-term-title-location-group3',
                'placeholder' => '%school_name%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'key' => 'school-term-title-location-group3',
                'placeholder' => '%type_of_booking%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'key' => 'choose-locker-minutes',
                'placeholder' => '%minutes%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'key' => 'school-term-end-later-date-validation',
                'placeholder' => '%fromDate%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'key' => 'school-term-end-later-date-validation',
                'placeholder' => '%toDate%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'key' => 'school-term-login-user-msg',
                'placeholder' => '%first_name%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'key' => 'school-term-login-user-msg',
                'placeholder' => '%surname%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'key' => 'school-term-login-user-msg',
                'placeholder' => '%email%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'key' => 'school-term-login-user-msg',
                'placeholder' => '%logout%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'key' => 'choose-locker-child-name-date-title',
                'placeholder' => '%child_name%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'key' => 'choose-locker-child-name-date-title',
                'placeholder' => '%date%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'key' => 'payments-confirmation-card-success-msg',
                'placeholder' => '%locker_number%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'key' => 'payments-confirmation-card-success-msg',
                'placeholder' => '%school_name%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'key' => 'payments-confirmation-card-success-msg',
                'placeholder' => '%admin_email%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'key' => 'payments-confirmation-card-success-msg',
                'placeholder' => '%reservation_number%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'key' => 'payments-charity-card-main-disclaimer',
                'placeholder' => '%numberoflockers%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'key' => 'payments-charity-card-main-disclaimer',
                'placeholder' => '%amount_lockers%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'key' => 'payments-charity-card-main-disclaimer',
                'placeholder' => '%charity__payment%',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}