<?php

use Illuminate\Database\Seeder;

class DeleteManualCodes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// Block group id added newly from 1st September 2021
       \DB::table('manual_locker_codes')->where('start','>=',  date('2021-09-01 00:00:00'))->whereNull('block_group_id')->delete();
       \DB::table('manual_locker_codes')->whereNull('block_group_id')->update(array('updated' => 2));
    }
}
