<?php

use Illuminate\Database\Seeder;

class DistributionListGroupsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('distribution_list_groups')->delete();
        
        \DB::table('distribution_list_groups')->insert(array (
            0 => 
            array (
                'id' => 2,
                'name' => 'admins',
                'created_at' => '2019-09-06 10:35:48',
                'updated_at' => '2019-09-06 10:35:48',
            ),
            1 => 
            array (
                'id' => 3,
                'name' => 'Managers',
                'created_at' => '2019-09-06 13:08:06',
                'updated_at' => '2019-09-06 13:08:06',
            ),
            2 => 
            array (
                'id' => 5,
                'name' => 'adminss',
                'created_at' => '2020-09-16 07:28:12',
                'updated_at' => '2020-09-16 07:28:12',
            ),
        ));
        
        
    }
}