<?php

use Illuminate\Database\Seeder;

class EmailTemplatesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('email_templates')->delete();
        
        \DB::table('email_templates')->insert(array (
            0 => 
            array (
                'id' => 2,
                'system_function' => 0,
                'system_task_name' => NULL,
                'template_name' => 'welcome email',
                'subject' => 'welcome to our company',
                'template_text' => '<p>Dear %first_name% can we request lockers to be added at %location% Hoping to see this matter dealt with at the earliest, Richaed perris 34534554334&nbsp;</p><p style="line-height:1.5rem;">Dear %first_name% can we request lockers to be added at %location% Hoping to see this matter dealt with at the earliest, Richaed perris 34534554334&nbsp;</p>',
                'sms_message' => 'welcome email',
                'created_at' => NULL,
                'updated_at' => '2020-03-18 05:14:22',
            ),
            1 => 
            array (
                'id' => 4,
                'system_function' => 1,
                'system_task_name' => '',
                'template_name' => 'Send Remind Email',
                'subject' => 'Send Remind Email',
                'template_text' => '<div style="line-height:1.5rem">%firs_name% %surname%,</div>
<div><br></div>
<div  style="line-height:1.5rem">You recently visited locker.rentals and requested to change your password, if this was not you don\'t worry just ignore this email. If however, you would like to change your password please click on the below link.<br><br></div><div>%link%</div>',
                'sms_message' => 'Reminder email',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 5,
                'system_function' => 1,
                'system_task_name' => NULL,
                'template_name' => 'Confirmation Email',
                'subject' => 'Confirmation Email',
                'template_text' => '<div style="line-height: 1.5rem;">Dear %account_first_name% %account_surname%</div>
<div style="line-height: 1.5rem;">&nbsp;</div>
<div style="line-height: 1.5rem;"><strong>Your booked</strong></div>
<div style="line-height: 1.5rem;">
<table style="border-collapse: collapse; width: 629px; height: 86px;" border="1">
<tbody>
<tr>
<td style="width: 200px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">Reservation ID</td>
<td style="width: 422px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">%reservation_id%</td>
</tr>
<tr>
<td style="width: 200px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">Email</td>
<td style="width: 422px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">%email%</td>
</tr>
<tr>
<td style="width: 200px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">Total Cost</td>
<td style="width: 422px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">%total_cost%</td>
</tr>
</tbody>
</table>
</div>
<div style="line-height: 1.5rem;">____Start Repeat for Child_____<br />
<div style="line-height: 1.5rem;"><strong>%child_first_name% %child_surname% Locker Details</strong></div>
<div style="line-height: 1.5rem;">
<table style="border-collapse: collapse; width: 632px; height: 220px;" border="1">
<tbody>
<tr>
<td style="width: 200px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">Start</td>
<td style="width: 422px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">%start_date%</td>
</tr>
<tr>
<td style="width: 200px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">End</td>
<td style="width: 422px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">%end_date%</td>
</tr>
<tr>
<td style="width: 200px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">%name%</td>
<td style="width: 422px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">%school_name%</td>
</tr>
<tr>
<td style="width: 200px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">Block</td>
<td style="width: 422px; border: 1px solid lightgray !important; padding: 7px;">%block%</td>
</tr>
<tr>
<td style="width: 200px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">Locker Number</td>
<td style="width: 422px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">%number%</td>
</tr>
<tr>
<td style="width: 200px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">Locker Code</td>
<td style="width: 422px; border: 1px solid lightgray !important; padding: 7px;">%code%</td>
</tr>
<tr>
<td style="width: 200px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">Cost</td>
<td style="width: 422px; line-height: 1.5rem; border: 1px solid lightgray !important; padding: 7px;">%cost%</td>
</tr>
</tbody>
</table>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
<div style="line-height: 1.5rem;">____End Repeat for Child_____</div>
<div style="line-height: 1.5rem;"><strong>HOW TO REPLACE YOUR LOCKER CODE:</strong></div>
<div style="line-height: 1.5rem;">Go to the locker %number%</div>
<div style="line-height: 1.5rem;">Enter the code (%code%) and open the lock</div>
<div style="line-height: 1.5rem;">1. With lock unlocked press and hold the button on the side of the lock</div>
<div style="line-height: 1.5rem;">2. Whilst still holding the button turn the dials to the desired code</div>
<div style="line-height: 1.5rem;">3. Release the button on the side of the lock</div>
<div style="line-height: 1.5rem;">&nbsp;</div>
<div style="line-height: 1.5rem;">This rental agreement is between Prefect Rentals and the lessee</div>
<div style="line-height: 1.5rem;">&nbsp;</div>
<div style="line-height: 1.5rem;">The rental period is from the point of rental (regardless of date) until the end of the</div>
<div style="line-height: 1.5rem;">current school year. Reduced rates for short date ranges are not available</div>
<div style="line-height: 1.5rem;">Rental fees are non-refundable</div>
<div style="line-height: 1.5rem;">Rental of lockers is non-transferrable</div>
<div style="line-height: 1.5rem;">All locker components including the electronic lock and batteries remain the property of</div>
<div style="line-height: 1.5rem;">Prefect Rentals and cannot be removed from site</div>
<div style="line-height: 1.5rem;">The locker can only be used by the lessee and lockers cannot be shared</div>
<div style="line-height: 1.5rem;">Lockers can only be used for the storage of personal items that belong to the lessee and</div>
<div style="line-height: 1.5rem;">must not include illegal items or any item prohibited by the school</div>
<div style="line-height: 1.5rem;">Prefect will not be held liable for any loss, damage or theft of items kept in the lockers.</div>
<div style="line-height: 1.5rem;">The school has the right to inspect the contents of the locker without prior notice</div>
<div style="line-height: 1.5rem;">At the end of the school year all items must be removed from the locker and the locker</div>
<div style="line-height: 1.5rem;">left empty. Any items left in the locker will be removed and disposed of without exception</div>
<div style="line-height: 1.5rem;">The locker should be left clean and in good condition</div>
<div style="line-height: 1.5rem;">Any damage or vandalism must be reported to the school immediately</div>
<div style="line-height: 1.5rem;">Codes can be reset or forgotten codes reissued by visiting Locker.Rentals</div>
<div style="line-height: 1.5rem;">For assistance or support issues (other than to retrieve your code) please contact us on</div>
<div style="line-height: 1.5rem;">%company_phone_number% or %company_email%</div>
<div style="line-height: 1.5rem;">&nbsp;</div>
<div style="line-height: 1.5rem;">&nbsp;</div>
</div>',
                'sms_message' => 'Thank you for your booking for  %number_of_lockers% at %school_name% please visit %admin_portal%. Your reservation number is %booking_number%.',
                'created_at' => NULL,
                'updated_at' => '2020-04-09 19:52:59',
            ),
            3 => 
            array (
                'id' => 7,
                'system_function' => 1,
                'system_task_name' => NULL,
                'template_name' => 'child Confirmation Email',
                'subject' => 'Confirmation Email',
                'template_text' => '<div>Dear %child_first_name% %child_surname%</div>
<div>&nbsp;</div>
<div>
<div style="line-height: 1.5rem;"><strong>&nbsp;Locker Details</strong></div>
<div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
<table style="border-collapse: collapse; width: 100%;" border="1">
<tbody>
<tr style="height: 27px;">
<td style="width: 25%; height: 27px;border:1px solid lightgray !important;padding:7px;">Start&nbsp; &nbsp;</td>
<td style="width: 75%; height: 27px;border:1px solid lightgray !important;padding:7px;">%start_date%</td>
</tr>
<tr style="height: 27px;">
<td style="width: 25%; height: 27px;border:1px solid lightgray !important;padding:7px;">End</td>
<td style="width: 50%; height: 27px;border:1px solid lightgray !important;padding:7px;">%end_date%</td>
</tr>
<tr style="height: 27px;">
<td style="width: 25%; height: 27px;border:1px solid lightgray !important;padding:7px;">%name%</td>
<td style="width: 50%; height: 27px;border:1px solid lightgray !important;padding:7px;">%school_name%</td>
</tr>
<tr style="height: 27px;">
<td style="width: 25%; height: 27px;border:1px solid lightgray !important;padding:7px;">Block</td>
<td style="width: 50%; height: 27px;border:1px solid lightgray !important;padding:7px;">%block%</td>
</tr>
<tr style="height: 27px;">
<td style="width: 25%; height: 27px;border:1px solid lightgray !important;padding:7px;">Locker Number</td>
<td style="width: 50%; height: 27px;border:1px solid lightgray !important;padding:7px;">%number%</td>
</tr>
<tr style="height: 33px;">
<td style="width: 25%; height: 33px;border:1px solid lightgray !important;padding:7px;">Locker Code</td>
<td style="width: 50%; height: 33px;border:1px solid lightgray !important;padding:7px;">%code%</td>
</tr>
</tbody>
</table>
</div>
<div style="line-height: 1.5rem;"><br /><strong>HOW TO REPLACE YOUR LOCKER CODE:</strong></div>
<div style="line-height: 1.5rem;">Go to the locker %number%</div>
<div style="line-height: 1.5rem;">Enter the code (%code%) and open the lock</div>
<div style="line-height: 1.5rem;">1. With lock unlocked press and hold the button on the side of the lock</div>
<div style="line-height: 1.5rem;">2. Whilst still holding the button turn the dials to the desired code</div>
<div style="line-height: 1.5rem;">3. Release the button on the side of the lock</div>
<div style="line-height: 1.5rem;">&nbsp;</div>
<div style="line-height: 1.5rem;">This rental agreement is between Prefect Rentals and the lessee</div>
<div style="line-height: 1.5rem;">&nbsp;</div>
<div style="line-height: 1.5rem;">The rental period is from the point of rental (regardless of date) until the end of the</div>
<div style="line-height: 1.5rem;">current school year. Reduced rates for short date ranges are not available</div>
<div style="line-height: 1.5rem;">Rental fees are non-refundable</div>
<div style="line-height: 1.5rem;">Rental of lockers is non-transferrable</div>
<div style="line-height: 1.5rem;">All locker components including the electronic lock and batteries remain the property of</div>
<div style="line-height: 1.5rem;">Prefect Rentals and cannot be removed from site</div>
<div style="line-height: 1.5rem;">The locker can only be used by the lessee and lockers cannot be shared</div>
<div style="line-height: 1.5rem;">Lockers can only be used for the storage of personal items that belong to the lessee and</div>
<div style="line-height: 1.5rem;">must not include illegal items or any item prohibited by the school</div>
<div style="line-height: 1.5rem;">Prefect will not be held liable for any loss, damage or theft of items kept in the lockers.</div>
<div style="line-height: 1.5rem;">The school has the right to inspect the contents of the locker without prior notice</div>
<div style="line-height: 1.5rem;">At the end of the school year all items must be removed from the locker and the locker</div>
<div style="line-height: 1.5rem;">left empty. Any items left in the locker will be removed and disposed of without exception</div>
<div style="line-height: 1.5rem;">The locker should be left clean and in good condition</div>
<div style="line-height: 1.5rem;">Any damage or vandalism must be reported to the school immediately</div>
<div style="line-height: 1.5rem;">Codes can be reset or forgotten codes reissued by visiting Locker.Rentals</div>
<div style="line-height: 1.5rem;">For assistance or support issues (other than to retrieve your code) please contact us on</div>
<div style="line-height: 1.5rem;">%company_phone_number% or %company_email%</div>
</div>',
                'sms_message' => 'child confirmation',
                'created_at' => NULL,
                'updated_at' => '2020-03-15 18:48:15',
            ),
            4 => 
            array (
                'id' => 8,
                'system_function' => 1,
                'system_task_name' => NULL,
                'template_name' => 'Reset Password',
                'subject' => 'Reset Password',
                'template_text' => '<p style="text-align: left;"><strong>Password Reset&nbsp;</strong></p>
<p style="text-align: left;">You are receiving this email because we received a password reset request for your account</p>
<p style="text-align: left;"><strong><a href="abcdef">Reset Password</a></strong></p>
<p style="text-align: left;">&nbsp;This password reset link will expire in 60 minutes If you did not request a password reset, no further action is required</p>',
                'sms_message' => 'reset password',
                'created_at' => NULL,
                'updated_at' => '2020-03-13 15:25:30',
            ),
            5 => 
            array (
                'id' => 9,
                'system_function' => 0,
                'system_task_name' => NULL,
                'template_name' => 'Re Booking Reminder',
                'subject' => 'Locker Re Booking Reminder',
                'template_text' => '<p style="text-align: left;"><strong>Re Booking Reminder&nbsp;</strong></p>
<div style="line-height: 1.5rem;">Dear %first_name%</div>
<p style="text-align: left;">%error_reason%</p>
<p style="text-align: left;">You booked locker %number% and %company_name% from %start_date% to %end_date%. if you would like to book this locker for %new_start_date% to new %new_end_date% please book before %booking_cut_off_date%.</p>
<p style="text-align: left;">To book this locker please click on the below link <strong>%re_book_locker_link%</strong></p>
<p style="text-align: left;">You may choose another locker of if this locker is no longer suitable, if you do not confirm this locker %booking_cut_off_date% it will be opened up for other people to book.</p>
<p class="p1" style="line-height: 1.5rem; text-align: left;"><strong><span class="s1" style="display: ___if_locker_check___;">___if_locker_check___</span></strong></p>
<p class="p1" style="line-height: 1.5rem; text-align: left;"><span class="s1" style="display: ___endif_locker_check___;">%old_locker_number% has been changed to &nbsp; %new_locker_number%</span></p>
<p class="p1" style="line-height: 1.5rem; text-align: left;"><strong><span class="s1" style="display: ___if_locker_check___;">___endif_locker_check___</span></strong></p>
<p class="p1" style="line-height: 1.5rem; text-align: left;"><span class="s1">To cancel this reservation and stop receiving emails for <strong>%number%</strong> at&nbsp;<strong>%company_name%</strong> please %do_not_require_locker_link%.</span></p>
<p style="line-height: 1.5rem; text-align: left;">Thank you</p>
<p style="line-height: 1.5rem; text-align: left;">&nbsp;</p>
<p style="line-height: 1.5rem; text-align: left;">Locker.Rentals</p>
<p style="line-height: 1.5rem; text-align: left;">Web Support</p>',
                'sms_message' => 'Re Booking Reminder',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 10,
                'system_function' => 0,
                'system_task_name' => NULL,
                'template_name' => 'Refund Email',
                'subject' => 'Refund',
                'template_text' => '<p class="p1"><span class="s1">Dear %name%</span></p>
<p class="p2">&nbsp;</p>
<p class="p1"><span class="s1">We have refunded booking %booking_id% for %amount% on %date% your booking balance is currently %balance%. </span></p>
<p class="p2">&nbsp;</p>
<p class="p1"><span class="s1">Kind Regards</span></p>
<p class="p2">&nbsp;</p>
<p class="p1"><span class="s1">Web Admin</span></p>
<p class="p1"><span class="s1">Locker.Rentals</span></p>',
                'sms_message' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}