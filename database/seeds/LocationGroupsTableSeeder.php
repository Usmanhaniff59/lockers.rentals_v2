<?php

use Illuminate\Database\Seeder;

class LocationGroupsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('location_groups')->delete();
        
        \DB::table('location_groups')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Schools Bookings',
                'created_at' => NULL,
                'updated_at' => '2020-04-15 15:02:45',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Day Bookings',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Hourly Booking',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}