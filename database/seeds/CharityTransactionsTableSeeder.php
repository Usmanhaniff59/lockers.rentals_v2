<?php

use Illuminate\Database\Seeder;

class CharityTransactionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('charity_transactions')->delete();
        
        \DB::table('charity_transactions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'charity_id' => 1,
                'amount' => '30',
                'booking_id' => 41,
                'created_at' => '2020-02-28 10:44:08',
                'updated_at' => '2020-02-28 10:44:08',
            ),
            1 => 
            array (
                'id' => 2,
                'charity_id' => 1,
                'amount' => '30',
                'booking_id' => 41,
                'created_at' => '2020-02-28 10:44:40',
                'updated_at' => '2020-02-28 10:44:40',
            ),
            2 => 
            array (
                'id' => 3,
                'charity_id' => 1,
                'amount' => '500',
                'booking_id' => 41,
                'created_at' => '2020-02-28 10:46:45',
                'updated_at' => '2020-02-28 10:46:45',
            ),
            3 => 
            array (
                'id' => 4,
                'charity_id' => 1,
                'amount' => '500',
                'booking_id' => 41,
                'created_at' => '2020-02-28 10:48:31',
                'updated_at' => '2020-02-28 10:48:31',
            ),
            4 => 
            array (
                'id' => 5,
                'charity_id' => 1,
                'amount' => '500',
                'booking_id' => 41,
                'created_at' => '2020-02-28 10:49:54',
                'updated_at' => '2020-02-28 10:49:54',
            ),
            5 => 
            array (
                'id' => 6,
                'charity_id' => 1,
                'amount' => '500',
                'booking_id' => 41,
                'created_at' => '2020-02-28 10:50:24',
                'updated_at' => '2020-02-28 10:50:24',
            ),
            6 => 
            array (
                'id' => 7,
                'charity_id' => 1,
                'amount' => '500',
                'booking_id' => 41,
                'created_at' => '2020-02-28 10:50:59',
                'updated_at' => '2020-02-28 10:50:59',
            ),
            7 => 
            array (
                'id' => 8,
                'charity_id' => 1,
                'amount' => '20',
                'booking_id' => 43,
                'created_at' => '2020-02-28 10:56:59',
                'updated_at' => '2020-02-28 10:56:59',
            ),
            8 => 
            array (
                'id' => 9,
                'charity_id' => 1,
                'amount' => '20',
                'booking_id' => 43,
                'created_at' => '2020-02-28 11:02:03',
                'updated_at' => '2020-02-28 11:02:03',
            ),
            9 => 
            array (
                'id' => 10,
                'charity_id' => 1,
                'amount' => '20',
                'booking_id' => 44,
                'created_at' => '2020-02-28 11:03:49',
                'updated_at' => '2020-02-28 11:03:49',
            ),
            10 => 
            array (
                'id' => 11,
                'charity_id' => 1,
                'amount' => '20',
                'booking_id' => 48737,
                'created_at' => '2020-03-03 18:46:56',
                'updated_at' => '2020-03-03 18:46:56',
            ),
            11 => 
            array (
                'id' => 12,
                'charity_id' => 1,
                'amount' => '976',
                'booking_id' => 48749,
                'created_at' => '2020-03-03 20:56:39',
                'updated_at' => '2020-03-03 20:56:39',
            ),
            12 => 
            array (
                'id' => 13,
                'charity_id' => 1,
                'amount' => '20',
                'booking_id' => 48751,
                'created_at' => '2020-03-03 21:22:04',
                'updated_at' => '2020-03-03 21:22:04',
            ),
            13 => 
            array (
                'id' => 14,
                'charity_id' => 1,
                'amount' => '20',
                'booking_id' => 134799,
                'created_at' => '2020-03-20 17:48:45',
                'updated_at' => '2020-03-20 17:48:45',
            ),
            14 => 
            array (
                'id' => 15,
                'charity_id' => 1,
                'amount' => '20',
                'booking_id' => 134799,
                'created_at' => '2020-03-20 17:49:42',
                'updated_at' => '2020-03-20 17:49:42',
            ),
            15 => 
            array (
                'id' => 16,
                'charity_id' => 1,
                'amount' => '20',
                'booking_id' => 134799,
                'created_at' => '2020-03-20 17:50:08',
                'updated_at' => '2020-03-20 17:50:08',
            ),
            16 => 
            array (
                'id' => 17,
                'charity_id' => 1,
                'amount' => '20',
                'booking_id' => 134799,
                'created_at' => '2020-03-20 17:50:16',
                'updated_at' => '2020-03-20 17:50:16',
            ),
            17 => 
            array (
                'id' => 18,
                'charity_id' => 1,
                'amount' => '20',
                'booking_id' => 134799,
                'created_at' => '2020-03-20 17:50:25',
                'updated_at' => '2020-03-20 17:50:25',
            ),
            18 => 
            array (
                'id' => 19,
                'charity_id' => 1,
                'amount' => '20',
                'booking_id' => 134799,
                'created_at' => '2020-03-20 17:50:26',
                'updated_at' => '2020-03-20 17:50:26',
            ),
            19 => 
            array (
                'id' => 20,
                'charity_id' => 1,
                'amount' => '20',
                'booking_id' => 134799,
                'created_at' => '2020-03-20 17:50:28',
                'updated_at' => '2020-03-20 17:50:28',
            ),
            20 => 
            array (
                'id' => 21,
                'charity_id' => 1,
                'amount' => '20',
                'booking_id' => 134799,
                'created_at' => '2020-03-20 17:50:30',
                'updated_at' => '2020-03-20 17:50:30',
            ),
            21 => 
            array (
                'id' => 22,
                'charity_id' => 1,
                'amount' => '20',
                'booking_id' => 134799,
                'created_at' => '2020-03-20 17:50:32',
                'updated_at' => '2020-03-20 17:50:32',
            ),
            22 => 
            array (
                'id' => 23,
                'charity_id' => 1,
                'amount' => '20',
                'booking_id' => 134799,
                'created_at' => '2020-03-20 17:50:41',
                'updated_at' => '2020-03-20 17:50:41',
            ),
            23 => 
            array (
                'id' => 24,
                'charity_id' => 1,
                'amount' => '20',
                'booking_id' => 134799,
                'created_at' => '2020-03-20 17:50:43',
                'updated_at' => '2020-03-20 17:50:43',
            ),
            24 => 
            array (
                'id' => 25,
                'charity_id' => 1,
                'amount' => '20',
                'booking_id' => 134799,
                'created_at' => '2020-03-20 17:50:44',
                'updated_at' => '2020-03-20 17:50:44',
            ),
            25 => 
            array (
                'id' => 26,
                'charity_id' => 1,
                'amount' => '20',
                'booking_id' => 134799,
                'created_at' => '2020-03-20 17:50:54',
                'updated_at' => '2020-03-20 17:50:54',
            ),
            26 => 
            array (
                'id' => 27,
                'charity_id' => 1,
                'amount' => '20',
                'booking_id' => 134799,
                'created_at' => '2020-03-20 17:51:03',
                'updated_at' => '2020-03-20 17:51:03',
            ),
            27 => 
            array (
                'id' => 28,
                'charity_id' => 1,
                'amount' => '20',
                'booking_id' => 134801,
                'created_at' => '2020-03-20 17:55:07',
                'updated_at' => '2020-03-20 17:55:07',
            ),
            28 => 
            array (
                'id' => 29,
                'charity_id' => 1,
                'amount' => '400',
                'booking_id' => 134804,
                'created_at' => '2020-03-20 18:29:54',
                'updated_at' => '2020-03-20 18:29:54',
            ),
            29 => 
            array (
                'id' => 30,
                'charity_id' => 1,
                'amount' => '10',
                'booking_id' => 134806,
                'created_at' => '2020-03-20 18:58:01',
                'updated_at' => '2020-03-20 18:58:01',
            ),
            30 => 
            array (
                'id' => 31,
                'charity_id' => 1,
                'amount' => '20',
                'booking_id' => 134808,
                'created_at' => '2020-03-20 19:02:23',
                'updated_at' => '2020-03-20 19:02:23',
            ),
            31 => 
            array (
                'id' => 32,
                'charity_id' => 1,
                'amount' => '20',
                'booking_id' => 134821,
                'created_at' => '2020-03-23 17:46:03',
                'updated_at' => '2020-03-23 17:46:03',
            ),
            32 => 
            array (
                'id' => 33,
                'charity_id' => 1,
                'amount' => '20',
                'booking_id' => 134822,
                'created_at' => '2020-03-23 18:29:17',
                'updated_at' => '2020-03-23 18:29:17',
            ),
            33 => 
            array (
                'id' => 34,
                'charity_id' => 1,
                'amount' => '20',
                'booking_id' => 134837,
                'created_at' => '2020-03-24 16:30:30',
                'updated_at' => '2020-03-24 16:30:30',
            ),
            34 => 
            array (
                'id' => 35,
                'charity_id' => 1,
                'amount' => '2',
                'booking_id' => 134848,
                'created_at' => '2020-03-27 01:39:00',
                'updated_at' => '2020-03-27 01:39:00',
            ),
            35 => 
            array (
                'id' => 36,
                'charity_id' => 1,
                'amount' => '1',
                'booking_id' => 134855,
                'created_at' => '2020-03-27 22:18:51',
                'updated_at' => '2020-03-27 22:18:51',
            ),
            36 => 
            array (
                'id' => 37,
                'charity_id' => 1,
                'amount' => '1',
                'booking_id' => 134737,
                'created_at' => '2020-03-30 13:08:30',
                'updated_at' => '2020-03-30 13:08:30',
            ),
            37 => 
            array (
                'id' => 38,
                'charity_id' => 1,
                'amount' => '2',
                'booking_id' => 134742,
                'created_at' => '2020-03-30 13:31:34',
                'updated_at' => '2020-03-30 13:31:34',
            ),
            38 => 
            array (
                'id' => 39,
                'charity_id' => 1,
                'amount' => '1',
                'booking_id' => 134746,
                'created_at' => '2020-03-30 13:57:30',
                'updated_at' => '2020-03-30 13:57:30',
            ),
            39 => 
            array (
                'id' => 40,
                'charity_id' => 1,
                'amount' => '1',
                'booking_id' => 134760,
                'created_at' => '2020-03-30 14:01:58',
                'updated_at' => '2020-03-30 14:01:58',
            ),
            40 => 
            array (
                'id' => 41,
                'charity_id' => 1,
                'amount' => '1',
                'booking_id' => 134761,
                'created_at' => '2020-03-30 14:08:17',
                'updated_at' => '2020-03-30 14:08:17',
            ),
            41 => 
            array (
                'id' => 42,
                'charity_id' => 1,
                'amount' => '1',
                'booking_id' => 134763,
                'created_at' => '2020-03-30 15:24:20',
                'updated_at' => '2020-03-30 15:24:20',
            ),
            42 => 
            array (
                'id' => 43,
                'charity_id' => 1,
                'amount' => '1',
                'booking_id' => 156187,
                'created_at' => '2020-04-09 19:31:06',
                'updated_at' => '2020-04-09 19:31:06',
            ),
            43 => 
            array (
                'id' => 44,
                'charity_id' => 1,
                'amount' => '1',
                'booking_id' => 156189,
                'created_at' => '2020-04-09 19:35:38',
                'updated_at' => '2020-04-09 19:35:38',
            ),
            44 => 
            array (
                'id' => 45,
                'charity_id' => 1,
                'amount' => '1',
                'booking_id' => 156191,
                'created_at' => '2020-04-09 19:38:36',
                'updated_at' => '2020-04-09 19:38:36',
            ),
            45 => 
            array (
                'id' => 46,
                'charity_id' => 1,
                'amount' => '1.00',
                'booking_id' => 156266,
                'created_at' => '2020-05-20 00:12:09',
                'updated_at' => '2020-05-20 00:12:09',
            ),
            46 => 
            array (
                'id' => 47,
                'charity_id' => 1,
                'amount' => '1.00',
                'booking_id' => 156269,
                'created_at' => '2020-06-12 10:36:21',
                'updated_at' => '2020-06-12 10:36:21',
            ),
            47 => 
            array (
                'id' => 48,
                'charity_id' => 1,
                'amount' => '1.00',
                'booking_id' => 156270,
                'created_at' => '2020-06-12 12:03:31',
                'updated_at' => '2020-06-12 12:03:31',
            ),
            48 => 
            array (
                'id' => 49,
                'charity_id' => 1,
                'amount' => '1.00',
                'booking_id' => 156271,
                'created_at' => '2020-06-12 12:07:56',
                'updated_at' => '2020-06-12 12:07:56',
            ),
            49 => 
            array (
                'id' => 50,
                'charity_id' => 1,
                'amount' => '1.00',
                'booking_id' => 156272,
                'created_at' => '2020-06-12 12:18:55',
                'updated_at' => '2020-06-12 12:18:55',
            ),
            50 => 
            array (
                'id' => 51,
                'charity_id' => 1,
                'amount' => '1.00',
                'booking_id' => 156273,
                'created_at' => '2020-06-12 12:24:36',
                'updated_at' => '2020-06-12 12:24:36',
            ),
            51 => 
            array (
                'id' => 52,
                'charity_id' => 1,
                'amount' => '1.00',
                'booking_id' => 156276,
                'created_at' => '2020-06-12 13:03:30',
                'updated_at' => '2020-06-12 13:03:30',
            ),
            52 => 
            array (
                'id' => 53,
                'charity_id' => 1,
                'amount' => '1.00',
                'booking_id' => 156281,
                'created_at' => '2020-06-15 10:25:55',
                'updated_at' => '2020-06-15 10:25:55',
            ),
            53 => 
            array (
                'id' => 54,
                'charity_id' => 1,
                'amount' => '1.00',
                'booking_id' => 156282,
                'created_at' => '2020-06-19 09:44:11',
                'updated_at' => '2020-06-19 09:44:11',
            ),
            54 => 
            array (
                'id' => 55,
                'charity_id' => 1,
                'amount' => '1.00',
                'booking_id' => 156358,
                'created_at' => '2020-07-14 14:34:26',
                'updated_at' => '2020-07-14 14:34:26',
            ),
            55 => 
            array (
                'id' => 56,
                'charity_id' => 1,
                'amount' => '1.00',
                'booking_id' => 156362,
                'created_at' => '2020-07-24 11:33:02',
                'updated_at' => '2020-07-24 11:33:02',
            ),
            56 => 
            array (
                'id' => 57,
                'charity_id' => 1,
                'amount' => '1.00',
                'booking_id' => 156365,
                'created_at' => '2020-07-24 11:45:31',
                'updated_at' => '2020-07-24 11:45:31',
            ),
            57 => 
            array (
                'id' => 58,
                'charity_id' => 1,
                'amount' => '1.00',
                'booking_id' => 156366,
                'created_at' => '2020-07-24 11:47:15',
                'updated_at' => '2020-07-24 11:47:15',
            ),
            58 => 
            array (
                'id' => 59,
                'charity_id' => 1,
                'amount' => '1.00',
                'booking_id' => 156364,
                'created_at' => '2020-07-24 11:52:46',
                'updated_at' => '2020-07-24 11:52:46',
            ),
            59 => 
            array (
                'id' => 60,
                'charity_id' => 1,
                'amount' => '1.00',
                'booking_id' => 156369,
                'created_at' => '2020-07-24 12:01:11',
                'updated_at' => '2020-07-24 12:01:11',
            ),
            60 => 
            array (
                'id' => 61,
                'charity_id' => 1,
                'amount' => '1.00',
                'booking_id' => 156371,
                'created_at' => '2020-07-24 12:13:10',
                'updated_at' => '2020-07-24 12:13:10',
            ),
            61 => 
            array (
                'id' => 62,
                'charity_id' => 1,
                'amount' => '1.00',
                'booking_id' => 156373,
                'created_at' => '2020-07-24 13:24:36',
                'updated_at' => '2020-07-24 13:24:36',
            ),
            62 => 
            array (
                'id' => 63,
                'charity_id' => 1,
                'amount' => '1.00',
                'booking_id' => 156375,
                'created_at' => '2020-07-24 13:27:14',
                'updated_at' => '2020-07-24 13:27:14',
            ),
            63 => 
            array (
                'id' => 64,
                'charity_id' => 1,
                'amount' => '1.00',
                'booking_id' => 156414,
                'created_at' => '2020-09-15 11:38:59',
                'updated_at' => '2020-09-15 11:38:59',
            ),
            64 => 
            array (
                'id' => 65,
                'charity_id' => 1,
                'amount' => '1.00',
                'booking_id' => 156413,
                'created_at' => '2020-09-15 11:44:50',
                'updated_at' => '2020-09-15 11:44:50',
            ),
        ));
        
        
    }
}