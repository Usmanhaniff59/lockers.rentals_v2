<?php

use Illuminate\Database\Seeder;

class SchoolYearTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('school_year')->delete();
        
        \DB::table('school_year')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => '1 Sep, 2019 - 1 Aug, 2020',
                'from_date' => '2019-09-01 00:00:00',
                'to_date' => '2020-08-01 23:59:59',
                'status' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'title' => '1 Sep, 2020 - 30 Aug, 2021',
                'from_date' => '2020-09-01 00:00:00',
                'to_date' => '2021-08-01 23:59:59',
                'status' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'title' => '01 Sep, 2021 - 01 Aug, 2022',
                'from_date' => '2021-09-01 00:00:00',
                'to_date' => '2022-08-01 23:59:59',
                'status' => 0,
                'created_at' => '2020-05-01 20:25:22',
                'updated_at' => '2020-05-01 20:25:22',
            ),
            3 => 
            array (
                'id' => 4,
                'title' => '01 Sep, 2022 - 01 Aug, 2023',
                'from_date' => '2022-09-01 00:00:00',
                'to_date' => '2023-08-01 23:59:59',
                'status' => 0,
                'created_at' => '2020-08-13 12:45:08',
                'updated_at' => '2020-08-13 12:45:08',
            ),
            4 => 
            array (
                'id' => 5,
                'title' => '01 Sep, 2021 - 01 Aug, 2022',
                'from_date' => '2021-09-01 00:00:00',
                'to_date' => '2022-08-01 23:59:59',
                'status' => 0,
                'created_at' => '2020-08-20 16:55:48',
                'updated_at' => '2020-08-20 16:55:48',
            ),
            5 => 
            array (
                'id' => 6,
                'title' => '01 Sep, 2021 - 01 Aug, 2022',
                'from_date' => '2021-09-01 00:00:00',
                'to_date' => '2022-08-01 23:59:59',
                'status' => 0,
                'created_at' => '2020-08-20 16:55:51',
                'updated_at' => '2020-08-20 16:55:51',
            ),
            6 => 
            array (
                'id' => 7,
                'title' => '01 Sep, 2021 - 01 Aug, 2022',
                'from_date' => '2021-09-01 00:00:00',
                'to_date' => '2022-08-01 23:59:59',
                'status' => 0,
                'created_at' => '2020-09-15 11:38:58',
                'updated_at' => '2020-09-15 11:38:58',
            ),
            7 => 
            array (
                'id' => 8,
                'title' => '01 Sep, 2021 - 01 Aug, 2022',
                'from_date' => '2021-09-01 00:00:00',
                'to_date' => '2022-08-01 23:59:59',
                'status' => 0,
                'created_at' => '2020-09-15 11:44:49',
                'updated_at' => '2020-09-15 11:44:49',
            ),
        ));
        
        
    }
}