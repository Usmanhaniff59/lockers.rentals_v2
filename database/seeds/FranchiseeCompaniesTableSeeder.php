<?php

use Illuminate\Database\Seeder;

class FranchiseeCompaniesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('franchisee_companies')->delete();
        
        \DB::table('franchisee_companies')->insert(array (
            0 => 
            array (
                'id' => 3,
                'franchisee_id' => 1,
                'company_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 5,
                'franchisee_id' => 1,
                'company_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}