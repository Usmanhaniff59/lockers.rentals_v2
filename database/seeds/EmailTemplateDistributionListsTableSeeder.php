<?php

use Illuminate\Database\Seeder;

class EmailTemplateDistributionListsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('email_template_distribution_lists')->delete();
        
        \DB::table('email_template_distribution_lists')->insert(array (
            0 => 
            array (
                'id' => 1,
                'email_template_id' => 1,
                'user_id' => 1,
                'name' => 'admin',
                'email' => 'admin@gmail.com',
                'sms' => 'coming from email template updateemailtemplate',
                'created_at' => '2020-01-17 13:23:11',
                'updated_at' => '2020-01-17 13:23:11',
            ),
            1 => 
            array (
                'id' => 2,
                'email_template_id' => 1,
                'user_id' => 36,
                'name' => '1',
                'email' => 'sufyanhashmi301@gmail.com',
                'sms' => 'coming from email template updateemailtemplate',
                'created_at' => '2020-01-17 13:23:11',
                'updated_at' => '2020-01-17 13:23:11',
            ),
        ));
        
        
    }
}