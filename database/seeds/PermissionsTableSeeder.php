<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Create Post',
                'created_at' => '2019-07-17 02:52:13',
                'updated_at' => '2019-07-17 03:05:37',
            ),
            1 => 
            array (
                'id' => 3,
                'name' => 'Delete Post',
                'created_at' => '2019-07-17 02:53:20',
                'updated_at' => '2019-07-17 03:06:00',
            ),
            2 => 
            array (
                'id' => 4,
                'name' => 'Administer roles & permissions',
                'created_at' => '2019-07-17 03:06:14',
                'updated_at' => '2019-07-24 21:07:10',
            ),
            3 => 
            array (
                'id' => 7,
                'name' => 'Edit Post',
                'created_at' => '2019-07-24 21:10:37',
                'updated_at' => '2019-07-24 21:10:37',
            ),
            4 => 
            array (
                'id' => 8,
                'name' => 'Create Page',
                'created_at' => '2019-07-28 08:59:41',
                'updated_at' => '2019-07-28 08:59:41',
            ),
            5 => 
            array (
                'id' => 9,
                'name' => 'Edit Page',
                'created_at' => '2019-07-28 09:00:02',
                'updated_at' => '2019-07-28 09:00:02',
            ),
            6 => 
            array (
                'id' => 10,
                'name' => 'Delete Page',
                'created_at' => '2019-07-28 09:00:21',
                'updated_at' => '2019-07-28 09:00:21',
            ),
            7 => 
            array (
                'id' => 11,
                'name' => 'Posts Management',
                'created_at' => '2019-07-28 21:34:49',
                'updated_at' => '2020-03-19 17:25:01',
            ),
            8 => 
            array (
                'id' => 12,
                'name' => 'Pages Management',
                'created_at' => '2019-07-28 21:35:53',
                'updated_at' => '2020-03-19 17:25:44',
            ),
            9 => 
            array (
                'id' => 14,
                'name' => 'Create Company',
                'created_at' => '2019-08-06 10:26:43',
                'updated_at' => '2019-09-21 01:52:37',
            ),
            10 => 
            array (
                'id' => 15,
                'name' => 'Create DistributionListGroup',
                'created_at' => '2019-08-08 04:33:19',
                'updated_at' => '2019-08-08 04:33:19',
            ),
            11 => 
            array (
                'id' => 16,
                'name' => 'Edit DistributionListGroup',
                'created_at' => '2019-08-08 11:27:56',
                'updated_at' => '2019-08-08 11:27:56',
            ),
            12 => 
            array (
                'id' => 17,
                'name' => 'Delete DistributionListGroup',
                'created_at' => '2019-08-08 11:28:13',
                'updated_at' => '2019-08-08 11:28:13',
            ),
            13 => 
            array (
                'id' => 18,
                'name' => 'Create EmailTemplate',
                'created_at' => '2019-08-08 11:39:00',
                'updated_at' => '2019-08-08 11:39:00',
            ),
            14 => 
            array (
                'id' => 19,
                'name' => 'Edit EmailTemplate',
                'created_at' => '2019-08-08 11:39:16',
                'updated_at' => '2019-08-08 11:39:16',
            ),
            15 => 
            array (
                'id' => 20,
                'name' => 'Delete EmailTemplate',
                'created_at' => '2019-08-08 11:39:35',
                'updated_at' => '2019-08-08 11:39:35',
            ),
            16 => 
            array (
                'id' => 21,
                'name' => 'Edit Company',
                'created_at' => '2019-08-08 12:35:08',
                'updated_at' => '2019-08-08 12:35:08',
            ),
            17 => 
            array (
                'id' => 22,
                'name' => 'Delete Company',
                'created_at' => '2019-08-08 12:35:26',
                'updated_at' => '2019-08-08 12:35:26',
            ),
            18 => 
            array (
                'id' => 23,
                'name' => 'Edit BookingType',
                'created_at' => '2019-08-08 12:45:56',
                'updated_at' => '2019-08-08 12:45:56',
            ),
            19 => 
            array (
                'id' => 24,
                'name' => 'Create Help',
                'created_at' => '2019-08-10 00:10:30',
                'updated_at' => '2019-08-10 00:10:30',
            ),
            20 => 
            array (
                'id' => 25,
                'name' => 'Edit Help',
                'created_at' => '2019-08-10 00:10:42',
                'updated_at' => '2019-08-10 00:10:42',
            ),
            21 => 
            array (
                'id' => 26,
                'name' => 'Delete Help',
                'created_at' => '2019-08-10 00:10:58',
                'updated_at' => '2019-08-10 00:10:58',
            ),
            22 => 
            array (
                'id' => 28,
                'name' => 'Create Label',
                'created_at' => '2019-08-23 15:08:36',
                'updated_at' => '2019-08-23 15:11:18',
            ),
            23 => 
            array (
                'id' => 29,
                'name' => 'Edit Label',
                'created_at' => '2019-08-23 15:08:51',
                'updated_at' => '2019-08-23 15:08:51',
            ),
            24 => 
            array (
                'id' => 30,
                'name' => 'Delete Label',
                'created_at' => '2019-08-23 15:09:11',
                'updated_at' => '2019-08-23 15:09:11',
            ),
            25 => 
            array (
                'id' => 31,
                'name' => 'Email Management',
                'created_at' => '2019-08-27 11:34:26',
                'updated_at' => '2019-08-27 11:34:26',
            ),
            26 => 
            array (
                'id' => 34,
                'name' => 'Create Slider',
                'created_at' => '2019-09-11 00:22:54',
                'updated_at' => '2019-09-11 00:22:54',
            ),
            27 => 
            array (
                'id' => 35,
                'name' => 'Edit CMS',
                'created_at' => '2019-09-11 22:31:11',
                'updated_at' => '2019-09-11 22:31:11',
            ),
            28 => 
            array (
                'id' => 36,
                'name' => 'Edit Slider',
                'created_at' => '2019-09-18 03:10:26',
                'updated_at' => '2019-09-18 03:10:26',
            ),
            29 => 
            array (
                'id' => 37,
                'name' => 'Delete Slider',
                'created_at' => '2019-09-18 03:10:42',
                'updated_at' => '2019-09-18 03:10:42',
            ),
            30 => 
            array (
                'id' => 38,
                'name' => 'Create Language',
                'created_at' => '2019-10-03 05:17:03',
                'updated_at' => '2019-10-03 05:17:03',
            ),
            31 => 
            array (
                'id' => 39,
                'name' => 'Edit Language',
                'created_at' => '2019-10-03 05:17:28',
                'updated_at' => '2019-10-03 05:17:28',
            ),
            32 => 
            array (
                'id' => 42,
                'name' => 'Delete Language',
                'created_at' => '2019-10-03 05:18:50',
                'updated_at' => '2019-10-03 05:18:50',
            ),
            33 => 
            array (
                'id' => 44,
                'name' => 'Company Management',
                'created_at' => '2020-02-06 03:39:28',
                'updated_at' => '2020-02-06 03:39:28',
            ),
            34 => 
            array (
                'id' => 45,
                'name' => 'Order Management',
                'created_at' => '2020-02-10 23:16:29',
                'updated_at' => '2020-03-20 00:49:29',
            ),
            35 => 
            array (
                'id' => 46,
                'name' => 'User Management',
                'created_at' => '2020-02-11 01:22:23',
                'updated_at' => '2020-02-11 01:35:08',
            ),
            36 => 
            array (
                'id' => 47,
                'name' => 'Create User',
                'created_at' => '2020-02-11 01:24:46',
                'updated_at' => '2020-02-11 01:24:46',
            ),
            37 => 
            array (
                'id' => 49,
                'name' => 'Edit Locker Price',
                'created_at' => '2020-02-19 05:55:57',
                'updated_at' => '2020-02-19 05:55:57',
            ),
            38 => 
            array (
                'id' => 50,
                'name' => 'Refund Order',
                'created_at' => '2020-02-21 00:51:11',
                'updated_at' => '2020-03-20 00:50:52',
            ),
            39 => 
            array (
                'id' => 52,
                'name' => 'Assign Role To User',
                'created_at' => '2020-02-24 01:43:35',
                'updated_at' => '2020-03-30 13:55:14',
            ),
            40 => 
            array (
                'id' => 53,
                'name' => 'Assign Franchisee To User',
                'created_at' => '2020-02-24 01:45:22',
                'updated_at' => '2020-02-24 01:45:22',
            ),
            41 => 
            array (
                'id' => 99,
                'name' => 'Delete User',
                'created_at' => '2020-02-24 07:55:39',
                'updated_at' => '2020-02-24 07:55:39',
            ),
            42 => 
            array (
                'id' => 57,
                'name' => 'Rolls and Permissions Management',
                'created_at' => '2020-03-19 17:05:12',
                'updated_at' => '2020-03-19 17:05:12',
            ),
            43 => 
            array (
                'id' => 58,
                'name' => 'Permissions Management',
                'created_at' => '2020-03-19 17:06:09',
                'updated_at' => '2020-03-19 17:06:09',
            ),
            44 => 
            array (
                'id' => 59,
                'name' => 'Rolls Management',
                'created_at' => '2020-03-19 17:07:17',
                'updated_at' => '2020-03-19 17:07:17',
            ),
            45 => 
            array (
                'id' => 60,
                'name' => 'Administration Management',
                'created_at' => '2020-03-19 17:09:16',
                'updated_at' => '2020-03-19 17:09:16',
            ),
            46 => 
            array (
                'id' => 61,
                'name' => 'Booking Types Management',
                'created_at' => '2020-03-19 17:10:03',
                'updated_at' => '2020-03-19 17:10:03',
            ),
            47 => 
            array (
                'id' => 62,
                'name' => 'Email Templates Management',
                'created_at' => '2020-03-19 17:11:52',
                'updated_at' => '2020-03-19 17:11:52',
            ),
            48 => 
            array (
                'id' => 63,
                'name' => 'DistributionListGroup Management',
                'created_at' => '2020-03-19 17:15:11',
                'updated_at' => '2020-03-19 17:16:24',
            ),
            49 => 
            array (
                'id' => 64,
                'name' => 'Help Management',
                'created_at' => '2020-03-19 17:19:24',
                'updated_at' => '2020-03-19 17:19:24',
            ),
            50 => 
            array (
                'id' => 65,
                'name' => 'Language Management',
                'created_at' => '2020-03-19 17:20:12',
                'updated_at' => '2020-03-19 17:20:12',
            ),
            51 => 
            array (
                'id' => 66,
                'name' => 'Slider Management',
                'created_at' => '2020-03-19 17:20:57',
                'updated_at' => '2020-03-19 17:20:57',
            ),
            52 => 
            array (
                'id' => 67,
                'name' => 'Transaction Log Order',
                'created_at' => '2020-03-20 00:52:49',
                'updated_at' => '2020-03-20 00:52:49',
            ),
            53 => 
            array (
                'id' => 68,
                'name' => 'Bookings Order',
                'created_at' => '2020-03-20 00:53:06',
                'updated_at' => '2020-03-20 00:53:06',
            ),
            54 => 
            array (
                'id' => 69,
                'name' => 'Edit User',
                'created_at' => '2020-03-20 01:51:18',
                'updated_at' => '2020-03-20 01:51:18',
            ),
            55 => 
            array (
                'id' => 70,
                'name' => 'All Records',
                'created_at' => '2020-03-28 01:46:14',
                'updated_at' => '2020-03-28 01:46:14',
            ),
            56 => 
            array (
                'id' => 71,
                'name' => '3D Secure',
                'created_at' => '2020-03-28 14:42:05',
                'updated_at' => '2020-03-28 14:42:05',
            ),
            57 => 
            array (
                'id' => 73,
                'name' => 'Assign Company To User',
                'created_at' => '2020-04-09 09:17:52',
                'updated_at' => '2020-04-09 09:17:52',
            ),
            58 => 
            array (
                'id' => 74,
                'name' => 'Resend Email',
                'created_at' => '2020-05-13 17:59:34',
                'updated_at' => '2020-05-13 17:59:34',
            ),
            59 => 
            array (
                'id' => 75,
                'name' => 'Availability Report',
                'created_at' => '2020-05-22 09:39:10',
                'updated_at' => '2020-05-22 09:39:10',
            ),
            60 => 
            array (
                'id' => 76,
                'name' => 'Booking Report',
                'created_at' => '2020-05-22 09:39:27',
                'updated_at' => '2020-05-22 09:39:27',
            ),
            61 => 
            array (
                'id' => 77,
                'name' => 'Sales Report',
                'created_at' => '2020-05-22 09:39:42',
                'updated_at' => '2020-05-22 09:39:42',
            ),
            62 => 
            array (
                'id' => 79,
                'name' => 'Voucher Code',
                'created_at' => '2020-05-25 07:47:41',
                'updated_at' => '2020-05-25 07:47:41',
            ),
            63 => 
            array (
                'id' => 80,
                'name' => 'Edit Booking',
                'created_at' => '2020-05-29 19:01:23',
                'updated_at' => '2020-05-29 19:01:23',
            ),
            64 => 
            array (
                'id' => 81,
                'name' => 'Account Balance',
                'created_at' => '2020-06-10 11:45:30',
                'updated_at' => '2020-06-10 11:45:30',
            ),
            65 => 
            array (
                'id' => 82,
                'name' => 'Move Booking',
                'created_at' => '2020-08-11 14:08:07',
                'updated_at' => '2020-08-11 14:08:07',
            ),
            66 => 
            array (
                'id' => 83,
                'name' => 'Edit Orders',
                'created_at' => '2020-08-13 16:43:26',
                'updated_at' => '2020-08-13 16:43:26',
            ),
            67 => 
            array (
                'id' => 84,
                'name' => 'Company Errors',
                'created_at' => '2020-08-21 18:16:06',
                'updated_at' => '2020-08-21 18:16:06',
            ),
            68 => 
            array (
                'id' => 86,
                'name' => 'Students',
                'created_at' => '2020-09-04 10:37:28',
                'updated_at' => '2020-09-04 10:37:28',
            ),
            69 => 
            array (
                'id' => 87,
                'name' => 'Chat Report',
                'created_at' => '2020-09-28 12:48:33',
                'updated_at' => '2020-09-28 12:48:33',
            ),
            70 => 
            array (
                'id' => 88,
                'name' => 'Franchisee Report',
                'created_at' => '2020-09-28 12:48:51',
                'updated_at' => '2020-09-28 12:48:51',
            ),
            71 => 
            array (
                'id' => 89,
                'name' => 'Document Management',
                'created_at' => '2020-09-28 12:49:58',
                'updated_at' => '2020-09-28 12:49:58',
            ),
            72 => 
            array (
                'id' => 90,
                'name' => 'Assign Company To Franchisee',
                'created_at' => '2020-09-28 13:26:51',
                'updated_at' => '2020-09-28 13:26:51',
            ),
            73 => 
            array (
                'id' => 91,
                'name' => 'Assign Status Type To Company',
                'created_at' => '2020-09-28 13:27:05',
                'updated_at' => '2020-09-28 13:27:05',
            ),
            74 => 
            array (
                'id' => 92,
                'name' => 'Offsale Report',
                'created_at' => '2020-09-29 05:21:49',
                'updated_at' => '2020-09-29 05:21:49',
            ),
            75 => 
            array (
                'id' => 93,
                'name' => 'Current Maintenance OffSale Report',
                'created_at' => '2020-10-06 12:57:48',
                'updated_at' => '2020-10-06 12:57:48',
            ),
            76 => 
            array (
                'id' => 94,
                'name' => 'Maintenance OffSale Report',
                'created_at' => '2020-10-06 13:00:06',
                'updated_at' => '2020-10-06 13:00:06',
            ),
            77 => 
            array (
                'id' => 95,
                'name' => 'Academic Year Report',
                'created_at' => '2020-10-13 13:17:21',
                'updated_at' => '2020-10-13 13:17:21',
            ),
            78 => 
            array (
                'id' => 96,
                'name' => 'Voucher Payments Report',
                'created_at' => '2020-10-28 05:22:33',
                'updated_at' => '2020-10-28 05:22:33',
            ),
            79 => 
            array (
                'id' => 97,
                'name' => 'Pupil Premium Report',
                'created_at' => '2020-11-04 05:35:30',
                'updated_at' => '2020-11-04 05:35:30',
            ),
            80 => 
            array (
                'id' => 98,
                'name' => 'Company Booking Report',
                'created_at' => '2020-11-04 05:36:53',
                'updated_at' => '2020-11-04 05:36:53',
            ),
            81 => 
            array (
                'id' => 100,
                'name' => 'Locker Block Images',
                'created_at' => '2020-11-24 07:21:05',
                'updated_at' => '2020-11-24 07:21:05',
            ),
            82 => array (
                'id' => 101,
                'name' => 'Block Groups',
                'created_at' => '2020-11-24 07:21:05',
                'updated_at' => '2020-11-24 07:21:05',
            ),
            83 => array (
                'id' => 102,
                'name' => 'Locker Description',
                'created_at' => '2020-11-24 07:21:05',
                'updated_at' => '2020-11-24 07:21:05',
            ),
            84 => array (
                'id' => 103,
                'name' => 'Locker Number',
                'created_at' => '2020-11-24 07:21:05',
                'updated_at' => '2020-11-24 07:21:05',
            ),
            85 => array (
                'id' => 104,
                'name' => 'Ticketing Permission',
                'created_at' => '2020-11-24 07:21:05',
                'updated_at' => '2020-11-24 07:21:05',
            ),
        ));
        
        
    }
}