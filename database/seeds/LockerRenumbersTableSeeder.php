<?php

use Illuminate\Database\Seeder;

class LockerRenumbersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('locker_renumbers')->delete();
        
        \DB::table('locker_renumbers')->insert(array (
            0 => 
            array (
                'id' => 143,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 597,
                'new_number' => 108,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            1 => 
            array (
                'id' => 142,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 594,
                'new_number' => 107,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            2 => 
            array (
                'id' => 141,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 593,
                'new_number' => 106,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            3 => 
            array (
                'id' => 140,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 592,
                'new_number' => 105,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            4 => 
            array (
                'id' => 139,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 121,
                'new_number' => 104,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            5 => 
            array (
                'id' => 138,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 590,
                'new_number' => 103,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            6 => 
            array (
                'id' => 137,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 589,
                'new_number' => 102,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            7 => 
            array (
                'id' => 136,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 588,
                'new_number' => 101,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            8 => 
            array (
                'id' => 135,
                'locker_id' => 39099,
                'user_id' => 319,
                'old_number' => 596,
                'new_number' => 555,
                'created_at' => '2020-08-18 11:18:16',
                'updated_at' => '2020-08-18 11:18:16',
            ),
            9 => 
            array (
                'id' => 134,
                'locker_id' => 39099,
                'user_id' => 319,
                'old_number' => 596,
                'new_number' => 444,
                'created_at' => '2020-08-18 11:16:33',
                'updated_at' => '2020-08-18 11:16:33',
            ),
            10 => 
            array (
                'id' => 133,
                'locker_id' => 39099,
                'user_id' => 319,
                'old_number' => 596,
                'new_number' => 131,
                'created_at' => '2020-08-18 11:15:50',
                'updated_at' => '2020-08-18 11:15:50',
            ),
            11 => 
            array (
                'id' => 132,
                'locker_id' => 39099,
                'user_id' => 319,
                'old_number' => 596,
                'new_number' => 333,
                'created_at' => '2020-08-18 11:15:37',
                'updated_at' => '2020-08-18 11:15:37',
            ),
            12 => 
            array (
                'id' => 131,
                'locker_id' => 39098,
                'user_id' => 319,
                'old_number' => 595,
                'new_number' => 595,
                'created_at' => '2020-08-18 11:02:48',
                'updated_at' => '2020-08-18 11:02:48',
            ),
            13 => 
            array (
                'id' => 130,
                'locker_id' => 39094,
                'user_id' => 319,
                'old_number' => 591,
                'new_number' => 121,
                'created_at' => '2020-08-18 10:59:23',
                'updated_at' => '2020-08-18 10:59:23',
            ),
            14 => 
            array (
                'id' => 129,
                'locker_id' => 39072,
                'user_id' => 319,
                'old_number' => 569,
                'new_number' => 569,
                'created_at' => '2020-08-18 10:22:48',
                'updated_at' => '2020-08-18 10:22:48',
            ),
            15 => 
            array (
                'id' => 128,
                'locker_id' => 39072,
                'user_id' => 319,
                'old_number' => 569,
                'new_number' => 569,
                'created_at' => '2020-08-18 10:16:21',
                'updated_at' => '2020-08-18 10:16:21',
            ),
            16 => 
            array (
                'id' => 127,
                'locker_id' => 39084,
                'user_id' => 319,
                'old_number' => 111,
                'new_number' => 111,
                'created_at' => '2020-08-18 07:31:52',
                'updated_at' => '2020-08-18 07:31:52',
            ),
            17 => 
            array (
                'id' => 126,
                'locker_id' => 247,
                'user_id' => 319,
                'old_number' => 9,
                'new_number' => 9,
                'created_at' => '2020-07-27 14:58:48',
                'updated_at' => '2020-07-27 14:58:48',
            ),
            18 => 
            array (
                'id' => 125,
                'locker_id' => 44758,
                'user_id' => 319,
                'old_number' => 10,
                'new_number' => 10,
                'created_at' => '2020-07-27 14:56:58',
                'updated_at' => '2020-07-27 14:56:58',
            ),
            19 => 
            array (
                'id' => 124,
                'locker_id' => 44696,
                'user_id' => 319,
                'old_number' => 10,
                'new_number' => 10,
                'created_at' => '2020-07-27 14:55:37',
                'updated_at' => '2020-07-27 14:55:37',
            ),
            20 => 
            array (
                'id' => 123,
                'locker_id' => 44687,
                'user_id' => 319,
                'old_number' => 1,
                'new_number' => 1,
                'created_at' => '2020-07-27 14:55:37',
                'updated_at' => '2020-07-27 14:55:37',
            ),
            21 => 
            array (
                'id' => 122,
                'locker_id' => 44772,
                'user_id' => 319,
                'old_number' => 6,
                'new_number' => 6,
                'created_at' => '2020-07-27 14:43:34',
                'updated_at' => '2020-07-27 14:43:34',
            ),
            22 => 
            array (
                'id' => 121,
                'locker_id' => 44770,
                'user_id' => 319,
                'old_number' => 4,
                'new_number' => 4,
                'created_at' => '2020-07-27 14:43:34',
                'updated_at' => '2020-07-27 14:43:34',
            ),
            23 => 
            array (
                'id' => 120,
                'locker_id' => 44768,
                'user_id' => 319,
                'old_number' => 2,
                'new_number' => 2,
                'created_at' => '2020-07-27 14:43:34',
                'updated_at' => '2020-07-27 14:43:34',
            ),
            24 => 
            array (
                'id' => 119,
                'locker_id' => 44769,
                'user_id' => 319,
                'old_number' => 3,
                'new_number' => 3,
                'created_at' => '2020-07-27 14:43:11',
                'updated_at' => '2020-07-27 14:43:11',
            ),
            25 => 
            array (
                'id' => 118,
                'locker_id' => 44768,
                'user_id' => 319,
                'old_number' => 2,
                'new_number' => 2,
                'created_at' => '2020-07-27 14:43:11',
                'updated_at' => '2020-07-27 14:43:11',
            ),
            26 => 
            array (
                'id' => 117,
                'locker_id' => 44771,
                'user_id' => 319,
                'old_number' => 5,
                'new_number' => 5,
                'created_at' => '2020-07-27 14:42:55',
                'updated_at' => '2020-07-27 14:42:55',
            ),
            27 => 
            array (
                'id' => 116,
                'locker_id' => 44675,
                'user_id' => 319,
                'old_number' => 100,
                'new_number' => 1,
                'created_at' => '2020-07-24 07:36:44',
                'updated_at' => '2020-07-24 07:36:44',
            ),
            28 => 
            array (
                'id' => 144,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 598,
                'new_number' => 109,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            29 => 
            array (
                'id' => 145,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 599,
                'new_number' => 110,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            30 => 
            array (
                'id' => 146,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 600,
                'new_number' => 111,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            31 => 
            array (
                'id' => 147,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 601,
                'new_number' => 112,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            32 => 
            array (
                'id' => 148,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 602,
                'new_number' => 113,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            33 => 
            array (
                'id' => 149,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 603,
                'new_number' => 114,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            34 => 
            array (
                'id' => 150,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 604,
                'new_number' => 115,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            35 => 
            array (
                'id' => 151,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 605,
                'new_number' => 116,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            36 => 
            array (
                'id' => 152,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 606,
                'new_number' => 117,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            37 => 
            array (
                'id' => 153,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 607,
                'new_number' => 118,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            38 => 
            array (
                'id' => 154,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 608,
                'new_number' => 119,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            39 => 
            array (
                'id' => 155,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 609,
                'new_number' => 120,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            40 => 
            array (
                'id' => 156,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 610,
                'new_number' => 121,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            41 => 
            array (
                'id' => 157,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 611,
                'new_number' => 122,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            42 => 
            array (
                'id' => 158,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 612,
                'new_number' => 123,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            43 => 
            array (
                'id' => 159,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 613,
                'new_number' => 124,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            44 => 
            array (
                'id' => 160,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 614,
                'new_number' => 125,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            45 => 
            array (
                'id' => 161,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 615,
                'new_number' => 126,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            46 => 
            array (
                'id' => 162,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 616,
                'new_number' => 127,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            47 => 
            array (
                'id' => 163,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 617,
                'new_number' => 128,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            48 => 
            array (
                'id' => 164,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 618,
                'new_number' => 129,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            49 => 
            array (
                'id' => 165,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 619,
                'new_number' => 130,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            50 => 
            array (
                'id' => 166,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 620,
                'new_number' => 131,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            51 => 
            array (
                'id' => 167,
                'locker_id' => 39090,
                'user_id' => 319,
                'old_number' => 111,
                'new_number' => 100,
                'created_at' => '2020-08-18 11:31:06',
                'updated_at' => '2020-08-18 11:31:06',
            ),
            52 => 
            array (
                'id' => 168,
                'locker_id' => 45456,
                'user_id' => 319,
                'old_number' => 1,
                'new_number' => 42,
                'created_at' => '2020-09-16 06:51:02',
                'updated_at' => '2020-09-16 06:51:02',
            ),
            53 => 
            array (
                'id' => 169,
                'locker_id' => 45469,
                'user_id' => 319,
                'old_number' => 14,
                'new_number' => 42,
                'created_at' => '2020-09-16 06:51:13',
                'updated_at' => '2020-09-16 06:51:13',
            ),
            54 => 
            array (
                'id' => 170,
                'locker_id' => 35565,
                'user_id' => 319,
                'old_number' => 309,
                'new_number' => 301,
                'created_at' => '2020-09-17 11:16:41',
                'updated_at' => '2020-09-17 11:16:41',
            ),
        ));
        
        
    }
}