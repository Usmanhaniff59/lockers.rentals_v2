<?php

use Illuminate\Database\Seeder;

class MarketingPicturesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('marketing_pictures')->delete();
        
        \DB::table('marketing_pictures')->insert(array (
            0 => 
            array (
                'id' => 1,
                'file' => 'locker-rotate-1.jpg',
                'active' => 1,
                'deleted' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'file' => 'locker-rotate-2.jpg',
                'active' => 1,
                'deleted' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}