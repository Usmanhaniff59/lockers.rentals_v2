<?php

use Illuminate\Database\Seeder;

class SalesUpdateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	\DB::table('sales')->Where('status','l')->orWhere('status','r')->update(array('net_pass' => NULL));
    }
}
