<?php

use Illuminate\Database\Seeder;

class TextsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('texts')->delete();
        
        \DB::table('texts')->insert(array (
            0 => 
            array (
                'key' => '%login%',
                'lang' => 'en',
                'value' => 'Login',
                'scope' => 'global',
                'created_at' => '2020-05-05 04:08:24',
                'updated_at' => '2020-05-05 04:08:24',
            ),
            1 => 
            array (
                'key' => '%login%',
                'lang' => 'fr',
                'value' => 'Login',
                'scope' => 'global',
                'created_at' => '2020-05-05 04:09:22',
                'updated_at' => '2020-05-05 04:09:22',
            ),
            2 => 
            array (
                'key' => 'about-us-heading',
                'lang' => 'en',
                'value' => 'About Us',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            3 => 
            array (
                'key' => 'about-us-heading',
                'lang' => 'fr',
                'value' => 'About Us',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            4 => 
            array (
                'key' => 'about-us-menu',
                'lang' => 'en',
                'value' => 'About',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-20 14:33:13',
            ),
            5 => 
            array (
                'key' => 'about-us-menu',
                'lang' => 'fr',
                'value' => 'About',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            6 => 
            array (
                'key' => 'about-us-menu',
                'lang' => 'it',
                'value' => 'about-us-menu',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            7 => 
            array (
                'key' => 'about-us-section1-heading',
                'lang' => 'en',
                'value' => 'About Plans',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            8 => 
            array (
                'key' => 'about-us-section1-heading',
                'lang' => 'fr',
                'value' => 'About Plans',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            9 => 
            array (
                'key' => 'about-us-section1-text',
                'lang' => 'en',
                'value' => '<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal;mso-outline-level:
4">School Locker Rentals</p><p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal;mso-outline-level:
4">If you want to install new lockers or need to replace your old rusty metal lockers but don’t have enough capital budget to cover the cost we have the solution. Secure Locker Rentals has been designed specifically for you. Working in partnership with your school we will install our high quality lockers in your school completely free of charge.</p><p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal;mso-outline-level:
4">Pupils then rent a locker compartment for the school year directly from Prefect by visiting www.locker.rentals and paying online. The pupil is then sent the combination for their lock by text or email.</p><p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal;mso-outline-level:
4">At the end of the school year the combination lock on the locker is reset ready to be rented for the following year.</p><p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal;mso-outline-level:
4">Its as simple as that, no keys to manage, no money to collect, everything is processed online and runs automatically. All you have to do is provide the space for the lockers, we do the rest including an annual maintenance visit during which the lockers will be cleaned and maintained ready for the new school year.</p><p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal;mso-outline-level:
4"><span style="font-size: 1rem;">If space is an issue don’t worry, our Polyethylene Lockers are perfect for use outdoors and can be installed under our Rental Service.</span></p><p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal;mso-outline-level:
4">Get in touch today and get ready to install high quality lockers in your school completely free of charge!</p>',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-09 13:13:54',
            ),
            10 => 
            array (
                'key' => 'about-us-section1-text',
                'lang' => 'fr',
                'value' => '<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal;mso-outline-level:
4"><b><span style="font-size:13.5pt;font-family:&quot;&amp;quot&quot;,serif;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#555555;
mso-fareast-language:EN-GB">School Locker Rentals<o:p></o:p></span></b></p>

<p class="MsoNormal" style="line-height:normal"><span style="font-size:12.0pt;
font-family:&quot;&amp;quot&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:#F1F1F1;mso-fareast-language:EN-GB"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal" style="margin-bottom:15.6pt;line-height:normal"><span style="font-size:12.0pt;font-family:&quot;&amp;quot&quot;,serif;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#777777;
mso-fareast-language:EN-GB">If you want to install new lockers or need to
replace your old rusty metal lockers but don’t have enough capital budget to
cover the cost we have the solution.<o:p></o:p></span></p>

<p class="MsoNormal" style="margin-bottom:15.6pt;line-height:normal"><span style="font-size:12.0pt;font-family:&quot;&amp;quot&quot;,serif;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#777777;
mso-fareast-language:EN-GB">Secure Locker Rentals has been designed
specifically for you.<o:p></o:p></span></p>

<p class="MsoNormal" style="margin-bottom:15.6pt;line-height:normal"><span style="font-size:12.0pt;font-family:&quot;&amp;quot&quot;,serif;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#777777;
mso-fareast-language:EN-GB">Working in partnership with your school we will
install our high quality lockers in your school completely free of charge.<o:p></o:p></span></p>

<p class="MsoNormal" style="margin-bottom:15.6pt;line-height:normal"><span style="font-size:12.0pt;font-family:&quot;&amp;quot&quot;,serif;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#777777;
mso-fareast-language:EN-GB">Pupils then rent a locker compartment for the
school year directly from Prefect by visiting </span><a href="https://locker.rentals/" target="_blank"><span style="font-size:12.0pt;
font-family:&quot;&amp;quot&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:#334862;mso-fareast-language:EN-GB">www.locker.rentals
</span></a><span style="font-size:12.0pt;font-family:&quot;&amp;quot&quot;,serif;
mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:#777777;mso-fareast-language:EN-GB">and paying online. The pupil is then
sent the combination for their lock by text or email.<o:p></o:p></span></p>

<p class="MsoNormal" style="margin-bottom:15.6pt;line-height:normal"><span style="font-size:12.0pt;font-family:&quot;&amp;quot&quot;,serif;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#777777;
mso-fareast-language:EN-GB">At the end of the school year the combination lock
on the locker is reset ready to be rented for the following year.<o:p></o:p></span></p>

<p class="MsoNormal" style="margin-bottom:15.6pt;line-height:normal"><span style="font-size:12.0pt;font-family:&quot;&amp;quot&quot;,serif;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#777777;
mso-fareast-language:EN-GB">Its as simple as that, no keys to manage, no money
to collect, everything is processed online and runs automatically. All you have
to do is provide the space for the lockers, we do the rest including an annual
maintenance visit during which the lockers will be cleaned and maintained ready
for the new school year.<o:p></o:p></span></p>

<p class="MsoNormal" style="margin-bottom:15.6pt;line-height:normal"><span style="font-size:12.0pt;font-family:&quot;&amp;quot&quot;,serif;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#777777;
mso-fareast-language:EN-GB">If space is an issue don’t worry, our Polyethylene
Lockers are perfect for use outdoors and can be installed under our Rental
Service.<o:p></o:p></span></p>

<p class="MsoNormal" style="margin-bottom:15.6pt;line-height:normal"><span style="font-size:12.0pt;font-family:&quot;&amp;quot&quot;,serif;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#777777;
mso-fareast-language:EN-GB">Get in touch today and get ready to install high
quality lockers in your school completely free of charge!<o:p></o:p></span></p>

<p class="MsoNormal"><o:p>&nbsp;</o:p></p>',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            11 => 
            array (
                'key' => 'about-us-text',
                'lang' => 'en',
                'value' => '<p>If your question is not listed below please c<a href="https://locker.rentals/contactUs" target="_blank">contact us</a></p>',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-19 20:18:09',
            ),
            12 => 
            array (
                'key' => 'about-us-text',
                'lang' => 'fr',
                'value' => 'This page outlines how locker rentals manage your data and respect your privicy',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            13 => 
            array (
                'key' => 'accept-cookies-link-text',
                'lang' => 'en',
                'value' => 'Accept Cookies',
                'scope' => 'global',
                'created_at' => '2020-04-27 17:21:10',
                'updated_at' => '2020-04-27 20:08:49',
            ),
            14 => 
            array (
                'key' => 'accept-cookies-link-text',
                'lang' => 'fr',
                'value' => 'Accept Cookies',
                'scope' => 'global',
                'created_at' => '2020-04-27 17:21:10',
                'updated_at' => '2020-04-27 20:08:49',
            ),
            15 => 
            array (
                'key' => 'accept-cookies-text',
                'lang' => 'en',
                'value' => 'We use cookies to give you the best online experience %link%',
                'scope' => 'global',
                'created_at' => '2020-04-27 17:21:10',
                'updated_at' => '2020-04-27 20:11:46',
            ),
            16 => 
            array (
                'key' => 'accept-cookies-text',
                'lang' => 'fr',
                'value' => 'We use cookies to give you the best online experience %link%',
                'scope' => 'global',
                'created_at' => '2020-04-27 17:21:10',
                'updated_at' => '2020-04-27 20:11:46',
            ),
            17 => 
            array (
                'key' => 'admin-login-menu',
                'lang' => 'en',
                'value' => 'Login',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            18 => 
            array (
                'key' => 'admin-login-menu',
                'lang' => 'fr',
                'value' => 'Login',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            19 => 
            array (
                'key' => 'admin-login-menu',
                'lang' => 'it',
                'value' => 'admin-login-menu',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            20 => 
            array (
                'key' => 'booking-expired-heading',
                'lang' => 'en',
                'value' => 'Booking Expired',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'key' => 'booking-expired-heading',
                'lang' => 'fr',
                'value' => 'Booking Expired',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'key' => 'booking-expired-section1-text',
                'lang' => 'en',
                'value' => 'We are sorry your booking was reserved till <b> %expirty_date% </b>. If you would like to book another locker please use the locker <b> <a href="%booking_link%">Booking</a></b> page',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'key' => 'booking-expired-section1-text',
                'lang' => 'fr',
                'value' => 'We are sorry your booking was reserved till %expirty_date%. If you would like to book another locker please use the locker %booking_link% page',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'key' => 'booking-expired-text',
                'lang' => 'en',
                'value' => 'Sorry this booking has expired',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => '2020-05-04 21:03:43',
            ),
            25 => 
            array (
                'key' => 'booking-expired-text',
                'lang' => 'fr',
                'value' => 'Sorry This booking has expired',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'key' => 'booking-locker-disclaimer',
                'lang' => 'en',
                'value' => '<p>You are booking a locker at <b>%company_name%</b>. To purchase this locker please complete the booking form.</p><p><b>Booking Overview</b></p><ul><li><span style="font-size: 1rem;">On this page add the lockers you require and login</span></li><li><span style="font-size: 1rem;">Then choose/confirm your locker number</span></li><li><span style="font-size: 1rem;">Finally complete your payment details and confirm your reservation.</span></li></ul><p> </p>',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => '2020-05-19 20:14:55',
            ),
            27 => 
            array (
                'key' => 'booking-locker-disclaimer',
                'lang' => 'fr',
                'value' => 'booking-locker-disclaimer',
                'scope' => 'global',
                'created_at' => '2020-05-18 19:50:10',
                'updated_at' => '2020-05-18 19:50:10',
            ),
            28 => 
            array (
                'key' => 'booking-locker-disclaimer',
                'lang' => 'gb',
                'value' => '<p><p>You are booking a locker at <b>%company_name%</b>. To purchase this locker please complete the booking form.</p><p><b>Booking Overview</b></p><ul><li><span style="font-size: 1rem;">On this page add the lockers you require and login</span></li><li><span style="font-size: 1rem;">Then choose/confirm your locker number</span></li><li><span style="font-size: 1rem;">Finally complete your payment details and confirm your reservation.</span></li></ul><p> </p>

______Start_if_Reserved______
</p><p>We have filled in your booking form, please do check all the details to ensure they are correct. We have held your booking till %hold_booking_date%.</p><p>
______END_if_Reserved______
</p>',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'key' => 'booking-locker-disclaimer-condition',
                'lang' => 'en',
                'value' => 'We have filled in your booking form, please do check all the details to ensure they are correct. We have held your booking till %hold_booking_date%.
',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => '2020-06-02 16:39:40',
            ),
            30 => 
            array (
                'key' => 'booking-locker-disclaimer-condition',
                'lang' => 'gb',
                'value' => 'We have filled in your booking form, please do check all the details to ensure they are correct. We have held your booking till %hold_booking_date%.
',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'key' => 'choose-locker-already-exists-locker',
                'lang' => 'en',
                'value' => 'choose-locker-already-exists-locker',
                'scope' => 'global',
                'created_at' => '2020-05-11 17:41:14',
                'updated_at' => '2020-05-11 17:41:14',
            ),
            32 => 
            array (
                'key' => 'choose-locker-already-exists-locker',
                'lang' => 'fr',
                'value' => 'choose-locker-already-exists-locker',
                'scope' => 'global',
                'created_at' => '2020-05-11 17:41:14',
                'updated_at' => '2020-05-11 17:41:14',
            ),
            33 => 
            array (
                'key' => 'choose-locker-block-is-booked-msg',
                'lang' => 'en',
                'value' => 'This block is fully booked',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'key' => 'choose-locker-blue-key-text',
                'lang' => 'en',
                'value' => 'Selected',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-08 11:50:03',
            ),
            35 => 
            array (
                'key' => 'choose-locker-blue-key-text',
                'lang' => 'fr',
                'value' => 'Booked',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            36 => 
            array (
                'key' => 'choose-locker-change-and-continue-button',
                'lang' => 'en',
                'value' => 'Change And Continue',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'key' => 'choose-locker-child-name-date-title',
                'lang' => 'en',
            'value' => '%child_name% ( %date% )&nbsp;',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            38 => 
            array (
                'key' => 'choose-locker-child-name-date-title',
                'lang' => 'fr',
            'value' => '%child_name% ( %date% )&nbsp;',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            39 => 
            array (
                'key' => 'choose-locker-green-key-text',
                'lang' => 'en',
                'value' => 'Available',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-08 11:49:56',
            ),
            40 => 
            array (
                'key' => 'choose-locker-green-key-text',
                'lang' => 'fr',
                'value' => 'Booked',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            41 => 
            array (
                'key' => 'choose-locker-grid-hidden-button',
                'lang' => 'en',
                'value' => 'Show Available Lockers Only',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-08 11:42:57',
            ),
            42 => 
            array (
                'key' => 'choose-locker-grid-hidden-button',
                'lang' => 'fr',
                'value' => 'Grid Hidden',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            43 => 
            array (
                'key' => 'choose-locker-grid-show-button',
                'lang' => 'en',
                'value' => 'Show all Lockers Booked',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-08 11:41:53',
            ),
            44 => 
            array (
                'key' => 'choose-locker-grid-show-button',
                'lang' => 'fr',
                'value' => 'Grid Show',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            45 => 
            array (
                'key' => 'choose-locker-hide-booked-lockers-title',
                'lang' => 'en',
                'value' => '<b>Booked</b> Lockers',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-08 11:45:50',
            ),
            46 => 
            array (
                'key' => 'choose-locker-hide-booked-lockers-title',
                'lang' => 'fr',
                'value' => 'Hide lockers that are booked',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            47 => 
            array (
                'key' => 'choose-locker-minutes',
                'lang' => 'en',
                'value' => '%minutes%',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            48 => 
            array (
                'key' => 'choose-locker-minutes',
                'lang' => 'fr',
                'value' => '%minutes%',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            49 => 
            array (
                'key' => 'choose-locker-proceed-button',
                'lang' => 'en',
                'value' => 'Confirm Reservation and Purchase Lockers',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-08 11:11:43',
            ),
            50 => 
            array (
                'key' => 'choose-locker-proceed-button',
                'lang' => 'fr',
                'value' => 'Confirm Reservation and Purchase Lockers',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-08 11:12:22',
            ),
            51 => 
            array (
                'key' => 'choose-locker-red-key-text',
                'lang' => 'en',
                'value' => 'Not available',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-06-19 13:54:48',
            ),
            52 => 
            array (
                'key' => 'choose-locker-red-key-text',
                'lang' => 'fr',
                'value' => 'Not available',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            53 => 
            array (
                'key' => 'choose-locker-reset-button',
                'lang' => 'en',
                'value' => 'Cancel Reservation',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-08 11:03:34',
            ),
            54 => 
            array (
                'key' => 'choose-locker-reset-button',
                'lang' => 'fr',
                'value' => 'Reset',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            55 => 
            array (
                'key' => 'choose-locker-select-block-title',
                'lang' => 'en',
                'value' => 'Select Block',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            56 => 
            array (
                'key' => 'choose-locker-select-block-title',
                'lang' => 'fr',
                'value' => 'Select Block',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            57 => 
            array (
                'key' => 'choose-locker-select-child-locker-validation',
                'lang' => 'en',
                'value' => 'Please select a locker for each child. If you have added more than one child and you no longer require them all &nbsp;please go back and remove the child you do not require',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-06-25 13:58:05',
            ),
            58 => 
            array (
                'key' => 'choose-locker-select-child-locker-validation',
                'lang' => 'fr',
                'value' => 'Please select a locker for each child',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            59 => 
            array (
                'key' => 'choose-locker-show-all-lockers',
                'lang' => 'en',
                'value' => '<b>Available</b> Lockers',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-08 11:45:35',
            ),
            60 => 
            array (
                'key' => 'choose-locker-show-all-lockers',
                'lang' => 'fr',
                'value' => 'Show all lockers',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            61 => 
            array (
                'key' => 'choose-locker-timer-text',
                'lang' => 'en',
                'value' => 'Minutes before <b>Reservation Expires</b>',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-08 11:45:08',
            ),
            62 => 
            array (
                'key' => 'choose-locker-timer-text',
                'lang' => 'fr',
                'value' => 'Complete booking minutes remaining',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            63 => 
            array (
                'key' => 'choose-locker-title',
                'lang' => 'en',
                'value' => 'Choose your Locker',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-08 11:46:19',
            ),
            64 => 
            array (
                'key' => 'choose-locker-title',
                'lang' => 'fr',
                'value' => '<b>Choose my locker</b>',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            65 => 
            array (
                'key' => 'chose-location-text',
                'lang' => 'en',
                'value' => 'Choose Location',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            66 => 
            array (
                'key' => 'chose-location-text',
                'lang' => 'fr',
                'value' => 'Choose Location',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            67 => 
            array (
                'key' => 'chose-location-text',
                'lang' => 'it',
                'value' => 'chose-location-text',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            68 => 
            array (
                'key' => 'contact-menu',
                'lang' => 'en',
                'value' => 'CONTACT',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            69 => 
            array (
                'key' => 'contact-menu',
                'lang' => 'fr',
                'value' => 'CONTACT',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            70 => 
            array (
                'key' => 'contact-menu',
                'lang' => 'it',
                'value' => 'contact-menu',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            71 => 
            array (
                'key' => 'contact-policy-review',
                'lang' => 'en',
                'value' => '<div><span style="font-size: 1rem;">By pressing submit below you are consenting locker rentals to storing your data. Review our&nbsp;</span><a href="https://locker.rentals/policy" target="_blank">Policy</a><br></div>',
                'scope' => 'global',
                'created_at' => '2020-05-04 17:19:59',
                'updated_at' => '2020-05-09 13:15:07',
            ),
            72 => 
            array (
                'key' => 'contact-policy-review',
                'lang' => 'fr',
                'value' => 'contact-policy-review',
                'scope' => 'global',
                'created_at' => '2020-05-04 17:19:59',
                'updated_at' => '2020-05-04 17:19:59',
            ),
            73 => 
            array (
                'key' => 'contact-policy-review-link',
                'lang' => 'en',
                'value' => 'Policy',
                'scope' => 'global',
                'created_at' => '2020-05-04 17:19:59',
                'updated_at' => '2020-05-04 17:20:46',
            ),
            74 => 
            array (
                'key' => 'contact-policy-review-link',
                'lang' => 'fr',
                'value' => 'contact-policy-review-link',
                'scope' => 'global',
                'created_at' => '2020-05-04 17:19:59',
                'updated_at' => '2020-05-04 17:19:59',
            ),
            75 => 
            array (
                'key' => 'contact-us-footer',
                'lang' => 'en',
                'value' => 'Links',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            76 => 
            array (
                'key' => 'contact-us-footer',
                'lang' => 'fr',
                'value' => 'Links',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            77 => 
            array (
                'key' => 'contact-us-footer',
                'lang' => 'it',
                'value' => 'contact-us-footer',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            78 => 
            array (
                'key' => 'contact-us-heading',
                'lang' => 'en',
                'value' => 'Contact Us',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            79 => 
            array (
                'key' => 'contact-us-heading',
                'lang' => 'fr',
                'value' => 'Contact Us',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            80 => 
            array (
                'key' => 'contact-us-text',
                'lang' => 'en',
                'value' => 'Please complete the below form and we will contact you with 2 working days.',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-29 22:58:59',
            ),
            81 => 
            array (
                'key' => 'contact-us-text',
                'lang' => 'fr',
                'value' => 'Please complete the below form and we will contact you with 2 working days.',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            82 => 
            array (
                'key' => 'edit-booking-change-and-continue-button',
                'lang' => 'en',
                'value' => 'Change And Continue',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            83 => 
            array (
                'key' => 'edit-booking-orders-back-button',
                'lang' => 'en',
                'value' => 'Back To Order',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            84 => 
            array (
                'key' => 'facebook-link-footer',
                'lang' => 'en',
                'value' => '#',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            85 => 
            array (
                'key' => 'facebook-link-footer',
                'lang' => 'fr',
                'value' => '#',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            86 => 
            array (
                'key' => 'facebook-link-footer',
                'lang' => 'it',
                'value' => 'facebook-link-footer',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            87 => 
            array (
                'key' => 'faq-heading',
                'lang' => 'en',
                'value' => 'FAQ',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            88 => 
            array (
                'key' => 'faq-heading',
                'lang' => 'gb',
                'value' => 'FAQ',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            89 => 
            array (
                'key' => 'faq-menu',
                'lang' => 'en',
                'value' => 'FAQ',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => '2020-05-19 20:34:40',
            ),
            90 => 
            array (
                'key' => 'faq-menu',
                'lang' => 'fr',
                'value' => 'faq-menu',
                'scope' => 'global',
                'created_at' => '2020-05-18 19:49:52',
                'updated_at' => '2020-05-18 19:49:52',
            ),
            91 => 
            array (
                'key' => 'faq-menu',
                'lang' => 'gb',
                'value' => 'FAQ',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            92 => 
            array (
                'key' => 'faq-section1-heading',
                'lang' => 'en',
                'value' => 'Questions',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => '2020-05-19 22:21:15',
            ),
            93 => 
            array (
                'key' => 'faq-section1-heading',
                'lang' => 'gb',
                'value' => 'FAQ',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            94 => 
            array (
                'key' => 'faq-section1-text',
                'lang' => 'en',
                'value' => '<p><b>I have not received my forgot password email.&nbsp;</b><span style="font-size: 1rem;">Please check your spam folder as sometime our your spam filters move system generated emails in to spam folders.</span></p><p><span style="font-size: 1rem;"><b>I have booked a locker but I have not received an email to book my locker the next year.&nbsp;</b></span><span style="font-size: 1rem;">We send out all renewal emails every April-May. If you do not receive this email it may be its in your spam folder. However you can always logo to the portal and review all reserved lockers and purchase them without the need of a reminder email.</span></p><p><b>How do I stop the booking reminder emails.&nbsp;</b><span style="font-size: 1rem;">We send out all renewal emails every April-May. On the email their is a cancel link, if you select that you will no longer get another reminder.&nbsp;</span></p><p><b style="font-size: 1rem;">Can I add multiple children to a reservation?</b><span style="font-size: 1rem;">&nbsp;Yes,&nbsp;</span>just<span style="font-size: 1rem;">&nbsp;select add another child</span></p><p><span style="font-size: 1rem;"><br></span></p><div><br></div>',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => '2020-05-21 15:05:43',
            ),
            95 => 
            array (
                'key' => 'faq-section1-text',
                'lang' => 'gb',
                'value' => 'FAQFAQFAQ',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            96 => 
            array (
                'key' => 'faq-text',
                'lang' => 'en',
                'value' => '<p>Please review commonly asked questions. If your answer is not here please&nbsp;<a href="https://locker.rentals/contactUs" target="_blank">contact us</a></p>',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => '2020-05-19 22:20:35',
            ),
            97 => 
            array (
                'key' => 'faq-text',
                'lang' => 'gb',
                'value' => 'FAQFAQ',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            98 => 
            array (
                'key' => 'find-location-text',
                'lang' => 'en',
                'value' => 'FIND LOCATION',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            99 => 
            array (
                'key' => 'find-location-text',
                'lang' => 'fr',
                'value' => 'FIND LOCATION',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            100 => 
            array (
                'key' => 'find-location-text',
                'lang' => 'it',
                'value' => 'find-location-text',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            101 => 
            array (
                'key' => 'footer-address-heading',
                'lang' => 'en',
                'value' => 'Address',
                'scope' => 'global',
                'created_at' => '2020-04-27 16:44:43',
                'updated_at' => '2020-04-27 17:06:08',
            ),
            102 => 
            array (
                'key' => 'footer-address-heading',
                'lang' => 'fr',
                'value' => 'Address',
                'scope' => 'global',
                'created_at' => '2020-04-27 16:44:43',
                'updated_at' => '2020-04-27 17:06:08',
            ),
            103 => 
            array (
                'key' => 'footer-address-text',
                'lang' => 'en',
            'value' => '<div class="foo-contact " style="caret-color: rgb(119, 119, 119); color: rgb(119, 119, 119); font-family: Lato, sans-serif;">14 Stephenson Court,</div><div class="foo-contact " style="caret-color: rgb(119, 119, 119); color: rgb(119, 119, 119); font-family: Lato, sans-serif;">Priory Business Park,</div><div class="foo-contact " style="caret-color: rgb(119, 119, 119); color: rgb(119, 119, 119); font-family: Lato, sans-serif;">Bedford,</div><div class="foo-contact " style="caret-color: rgb(119, 119, 119); color: rgb(119, 119, 119); font-family: Lato, sans-serif;">MK44 3WH</div><div style="caret-color: rgb(119, 119, 119); color: rgb(119, 119, 119); font-family: Lato, sans-serif;"></div><div class="foo-contact " style="caret-color: rgb(119, 119, 119); color: rgb(119, 119, 119); font-family: Lato, sans-serif;">Reg No: 6535355 VAT No: 936 5596 78</div>',
                'scope' => 'global',
                'created_at' => '2020-04-27 16:44:43',
                'updated_at' => '2020-04-27 17:05:13',
            ),
            104 => 
            array (
                'key' => 'footer-address-text',
                'lang' => 'fr',
            'value' => '<div class="foo-contact " style="caret-color: rgb(119, 119, 119); color: rgb(119, 119, 119); font-family: Lato, sans-serif;">14 Stephenson Court,</div><div class="foo-contact " style="caret-color: rgb(119, 119, 119); color: rgb(119, 119, 119); font-family: Lato, sans-serif;">Priory Business Park,</div><div class="foo-contact " style="caret-color: rgb(119, 119, 119); color: rgb(119, 119, 119); font-family: Lato, sans-serif;">Bedford,</div><div class="foo-contact " style="caret-color: rgb(119, 119, 119); color: rgb(119, 119, 119); font-family: Lato, sans-serif;">MK44 3WH</div><div style="caret-color: rgb(119, 119, 119); color: rgb(119, 119, 119); font-family: Lato, sans-serif;"></div><div class="foo-contact " style="caret-color: rgb(119, 119, 119); color: rgb(119, 119, 119); font-family: Lato, sans-serif;">Reg No: 6535355 VAT No: 936 5596 78</div>',
                'scope' => 'global',
                'created_at' => '2020-04-27 16:44:43',
                'updated_at' => '2020-04-27 17:05:13',
            ),
            105 => 
            array (
                'key' => 'footer-addresstext',
                'lang' => 'en',
            'value' => '<div style="color: rgb(238, 238, 238); background-color: rgb(17, 17, 17);">Prefect Equipment LTD</div><div style="color: rgb(238, 238, 238); background-color: rgb(17, 17, 17);">14 Stephenson Court,</div><div style="color: rgb(238, 238, 238); background-color: rgb(17, 17, 17);">Priory Business Park,</div><div style="color: rgb(238, 238, 238); background-color: rgb(17, 17, 17);">Bedford,</div><div style="color: rgb(238, 238, 238); background-color: rgb(17, 17, 17);">MK44 3WH</div><div style="color: rgb(238, 238, 238); background-color: rgb(17, 17, 17);">Reg No: 6535355 VAT No: 936 5596 78</div>',
                'scope' => 'global',
                'created_at' => '2020-04-27 17:21:10',
                'updated_at' => '2020-04-27 17:22:11',
            ),
            106 => 
            array (
                'key' => 'footer-addresstext',
                'lang' => 'fr',
            'value' => '<div style="color: rgb(238, 238, 238); background-color: rgb(17, 17, 17);">Prefect Equipment LTD</div><div style="color: rgb(238, 238, 238); background-color: rgb(17, 17, 17);">14 Stephenson Court,</div><div style="color: rgb(238, 238, 238); background-color: rgb(17, 17, 17);">Priory Business Park,</div><div style="color: rgb(238, 238, 238); background-color: rgb(17, 17, 17);">Bedford,</div><div style="color: rgb(238, 238, 238); background-color: rgb(17, 17, 17);">MK44 3WH</div><div style="color: rgb(238, 238, 238); background-color: rgb(17, 17, 17);">Reg No: 6535355 VAT No: 936 5596 78</div>',
                'scope' => 'global',
                'created_at' => '2020-04-27 17:21:10',
                'updated_at' => '2020-04-27 17:22:11',
            ),
            107 => 
            array (
                'key' => 'footer-web-portal-heading',
                'lang' => 'en',
                'value' => 'Portal',
                'scope' => 'global',
                'created_at' => '2020-04-27 16:44:43',
                'updated_at' => '2020-04-27 17:06:16',
            ),
            108 => 
            array (
                'key' => 'footer-web-portal-heading',
                'lang' => 'fr',
                'value' => 'Portal',
                'scope' => 'global',
                'created_at' => '2020-04-27 16:44:43',
                'updated_at' => '2020-04-27 17:06:16',
            ),
            109 => 
            array (
                'key' => 'footer-web-portal-link',
                'lang' => 'en',
                'value' => 'footer-web-portal-link',
                'scope' => 'global',
                'created_at' => '2020-04-27 16:44:43',
                'updated_at' => '2020-04-27 16:44:43',
            ),
            110 => 
            array (
                'key' => 'footer-web-portal-link',
                'lang' => 'fr',
                'value' => 'footer-web-portal-link',
                'scope' => 'global',
                'created_at' => '2020-04-27 16:44:43',
                'updated_at' => '2020-04-27 16:44:43',
            ),
            111 => 
            array (
                'key' => 'get-started-button',
                'lang' => 'en',
                'value' => 'slider-1-buuton2',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            112 => 
            array (
                'key' => 'get-started-button',
                'lang' => 'fr',
                'value' => 'slider-1-buuton2',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            113 => 
            array (
                'key' => 'get-started-button-link',
                'lang' => 'en',
                'value' => '/frontend/requestavailability',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            114 => 
            array (
                'key' => 'get-started-button-link',
                'lang' => 'fr',
                'value' => '/frontend/requestavailability',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            115 => 
            array (
                'key' => 'get-started-button-link',
                'lang' => 'it',
                'value' => 'get-started-button-link',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            116 => 
            array (
                'key' => 'home-booking-detail',
                'lang' => 'en',
                'value' => 'Booking details',
                'scope' => 'global',
                'created_at' => '2020-05-04 17:12:27',
                'updated_at' => '2020-05-04 17:19:32',
            ),
            117 => 
            array (
                'key' => 'home-booking-detail',
                'lang' => 'fr',
                'value' => 'home-booking-detail',
                'scope' => 'global',
                'created_at' => '2020-05-04 17:12:27',
                'updated_at' => '2020-05-04 17:12:27',
            ),
            118 => 
            array (
                'key' => 'home-booking-id-card',
                'lang' => 'en',
                'value' => 'Your Reserved Bookings',
                'scope' => 'global',
                'created_at' => '2020-04-16 13:37:49',
                'updated_at' => '2020-04-20 15:58:29',
            ),
            119 => 
            array (
                'key' => 'home-booking-id-card',
                'lang' => 'fr',
                'value' => 'Your Reserved Bookings',
                'scope' => 'global',
                'created_at' => '2020-04-16 13:37:49',
                'updated_at' => '2020-04-16 13:37:49',
            ),
            120 => 
            array (
                'key' => 'home-booking-info-child-name',
                'lang' => 'en',
                'value' => 'Childs Name:',
                'scope' => 'global',
                'created_at' => '2020-04-16 13:37:49',
                'updated_at' => '2020-04-20 16:00:50',
            ),
            121 => 
            array (
                'key' => 'home-booking-info-child-name',
                'lang' => 'fr',
                'value' => 'Childs Name:',
                'scope' => 'global',
                'created_at' => '2020-04-16 13:37:49',
                'updated_at' => '2020-04-16 13:37:49',
            ),
            122 => 
            array (
                'key' => 'home-booking-info-locker-block',
                'lang' => 'en',
                'value' => 'Locker Block:&nbsp;',
                'scope' => 'global',
                'created_at' => '2020-04-16 13:37:49',
                'updated_at' => '2020-04-20 16:01:16',
            ),
            123 => 
            array (
                'key' => 'home-booking-info-locker-block',
                'lang' => 'fr',
                'value' => 'Locker Block:&nbsp;',
                'scope' => 'global',
                'created_at' => '2020-04-16 13:37:49',
                'updated_at' => '2020-04-16 13:37:49',
            ),
            124 => 
            array (
                'key' => 'home-booking-info-locker-number',
                'lang' => 'en',
                'value' => 'Locker Number:&nbsp;',
                'scope' => 'global',
                'created_at' => '2020-04-16 13:37:49',
                'updated_at' => '2020-04-20 16:00:39',
            ),
            125 => 
            array (
                'key' => 'home-booking-info-locker-number',
                'lang' => 'fr',
                'value' => 'Locker Number:&nbsp;',
                'scope' => 'global',
                'created_at' => '2020-04-16 13:37:49',
                'updated_at' => '2020-04-16 13:37:49',
            ),
            126 => 
            array (
                'key' => 'home-company-info-card',
                'lang' => 'en',
                'value' => 'Company Names',
                'scope' => 'global',
                'created_at' => '2020-04-16 13:37:49',
                'updated_at' => '2020-04-20 15:59:22',
            ),
            127 => 
            array (
                'key' => 'home-company-info-card',
                'lang' => 'fr',
                'value' => 'Company Names',
                'scope' => 'global',
                'created_at' => '2020-04-16 13:37:49',
                'updated_at' => '2020-04-16 13:37:49',
            ),
            128 => 
            array (
                'key' => 'home-email-confirmation-msg',
                'lang' => 'en',
                'value' => 'We have send a password reset to your Email. If you have not received the email within 15 minutes, you may have added an invalid email address. Please check your spam email folder if you do not receive an email.',
                'scope' => 'global',
                'created_at' => '2020-09-09 23:01:14',
                'updated_at' => '2020-09-09 23:01:14',
            ),
            129 => 
            array (
                'key' => 'home-end-date-card',
                'lang' => 'en',
                'value' => 'End Date',
                'scope' => 'global',
                'created_at' => '2020-04-16 13:37:49',
                'updated_at' => '2020-04-20 16:00:26',
            ),
            130 => 
            array (
                'key' => 'home-end-date-card',
                'lang' => 'fr',
                'value' => 'End Date',
                'scope' => 'global',
                'created_at' => '2020-04-16 13:37:49',
                'updated_at' => '2020-04-16 13:37:49',
            ),
            131 => 
            array (
                'key' => 'home-logged-email-text',
                'lang' => 'en',
                'value' => 'We will notify %email%&nbsp;if the decision from when we have had a response',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            132 => 
            array (
                'key' => 'home-logged-email-text',
                'lang' => 'fr',
                'value' => 'We will notify %email%&nbsp;if the decision from when we have had a response',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            133 => 
            array (
                'key' => 'home-login-reset-text',
                'lang' => 'en',
                'value' => 'To find your locker code please %login%. If you cannot remember your password please %reset% your password',
                'scope' => 'global',
                'created_at' => '2020-09-09 22:48:50',
                'updated_at' => '2020-09-09 22:48:50',
            ),
            134 => 
            array (
                'key' => 'home-menu',
                'lang' => 'en',
                'value' => '<p>Home</p>',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-21 14:47:29',
            ),
            135 => 
            array (
                'key' => 'home-menu',
                'lang' => 'fr',
                'value' => '<p>Home</p>',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            136 => 
            array (
                'key' => 'home-menu',
                'lang' => 'it',
                'value' => 'home-menu',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            137 => 
            array (
                'key' => 'home-non-availability-text',
                'lang' => 'en',
                'value' => 'Would you like to request %school_name%&nbsp;to be contacted to install locker',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            138 => 
            array (
                'key' => 'home-non-availability-text',
                'lang' => 'fr',
                'value' => 'Would you like to request %school_name%&nbsp;to be contacted to install locker',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            139 => 
            array (
                'key' => 'home-offer',
                'lang' => 'en',
                'value' => 'We Offer Lockers',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            140 => 
            array (
                'key' => 'home-offer',
                'lang' => 'fr',
                'value' => 'We Offer Lockers',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            141 => 
            array (
                'key' => 'home-ofline-login-text',
                'lang' => 'en',
                'value' => 'If you have been informed that your child has been moved to a different locker due to Covid-19 and you have not received details of the change, please %login% into your account to view your amended booking.
<br> <br> 
If you have not received the 4-digit code needed to access your locker, please %login% to your account to retrieve this.',
                'scope' => 'global',
                'created_at' => '2020-05-06 14:06:30',
                'updated_at' => '2020-05-13 12:49:02',
            ),
            142 => 
            array (
                'key' => 'home-ofline-login-text',
                'lang' => 'fr',
                'value' => 'home-ofline-login-text',
                'scope' => 'global',
                'created_at' => '2020-05-06 14:06:30',
                'updated_at' => '2020-05-06 14:06:30',
            ),
            143 => 
            array (
                'key' => 'home-policy-review',
                'lang' => 'en',
                'value' => '<div><span style="font-size: 1rem;">By pressing proceed below you are consenting locker rentals to storing your data. Review our&nbsp;</span><a href="https://locker.rentals/policy" target="_blank">Policy</a><br></div>',
                'scope' => 'global',
                'created_at' => '2020-05-04 16:16:04',
                'updated_at' => '2020-06-19 13:53:06',
            ),
            144 => 
            array (
                'key' => 'home-policy-review',
                'lang' => 'fr',
                'value' => 'home-policy-review',
                'scope' => 'global',
                'created_at' => '2020-05-18 14:45:38',
                'updated_at' => '2020-05-18 14:45:38',
            ),
            145 => 
            array (
                'key' => 'home-policy-review-link',
                'lang' => 'en',
                'value' => 'Policy',
                'scope' => 'global',
                'created_at' => '2020-05-04 16:16:04',
                'updated_at' => '2020-05-04 17:19:14',
            ),
            146 => 
            array (
                'key' => 'home-purchase-button-card',
                'lang' => 'en',
                'value' => 'Complete your Reserved Booking',
                'scope' => 'global',
                'created_at' => '2020-04-16 13:37:49',
                'updated_at' => '2020-04-20 15:59:56',
            ),
            147 => 
            array (
                'key' => 'home-purchase-button-card',
                'lang' => 'fr',
                'value' => 'Complete your Reserved Booking',
                'scope' => 'global',
                'created_at' => '2020-04-16 13:37:49',
                'updated_at' => '2020-04-16 13:37:49',
            ),
            148 => 
            array (
                'key' => 'home-renew-locker-login',
                'lang' => 'en',
                'value' => 'If you have been informed that your child has been moved to a different locker due to Covid-19 and you have not received details of the change, please %login% into your account to view your amended booking.
<br> <br> 
If you have not received the 4-digit code needed to access your locker, please %login% to your account to retrieve this.',
                'scope' => 'global',
                'created_at' => '2020-04-15 19:57:21',
                'updated_at' => '2020-05-05 13:33:39',
            ),
            149 => 
            array (
                'key' => 'home-renew-locker-login',
                'lang' => 'fr',
                'value' => 'If you have been informed that your child has been moved to a different locker due to Covid-19 and you have not received details of the change, please %login% into your account to view your amended booking.
<br> <br> 
If you have not received the 4-digit code needed to access your locker, please %login% to your account to retrieve this.',
                'scope' => 'global',
                'created_at' => '2020-04-15 19:57:21',
                'updated_at' => '2020-04-15 19:57:21',
            ),
            150 => 
            array (
                'key' => 'home-request-confirm-password-placeholder',
                'lang' => 'en',
                'value' => 'Confirm Password',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            151 => 
            array (
                'key' => 'home-request-confirm-password-placeholder',
                'lang' => 'fr',
                'value' => 'Confirm Password',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            152 => 
            array (
                'key' => 'home-request-confirm-password-validation',
                'lang' => 'en',
                'value' => 'Password does not matched',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            153 => 
            array (
                'key' => 'home-request-confirm-password-validation',
                'lang' => 'fr',
                'value' => 'Password does not matched',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            154 => 
            array (
                'key' => 'home-request-country-placeholder',
                'lang' => 'en',
                'value' => 'Country',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            155 => 
            array (
                'key' => 'home-request-country-placeholder',
                'lang' => 'fr',
                'value' => 'Country',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            156 => 
            array (
                'key' => 'home-request-country-validation',
                'lang' => 'en',
                'value' => 'kindly enter country',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            157 => 
            array (
                'key' => 'home-request-country-validation',
                'lang' => 'fr',
                'value' => 'kindly enter country',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            158 => 
            array (
                'key' => 'home-request-create-account-tab-text',
                'lang' => 'en',
                'value' => 'Create Account',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            159 => 
            array (
                'key' => 'home-request-create-account-tab-text',
                'lang' => 'fr',
                'value' => 'Create Account',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            160 => 
            array (
                'key' => 'home-request-email-placeholder',
                'lang' => 'en',
                'value' => 'Email',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            161 => 
            array (
                'key' => 'home-request-email-placeholder',
                'lang' => 'fr',
                'value' => 'Email',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            162 => 
            array (
                'key' => 'home-request-email-validation',
                'lang' => 'en',
                'value' => 'kindly enter valid Email \'abc@xyz.com\'',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            163 => 
            array (
                'key' => 'home-request-email-validation',
                'lang' => 'fr',
                'value' => 'kindly enter valid Email \'abc@xyz.com\'',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            164 => 
            array (
                'key' => 'home-request-forget-password-button',
                'lang' => 'en',
                'value' => 'Send Email',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            165 => 
            array (
                'key' => 'home-request-forget-password-button',
                'lang' => 'fr',
                'value' => 'Send Email',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            166 => 
            array (
                'key' => 'home-request-forget-password-email-exist-validation',
                'lang' => 'en',
                'value' => 'This Email address does not exist',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            167 => 
            array (
                'key' => 'home-request-forget-password-email-exist-validation',
                'lang' => 'fr',
                'value' => 'This Email address does not exist',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            168 => 
            array (
                'key' => 'home-request-forget-password-email-placeholder',
                'lang' => 'en',
                'value' => 'Email',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            169 => 
            array (
                'key' => 'home-request-forget-password-email-placeholder',
                'lang' => 'fr',
                'value' => 'Email',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            170 => 
            array (
                'key' => 'home-request-forget-password-email-validation',
                'lang' => 'en',
                'value' => 'Enter email',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            171 => 
            array (
                'key' => 'home-request-forget-password-email-validation',
                'lang' => 'fr',
                'value' => 'Enter email',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            172 => 
            array (
                'key' => 'home-request-forget-password-send-email-validation',
                'lang' => 'en',
                'value' => 'we have  emailed you Reset password link check your email',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            173 => 
            array (
                'key' => 'home-request-forget-password-send-email-validation',
                'lang' => 'fr',
                'value' => 'we have  emailed you Reset password link check your email',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            174 => 
            array (
                'key' => 'home-request-forget-password-sub-title-msg',
                'lang' => 'en',
                'value' => 'Please add your registered email address and you will be sent a link to change your password',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            175 => 
            array (
                'key' => 'home-request-forget-password-sub-title-msg',
                'lang' => 'fr',
                'value' => 'Please add your registered email address and you will be sent a link to change your password',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            176 => 
            array (
                'key' => 'home-request-forget-password-title',
                'lang' => 'en',
                'value' => 'Forget Password',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            177 => 
            array (
                'key' => 'home-request-forget-password-title',
                'lang' => 'fr',
                'value' => 'Forget Password',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            178 => 
            array (
                'key' => 'home-request-line1address-placeholder',
                'lang' => 'en',
                'value' => 'line 1 Address',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            179 => 
            array (
                'key' => 'home-request-line1address-placeholder',
                'lang' => 'fr',
                'value' => 'line 1 Address',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            180 => 
            array (
                'key' => 'home-request-line1address-validation',
                'lang' => 'en',
                'value' => 'kindly enter Line 1 Address',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            181 => 
            array (
                'key' => 'home-request-line1address-validation',
                'lang' => 'fr',
                'value' => 'kindly enter Line 1 Address',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            182 => 
            array (
                'key' => 'home-request-line2-address-placeholder',
                'lang' => 'en',
                'value' => 'line 2 Address',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            183 => 
            array (
                'key' => 'home-request-line2-address-placeholder',
                'lang' => 'fr',
                'value' => 'line 2 Address',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            184 => 
            array (
                'key' => 'home-request-line2address-validation',
                'lang' => 'en',
                'value' => 'kindly enter Line 2 Address',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            185 => 
            array (
                'key' => 'home-request-line2address-validation',
                'lang' => 'fr',
                'value' => 'kindly enter Line 2 Address',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            186 => 
            array (
                'key' => 'home-request-name-validation',
                'lang' => 'en',
                'value' => 'kindly enter name',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            187 => 
            array (
                'key' => 'home-request-name-validation',
                'lang' => 'fr',
                'value' => 'kindly enter name',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            188 => 
            array (
                'key' => 'home-request-new-school-button',
                'lang' => 'en',
                'value' => 'Submit',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            189 => 
            array (
                'key' => 'home-request-new-school-button',
                'lang' => 'fr',
                'value' => 'Submit',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            190 => 
            array (
                'key' => 'home-request-new-school-text',
                'lang' => 'en',
                'value' => 'Would you like to request %school_name% to be contacted to install lockers',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            191 => 
            array (
                'key' => 'home-request-new-school-text',
                'lang' => 'fr',
                'value' => 'Would you like to request %school_name% to be contacted to install lockers',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            192 => 
            array (
                'key' => 'home-request-password-placeholder',
                'lang' => 'en',
                'value' => 'Password',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            193 => 
            array (
                'key' => 'home-request-password-placeholder',
                'lang' => 'fr',
                'value' => 'Password',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            194 => 
            array (
                'key' => 'home-request-password-validation',
                'lang' => 'en',
                'value' => 'Password must be 6 characters required',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            195 => 
            array (
                'key' => 'home-request-password-validation',
                'lang' => 'fr',
                'value' => 'Password must be 6 characters required',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            196 => 
            array (
                'key' => 'home-request-post-code-placeholder',
                'lang' => 'en',
                'value' => 'Post Code',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            197 => 
            array (
                'key' => 'home-request-post-code-placeholder',
                'lang' => 'fr',
                'value' => 'Post Code',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            198 => 
            array (
                'key' => 'home-request-post-code-validation',
                'lang' => 'en',
                'value' => 'kindly enter post code',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            199 => 
            array (
                'key' => 'home-request-post-code-validation',
                'lang' => 'fr',
                'value' => 'kindly enter post code',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            200 => 
            array (
                'key' => 'home-request-school-name-validation',
                'lang' => 'en',
                'value' => 'kindly enter school name',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            201 => 
            array (
                'key' => 'home-request-school-name-validation',
                'lang' => 'fr',
                'value' => 'kindly enter school name',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            202 => 
            array (
                'key' => 'home-request-schoolname-placeholder',
                'lang' => 'en',
                'value' => 'School Name',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            203 => 
            array (
                'key' => 'home-request-schoolname-placeholder',
                'lang' => 'fr',
                'value' => 'School Name',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            204 => 
            array (
                'key' => 'home-request-sign-in-button',
                'lang' => 'en',
                'value' => 'Sign In',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            205 => 
            array (
                'key' => 'home-request-sign-in-button',
                'lang' => 'fr',
                'value' => 'Sign In',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            206 => 
            array (
                'key' => 'home-request-sign-in-email-placeholder',
                'lang' => 'en',
                'value' => 'Your Email',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            207 => 
            array (
                'key' => 'home-request-sign-in-email-placeholder',
                'lang' => 'fr',
                'value' => 'Your Email',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            208 => 
            array (
                'key' => 'home-request-sign-in-email-validation',
                'lang' => 'en',
                'value' => 'enter valid email \'abc@xyz.com\'',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            209 => 
            array (
                'key' => 'home-request-sign-in-email-validation',
                'lang' => 'fr',
                'value' => 'enter valid email \'abc@xyz.com\'',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            210 => 
            array (
                'key' => 'home-request-sign-in-incorrect-validation',
                'lang' => 'en',
                'value' => 'your credentials does not matched to our system',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            211 => 
            array (
                'key' => 'home-request-sign-in-incorrect-validation',
                'lang' => 'fr',
                'value' => 'your credentials does not matched to our system',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            212 => 
            array (
                'key' => 'home-request-sign-in-password-placeholder',
                'lang' => 'en',
                'value' => 'password',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            213 => 
            array (
                'key' => 'home-request-sign-in-password-placeholder',
                'lang' => 'fr',
                'value' => 'password',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            214 => 
            array (
                'key' => 'home-request-sign-in-password-validation',
                'lang' => 'en',
                'value' => 'enter valid password',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            215 => 
            array (
                'key' => 'home-request-sign-in-password-validation',
                'lang' => 'fr',
                'value' => 'enter valid password',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            216 => 
            array (
                'key' => 'home-request-sign-in-tab-text',
                'lang' => 'en',
                'value' => 'Login',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            217 => 
            array (
                'key' => 'home-request-sign-in-tab-text',
                'lang' => 'fr',
                'value' => 'Login',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            218 => 
            array (
                'key' => 'home-request-sign-in-title',
                'lang' => 'en',
                'value' => 'Sign In',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            219 => 
            array (
                'key' => 'home-request-sign-in-title',
                'lang' => 'fr',
                'value' => 'Sign In',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            220 => 
            array (
                'key' => 'home-request-sign-in-validation',
                'lang' => 'en',
                'value' => 'please sign In before submition',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            221 => 
            array (
                'key' => 'home-request-sign-in-validation',
                'lang' => 'fr',
                'value' => 'please sign In before submition',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            222 => 
            array (
                'key' => 'home-request-sign-up-button',
                'lang' => 'en',
                'value' => 'Sign Up',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            223 => 
            array (
                'key' => 'home-request-sign-up-button',
                'lang' => 'fr',
                'value' => 'Sign Up',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            224 => 
            array (
                'key' => 'home-request-sign-up-confirm-password-placeholder',
                'lang' => 'en',
                'value' => 'confirm password',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            225 => 
            array (
                'key' => 'home-request-sign-up-confirm-password-placeholder',
                'lang' => 'fr',
                'value' => 'confirm password',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            226 => 
            array (
                'key' => 'home-request-sign-up-confirm-password-validation',
                'lang' => 'en',
                'value' => 'Password does not  matched!',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            227 => 
            array (
                'key' => 'home-request-sign-up-confirm-password-validation',
                'lang' => 'fr',
                'value' => 'Password does not  matched!',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            228 => 
            array (
                'key' => 'home-request-sign-up-email-placeholder',
                'lang' => 'en',
                'value' => 'Email',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            229 => 
            array (
                'key' => 'home-request-sign-up-email-placeholder',
                'lang' => 'fr',
                'value' => 'Email',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            230 => 
            array (
                'key' => 'home-request-sign-up-email-validation',
                'lang' => 'en',
                'value' => 'enter valid email \'abcd@xyz.com\'',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            231 => 
            array (
                'key' => 'home-request-sign-up-email-validation',
                'lang' => 'fr',
                'value' => 'enter valid email \'abcd@xyz.com\'',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            232 => 
            array (
                'key' => 'home-request-sign-up-first-name-placeholder',
                'lang' => 'en',
                'value' => 'first name',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            233 => 
            array (
                'key' => 'home-request-sign-up-first-name-placeholder',
                'lang' => 'fr',
                'value' => 'first name',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            234 => 
            array (
                'key' => 'home-request-sign-up-first-name-validation',
                'lang' => 'en',
                'value' => 'enter first name',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            235 => 
            array (
                'key' => 'home-request-sign-up-first-name-validation',
                'lang' => 'fr',
                'value' => 'enter first name',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            236 => 
            array (
                'key' => 'home-request-sign-up-password-placeholder',
                'lang' => 'en',
                'value' => 'password',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            237 => 
            array (
                'key' => 'home-request-sign-up-password-placeholder',
                'lang' => 'fr',
                'value' => 'password',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            238 => 
            array (
                'key' => 'home-request-sign-up-password-validation',
                'lang' => 'en',
                'value' => 'password must be 6-20 in length, containing an uppercase and numeric value',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            239 => 
            array (
                'key' => 'home-request-sign-up-password-validation',
                'lang' => 'fr',
                'value' => 'password must be 6-20 in length, containing an uppercase and numeric value',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            240 => 
            array (
                'key' => 'home-request-sign-up-phone-number-placeholder',
                'lang' => 'en',
                'value' => 'phone number',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            241 => 
            array (
                'key' => 'home-request-sign-up-phone-number-placeholder',
                'lang' => 'fr',
                'value' => 'phone number',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            242 => 
            array (
                'key' => 'home-request-sign-up-phone-number-validation',
                'lang' => 'en',
                'value' => 'Enter correct Phone Number',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            243 => 
            array (
                'key' => 'home-request-sign-up-phone-number-validation',
                'lang' => 'fr',
                'value' => 'enter phone number',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            244 => 
            array (
                'key' => 'home-request-sign-up-sub-title-msg',
                'lang' => 'en',
                'value' => 'Please create an account that can be used to manage all your locker bookings',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            245 => 
            array (
                'key' => 'home-request-sign-up-sub-title-msg',
                'lang' => 'fr',
                'value' => 'Please create an account that can be used to manage all your locker bookings',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            246 => 
            array (
                'key' => 'home-request-sign-up-surname-placeholder',
                'lang' => 'en',
                'value' => 'Surname',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            247 => 
            array (
                'key' => 'home-request-sign-up-surname-placeholder',
                'lang' => 'fr',
                'value' => 'Surname',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            248 => 
            array (
                'key' => 'home-request-sign-up-surname-validation',
                'lang' => 'en',
                'value' => 'Enter surname',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            249 => 
            array (
                'key' => 'home-request-sign-up-surname-validation',
                'lang' => 'fr',
                'value' => 'Enter surname',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            250 => 
            array (
                'key' => 'home-request-sign-up-title',
                'lang' => 'en',
                'value' => 'Sign Up',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            251 => 
            array (
                'key' => 'home-request-sign-up-title',
                'lang' => 'fr',
                'value' => 'Sign Up',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            252 => 
            array (
                'key' => 'home-request-sign-up-user-exist-msg',
                'lang' => 'en',
                'value' => 'This email has already been registered
please sign in',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            253 => 
            array (
                'key' => 'home-request-sign-up-user-exist-msg',
                'lang' => 'fr',
                'value' => 'This email has already been registered
please sign in',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            254 => 
            array (
                'key' => 'home-request-town-placeholder',
                'lang' => 'en',
                'value' => 'Town',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            255 => 
            array (
                'key' => 'home-request-town-placeholder',
                'lang' => 'fr',
                'value' => 'Town',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            256 => 
            array (
                'key' => 'home-request-town-validation',
                'lang' => 'en',
                'value' => 'kindly enter town',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            257 => 
            array (
                'key' => 'home-request-town-validation',
                'lang' => 'fr',
                'value' => 'kindly enter town',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            258 => 
            array (
                'key' => 'home-request-user-name-placeholder',
                'lang' => 'en',
                'value' => 'Name',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            259 => 
            array (
                'key' => 'home-request-user-name-placeholder',
                'lang' => 'fr',
                'value' => 'Name',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            260 => 
            array (
                'key' => 'home-reset-email-instruction-msg',
                'lang' => 'en',
                'value' => 'Please enter your Mobile Phone Number or Email and we will re-send you your instructions to enter the portal and view your bookings',
                'scope' => 'global',
                'created_at' => '2020-09-09 23:00:54',
                'updated_at' => '2020-09-09 23:00:54',
            ),
            261 => 
            array (
                'key' => 'home-reset-password-header',
                'lang' => 'en',
                'value' => 'Reset Password',
                'scope' => 'global',
                'created_at' => '2020-09-09 22:48:27',
                'updated_at' => '2020-09-09 22:48:27',
            ),
            262 => 
            array (
                'key' => 'home-sms-confirmation-msg',
                'lang' => 'en',
                'value' => 'We have send a code to your Mobile Number. If you have not received the code within 15 minutes. Please try again.',
                'scope' => 'global',
                'created_at' => '2020-09-09 23:02:56',
                'updated_at' => '2020-09-09 23:03:12',
            ),
            263 => 
            array (
                'key' => 'home-start-date-card',
                'lang' => 'en',
                'value' => 'Start Date:&nbsp;',
                'scope' => 'global',
                'created_at' => '2020-04-16 13:37:49',
                'updated_at' => '2020-04-20 16:00:18',
            ),
            264 => 
            array (
                'key' => 'home-start-date-card',
                'lang' => 'fr',
                'value' => 'Start Date:&nbsp;',
                'scope' => 'global',
                'created_at' => '2020-04-16 13:37:49',
                'updated_at' => '2020-04-16 13:37:49',
            ),
            265 => 
            array (
                'key' => 'location-input-place-holder',
                'lang' => 'en',
                'value' => 'Enter School Name, Town or City',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            266 => 
            array (
                'key' => 'location-input-place-holder',
                'lang' => 'fr',
                'value' => 'Enter School Name, Town or City',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            267 => 
            array (
                'key' => 'location-input-place-holder',
                'lang' => 'it',
                'value' => 'location-input-place-holder',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            268 => 
            array (
                'key' => 'nava page',
                'lang' => 'en',
                'value' => 'nava page',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            269 => 
            array (
                'key' => 'nava page',
                'lang' => 'fr',
                'value' => 'nava page',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            270 => 
            array (
                'key' => 'office-timing-header',
                'lang' => 'en',
                'value' => '&nbsp; &nbsp;Login and Review your Bookings',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            271 => 
            array (
                'key' => 'office-timing-header',
                'lang' => 'fr',
                'value' => '&nbsp; &nbsp;Login and Review your Bookings',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            272 => 
            array (
                'key' => 'office-timing-header',
                'lang' => 'it',
                'value' => 'office-timing-header',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            273 => 
            array (
                'key' => 'ofline-login-form-text',
                'lang' => 'en',
                'value' => 'ofline-login-form-text',
                'scope' => 'global',
                'created_at' => '2020-05-06 14:06:51',
                'updated_at' => '2020-05-06 14:06:51',
            ),
            274 => 
            array (
                'key' => 'ofline-login-form-text',
                'lang' => 'fr',
                'value' => 'ofline-login-form-text',
                'scope' => 'global',
                'created_at' => '2020-05-06 14:06:51',
                'updated_at' => '2020-05-06 14:06:51',
            ),
            275 => 
            array (
                'key' => 'password-reset-text',
                'lang' => 'en',
                'value' => 'We have send a password reset to your Email. If you have not received the email within 15 minutes, you may have added an invalid email address. Please check your spam email folder if you do not receive an email.',
                'scope' => 'global',
                'created_at' => '2020-05-13 11:22:15',
                'updated_at' => '2020-05-13 12:31:08',
            ),
            276 => 
            array (
                'key' => 'password-reset-text',
                'lang' => 'fr',
                'value' => 'password-reset-text',
                'scope' => 'global',
                'created_at' => '2020-05-13 11:22:15',
                'updated_at' => '2020-05-13 11:22:15',
            ),
            277 => 
            array (
                'key' => 'payment-recieve-confirmation-sms-checkbox-text',
                'lang' => 'en',
                'value' => 'I would like to receive a confirmation via SMS',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            278 => 
            array (
                'key' => 'payment-recieve-confirmation-sms-checkbox-text',
                'lang' => 'fr',
                'value' => 'I would like to receive a confirmation via SMS',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            279 => 
            array (
                'key' => 'payments-add-address-manually-button',
                'lang' => 'en',
                'value' => 'Add Address Manually',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            280 => 
            array (
                'key' => 'payments-add-address-manually-button',
                'lang' => 'fr',
                'value' => 'Add Address Manually',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            281 => 
            array (
                'key' => 'payments-billingaddress1-label',
                'lang' => 'en',
                'value' => 'Billing Address 1',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            282 => 
            array (
                'key' => 'payments-billingAddress1-label',
                'lang' => 'fr',
                'value' => 'Billing Address 1',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            283 => 
            array (
                'key' => 'payments-billingaddress1-placeholder',
                'lang' => 'en',
                'value' => 'Billing Address 1',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            284 => 
            array (
                'key' => 'payments-billingaddress1-placeholder',
                'lang' => 'fr',
                'value' => 'Billing Address 1',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            285 => 
            array (
                'key' => 'payments-billingaddress1-validation',
                'lang' => 'en',
                'value' => 'Wrong billingAddress1',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            286 => 
            array (
                'key' => 'payments-billingaddress1-validation',
                'lang' => 'fr',
                'value' => 'Wrong billingAddress1',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            287 => 
            array (
                'key' => 'payments-billingaddress2-label',
                'lang' => 'en',
                'value' => 'Billing Address 2',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            288 => 
            array (
                'key' => 'payments-billingaddress2-label',
                'lang' => 'fr',
                'value' => 'Billing Address 2',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            289 => 
            array (
                'key' => 'payments-billingaddress2-placeholder',
                'lang' => 'en',
                'value' => 'Billing Address 2',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            290 => 
            array (
                'key' => 'payments-billingaddress2-placeholder',
                'lang' => 'fr',
                'value' => 'Billing Address 2',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            291 => 
            array (
                'key' => 'payments-billingaddress2-validation',
                'lang' => 'en',
                'value' => 'wrong billingAddress1',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            292 => 
            array (
                'key' => 'payments-billingaddress2-validation',
                'lang' => 'fr',
                'value' => 'wrong billingAddress1',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            293 => 
            array (
                'key' => 'payments-booking-info-address',
                'lang' => 'en',
                'value' => 'Address :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            294 => 
            array (
                'key' => 'payments-booking-info-address',
                'lang' => 'fr',
                'value' => 'Address :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            295 => 
            array (
                'key' => 'payments-booking-info-booked-title',
                'lang' => 'en',
                'value' => 'Booking Details',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            296 => 
            array (
                'key' => 'payments-booking-info-booked-title',
                'lang' => 'fr',
                'value' => 'Booking Details',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            297 => 
            array (
                'key' => 'payments-booking-info-child-email',
                'lang' => 'en',
                'value' => 'Child Email :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            298 => 
            array (
                'key' => 'payments-booking-info-child-email',
                'lang' => 'fr',
                'value' => 'Child Email :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            299 => 
            array (
                'key' => 'payments-booking-info-child-name',
                'lang' => 'en',
                'value' => 'Child Name :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            300 => 
            array (
                'key' => 'payments-booking-info-child-name',
                'lang' => 'fr',
                'value' => 'Child Name :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            301 => 
            array (
                'key' => 'payments-booking-info-company-title',
                'lang' => 'en',
                'value' => 'Company Info',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            302 => 
            array (
                'key' => 'payments-booking-info-company-title',
                'lang' => 'fr',
                'value' => 'Company Info',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            303 => 
            array (
                'key' => 'payments-booking-info-end-rent',
                'lang' => 'en',
                'value' => 'End Rent :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            304 => 
            array (
                'key' => 'payments-booking-info-end-rent',
                'lang' => 'fr',
                'value' => 'End Rent :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            305 => 
            array (
                'key' => 'payments-booking-info-locker-block',
                'lang' => 'en',
                'value' => 'Locker Block :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            306 => 
            array (
                'key' => 'payments-booking-info-locker-block',
                'lang' => 'fr',
                'value' => 'Locker Block :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            307 => 
            array (
                'key' => 'payments-booking-info-locker-code',
                'lang' => 'en',
                'value' => 'Code :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            308 => 
            array (
                'key' => 'payments-booking-info-locker-code',
                'lang' => 'fr',
                'value' => 'Code :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            309 => 
            array (
                'key' => 'payments-booking-info-locker-number',
                'lang' => 'en',
                'value' => 'Locker Number :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            310 => 
            array (
                'key' => 'payments-booking-info-locker-number',
                'lang' => 'fr',
                'value' => 'Locker Number :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            311 => 
            array (
                'key' => 'payments-booking-info-locker-price',
                'lang' => 'en',
                'value' => 'Price :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            312 => 
            array (
                'key' => 'payments-booking-info-locker-price',
                'lang' => 'fr',
                'value' => 'Price :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            313 => 
            array (
                'key' => 'payments-booking-info-name',
                'lang' => 'en',
                'value' => 'Name :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            314 => 
            array (
                'key' => 'payments-booking-info-name',
                'lang' => 'fr',
                'value' => 'Name :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            315 => 
            array (
                'key' => 'payments-booking-info-start-rent',
                'lang' => 'en',
                'value' => 'Start Rent :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            316 => 
            array (
                'key' => 'payments-booking-info-start-rent',
                'lang' => 'fr',
                'value' => 'Start Rent :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            317 => 
            array (
                'key' => 'payments-booking-info-terms-msg',
                'lang' => 'en',
                'value' => 'Agreed terms and conditions :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            318 => 
            array (
                'key' => 'payments-booking-info-terms-msg',
                'lang' => 'fr',
                'value' => 'Agreed terms and conditions :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            319 => 
            array (
                'key' => 'payments-booking-info-user-main-title',
                'lang' => 'en',
                'value' => 'Personal Info',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            320 => 
            array (
                'key' => 'payments-booking-info-user-main-title',
                'lang' => 'fr',
                'value' => 'Personal Info',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            321 => 
            array (
                'key' => 'payments-card-cvv-label',
                'lang' => 'en',
                'value' => 'CVV',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            322 => 
            array (
                'key' => 'payments-card-cvv-label',
                'lang' => 'fr',
                'value' => 'CVV',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            323 => 
            array (
                'key' => 'payments-card-cvv-placeholder',
                'lang' => 'en',
                'value' => 'cvv',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            324 => 
            array (
                'key' => 'payments-card-cvv-placeholder',
                'lang' => 'fr',
                'value' => 'cvv',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            325 => 
            array (
                'key' => 'payments-card-cvv-validation',
                'lang' => 'en',
                'value' => 'Enter valid CVV',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-11 17:33:32',
            ),
            326 => 
            array (
                'key' => 'payments-card-cvv-validation',
                'lang' => 'fr',
                'value' => 'Wrong CVV',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            327 => 
            array (
                'key' => 'payments-card-expiration-date-label',
                'lang' => 'en',
                'value' => 'Expiration Date',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            328 => 
            array (
                'key' => 'payments-card-expiration-date-label',
                'lang' => 'fr',
                'value' => 'Expiration Date',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            329 => 
            array (
                'key' => 'payments-card-expiry-date-validation',
                'lang' => 'en',
                'value' => 'Wrong card expiry date',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            330 => 
            array (
                'key' => 'payments-card-expiry-date-validation',
                'lang' => 'fr',
                'value' => 'Wrong card expiry date',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            331 => 
            array (
                'key' => 'payments-card-inner-title',
                'lang' => 'en',
                'value' => 'Address',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            332 => 
            array (
                'key' => 'payments-card-inner-title',
                'lang' => 'fr',
                'value' => 'Address',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            333 => 
            array (
                'key' => 'payments-card-main-title',
                'lang' => 'en',
                'value' => 'Billing Information',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-06-19 13:55:23',
            ),
            334 => 
            array (
                'key' => 'payments-card-main-title',
                'lang' => 'fr',
                'value' => 'Confirm Purchase',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            335 => 
            array (
                'key' => 'payments-card-number-label',
                'lang' => 'en',
                'value' => 'Card Number',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            336 => 
            array (
                'key' => 'payments-card-number-label',
                'lang' => 'fr',
                'value' => 'Card Number',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            337 => 
            array (
                'key' => 'payments-card-number-placeholder',
                'lang' => 'en',
                'value' => 'Card Number',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-11 22:32:09',
            ),
            338 => 
            array (
                'key' => 'payments-card-number-placeholder',
                'lang' => 'fr',
                'value' => 'card number',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            339 => 
            array (
                'key' => 'payments-card-number-validation',
                'lang' => 'en',
                'value' => 'Enter valid card number',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-11 17:33:52',
            ),
            340 => 
            array (
                'key' => 'payments-card-number-validation',
                'lang' => 'fr',
                'value' => 'Wrong Card Number',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            341 => 
            array (
                'key' => 'payments-card-owner-label',
                'lang' => 'en',
                'value' => 'Name on your Card',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-11 17:32:43',
            ),
            342 => 
            array (
                'key' => 'payments-card-owner-label',
                'lang' => 'fr',
                'value' => 'Owner',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            343 => 
            array (
                'key' => 'payments-card-owner-placeholder',
                'lang' => 'en',
                'value' => 'Name on card',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-11 22:32:00',
            ),
            344 => 
            array (
                'key' => 'payments-card-owner-placeholder',
                'lang' => 'fr',
                'value' => 'owner',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            345 => 
            array (
                'key' => 'payments-card-owner-validation',
                'lang' => 'en',
                'value' => 'Enter a valid name on card',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-11 17:33:38',
            ),
            346 => 
            array (
                'key' => 'payments-card-owner-validation',
                'lang' => 'fr',
                'value' => 'Wrong owner',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            347 => 
            array (
                'key' => 'payments-charity-card-main-diclaimer',
                'lang' => 'fr',
            'value' => 'I am paying for renting %numberoflockers% locker(s) for %amount_lockers% with %charity__payment% charity payment',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            348 => 
            array (
                'key' => 'payments-charity-card-main-disclaimer',
                'lang' => 'en',
            'value' => 'I am paying for renting %numberoflockers% locker(s) for %amount_lockers% with %charity__payment% charity payment',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            349 => 
            array (
                'key' => 'payments-charity-card-main-disclaimer',
                'lang' => 'fr',
            'value' => 'I am paying for renting %numberoflockers% locker(s) for %amount_lockers% with %charity__payment% charity payment',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            350 => 
            array (
                'key' => 'payments-charity-card-main-title',
                'lang' => 'en',
                'value' => 'Charity',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            351 => 
            array (
                'key' => 'payments-charity-card-main-title',
                'lang' => 'fr',
                'value' => 'Charity',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            352 => 
            array (
                'key' => 'payments-city-label',
                'lang' => 'en',
                'value' => 'City',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            353 => 
            array (
                'key' => 'payments-city-label',
                'lang' => 'fr',
                'value' => 'City',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            354 => 
            array (
                'key' => 'payments-city-placeholder',
                'lang' => 'en',
                'value' => 'city',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            355 => 
            array (
                'key' => 'payments-city-placeholder',
                'lang' => 'fr',
                'value' => 'city',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            356 => 
            array (
                'key' => 'payments-city-validation',
                'lang' => 'en',
                'value' => 'Wrong city',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            357 => 
            array (
                'key' => 'payments-city-validation',
                'lang' => 'fr',
                'value' => 'Wrong city',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            358 => 
            array (
                'key' => 'payments-confirm-button',
                'lang' => 'en',
                'value' => 'Pay and Complete Reservation',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-08 11:16:06',
            ),
            359 => 
            array (
                'key' => 'payments-confirm-button',
                'lang' => 'fr',
                'value' => 'Confirm',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            360 => 
            array (
                'key' => 'payments-confirmation-book-another-locker',
                'lang' => 'en',
                'value' => 'Book Another Locker',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            361 => 
            array (
                'key' => 'payments-confirmation-book-another-locker',
                'lang' => 'fr',
                'value' => 'Book Another Locker',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            362 => 
            array (
                'key' => 'payments-confirmation-card-failed-title',
                'lang' => 'en',
                'value' => 'Failed',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            363 => 
            array (
                'key' => 'payments-confirmation-card-failed-title',
                'lang' => 'fr',
                'value' => 'Failed',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            364 => 
            array (
                'key' => 'payments-confirmation-card-main-title',
                'lang' => 'en',
                'value' => 'Booking Confirmation',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-12 18:46:53',
            ),
            365 => 
            array (
                'key' => 'payments-confirmation-card-main-title',
                'lang' => 'fr',
                'value' => 'Payment Confirmation',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            366 => 
            array (
                'key' => 'payments-confirmation-card-success-msg',
                'lang' => 'en',
                'value' => 'Thank you for booking locker in&nbsp;<b>%school_name%</b>&nbsp;your booking reference number is
<b>%reservation_number%. </b>All your booking confirmation details are on this page however for future reference please login to the portal to review your reservation.',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-12 18:51:45',
            ),
            367 => 
            array (
                'key' => 'payments-confirmation-card-success-msg',
                'lang' => 'fr',
                'value' => 'Thank you for booking locker in&nbsp;<b>%school_name%</b> your confirmation
email is being sent to <b>%admin_email%</b> and your booking reference number is
<b>%reservation_number%</b>',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            368 => 
            array (
                'key' => 'payments-confirmation-card-success-title',
                'lang' => 'en',
                'value' => 'Success',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            369 => 
            array (
                'key' => 'payments-confirmation-card-success-title',
                'lang' => 'fr',
                'value' => 'Success',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            370 => 
            array (
                'key' => 'payments-confirmation-check-your-order-button',
                'lang' => 'en',
                'value' => 'View your Booking in the Portal',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-12 18:53:27',
            ),
            371 => 
            array (
                'key' => 'payments-confirmation-check-your-order-button',
                'lang' => 'fr',
                'value' => 'Check Your Order',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            372 => 
            array (
                'key' => 'payments-confirmation-main-title',
                'lang' => 'en',
                'value' => 'Payments Confirmation',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            373 => 
            array (
                'key' => 'payments-confirmation-main-title',
                'lang' => 'fr',
                'value' => 'Payments Confirmation',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            374 => 
            array (
                'key' => 'payments-country-label',
                'lang' => 'en',
                'value' => 'Country',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            375 => 
            array (
                'key' => 'payments-country-label',
                'lang' => 'fr',
                'value' => 'Country',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            376 => 
            array (
                'key' => 'payments-country-placeholder',
                'lang' => 'en',
                'value' => 'country',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            377 => 
            array (
                'key' => 'payments-country-placeholder',
                'lang' => 'fr',
                'value' => 'country',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            378 => 
            array (
                'key' => 'payments-country-validation',
                'lang' => 'en',
                'value' => 'Select Country',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            379 => 
            array (
                'key' => 'payments-country-validation',
                'lang' => 'fr',
                'value' => 'Select Country',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            380 => 
            array (
                'key' => 'payments-credit-card-main-title',
                'lang' => 'en',
                'value' => 'Credit Card',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            381 => 
            array (
                'key' => 'payments-credit-card-main-title',
                'lang' => 'fr',
                'value' => 'Credit Card',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            382 => 
            array (
                'key' => 'payments-edit-add-another-transaction',
                'lang' => 'en',
            'value' => 'Total Booking&nbsp;<span style="color: rgb(102, 102, 102);">%currency%</span><span style="color: rgb(102, 102, 102);">%total_booking_price% ,New Price&nbsp;</span><span style="color: rgb(102, 102, 102);">%currency%</span><span style="color: rgb(102, 102, 102);">%new_price%</span>',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => '2020-05-29 23:19:28',
            ),
            383 => 
            array (
                'key' => 'payments-edit-no-price-change-mode',
                'lang' => 'en',
                'value' => 'No price Changes Made',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            384 => 
            array (
                'key' => 'payments-main-title',
                'lang' => 'en',
                'value' => 'Payments',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            385 => 
            array (
                'key' => 'payments-main-title',
                'lang' => 'fr',
                'value' => 'Payments',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            386 => 
            array (
                'key' => 'payments-payername1-label',
                'lang' => 'en',
                'value' => 'First Name',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-12 16:49:51',
            ),
            387 => 
            array (
                'key' => 'payments-payername1-label',
                'lang' => 'fr',
                'value' => 'Payer Name&nbsp;',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            388 => 
            array (
                'key' => 'payments-payername1-placeholder',
                'lang' => 'en',
                'value' => 'First Name',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-12 16:50:09',
            ),
            389 => 
            array (
                'key' => 'payments-payername1-placeholder',
                'lang' => 'fr',
                'value' => 'Payer Name',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            390 => 
            array (
                'key' => 'payments-payername1-validation',
                'lang' => 'en',
                'value' => 'This field is required',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            391 => 
            array (
                'key' => 'payments-payername1-validation',
                'lang' => 'fr',
                'value' => 'This field is required',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            392 => 
            array (
                'key' => 'payments-payment-type-label',
                'lang' => 'en',
                'value' => 'Payment Type',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            393 => 
            array (
                'key' => 'payments-payment-type-label',
                'lang' => 'fr',
                'value' => 'Payment Type',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            394 => 
            array (
                'key' => 'payments-phon-number-label',
                'lang' => 'en',
                'value' => 'Phone Number',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            395 => 
            array (
                'key' => 'payments-phon-number-label',
                'lang' => 'fr',
                'value' => 'Phone Number',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            396 => 
            array (
                'key' => 'payments-phon-number-placeholder',
                'lang' => 'en',
                'value' => 'phone number',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            397 => 
            array (
                'key' => 'payments-phon-number-placeholder',
                'lang' => 'fr',
                'value' => 'phone number',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            398 => 
            array (
                'key' => 'payments-phon-number-validation',
                'lang' => 'en',
                'value' => 'Wrong Phone Number',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            399 => 
            array (
                'key' => 'payments-phon-number-validation',
                'lang' => 'fr',
                'value' => 'Wrong Phone Number',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            400 => 
            array (
                'key' => 'payments-postcode-label',
                'lang' => 'en',
                'value' => 'Post Code',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            401 => 
            array (
                'key' => 'payments-postcode-label',
                'lang' => 'fr',
                'value' => 'Post Code',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            402 => 
            array (
                'key' => 'payments-postcode-lookup-placeholder',
                'lang' => 'en',
                'value' => 'Postcode lookup',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            403 => 
            array (
                'key' => 'payments-postcode-lookup-placeholder',
                'lang' => 'fr',
                'value' => 'Postcode lookup',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            404 => 
            array (
                'key' => 'payments-postcode-lookup-validation',
                'lang' => 'en',
                'value' => 'Please enter a valid postcode',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            405 => 
            array (
                'key' => 'payments-postcode-lookup-validation',
                'lang' => 'fr',
                'value' => 'Please enter a valid postcode',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            406 => 
            array (
                'key' => 'payments-postcode-placeholder',
                'lang' => 'en',
                'value' => 'post Code',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            407 => 
            array (
                'key' => 'payments-postcode-placeholder',
                'lang' => 'fr',
                'value' => 'post Code',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            408 => 
            array (
                'key' => 'payments-postcode-validation',
                'lang' => 'en',
                'value' => 'Wrong post Code',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            409 => 
            array (
                'key' => 'payments-postcode-validation',
                'lang' => 'fr',
                'value' => 'Wrong post Code',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            410 => 
            array (
                'key' => 'payments-refresh-page-button',
                'lang' => 'en',
                'value' => 'Refresh Page',
                'scope' => 'global',
                'created_at' => '2020-05-22 17:10:20',
                'updated_at' => '2020-05-22 17:10:20',
            ),
            411 => 
            array (
                'key' => 'payments-refresh-page-button',
                'lang' => 'fr',
                'value' => 'payments-refresh-page-button',
                'scope' => 'global',
                'created_at' => '2020-05-22 17:10:20',
                'updated_at' => '2020-05-22 17:10:20',
            ),
            412 => 
            array (
                'key' => 'payments-search-for-my-address-button',
                'lang' => 'en',
                'value' => 'Search For My Address',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            413 => 
            array (
                'key' => 'payments-search-for-my-address-button',
                'lang' => 'fr',
                'value' => 'Search For My Address',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            414 => 
            array (
                'key' => 'payments-state-label',
                'lang' => 'en',
                'value' => 'County',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-01 16:04:26',
            ),
            415 => 
            array (
                'key' => 'payments-state-label',
                'lang' => 'fr',
                'value' => 'State',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            416 => 
            array (
                'key' => 'payments-state-placeholder',
                'lang' => 'en',
                'value' => 'County',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-06-04 19:04:51',
            ),
            417 => 
            array (
                'key' => 'payments-state-placeholder',
                'lang' => 'fr',
                'value' => 'state',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            418 => 
            array (
                'key' => 'payments-state-validation',
                'lang' => 'en',
                'value' => 'Wrong State',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            419 => 
            array (
                'key' => 'payments-state-validation',
                'lang' => 'fr',
                'value' => 'Wrong State',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            420 => 
            array (
                'key' => 'payments-surname-label',
                'lang' => 'en',
                'value' => 'Surname',
                'scope' => 'global',
                'created_at' => '2020-05-12 16:45:04',
                'updated_at' => '2020-05-12 16:49:57',
            ),
            421 => 
            array (
                'key' => 'payments-surname-label',
                'lang' => 'fr',
                'value' => 'payments-surname-label',
                'scope' => 'global',
                'created_at' => '2020-05-12 16:45:04',
                'updated_at' => '2020-05-12 16:45:04',
            ),
            422 => 
            array (
                'key' => 'payments-surname-placeholder',
                'lang' => 'en',
                'value' => 'Surname',
                'scope' => 'global',
                'created_at' => '2020-05-12 16:45:04',
                'updated_at' => '2020-05-12 16:50:17',
            ),
            423 => 
            array (
                'key' => 'payments-surname-placeholder',
                'lang' => 'fr',
                'value' => 'payments-surname-placeholder',
                'scope' => 'global',
                'created_at' => '2020-05-12 16:45:04',
                'updated_at' => '2020-05-12 16:45:04',
            ),
            424 => 
            array (
                'key' => 'payments-surname-validation',
                'lang' => 'en',
                'value' => 'payments-surname-validation',
                'scope' => 'global',
                'created_at' => '2020-05-12 16:45:04',
                'updated_at' => '2020-05-12 16:45:04',
            ),
            425 => 
            array (
                'key' => 'payments-surname-validation',
                'lang' => 'fr',
                'value' => 'payments-surname-validation',
                'scope' => 'global',
                'created_at' => '2020-05-12 16:45:04',
                'updated_at' => '2020-05-12 16:45:04',
            ),
            426 => 
            array (
                'key' => 'payments-use-my-address1-text',
                'lang' => 'en',
                'value' => '<b>Address</b>',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            427 => 
            array (
                'key' => 'payments-use-my-address1-text',
                'lang' => 'fr',
                'value' => '<b>Address</b>',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            428 => 
            array (
                'key' => 'payments-use-my-address2-text',
                'lang' => 'en',
                'value' => 'Billing Address 2 :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            429 => 
            array (
                'key' => 'payments-use-my-address2-text',
                'lang' => 'fr',
                'value' => 'Billing Address 2 :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            430 => 
            array (
                'key' => 'payments-use-my-city-text',
                'lang' => 'en',
                'value' => 'City :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            431 => 
            array (
                'key' => 'payments-use-my-city-text',
                'lang' => 'fr',
                'value' => 'City :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            432 => 
            array (
                'key' => 'payments-use-my-post-code-text',
                'lang' => 'en',
                'value' => 'Post Code :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            433 => 
            array (
                'key' => 'payments-use-my-post-code-text',
                'lang' => 'fr',
                'value' => 'Post Code :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            434 => 
            array (
                'key' => 'payments-use-my-state-text',
                'lang' => 'en',
                'value' => 'State :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            435 => 
            array (
                'key' => 'payments-use-my-state-text',
                'lang' => 'fr',
                'value' => 'State :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            436 => 
            array (
                'key' => 'payments-user-info-charity-cost',
                'lang' => 'en',
                'value' => 'Charity Cost:',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            437 => 
            array (
                'key' => 'payments-user-info-charity-cost',
                'lang' => 'fr',
                'value' => 'Charity Cost:',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            438 => 
            array (
                'key' => 'payments-user-info-cost',
                'lang' => 'en',
                'value' => 'Total Cost :&nbsp;',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            439 => 
            array (
                'key' => 'payments-user-info-cost',
                'lang' => 'fr',
                'value' => 'Total Cost :&nbsp;',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            440 => 
            array (
                'key' => 'payments-user-info-email',
                'lang' => 'en',
                'value' => 'Email :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            441 => 
            array (
                'key' => 'payments-user-info-email',
                'lang' => 'fr',
                'value' => 'Email :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            442 => 
            array (
                'key' => 'payments-user-info-lockers-cost',
                'lang' => 'en',
                'value' => 'Locker Cost:&nbsp;',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            443 => 
            array (
                'key' => 'payments-user-info-lockers-cost',
                'lang' => 'fr',
                'value' => 'Locker Cost:&nbsp;',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            444 => 
            array (
                'key' => 'payments-user-info-name',
                'lang' => 'en',
                'value' => 'Name :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            445 => 
            array (
                'key' => 'payments-user-info-name',
                'lang' => 'fr',
                'value' => 'Name :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            446 => 
            array (
                'key' => 'payments-user-info-reservation-id',
                'lang' => 'en',
                'value' => 'Reservation ID :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            447 => 
            array (
                'key' => 'payments-user-info-reservation-id',
                'lang' => 'fr',
                'value' => 'Reservation ID :',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            448 => 
            array (
                'key' => 'phone-number-header',
                'lang' => 'en',
                'value' => 'Book your Locker &nbsp; &nbsp; &nbsp; -',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            449 => 
            array (
                'key' => 'phone-number-header',
                'lang' => 'fr',
                'value' => 'Book your Locker &nbsp; &nbsp; &nbsp; -',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            450 => 
            array (
                'key' => 'phone-number-header',
                'lang' => 'it',
                'value' => 'phone-number-header',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            451 => 
            array (
                'key' => 'policy-heading',
                'lang' => 'en',
                'value' => 'Web Policy',
                'scope' => 'global',
                'created_at' => '2020-04-27 18:50:14',
                'updated_at' => '2020-04-29 22:28:16',
            ),
            452 => 
            array (
                'key' => 'policy-heading',
                'lang' => 'fr',
                'value' => 'Web Policy',
                'scope' => 'global',
                'created_at' => '2020-04-27 18:50:14',
                'updated_at' => '2020-04-27 18:50:14',
            ),
            453 => 
            array (
                'key' => 'policy-section1-heading',
                'lang' => 'en',
                'value' => 'Policy',
                'scope' => 'global',
                'created_at' => '2020-04-27 18:50:14',
                'updated_at' => '2020-04-29 22:28:03',
            ),
            454 => 
            array (
                'key' => 'policy-section1-heading',
                'lang' => 'fr',
                'value' => 'Policy',
                'scope' => 'global',
                'created_at' => '2020-04-27 18:50:14',
                'updated_at' => '2020-04-27 18:50:14',
            ),
            455 => 
            array (
                'key' => 'policy-section1-text',
                'lang' => 'en',
            'value' => '<div>We use cookies and save your data into our database to improve your booking experience</div><div><br></div><div>Recognising you when you sign-in to use our services. This allows us:-</div><div><br></div><ul><li>To conducting research and diagnostics to improve content, products, and services.</li><li>Preventing fraudulent activity.</li><li>Improving security.</li><li>Reporting. This allows us to measure and analyse the performance of our services.&nbsp;</li><li>Improve the user experience&nbsp;</li><li>Lockers Rentals cookies allow you to take advantage of some of our essential features. For instance, if you do not accept our policy you cannot log in or make bookings for lockers.</li></ul><div>You can manage browser cookies through your browser settings. The \'Help\' feature on most browsers will tell you how to prevent your browser from accepting new cookies, how to have the browser notify you when you receive a new cookie, how to disable cookies, and when cookies will expire. If you disable all cookies on your browser, neither we nor third parties will transfer cookies to your browser. If you do this, however, you may have to manually adjust some preferences every time you visit a site and some features and services may not work.</div><div><br></div><div>Please note regardless of how you manage your cookies we will be storing your data in our database to facilitate the booking process and account management,</div><div><br></div><div>We know that you care how information about you is used and shared and we appreciate your trust in us to do that carefully and sensibly. This Privacy Notice describes how we collect and process your personal information through this website.</div><div><br></div><div><b>For What Purposes do we Process Your Personal Information?</b></div><div><b><br></b></div><ul><li>Purchase and delivery of products and services. We use your personal information to take and handle orders, deliver products and services, process payments, and communicate with you about orders, products and services, and promotional offers.</li><li>Provide, troubleshoot, and improve our Services. We use your personal information to provide functionality, analyse performance, fix errors, and improve usability and effectiveness of the &nbsp;Services.</li><li>Recommendations and personalisation. We use your personal information to recommend features, products, and services that might be of interest to you, identify your preferences, and personalise your experience with our Services.</li><li>Comply with legal obligations. In certain cases, we have a legal obligation to collect and process your personal information. For instance, we collect from sellers information regarding place of establishment and bank account information for identity verification and other purposes.</li><li>Communicate with you. We use your personal information to communicate with you in relation to Services via different channels (e.g., by phone, email, chat).</li><li>Advertising. We use your personal information to display interest-based ads for features, products, and services that might be of interest to you. To learn more, please read our Interest-Based Ads notice.</li><li>Fraud Prevention and Credit Risks. We process personal information to prevent and detect fraud and abuse in order to protect the security of our customers. We may also use scoring methods to assess and manage credit risks.</li><li>Purposes for which we seek your consent. We may also ask for your consent to process your personal information for a specific purpose that we communicate to you. When you consent to our processing your personal information for a specified purpose, you may withdraw your consent at any time and we will stop processing of your data for that purpose.</li><li><br></li></ul><div><b>Do we Share Your Personal Information?</b></div><div>Information about our customers is an important part of our business and we are not in the business of selling our customers\' personal information to others. We share customers\' information only to process orders and t<span style="font-size: 1rem;">ransactions as we do not hold your financial information on our servers we user sage pay to process all bookings. In this process, we send sage pay your payment information securely and only for the purpose of making a secure financial transaction.&nbsp;</span></div><div><br></div>',
                'scope' => 'global',
                'created_at' => '2020-04-27 18:50:14',
                'updated_at' => '2020-04-29 22:41:49',
            ),
            456 => 
            array (
                'key' => 'policy-section1-text',
                'lang' => 'fr',
            'value' => '<div>We use cookies and save your data into our database to improve your booking experience</div><div><br></div><div>Recognising you when you sign-in to use our services. This allows us:-</div><div><br></div><ul><li>To conducting research and diagnostics to improve content, products, and services.</li><li>Preventing fraudulent activity.</li><li>Improving security.</li><li>Reporting. This allows us to measure and analyse the performance of our services.&nbsp;</li><li>Improve the user experience&nbsp;</li><li>Lockers Rentals cookies allow you to take advantage of some of our essential features. For instance, if you do not accept our policy you cannot log in or make bookings for lockers.</li></ul><div>You can manage browser cookies through your browser settings. The \'Help\' feature on most browsers will tell you how to prevent your browser from accepting new cookies, how to have the browser notify you when you receive a new cookie, how to disable cookies, and when cookies will expire. If you disable all cookies on your browser, neither we nor third parties will transfer cookies to your browser. If you do this, however, you may have to manually adjust some preferences every time you visit a site and some features and services may not work.</div><div><br></div><div>Please note regardless of how you manage your cookies we will be storing your data in our database to facilitate the booking process and account management,</div><div><br></div><div>We know that you care how information about you is used and shared and we appreciate your trust in us to do that carefully and sensibly. This Privacy Notice describes how we collect and process your personal information through this website.</div><div><br></div><div><b>For What Purposes do we Process Your Personal Information?</b></div><div><b><br></b></div><ul><li>Purchase and delivery of products and services. We use your personal information to take and handle orders, deliver products and services, process payments, and communicate with you about orders, products and services, and promotional offers.</li><li>Provide, troubleshoot, and improve our Services. We use your personal information to provide functionality, analyse performance, fix errors, and improve usability and effectiveness of the &nbsp;Services.</li><li>Recommendations and personalisation. We use your personal information to recommend features, products, and services that might be of interest to you, identify your preferences, and personalise your experience with our Services.</li><li>Comply with legal obligations. In certain cases, we have a legal obligation to collect and process your personal information. For instance, we collect from sellers information regarding place of establishment and bank account information for identity verification and other purposes.</li><li>Communicate with you. We use your personal information to communicate with you in relation to Services via different channels (e.g., by phone, email, chat).</li><li>Advertising. We use your personal information to display interest-based ads for features, products, and services that might be of interest to you. To learn more, please read our Interest-Based Ads notice.</li><li>Fraud Prevention and Credit Risks. We process personal information to prevent and detect fraud and abuse in order to protect the security of our customers. We may also use scoring methods to assess and manage credit risks.</li><li>Purposes for which we seek your consent. We may also ask for your consent to process your personal information for a specific purpose that we communicate to you. When you consent to our processing your personal information for a specified purpose, you may withdraw your consent at any time and we will stop processing of your data for that purpose.</li><li><br></li></ul><div><b>Do we Share Your Personal Information?</b></div><div>Information about our customers is an important part of our business and we are not in the business of selling our customers\' personal information to others. We share customers\' information only to process orders and t<span style="font-size: 1rem;">ransactions as we do not hold your financial information on our servers we user sage pay to process all bookings. In this process, we send sage pay your payment information securely and only for the purpose of making a secure financial transaction.&nbsp;</span></div><div><br></div>',
                'scope' => 'global',
                'created_at' => '2020-04-27 18:50:14',
                'updated_at' => '2020-04-27 18:50:14',
            ),
            457 => 
            array (
                'key' => 'policy-subheading-text',
                'lang' => 'en',
                'value' => 'Locker Rentals policy for locker bookings',
                'scope' => 'global',
                'created_at' => '2020-05-13 12:06:17',
                'updated_at' => '2020-05-13 21:51:41',
            ),
            458 => 
            array (
                'key' => 'policy-subheading-text',
                'lang' => 'fr',
                'value' => 'policy-subheading-text',
                'scope' => 'global',
                'created_at' => '2020-05-13 12:06:17',
                'updated_at' => '2020-05-13 12:06:17',
            ),
            459 => 
            array (
                'key' => 'policy-text',
                'lang' => 'en',
                'value' => 'policy-text',
                'scope' => 'global',
                'created_at' => '2020-04-27 18:50:14',
                'updated_at' => '2020-04-27 18:50:14',
            ),
            460 => 
            array (
                'key' => 'policy-text',
                'lang' => 'fr',
                'value' => 'policy-text',
                'scope' => 'global',
                'created_at' => '2020-04-27 18:50:14',
                'updated_at' => '2020-04-27 18:50:14',
            ),
            461 => 
            array (
                'key' => 'proceed-button',
                'lang' => 'en',
                'value' => 'Start Booking',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-08 11:14:01',
            ),
            462 => 
            array (
                'key' => 'proceed-button',
                'lang' => 'fr',
                'value' => 'PROCEED',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            463 => 
            array (
                'key' => 'proceed-button',
                'lang' => 'it',
                'value' => 'proceed-button',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            464 => 
            array (
                'key' => 'reg-vat-num-footer',
                'lang' => 'en',
            'value' => '<div class="foo-contact " style="caret-color: rgb(119, 119, 119); color: rgb(119, 119, 119); font-family: Lato, sans-serif;"><span style="font-size: 1rem;">Prefect Equipment LTD</span><br></div><div class="foo-contact " style="caret-color: rgb(119, 119, 119); color: rgb(119, 119, 119); font-family: Lato, sans-serif;">14 Stephenson Court,</div><div class="foo-contact " style="caret-color: rgb(119, 119, 119); color: rgb(119, 119, 119); font-family: Lato, sans-serif;">Priory Business Park,</div><div class="foo-contact " style="caret-color: rgb(119, 119, 119); color: rgb(119, 119, 119); font-family: Lato, sans-serif;">Bedford,</div><div class="foo-contact " style="caret-color: rgb(119, 119, 119); color: rgb(119, 119, 119); font-family: Lato, sans-serif;">MK44 3WH</div><div style="caret-color: rgb(119, 119, 119); color: rgb(119, 119, 119); font-family: Lato, sans-serif;"></div><div class="foo-contact " style="caret-color: rgb(119, 119, 119); color: rgb(119, 119, 119); font-family: Lato, sans-serif;">Reg No: 6535355 VAT No: 936 5596 78</div>',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            465 => 
            array (
                'key' => 'reg-vat-num-footer',
                'lang' => 'fr',
            'value' => '<div class="foo-contact " style="caret-color: rgb(119, 119, 119); color: rgb(119, 119, 119); font-family: Lato, sans-serif;"><span style="font-size: 1rem;">Prefect Equipment LTD</span><br></div><div class="foo-contact " style="caret-color: rgb(119, 119, 119); color: rgb(119, 119, 119); font-family: Lato, sans-serif;">14 Stephenson Court,</div><div class="foo-contact " style="caret-color: rgb(119, 119, 119); color: rgb(119, 119, 119); font-family: Lato, sans-serif;">Priory Business Park,</div><div class="foo-contact " style="caret-color: rgb(119, 119, 119); color: rgb(119, 119, 119); font-family: Lato, sans-serif;">Bedford,</div><div class="foo-contact " style="caret-color: rgb(119, 119, 119); color: rgb(119, 119, 119); font-family: Lato, sans-serif;">MK44 3WH</div><div style="caret-color: rgb(119, 119, 119); color: rgb(119, 119, 119); font-family: Lato, sans-serif;"></div><div class="foo-contact " style="caret-color: rgb(119, 119, 119); color: rgb(119, 119, 119); font-family: Lato, sans-serif;">Reg No: 6535355 VAT No: 936 5596 78</div>',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            466 => 
            array (
                'key' => 'reg-vat-num-footer',
                'lang' => 'it',
                'value' => 'reg-vat-num-footer',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            467 => 
            array (
                'key' => 'request-form-button',
                'lang' => 'en',
                'value' => 'Send Message',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            468 => 
            array (
                'key' => 'request-form-button',
                'lang' => 'fr',
                'value' => 'Send Message',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            469 => 
            array (
                'key' => 'request-form-label',
                'lang' => 'en',
                'value' => 'REQUEST FORM</span>',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            470 => 
            array (
                'key' => 'request-form-label',
                'lang' => 'fr',
                'value' => 'REQUEST FORM</span>',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            471 => 
            array (
                'key' => 'request-form-menu',
                'lang' => 'en',
                'value' => 'Request Form',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            472 => 
            array (
                'key' => 'request-form-menu',
                'lang' => 'fr',
                'value' => 'Request Form',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            473 => 
            array (
                'key' => 'request-form-menu',
                'lang' => 'it',
                'value' => 'request-form-menu',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            474 => 
            array (
                'key' => 'request-locker-button',
                'lang' => 'en',
                'value' => 'School Enquiry',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            475 => 
            array (
                'key' => 'request-locker-button',
                'lang' => 'fr',
                'value' => 'School Enquiry',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            476 => 
            array (
                'key' => 'request-locker-button',
                'lang' => 'it',
                'value' => 'request-locker-button',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            477 => 
            array (
                'key' => 'requestform-policy-review',
                'lang' => 'en',
                'value' => 'requestform-policy-review',
                'scope' => 'global',
                'created_at' => '2020-05-04 17:26:45',
                'updated_at' => '2020-05-04 17:26:45',
            ),
            478 => 
            array (
                'key' => 'requestform-policy-review',
                'lang' => 'fr',
                'value' => 'requestform-policy-review',
                'scope' => 'global',
                'created_at' => '2020-05-04 17:26:45',
                'updated_at' => '2020-05-04 17:26:45',
            ),
            479 => 
            array (
                'key' => 'requestform-policy-review-link',
                'lang' => 'en',
                'value' => 'requestform-policy-review-link',
                'scope' => 'global',
                'created_at' => '2020-05-04 17:26:45',
                'updated_at' => '2020-05-04 17:26:45',
            ),
            480 => 
            array (
                'key' => 'requestform-policy-review-link',
                'lang' => 'fr',
                'value' => 'requestform-policy-review-link',
                'scope' => 'global',
                'created_at' => '2020-05-04 17:26:45',
                'updated_at' => '2020-05-04 17:26:45',
            ),
            481 => 
            array (
                'key' => 'resend-code-button',
                'lang' => 'en',
                'value' => 'Resend Code',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            482 => 
            array (
                'key' => 'resend-code-button',
                'lang' => 'fr',
                'value' => 'Resend Code',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            483 => 
            array (
                'key' => 'resend-code-button',
                'lang' => 'it',
                'value' => 'resend-code-button',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            484 => 
            array (
                'key' => 'resend-confirmation-email-button',
                'lang' => 'en',
                'value' => 'Resend Confirmation',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            485 => 
            array (
                'key' => 'resend-confirmation-email-button',
                'lang' => 'fr',
                'value' => 'Resend Confirmation',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            486 => 
            array (
                'key' => 'reset-email-button',
                'lang' => 'en',
                'value' => 'Reset Password',
                'scope' => 'global',
                'created_at' => '2020-09-09 22:48:05',
                'updated_at' => '2020-09-09 22:48:05',
            ),
            487 => 
            array (
                'key' => 'school-term-add-new-child-button',
                'lang' => 'en',
                'value' => 'Add Another Pupil',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-19 18:21:39',
            ),
            488 => 
            array (
                'key' => 'school-term-add-new-child-button',
                'lang' => 'fr',
                'value' => 'Add another Child',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            489 => 
            array (
                'key' => 'school-term-agree-terms-check-alert',
                'lang' => 'en',
                'value' => 'Please read &amp; accept our terms and conditions before proceeding.',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 14:38:25',
            ),
            490 => 
            array (
                'key' => 'school-term-agree-terms-check-alert',
                'lang' => 'fr',
                'value' => 'Please read &amp; accept our terms and conditions before proceeding.',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            491 => 
            array (
                'key' => 'school-term-agree-text',
                'lang' => 'en',
                'value' => 'I agree',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            492 => 
            array (
                'key' => 'school-term-agree-text',
                'lang' => 'fr',
                'value' => 'I agree',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            493 => 
            array (
                'key' => 'school-term-check-availability-button',
                'lang' => 'en',
                'value' => 'Choose Lockers',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-08 11:07:37',
            ),
            494 => 
            array (
                'key' => 'school-term-check-availability-button',
                'lang' => 'fr',
                'value' => 'Choose Lockers',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-08 11:07:19',
            ),
            495 => 
            array (
                'key' => 'school-term-child--surname-validation',
                'lang' => 'en',
                'value' => 'Enter child surname',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 14:26:51',
            ),
            496 => 
            array (
                'key' => 'school-term-child--surname-validation',
                'lang' => 'fr',
                'value' => 'Enter child surname',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            497 => 
            array (
                'key' => 'school-term-child-confirm-email-placeholder',
                'lang' => 'en',
                'value' => 'Confirm Email Address',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-06-22 14:12:30',
            ),
            498 => 
            array (
                'key' => 'school-term-child-confirm-email-placeholder',
                'lang' => 'fr',
                'value' => 'Confirm email address',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            499 => 
            array (
                'key' => 'school-term-child-confirm-email-validation',
                'lang' => 'en',
                'value' => 'email not matched',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
        ));
        \DB::table('texts')->insert(array (
            0 => 
            array (
                'key' => 'school-term-child-confirm-email-validation',
                'lang' => 'fr',
                'value' => 'email not matched',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            1 => 
            array (
                'key' => 'school-term-child-detail-title',
                'lang' => 'en',
                'value' => '<p>Student Details</p><p><br></p>',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-09-07 13:47:40',
            ),
            2 => 
            array (
                'key' => 'school-term-child-detail-title',
                'lang' => 'fr',
                'value' => 'My child detail',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            3 => 
            array (
                'key' => 'school-term-child-email-placeholder',
                'lang' => 'en',
                'value' => 'Email Address',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-06-22 14:12:18',
            ),
            4 => 
            array (
                'key' => 'school-term-child-email-placeholder',
                'lang' => 'fr',
                'value' => 'Email address',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            5 => 
            array (
                'key' => 'school-term-child-email-validation',
                'lang' => 'en',
                'value' => 'enter valid child email \'abc1@xyz.com\'',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            6 => 
            array (
                'key' => 'school-term-child-email-validation',
                'lang' => 'fr',
                'value' => 'enter valid child email \'abc1@xyz.com\'',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            7 => 
            array (
                'key' => 'school-term-child-first-name-placeholder',
                'lang' => 'en',
                'value' => 'Students First Name',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-07-10 18:18:44',
            ),
            8 => 
            array (
                'key' => 'school-term-child-first-name-placeholder',
                'lang' => 'fr',
                'value' => 'First Nameeee',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            9 => 
            array (
                'key' => 'school-term-child-first-name-validation',
                'lang' => 'en',
                'value' => 'Enter child Name',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 14:26:45',
            ),
            10 => 
            array (
                'key' => 'school-term-child-first-name-validation',
                'lang' => 'fr',
                'value' => 'Enter child Name',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            11 => 
            array (
                'key' => 'school-term-child-number-title',
                'lang' => 'en',
                'value' => 'Pupil',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-06-25 13:59:24',
            ),
            12 => 
            array (
                'key' => 'school-term-child-number-title',
                'lang' => 'fr',
                'value' => 'Child',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            13 => 
            array (
                'key' => 'school-term-child-surname-placeholder',
                'lang' => 'en',
                'value' => 'Students Surname',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-07-10 18:18:54',
            ),
            14 => 
            array (
                'key' => 'school-term-child-surname-placeholder',
                'lang' => 'fr',
                'value' => 'Surname',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            15 => 
            array (
                'key' => 'school-term-end-date-placeholder',
                'lang' => 'en',
                'value' => 'Please select End Date',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            16 => 
            array (
                'key' => 'school-term-end-date-placeholder',
                'lang' => 'fr',
                'value' => 'Please select End Date',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            17 => 
            array (
                'key' => 'school-term-end-date-validation',
                'lang' => 'en',
                'value' => 'kindly enter end date',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            18 => 
            array (
                'key' => 'school-term-end-date-validation',
                'lang' => 'fr',
                'value' => 'kindly enter end date',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            19 => 
            array (
                'key' => 'school-term-end-later-date-validation',
                'lang' => 'en',
                'value' => 'End date %toDate% is a previous date then start date %fromDate%.kindly enter correct date flow',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            20 => 
            array (
                'key' => 'school-term-end-later-date-validation',
                'lang' => 'fr',
                'value' => 'End date %toDate% is a previous date then start date %fromDate%.kindly enter correct date flow',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            21 => 
            array (
                'key' => 'school-term-first-name-validation',
                'lang' => 'en',
                'value' => 'kindly enter first name',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            22 => 
            array (
                'key' => 'school-term-first-name-validation',
                'lang' => 'fr',
                'value' => 'kindly enter first name',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            23 => 
            array (
                'key' => 'school-term-hold-locker-availability-validation',
                'lang' => 'en',
                'value' => 'Their are no lockers available for <b>%fromDate%</b> - <b>%toDate%</b> for <b>%locationName%</b> please either choose another date or <b style="color:#18D26E"><a href="/frontend/requestavailability" target="_blank">%requestFormLINK%</a></b>',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            24 => 
            array (
                'key' => 'school-term-hold-locker-availability-validation',
                'lang' => 'fr',
                'value' => 'Their are no lockers available for <b>%fromDate%</b> - <b>%toDate%</b> for <b>%locationName%</b> please either choose another date or <b style="color:#18D26E"><a href="/frontend/requestavailability" target="_blank">%requestFormLINK%</a></b>',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            25 => 
            array (
                'key' => 'school-term-hold-locker-checkbox-text',
                'lang' => 'en',
                'value' => 'Retain a locker for the duration of my child\'s attendance whilst at this school. You will be given the opportunity to confirm this booking in April.',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            26 => 
            array (
                'key' => 'school-term-hold-locker-checkbox-text',
                'lang' => 'fr',
                'value' => 'Retain a locker for the duration of my child\'s attendance whilst at this school. You will be given the opportunity to confirm this booking in April.',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            27 => 
            array (
                'key' => 'school-term-hold-this-locker-checkbox-text',
                'lang' => 'en',
                'value' => 'Hold this locker to be booked while my child is at the school. You will be given the opportunity to confirm this booking in April.',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            28 => 
            array (
                'key' => 'school-term-hold-this-locker-checkbox-text',
                'lang' => 'fr',
                'value' => 'Hold this locker to be booked while my child is at the school. You will be given the opportunity to confirm this booking in April.',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            29 => 
            array (
                'key' => 'school-term-locker-availability-validation',
                'lang' => 'en',
            'value' => 'There are no lockers available for <b>%fromDate%</b> - <b>%toDate%</b> for <b>%locationName%</b> please either choose another date, check again shortly as we are constantly reviewing availability or&nbsp;<span style="caret-color: rgb(102, 102, 102); color: rgb(102, 102, 102);"><b><a href="https://locker.rentals/frontend/requestavailability" target="_blank">Request New Locker</a></b></span><br>',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-12 16:49:00',
            ),
            30 => 
            array (
                'key' => 'school-term-locker-availability-validation',
                'lang' => 'fr',
            'value' => 'There are no lockers available for <b>%fromDate%</b> - <b>%toDate%</b> for <b>%locationName%</b> please either choose another date, check again shortly as we are constantly reviewing availability or&nbsp;<span style="caret-color: rgb(102, 102, 102); color: rgb(102, 102, 102);"><b>%requestFormLINK%</b></span><br>',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            31 => 
            array (
                'key' => 'school-term-log-out-button',
                'lang' => 'en',
                'value' => 'Log Out',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            32 => 
            array (
                'key' => 'school-term-log-out-button',
                'lang' => 'fr',
                'value' => 'Log Out',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            33 => 
            array (
                'key' => 'school-term-login-user-msg',
                'lang' => 'en',
                'value' => 'Welcome <b>%first_name%</b> <b>%surname%</b> your booking will be attached to your user account with the
<b>%email%</b>. If you wish to create an alternative account <b>%logout%</b>',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            34 => 
            array (
                'key' => 'school-term-login-user-msg',
                'lang' => 'fr',
                'value' => 'Welcome <b>%first_name%</b> <b>%surname%</b> your booking will be attached to your user account with the
<b>%email%</b>. If you wish to create an alternative account <b>%logout%</b>',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            35 => 
            array (
                'key' => 'school-term-my-confirm-email-placeholder',
                'lang' => 'en',
                'value' => 'confirmEmail',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            36 => 
            array (
                'key' => 'school-term-my-confirm-email-placeholder',
                'lang' => 'fr',
                'value' => 'confirmEmail',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            37 => 
            array (
                'key' => 'school-term-my-confirm-email-validation',
                'lang' => 'en',
                'value' => 'email not matched',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            38 => 
            array (
                'key' => 'school-term-my-confirm-email-validation',
                'lang' => 'fr',
                'value' => 'email not matched',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            39 => 
            array (
                'key' => 'school-term-my-detail-title',
                'lang' => 'en',
                'value' => 'My Details',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-09-02 19:18:05',
            ),
            40 => 
            array (
                'key' => 'school-term-my-detail-title',
                'lang' => 'fr',
                'value' => 'My detail',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            41 => 
            array (
                'key' => 'school-term-my-email-placeholder',
                'lang' => 'en',
                'value' => 'Email',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            42 => 
            array (
                'key' => 'school-term-my-email-placeholder',
                'lang' => 'fr',
                'value' => 'Email',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            43 => 
            array (
                'key' => 'school-term-my-email-validation',
                'lang' => 'en',
                'value' => 'kindly enter valid email \'abc1@xyz.com\'',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            44 => 
            array (
                'key' => 'school-term-my-email-validation',
                'lang' => 'fr',
                'value' => 'kindly enter valid email \'abc1@xyz.com\'',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            45 => 
            array (
                'key' => 'school-term-my-first-name-placeholder',
                'lang' => 'en',
                'value' => 'First Name',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            46 => 
            array (
                'key' => 'school-term-my-first-name-placeholder',
                'lang' => 'fr',
                'value' => 'First Name',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            47 => 
            array (
                'key' => 'school-term-my-phone-number-placeholder',
                'lang' => 'en',
                'value' => 'Phone Number',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            48 => 
            array (
                'key' => 'school-term-my-phone-number-placeholder',
                'lang' => 'fr',
                'value' => 'Phone Number',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            49 => 
            array (
                'key' => 'school-term-my-phone-number-validation',
                'lang' => 'en',
                'value' => 'kindly enter phone number',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            50 => 
            array (
                'key' => 'school-term-my-phone-number-validation',
                'lang' => 'fr',
                'value' => 'kindly enter phone number',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            51 => 
            array (
                'key' => 'school-term-my-surname-placeholder',
                'lang' => 'en',
                'value' => 'Surname',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            52 => 
            array (
                'key' => 'school-term-my-surname-placeholder',
                'lang' => 'fr',
                'value' => 'Surname',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            53 => 
            array (
                'key' => 'school-term-my-surname-validation',
                'lang' => 'en',
                'value' => 'kindly enter surname',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            54 => 
            array (
                'key' => 'school-term-my-surname-validation',
                'lang' => 'fr',
                'value' => 'kindly enter surname',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            55 => 
            array (
                'key' => 'school-term-receive-future-checkbox-text',
                'lang' => 'en',
                'value' => 'I would like to receive future communications from Prefect Lockers.',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            56 => 
            array (
                'key' => 'school-term-receive-future-checkbox-text',
                'lang' => 'fr',
                'value' => 'I would like to receive future communications from Prefect Lockers.',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            57 => 
            array (
                'key' => 'school-term-remove-new-child-button',
                'lang' => 'en',
                'value' => 'Remove Pupil',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-19 18:21:18',
            ),
            58 => 
            array (
                'key' => 'school-term-remove-new-child-button',
                'lang' => 'fr',
                'value' => 'Remove Child',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            59 => 
            array (
                'key' => 'school-term-restart-booking-button',
                'lang' => 'en',
                'value' => 'Cancel Reservation',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-08 11:06:17',
            ),
            60 => 
            array (
                'key' => 'school-term-restart-booking-button',
                'lang' => 'fr',
                'value' => 'Cancel Reservation',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-08 11:05:55',
            ),
            61 => 
            array (
                'key' => 'school-term-school-academic-year',
                'lang' => 'en',
                'value' => 'Student Year Group',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-19 18:17:41',
            ),
            62 => 
            array (
                'key' => 'school-term-school-academic-year',
                'lang' => 'fr',
                'value' => 'Pupils Year Group',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            63 => 
            array (
                'key' => 'school-term-school-academic-year-validation',
                'lang' => 'en',
                'value' => 'Choose  School Academic Year',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 14:26:59',
            ),
            64 => 
            array (
                'key' => 'school-term-school-academic-year-validation',
                'lang' => 'fr',
                'value' => 'Choose  School Academic Year',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            65 => 
            array (
                'key' => 'school-term-school-year',
                'lang' => 'en',
                'value' => 'Academic School Year',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            66 => 
            array (
                'key' => 'school-term-school-year',
                'lang' => 'fr',
                'value' => 'Academic School Year',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            67 => 
            array (
                'key' => 'school-term-school-year-user-guide-msg',
                'lang' => 'en',
                'value' => 'Kindly select school year for Proceed!',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            68 => 
            array (
                'key' => 'school-term-school-year-user-guide-msg',
                'lang' => 'fr',
                'value' => 'Kindly select school year for Proceed!',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            69 => 
            array (
                'key' => 'school-term-school-year-validation',
                'lang' => 'en',
                'value' => 'Select School Year',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 14:27:07',
            ),
            70 => 
            array (
                'key' => 'school-term-school-year-validation',
                'lang' => 'fr',
                'value' => 'Select School Year',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            71 => 
            array (
                'key' => 'school-term-start-date-placeholder',
                'lang' => 'en',
                'value' => 'Please select Start Date',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            72 => 
            array (
                'key' => 'school-term-start-date-placeholder',
                'lang' => 'fr',
                'value' => 'Please select Start Date',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            73 => 
            array (
                'key' => 'school-term-start-date-validation',
                'lang' => 'en',
                'value' => 'kindly enter start date',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            74 => 
            array (
                'key' => 'school-term-start-date-validation',
                'lang' => 'fr',
                'value' => 'kindly enter start date',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            75 => 
            array (
                'key' => 'school-term-sub-title',
                'lang' => 'en',
                'value' => 'Pay for Reservation',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-05-08 11:38:40',
            ),
            76 => 
            array (
                'key' => 'school-term-sub-title',
                'lang' => 'fr',
                'value' => 'Select Locker Rental Period',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            77 => 
            array (
                'key' => 'school-term-terms-and-conditions',
                'lang' => 'en',
            'value' => '<ul><li>The rental period is from the point of rental (regardless of date) until the end of the current school year. Reduced rates for short date ranges are not available.</li><li>Rental fees are non-refundable.</li><li>Rental of lockers is non-transferable.</li><li>The locker can only be used by the lessee and lockers cannot be shared.</li><li>
Lockers can only be used for the storage of personal items that belong to the lessee and must not include illegal items or any item prohibited by the school.</li><li>Prefect will not be held liable for any loss, damage or theft of items kept in the lockers.</li><li>
The school has the right to inspect the contents of the locker without prior notice.</li><li>
At the end of the school year all items must be removed from the locker and the locker left empty. Any items left in the locker will be removed and disposed of without exception.</li><li>
The locker should be left clean and in good condition.</li><li>Any damage or vandalism must be reported to the school immediately.</li><li>Codes can be reset, forgotten codes can be re-issued by visiting Locker.Rentals.</li></ul>',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            78 => 
            array (
                'key' => 'school-term-terms-and-conditions',
                'lang' => 'fr',
            'value' => '<ul><li>The rental period is from the point of rental (regardless of date) until the end of the current school year. Reduced rates for short date ranges are not available.</li><li>Rental fees are non-refundable.</li><li>Rental of lockers is non-transferable.</li><li>The locker can only be used by the lessee and lockers cannot be shared.</li><li>
Lockers can only be used for the storage of personal items that belong to the lessee and must not include illegal items or any item prohibited by the school.</li><li>Prefect will not be held liable for any loss, damage or theft of items kept in the lockers.</li><li>
The school has the right to inspect the contents of the locker without prior notice.</li><li>
At the end of the school year all items must be removed from the locker and the locker left empty. Any items left in the locker will be removed and disposed of without exception.</li><li>
The locker should be left clean and in good condition.</li><li>Any damage or vandalism must be reported to the school immediately.</li><li>Codes can be reset, forgotten codes can be re-issued by visiting Locker.Rentals.</li></ul>',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            79 => 
            array (
                'key' => 'school-term-terms-bottom-text',
                'lang' => 'en',
            'value' => 'For assistance or support issues (other than to retrieve your code) please e-mail us at rentals@prefectlockers.com',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-08-26 16:28:44',
            ),
            80 => 
            array (
                'key' => 'school-term-terms-bottom-text',
                'lang' => 'fr',
            'value' => 'For assistance or support issues (other than to retrieve your code) please contact us on 0330 311 1003 or rentals@prefectlockers.com',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            81 => 
            array (
                'key' => 'school-term-terms-conditions-button',
                'lang' => 'en',
                'value' => 'Terms and Conditions',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            82 => 
            array (
                'key' => 'school-term-terms-conditions-button',
                'lang' => 'fr',
                'value' => 'Terms and Conditions',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            83 => 
            array (
                'key' => 'school-term-terms-conditions-title',
                'lang' => 'en',
                'value' => 'Please tick the box to confirm that you have read and accepted our terms and conditions.',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            84 => 
            array (
                'key' => 'school-term-terms-conditions-title',
                'lang' => 'fr',
                'value' => 'Please tick the box to confirm that you have read and accepted our terms and conditions.',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            85 => 
            array (
                'key' => 'school-term-terms-title',
                'lang' => 'en',
                'value' => 'This rental agreement is between Prefect Rentals and the lessee',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            86 => 
            array (
                'key' => 'school-term-terms-title',
                'lang' => 'fr',
                'value' => 'This rental agreement is between Prefect Rentals and the lessee',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            87 => 
            array (
                'key' => 'school-term-title',
                'lang' => 'en',
                'value' => '%type_of_booking% for <b>%school_name%</b>',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            88 => 
            array (
                'key' => 'school-term-title',
                'lang' => 'fr',
                'value' => '%type_of_booking% for <b>%school_name%</b>',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            89 => 
            array (
                'key' => 'school-term-title-location-group2',
                'lang' => 'en',
                'value' => '%type_of_booking% for <b>%school_name%</b>&nbsp;',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            90 => 
            array (
                'key' => 'school-term-title-location-group2',
                'lang' => 'fr',
                'value' => '%type_of_booking% for <b>%school_name%</b>&nbsp;',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            91 => 
            array (
                'key' => 'school-term-title-location-group3',
                'lang' => 'en',
                'value' => '%type_of_booking% for <b>%school_name%</b>&nbsp;',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            92 => 
            array (
                'key' => 'school-term-title-location-group3',
                'lang' => 'fr',
                'value' => '%type_of_booking% for <b>%school_name%</b>&nbsp;',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            93 => 
            array (
                'key' => 'school-term-voucher-code-label',
                'lang' => 'en',
                'value' => 'Voucher Code',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            94 => 
            array (
                'key' => 'school-term-voucher-code-placeholder',
                'lang' => 'en',
                'value' => 'aa',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => '2020-05-25 11:51:20',
            ),
            95 => 
            array (
                'key' => 'secure-locker-rentals-footer',
                'lang' => 'en',
                'value' => 'Secure Rentals',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            96 => 
            array (
                'key' => 'secure-locker-rentals-footer',
                'lang' => 'fr',
                'value' => 'Secure Rentals',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            97 => 
            array (
                'key' => 'secure-locker-rentals-footer',
                'lang' => 'it',
                'value' => 'secure-locker-rentals-footer',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            98 => 
            array (
                'key' => 'slider-1',
                'lang' => 'en',
                'value' => 'slider-1333',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            99 => 
            array (
                'key' => 'slider-1',
                'lang' => 'fr',
                'value' => 'slider-1333',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            100 => 
            array (
                'key' => 'slider-1-buuton',
                'lang' => 'en',
                'value' => 'slider-1-buuton',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            101 => 
            array (
                'key' => 'slider-1-buuton',
                'lang' => 'fr',
                'value' => 'slider-1-buuton',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            102 => 
            array (
                'key' => 'slider-1-buuton',
                'lang' => 'it',
                'value' => 'slider-1-buuton',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            103 => 
            array (
                'key' => 'slider-13',
                'lang' => 'en',
                'value' => 'slider-13',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            104 => 
            array (
                'key' => 'slider-13',
                'lang' => 'fr',
                'value' => 'slider-13',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            105 => 
            array (
                'key' => 'slider-1333',
                'lang' => 'fr',
                'value' => 'slider-1333',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            106 => 
            array (
                'key' => 'static-banner-button',
                'lang' => 'en',
                'value' => 'Request Locker',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            107 => 
            array (
                'key' => 'static-banner-button',
                'lang' => 'fr',
                'value' => 'Request Locker',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            108 => 
            array (
                'key' => 'static-banner-button',
                'lang' => 'it',
                'value' => 'helllo',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            109 => 
            array (
                'key' => 'static-banner-title',
                'lang' => 'en',
                'value' => 'Book Your Locker',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            110 => 
            array (
                'key' => 'static-banner-title',
                'lang' => 'fr',
                'value' => 'Book Your Locker',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            111 => 
            array (
                'key' => 'static-banner-title',
                'lang' => 'it',
                'value' => 'static-banner-title',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            112 => 
            array (
                'key' => 'support-menu',
                'lang' => 'en',
                'value' => 'support',
                'scope' => 'global',
                'created_at' => '2020-09-09 23:05:33',
                'updated_at' => '2020-09-09 23:05:33',
            ),
            113 => 
            array (
                'key' => 'twitter-link-footer',
                'lang' => 'en',
                'value' => '#',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            114 => 
            array (
                'key' => 'twitter-link-footer',
                'lang' => 'fr',
                'value' => '#',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            115 => 
            array (
                'key' => 'twitter-link-footer',
                'lang' => 'it',
                'value' => 'twitter-link-footer',
                'scope' => 'global',
                'created_at' => '2020-04-15 13:00:00',
                'updated_at' => '2020-04-15 13:00:00',
            ),
            116 => 
            array (
                'key' => 'unsub-heading',
                'lang' => 'en',
                'value' => 'Booking Management',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => '2020-05-28 23:19:05',
            ),
            117 => 
            array (
                'key' => 'unsub-heading',
                'lang' => 'fr',
                'value' => 'Subscription',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            118 => 
            array (
                'key' => 'unsub-section1-text',
                'lang' => 'en',
                'value' => 'We are sorry your have chosen to stop receiving booking reminders if you wish to update these settings to mange your bookings please %login%',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            119 => 
            array (
                'key' => 'unsub-section1-text',
                'lang' => 'fr',
                'value' => 'We are sorry your have chosen to stop receiving booking reminders if you wish to update these settings to mange your bookings please %login%',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            120 => 
            array (
                'key' => 'unsub-text',
                'lang' => 'en',
                'value' => 'You will not recieve further emails regarding this booking',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => '2020-05-28 23:19:37',
            ),
            121 => 
            array (
                'key' => 'unsub-text',
                'lang' => 'fr',
                'value' => 'You have been unsubscribed',
                'scope' => 'global',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}