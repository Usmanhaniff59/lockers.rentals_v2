<?php

use Illuminate\Database\Seeder;

class RefundTransactionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('refund_transactions')->delete();
        
        \DB::table('refund_transactions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'transaction_id' => '516254D4-2B23-1D31-AE91-025D2A3C6FCD',
                'status' => 1,
                'amount' => 45,
                'created_at' => '2020-05-08 13:35:46',
                'updated_at' => '2020-05-08 13:35:46',
            ),
            1 => 
            array (
                'id' => 2,
                'transaction_id' => 'B4806215-F481-C52D-2756-549A24218FA5',
                'status' => 1,
                'amount' => 8,
                'created_at' => '2020-09-16 12:45:11',
                'updated_at' => '2020-09-16 12:45:11',
            ),
        ));
        
        
    }
}