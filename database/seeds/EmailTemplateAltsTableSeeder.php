<?php

use Illuminate\Database\Seeder;

class EmailTemplateAltsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('email_template_alts')->delete();
        
        \DB::table('email_template_alts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'email_template_id' => 5,
                'tab_name' => 'English Dynamic',
                'lang' => 'EN',
                'link_text' => 'd',
                'text' => 'test english dynamic',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'email_template_id' => 5,
                'tab_name' => 'French Manual',
                'lang' => 'FR',
                'link_text' => 'm',
                'text' => 'test french manual',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'email_template_id' => 5,
                'tab_name' => 'French Dynamic',
                'lang' => 'FR',
                'link_text' => 'd',
                'text' => 'test french dynamic',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'email_template_id' => 7,
                'tab_name' => 'English Dynamic',
                'lang' => 'EN',
                'link_text' => 'd',
                'text' => 'test english dynamic',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'email_template_id' => 7,
                'tab_name' => 'French Manual',
                'lang' => 'FR',
                'link_text' => 'm',
                'text' => 'test french manual',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'email_template_id' => 7,
                'tab_name' => 'French Dynamic',
                'lang' => 'FR',
                'link_text' => 'd',
                'text' => 'test french dynamic',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}