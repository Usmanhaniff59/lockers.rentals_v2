<?php

use Illuminate\Database\Seeder;

class EmailFoldersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('email_folders')->delete();
        
        \DB::table('email_folders')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Social',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Promotions',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Updated',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}