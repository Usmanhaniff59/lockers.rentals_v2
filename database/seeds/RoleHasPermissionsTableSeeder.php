<?php

use Illuminate\Database\Seeder;

class RoleHasPermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('role_has_permissions')->truncate();
        
        \DB::table('role_has_permissions')->insert(array (
            0 => 
            array (
                'permission_id' => 1,
                'role_id' => 1,
            ),
            1 => 
            array (
                'permission_id' => 3,
                'role_id' => 1,
            ),
            2 => 
            array (
                'permission_id' => 4,
                'role_id' => 1,
            ),
            3 => 
            array (
                'permission_id' => 4,
                'role_id' => 2,
            ),
            4 => 
            array (
                'permission_id' => 7,
                'role_id' => 1,
            ),
            5 => 
            array (
                'permission_id' => 8,
                'role_id' => 1,
            ),
            6 => 
            array (
                'permission_id' => 9,
                'role_id' => 1,
            ),
            7 => 
            array (
                'permission_id' => 10,
                'role_id' => 1,
            ),
            8 => 
            array (
                'permission_id' => 11,
                'role_id' => 1,
            ),
            9 => 
            array (
                'permission_id' => 12,
                'role_id' => 1,
            ),
            10 => 
            array (
                'permission_id' => 14,
                'role_id' => 1,
            ),
            11 => 
            array (
                'permission_id' => 14,
                'role_id' => 2,
            ),
            12 => 
            array (
                'permission_id' => 15,
                'role_id' => 1,
            ),
            13 => 
            array (
                'permission_id' => 15,
                'role_id' => 2,
            ),
            14 => 
            array (
                'permission_id' => 16,
                'role_id' => 1,
            ),
            15 => 
            array (
                'permission_id' => 16,
                'role_id' => 2,
            ),
            16 => 
            array (
                'permission_id' => 17,
                'role_id' => 1,
            ),
            17 => 
            array (
                'permission_id' => 17,
                'role_id' => 2,
            ),
            18 => 
            array (
                'permission_id' => 18,
                'role_id' => 1,
            ),
            19 => 
            array (
                'permission_id' => 18,
                'role_id' => 2,
            ),
            20 => 
            array (
                'permission_id' => 19,
                'role_id' => 1,
            ),
            21 => 
            array (
                'permission_id' => 19,
                'role_id' => 2,
            ),
            22 => 
            array (
                'permission_id' => 20,
                'role_id' => 1,
            ),
            23 => 
            array (
                'permission_id' => 20,
                'role_id' => 2,
            ),
            24 => 
            array (
                'permission_id' => 21,
                'role_id' => 1,
            ),
            25 => 
            array (
                'permission_id' => 21,
                'role_id' => 2,
            ),
            26 => 
            array (
                'permission_id' => 21,
                'role_id' => 3,
            ),
            27 => 
            array (
                'permission_id' => 21,
                'role_id' => 4,
            ),
            28 => 
            array (
                'permission_id' => 21,
                'role_id' => 10,
            ),
            29 => 
            array (
                'permission_id' => 22,
                'role_id' => 1,
            ),
            30 => 
            array (
                'permission_id' => 22,
                'role_id' => 2,
            ),
            31 => 
            array (
                'permission_id' => 23,
                'role_id' => 1,
            ),
            32 => 
            array (
                'permission_id' => 23,
                'role_id' => 2,
            ),
            33 => 
            array (
                'permission_id' => 24,
                'role_id' => 1,
            ),
            34 => 
            array (
                'permission_id' => 25,
                'role_id' => 1,
            ),
            35 => 
            array (
                'permission_id' => 25,
                'role_id' => 2,
            ),
            36 => 
            array (
                'permission_id' => 26,
                'role_id' => 1,
            ),
            37 => 
            array (
                'permission_id' => 28,
                'role_id' => 1,
            ),
            38 => 
            array (
                'permission_id' => 28,
                'role_id' => 2,
            ),
            39 => 
            array (
                'permission_id' => 29,
                'role_id' => 1,
            ),
            40 => 
            array (
                'permission_id' => 29,
                'role_id' => 2,
            ),
            41 => 
            array (
                'permission_id' => 30,
                'role_id' => 1,
            ),
            42 => 
            array (
                'permission_id' => 31,
                'role_id' => 1,
            ),
            43 => 
            array (
                'permission_id' => 34,
                'role_id' => 1,
            ),
            44 => 
            array (
                'permission_id' => 35,
                'role_id' => 1,
            ),
            45 => 
            array (
                'permission_id' => 35,
                'role_id' => 2,
            ),
            46 => 
            array (
                'permission_id' => 36,
                'role_id' => 1,
            ),
            47 => 
            array (
                'permission_id' => 37,
                'role_id' => 1,
            ),
            48 => 
            array (
                'permission_id' => 38,
                'role_id' => 1,
            ),
            49 => 
            array (
                'permission_id' => 39,
                'role_id' => 1,
            ),
            50 => 
            array (
                'permission_id' => 42,
                'role_id' => 1,
            ),
            51 => 
            array (
                'permission_id' => 44,
                'role_id' => 1,
            ),
            52 => 
            array (
                'permission_id' => 44,
                'role_id' => 2,
            ),
            53 => 
            array (
                'permission_id' => 44,
                'role_id' => 3,
            ),
            54 => 
            array (
                'permission_id' => 44,
                'role_id' => 10,
            ),
            55 => 
            array (
                'permission_id' => 45,
                'role_id' => 1,
            ),
            56 => 
            array (
                'permission_id' => 45,
                'role_id' => 2,
            ),
            57 => 
            array (
                'permission_id' => 45,
                'role_id' => 3,
            ),
            58 => 
            array (
                'permission_id' => 45,
                'role_id' => 4,
            ),
            59 => 
            array (
                'permission_id' => 45,
                'role_id' => 10,
            ),
            60 => 
            array (
                'permission_id' => 46,
                'role_id' => 1,
            ),
            61 => 
            array (
                'permission_id' => 46,
                'role_id' => 2,
            ),
            62 => 
            array (
                'permission_id' => 46,
                'role_id' => 3,
            ),
            63 => 
            array (
                'permission_id' => 46,
                'role_id' => 4,
            ),
            64 => 
            array (
                'permission_id' => 46,
                'role_id' => 10,
            ),
            65 => 
            array (
                'permission_id' => 47,
                'role_id' => 1,
            ),
            66 => 
            array (
                'permission_id' => 47,
                'role_id' => 2,
            ),
            67 => 
            array (
                'permission_id' => 49,
                'role_id' => 1,
            ),
            68 => 
            array (
                'permission_id' => 49,
                'role_id' => 2,
            ),
            69 => 
            array (
                'permission_id' => 50,
                'role_id' => 1,
            ),
            70 => 
            array (
                'permission_id' => 50,
                'role_id' => 2,
            ),
            71 => 
            array (
                'permission_id' => 52,
                'role_id' => 1,
            ),
            72 => 
            array (
                'permission_id' => 52,
                'role_id' => 2,
            ),
            73 => 
            array (
                'permission_id' => 53,
                'role_id' => 1,
            ),
            74 => 
            array (
                'permission_id' => 53,
                'role_id' => 2,
            ),
            // 75 => 
            // array (
            //     'permission_id' => 54,
            //     'role_id' => 1,
            // ),
            // 76 => 
            // array (
            //     'permission_id' => 54,
            //     'role_id' => 2,
            // ),
            77 => 
            array (
                'permission_id' => 57,
                'role_id' => 1,
            ),
            78 => 
            array (
                'permission_id' => 57,
                'role_id' => 2,
            ),
            79 => 
            array (
                'permission_id' => 58,
                'role_id' => 1,
            ),
            80 => 
            array (
                'permission_id' => 58,
                'role_id' => 2,
            ),
            81 => 
            array (
                'permission_id' => 59,
                'role_id' => 1,
            ),
            82 => 
            array (
                'permission_id' => 59,
                'role_id' => 2,
            ),
            83 => 
            array (
                'permission_id' => 60,
                'role_id' => 1,
            ),
            84 => 
            array (
                'permission_id' => 60,
                'role_id' => 2,
            ),
            85 => 
            array (
                'permission_id' => 61,
                'role_id' => 1,
            ),
            86 => 
            array (
                'permission_id' => 61,
                'role_id' => 2,
            ),
            87 => 
            array (
                'permission_id' => 62,
                'role_id' => 1,
            ),
            88 => 
            array (
                'permission_id' => 62,
                'role_id' => 2,
            ),
            89 => 
            array (
                'permission_id' => 63,
                'role_id' => 1,
            ),
            90 => 
            array (
                'permission_id' => 63,
                'role_id' => 2,
            ),
            91 => 
            array (
                'permission_id' => 64,
                'role_id' => 1,
            ),
            92 => 
            array (
                'permission_id' => 65,
                'role_id' => 1,
            ),
            93 => 
            array (
                'permission_id' => 66,
                'role_id' => 1,
            ),
            94 => 
            array (
                'permission_id' => 67,
                'role_id' => 1,
            ),
            95 => 
            array (
                'permission_id' => 67,
                'role_id' => 2,
            ),
            96 => 
            array (
                'permission_id' => 68,
                'role_id' => 1,
            ),
            97 => 
            array (
                'permission_id' => 68,
                'role_id' => 2,
            ),
            98 => 
            array (
                'permission_id' => 68,
                'role_id' => 3,
            ),
            99 => 
            array (
                'permission_id' => 68,
                'role_id' => 4,
            ),
            100 => 
            array (
                'permission_id' => 68,
                'role_id' => 10,
            ),
            101 => 
            array (
                'permission_id' => 69,
                'role_id' => 1,
            ),
            102 => 
            array (
                'permission_id' => 69,
                'role_id' => 2,
            ),
            103 => 
            array (
                'permission_id' => 69,
                'role_id' => 3,
            ),
            104 => 
            array (
                'permission_id' => 69,
                'role_id' => 10,
            ),
            105 => 
            array (
                'permission_id' => 70,
                'role_id' => 2,
            ),
            106 => 
            array (
                'permission_id' => 71,
                'role_id' => 1,
            ),
            107 => 
            array (
                'permission_id' => 71,
                'role_id' => 2,
            ),
            108 => 
            array (
                'permission_id' => 73,
                'role_id' => 1,
            ),
            109 => 
            array (
                'permission_id' => 73,
                'role_id' => 2,
            ),
            110 => 
            array (
                'permission_id' => 73,
                'role_id' => 3,
            ),
            111 => 
            array (
                'permission_id' => 73,
                'role_id' => 10,
            ),
            112 => 
            array (
                'permission_id' => 74,
                'role_id' => 1,
            ),
            113 => 
            array (
                'permission_id' => 74,
                'role_id' => 2,
            ),
            114 => 
            array (
                'permission_id' => 75,
                'role_id' => 1,
            ),
            115 => 
            array (
                'permission_id' => 75,
                'role_id' => 2,
            ),
            116 => 
            array (
                'permission_id' => 75,
                'role_id' => 3,
            ),
            117 => 
            array (
                'permission_id' => 75,
                'role_id' => 10,
            ),
            118 => 
            array (
                'permission_id' => 76,
                'role_id' => 1,
            ),
            119 => 
            array (
                'permission_id' => 76,
                'role_id' => 2,
            ),
            120 => 
            array (
                'permission_id' => 76,
                'role_id' => 3,
            ),
            121 => 
            array (
                'permission_id' => 76,
                'role_id' => 10,
            ),
            122 => 
            array (
                'permission_id' => 77,
                'role_id' => 1,
            ),
            123 => 
            array (
                'permission_id' => 77,
                'role_id' => 2,
            ),
            124 => 
            array (
                'permission_id' => 79,
                'role_id' => 1,
            ),
            125 => 
            array (
                'permission_id' => 79,
                'role_id' => 2,
            ),
            126 => 
            array (
                'permission_id' => 79,
                'role_id' => 4,
            ),
            127 => 
            array (
                'permission_id' => 80,
                'role_id' => 1,
            ),
            128 => 
            array (
                'permission_id' => 80,
                'role_id' => 2,
            ),
            129 => 
            array (
                'permission_id' => 81,
                'role_id' => 1,
            ),
            130 => 
            array (
                'permission_id' => 81,
                'role_id' => 2,
            ),
            131 => 
            array (
                'permission_id' => 81,
                'role_id' => 4,
            ),
            132 => 
            array (
                'permission_id' => 82,
                'role_id' => 1,
            ),
            133 => 
            array (
                'permission_id' => 82,
                'role_id' => 2,
            ),
            134 => 
            array (
                'permission_id' => 82,
                'role_id' => 3,
            ),
            135 => 
            array (
                'permission_id' => 83,
                'role_id' => 1,
            ),
            136 => 
            array (
                'permission_id' => 83,
                'role_id' => 2,
            ),
            137 => 
            array (
                'permission_id' => 84,
                'role_id' => 1,
            ),
            138 => 
            array (
                'permission_id' => 84,
                'role_id' => 2,
            ),
            139 => 
            array (
                'permission_id' => 86,
                'role_id' => 1,
            ),
            140 => 
            array (
                'permission_id' => 86,
                'role_id' => 2,
            ),
            141 => 
            array (
                'permission_id' => 87,
                'role_id' => 1,
            ),
            142 => 
            array (
                'permission_id' => 87,
                'role_id' => 2,
            ),
            143 => 
            array (
                'permission_id' => 87,
                'role_id' => 10,
            ),
            144 => 
            array (
                'permission_id' => 88,
                'role_id' => 1,
            ),
            145 => 
            array (
                'permission_id' => 88,
                'role_id' => 2,
            ),
            146 => 
            array (
                'permission_id' => 88,
                'role_id' => 10,
            ),
            147 => 
            array (
                'permission_id' => 89,
                'role_id' => 1,
            ),
            148 => 
            array (
                'permission_id' => 89,
                'role_id' => 2,
            ),
            149 => 
            array (
                'permission_id' => 90,
                'role_id' => 1,
            ),
            150 => 
            array (
                'permission_id' => 90,
                'role_id' => 2,
            ),
            151 => 
            array (
                'permission_id' => 91,
                'role_id' => 1,
            ),
            152 => 
            array (
                'permission_id' => 91,
                'role_id' => 2,
            ),
            153 => 
            array (
                'permission_id' => 92,
                'role_id' => 1,
            ),
            154 => 
            array (
                'permission_id' => 92,
                'role_id' => 2,
            ),
            155 => 
            array (
                'permission_id' => 92,
                'role_id' => 10,
            ),
            156 => 
            array (
                'permission_id' => 93,
                'role_id' => 1,
            ),
            157 => 
            array (
                'permission_id' => 93,
                'role_id' => 2,
            ),
            158 => 
            array (
                'permission_id' => 94,
                'role_id' => 1,
            ),
            159 => 
            array (
                'permission_id' => 94,
                'role_id' => 2,
            ),
            160 => 
            array (
                'permission_id' => 95,
                'role_id' => 1,
            ),
            161 => 
            array (
                'permission_id' => 95,
                'role_id' => 2,
            ),
            162 => 
            array (
                'permission_id' => 95,
                'role_id' => 10,
            ),
            163 => 
            array (
                'permission_id' => 96,
                'role_id' => 1,
            ),
            164 => 
            array (
                'permission_id' => 96,
                'role_id' => 2,
            ),
            165 => 
            array (
                'permission_id' => 97,
                'role_id' => 1,
            ),
            166 => 
            array (
                'permission_id' => 97,
                'role_id' => 2,
            ),
            167 => 
            array (
                'permission_id' => 98,
                'role_id' => 1,
            ),
            168 => 
            array (
                'permission_id' => 98,
                'role_id' => 2,
            ),
            169 => 
            array (
                'permission_id' => 99,
                'role_id' => 1,
            ),
            170 => 
            array (
                'permission_id' => 99,
                'role_id' => 2,
            ),
            171 => 
            array (
                'permission_id' => 100,
                'role_id' => 1,
            ),
            172 => 
            array (
                'permission_id' => 100,
                'role_id' => 2,
            ),
            173 => 
            array (
                'permission_id' => 101,
                'role_id' => 1,
            ), 
            174 => 
            array (
                'permission_id' => 101,
                'role_id' => 2,
            ), 
            175 => 
            array (
                'permission_id' => 102,
                'role_id' => 1,
            ),
            176 => 
            array (
                'permission_id' => 102,
                'role_id' => 2,
            ),
            177 => 
            array (
                'permission_id' => 103,
                'role_id' => 1,
            ),
            178 => 
            array (
                'permission_id' => 103,
                'role_id' => 2,
            ),
            179 => 
            array (
                'permission_id' => 104,
                'role_id' => 1,
            ),
            180 => 
            array (
                'permission_id' => 104,
                'role_id' => 2,
            ),
        ));
        
        
    }
}