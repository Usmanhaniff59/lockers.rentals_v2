<?php

use Illuminate\Database\Seeder;

class ChatTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('chat_types')->delete();
        
        \DB::table('chat_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'type' => 'Introduction Call',
            ),
            1 => 
            array (
                'id' => 2,
                'type' => 'Meeting Arranged',
            ),
            2 => 
            array (
                'id' => 3,
                'type' => 'Follow Up Call',
            ),
            3 => 
            array (
                'id' => 4,
                'type' => 'Sale Complete',
            ),
            4 => 
            array (
                'id' => 5,
                'type' => 'Courtesy Call',
            ),
        ));
        
        
    }
}