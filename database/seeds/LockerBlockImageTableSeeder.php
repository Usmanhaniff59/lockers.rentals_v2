<?php

use Illuminate\Database\Seeder;

class LockerBlockImageTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('locker_block_image')->delete();
        
        \DB::table('locker_block_image')->insert(array (
            0 => 
            array (
                'id' => 1,
                'file_name' => '2 High Block.png',
                'active' => 1,
                'deleted' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'file_name' => '3 High Block.png',
                'active' => 1,
                'deleted' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'file_name' => '4 High Block.png',
                'active' => 1,
                'deleted' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 5,
                'file_name' => '5 High Block.png',
                'active' => 1,
                'deleted' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 6,
                'file_name' => 'outdoor_3.png',
                'active' => 1,
                'deleted' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 7,
                'file_name' => 'outdoor_4.png',
                'active' => 0,
                'deleted' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 8,
                'file_name' => '6 High Block.png',
                'active' => 1,
                'deleted' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 9,
                'file_name' => '10 High Block.png',
                'active' => 1,
                'deleted' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}