<?php

use Illuminate\Database\Seeder;

class SchoolRequestsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('school_requests')->delete();
        
        \DB::table('school_requests')->insert(array (
            0 => 
            array (
                'id' => 18,
                'user_id' => 3,
                'school_name' => 'ggg',
                'line_1_address' => '12-a green avenu muslim town lht',
                'line_2_address' => 'aa',
                'town' => 'lahore',
                'post_code' => '54000',
                'country' => 'Pakistan',
                'date_requested' => '2019-11-22',
                'requests' => '3',
                'response' => '0',
                'date_responded' => NULL,
                'created_at' => '2019-11-22 02:39:27',
                'updated_at' => '2019-11-22 03:16:41',
            ),
            1 => 
            array (
                'id' => 19,
                'user_id' => 35,
                'school_name' => 'ggg',
                'line_1_address' => '12-a green avenu muslim town lht',
                'line_2_address' => 'aa',
                'town' => 'lahore',
                'post_code' => '54000',
                'country' => 'Pakistan',
                'date_requested' => '2019-11-22',
                'requests' => '1',
                'response' => '0',
                'date_responded' => NULL,
                'created_at' => '2019-11-22 09:16:09',
                'updated_at' => '2019-11-22 09:16:09',
            ),
            2 => 
            array (
                'id' => 20,
                'user_id' => 1,
                'school_name' => 'ggg',
                'line_1_address' => '12-a green avenu muslim town lht',
                'line_2_address' => 'aa',
                'town' => 'lahore',
                'post_code' => '54000',
                'country' => 'Pakistan',
                'date_requested' => '2020-01-17',
                'requests' => '1',
                'response' => '0',
                'date_responded' => NULL,
                'created_at' => '2020-01-17 08:06:26',
                'updated_at' => '2020-01-17 08:06:26',
            ),
            3 => 
            array (
                'id' => 21,
                'user_id' => 27003,
                'school_name' => 'gsdgdds',
                'line_1_address' => 'aa',
                'line_2_address' => 'fasdf',
                'town' => 'dd',
                'post_code' => 'dsaff',
                'country' => 'fasdf',
                'date_requested' => '2020-03-19',
                'requests' => '1',
                'response' => '0',
                'date_responded' => NULL,
                'created_at' => '2020-03-19 07:02:38',
                'updated_at' => '2020-03-19 07:02:38',
            ),
            4 => 
            array (
                'id' => 22,
                'user_id' => 319,
                'school_name' => 'aas',
                'line_1_address' => 'ss',
                'line_2_address' => 's',
                'town' => 'ss',
                'post_code' => 'ss',
                'country' => 'ss',
                'date_requested' => '2020-07-10',
                'requests' => '1',
                'response' => '0',
                'date_responded' => NULL,
                'created_at' => '2020-07-10 14:41:56',
                'updated_at' => '2020-07-10 14:41:56',
            ),
        ));
        
        
    }
}