
$('body').on('keyup','#location_search',function(e){

    var location = $('#location_search').val();

    $('#display_locations').html('');
    $('#locker-non-availability').html('');
    $('#next_stage_proceede').html('');
    $('#location_proceed_button').attr("disabled", true);
    $('#location_proceed_button').css("background-color", "");

    $("#location_proceed_button").hover(function() {
        $(this).css("background", "");
    }, function() {
        $(this).css("background", "");

    });
    if(location.length >= 3) {

        $('#next_stage_proceede').html('');
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            url: '/frontend/searchlocation',
            type: 'POST',
            dataType: 'JSON',
            cache: false,
            data: {location: location , _token: CSRF_TOKEN},
            success: function (data) {

                if(data.locations.length != 0) {
                    $('#display_locations').html(data.radio_locations);
                }else{
                    $('#display_locations').html('');
                    $('#locker-non-availability').html('<p style="color: red">Locker not available into user selection, Request for New Locker in your location click on <a href="/frontend/requestavailability">Request Lockers</a> button.</p>\n');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });
    }
});

$('body').on('click','.radio-locations',function(e){

    var location_id = $(this).data('location_id');
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        url: '/frontend/getlocationgroup',
        type: 'POST',
        dataType: 'JSON',
        cache: false,
        data: {location_id: location_id , _token: CSRF_TOKEN},
        success: function (data) {

            var str = '';
            str+='<div class="card" >';
            str+='<div class="card-body">';
            str+='<h6 class="card-title text-left">'+ data.location_group.name +'</h6>';
            str+='<h6 class="card-title text-left"><strong>'+data.name +'</strong></h6>';
            str+='<p class="text-left text-muted"  ><b>Address : </b><small>'+ data.street +' '+ data.city +','+ data.country +'</small></p>';
            str+='</div>';
            str+='</div>';
            $('#next_stage_proceede').html(str);

            $('#location_proceed_button').attr("disabled", false);
            $("#location_proceed_button").css({"background": "#F5B54A"});
            $("#location_proceed_button").hover(function() {
                $(this).css("background", "#13a456");
            }, function() {
                $(this).css("background", "#F5B54A");

            });

            if($('#edit_cms_switch').is(":checked")){
                $('.get_text').prop("disabled", false);
            }else{
                $('.get_text').prop("disabled", true);
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
        }
    });



});

$('body').on('click','#location_proceed_button',function(e){

    var location_id = $('#location_proceed_button').data('location_id');
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        url: '/frontend/getlocationgroup',
        type: 'POST',
        dataType: 'JSON',
        cache: false,
        data: {location_id: location_id , _token: CSRF_TOKEN},
        success: function (data) {

            var str = '';
            str+='<div class="card" style="width: 18rem;">';
            str+='<div class="card-body">';
            str+='<h6 class="card-title text-left"><b>Location Group : </b>'+ data.location_group.name +'</h6>';
            str+='<h6 class="card-title text-left"><b>Company : </b>'+data.name +'</h6>';
            str+='<p class="card-text text-muted text-left" ><b>Address : </b>'+ data.address +'</p>';
            str+='</div>';
            str+='</div>';
            $('#next_stage_proceede').html(str);

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
        }
    });

});