
$( document ).ready(function() {
    checkCookie('1');
    $("body").on('click','.close-cookies-button',function () {
        $(".accept-cookies-main").remove();
    });

    // $('.get_text').prop("disabled", true);

    if($('#edit_cms_switch').is(":checked")){
        // $('.get_text').prop("disabled", false);
        $('.assign_class').addClass("get_text");
    }else{
        // $('.button_text').removeClass("get_text");
        $('.assign_class').removeClass("get_text");
        // $('.get_text').prop("disabled", true);
    }
});
/*******set browser cookies************/
function setCookie(cname,cvalue,exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie(load) {
    var ip_address=getCookie("ip_address");
    var userAgent=getCookie("userAgent");
    if (userAgent!=="" && ip_address!=="") {
        $(".accept-cookies-main").hide();
    } else {
        if (load =="0"){
            var Sys = {};
            var ua = navigator.userAgent.toLowerCase();
            var s;
            (s = ua.match(/msie ([\d.]+)/)) ? Sys.ie = s[1] : (s = ua.match(/firefox\/([\d.]+)/)) ? Sys.firefox = s[1] :
                (s = ua.match(/chrome\/([\d.]+)/)) ? Sys.chrome = s[1] :
                    (s = ua.match(/opera.([\d.]+)/)) ? Sys.opera = s[1] :
                        (s = ua.match(/version\/([\d.]+).*safari/)) ? Sys.safari = s[1] : 0;
            if (Sys.ie) setCookie("userAgent", Sys.ie, 365);
            if (Sys.firefox) setCookie("userAgent", Sys.firefox, 365);
            if (Sys.chrome) setCookie("userAgent", Sys.chrome, 365);
            if (Sys.opera) setCookie("userAgent", Sys.opera, 365);
            if (Sys.safari)  setCookie("userAgent", Sys.safari, 365);
            $.get("https://ipinfo.io", function(response) {
                setCookie("ip_address", response.ip, 365);
            }, "jsonp");
            $(".accept-cookies-main").hide();
        }else{
            $(".accept-cookies-main").show();
        }
    }
}


$('body').on('change','#edit_cms_switch',function(e){
    e.preventDefault();
    var switchCMS = '';
    if($(this).is(":checked")){
        $('.assign_class').addClass("get_text");
        // $('.button_text').addClass("get_text");
        // $('. ').prop("disabled", false);
        // $('.slider-title').attr("disabled","enabled");
        switchCMS =true;
    }else{
        // $('.button_text').removeClass("get_text");
        // $('.get_text').prop("disabled", true);
        // $('.slider-title').attr("disabled","disabled");
        $('.assign_class').removeClass("get_text");
        switchCMS =false;
    }
    var token = $("meta[name='csrf-token']").attr("content");
    $.ajax({
        url:"/frontend/switchcms",
        method:"POST",
        data:{switchCMS: switchCMS ,  _token:token},
        success:function(data){
            // console.log(data);

            // location.reload();
        }
    });
});



$('body').on('click','.get_text', function(e){


    var textValue = $(this).data('value');
    var data_id = $(this).data('id');
    $('.note-editable.card-block').html('');
     $(".note-editable.card-block").html(textValue);
    
    $('#modal_input_placeholder').val('');
    var input_placeholder = $(this).data('inputplaceholder');
    if(input_placeholder){
        $('#modal_input_placeholder').val(input_placeholder);
    }



    $('#placeholders').html('');
    var placeholders = $(this).data('placeholders');

    var placeholderValues = $(this).data('placeholderval');
    // alert(placeholders);
    // alert(placeholderValues);
    // alert('place before');
    console.log("placeholders", textValue, data_id, placeholders, placeholderValues, input_placeholder, $('.note-editable.card-block').html());
    if(placeholders) {
        // alert(placeholders);



        var placeholders_array = placeholders.split(',');

        if(placeholders_array.length == 1) {
            // alert('single');
            $('#placeholders').html('<p style="margin-bottom: 0px">'+ placeholders_array  +' </p>');

        }else{
            // alert('multi');

            var placeholderValues_array = placeholderValues.split(',');

            var token = $("meta[name='csrf-token']").attr("content");
            $.ajax({
                url:"/frontend/multiplaceholders",
                method:"POST",
                data:{data_id:data_id, _token:token},
                success:function(data){
                    console.log(data);
                    var str ='';
                    var placeholders = [];
                    $.each(data ,function (index , val) {
                        str+='<p style="margin-bottom: 0px">'+val.placeholder+' </p>';
                        placeholders.push(val.placeholder)
                    });
                    $('#placeholders').html(str);
                    // alert(placeholders);
                    // alert(placeholderValues);
                    $('#modal_placeholders').val(placeholders);
                    $('#modal_placeholder_values').val(placeholderValues);
                    // if(steps == 'yes') {
                    //     alert('yes inner');

                    // }

                }
            });
        }
        $('#modal_placeholders').val(placeholders);
        $('#modal_placeholder_values').val(placeholderValues);

    }
    // var steps = ;

    var navitems =  $(this).hasClass('navigation')
    console.log(navitems);

    $('#modal_steps').val($(this).data('steps'));

    e.preventDefault();

    $("#modal_text_id").val(data_id);

    // $("#body-text").summernote('code', '');

    //$("#body-text").summernote('code', textValue);

    $('.summernote').summernote({
        height: 100,
        tabsize: 2,
        followingToolbar: true,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['fontname', ['fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'span']],
            ['insert', ['link']],
            ['view', ['fullscreen', 'help']],
        ],
        popover: {
            link: [
                ['link', ['linkDialogShow', 'unlink']]
            ],
        },
        callbacks: {
            onInit: function() {
                console.log('Summernote is launched');
                $('.note-editable.card-block').html('');
                $(".note-editable.card-block").html(textValue);
                //$('.note-editable.card-block').text(textValue)
            },
            onChange: function(contents, $editable) {
                var written = stripHtml(contents);
                var pasted = stripHtml($('.note-editable.card-block').html());
                console.log(navitems)
                console.log(written);
                if(navitems){
                    if(written.length > 20){
                        var x = pasted.substring(0,pasted.length - 1);
                        $('.note-editable.card-block').html('<p>'+x+'</p>');
                    }
                }
            }
        }
    });

    $('#myModal').modal('show');

});


function stripHtml(html)
{
    var tmp = document.createElement("DIV");
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || "";
}

// $('#myModal').on('hidden.bs.modal', function (e) {
//     $('.summernote').summernote('destroy');
// })

$('#myModal .save-button').click(function() {
    var data_id = $('#modal_text_id').val();
    var data_value = $('#body-text').summernote('code');
    var token = $("meta[name='csrf-token']").attr("content");
    var placeholders = $('#modal_placeholders').val();
    var placeholders_array = placeholders.split(',');
    var placeholderValues = $('#modal_placeholder_values').val();
    var placeholderValues_array = placeholderValues.split(',');
    var replaced_data_value = data_value;
    // alert(placeholders_array);
    // alert(placeholderValues_array);
    if(placeholders_array.length > 0) {
        if(placeholders_array.length == 1) {
            // alert(placeholders_array.length);
            replaced_data_value = replaced_data_value.replace(placeholders, placeholderValues);
        }else{
            // alert('multi');
            var str = '';
            $.each(placeholders_array, function (index, val) {
                str += '<span style="margin-bottom: 0px">'+ val.placeholder +'</span>';
                // placeholders.push(val.placeholder);
            });
            $('#placeholders').html(str);
            // alert(placeholders_array);
            $.each(placeholders_array, function (index, val) {
                // alert(replaced_data_value);
                // alert(index);
                // alert(val);
                // alert(placeholderValues_array[index]);
                replaced_data_value = replaced_data_value.replace(val, placeholderValues_array[index]);
                // alert(replaced_data_value);
            });
        }
    }
    $.ajax({
        url:"/frontend/changeText",
        method:"POST",
        data:{data_id:data_id, data_value:data_value, _token:token},
        success:function(data){
            // alert(replaced_data_value);
            var modal_input_placeholder = $('#modal_input_placeholder').val();
            var modal_steps = $('#modal_steps').val();
            if(modal_input_placeholder){
                $("[data-id="+data_id+"]").attr("placeholder",replaced_data_value);
                $("[data-id="+data_id+"]").data('value', data_value);
            }else if(modal_steps) {
                $("[data-id=" + data_id + "]").html(replaced_data_value+'<div class="after-title-step2"></div><div class="after-title-step3"></div><div class="after-title-step4"></div><div class="after-title-step5"></div>');
                $("[data-id=" + data_id + "]").data('value', data_value);
            }else{
                $("[data-id=" + data_id + "]").html(replaced_data_value);
                $("[data-id=" + data_id + "]").data('value', data_value);
            }
            // location.reload();
        }
    });
    $('.summernote').summernote('destroy');
    // $('#myModal')[0].reset();
});


