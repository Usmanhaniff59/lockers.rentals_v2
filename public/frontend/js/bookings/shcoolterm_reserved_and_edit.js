
function check_need_validation(counter) {
    var  block_id = $.trim($('#block_id'+counter).val());


    if(block_id!='') {

        var company_id = $("input[name=company_id]").val();
        var locker_id = $.trim($('#locker_id' + counter).val());

        var school_academic_year_id = $('#shcoolAcademicYear' + counter).val();
        var shcool_year_id = $('#shcoolYear' + counter).val();

        $('#enterEndDateValidation' + counter).addClass('d-none');
        $('#endValidation' + counter).addClass('d-none');
        $('#hold_lockerValidation' + counter).addClass('d-none');
        $('#shcoolAcademicYearValidation' + counter).addClass('d-none');
        $('#shcoolYearValidation' + counter).addClass('d-none');
        $('#needBlockYearValidation' + counter).val(false);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({

            url: '/admin/reserved/lockers/check/same/school/year',

            type: 'POST',

            dataType: 'JSON',

            cache: false,

            data: {
                company_id: company_id,
                block_id: block_id,
                locker_id: locker_id,
                school_academic_year_id: school_academic_year_id,
                shcool_year_id: shcool_year_id
            },

            success: function (data) {

                if (data.status == 'not_exists') {

                    $('#needBlockYearValidation' + counter).val(true);

                    var placeholdervaluesArray = [];

                    placeholdervaluesArray[0] = data.splitName[0].trim();

                    placeholdervaluesArray[1] = data.splitName[1].trim();

                    placeholdervaluesArray[2] = data.company_name;

                    placeholdervaluesArray[3] = 'Request New Locker';

                    var textValue = $('#endValidation' + counter).data('value');

                    var placeholders = $('#endValidation' + counter).data('placeholders');

                    var placeholders_array = placeholders.split(',');

                    var replaced_data_value = textValue;

                    $.each(placeholders_array, function (index, placeholder) {

                        replaced_data_value = replaced_data_value.replace(placeholder, placeholdervaluesArray[index]);

                    });

                    $('#endValidation' + counter).html(replaced_data_value);

                    var placeholdervalues = placeholdervaluesArray.toString();

                    $('#endValidation' + counter).data('placeholderval', placeholdervalues);

                    $('#endValidation' + counter).removeClass('d-none');


                    if ($('#hold_locker').is(":checked") == true) {
                        var start_next_month = data.splitName[0].trim().split(',');
                        var start_next = new Date(data.start_next);
                        var start_year = start_next.getFullYear();
                        placeholdervaluesArray[0] = start_next_month[0] + ', ' + start_year;

                        var end_next_month = data.splitName[1].trim().split(',');
                        var end_next = new Date(data.end_next);
                        var end_year = end_next.getFullYear();

                        placeholdervaluesArray[1] = end_next_month[0] + ', ' + end_year;

                        placeholdervaluesArray[2] = data.company_name;

                        placeholdervaluesArray[3] = 'Request New Locker';

                        var textValue = $('#hold_lockerValidation' + counter).data('value');

                        var placeholders = $('#hold_lockerValidation' + counter).data('placeholders');

                        var placeholders_array = placeholders.split(',');

                        var replaced_data_value = textValue;

                        $.each(placeholders_array, function (index, placeholder) {

                            replaced_data_value = replaced_data_value.replace(placeholder, placeholdervaluesArray[index]);

                        });

                        $('#hold_lockerValidation' + counter).html(replaced_data_value);

                        var placeholdervalues = placeholdervaluesArray.toString();

                        $('#hold_lockerValidation' + counter).data('placeholderval', placeholdervalues);

                        $('#hold_lockerValidation' + counter).removeClass('d-none');
                    }
                    $("#daterange" + counter).val(false);
                    // shcoolYearValidation(counter);
                } else {

                    $("#daterange" + counter).val(true);

                }
            },

            error: function (jqXHR, textStatus, errorThrown) {



            }

        });
    }else{
        shcoolYearValidation(counter);
    }
}

var previous_school_academic_year;



$('body').on('click','#school-academic-year-modal-keep-my-dates-button',function(e){

    var  school_academic_year_previous_value = $('#school_academic_year_previous_value').val();
    var  school_academic_year_modal_counter = $('#school_academic_year_modal_counter').val();


    $('#shcoolAcademicYear'+school_academic_year_modal_counter).val(school_academic_year_previous_value);

});


$('body').on('click','#school-academic-year-modal-change-date-button',function(e){

    var  school_academic_year_changed_value = $('#school_academic_year_changed_value').val();
    var  school_academic_year_modal_counter = $('#school_academic_year_modal_counter').val();

    var  sale_id = $('#sale_academic_id_modal').val();

    $('#block_id'+school_academic_year_modal_counter).val('');
    $('#locker_id'+school_academic_year_modal_counter).val('');

    $('#shcoolAcademicYear'+school_academic_year_modal_counter).val(school_academic_year_changed_value);
    $('#changeDateStatus'+school_academic_year_modal_counter).val(true);
    $('#changeShcoolAcademicYear'+school_academic_year_modal_counter).val(true);
    $('#needBlockYearValidation'+school_academic_year_modal_counter).val(true);


    shcoolYearValidation(school_academic_year_modal_counter);

    lost_booking(sale_id);




});







$('body').on('click','#school-year-modal-keep-my-dates-button',function(e){

    var  school_year_previous_value = $('#school_year_previous_value').val();
    var  school_year_modal_counter = $('#school_year_modal_counter').val();

    $('#shcoolYear'+school_year_modal_counter).val(school_year_previous_value);

});


$('body').on('click','#school-year-modal-change-date-button',function(e){

    var  school_year_changed_value = $('#school_year_changed_value').val();
    var  school_year_modal_counter = $('#school_year_modal_counter').val();

    var  sale_id = $('#sale_id_modal').val();

    $('#block_id'+school_year_modal_counter).val('');
    $('#locker_id'+school_year_modal_counter).val('');

    $('#shcoolYear'+school_year_modal_counter).val(school_year_changed_value);
    $('#changeDateStatus'+school_year_modal_counter).val(true);
    $('#changeShcoolAcademicYear'+school_year_modal_counter).val(true);
    $('#needBlockYearValidation'+school_year_modal_counter).val(true);

     shcoolYearValidation(school_year_modal_counter);

      lost_booking(sale_id);


});





function lost_booking(sale_id) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({

        url: '/admin/reserved/lockers/dateChangeLostLocker',

        type: 'POST',

        dataType: 'JSON',

        cache: false,

        data: {sale_id: sale_id},

        success: function (data) {

        },

        error: function (jqXHR, textStatus, errorThrown) {



        }

    });
}