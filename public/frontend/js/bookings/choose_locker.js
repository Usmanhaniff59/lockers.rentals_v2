
$( window ).load(function() {
    sessionStorage.setItem("selected_dropdown_current_tab", 0);
    setTimeout(function () {
        //do something once
        $(".blocks").change();

        getNewLockers();

    }, 1500);


});
function reloadChooseLockerPage(){
    // window.location.reload();
     location.reload();
  
    // getNewLockers();
     // document.location.href = href;
}
function getNewLockers(){

    var array_of_sale_ids = [];
    $('.blocks').each( function(i,e) {
        array_of_sale_ids.push($(e).attr('data-sale_id'))
    });

    var index_of_sale_ids = sessionStorage.getItem("selected_dropdown_current_tab");
    var sale_id = array_of_sale_ids[index_of_sale_ids];
    var start = sessionStorage.getItem("selected_dropdown_start");
    var end = sessionStorage.getItem("selected_dropdown_end");
    var rate = sessionStorage.getItem("selected_dropdown_rate");
    var tax = sessionStorage.getItem("selected_dropdown_tax");
    var block_id = $('.blocks').val();


    var selected_child = $('.selected-child').attr('id');


    var counter = parseInt(sessionStorage.getItem("selected_dropdown_current_tab")) + 1;
    var hold_locker = sessionStorage.getItem("selected_dropdown_hold_locker");

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    // var childcount = "{{ count($child_blocks) }}";
    var childcount = $('#child_counter').val();


    var storedArray = JSON.parse(sessionStorage.getItem("selected_lockers"));

    console.log(storedArray,'storedArray');
    console.log(typeof storedArray,'storedArray');

    if(storedArray != null) {

        for (var i=0 ; i < childcount ; i++){
           var locker = storedArray[i];

            if (typeof locker !== "undefined" && locker !==null) {

               $('#tab-pills'+i).css('color','white');
               $('#tab-pills'+i).css('background-color','#66ff66');
           }

        }
         locker_id = storedArray[counter - 1];
        console.log(locker_id,'locker_id');
        var hide_show_locker_status = $('#hide_show_locker_status').val();
        $.ajax({

            url: '/frontend/getlockers',

            type: 'POST',

            dataType: 'JSON',

            cache: false,

            data: {
                block_id: block_id,
                sale_id: sale_id,
                start: start,
                end: end,
                rate: rate,
                tax: tax,
                counter: counter,
                hold_locker: hold_locker,
                selected_lockers: locker_id,
                _token: CSRF_TOKEN
            },

            success: function (data) {
                    $(".loader").hide(); 
                    jQuery('body').css('opacity', '1');
                if(data == 'home_page'){
                    document.location.href = "/";
                }
                $('#display_lockers' + counter).html(data);
                $("#locker_table" + counter).each(function () {
                    var $this = $(this);
                    var newrows = [];
                    $this.find("tr").each(function () {
                        var i = 0;
                        $(this).find("td").each(function () {
                            i++;
                            if (newrows[i] === undefined) {
                                newrows[i] = $("<tr></tr>");
                            }
                            newrows[i].append($(this));
                        });
                    });
                    $this.find("tr").remove();
                    $.each(newrows, function () {
                        $this.append(this);
                    });
                    if (hide_show_locker_status == 'Grid Hidden') {
                        $('.red-lockers').addClass('d-none');

                    }

                    return false;
                });

            },

            error: function (jqXHR, textStatus, errorThrown) {



            }

        });
    }
}

var count_time = $('#timeDifference').val();

var timer_counter = setInterval(timer, 1000); //1000 will  run it every 1 second

function timer() {

    count_time = count_time - 1;

    if (count_time == -1) {
        clearInterval(timer_counter);
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var sale_id = $('#sale_id').val();
        $.ajax({

            url: '/frontend/expireBooking',

            type: 'POST',

            dataType: 'JSON',

            cache: false,

            data: {sale_id: sale_id, _token: CSRF_TOKEN},

            success: function (data) {
                document.location.href = "/";
            },
            error: function (jqXHR, textStatus, errorThrown) {

            }
        });
        return;
    }

    var seconds = count_time % 60;
    var minutes = Math.floor(count_time / 60);
    // var hours = Math.floor(minutes / 60);
    minutes %= 60;
    // hours %= 60;
    if(minutes >= 10 ){
        $("#timer-card").css("background-color", "#99BDBC");
    }
    if(minutes < 10){
        $("#timer-card").css("background-color", "#F6B929");
    }
    if(minutes < 5){
        $("#timer-card").css("background-color", "#AA0911");
    }
    if(seconds < 10){
        seconds =  ('0' + seconds).slice(-2);
    }
    if(minutes < 10){
        minutes =  ('0' + minutes).slice(-2);
    }
    var placeholder = $('#timer').data('placeholders');

    var data = $('#timer').data('value');
    var time = minutes +":"+ seconds;
    replaced_data_value = data.replace(placeholder, time);
    document.getElementById("timer").innerHTML =  replaced_data_value; // watch for spelling
    $('#timer').data('placeholderval',time);
}

$(function () {

    $('.tab-show').on('click', function(){

        var array = [];
        var data = $('.blocks').each( function(i,e) {

            array.push($(e).val());
        });

        var current_tab = $(this).attr('id').substr($(this).attr('id').length - 1);

// console.log(current_tab,'current_tab');
// console.log(array[current_tab],'array[current_tab]');
        sessionStorage.setItem("selected_dropdown_current_tab", current_tab);
        if(array[current_tab] != ''){
            // getNewLockers();
        }else{

            count = parseInt(current_tab +1);

            $("#locker_table"+count).addClass("d-none");
            // getNewLockers();
        }
    });

    $('body').on('change', '.blocks', function (e) {
      
        // var  start = $('input('start_date').val();

        var counter = $(this).data('counter');
        $(".show_locker_image").html('');
        $('#booked_block' + counter).addClass('d-none');
        var block = $(this).val();
        if(block!=undefined && block!=null && block!='')
        {
                 $(".loader").show(); 
                
        jQuery('body').css('opacity', '0.1');

        block = block.split("_");
        var block_id = block[0];
        var rate = block[1];
        var tax = block[2];
        var sale_id = $(this).data('sale_id');

        var start = $(this).data('start');
        var end = $(this).data('end');

        var hold_locker = $(this).data('hold_locker');

        sessionStorage.setItem("selected_dropdown_block_id", block_id);
        sessionStorage.setItem("selected_dropdown_sale_id", sale_id);
        sessionStorage.setItem("selected_dropdown_start", start);
        sessionStorage.setItem("selected_dropdown_end", end);
        sessionStorage.setItem("selected_dropdown_rate", rate);
        sessionStorage.setItem("selected_dropdown_tax", tax);
        sessionStorage.setItem("selected_dropdown_counter", counter);
        sessionStorage.setItem("selected_dropdown_hold_locker", hold_locker);



        $('#display_lockers' + counter).html('');
        var hide_show_locker_status = $('#hide_show_locker_status').val();

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.ajax({

            url: '/frontend/getlockers',

            type: 'POST',

            dataType: 'JSON',

            cache: false,

            data: {
                block_id: block_id,
                sale_id: sale_id,
                start: start,
                end: end,
                rate: rate,
                tax: tax,
                counter: counter,
                hold_locker: hold_locker,
                _token: CSRF_TOKEN
            },

            success: function (data) {
                    $(".loader").hide(); 
        jQuery('body').css('opacity', '1');
                //if(data == 'home_page'){
                    // document.location.href = "/";
                //}
               
                if(data == 'home_page'){
                    var reload_page='<p>Oops! something went wrong. Please, refresh.</p><a href="javascript:void(0)" class="green-button-proceed" onclick="reloadChooseLockerPage()">Refresh</a>';
                    $("#display_lockers1").html(reload_page);                    
                    // document.location.href = "/";
                  
					return 0;
                }else{
                    if(data == 'booked') {
                        $('#booked_block' + counter).removeClass('d-none');
                    }else{
                        $('#display_lockers' + counter).html(data);
                        $("#locker_table" + counter).each(function () {
                            var $this = $(this);
                            var newrows = [];
                            $this.find("tr").each(function () {
                                var i = 0;
                                $(this).find("td").each(function () {
                                    i++;
                                    if (newrows[i] === undefined) {
                                        newrows[i] = $("<tr></tr>");
                                    }
                                    newrows[i].append($(this));
                                });
                            });
                            $this.find("tr").remove();
                            $.each(newrows, function () {
                                $this.append(this);
                            });
    
                            if (hide_show_locker_status == 'Grid Hidden') {
                                $('.red-lockers').addClass('d-none');
                                $('.red-lockers').closest("td").addClass('d-none');
    
                            }
                            $(".show_locker_image").html($(".locker_image").html());
                            return false;
                        });
                    }
                }

                

            },

            error: function (jqXHR, textStatus, errorThrown) {



            }

        });
    }


    });

    $('body').on('click', '.reset-booking', function (e) {



        var sale_id = $(this).data('sale_id');

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.ajax({

            url: '/frontend/resetSale',

            type: 'POST',

            dataType: 'JSON',

            cache: false,

            data: {sale_id: sale_id, _token: CSRF_TOKEN},

            success: function (data) {
                document.location.href = "/";

            },

            error: function (jqXHR, textStatus, errorThrown) {



            }

        });


    });

    $('body').on('click', '#show_all_lockers', function (e) {
        e.preventDefault();
        var lockers_status = 'Grid Show';

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.ajax({

            url: '/frontend/showLockersStatus',

            type: 'POST',

            dataType: 'JSON',

            cache: false,

            data: {lockers_status: lockers_status, _token: CSRF_TOKEN},

            success: function (data) {
                if(data == 'home_page'){
                    document.location.href = "/";
                }


                $('#hide_booked_lockers_title').removeClass('d-none');
                $('#show_all_lockers_title').addClass('d-none');
                $('.show-hide-locker-box').css("background-color", "black");
                $('#show_all_lockers').addClass('d-none');
                $('#hide_booked_lockers').removeClass('d-none');
                $('.red-lockers').removeClass('d-none');
                $('.red-lockers').closest("td").removeClass('d-none');
                $('#hide_show_locker_status').val(lockers_status);

            },

            error: function (jqXHR, textStatus, errorThrown) {



            }

        });


    });

    $('body').on('click', '#hide_booked_lockers', function (e) {
        e.preventDefault();

        var lockers_status = 'Grid Hidden';

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.ajax({

            url: '/frontend/showLockersStatus',

            type: 'POST',

            dataType: 'JSON',

            cache: false,

            data: {lockers_status: lockers_status, _token: CSRF_TOKEN},

            success: function (data) {
                if(data == 'home_page'){
                    document.location.href = "/";
                }
                $('#show_all_lockers_title').removeClass('d-none');
                $('#hide_booked_lockers_title').addClass('d-none');
                $('.show-hide-locker-box').css("background-color", "#f5b54a");
                $('#hide_booked_lockers').addClass('d-none');
                $('.red-lockers').addClass('d-none');
                $('.red-lockers').closest("td").addClass('d-none');
                $('#show_all_lockers').removeClass('d-none');
                $('#hide_show_locker_status').val(lockers_status);

            },

            error: function (jqXHR, textStatus, errorThrown) {



            }

        });


    });

    $('body').on('click', '#proceed_button', function (e) {

        e.preventDefault();
        $(".loader").show(); 
        jQuery('body').css('opacity', '0.1');
        var sale_id = $('#sale_id').val();

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            url: '/frontend/validateSale',

            type: 'POST',

            dataType: 'JSON',

            cache: false,

            data: {sale_id: sale_id, _token: CSRF_TOKEN},

            success: function (data) {
                if(data == 'home_page'){
                    document.location.href = "/";
                }
                var loop_counter = 0;
                var child_counter = $('#child_counter').val();

                $.each(data.bookings, function (index, val) {

                    if (val.locker_id == null || val.block_id == null) {

                        $('.tab-show').removeClass('active show');
                        // $('.validation_text').addClass('d-none');

                        const  existed_lockers = data.existed_lockers;

                        var  sale_id = parseInt(val.id);

                        const sale_index = existed_lockers.indexOf(sale_id);


                        if (sale_index > -1) {
                            $('#locker_already_exists_msg'+ index).removeClass('d-none');

                            $('#tabValidation'+ index).addClass('d-none');
                        }else{
                            $('#tabValidation'+ index).removeClass('d-none');
                            $('#locker_already_exists_msg'+ index).addClass('d-none');

                        }

                        if (data.selected_lockers_db === undefined || data.selected_lockers_db.length == 0) {

                        }else{
                            const  selected_lockers = JSON.parse(sessionStorage.getItem("selected_lockers"));

                            var  locker_id = parseInt(data.selected_lockers_db[index]);
                            const locker_index = selected_lockers.indexOf(locker_id);
                            if(locker_index > -1){
                                selected_lockers.splice(locker_index, 1);
                            }
                            sessionStorage.setItem("selected_lockers", JSON.stringify(selected_lockers));

                        }

                        $('#tab-pills' + index).addClass('active show');
                        $('#tab' + index).addClass('active show');

                        index++;
                        $('#blocks' + index).focus();


                        if(child_counter == index )
                            getNewLockers();
                    } else {

                        $('#tabValidation'+ index).addClass('d-none');
                        $('#locker_already_exists_msg'+ index).addClass('d-none');

                        loop_counter++;

                    }
                });
                if (loop_counter == data.count) {
                        $(".loader").hide(); 
                    jQuery('body').css('opacity', '1');
                    $('#SchoolBookingForm').submit();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                 $(".loader").hide(); 
        jQuery('body').css('opacity', '1');
            }
        });

    });
});