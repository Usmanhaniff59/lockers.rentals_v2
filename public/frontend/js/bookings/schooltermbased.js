
function shcoolYearValidation(counter) {

    var total_child = $('#child_count').val();

    $('#enterEndDateValidation'+counter).addClass('d-none');
    $('#endValidation'+counter).addClass('d-none');
    $('#hold_lockerValidation'+counter).addClass('d-none');
    $('#shcoolAcademicYearValidation'+counter).addClass('d-none');
    $('#shcoolYearValidation'+counter).addClass('d-none');
    var  shcoolAcademicYearId = $('#shcoolAcademicYear'+counter).val();
    var  shcoolYearId = $('#shcoolYear'+counter).val();


    if(shcoolAcademicYearId.length == 0 ){

        $("#daterange"+counter).val(false);
        $('#shcoolAcademicYearValidation'+counter).removeClass('d-none');
        $('#shcoolAcademicYear'+counter).focus();
        if( $('#form_submittion').val() == 'true') {
            form_validation();
        }

    }else if(shcoolYearId.length == 0 ){

        $("#daterange"+counter).val(false);
        $('#shcoolYearValidation'+counter).removeClass('d-none');
        if( $('#form_submittion').val() == 'true') {
            form_validation();
        }

    }else if(shcoolYearId.length != 0 && shcoolAcademicYearId.length != 0){

        var company_id = $("input[name=company_id]").val();
        var school_name = $("input[name=school_name]").val();
        if($('#hold_locker').is(":checked") == true){
            var hold_locker= true;
        }else{
            var hold_locker= false;
        }

        var placeholdervaluesArray = [];

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({

            url: '/frontend/lockerAvailabilityCheckSchoolYear',

            type: 'POST',

            dataType: 'JSON',

            cache: false,

            data: {shcoolYearId: shcoolYearId,shcoolAcademicYearId: shcoolAcademicYearId, company_id: company_id, hold_locker: hold_locker},

            success: function (data) {


                if (data.validation_error == true) {

                    placeholdervaluesArray[0] = data.splitName[0].trim();

                    placeholdervaluesArray[1] = data.splitName[1].trim();

                    placeholdervaluesArray[2] = school_name;

                    placeholdervaluesArray[3] = 'Request New Locker';

                    var textValue = $('#endValidation' + counter).data('value');

                    var placeholders = $('#endValidation' + counter).data('placeholders');

                    var placeholders_array = placeholders.split(',');

                    var replaced_data_value = textValue;

                    $.each(placeholders_array, function (index, placeholder) {

                        replaced_data_value = replaced_data_value.replace(placeholder, placeholdervaluesArray[index]);

                    });



                    $('#endValidation' + counter).html(replaced_data_value);

                    var placeholdervalues = placeholdervaluesArray.toString();

                    $('#endValidation' + counter).data('placeholderval', placeholdervalues);

                    $('#endValidation' + counter).removeClass('d-none');



                    if($('#hold_locker').is(":checked") == true){

                       
                        placeholdervaluesArray[0] = data.splitName[0].trim();

                        placeholdervaluesArray[1] = data.splitName[1].trim();

                        placeholdervaluesArray[2] = school_name;

                        placeholdervaluesArray[3] = 'Request New Locker';

                        var textValue = $('#hold_lockerValidation' + counter).data('value');

                        var placeholders = $('#hold_lockerValidation' + counter).data('placeholders');

                        var placeholders_array = placeholders.split(',');



                        var replaced_data_value = textValue;

                        $.each(placeholders_array, function (index, placeholder) {

                            replaced_data_value = replaced_data_value.replace(placeholder, placeholdervaluesArray[index]);
                        });

                        console.log(replaced_data_value);
                 

                        $('#hold_lockerValidation' + counter).html(replaced_data_value);

                        var placeholdervalues = placeholdervaluesArray.toString();

                        $('#hold_lockerValidation' + counter).data('placeholderval', placeholdervalues);

                        $('#hold_lockerValidation' + counter).removeClass('d-none');
                    }


                    $("#daterange" + counter).val(false);

                } else {
                    $("#daterange" + counter).val(true);
                }

                if( total_child == counter && $('#form_submittion').val() == 'true'){
                    form_validation();

                }

            },

            error: function (jqXHR, textStatus, errorThrown) {
                $('#check_available_button').prop('disabled', false);


            }

        });
    }


}

function add_child(obj) {

    var count = $('#child_count').val();

    count++;

    $('#child'+count).removeClass('d-none');

    var title =  $('#child_number_title1').data('value');

    $('#child_number_title1').removeClass('d-none');

    $('#child_number_title1').text(title +' '+ 1);

    $('#child_number_title'+count).removeClass('d-none');

    $('#child_number_title'+count).text(title +' '+ count);

    $('#child_count').val(count);

    // $('.holdLocker-validation').addClass('d-none');

    var location_group = $.trim($('#location_group').val());

    if(location_group == 1){

        var years = $.trim($('#school_academic_years_count').val());

        if(years > 1){

            $('#add_child_button').attr('tabindex',(count * 6)+1);
            $('#remove_child_button').attr('tabindex',(count * 6)+2);

        }else{

            $('#add_child_button').attr('tabindex',(count * 5) +1);
            $('#remove_child_button').attr('tabindex',(count * 5)+2);
        }

    }else{
        $('#add_child_button').attr('tabindex',(count * 6)+1);
        $('#remove_child_button').attr('tabindex',(count * 6)+2);
    }


}

function isDate(txtDate) {

    var currVal = txtDate;

    if(currVal == '')

        return false;

    //Declare Regex
    var rxDatePattern = /^(\d{1,2})(\/)(\d{1,2})(\/)(\d{4})$/;

    var dtArray = currVal.match(rxDatePattern); // is format OK?

    if (dtArray == null)

        return false;

    dtDay = dtArray[1];

    dtMonth= dtArray[3];

    dtYear = dtArray[5];

    if (dtMonth < 1 || dtMonth > 12)

        return false;

    else if (dtDay < 1 || dtDay> 31)


        return false;

    else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31)

        return false;

    else if (dtMonth == 2)

    {

        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));

        if (dtDay> 29 || (dtDay ==29 && !isleap))

            return false;

    }

    return true;

}

function isDateTime(txtDate) {

    var currVal = txtDate;

    if(currVal == '')

        return false;

    //Declare Regex

    var rxDatePattern = /^(\d{1,2})(\/)(\d{1,2})(\/)(\d{4})\s(\d{1,2})(\:)(\d{1,2})$/;

    var dtArray = currVal.match(rxDatePattern); // is format OK?

    //alert(dtArray);

    if (dtArray == null)

        return false;

    dtDay = dtArray[1];

    dtMonth= dtArray[3];

    dtYear = dtArray[5];

    if (dtMonth < 1 || dtMonth > 12)

        return false;

    else if (dtDay < 1 || dtDay> 31)

        return false;

    else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31)

        return false;

    else if (dtMonth == 2)

    {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));

        if (dtDay> 29 || (dtDay ==29 && !isleap))

            return false;

    }

    return true;

}

function show_terms() {

    var x = document.getElementById("terms_div");

    if (x.style.display === "none") {

        x.style.display = "block";

    } else {

        x.style.display = "none";

    }

}

function isEmail(email) {

    var regex = /^([a-zA-Z0-9_.+-]{1,20})+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    return regex.test(email);

}

function replaced_email_msg(name,surname,loggedUserEmail){

    // var data_id = $('#login_msg').data('id');
    placeholdervaluesArray = [];
    placeholdervaluesArray[0]= name;
    placeholdervaluesArray[1]= surname;
    placeholdervaluesArray[2]= loggedUserEmail;
    placeholdervaluesArray[3]= 'logout';


    // $.ajaxSetup({
    //     headers: {
    //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //     }
    // });
    //
    // $.ajax({
    //
    //     url:"/frontend/placeholders",
    //
    //     method:"POST",
    //
    //     data:{data_id:data_id},
    //
    //     success:function(data){

    // var placeholder = data.placeholder;

    // var str = $('#login_msg').data('text');

    var textValue = $('#login_msg').data('value');

    var placeholders = $('#login_msg').data('placeholders');

    var placeholders_array = placeholders.split(',');

    var replaced_data_value =textValue;

    $.each(placeholders_array ,function (index , placeholder) {

        if(placeholdervaluesArray[index] == null || placeholdervaluesArray[index] == '') {

            replaced_data_value = replaced_data_value.replace(placeholder, '');
        }else{
            replaced_data_value = replaced_data_value.replace(placeholder, placeholdervaluesArray[index]);
        }

    });

    $('#login_msg').html(replaced_data_value);

    var placeholdervalues = placeholdervaluesArray.toString();

    $('#login_msg').data('placeholderval',placeholdervalues);



}

function voucher_code_validation(code){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    var user_id = $.trim($('#user_id').val());
    var company_id = $.trim($('#company_id').val());
    $.ajax({

        url: '/frontend/voucherCodeValidation',

        type: 'POST',

        dataType: 'JSON',

        cache: false,

        data: {
            code: code,
            user_id: user_id,
            company_id: company_id,
        },

        success: function (data) {

            if(data.status == 'user_not_booked'){
                $('#voucherCodeValidation').removeClass('d-none');
                $('#voucherCodeValidation').text('This voucher can only be used against certain bookings. Please contact support.');
                $('#voucherCodeValidation').focus();
            }
            else if(data.status == 'log_in'){
                $('#voucherCodeValidation').removeClass('d-none');
                $('#voucherCodeValidation').text('Please Login to use a voucher');
                $('#voucherCodeValidation').focus();
            } else if(data.status == 'already_used'){
                $('#voucherCodeValidation').removeClass('d-none');
                $('#voucherCodeValidation').text('This code has already been used. Please try another code');
                $('#voucherCodeValidation').focus();
            }else if(data.status == 'valid_code'){
                $('#voucher_code_id').val(data.code_id);
                $('#SchoolBookingForm').submit();
            }else{

                $('#voucherCodeValidation').removeClass('d-none');
                $('#voucherCodeValidation').text('This voucher code is not valid');
                $('#voucherCodeValidation').focus();

            }

            //

        },

        error: function (jqXHR, textStatus, errorThrown) {


        }

    });

}




$('body').on('dp.change','.datetimepicker_start, .datetimepicker_end',function(e){


    var  counter = $(this).data('counter');
    var x = $(this).next().addClass('d-none');

    $('#endValidation'+counter).addClass('d-none');
    $('#startValidation'+counter).addClass('d-none');
    $('#endDateValidation'+counter).addClass('d-none');

    $('#laterDateValidation'+counter).addClass('d-none');

    var  start_date_unformated = $.trim($("#start_date"+counter).val());
    var start_date_obj =  new Date(start_date_unformated);
    // alert(start_date_obj);

    var  end_date_unformated = $("#end_date"+counter).val();
    // alert(end_date_unformated);
    var end_date_obj = new Date(end_date_unformated);
    // alert(end_date_obj);

    var  start_dateTime_unformated = $.trim($("#start_dateTime"+counter).val());
    var start_dateTime_obj =  new Date(start_dateTime_unformated);
    // alert(start_dateTime_obj);

    var  end_dateTime_unformated = $.trim($("#end_dateTime"+counter).val());
    var end_dateTime_obj = new Date(end_dateTime_unformated);
    // alert(start_dateTime_unformated.length);
    var  company_id = $("input[name=company_id]").val();

    var  school_name = $("input[name=school_name]").val();
    // alert(start_dateTime_unformated.length);

    var placeholdervaluesArray = [];

    if(start_date_unformated.length == 12 && end_date_unformated.length == 12 ) {
        // alert('both dates');

        var start_date = start_date_unformated;

        var end_date = end_date_unformated;
        // alert(start_date);
        // alert(end_date);

        placeholdervaluesArray[0] = start_date_unformated;

        placeholdervaluesArray[1] = end_date_unformated;

        placeholdervaluesArray[2] = school_name;

        placeholdervaluesArray[3] = 'Request New Locker';

        if(start_date_obj > end_date_obj){

            var textValue = $('#laterDateValidation'+counter).data('value');

            var placeholders = $('#laterDateValidation'+counter).data('placeholders');

            var placeholders_array = placeholders.split(',');

            var replaced_data_value =textValue;

            $.each(placeholders_array ,function (index , placeholder) {

                replaced_data_value = replaced_data_value.replace(placeholder, placeholdervaluesArray[index]);

            });

            $('#laterDateValidation'+counter).html(replaced_data_value);

            var placeholdervalues = placeholdervaluesArray.toString();

            $('#laterDateValidation'+counter).data('placeholderval',placeholdervalues);

            $('#laterDateValidation'+counter).removeClass('d-none');
            $('#end_date'+counter).val(' ');
            $("#daterange"+counter).val(false);

            return false;

        }

        // var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({

            url: '/frontend/lockerAvailabilityCheck',

            type: 'POST',

            dataType: 'JSON',

            cache: false,

            data: {start_date: start_date,end_date: end_date,company_id: company_id },

            success: function (data) {



                if(data == true){

                    var textValue = $('#endValidation'+counter).data('value');

                    var placeholders = $('#endValidation'+counter).data('placeholders');

                    var placeholders_array = placeholders.split(',');

                    var replaced_data_value = textValue;

                    $.each(placeholders_array ,function (index , placeholder) {

                        // alert(replaced_data_value);

                        // alert(index);

                        // alert(placeholder);

                        // alert(placeholdervaluesArray[index]);

                        replaced_data_value = replaced_data_value.replace(placeholder, placeholdervaluesArray[index]);

                    });

                    $('#endValidation'+counter).html(replaced_data_value);

                    var placeholdervalues = placeholdervaluesArray.toString();

                    // alert(placeholdervalues);

                    $('#endValidation'+counter).data('placeholderval',placeholdervalues);

                    $('#endValidation'+counter).removeClass('d-none');

                    // $("input[name=daterange]").val(false);

                }else{

                    $("#daterange"+counter).val(true);
                }

            },

            error: function (jqXHR, textStatus, errorThrown) {



            }

        });

    }else if(start_dateTime_unformated.length == 18 && end_dateTime_unformated.length == 18 ) {
        // alert('both');

        // var start = start_dateTime_unformated.split(' ');
        //
        // var start2 = start[0].split('/').reverse().join('-');
        //
        // var start_date = start2 +' '+ start[1];

        var start_date = formatted_date = start_dateTime_obj.getFullYear() + "-" + (start_dateTime_obj.getMonth() + 1) + "-" + start_dateTime_obj.getDate() + " " + start_dateTime_obj.getHours() + ":" + start_dateTime_obj.getMinutes() + ":" + start_dateTime_obj.getSeconds() ;

        // var end = end_dateTime_unformated.split(' ') ;
        //
        // var end2 = end[0].split('/').reverse().join('-');
        //
        // var end_date = end2 +' '+ end[1];
        // var end_date = end_dateTime_unformated;
        // var end_date = end_dateTime_obj;
        var end_date = formatted_date = end_dateTime_obj.getFullYear() + "-" + (end_dateTime_obj.getMonth() + 1) + "-" + end_dateTime_obj.getDate() + " " + end_dateTime_obj.getHours() + ":" + end_dateTime_obj.getMinutes() + ":" + end_dateTime_obj.getSeconds() ;


        placeholdervaluesArray[0] = start_dateTime_unformated;

        placeholdervaluesArray[1] = end_dateTime_unformated;

        placeholdervaluesArray[2] = school_name;

        placeholdervaluesArray[3] = 'Request New Locker';

        // alert('dateTime');
        if(start_dateTime_obj > end_dateTime_obj){

            var textValue = $('#laterDateValidation'+counter).data('value');

            var placeholders = $('#laterDateValidation'+counter).data('placeholders');

            var placeholders_array = placeholders.split(',');

            var replaced_data_value =textValue;

            $.each(placeholders_array ,function (index , placeholder) {

                replaced_data_value = replaced_data_value.replace(placeholder, placeholdervaluesArray[index]);

            });

            $('#laterDateValidation'+counter).html(replaced_data_value);

            var placeholdervalues = placeholdervaluesArray.toString();

            $('#laterDateValidation'+counter).data('placeholderval',placeholdervalues);

            $('#laterDateValidation'+counter).removeClass('d-none');
            $('#end_dateTime'+counter).val(' ');
            $("#daterange"+counter).val(false);
            return false;

        }

        // var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({

            url: '/frontend/lockerAvailabilityCheck',

            type: 'POST',

            dataType: 'JSON',

            cache: false,

            data: {start_date: start_date,end_date: end_date,company_id: company_id },

            success: function (data) {



                if(data == true){

                    var textValue = $('#endValidation'+counter).data('value');

                    var placeholders = $('#endValidation'+counter).data('placeholders');

                    var placeholders_array = placeholders.split(',');

                    var replaced_data_value =textValue;

                    $.each(placeholders_array ,function (index , placeholder) {

                        // alert(replaced_data_value);

                        // alert(index);

                        // alert(placeholder);

                        // alert(placeholdervaluesArray[index]);

                        replaced_data_value = replaced_data_value.replace(placeholder, placeholdervaluesArray[index]);

                    });

                    $('#endValidation'+counter).html(replaced_data_value);

                    var placeholdervalues = placeholdervaluesArray.toString();

                    // alert(placeholdervalues);

                    $('#endValidation'+counter).data('placeholderval',placeholdervalues);

                    $('#endValidation'+counter).removeClass('d-none');

                    // $("input[name=daterange]").val(false);

                }else {
                    $("#daterange"+counter).val(true);
                }

            },

            error: function (jqXHR, textStatus, errorThrown) {



            }

        });

    }
});

$('body').on('click','#signIn-button',function(e){
    e.preventDefault();
    $('.validation_text').addClass('d-none');
    var email = $.trim($('#signInEmail').val());
    var email_validation = isEmail(email);

    var password = $.trim($('#signInPassword').val());

    if ( email_validation == false || password.length <= 4 ) {

        if (email_validation == false) {

            $('#signInEmailValidation').removeClass('d-none');
            $('#signInEmail').focus();

        }

        // var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
        // if(!password.value.match(passw))
        // {
        //  $('#signInPasswordValidation').removeClass('d-none');
        //     $('#signInPassword').focus();
        // }

        if (password.length <= 4) {

            $('#signInPasswordValidation').removeClass('d-none');
            $('#signInPassword').focus();

        }

        return false;
    }

    // var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var token = $('meta[name="csrf-token"]').attr('content');
    // alert(token);
    $.ajax({

        url: '/login',
        type: 'POST',
        dataType: 'JSON',
        cache: false,
        data: {email: email, password: password,_token : token},
        success: function (response) {

            $('meta[name="csrf-token"]').attr('content',response.token);
            $('#csrf_token').val(response.token);
            $('.csrf-token').val(response.token);
            $('#csrf-schooltermbased').val(response.token);
            var subscription = response.user.subscription;
            if (subscription==1){
                $("#futureComunication").attr("checked",true);
                $("#futureComunication").val("1");
            }else{
                $("#futureComunication").val("0");
            }

            if(response.auth == true) {
                $('#user_id').val(response.user.id);
                $('#user_email').val(response.user.email);
                $('#user_name').val(response.user.name);
                $('#user_surname').val(response.user.surname);
                $('#login_request').addClass('d-none');
                $('.login_msg').removeClass('d-none');
                replaced_email_msg(response.user.name,response.user.surname,response.user.email);
                $('#login_msg').removeClass('d-none');
            }
        },
        error: function (jqXHR) {

            var response = $.parseJSON(jqXHR.responseText);
            if(response.message) {
                $('#signInIncorrectValidation').removeClass('d-none');
                // alert(response.message);
            }
        }
    });

});

$('body').on('click','#password_reset_button',function(e){

    e.preventDefault();
    $('.validation_text').addClass('d-none');
    var email = $.trim($('#passwordResetEmail').val());
    var email_validation = isEmail(email);

    if ( email_validation == false ) {

        if (email_validation == false) {

            $('#passwordResetEmailValidation').removeClass('d-none');
            $('#passwordResetEmail').focus();

        }

        return false;
    }

    // var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: '/frontend/userExist',
        type: 'POST',
        dataType: 'JSON',
        cache: false,
        data: {email: email},
        success: function (response) {


            if(response == false) {

                $('#emailExistValidation').removeClass('d-none');
            }else{
                $.ajax({
                    url: '/password/email',
                    type: 'POST',
                    dataType: 'JSON',
                    cache: false,
                    data: {email: email},
                    success: function (response) {


                    },
                    error: function (jqXHR) {

                        var response = $.parseJSON(jqXHR.responseText);
                        if(response.message) {


                        }
                    }
                });
                $('#emailSubmitValidation').removeClass('d-none');
                $('#passwordResetEmail').val('');
                // $('#ResetPassForm').submit();

            }
        },
        error: function (jqXHR) {

            var response = $.parseJSON(jqXHR.responseText);
            if(response.message) {

            }
        }
    });

});

$('body').on('click','#signUp-button',function(e){

    e.preventDefault();
    $('.validation_text').addClass('d-none');
    var signUpFirstName = $.trim($('#signUpFirstName').val());
    var signUpSurname = $.trim($('#signUpSurname').val());
    var email = $.trim($('#signUpEmail').val());
    var email_validation = isEmail(email);

    var password = $.trim($('#signUpPassword').val());

    var confirmPassword = $.trim($('#signUpConfirmPassword').val());
    var signUpPhoneNumber = $.trim($('#signUpPhoneNumber').val());
    var ajax = 1;

    var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
    // var phone_patteren = /^\(\d{3}\)\s*\d{3}(?:-|\s*)\d{4}$/
    // var phone_patteren = /^\d{10,15}$/;
    var phone_patteren =/^\+[0-9]{2}|^\+[0-9]{2}\(0\)|^\(\+[0-9]{2}\)\(0\)|^00[0-9]{2}|^0[0-9]{9}$|[0-9\-\s]{10}$/;

    if ( signUpFirstName.length == 0 || signUpSurname.length == 0 ||email_validation == false || !password.match(passw) || password != confirmPassword || !signUpPhoneNumber.match(phone_patteren) ) {


        if (signUpFirstName.length == 0) {

            $('#signUpFirstNameValidation').removeClass('d-none');
            $('#signUpFirstName').focus();

        }
        if (signUpSurname.length == 0) {

            $('#signUpSurnameValidation').removeClass('d-none');
            $('#signUpSurname').focus();

        }


        if (email_validation == false) {

            $('#signUpEmailValidation').removeClass('d-none');
            $('#signUpEmail').focus();

        }


        if(!password.match(passw))
        {
            $('#signUpPasswordValidation').removeClass('d-none');
            $('#signUpPassword').focus();
        }

        // if (password.length <= 4) {

        //     $('#signUpPasswordValidation').removeClass('d-none');
        //     $('#signUpPassword').focus();

        // }

        if (password != confirmPassword) {

            $('#signUpConfirmPasswordValidation').removeClass('d-none');
            $('#signUpConfirmPassword').focus();
        }

        if (!signUpPhoneNumber.match(phone_patteren)) {

            $('#signUpPhoneNumberValidation').removeClass('d-none');
            $('#signUpPhoneNumber').focus();

        }

        return false;
    }

    // var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({

        url: '/frontend/createAjax',
        type: 'POST',
        dataType: 'JSON',
        cache: false,
        data: {name: signUpFirstName ,surname: signUpSurname , email: email, password: password, password_confirmation: confirmPassword,phone_number: signUpPhoneNumber,ajax : ajax },
        success: function (response) {


            // $('meta[name="csrf-token"]').attr('content',response.token);
            if(response.user_exist == false) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({

                    url: '/login',
                    type: 'POST',
                    dataType: 'JSON',
                    cache: false,
                    data: {email: email, password: password,_token:CSRF_TOKEN},
                    success: function (response) {

                        $('meta[name="csrf-token"]').attr('content', response.token);
                        $('.csrf-token').val(response.token);
                        $('#csrf-schooltermbased').val(response.token);

                        if (response.auth == true) {

                            $('#user_id').val(response.user.id);
                            $('#user_email').val(response.user.email);
                            $('#user_name').val(response.user.name);
                            $('#user_surname').val(response.user.surname);
                            $('#login_request').addClass('d-none');
                            $('.login_msg').removeClass('d-none');
                            replaced_email_msg(response.user.name, response.user.surname, response.user.email);
                            $('#login_msg').removeClass('d-none');

                        }
                    },
                    error: function (jqXHR) {

                        var response = $.parseJSON(jqXHR.responseText);
                        if (response.message) {
                            $('#signInIncorrectValidation').removeClass('d-none');
                            // alert(response.message);
                        }
                    }
                });
            }

            if(response.user_exist == true) {
                $('#user_exist_msg').removeClass('d-none');
                $('#signUpEmail').focus();
            }
            if(response.auth == true) {
                $('#user_id').val(response.user.id);
                $('#user_email').val(response.user.email);
                $('#user_name').val(response.user.name);
                $('#user_surname').val(response.user.surname);
                $('#login_request').addClass('d-none');
                $('.login_msg').removeClass('d-none');
                replaced_email_msg(response.user.name,response.user.surname,response.user.email);
                $('#login_msg').removeClass('d-none');
            }
        },
        error: function (jqXHR) {

            var response = $.parseJSON(jqXHR.responseText);
            if(response.message) {
                // alert(response.message);
            }
        }
    });

});

$('body').on('click','.reset-booking',function(e){

    // var  start = $('input('start_date').val();

    var sale_id = $.trim($(this).data('sale_id'));

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');


    $.ajax({

        url: '/frontend/resetSale',

        type: 'POST',

        dataType: 'JSON',

        cache: false,

        data: {sale_id: sale_id, _token: CSRF_TOKEN},

        success: function (data) {

            document.location.href="/";

        },

        error: function (jqXHR, textStatus, errorThrown) {



        }

    });


});


        $('#hold_locker').prop('checked', true);

        var location_group = $.trim($('#location_group').val());

        var format = '';

        if(location_group == 3){


            format = "DD MMM, YYYY HH:mm";
        }

        else{
            format = "DD MMM, YYYY";

        }

        $('.datetimepicker_start').datetimepicker(
            {
                // inline: true,
                sideBySide:true,

                format: format,

            }

        );

        $('.datetimepicker_end').datetimepicker(
            {
                // inline: true,
                sideBySide:true,
                format: format,

                // minDate: $('.datetimepicker_start').val() === '' ? new Date('1996-01-01') : new Date($('.datetimepicker_start').val()),

            }

        );

        var count = $('#child_count').val();

        if(count>1){
            for(var i=2 ; i<=count; i++){
                $('#child'+i).removeClass('d-none');
                var title =  $('#child_number_title1').data('value');

                $('#child_number_title1').removeClass('d-none');
                $('#child_number_title1').text(title +' '+ 1);

                $('#child_number_title'+i).removeClass('d-none');

                $('#child_number_title'+i).text(title +' '+ i);


                if(location_group == 1){
                    // $('.validation-date').addClass('d-none');
                    // $("#daterange"+i).val(false);
                }


            }
        }
        var user_id = $.trim($('#user_id').val());

        if(user_id == 0 ) {

            $('#login_request').removeClass('d-none');

        }else{

            $('.login_msg').removeClass('d-none');
            $('.login_msg').removeClass('d-none');
        }


        $('#child1').removeClass('d-none');