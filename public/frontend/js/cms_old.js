
$( document ).ready(function() {

    // $('.get_text').prop("disabled", true);

    if($('#edit_cms_switch').is(":checked")){
        $('.get_text').prop("disabled", false);
    }else{
        $('.button_text').removeClass("get_text");
        $('.get_text').prop("disabled", true);
    }
});

$('body').on('change','#edit_cms_switch',function(e){
    e.preventDefault();
    var switchCMS = '';
    if($(this).is(":checked")){
        $('.button_text').addClass("get_text");
        $('.get_text').prop("disabled", false);
        $('.slider-title').attr("disabled","enabled");
        switchCMS =true;
    }else{
        $('.button_text').removeClass("get_text");
        $('.get_text').prop("disabled", true);
        $('.slider-title').attr("disabled","disabled");
        switchCMS =false;
    }
    var token = $("meta[name='csrf-token']").attr("content");
    $.ajax({
        url:"/frontend/switchcms",
        method:"POST",
        data:{switchCMS: switchCMS ,  _token:token},
        success:function(data){
            // console.log(data);

            // location.reload();
        }
    });
});



$('body').on('click','.get_text', function(e){
    var textValue = $(this).data('value');
    // alert(textValue);
    $('#body-text-append').html('');
    var textId = $(this).data('id');
    e.preventDefault();
    $("#modal_text_id").val(textId);
    var token = $("meta[name='csrf-token']").attr("content");

    $.ajax({
        url:"/frontend/getLanguages",
        method:"POST",
        data:{textId: textId , _token:token},
        success:function(data){
            console.log(data);
            var counter = 0;
            // $.each(data, function () {
                $.each(data.texts, function (index, value) {
                     if(data.locale == value.lang){
                         $('#body-text-append').append('<input type="hidden" id="lang'+counter+'" value="'+value.lang+'"><input type="hidden" id="data_value_id" value="body-text'+counter+'"><h2>'+value.lang+'</h2><div class="summernote'+counter+'" id="body-text'+counter+'"></div><br>');
                     }else {

                         $('#body-text-append').append('<input type="hidden" id="lang' + counter + '" value="' + value.lang + '"><h2>' + value.lang + '</h2><div class="summernote' + counter + '" id="body-text' + counter + '"></div><br>');
                     }
                    $("#body-text"+counter).summernote('code', '');
                    $("#body-text"+counter).summernote('code', value.value);
                    // $("#body-text").html(textValue);
                    $('.summernote+counter').summernote({
                        height: 300,
                        tabsize: 2,
                        followingToolbar: true,
                        toolbar: [
                            ['style', ['style']],
                            ['font', ['bold', 'underline', 'clear']],
                            ['fontname', ['fontname']],
                            ['color', ['color']],
                            ['para', ['ul', 'ol', 'paragraph']],
                            ['view', ['fullscreen', 'help']],
                        ],
                    });
                    counter++;

                });
                $('#total-lang').val(counter);
            // });

        }
    });

    $('#myModal').modal('show');
});

$('#myModal .save-button').click(function() {
    var total_lang = $('#total-lang').val();

    var languages = [];
    var data_values = [];

    for(var i=0 ; i < total_lang ; i++){

        languages[i] = $('#lang'+i).val();
        data_values[i] = $('#body-text'+i).summernote('code');
    }
    var data_id = $('#modal_text_id').val();
    console.log(data_id);
    // var data_value = $('#body-text').summernote('code');
    var token = $("meta[name='csrf-token']").attr("content");

    $.ajax({
        url:"/frontend/changeText",
        method:"POST",
        data:{data_id:data_id, languages:languages, data_values:data_values,  _token:token},
        success:function(data){

          // console.log(data);
           var data_value_id = $('#data_value_id').val();
           var data_value = $('#'+data_value_id).summernote('code');
            $("[data-id="+data_id+"]").html(data_value);
            $("[data-id="+data_id+"]").data('value', data_value);
            // location.reload();
        }
    });

    // $('#myModal')[0].reset();
    // $('#myMainPageInput').val(value);
});


