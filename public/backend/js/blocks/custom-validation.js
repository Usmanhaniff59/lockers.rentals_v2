$("#block-group").validate({
			rules: {
				name: {
					required: true,
					maxlength: 100
				},
		},
		messages: {
				
				name: {
					required: "Please enter a Name",
					maxlength: "Your Name should not be greater than 100 characters"
				},
			}
	});