

var selected_lockers=[];
sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));


function clickOnlocker(id,locker_number,active,display){

    $("#error_label").html("");
    var radio_option = $('input[name=radio1]:checked').val();
    var locker_active = 0;
    var locker_display = 0;
    var checkboxvalue = 0;
    var  selected_lockers = JSON.parse(sessionStorage.getItem("selected_lockers_backend"));
    if($("#deleted_checkbox").prop("checked") == true){
        checkboxvalue = 1;
    }
    else if($("#deleted_checkbox").prop("checked") == false){
        checkboxvalue = 0;
    }

    if(radio_option == 'display'){
        $("#locker_name").attr("readonly", false);
        $("#locker_name").val(locker_number);
        $("#locker_id").val(id);
    }

    else if(radio_option == 'blank'){

        if(checkboxvalue == 0){

            var locker_checed = $('#locker'+id).prop("checked");

            if(locker_checed == false){

                selected_lockers.push([locker_number, id]);
                sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));
                console.log(selected_lockers,'selected_lockers in blank checked');

                $('#locker'+id).prop('checked', true);

                $('#locker'+id).css("transform", "scale(1, 1)");
                $('#locker'+id).css("border", "5px solid #66ff66");

                $('#'+id).css("transform", "scale(1, 1)");
                $('#'+id) .css("border", "5px solid #66ff66");

            }else if(locker_checed == true){
                const  selected_lockers_in_deselect = JSON.parse(sessionStorage.getItem("selected_lockers_backend"));
                $.each(selected_lockers_in_deselect ,function (index, value) {
                    console.log(value,'value');
                    if(value[0] == locker_number && value[1] == id ){
                        // console.log('same');
                        selected_lockers_in_deselect.splice(index, 1);

                        sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers_in_deselect));
                    }
                    console.log(selected_lockers_in_deselect,'selected_lockers in blank unchecked');

                });

                $('#locker'+id).prop('checked', false);

                $('#locker'+id).css("transform", "scale(1, 1)");
                $('#'+id).css("transform", "scale(1, 1)");
                if($('#locker'+id).data('blank') == true) {
                    $('#locker' + id).css("border", "5px solid #9d9d9d");
                    $('#'+id) .css("border", "5px solid #9d9d9d");
                }else{

                    $('#locker' + id).css("border", "5px solid #F1F1F1");
                    $('#'+id) .css("border", "5px solid #F1F1F1");
                }

            }

            // locker_active = 1;
            // locker_display = 0;
            // alert('blank')
            // lockerAction(locker_number,id,locker_active,locker_display,0);
        }

    }

    else if(radio_option == 'delete'){

        var locker_checed = $('#locker'+id).prop("checked");

        if(locker_checed == false){

            selected_lockers.push([locker_number, id]);
            sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));
            console.log(selected_lockers,'selected_lockers in delete checked');

            $('#locker'+id).prop('checked', true);

            $('#locker'+id).css("transform", "scale(1, 1)");
            $('#locker'+id).css("border", "5px solid #66ff66");

            $('#'+id).css("transform", "scale(1, 1)");
            $('#'+id) .css("border", "5px solid #66ff66");

        }else if(locker_checed == true){
            const  selected_lockers_in_deselect = JSON.parse(sessionStorage.getItem("selected_lockers_backend"));

            $.each(selected_lockers_in_deselect ,function (index, value) {
                console.log(value,'value');
                if(value[0] == locker_number && value[1] == id ){
                    // console.log('same');
                    selected_lockers_in_deselect.splice(index, 1);

                    sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers_in_deselect));
                }
                console.log(selected_lockers,'selected_lockers in delete unchecked');
            });

            $('#locker'+id).prop('checked', false);

            $('#locker'+id).css("transform", "scale(1, 1)");
            $('#'+id).css("transform", "scale(1, 1)");
            if($('#locker'+id).data('blank') == true) {
                $('#locker' + id).css("border", "5px solid #9d9d9d");
                $('#'+id) .css("border", "5px solid #9d9d9d");
            }else{

                $('#locker' + id).css("border", "5px solid #F1F1F1");
                $('#'+id) .css("border", "5px solid #F1F1F1");
            }

        }

        // if(checkboxvalue == 0){
        //         locker_active = 0;
        //         locker_display = 1;
        // }
        // else{
        //         locker_active = 1;
        //         locker_display = 1;
        // }
        //
        // lockerAction(locker_number,id,locker_active,locker_display,0);
    }

    else if(radio_option == 'maintenance'){

        var locker_checed = $('#locker'+id).prop("checked");

        if(locker_checed == false){

            selected_lockers.push([locker_number, id]);
            sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));
            console.log(selected_lockers,'selected_lockers');

            $('#locker'+id).prop('checked', true);

            if($('#locker'+id).data('type') == 'maintenance') {
                $('#locker' + id).css("border", "5px solid #9d9d9d");
                $('#'+id) .css("border", "5px solid #9d9d9d");
            }else{

                $('#locker' + id).css("border", "5px solid #F1F1F1");
                $('#'+id) .css("border", "5px solid #F1F1F1");
            }

            $('#locker'+id).css("transform", "scale(1, 1)");
            $('#locker'+id).css("border", "5px solid #66ff66");

            $('#'+id).css("transform", "scale(1, 1)");
            $('#'+id) .css("border", "5px solid #66ff66");

        }else if(locker_checed == true){
            const  selected_lockers_in_deselect = JSON.parse(sessionStorage.getItem("selected_lockers_backend"));
            $.each(selected_lockers_in_deselect ,function (index, value) {
                console.log(value,'value');
                if(value[0] == locker_number && value[1] == id ){
                    // console.log('same');
                    selected_lockers_in_deselect.splice(index, 1);
                    console.log(selected_lockers_in_deselect);
                    sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers_in_deselect));
                }
            });

            $('#locker'+id).prop('checked', false);

            $('#locker'+id).css("transform", "scale(1, 1)");
            $('#'+id).css("transform", "scale(1, 1)");
            if($('#locker'+id).data('type') == 'blank') {
                $('#locker' + id).css("border", "5px solid #9d9d9d");
                $('#'+id) .css("border", "5px solid #9d9d9d");
            }else{

                $('#locker' + id).css("border", "5px solid #F1F1F1");
                $('#'+id) .css("border", "5px solid #F1F1F1");
            }

        }

        // if(checkboxvalue == 0){
        //         locker_active = 0;
        //         locker_display = 1;
        // }
        // else{
        //         locker_active = 1;
        //         locker_display = 1;
        // }
        //
        // lockerAction(locker_number,id,locker_active,locker_display,0);
    }

    else if(radio_option == 'on_off_sale'){

        var locker_checed = $('#locker'+id).prop("checked");

        if(locker_checed == false){

            selected_lockers.push([locker_number, id]);
            sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));
            console.log(selected_lockers,'selected_lockers');

            $('#locker'+id).prop('checked', true);

            $('#locker'+id).css("transform", "scale(1, 1)");
            $('#locker'+id).css("border", "5px solid #66ff66");

            $('#'+id).css("transform", "scale(1, 1)");
            $('#'+id) .css("border", "5px solid #66ff66");

        }else if(locker_checed == true){
            const  selected_lockers_in_deselect = JSON.parse(sessionStorage.getItem("selected_lockers_backend"));
            $.each(selected_lockers_in_deselect ,function (index, value) {
                console.log(value,'value');
                if(value[0] == locker_number && value[1] == id ){
                    // console.log('same');
                    selected_lockers_in_deselect.splice(index, 1);
                    console.log(selected_lockers_in_deselect);
                    sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers_in_deselect));
                }
            });

            $('#locker'+id).prop('checked', false);

            $('#locker'+id).css("transform", "scale(1, 1)");
            $('#'+id).css("transform", "scale(1, 1)");

            if($('#locker'+id).data('blank') == true) {
                $('#locker' + id).css("border", "5px solid #9d9d9d");
                $('#'+id) .css("border", "5px solid #9d9d9d");
            }else{

                $('#locker' + id).css("border", "5px solid #F1F1F1");
                $('#'+id) .css("border", "5px solid #F1F1F1");
            }

        }

        // if(checkboxvalue == 0){
        //         locker_active = 0;
        //         locker_display = 1;
        // }
        // else{
        //         locker_active = 1;
        //         locker_display = 1;
        // }
        //
        // lockerAction(locker_number,id,locker_active,locker_display,0);
    }

    else if(radio_option == 'move'){

        var locker_checed = $('#locker'+id).prop("checked");
        console.log(selected_lockers,'selected_lockers');


        if(locker_checed == false){

            selected_lockers.push([locker_number, id]);
            sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));
            console.log(selected_lockers,'selected_lockers');

            $('#locker'+id).prop('checked', true);

            $('#locker'+id).css("transform", "scale(1, 1)");
            $('#locker'+id).css("border", "5px solid #66ff66");

            $('#'+id).css("transform", "scale(1, 1)");
            $('#'+id) .css("border", "5px solid #66ff66");

        }else if(locker_checed == true){
            const  selected_lockers_in_deselect = JSON.parse(sessionStorage.getItem("selected_lockers_backend"));
            $.each(selected_lockers_in_deselect ,function (index, value) {
                console.log(value,'value');
                if(value[0] == locker_number && value[1] == id ){
                    // console.log('same');
                    selected_lockers_in_deselect.splice(index, 1);
                    console.log(selected_lockers_in_deselect);
                    sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers_in_deselect));
                }
            });

            $('#locker'+id).prop('checked', false);

            $('#locker'+id).css("transform", "scale(1, 1)");
            $('#'+id).css("transform", "scale(1, 1)");

            if($('#locker'+id).data('blank') == true) {
                $('#locker' + id).css("border", "5px solid #9d9d9d");
                $('#'+id) .css("border", "5px solid #9d9d9d");
            }else{

                $('#locker' + id).css("border", "5px solid #F1F1F1");
                $('#'+id) .css("border", "5px solid #F1F1F1");
            }

        }


    }

    else if(radio_option == 'move_out_side_block'){

        var locker_checed = $('#locker'+id).prop("checked");
        console.log(selected_lockers,'selected_lockers');

        if(locker_checed == false){
           //perviouse locker deselect

            selected_lockers.push([locker_number, id]);
            sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers));
            console.log(selected_lockers,'selected_lockers');

            $('#locker'+id).prop('checked', true);

            $('#locker'+id).css("transform", "scale(1, 1)");
            $('#locker'+id).css("border", "5px solid #66ff66");

            $('#'+id).css("transform", "scale(1, 1)");
            $('#'+id) .css("border", "5px solid #66ff66");

        }else if(locker_checed == true){
            const  selected_lockers_in_deselect = JSON.parse(sessionStorage.getItem("selected_lockers_backend"));
            $.each(selected_lockers_in_deselect ,function (index, value) {
                console.log(value,'value');
                if(value[0] == locker_number && value[1] == id ){
                    // console.log('same');
                    selected_lockers_in_deselect.splice(index, 1);
                    console.log(selected_lockers_in_deselect);
                    sessionStorage.setItem("selected_lockers_backend", JSON.stringify(selected_lockers_in_deselect));
                }
            });

            $('#locker'+id).prop('checked', false);

            $('#locker'+id).css("transform", "scale(1, 1)");
            $('#'+id).css("transform", "scale(1, 1)");

            if($('#locker'+id).data('blank') == true) {
                $('#locker' + id).css("border", "5px solid #9d9d9d");
                $('#'+id) .css("border", "5px solid #9d9d9d");
            }else{

                $('#locker' + id).css("border", "5px solid #F1F1F1");
                $('#'+id) .css("border", "5px solid #F1F1F1");
            }

        }


    }
}


$('body').on('click','input[name=radio1]:checked',function(e){
    var radio_option = $('input[name=radio1]:checked').val();
    $("#error_label").html("");
    if(radio_option == 'display'){
        $(".display_edit_div").show();
        $(".display_blank_div").addClass('d-none');
        $(".display_delete_div").addClass('d-none');
        $(".display_delete_div").addClass('d-none');
        $(".display_maintenance_div").addClass('d-none');
        $(".display_on_off_sale_div").addClass('d-none');
        $(".display_move_div").addClass('d-none');
        $(".display_move_out_side_block_div").addClass('d-none');
    }
    else if(radio_option == 'blank'){
        $(".display_blank_div").removeClass('d-none');
        $(".display_delete_div").addClass('d-none');
        $(".display_maintenance_div").addClass('d-none');
        $(".display_on_off_sale_div").addClass('d-none');
        $(".display_edit_div").hide();
        $(".display_move_div").addClass('d-none');
        $(".display_move_out_side_block_div").addClass('d-none');

    }else if(radio_option == 'delete'){
        $(".display_blank_div").addClass('d-none');
        $(".display_delete_div").removeClass('d-none');
        $(".display_maintenance_div").addClass('d-none');
        $(".display_on_off_sale_div").addClass('d-none');
        $(".display_edit_div").hide();
        $(".display_move_div").addClass('d-none');
        $(".display_move_out_side_block_div").addClass('d-none');

    }else if(radio_option == 'maintenance'){
        $(".display_blank_div").addClass('d-none');
        $(".display_delete_div").addClass('d-none');
        $(".display_maintenance_div").removeClass('d-none');
        $(".display_on_off_sale_div").addClass('d-none');
        $(".display_edit_div").hide();
        $(".display_move_div").addClass('d-none');
        $(".display_move_out_side_block_div").addClass('d-none');

    }else if(radio_option == 'on_off_sale'){
        $(".display_blank_div").addClass('d-none');
        $(".display_delete_div").addClass('d-none');
        $(".display_maintenance_div").addClass('d-none');
        $(".display_on_off_sale_div").removeClass('d-none');
        $(".display_edit_div").hide();
        $(".display_move_div").addClass('d-none');
        $(".display_move_out_side_block_div").addClass('d-none');

       }else if(radio_option == 'move'){
        $(".display_blank_div").addClass('d-none');
        $(".display_delete_div").addClass('d-none');
        $(".display_maintenance_div").addClass('d-none');
        $(".display_on_off_sale_div").addClass('d-none');
        $(".display_move_div").removeClass('d-none');
        $(".display_move_out_side_block_div").addClass('d-none');
        $(".display_edit_div").hide();

       }else if(radio_option == 'move_out_side_block'){
        $(".display_blank_div").addClass('d-none');
        $(".display_delete_div").addClass('d-none');
        $(".display_maintenance_div").addClass('d-none');
        $(".display_on_off_sale_div").addClass('d-none');
        $(".display_move_div").addClass('d-none');
         $(".display_move_out_side_block_div").removeClass('d-none');
        $(".display_edit_div").hide();

    }
    $(".loader").show();
    jQuery('#lockers_div').css('opacity', '0.05');
    var block_id = $("#blocksul").val();
    var checkboxvalue = 0;
    if ($("#deleted_checkbox").prop("checked") == true) {
        checkboxvalue = 1;
    } else if ($("#deleted_checkbox").prop("checked") == false) {
        checkboxvalue = 0;
    }
    create_lockers_ajax(block_id, checkboxvalue);
    sessionStorage.setItem("selected_lockers_backend", JSON.stringify([]));
});

$('body').on('click','#locker_active_refresh',function(e){
    $(".loader").show();
    jQuery('#lockers_div').css('opacity', '0.05');
    var block_id = $("#blocksul").val();
    var checkboxvalue = 0;
    if ($("#deleted_checkbox").prop("checked") == true) {
        checkboxvalue = 1;
    } else if ($("#deleted_checkbox").prop("checked") == false) {
        checkboxvalue = 0;
    }
    create_lockers_ajax(block_id, checkboxvalue);
});

$('body').on('click','#locker_active_submit',function(e){
    var locker_name = $("#locker_name").val();
    var locker_id = $("#locker_id").val();
    if(locker_name == ''){
        $("#error_label").html("Please click any locker");
    }
    var reorder = 0;
    if($("#reorder_checkbox").prop("checked") == true){
        reorder = 1;
    }
    else if($("#reorder_checkbox").prop("checked") == false){
        reorder = 0;
    }
    var status = $("#locker_active").val();
    var locker_name_input = document.getElementById("locker_name");
    const isValidlocker_name = locker_name_input.checkValidity();
    if(isValidlocker_name) {
        $("#error_label").html("");
        var selected_lockers = [];
        selected_lockers.push([locker_name, locker_id]);
        lockerAction(selected_lockers,1,1,reorder);
        $("#locker_name").attr("readonly", true);
        $("#locker_name").val('');
        $("#locker_id").val('');
        $("#reorder_checkbox"). prop("checked", false);
    }
    else{
        $("#error_label").html("Please input value between 1-999.");
    }
});

//locker blank submit
$('body').on('click','#locker_blank_submit',function(e){
    var  selected_lockers = JSON.parse(sessionStorage.getItem("selected_lockers_backend"));
    console.log(selected_lockers,'selected_lockers in blank submit');
    locker_active = 1;
    locker_display = 0;

     lockerAction(selected_lockers,locker_active,locker_display,0);
});
//locker delete submit
$('body').on('click','#locker_delete_submit',function(e){
    const  selected_lockers = JSON.parse(sessionStorage.getItem("selected_lockers_backend"));
    console.log(selected_lockers,'selected_lockers in delete submit');

    var checkboxvalue = 0;
    if($("#deleted_checkbox").prop("checked") == true){
        checkboxvalue = 1;
    }
    else if($("#deleted_checkbox").prop("checked") == false){
        checkboxvalue = 0;
    }

    if(checkboxvalue == 0){
        locker_active = 0;
        locker_display = 1;

        if (confirm("Are you sure to Delete?")) {
            lockerAction(selected_lockers, locker_active, locker_display, 0);
        }
    }
    else{
        locker_active = 0;
        locker_display = 0;
        
        if (confirm("Are you sure to Restore?")) {
            lockerAction(selected_lockers, locker_active, locker_display, 0);
        }
    }


    // lockerAction(locker_number,id,locker_active,locker_display,0);


});

//locker delete submit
$('body').on('click','#locker_maintenance_submit',function(e){

    const  selected_lockers = JSON.parse(sessionStorage.getItem("selected_lockers_backend"));
    console.log(selected_lockers,'selected_lockers mintenacne');
    var maintenance_till_date = $('#maintenance_till_date').val();
    $('#error_maintenance_type').text('');
    $('#error_maintenance_till_date').text('');

    var maintenance_type = $('#maintenance_type').val();

    if(maintenance_type == '' || maintenance_till_date == '' ){
        if(maintenance_type == '' ) {
            $('#error_maintenance_type').text('Select Type');
        }
        if(maintenance_till_date == ''){
            $('#error_maintenance_till_date').text('Select Date');
        }
        return false;
    }

    // alert(maintenance_till_date);
    var checkboxvalue = 0;

    if($("#deleted_checkbox").prop("checked") == true){
        checkboxvalue = 1;
    }
    else if($("#deleted_checkbox").prop("checked") == false){
        checkboxvalue = 0;
    }

    if(checkboxvalue == 0){
        var locker_active = 1;
        var locker_display = 1;
    }
    else{
        var locker_active = 1;
        var locker_display = 1;
    }
    maintenanceAction(selected_lockers ,maintenance_type,maintenance_till_date,locker_active,locker_display )

    // lockerAction(locker_number,id,locker_active,locker_display,0);
    // lockerAction(selected_lockers,locker_active,locker_display,0);
});

$('body').on('click','#locker_on_off_sale_submit',function(e){

    const  selected_lockers = JSON.parse(sessionStorage.getItem("selected_lockers_backend"));
    console.log(selected_lockers,'selected_lockers on off sale')

    // alert(maintenance_till_date);
    var checkboxvalue = 0;

    if($("#deleted_checkbox").prop("checked") == true){
        checkboxvalue = 1;
    }
    else if($("#deleted_checkbox").prop("checked") == false){
        checkboxvalue = 0;
    }

    if(checkboxvalue == 0){
        var locker_active = 1;

    }
    else{
        var locker_active = 1;

    }
    onOffSaleAction(selected_lockers ,locker_active)

    // lockerAction(locker_number,id,locker_active,locker_display,0);
    // lockerAction(selected_lockers,locker_active,locker_display,0);
});

$('body').on('click','#locker_move_submit',function(e){

    const  selected_lockers = JSON.parse(sessionStorage.getItem("selected_lockers_backend"));
    console.log(selected_lockers,'selected_lockers move sale');

    // alert(maintenance_till_date);
    var checkboxvalue = 0;

    if($("#deleted_checkbox").prop("checked") == true){
        checkboxvalue = 1;
    }
    else if($("#deleted_checkbox").prop("checked") == false){
        checkboxvalue = 0;
    }

    if(checkboxvalue == 0){
        var locker_active = 1;

    }
    else{
        var locker_active = 1;

    }
    var move_locker_number = $.trim($('#move_locker_number').val());

    if(move_locker_number == ''){
        $('#error_label_move').text('kindly enter the new order!');
        return false;
    }
    if (selected_lockers === undefined || selected_lockers.length == 0 ) {

    }else {
        MoveWithinBlockAction(selected_lockers, move_locker_number)
    }

    // lockerAction(locker_number,id,locker_active,locker_display,0);
    // lockerAction(selected_lockers,locker_active,locker_display,0);
})

;$('body').on('click','#locker_move_out_side_block_submit',function(e){

    const  selected_lockers = JSON.parse(sessionStorage.getItem("selected_lockers_backend"));
    console.log(selected_lockers,'selected_lockers move sale');

    // alert(maintenance_till_date);
    var checkboxvalue = 0;

    if($("#deleted_checkbox").prop("checked") == true){
        checkboxvalue = 1;
    }
    else if($("#deleted_checkbox").prop("checked") == false){
        checkboxvalue = 0;
    }

    if(checkboxvalue == 0){
        var locker_active = 1;

    }
    else{
        var locker_active = 1;

    }
    var block_id = $.trim($('#move_locker_outside_block').val());

    if(block_id == ''){
        $('#error_label_move_outside_block').text('kindly select block!');
        return false;
    }
    if (selected_lockers === undefined || selected_lockers.length == 0 ) {

    }else {
        MoveOutSideBlockAction(selected_lockers, block_id)
    }

});