
//------------------------------------Before Modal Show Do Something------------------------------//
function modelToggle(object) {

    switch ($(object).data('id')) {
        case 'institute-type-modal':
            institute_type_table.draw();
            break;
        case 'kinship-status-modal':
            kinship_status_table.draw();
            break;
        case 'section-modal':
            section_table.draw();
            break;
        case 'session-modal':
            session_table.draw();
            break;
        case 'fee-modal':
            fee_table.draw();
            break;
        case 'fee-status-modal':
            fee_status_table.draw();
            break;
        case 'designation-modal':
            designation_table.draw();
            break;
        case ' payment-status-modal':
            payment_status_table.draw();
            break;
        case 'expense-status-modal':
            expense_status_table.draw();
            break;
        case 'attendance-status-modal':
            attendance_status_table.draw();
            break;
        case 'expense-type-modal':
            expense_type_table.draw();
            break;
        case 'expense-sub-type-modal':
            params              = {};
            params['table']     = 'expense_types';
            params['select_id'] = 'expense_type_id_for_sub_type';
            params['token'] = $('#expense_sub_type_token').val();
            params1              = {};
            params1['table']     = 'expense_types';
            params1['select_id'] = 'expense_type_id_for_sub_type_update';
            params1['token'] = $('#expense_sub_type_update_token').val();
            refreshSelect(null, params);
            refreshSelect(null, params1);
            expense_sub_type_table.draw();
            break;
        case 'datesheet-modal':
            params              = {};
            params['table']     = 'subjects';
            params['column']     = 'title';
            params['select_id'] = 'exam_syllabus_subject_id';
            params['token'] = $('#exam_syllabus_subject_token').val();

            refreshSelect(null, params);
            break;
        case 'datesheet-update-modal':
            params              = {};
            params['table']     = 'subjects';
            params['column']     = 'title';
            params['select_id'] = 'exam_syllabus_update_subject_id';
            params['token'] = $('#exam_syllabus_update_subject_token').val();

            refreshSelect(null, params);
            break;
        case 'student-result-modal':
            params              = {};
            params['table']     = 'subjects';
            params['select_id'] = 'student_result_subject_id';
            params['token'] = $('#student_result_token').val();
            refreshSelect(null, params);
            expense_sub_type_table.draw();
            break;
        case 'expense-modal':
            params              = {};
            params['table']     = 'expense_types';
            params['select_id'] = 'expense-type-id';
            params['token'] = $('#add-expense-token').val();
            refreshSelect(null, params);

            break;
        case 'expense-report-modal':
            params              = {};
            params['table']     = 'expense_types';
            params['select_id'] = 'rpt-expense-type-id';
            params['token'] = $('#rpt-expense-token').val();
            refreshSelect(null, params);
            break;
        case 'students-attendance-report-modal':
            params              = {};
            params['table']     = 'classes';
            params['select_id'] = 'rpt-students-attendance-class-id';
            params['token'] = $('#rpt-students-attendance-token').val();
            refreshSelect(null, params);
            break;
        case 'fee-report-modal':
            params              = {};
            params['table']     = 'classes';
            params['select_id'] = 'rpt-class-id';
            params['token'] = $('#rpt-fee-token').val();
            refreshSelect(null, params);

            break;
        case 'students-report-modal':
            params              = {};
            params['table']     = 'classes';
            params['select_id'] = 'rpt-student-class-id';
            params['token'] = $('#rpt-students-token').val();
            refreshSelect(null, params);

            break;
        case 'class-subject-modal':

            getDataForSelectBox('','class-subject-class-id',$('#select-class-id-url').val(),$('#token').val());
            setTimeout(function() {
                getDataForSelectBox('','class-subject-subject-id',$('#select-subject-id-url').val(),$('#token').val());
            }, 1500);
            break;
        case 'payment-method-modal':
            payment_method_table.draw();
    }

    $('#'+$(object).data('id')).modal('toggle');
};
//-------------------------------------------Modal Toggle End-------------------------------------------//

//----------------------------Get Local Date-------------------------//
function

getLocalDate(id) {

    var date = moment(new Date()).format('YYYY-MM-DD');
    $('#'+id).val(date);
}

//----------------------------Ajax Form Submission-------------------------//
function saveData(form_id,btn_id,modal_id,table_id,callback){

    // $('#'+ form_id).addClass('was-validated');

    if(btn_id !== '') {
        var btn = $('#' + btn_id);
        var btn_txt = btn.html();
        btn.html("<i class='fa fa-spinner'></i> Processing...");
    }

    // if(typeof $('#'+ form_id +' #ignore-mask').val() !== 'undefined' && $('#'+ form_id +' #ignore-mask').val() === 'true'){}
    // else {
    //     clearMask();
    // }
    // debugger;
    // console.log($('#'+form_id).serialize());
    // $('#sample_611').DataTable().clear().destroy();
    $.ajax({

        type    :$('#'+form_id).attr('method'),
        url     :$('#'+form_id).attr('action'),
        data    :$('#'+form_id).serialize(),
        dataType: 'json',
        success : function(data){
            //console.log(data);
            $('#'+form_id).trigger("reset");
            if(modal_id !== '') {
                $('#' + modal_id).modal('toggle');
            }
            if(table_id !== '') {
                // table_id.DataTable().clear().destroy();
                // console.log(table_id);
                table_id.draw( 'page' );
                // $('#sample_611').DataTable().draw();


            }
            if(btn_id !== '') {
                btn.html(btn_txt);
            }
            if(data) {
                if(data.success){
                    // debugger;
                    resetFormWithOutConfirm(form_id);
                    // successMessage(data.success);
                    //

                // if(callback && typeof callback === "function") {
                //     callback(data);
                }
                else{
                    // if(data.error){
                    //     errorMessage(data.error);
                    // }else{
                    //     successMessage(data.success);
                    // }
                }

            }
            else {
                // swalE('Something went wrong! please try again.');
            }
            // masking();
        },
        error: function(data){
            if(btn_id !== '')
                btn.html(btn_txt);
            // console.log(data.responseJSON.errors);
            // printErrorMsg(data.responseJSON.errors);
            $(window).scrollTop(0);
            // masking();
        }
    })
}

function successMessage(message) {
    $(".print-success-message").removeClass('d-none');
    $('.success-message').text(message);
}
function errorMessage(message) {
    $(".print-error-message").removeClass('d-none');
    $('.error-message').text(message);
}
function formSubmitWithBlob(form_id,btn_id,card_id,blob,callback){

    // formData = new FormData($('#'+form_id));
    // var form = document.getElementById(form_id); // Find the <form> element
    // var formData = new FormData(form); // Wrap form contents

    // form_id.reportValidity();
    clearMask();
    var formData = new FormData();
    var formDataArray = $('#'+form_id).serializeArray();
    console.log(formDataArray);
    for(var i = 0; i < formDataArray.length; i++){
        var formDataItem = formDataArray[i];
        formData.append(formDataItem.name, formDataItem.value);
    }

    if(blob && blob !== 'undefined'){
        formData.append('avatar', blob, 'profile.jpg');
    }
    console.log(blob);
    if(btn_id !== '') {
        $btn = $('#' + btn_id);
        $btn_txt = $btn.html();
        $btn.html("<i class='ft-loader'></i> Processing...");
    }
    if(card_id !== '')
        $('#'+card_id).attr('disabled',true);

    console.log(formData);
    $.ajax({

        method: 'POST',
        url     :$('#'+form_id).attr('action'),
        data    :formData,
        processData: false,
        contentType: false,
        success : function(data){
            console.log(data);

            if(btn_id !== '')
                $btn.html($btn_txt);
            if(card_id !== '')
                $('#'+card_id).attr('disabled',false);

            if(data) {
                if(callback && typeof callback === "function") {
                    callback(data);
                }else{
                    if(data.error){
                        swalE(data.error);
                    }else{
                        swalS(data);
                    }

                }
            }
            else {
                swalE('Something went wrong! please try again.');
            }
        },
        error: function(data){
            if(btn_id !== '')
                $btn.html($btn_txt);
            // console.log(data.responseJSON.errors);
            printErrorMsg(data.responseJSON.errors);
            $(window).scrollTop(0);
            masking();
        }
    })
}
function updateData(form_id,btn_id,modal_id,table_id,url = null,params = [],method = null){

    if(url){
        $('#'+form_id).attr('action',url)
    }
    if(method){
        $('#'+form_id).attr('method',method)
    }
    $.each(params, function( index, value ) {
        if(modal_id !== '')
            $('#'+modal_id).modal('show');
        if($('select#'+ index).is('select')) {
            $('select#'+ index).val(value).trigger('change');
        }else  if($('#'+ index).is('textarea')) {
            $('#'+ index).text(value);
        }else{
            $('input#'+ index).val(value);
        }
    });
    if(btn_id)
        $('#'+ btn_id).text('Update');

}

function deleteData(url,token,modal_id,table_id,method = null) {

    type = "POST";
    if(method){
        type = method;
    }

    if (confirm("Are you sure to Delete?")) {

        $.ajax({

            type: type,
            url: url,
            data: {
                '_token': token
            },
            dataType: 'json',
            success: function (data) {
                console.log(data);
                console.log("im here",url.split('/')[4]);

                if (table_id !== '') {
                    table_id.draw('page');
                }

                if(url.split('/')[4] === 'company'){
                    companyEntryTable();
                }

                if(data.error){
                    errorMessage(data.error);
                }else{
                    successMessage(data.success);
                }
            },
            error: function (data) {

            }
        })
    }

}

function printErrorMsg (msg) {
    $(".print-error-msg").find("ul").html('');
    $(".print-error-msg").css('display','block');
    $.each( msg, function( key, value ) {
        $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
    });
}

//-----------------------------------End-----------------------------------//

//----------------------------Ajax Select-box getData-------------------------//
function getDataForSelectBox(object,select_id,url,token){

    if(select_id !== '')
        $select = $('#'+select_id);
    else
        $select = $(object);

    $select.html('');
    $.ajax({

        type:"POST",
        url :url,
        data: {'_token': token},
        dataType: 'json',
        success : function(data){
            console.log(data);

            $select.append(data);
            // $.each(data, function( index, value ) {
            // });
        },
        error: function(data){

        }
    })
}
//-----------------------------------End-----------------------------------//

//-----------------------------------Refresh Select-----------------------------------//
function refreshSelect(object=null, params=[]){

    var url = '';
    var token = '';
    var table = '';
    var select_id = '';
    if (typeof object !== "undefined" && object !== null) {
        table       = $(object).data('table');
        url         = $('#select-refresh-url').val();
        select_id   = $(object).data('select_id');
        token       = $(object).data('token');
    }else{
        table       = params['table'];
        url         = $('#select-refresh-url').val();
        select_id   = params['select_id'];
        token       = params['token'];
    }

    $.ajax({
        type    :"POST",
        url     :url,
        data    : {
            '_token': token,
            'table' : table
        },
        dataType: 'json',
        success : function(data){
            console.log(data);
            $('#'+select_id).find('option').not(':first').remove();
             $.each(data, function( index, val ) {
                 if(typeof params['column'] !== 'undefined')
                    $('#'+select_id).append('<option value="'+val.id+'">'+val[params['column']]+'</option>');
                 else
                    $('#'+select_id).append('<option value="'+val.id+'">'+val['name']+'</option>');
             });
        },
        error: function(data){

        }
    })
}
//-----------------------------------------End----------------------------------------//

//-----------------------------------Assign Data To Modal-----------------------------------//
function assignDataToModal(object,params = []){
    $.each(params, function( index, val ) {
        $('#'+val).val($(object).data(index));
    });
    if($('#charge-fee-slip-from').val($('#datepicker').val()))
        $('#charge-fee-slip-from').val($('#datepicker').val());
    if($('#charge-fee-slip-to').val($('#datepicker').val()))
        $('#charge-fee-slip-to').val($('#datepicker1').val());
    if($('#student-result-list-date-from').val())
        $('#result-slip-from').val($('#student-result-list-date-from').val());
    if($('#student-result-list-date-to').val())
        $('#result-slip-to').val($('#student-result-list-date-to').val());
}
//-----------------------------------------End----------------------------------------//

//-----------------------------------Search By Any Column-----------------------------------//

function searchByThis(table){
    table.draw('page');
    // excel_filters();
    // booked_excel_filters();
}

function feeSearchByThis(){
    students_fee_table.draw();
}
function studentFeeSearchByThis(){
    student_fee_table.draw();
}
//-----------------------------------------End----------------------------------------//


//-----------------------------Select Box Option Settings------------------------------//

function selectBoxOptionSettings(object,url,next_select,token){
    //alert($(object).val()+' -- '+url+' -- '+next_select);
    $.ajax({
        type:"POST",
        url :url,
        data: {
            '_token': token,
            'id'    : $(object).val()
        },
        dataType: 'json',
        success : function(data){
            if(data) {
                $('#' + next_select).removeAttr('disabled');
                $('#' + next_select).find('option').not(':first').remove();
                $.each(data, function (index, val) {
                    $('#' + next_select).append('<option value="' + val.id + '">' + val.name + '</option>');
                });
            }else{
                $('#' + next_select).attr('disabled','disabled');
                swalE('NO record found! Try another or add first.')
            }
        },
        error: function(data){

        }
    })
}
//-----------------------------------------End----------------------------------------//

//---------------------------------Render Models js Call------------------------------//
function renderModal(object){
    $('.modal').modal('hide');
    var model = $(object).data('model_id');
    var url = $(object).data('url');
    $.get( url, function( data ) {
        $('.load_modal').html('');
        $('.load_modal').html(data);
        $('#'+ model).modal('toggle');
        masking();
    });
}
//-----------------------------------------End----------------------------------------//

//--------------------------------Download File By JS---------------------------------//
function download(){
    if($('#datepicker').val())
        $('#invoice-fee-slip-from').val($('#datepicker').val());
    if($('#datepicker').val())
        $('#invoice-fee-slip-to').val($('#datepicker1').val());
    if($('#student-result-list-date-from').val())
        $('#result-slip-date-from').val($('#student-result-list-date-from').val());
    if($('#student-result-list-date-to').val())
        $('#result-slip-date-to').val($('#student-result-list-date-to').val());
}
//-----------------------------------------End----------------------------------------//

//--------------------------------On Select Show Perform These functions---------------------------------//
function getClassStudents(object,id){

   $('#'+id).val($(object).find(':selected').data('students'));
}
function showHideTable(){
    $('#class-students-attendance-default').hide();
    $('#class-students-attendance-manual').hide();
    $('#'+ $('#class-attendance-type').find(':selected').val()).show();

}
//-----------------------------------------End----------------------------------------//

function printData(id)
{
    var divToPrint=document.getElementById(id);
    newWin= window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    newWin.close();
}


//-------------------------------------------Select box Filter-------------------------------------------//
function setSelectBoxData(table,select_id,token,table_id) {
    params              = {};
    params['table']     = table;
    params['select_id'] = select_id;
    params['token'] = token;
    refreshSelect(null, params);
    table_id.draw();
}

//--------------------------------------------Set nev item active----------------------------------------//

function activeByIdNevItem(id){
    $('#'+ id).addClass('active');
}

//-------------------------------------Form Validation Array-------------------------------------//
var validate = {
    email: function (input,status) {
        pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if(!pattern.test(input.val())){
            input.addClass('is-invalid');
            input.children('span').addClass("invalid-select");
            input.siblings('div.invalid-feedback').html((input.data('field-name') !== 'undefined')? input.data('field-name')+' must be valid.':' must be valid.');
            status = false;
        }else{
            input.removeClass('is-invalid');
            input.addClass('is-valid');
            // status = true;
        }
        return status;
    },
    phone: function (input,status) {
        pattern = /^[0][1-9]\d{10}$|^[1-9]\d{10}$/g;
        if(!pattern.test(input.val())){
            input.addClass('is-invalid');
            input.children('span').addClass("invalid-select");
            status = false;
            input.siblings('div.invalid-feedback').html((input.data('field-name') !== 'undefined')? input.data('field-name')+' must be 11 digits.':' must be 11 digits.');
        }else{
            input.removeClass('is-invalid');
            input.addClass('is-valid');
            // status = true;
        }
        return status;
    },
    name: function (input,status) {
        pattern = /^[a-zA-Z\_]{1,50}$/i;
        if(!pattern.test(input.val())){
            input.addClass('is-invalid');
            input.children('span').addClass("invalid-select");
            status = false;
            input.siblings('div.invalid-feedback').html((input.data('field-name') !== 'undefined')? input.data('field-name')+' must contain alphabets.':' must contain alphabets.');
        }else{
            input.removeClass('is-invalid');
            input.addClass('is-valid');
            // status = true;
        }
        return status;
    },
    zip_code: function (input,status) {
        pattern = /^[a-zA-Z0-9\_\-]{1,10}$/i;
        if(!pattern.test(input.val())){
            input.addClass('is-invalid');
            input.children('span').addClass("invalid-select");
            status = false;
            input.siblings('div.invalid-feedback').html((input.data('field-name') !== 'undefined')? input.data('field-name')+' must be less than 10 characters.':' must be less than 10 characters.');
        }else{
            input.removeClass('is-invalid');
            input.addClass('is-valid');
            // status = true;
        }
        return status;
    },
    digits: function (input,status) {
        pattern = /^[1-9]\d{0,10}$/i;
        if(!pattern.test(input.val())){
            input.addClass('is-invalid');
            input.children('span').addClass("invalid-select");
            status = false;
            input.siblings('div.invalid-feedback').html((input.data('field-name') !== 'undefined')? input.data('field-name') +' must be digits.':' must be digits.');

        }else{
            input.removeClass('is-invalid');
            input.addClass('is-valid');
            // status = true;
        }
        return status;
    },
    amount: function (input,status) {
        pattern = /^[+-]?\d+(\.\d+)?$/i;
        if(!pattern.test(input.val())){
            input.addClass('is-invalid');
            input.children('span').addClass("invalid-select");
            status = false;
            input.siblings('div.invalid-feedback').html((input.data('field-name') !== 'undefined')? input.data('field-name')+' must be a valid amount.':' must be a valid amount.');
        }else{
            input.removeClass('is-invalid');
            input.addClass('is-valid');
            // status = true;
        }
        return status;
    }
};

//---------------------------------- Input Masking ---------------------------------------------------//

function masking(){

    $('.money').mask('0,000,000,000', {reverse: true}).attr('maxlength', 21);
    $('.time-mask').mask('00:00:00');
    $('.date-time-mask').mask('00/00/0000 00:00:00');
    // $('.phone').mask('000-0000000', {
    //     translation: {
    //         'Z': {
    //             pattern: /[9]/, optional: false
    //         }
    //     },
    //     placeholder: "92-300-2323100"
    // });
    $('.phone_with_ddd').mask('0000000000');
    $('.zip-code').mask('00000');
    $('.alpha-numeric').mask('Z',{translation: {'Z': {pattern: /[a-zA-Z0-9\-]/, recursive: true}}}).attr('maxlength', 10);
    $('.alpha-numeric-title').mask('Z',{translation: {'Z': {pattern: /[a-zA-Z0-9\-\ ]/, recursive: true}}}).attr('maxlength', 200);

    $('.limited-number').mask('Z',{translation: {'Z': {pattern: /[0-9]/, recursive: true}}}).attr('maxlength', 2);
    $('.name').mask('Z',{translation: {'Z': {pattern: /[a-zA-Z\'\.\-\ ]/, recursive: true}}}).attr('maxlength', 56).addClass('capital');
    $('.cnic').mask('00000-0000000-0');
    $('.marks').mask('00000');
    $('.session').mask('0000-0000');
    $('.percentage').mask('000');
}
function clearMask() {
    // $('.money').unmask();
    // $('.phone').unmask();

    $('.money').unmask();
    $('.time-mask').unmask();
    $('.date-time-mask').unmask();
    // $('.phone').unmask();
    $('.phone_with_ddd').unmask();
    $('.zip-code').unmask();
    $('.alpha-numeric').unmask();
    $('.alpha-numeric-title').unmask();
    $('.limited-number').unmask();
    $('.name').unmask();
    $('.cnic').unmask();
    $('.marks').unmask();
    $('.session').unmask();
    $('.percentage').unmask();
}

function resetForm(form_id) {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this!",
        icon: "warning",
        buttons: [
            'No, cancel it!',
            'Yes, I am sure!'
        ],
        dangerMode: true,
    }).then(function (isConfirm) {
        if (isConfirm) {
            var block_ele = $('#' + form_id).closest('.card');

            // Block Element
            block_ele.block({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                // timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    cursor: 'wait',
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'none'
                }
            });
            if ($('#' + form_id).trigger("reset")) {
                $("#"+ form_id +" :select").each(function(){
                    $(this).val(null).trigger('change');
                });
                block_ele.unblock();
            }
        }
    });
}
function resetFormWithOutConfirm(form_id) {
    // debugger;
        // var block_ele = $('#' + form_id).closest('.card');
        // // Block Element
        // block_ele.block({
        //     message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
        //     // timeout: 2000, //unblock after 2 seconds
        //     overlayCSS: {
        //         backgroundColor: '#FFF',
        //         cursor: 'wait',
        //     },
        //     css: {
        //         border: 0,
        //         padding: 0,
        //         backgroundColor: 'none'
        //     }
        // });

        if ($('#' + form_id).trigger("reset")) {
            $("form#"+ form_id +" :input, select, textarea").each(function(){

                 if($(this).attr('name') == '_token'){

                 }else {
                     $(this).val(null).trigger('change');
                 }
            });
            // block_ele.unblock();
            displayNone();
        }

}
function displayNone(){
    $(".print-error-msg").css('display','none');
}
