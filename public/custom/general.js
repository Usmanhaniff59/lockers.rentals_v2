// $('.table').each(function(){
//         var table_id=$(this)[0].id;
//         $.fn.dataTable.ext.errMode = 'none';
//         $('#'+table_id).on( 'error.dt', function ( e, settings, techNote, message ) {
//             console.log( 'Settings : ',settings.ajax.url );
//             console.log( 'An error has been reported by DataTables: ', message );
//             var apiUrl=settings.ajax.url;
//             customErrorLog(message,apiUrl);
//             setTimeout(function(){ 
//             	// await ApiCall.getAPICall("/custom/error/log");
//             	location.reload(); }, 3000);
//         });
//     });

function customErrorLog(message,apiUrl=null){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "POST",
        url:'/custom/error/log',
        data: {'message':message,"apiUrl":apiUrl},
        success: function(response)
        {
            console.log(response.message,'RESPONSE');
        },
        error: function (error) {
            console.log(error);
        }
    });
}