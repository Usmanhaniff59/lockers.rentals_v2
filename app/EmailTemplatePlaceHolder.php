<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailTemplatePlaceHolder extends Model
{
    protected $fillable = [
        'email_template_id', 'placeholder', 'description',
    ];
}
