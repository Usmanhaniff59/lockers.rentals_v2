<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LockerBlockImage extends Model
{
	protected $table = 'locker_block_image';
    protected $fillable = [
        'file_name', 'image_type','tier','active', 'deleted',
    ];
}
