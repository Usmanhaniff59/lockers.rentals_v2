<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketingPictures extends Model
{
    protected $fillable = [
        'file_name', 'active', 'deleted',
    ];
}
