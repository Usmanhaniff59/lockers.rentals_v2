<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class CompanyBookingsReport extends Model
{
    protected $fillable = [
        'company_id','user_id','bookings'
    ];

    public function company_booking_record_permissions($query)
    {

        $user = Auth::user();
        $franchisees = $user->franchisee()->pluck('franchisee_id')->toArray();

        if(auth()->user()->can('All Records')) {


        }elseif( Auth::user()->company_id && $franchisees) {

            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id',$franchisees)->pluck('company_id')->toArray();

            $query->Where(function ($query) use ($franchisee_companies) {
                $query->WhereIn('sales.company_id',$franchisee_companies)
                    ->orWhere('sales.company_id',Auth::user()->company_id);
            });
        }
        elseif( $franchisees ) {
            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id',$franchisees)->pluck('company_id')->toArray();

            $query->whereIn('sales.company_id',$franchisee_companies);
        }
        elseif( Auth::user()->company_id ){

            $query->where('sales.company_id',Auth::user()->company_id);

        }else{
            $query->where('sales.company_id',0);
        }


        return $query;

    }
}
