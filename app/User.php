<?php

namespace App;

use App\Notifications\MailResetPasswordToken;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasRoles;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','username','surname','email','franchisee_id','company_id','response_group_id', 'password','phone_number','address_1', 'address_2', 'state', 'city', 'post code', 'country'
    ];

     public function chats()
    {
        return $this->hasMany('App\Chat','user_id');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function distributionGroups()
    {
        return $this->belongsToMany('App\DistributionListGroup');
    }
    public function franchisee()
    {
        return $this->belongsToMany('App\Franchisee');
    }
    public function payments()
    {
        return $this->hasMany('App\Payment','user_id');

    }
    public function sales()
    {
        return $this->hasMany('App\Sale','user_id','id');

    }
    public function transactions()
    {
        return $this->hasMany('App\Transaction','user_id');

    }

    protected $appends = ['full_name'];

    public function getFullNameAttribute(){

        if(!empty($this->surname) || !empty($this->name)){

            return ucwords($this->name).' '.ucwords($this->surname);
        }else{
            return ucwords($this->username);
        }
    }


    public function first_letter_capital($data){
        return ucwords($data);
    }


    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordToken($token));
    }
    public function users_record_permissions($users){

        $user = Auth::user();
        $franchisees = $user->franchisee()->pluck('franchisee_id')->toArray();
        
        if(auth()->user()->can('All Records')) {

        }
        elseif( Auth::user()->company_id && $franchisees) {

            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id',$franchisees)->pluck('company_id')->toArray();
             array_push($franchisee_companies, Auth::user()->company_id);
            $users->whereIn('company_id',$franchisee_companies);
        }
        elseif( $franchisees ) {
            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id',$franchisees)->pluck('company_id')->toArray();

            $users->whereIn('company_id',$franchisee_companies);
        }
        elseif( Auth::user()->company_id ){

            $users->where('company_id',Auth::user()->company_id);
        }else{
            $users->where('id',Auth::user()->id);

        }
        return $users;
    }

}
