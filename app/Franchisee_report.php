<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Franchisee_report extends Model
{
	 protected $fillable = ['month','year','franchisee_id','new_customer','customer','lead','prospective','total'];   
	 public function franchisee_report()
	 {
		 return $this->belongsTo('App\Franchisee','franchisee_id');
	 }

     public function franchisee_dropdown_record_permissions($company_chats)
    {

       
          $user = Auth::user();
            $franchisees = $user->franchisee()->pluck('franchisee_id')->toArray();
        if(auth()->user()->can('All Records')) {

        }
        elseif( $franchisees) {
           
            $company_chats->whereIn('id',$franchisees);

        }
        else{
            $company_chats->where('id',0);

        }

        return $company_chats;

    }

    public function franchisee_record_permissions($company_chats)
    {

        $user = Auth::user();
        $franchisees = $user->franchisee()->pluck('franchisee_id')->toArray();

        if(auth()->user()->can('All Records')) {

        }
        elseif( $franchisees) {

            $company_chats->whereIn('franchisee_reports.franchisee_id',$franchisees);

        }
        else{
            $company_chats->where('franchisee_reports.franchisee_id',0);

        }

        return $company_chats;

    }
}