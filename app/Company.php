<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Company extends Model
{

    protected $fillable = [
        'location_group_id', 'currency', 'name', 'stop_auto_rebook', 'name_number', 'street', 'city',  'country', 'post_code', 'active', 'display'.'keep_block', 'general_email','telephone'
    ];

    public function Chats()
    {
        return $this->hasMany('App\Chat','company_id');
    }

    public function franchisee_companies()
    {
        return $this->hasOne('App\FranchiseeCompany','company_id');

    }
    public function locationGroup()
    {
        return $this->belongsTo('App\LocationGroup','location_group_id');
    }
    public function blocks()
    {
        return $this->hasMany('App\Block','company_id')->orderBy('company_id');

    }
    public function company_errors()
    {
        return $this->hasMany('App\CompanyBlockError','company_id');

    }
    public function sales()
    {
        return $this->hasMany('App\Sale','company_id','id');

    }
    public function lockers()
    {
        return $this->hasMany('App\Locker','company_id','id');

    }
    public function maintenances()
    {
        return $this->hasMany('App\Maintenance','company_id','id');

    }
    public function academicYearReports()
    {
        return $this->hasMany('App\AcademicYearReport','company_id');

    }
    public function voucherCodes()
    {
        return $this->hasMany('App\VoucherCode','company_id');

    }
    public function franchise()
    {
        return $this->belongsTo('App\Franchisee','territory_code');
    }

    //record permission on the basis of admin, franchise group & direct company assigned & user
    public function company_record_permissions($company)
    {
        $user = Auth::user();
        $franchisees = $user->franchisee()->pluck('franchisee_id')->toArray();

        if(auth()->user()->can('All Records')) {

        }elseif( Auth::user()->company_id && $franchisees) {
            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id',$franchisees)->pluck('company_id')->toArray();
            if (!empty($franchisee_companies)) {
                array_push($franchisee_companies, auth()->user()->company_id);
                $company->Where(function ($query) use ($franchisee_companies) {
                    $query->WhereIn('id', $franchisee_companies);
                        // ->orWhere('id', Auth::user()->company_id);
                });
            }else{
                $company->Where(function ($query)  {
                    $query->Where('id', Auth::user()->company_id);
                });
            }
        }
        elseif($franchisees ) {

            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id',$franchisees)->pluck('company_id')->toArray();

            $company->whereIn('id',$franchisee_companies);
        }
        elseif( Auth::user()->company_id ){

            $company->where('id',Auth::user()->company_id);
        }else{
            $company->where('id',0);
        }
        return $company;
    }
}
