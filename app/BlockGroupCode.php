<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlockGroupCode extends Model
{
   protected $table = 'block_group_code';

    public function blockgroup()
    {
        return $this->belongsTo('App\BlockGroup','block_group_id');

    }
}
