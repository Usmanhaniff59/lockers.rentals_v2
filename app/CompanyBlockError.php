<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyBlockError extends Model
{

    public function company()
    {
        return $this->belongsTo('App\Company','company_id');
    }
    public function block()
    {
        return $this->belongsTo('App\Block','block_id');
    }
}
