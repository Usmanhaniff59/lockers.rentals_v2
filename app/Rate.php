<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $primaryKey = 'key';


    public function rates()
    {
        return $this->hasMany('App\Block','rate_id','id');

    }

    public function block_rate($start,$end,$company_id,$block_id,$shcoolAcademicYearId)
    {

        $blocks = Block::Join('block_years','block_years.block_id','=','blocks.id')
            ->Join('rates', function ($join) use ($start, $end) {
                $join->on('blocks.rate_id', '=', 'rates.id')
                    ->where('rates.valid_too', '>=', $start)
                    ->Where('rates.valid_from', '<=', $end);
            })
            ->where('blocks.id',  $block_id)
            ->where('blocks.company_id',  $company_id)
            ->where('blocks.active', 1)->where('blocks.display', 1)
            ->where('block_years.school_academic_year_id',$shcoolAcademicYearId)
            ->select('blocks.id', 'rates.id as rate_id', 'rates.key' , 'rates.hour', 'rates.day', 'rates.week', 'rates.month', 'rates.year', 'rates.unit', 'rates.vat' )
            ->first();

        $now =Carbon::now();

        if($start < $now) {

            $date = $now->format('F');

            $discount_month = PaymentDiscount::where('rate_id', $blocks->rate_id)->first();
            dd($discount_month);
            if ($date == 'September' || $date == 'July') {
                $month = strtolower(substr($date, 0, 4));
            } elseif ($date == 'April') {
                $month = strtolower(substr($date, 0, 5));
            } else {
                $month = strtolower(substr($date, 0, 3));
            }

            $discount = number_format((float)($discount_month[$month] / 100), 2, '.', '');

            $discount_price = number_format((float)($blocks->unit * $discount), 2, '.', '');

            $unit = number_format((float)($blocks->unit - $discount_price), 2, '.', '');

            $tax = number_format((float)($unit * $blocks->vat), 2, '.', '');
        }else{
            $unit = number_format((float)($blocks->unit), 2, '.', '');

            $tax = number_format((float)($unit * $blocks->vat), 2, '.', '');

        }
        return  [$unit, $tax];
    }
}
