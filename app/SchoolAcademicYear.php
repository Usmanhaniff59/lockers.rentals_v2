<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class SchoolAcademicYear extends Model
{
    //
    public function company_school_year()
    {
        return $this->belongsTo('App\CompanySchoolYear', 'school_academic_year_id');
    }
}
