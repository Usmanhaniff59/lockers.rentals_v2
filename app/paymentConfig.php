<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class paymentConfig extends Model
{
    static function integrationKey(){

        $Integration_key = \Config::get('app.integration_key');
        $Integration_password = \Config::get('app.integration_password');

        $Authorization = $Integration_key . ':' . $Integration_password;
         $Authorization = base64_encode($Authorization);
        return $Authorization;

    }

}
