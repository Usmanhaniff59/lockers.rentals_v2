<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class CharityTransactions extends Model
{
    protected $table = 'charity_transactions';
     protected $fillable = [
        'charity_id', 'booking_id', 'amount'
    ];
}
