<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Locker extends Model
{
    protected $fillable = [
        'company_id', 'block_id', 'locker_number', 'location_info', 'date_added', 'locker_order', 'active',  'display', 'updated'
    ];
    public function block()
    {
        return $this->belongsTo('App\Block','block_id');
    }
    public function sales()
    {
        return $this->hasMany('App\Sale','locker_id');

    }
    public function maintenances()
    {
        return $this->hasMany('App\Maintenance','locker_id');

    }
    public function manualLockerCodes()
    {
        return $this->hasMany('App\ManualLockerCode');

    }
     public function company()
    {
        return $this->belongsTo('App\Company','company_id');
    }

    public function lockerOffSales()
    {
        return $this->hasMany('App\LockerOffSale','locker_id');

    }
    public function locker_available_and_booked_report_record_permissions($locker)
    {

        $user = Auth::user();
        $franchisees = $user->franchisee()->pluck('franchisee_id')->toArray();

        if (auth()->user()->can('All Records')) {


        } elseif( Auth::user()->company_id && $franchisees) {


            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id', $franchisees)->pluck('company_id')->toArray();
            if (!empty($franchisee_companies)) {
                $locker->Where(function ($query) use ($franchisee_companies) {
                    $query->WhereIn('lockers.company_id', $franchisee_companies)
                        ->orWhere('lockers.company_id', Auth::user()->company_id);
                });

            } else {
                $locker->Where(function ($query){
                    $query->Where('lockers.company_id', Auth::user()->company_id);
                });
            }

        } elseif ($franchisees) {
            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id', $franchisees)->pluck('company_id')->toArray();
            $locker->whereIn('lockers.company_id', $franchisee_companies);
        } elseif (Auth::user()->company_id) {
            $locker->where('lockers.company_id', Auth::user()->company_id);
        } else {
            $locker->where('lockers.company_id', 0);
        }

        return $locker;

    }





}
