<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LockerOffSale extends Model
{

    public function locker()
    {
        return $this->belongsTo('App\Locker','locker_id');
    }
}
