<?php

namespace App\Exports\reports;


use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
class ManualLockerCodeExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    private $data;
    private $date_passed;
    private $this_month_start_date;
    private $this_month_end_date;

    public function __construct($data,$date_passed,$this_month_start_date,$this_month_end_date)
    {

        $this->data = $data;
        $this->date_passed = $date_passed;
        $this->this_month_start_date = $this_month_start_date;
        $this->this_month_end_date = $this_month_end_date;
    }
    public function view(): View
    {
//        dd($this->data);
        return view('backend.pages.company.exports.manualLockerCodes.excel', [
            'data' => $this->data,
            'date_passed' => $this->date_passed,
            'this_month_start_date' => $this->this_month_start_date,
            'this_month_end_date' => $this->this_month_end_date,
        ]);
    }
}
