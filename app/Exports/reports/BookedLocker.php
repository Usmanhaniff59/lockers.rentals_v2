<?php

namespace App\Exports\reports;


use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class BookedLocker implements FromView
{
    private $data;

    public function __construct($data)
    {

        $this->data = $data;

    }
    public function view(): View
    {
//        dd($this->data);
        return view('backend.pages.reports.export.booked.excel', [
            'data' => $this->data,

        ]);
    }
}