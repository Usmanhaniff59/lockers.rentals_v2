<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $fillable = [
        'email','phone_number','action','subject','mail_body','sms_body','date_added','type','result_code','star','send','complete','deleted','fail','email_folder_id'
    ];
}