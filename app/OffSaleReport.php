<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class OffSaleReport extends Model
{
    protected $fillable = ['year','month','company_id','Franchise_id','total_active_lockers','total_off_sale','maintenance_cleaning','maintenance_fixing','total_lockers','offsale_late','maintenance_late'];

    public function off_sale_record_permissions($offsalereport)
    {

        $user = Auth::user();
        $franchisees = $user->franchisee()->pluck('franchisee_id')->toArray();

        if(auth()->user()->can('All Records')) {


        }elseif( Auth::user()->company_id && $franchisees) {

            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id',$franchisees)->pluck('company_id')->toArray();

            $offsalereport->Where(function ($query) use ($franchisee_companies) {
                $query->WhereIn('off_sale_reports.company_id',$franchisee_companies)
                    ->orWhere('off_sale_reports.company_id',Auth::user()->company_id);
            });
        }
        elseif( $franchisees ) {
            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id',$franchisees)->pluck('company_id')->toArray();

            $offsalereport->whereIn('off_sale_reports.company_id',$franchisee_companies);
        }
        elseif( Auth::user()->company_id ){

            $offsalereport->where('off_sale_reports.company_id',Auth::user()->company_id);

        }else{
            $offsalereport->where('off_sale_reports.company_id',0);
        }
        return $offsalereport;

    }

}