<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FranchiseeCompany extends Model
{
	  protected $fillable = [
        'company_id','franchisee_id'
    ];
    public function franchisee()
    {
        return $this->belongsTo('App\Franchisee','franchisee_id');
    }
    public function company()
    {
        return $this->belongsTo('App\Company','company_id');
    }
}
