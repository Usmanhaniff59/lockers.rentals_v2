<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MassRefunds extends Model
{
     protected $table = 'mass_refunds';
}
