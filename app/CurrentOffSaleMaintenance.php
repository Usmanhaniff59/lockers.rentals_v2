<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class CurrentOffSaleMaintenance extends Model
{


    public function current_offsale_maintenance_record_permissions($maintenances)
    {

        $user = Auth::user();
        $franchisees = $user->franchisee()->pluck('franchisee_id')->toArray();

        if(auth()->user()->can('All Records')) {


        }elseif( Auth::user()->company_id && $franchisees) {

            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id',$franchisees)->pluck('company_id')->toArray();

            $maintenances->Where(function ($query) use ($franchisee_companies) {
                $query->WhereIn('current_off_sale_maintenances.company_id',$franchisee_companies)
                    ->orWhere('current_off_sale_maintenances.company_id',Auth::user()->company_id);
            });
        }
        elseif( $franchisees ) {
            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id',$franchisees)->pluck('company_id')->toArray();

            $maintenances->whereIn('current_off_sale_maintenances.company_id',$franchisee_companies);
        }
        elseif( Auth::user()->company_id ){

            $maintenances->where('current_off_sale_maintenances.company_id',Auth::user()->company_id);

        }else{
            $maintenances->where('current_off_sale_maintenances.company_id',0);
        }

        return $maintenances;

    }
}
