<?php

namespace App\Imports;

use App\CompanyProspect;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\ToModel;


class CompanyImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 300);

        return
            new CompanyProspect([
            'territory_code'     => $row[0],
            'short_post_code'    => $row[1],
            'buzz_code' =>  $row[2],
            'establishment_name' =>  $row[3],
            'town' =>  $row[7],
            'country' =>  $row[8],
            'post_code' =>  $row[9],
            'telephone' =>  $row[10],
            'gender' =>  $row[11],
            'la' =>  $row[12],
            'school_lea_code' =>  $row[13],
            'low_age' =>  $row[14],
            'high_age' =>  $row[15],

            'open_date' =>  $this->transformDate($row[16]),

            'type_of_establishment' =>  $row[17],
            'sixth_form' =>  $row[18],
            'phase_of_education' =>  $row[19],
            'boarding_school' =>  $row[20],
            'academy_trust' =>  $row[21],
            'free_school_meals_percentage' =>  $row[22],
            'religious_affiliation' =>  $row[23],
            'number_of_pupils' =>  $row[24],
            'area' =>  $row[25],
            'website_address' =>  $row[26],
            'general_email' =>  $row[27],
            'head_title' =>  $row[28],
            'head_first' =>  $row[29],
            'head_surname' =>  $row[30],
        ]);
    }

    public function transformDate($value, $format = 'd/m/Y')
    {
        if(!empty($value)) {

                if(strlen($value) >= 8) {
                    $input = $value;
                    $value = explode("/", $value);
                    if(strlen($value[0]) > 2 ){
                        $value = explode("-", $input);
                        $day = substr(sprintf("%02d", $value[0]), -2);
                        $month = substr(sprintf("%02d", $value[1]), -2);
                        $year = substr(sprintf("%04d", $value[2]), -4);

                        $date = $year . '-' . $month . '-' . $day;

                        return $date;
                    }else {
                        $day = substr(sprintf("%02d", $value[0]), -2);
                        $month = substr(sprintf("%02d", $value[1]), -2);
                        $year = substr(sprintf("%04d", $value[2]), -4);

                        $date = $year . '-' . $month . '-' . $day;

                        return $date;
                    }
                }
        }
    }


}