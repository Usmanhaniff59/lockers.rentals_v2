<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class AcademicYearReport extends Model
{
    public function company()
    {
        return $this->belongsTo('App\Company','company_id');
    }

    public function academic_year_record_permissions($query)
    {

        $user = Auth::user();
        $franchisees = $user->franchisee()->pluck('franchisee_id')->toArray();

        if(auth()->user()->can('All Records')) {


        }elseif( Auth::user()->company_id && $franchisees) {

            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id',$franchisees)->pluck('company_id')->toArray();

            $query->Where(function ($query) use ($franchisee_companies) {
                $query->WhereIn('academic_year_reports.company_id',$franchisee_companies)
                    ->orWhere('academic_year_reports.company_id',Auth::user()->company_id);
            });
        }
        elseif( $franchisees ) {
            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id',$franchisees)->pluck('company_id')->toArray();

            $query->whereIn('academic_year_reports.company_id',$franchisee_companies);
        }
        elseif( Auth::user()->company_id ){

            $query->where('academic_year_reports.company_id',Auth::user()->company_id);

        }else{
            $query->where('academic_year_reports.company_id',0);
        }

        return $query;

    }

}
