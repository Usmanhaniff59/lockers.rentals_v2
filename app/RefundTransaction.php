<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefundTransaction extends Model
{
    protected $table = 'refund_transactions';
}
