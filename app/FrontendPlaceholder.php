<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FrontendPlaceholder extends Model
{
    protected $table = 'frontend_placeholders';
    protected $primaryKey = 'key';
    protected $keyType = 'string';
    public $incrementing = false;
    protected $fillable = [
        'key', 'placeholder',
    ];
}
