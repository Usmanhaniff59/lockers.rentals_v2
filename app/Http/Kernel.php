<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,


    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
             \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            \App\Http\Middleware\SetLocale::class,
        ],

        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'isAdmin' => \App\Http\Middleware\AdminMiddleware::class,
        'clearance' => \App\Http\Middleware\ClearanceMiddleware::class,
        'timer' => \App\Http\Middleware\BookingTimer::class,
        'IpCheckMiddleware' => \App\Http\Middleware\IpCheckMiddleware::class,
        'BlockLinkforUser' => \App\Http\Middleware\EnsureUser::class,
        'CompanyError' => \App\Http\Middleware\CompanyError::class,
        'OrderManagment' => \App\Http\Middleware\OrderManagment::class,
        'EditOrders' => \App\Http\Middleware\EditOrders::class,
        'UserManagment' => \App\Http\Middleware\UserManagment::class,
        'PermissionManagement' => \App\Http\Middleware\PermissionManagement::class,
        'RollManagement' => \App\Http\Middleware\RollManagement::class,
        'EmailTemplateManagement' => \App\Http\Middleware\EmailTemplateManagement::class,
        'DocumentManagment' => \App\Http\Middleware\DocumentManagment::class,
        'BookingTypeManagement' => \App\Http\Middleware\BookingTypeManagement::class,
        'DistributionListGroupManagement' => \App\Http\Middleware\DistributionListGroupManagement::class,
        'LockerBlockImages' => \App\Http\Middleware\LockerBlockImages::class,
        'BlockGroup' => \App\Http\Middleware\BlockGroup::class,
        'LockerDescription' => \App\Http\Middleware\LockerDescription::class,
        'HelpManagment' => \App\Http\Middleware\HelpManagment::class,
        'LanguageManagment' => \App\Http\Middleware\LanguageManagment::class,
        'SliderManagement' => \App\Http\Middleware\SliderManagement::class,
        'SalesReport' => \App\Http\Middleware\SalesReport::class,
        'AvailabilityReport' => \App\Http\Middleware\AvailabilityReport::class,
        'BookingReport' => \App\Http\Middleware\BookingReport::class,
        'LockerNumberReport' => \App\Http\Middleware\LockerNumberReport::class,
        'ChatReport' => \App\Http\Middleware\ChatReport::class,
        'FranchiseReport' => \App\Http\Middleware\FranchiseReport::class,
        'MaintanceOffsaleReport' => \App\Http\Middleware\MaintanceOffsaleReport::class,
        'AcademicYearReport' => \App\Http\Middleware\AcademicYearReport::class,
        'VoucherPaymentsReport' => \App\Http\Middleware\VoucherPaymentsReport::class,
        'CompanyBookingReport' => \App\Http\Middleware\CompanyBookingReport::class,
        'CharityReport' => \App\Http\Middleware\CharityReport::class,
        'PupilPremium' => \App\Http\Middleware\PupilPremium::class,
        'Students' => \App\Http\Middleware\Students::class,
        
        



























    ];
}
