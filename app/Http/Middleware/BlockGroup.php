<?php

namespace App\Http\Middleware;

use Closure;

class BlockGroup
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth()->user()->can('Block Groups')) {
             return $next($request);
        }
        return redirect('/');
    }
}
