<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class EnsureUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth()->user()->can('Company Management')) {
             return $next($request);
        }
        // if (Auth()->user()->can('Delete Post')) {
        //      return $next($request);
        // }
        // if (Auth()->user()->can('Administer roles & permissions')) {
        //      return $next($request);
        // }
        // if (Auth()->user()->can('Edit Post')) {
        //      return $next($request);
        // }
        // if (Auth()->user()->can('Create Page')) {
        //      return $next($request);
        // }
        // if (Auth()->user()->can('Edit Page')) {
        //      return $next($request);
        // }
        // if (Auth()->user()->can('Delete Page')) {
        //      return $next($request);
        // }
            return redirect('/');
        


       
    }
}
