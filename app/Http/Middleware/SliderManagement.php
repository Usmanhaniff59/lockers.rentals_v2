<?php

namespace App\Http\Middleware;

use Closure;

class SliderManagement
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth()->user()->can('Slider Management')) {
             return $next($request);
        }
        return redirect('/');
    }
}
