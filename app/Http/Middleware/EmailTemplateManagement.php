<?php

namespace App\Http\Middleware;

use Closure;

class EmailTemplateManagement
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth()->user()->can('Email Templates Management')) {
             return $next($request);
        }
        return redirect('/');
    }
}
