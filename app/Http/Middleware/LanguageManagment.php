<?php

namespace App\Http\Middleware;

use Closure;

class LanguageManagment
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth()->user()->can('Language Management')) {
             return $next($request);
        }
        return redirect('/');
    }
}
