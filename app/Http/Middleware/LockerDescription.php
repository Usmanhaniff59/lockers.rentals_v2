<?php

namespace App\Http\Middleware;

use Closure;

class LockerDescription
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth()->user()->can('Locker Description')) {
             return $next($request);
        }
        return redirect('/');
    }
}
