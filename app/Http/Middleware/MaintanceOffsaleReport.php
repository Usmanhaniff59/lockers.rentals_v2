<?php

namespace App\Http\Middleware;

use Closure;

class MaintanceOffsaleReport
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth()->user()->can('Maintenance OffSale Report')) {
             return $next($request);
        }
        return redirect('/');
    }
}
