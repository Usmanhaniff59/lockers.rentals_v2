<?php

namespace App\Http\Middleware;

use Closure;

use App;
use Illuminate\Support\Facades\Session;


class SetLocale
{
    /**
     *
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

//        dd(Session::get('locale'));
//        var_dump(Session::all());
//        dd(Session::has('locale'));

        if (Session::has('locale')) {
            $locale = Session::get('locale', \Illuminate\Support\Facades\Config::get('app.locale'));
        } else {
            $locale = substr($request->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);
//            dd($locale,'non session');

            if ($locale != 'fr' && $locale != 'en') {
                $locale = 'en';
            }
        }

        \Illuminate\Support\Facades\App::setLocale($locale);


        return $next($request);
    }
}