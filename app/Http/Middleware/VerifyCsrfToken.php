<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
//    protected $except = ['frontend/placeholders','frontend/schoolRequest'];
    protected $except = ['frontend/schooltermbased','frontend/saveSchoolBooking','frontend/lockerAvailabilityCheck','frontend/payments3D','frontend/logout','/frontend/validatepasscode'];
}
