<?php

namespace App\Http\Middleware;

use Closure;

class LockerBlockImages
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth()->user()->can('Locker Block Images')) {
             return $next($request);
        }
        return redirect('/');
    }
}
