<?php

namespace App\Http\Middleware;

use Closure;

class FranchiseReport
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       if (Auth()->user()->can('Franchisee Report')) {
             return $next($request);
        }
        return redirect('/');
    }
}
