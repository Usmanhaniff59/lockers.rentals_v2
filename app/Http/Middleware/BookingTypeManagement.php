<?php

namespace App\Http\Middleware;

use Closure;

class BookingTypeManagement
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth()->user()->can('Booking Types Management')) {
             return $next($request);
        }
        return redirect('/');
    }
}
