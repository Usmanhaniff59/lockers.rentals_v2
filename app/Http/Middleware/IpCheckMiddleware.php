<?php

namespace App\Http\Middleware;

use Closure;

class IpCheckMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public $whiteIps = ['192.168.1.1','127.0.0.1','80.7.42.122','175.110.64.30','196.195.97.227',
        '45.116.232.16','119.160.66.19','45.116.232.10'];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

//        if (!in_array($request->ip(), $this->whiteIps)) {
//
//            return redirect()->route('maintenance');
//
//        }

        return $next($request);
    }

}
