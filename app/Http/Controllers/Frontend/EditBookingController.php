<?php

namespace App\Http\Controllers\Frontend;

use App\Block;
use App\BlockYears;
use App\Charity;
use App\Company;
use App\CompanySchoolYear;
use App\FrontendPlaceholder;
use App\LockerMove;
use App\Payment;
use App\paymentConfig;
use App\Sale;
use App\SalesAudit;
use App\SchoolAcademicYear;
use App\SchoolYear;
use App\Transaction;
use App\VoucherCodeUser;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class EditBookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
    }

    public function editBooking($id){

        $sales = Sale::where('booking_id',$id)->get();


        $locker_ids =[];
        $sale_ids =[];
        if(count($sales) > 0) {

            $user = User::where('id', $sales[0]->user_id)->first();
            $useremail = $user->email;

            $sales_count = $sales->count();

            $school = Company::where('id', $sales[0]->company_id)->first();

            $years = SchoolYear::where('status', 1)->get()->toArray();

            $company_school_year = CompanySchoolYear::where('company_id', $sales[0]->company_id)->orderBy('id', 'asc')->get();

            $academic_years = [];

            foreach ($company_school_year as $key => $value) {
                $x = SchoolAcademicYear::where('id', $value->school_academic_year_id)->first();
                array_push($academic_years, $x);
            }
            foreach ($sales as $sale) {

                    if(empty($sale->school_year_id)) {
                        $school_year = SchoolYear::whereDate('from_date', $sale->start)->whereDate('to_date', 'LIKE', '%' .Carbon::parse($sale->end)->format('Y-m-d').'%')->first();
                        if ($school_year) {
                            $school_year_id = $school_year->id;
                        } else {
                            $start_title = Carbon::parse($sale->start)->format('d M, Y');
                            $end_title = Carbon::parse($sale->end)->format('d M, Y');
                            $title = $start_title . ' - ' . $end_title;
                             $dontAllowDuplicate = SchoolYear::where('title', $title)->first();
                     if(empty($dontAllowDuplicate)){
                            $SchoolYear = new SchoolYear();
                            $SchoolYear->title = $title;
                            $SchoolYear->from_date = $sale->start;
                            $SchoolYear->to_date = $sale->end;
                            $SchoolYear->status = 0;
                            $SchoolYear->save();
                            $school_year_id = $SchoolYear->id;
                        }else{
                             $school_year_id = $dontAllowDuplicate->id;
                        }

                        }
                        $sale->school_year_id = $school_year_id;


                        $sale->save();

                        //add sales_audit log
                        $sales_audit_obj = new SalesAudit();
                        $sales_audit_obj->sales_audit_log($sale->id, $type = 'validate');
                    }


                    if(!empty($sale->block_id)) {
                        if (empty($sale->school_academic_year_id)) {
                            $school_year = BlockYears::where('block_id', $sale->block_id)->first();

                            if (!empty($school_year->school_academic_year_id)) {
                                $sale->school_academic_year_id = $school_year->school_academic_year_id;
                                $sale->save();

                                //add sales_audit log
                                $sales_audit_obj = new SalesAudit();
                                $sales_audit_obj->sales_audit_log($sale->id, $type = 'validate');
                            }
                        }
                    }




            }

            $placeholders = FrontendPlaceholder::where('key', 'school-term-login-user-msg')->pluck('placeholder')->toArray();

            $login_user_msg_placeholders = implode(",", $placeholders);
            \Illuminate\Support\Facades\Session::put('page_number', 3);
            $expiry = Sale::where('booking_id', $id)->select('booking_expiry')->first();
//        Session::put('company_info', $school);
            Session::put('saleKey', $id);
            Session::put('companyId', $sales[0]->company_id);
            Session::put('bookingTimer', 15);
            Session::put('schoolTermSubmit', 1);
            Session::put('schoolTermReSubmit', 1);

            Session::put('show_hide_lockers', 'Grid Hidden');
            if (Session::has('arrowsPage') == false) {
                Session::put('arrowsPage', 2);
            }
            $voucher_code_user = VoucherCodeUser::where('booking_id',$id)->first();
//            $sales = Sale::where('booking_id',$id)->orderBy('id','ASC')->get();

            foreach ($sales as $sale) {

                if(SalesAudit::where('sale_id',$sale->id)->doesntExist()){
                    //add sales_audit log
                    $sales_audit_obj = new SalesAudit();
                    $sales_audit_obj->sales_audit_log($sale->id);
                    $hold_next_year =  Sale::where('hold_sales_id',$sale->id)->first();
//                dd($hold_next_year);
                    if($hold_next_year){
                        //add sales_audit log
                        $sales_audit_obj = new SalesAudit();
                        $sales_audit_obj->sales_audit_log($hold_next_year->id);
                    }
                }else{
                    $sales_audit_obj = new SalesAudit();
                    $sales_audit_obj->update_audit_log($sale->id);
                    $hold_next_year =  Sale::where('hold_sales_id',$sale->id)->first();
//                dd($hold_next_year);
                    if($hold_next_year){
                        //add sales_audit log
                        $sales_audit_obj = new SalesAudit();
                        $sales_audit_obj->update_audit_log($hold_next_year->id);
                    }
                }
            }
            $sales_count1 = $sales->count();

            $sales = SalesAudit::where('booking_id',$id)->orderBy('id','DESC')->take($sales_count1)->get();
            $sales = $sales->reverse();

            return view('frontend.pages.editBooking.schooltermbased_edit', compact('school', 'academic_years', 'years', 'login_user_msg_placeholders', 'sales', 'sales_count', 'locker_ids', 'sale_ids', 'useremail', 'voucher_code_user'));
        }


    }
    public  function updateSaleLog(Request $request){

        //add sales_audit log
        $sale = Sale::find($request->sale_id);

        $sale_audit = new SalesAudit();
        $sale_audit->sale_id = $sale->id;
        $sale_audit->user_id = $sale->user_id;
        $sale_audit->updated_by = Auth::user()->id;
        $sale_audit->booking_id = $sale->booking_id;
        $sale_audit->number = $sale->number;
        $sale_audit->net_pass = $sale->net_pass;
        $sale_audit->company_id = $sale->company_id;
        $sale_audit->locker_id = $sale->locker_id;
        $sale_audit->block_id = $sale->block_id;
        $sale_audit->block_name = $sale->block_name;
        $sale_audit->start = $sale->start;
        $sale_audit->end = $sale->end;
        $sale_audit->price = $sale->price;
        $sale_audit->email = $sale->email;
        $sale_audit->status = $sale->status;
        $sale_audit->street = $sale->street;
        $sale_audit->city = $sale->city;
        $sale_audit->post_code = $sale->post_code;
        $sale_audit->country = $sale->country;
        $sale_audit->child_first_name = $sale->child_first_name;
        $sale_audit->child_surname = $sale->child_surname;
        $sale_audit->child_email = $sale->child_email;

        if(isset($request->shcoolYear)) {
            $sale_audit->school_year_id = $request->shcoolYear;
        }else{
            $sale_audit->school_year_id = $sale->school_year_id;
        }

        if(isset($request->shcoolAcademicYear)) {
            $sale_audit->school_academic_year_id = $request->shcoolAcademicYear;
        }else{
            $sale_audit->school_academic_year_id = $sale->school_academic_year_id;
        }

        $sale_audit->hold_sales_id = $sale->hold_sales_id;
        $sale_audit->type = 'validate';
        $sale_audit->hold_locker = $sale->hold_locker;
        $sale_audit->save();

        return response()->json('success');
    }

    public function schooltermbasedback ()
    {
//           dd(Session::has('saleKey'));
        if(Session::has('saleKey')) {
            $sale_id = Session::get('saleKey');
            $sales = Sale::where('booking_id', $sale_id)->get();
            $user = User::where('id', $sales[0]->user_id)->first();
            $useremail = $user->email;
            $sales_count = $sales->count();


            $companyId = Session::get('companyId');

            $years = SchoolYear::where('status', 1)->get()->toArray();

            $school = Company::where('id', $companyId)->first();
//            $academic_years = SchoolAcademicYear::all()->toArray();
            $academic_years = SchoolAcademicYear::Join('company_years','company_years.school_academic_year_id','=','school_academic_years.id')->where('company_years.company_id',$companyId)->select('school_academic_years.*')->get()->toArray();


            $placeholders = FrontendPlaceholder::where('key', 'school-term-login-user-msg')->pluck('placeholder')->toArray();
            $login_user_msg_placeholders = implode(",", $placeholders);

            $book_lock_desc = FrontendPlaceholder::where('key', 'booking-locker-disclaimer')->pluck('placeholder')->toArray();
            $book_lock_desc_placeholders = implode(",", $book_lock_desc);

            $book_lock_desc_cond = FrontendPlaceholder::where('key', 'booking-locker-disclaimer-condition')->pluck('placeholder')->toArray();
            $book_lock_desc_cond_placeholders = implode(",", $book_lock_desc_cond);

            Session::put('schoolTermSubmit', 1);
            Session::put('schoolTermReSubmit', 1);
            Session::put('schooltermbasedback', 1);
            $voucher_code_user = VoucherCodeUser::where('booking_id',$sale_id)->first();


                $locker_ids =[];
                $sale_ids =[];
                foreach ($sales as $sale){


                        array_push($locker_ids, $sale->locker_id);
                        array_push($sale_ids, $sale->id);

                }
                return view('frontend.pages.editBooking.schooltermbased_edit',compact('school', 'academic_years', 'years', 'login_user_msg_placeholders', 'sales', 'sales_count', 'locker_ids', 'sale_ids', 'useremail', 'book_lock_desc_placeholders', 'book_lock_desc_cond_placeholders','voucher_code_user'));


        }else{
            return  redirect('/');
        }

    }

    public function saveSchoolBooking (Request $request)
    {

        if($request->futureComunication){
            User::where('id', auth()->user()->id)->update([
                'subscription' => 1
            ]);
        }
        if(isset($request->hold_locker) ) {
            Session::put('hold_locker_checked', true);
        }

        Session::put('voucher_code_id',$request->voucher_code_id);

        if(Session::get('schooltermbasedback') == 1) {

            Session::forget('sale_id');
            Session::forget('hold_locker_id');

        }

        if(Session::get('schoolTermSubmit') == 1) {

            $date_hold = (new \App\Sale)->hold_booking_date();
            $expiry_time = (new \App\Sale)->booking_expiry_time();
//            $date_hold = $date_hold;

            Session::put('show_hide_lockers','Grid Hidden');

            if ($request['start_dateTime1'] && $request['end_dateTime1']) {

                $start_datetime = explode(' ',$request['start_dateTime1']);
                $start_day = $start_datetime[0];
                $start_month = explode(',', $start_datetime[1]);

                $start_month = Carbon::parse($start_month[0])->month;

                $start_year = $start_datetime[2];

                $start_time = explode(':',$start_datetime[3]);
                $start_hour = $start_time[0];
                $start_minutes = $start_time[1];

                $start = Carbon::create($start_year, $start_month, $start_day, $start_hour, $start_minutes, 00)->toDateTimeString();

                $end_datetime = explode(' ',$request['end_dateTime1']);
                $end_day = $end_datetime[0];
                $end_month = explode(',', $end_datetime[1]);

                $end_month = Carbon::parse($end_month[0])->month;

                $end_year = $end_datetime[2];

                $end_time = explode(':',$end_datetime[3]);

                $end_hour = $end_time[0];
                $end_minutes = $end_time[1];

                $end = Carbon::create($end_year, $end_month, $end_day, $end_hour, $end_minutes, 00)->toDateTimeString();




            }
            elseif ($request['start_date1'] && $request['end_date1']) {


                $start_date = explode(' ',$request['start_date1']);

                $start_day = $start_date[0];

                $start_month = explode(',', $start_date[1]);

                $start_month = Carbon::parse($start_month[0])->month;

                $start_year = $start_date[2];

                $start = Carbon::create($start_year, $start_month, $start_day )->toDateTimeString();

                $end_date = explode(' ',$request['end_date1']);

                $end_day = $end_date[0];

                $end_month = explode(',', $end_date[1]);

                $end_month = Carbon::parse($end_month[0])->month;

                $end_year = $end_date[2];

                $end = Carbon::create($end_year, $end_month, $end_day )->endOfDay()->toDateTimeString();


            }
            if ($request['school_year_id1']) {


                $school_year_id = $request['school_year_id1'];
                $schoolYear = SchoolYear::where('id', $school_year_id)->first();
                $splitName = explode('-', $schoolYear->title);

                $start = Carbon::parse($schoolYear->from_date)->startOfDay()->toDateTimeString();

                $end = Carbon::parse($schoolYear->to_date)->endOfDay()->toDateTimeString();


            } else {
                $school_year_id = null;
            }

            $locationGroup = Company::where('id', $request['company_id'])->select('location_group_id')->first();
            $language = Session::get('locale');

            if (Session::get('schoolTermReSubmit')== 1) {

                Session::put('bookingTimer', 15);

                $expired_time = Carbon::now()->addMinutes(15);
                $bookings = Sale::where('booking_id', $request['booking_id'])->orderBy('id', 'ASC')->get();

                foreach ($bookings as $i => $booking) {
                    $i++;

                    if ($request['start_dateTime' . $i] && $request['end_dateTime' . $i]) {

                        //start date Time format
                        $start_datetime = explode(' ',$request['start_dateTime'. $i]);
                        $start_day = $start_datetime[0];
                        $start_month = explode(',', $start_datetime[1]);

                        $start_month = Carbon::parse($start_month[0])->month;

                        $start_year = $start_datetime[2];

                        $start_time = explode(':',$start_datetime[3]);
                        $start_hour = $start_time[0];
                        $start_minutes = $start_time[1];

                        $start = Carbon::create($start_year, $start_month, $start_day, $start_hour, $start_minutes, 00)->toDateTimeString();
                        //end date Time format
                        $end_datetime = explode(' ',$request['end_dateTime'. $i]);
                        $end_day = $end_datetime[0];
                        $end_month = explode(',', $end_datetime[1]);

                        $end_month = Carbon::parse($end_month[0])->month;

                        $end_year = $end_datetime[2];

                        $end_time = explode(':',$end_datetime[3]);

                        $end_hour = $end_time[0];
                        $end_minutes = $end_time[1];

                        $end = Carbon::create($end_year, $end_month, $end_day, $end_hour, $end_minutes, 00)->toDateTimeString();


                    }
                    elseif ($request['start_date' . $i] && $request['end_date' . $i]) {
                        $start_date = explode(' ',$request['start_date'.$i]);

                        $start_day = $start_date[0];

                        $start_month = explode(',', $start_date[1]);

                        $start_month = Carbon::parse($start_month[0])->month;

                        $start_year = $start_date[2];

                        $start = Carbon::create($start_year, $start_month, $start_day )->toDateTimeString();

                        $end_date = explode(' ',$request['end_date'.$i]);

                        $end_day = $end_date[0];

                        $end_month = explode(',', $end_date[1]);

                        $end_month = Carbon::parse($end_month[0])->month;

                        $end_year = $end_date[2];

                        $end = Carbon::create($end_year, $end_month, $end_day )->endOfDay()->toDateTimeString();

                    }
                    else {
                        $start = null;
                        $end = null;
                    }

                    if ($request['school_year_id' . $i]) {

                        $school_year_id = $request['school_year_id' . $i];
                        $schoolYear = SchoolYear::where('id', $school_year_id)->first();

                        $start = Carbon::parse($schoolYear->from_date)->startOfDay()->toDateTimeString();

                        $end = Carbon::parse($schoolYear->to_date)->endOfDay()->toDateTimeString();


                    }
                    else {
                        $school_year_id = null;
                    }

                    $sale = Sale::find($booking->id);
                    if(!auth()->user()->can('3D Secure')) {
                        $sale->user_id = $request['user_id'];
                    }

                    if (isset($language)) {
                        if ($language == 'fr') {
                            $sale->lang = 'FR';
                        } else {
                            $sale->lang = 'EN';
                        }
                    } else {
                        $sale->lang = 'EN';
                    }
                    $sale->title = 'LOCKER';
                    $sale->lock_type = 'm';
                    $sale->company_id = $request['company_id'];

                    $sale->locker_id = $request['locker_id'. $i];
                    $sale->block_id = $request['block_id'. $i];
                    $sale->start = $start;
                    $sale->end = $end;
                    if(!auth()->user()->can('3D Secure')) {
                        $sale->email = $request['userEmail'];
                    }
//                    if ($request['locker_id' . $i] != 0 && $request['block_id' . $i] != 0) {

//                    } else {
//                        $sale->status = 0;
//                    }

                    if ($request['hold_booking_date'. $i] >= Carbon::now()->subHour()->toDateTimeString() && $request['hold_booking_date'. $i] <= Carbon::now()->toDateTimeString()) {
                        $sale->hold_booking_date = Carbon::parse($request['hold_booking_date'. $i])->addHour()->toDateTimeString();
                    }
                    $sale->confirmation_sent = 0;
                    $sale->delete = 0;
                    $sale->street = $request['street'];
                    $sale->city = $request['city'];
                    $sale->post_code = $request['post_code'];
                    $sale->country = $request['country'];
                    $sale->child_first_name = $request['child_first_name' . $i];
                    $sale->child_surname = $request['child_surname' . $i];
                    if ($request['child_email' . $i]) {
                        $sale->child_email = $request['child_email' . $i];
                    }
                    $sale->school_year_id = $school_year_id;
                    if ($request['hold_locker']) {
                        if ($request['hold_locker'. $i]) {
                            $sale->hold_locker = 1;
                        }else{
                            $sale->hold_locker = 0;
                        }
                    }else{
                        $sale->hold_locker = 0;
                    }
                    if ($locationGroup->location_group_id == 1) {
                        $sale->school_academic_year_id = $request['school_academic_year_id' . $i];
                    }
                    $sale->booking_expiry = $expiry_time;
                    $sale->save();

                    //add sales_audit log
                    $sales_audit_obj = new SalesAudit();
                    $sales_audit_obj->sales_audit_log($sale->id,$type = 'validate');
                }
//                $expiry = Sale::where('booking_id',$sale_id)->select('booking_expiry')->first();

                if($request['child_count'] > $i){

                    for ($j = $i+1; $j <= $request['child_count']; $j++) {

                        if ($request['start_dateTime' . $j] && $request['end_dateTime' . $j]) {
                            //start date Time format
                            $start_datetime = explode(' ',$request['start_dateTime'. $j]);
                            $start_day = $start_datetime[0];
                            $start_month = explode(',', $start_datetime[1]);

                            $start_month = Carbon::parse($start_month[0])->month;

                            $start_year = $start_datetime[2];

                            $start_time = explode(':',$start_datetime[3]);
                            $start_hour = $start_time[0];
                            $start_minutes = $start_time[1];

                            $start = Carbon::create($start_year, $start_month, $start_day, $start_hour, $start_minutes, 00)->toDateTimeString();
                            //end date Time format
                            $end_datetime = explode(' ',$request['end_dateTime'. $j]);
                            $end_day = $end_datetime[0];
                            $end_month = explode(',', $end_datetime[1]);

                            $end_month = Carbon::parse($end_month[0])->month;

                            $end_year = $end_datetime[2];

                            $end_time = explode(':',$end_datetime[3]);

                            $end_hour = $end_time[0];
                            $end_minutes = $end_time[1];

                            $end = Carbon::create($end_year, $end_month, $end_day, $end_hour, $end_minutes, 00)->toDateTimeString();


                        }
                        elseif ($request['start_date' . $j] && $request['end_date' . $j]) {
                            $start_date = explode(' ',$request['start_date'.$j]);

                            $start_day = $start_date[0];

                            $start_month = explode(',', $start_date[1]);

                            $start_month = Carbon::parse($start_month[0])->month;

                            $start_year = $start_date[2];

                            $start = Carbon::create($start_year, $start_month, $start_day )->toDateTimeString();

                            $end_date = explode(' ',$request['end_date'.$j]);

                            $end_day = $end_date[0];

                            $end_month = explode(',', $end_date[1]);

                            $end_month = Carbon::parse($end_month[0])->month;

                            $end_year = $end_date[2];

                            $end = Carbon::create($end_year, $end_month, $end_day )->endOfDay()->toDateTimeString();


                        }
                        else {
                            $start = null;
                            $end = null;
                        }

                        if ($request['school_year_id' . $j]) {

                            $school_year_id = $request['school_year_id' . $j];
                            $schoolYear = SchoolYear::where('id', $school_year_id)->first();
                            $splitName = explode('-', $schoolYear->title);

                            $start = Carbon::parse($schoolYear->from_date)->startOfDay()->toDateTimeString();

                            $end = Carbon::parse($schoolYear->to_date)->endOfDay()->toDateTimeString();

                        } else {
                            $school_year_id = null;
                        }


                        $sale = new Sale();
//                        if(!auth()->user()->can('3D Secure')) {
                        $sale->user_id = $request['user_id'];
//                        }
                        $sale->booking_id = $request['booking_id'];
                        $sale->booking_type = 1;
                        if (isset($language)) {
                            if ($language == 'fr') {
                                $sale->lang = 'FR';
                            } else {
                                $sale->lang = 'EN';
                            }
                        } else {
                            $sale->lang = 'EN';
                        }
                        $sale->title = 'LOCKER';
                        $sale->lock_type = 'm';
                        $sale->company_id = $request['company_id'];

                        $sale->start = $start;
                        $sale->end = $end;
                        $sale->email = $request['userEmail'];
//                        if ($request['locker_id' . $j] != 0 && $request['block_id' . $j] != 0) {

//                        } else {
//                            $sale->status = 0;
//                        }
                        if ($request['booking_id']) {
                            $hold_sale  = Sale::find($request['booking_id']);
//                            dd($hold_sale->hold_booking_date);
                            if ($hold_sale->hold_booking_date >= Carbon::now()->subHour()->toDateTimeString() && $hold_sale->hold_booking_date <= Carbon::now()->toDateTimeString()) {
                                $hold_booking_date = Carbon::parse($hold_sale->hold_booking_date)->addHour()->toDateTimeString();
                            }else{
                                $hold_booking_date =  $hold_sale->hold_booking_date;
                            }
                        }else{
                            $hold_booking_date =  Carbon::now()->addHour();
                        }
                        $sale->hold_booking_date = $hold_booking_date;
                        $sale->confirmation_sent = 0;
                        $sale->delete = 0;
                        $sale->street = $request['street'];
                        $sale->city = $request['city'];
                        $sale->post_code = $request['post_code'];
                        $sale->country = $request['country'];
                        $sale->child_first_name = $request['child_first_name' . $j];
                        $sale->child_surname = $request['child_surname' . $j];
                        if ($request['child_email' . $j]) {
                            $sale->child_email = $request['child_email' . $j];
                        }

                        $sale->school_year_id = $school_year_id;
                        if ($request['hold_locker' . $j]) {
                            if ($request['hold_locker'. $j]) {
                                $sale->hold_locker = 1;
                            }else{
                                $sale->hold_locker = 0;
                            }
                        }else{
                            $sale->hold_locker = 0;
                        }
                        if ($locationGroup->location_group_id == 1) {
                            $sale->school_academic_year_id = $request['school_academic_year_id' . $j];
                        }
                        $sale->booking_expiry = $expiry_time;
                        $sale->save();

                        //add sales_audit log
                        $sales_audit_obj = new SalesAudit();
                        $sales_audit_obj->sales_audit_log($sale->id);
                    }

                }


            } else {

                $sale = new Sale();
                $sale->user_id = $request['user_id'];
                $sale->booking_type = 1;
                if (isset($language)) {
                    if ($language == 'fr') {
                        $sale->lang = 'FR';
                    } else {
                        $sale->lang = 'EN';
                    }
                } else {
                    $sale->lang = 'EN';
                }
                $sale->title = 'LOCKER';

                $sale->lock_type = 'm';
                $sale->company_id = $request['company_id'];
                $sale->locker_id = $request['locker_id1'];
                $sale->block_id = $request['block_id1'];
                $sale->start = $start;
                $sale->end = $end;
                $sale->email = $request['userEmail'];
//                if ($request['locker_id1'] != 0 && $request['block_id1'] != 0) {
                $sale->status = 'r';
//                } else {
//                    $sale->status = 0;
//                }
                $sale->hold_booking_date = Carbon::now()->addHour();
                $sale->confirmation_sent = 0;
                $sale->delete = 0;
                $sale->street = $request['street'];
                $sale->city = $request['city'];
                $sale->post_code = $request['post_code'];
                $sale->country = $request['country'];
                $sale->child_first_name = $request['child_first_name1'];
                $sale->child_surname = $request['child_surname1'];
                if ($request['child_email1']) {
                    $sale->child_email = $request['child_email1'];
                }
                $sale->school_year_id = $school_year_id;
                if ($request['hold_locker']) {
                    Session::put('hold_locker_page2_checked',true);
                    if ($request['hold_locker1']) {
                        $sale->hold_locker = 1;
                    }
                }
                if ($locationGroup->location_group_id == 1) {
                    $sale->school_academic_year_id = $request['school_academic_year_id1'];
                }
                $sale->booking_expiry = $expiry_time;
                $sale->save();
                $sale_id = $sale->id;

                Session::put('sale_id', $sale_id);

                Sale::where('id', $sale_id)->update(['booking_id' => $sale_id]);


                if ($request['child_count'] > 1) {

                    for ($i = 2; $i <= $request['child_count']; $i++) {

                        if ($request['start_dateTime' . $i] && $request['end_dateTime' . $i]) {

                            //start date Time format
                            $start_datetime = explode(' ',$request['start_dateTime'. $i]);
                            $start_day = $start_datetime[0];
                            $start_month = explode(',', $start_datetime[1]);

                            $start_month = Carbon::parse($start_month[0])->month;

                            $start_year = $start_datetime[2];

                            $start_time = explode(':',$start_datetime[3]);
                            $start_hour = $start_time[0];
                            $start_minutes = $start_time[1];

                            $start = Carbon::create($start_year, $start_month, $start_day, $start_hour, $start_minutes, 00)->toDateTimeString();
                            //end date Time format
                            $end_datetime = explode(' ',$request['end_dateTime'. $i]);
                            $end_day = $end_datetime[0];
                            $end_month = explode(',', $end_datetime[1]);

                            $end_month = Carbon::parse($end_month[0])->month;

                            $end_year = $end_datetime[2];

                            $end_time = explode(':',$end_datetime[3]);

                            $end_hour = $end_time[0];
                            $end_minutes = $end_time[1];

                            $end = Carbon::create($end_year, $end_month, $end_day, $end_hour, $end_minutes, 00)->toDateTimeString();


                        }
                        elseif ($request['start_date' . $i] && $request['end_date' . $i]) {
                            $start_date = explode(' ',$request['start_date'.$i]);

                            $start_day = $start_date[0];

                            $start_month = explode(',', $start_date[1]);

                            $start_month = Carbon::parse($start_month[0])->month;

                            $start_year = $start_date[2];

                            $start = Carbon::create($start_year, $start_month, $start_day )->toDateTimeString();

                            $end_date = explode(' ',$request['end_date'.$i]);

                            $end_day = $end_date[0];

                            $end_month = explode(',', $end_date[1]);

                            $end_month = Carbon::parse($end_month[0])->month;

                            $end_year = $end_date[2];

                            $end = Carbon::create($end_year, $end_month, $end_day )->endOfDay()->toDateTimeString();

                        }
                        else {
                            $start = null;
                            $end = null;
                        }

                        if ($request['school_year_id' . $i]) {

                            $school_year_id = $request['school_year_id' . $i];
                            $schoolYear = SchoolYear::where('id', $school_year_id)->first();

                            $start = Carbon::parse($schoolYear->from_date)->startOfDay()->toDateTimeString();

                            $end = Carbon::parse($schoolYear->to_date)->endOfDay()->toDateTimeString();


                        } else {
                            $school_year_id = null;
                        }

                        $sale = new Sale();
                        $sale->user_id = $request['user_id'];
                        $sale->booking_id = $sale_id;
                        $sale->booking_type = 1;
                        if (isset($language)) {
                            if ($language == 'fr') {
                                $sale->lang = 'FR';
                            } else {
                                $sale->lang = 'EN';
                            }
                        } else {
                            $sale->lang = 'EN';
                        }
                        $sale->title = 'LOCKER';

                        $sale->lock_type = 'm';
                        $sale->company_id = $request['company_id'];
                        $sale->locker_id = $request['locker_id' . $i];
                        $sale->block_id = $request['block_id' . $i];
                        $sale->start = $start;
                        $sale->end = $end;
                        $sale->email = $request['userEmail'];
//                        if ($request['locker_id' . $i] != 0 && $request['block_id' . $i] != 0) {
                        $sale->status = 'r';
//                        } else {
//                            $sale->status = 'r';
//                        }
                        $sale->hold_booking_date = Carbon::now()->addHour();
                        $sale->confirmation_sent = 0;
                        $sale->delete = 0;
                        $sale->street = $request['street'];
                        $sale->city = $request['city'];
                        $sale->post_code = $request['post_code'];
                        $sale->country = $request['country'];
                        $sale->child_first_name = $request['child_first_name' . $i];
                        $sale->child_surname = $request['child_surname' . $i];
                        if ($request['child_email' . $i]) {
                            $sale->child_email = $request['child_email' . $i];
                        }
                        $sale->school_year_id = $school_year_id;
                        if ($request['hold_locker']) {
                            if ($request['hold_locker' . $i]) {
                                $sale->hold_locker = 1;
                            }
                        }
                        if ($locationGroup->location_group_id == 1) {
                            $sale->school_academic_year_id = $request['school_academic_year_id' . $i];
                        }

                        $sale->booking_expiry = $expiry_time;
                        $sale->save();

                        //add sales_audit log
                        $sales_audit_obj = new SalesAudit();
                        $sales_audit_obj->sales_audit_log($sale->id);

                    }
                }

                Session::put('saleKey', $sale_id);
                Session::put('schoolTermSubmit', 2);
                Session::forget('schooltermbasedback');
            }
        }

        if(Session::has('saleKey')) {

            $voucher_code_id = $request['voucher_code_id'];
            $booking_id = Session::get('saleKey');

            return  $this->show_bookings($booking_id);

        }else{

            Session::forget('arrowsPage');
            return redirect('/');
        }

    }

    public function show_bookings($booking_id)
    {

        $childrens = Sale::where('booking_id', $booking_id)->get();

        $childrens_count = $childrens->count();

        $child_blocks = [];

        if (Carbon::now() >= Carbon::parse($childrens[0]->hold_booking_date)->subHour()->toDateTimeString() && Carbon::now() <= $childrens[0]->hold_booking_date) {
            $hold_booking_date = (new \App\Sale)->hold_booking_date();
        }

        foreach ($childrens as $index => $children) {
            $index++;

            $location_group = Sale::join('companies', 'sales.company_id', '=', 'companies.id')
                ->where('sales.id', $children->id)
                ->select('companies.location_group_id')->first();

            $location_group = $location_group->location_group_id;

            if ($location_group == 1) {

                $schoolYear = SchoolYear::where('id', $children->school_year_id)->first();

                $start = Carbon::parse($schoolYear->from_date)->startOfDay()->toDateTimeString();
                $end = Carbon::parse($schoolYear->to_date)->endOfDay()->toDateTimeString();

                if(Session::get('hold_locker_checked') == true){
                    $book_next_year =true;
                }else{
                    $book_next_year =false;
                }

                $sales = new Sale();
                $blocks = $sales->school_year_blocks($children->company_id , $children->school_academic_year_id);
//                 dd($blocks);
                if (count($blocks) > 0) {
                    $options = '';
                    $options .= '<option value="" selected>Choose Block</option>';

                    foreach ($blocks as $block) {
                        $block = Block::find($block->block_id);

                        $sales = new Sale();
                        $available_lockers = $sales->total_available_lockers_in_block($block->id,$start,$end,$book_next_year);

                        if($available_lockers['total_available'] > 0 || $children->block_id == $block->id) {

                            $rates = $sales->rate_calculation_edit($location_group,$block->id,$children->start,$children->end,$children->booking_id,$children->discount_percent);
                            $unit = $rates[0];
                            $tax = $rates[1];

                            $sale = Sale::find($children->id);
                            if (Carbon::now() >= Carbon::parse($children->hold_booking_date)->subHour()->toDateTimeString() && Carbon::now() <= $children->hold_booking_date) {

                                $sale->hold_booking_date = $hold_booking_date;
                            }
                            if ($children->block_id == $block->id ) {
                                $sale->price = $unit;
                                $sale->price_original = $unit;
                                $sale->tax = $tax;
                            }

                            $sale->save();


                            $currency = Company::join('currencies', 'companies.currency', '=', 'currencies.currency_name')
                                ->where('companies.id', $children->company_id)
                                ->select('currencies.currency_name', 'currencies.symbol', 'currencies.position')->first();

                            if ($currency->position == 'b') {

                                $options .= '<option  value="' . $block->id .'_'.$unit.'_'.$tax. '" ';
                                if ($block->id == $children->block_id) {
                                    $options .= 'selected';
                                }
                                $options .= '>' . $block->name . ' ' . $currency->symbol . '' . $unit . '</option>';

                            } else {

                                $options .= '<option  value="' . $block->id .'_'.$unit.'_'.$tax. '" ';
                                if ($block->id == $children->block_id) {
                                    $options .= 'selected';
                                }
                                $options .= '>' . $block->name . ' ' . $unit . ' ' . $currency->symbol . '</option>';
                            }
                        }

                    }

                    $options .= '</select>';
//                        dd($unit);

                    $select_html = '<select class="form-control chzn-select blocks fa " id="blocks" data-sale_id="' . $children->id . '" data-start="' . $start . '" data-end="' . $end . '"   data-counter="' . $index . '"   name="block">';

                    $blocks_html = $select_html . '' . $options;

                    array_push($child_blocks, $blocks_html);
//                        dd($child_blocks);

                } else {

                    $options = '';
                    $options .= '<select class="form-control chzn-select  fa " id="blocks" data-sale_id="' . $children->id . '" data-start="' . $start . '" data-end="' . $end . '" data-counter="' . $index . '" name="block">';
                    $options .= '<option  value="" selected>Choose Block</option>';
                    $options .= '</select>';

                    array_push($child_blocks, $options);
                }

            }
            elseif ($location_group == 2 || $location_group == 3){

                $start = Carbon::parse($children->start)->format('Y-m-d H:i:s');
                $end = Carbon::parse($children->end)->format('Y-m-d H:i:s');
                $schoolYear = 0;


                $blocks = Block::where('company_id', $children->company_id)->where('active', 1)->where('display', 1)
                    ->orderBy('name', 'ASC')->get();


                if (count($blocks) > 0) {


                    $options = '';

                    $options .= '<option  value="" selected>Choose Block</option>';



                    foreach ($blocks as $block) {

                        $book_next_year = false;

                        $sales = new Sale();
                        $available_lockers =  $sales->total_available_lockers_in_block($block->id,$start,$end,$book_next_year);

                        if($available_lockers['total_available'] > 0 || $children->block_id == $block->id) {




                            $sales = new Sale();
                            $rates = $sales->rate_calculation_edit($location_group , $block->id,$start,$end,$children->booking_id,$children->discount_percent);
                            $lowest_rate = $rates[0];
                            $tax = $rates[1];

                            $currency = Company::join('currencies', 'companies.currency', '=', 'currencies.currency_name')
                                ->where('companies.id', $block->company_id)
                                ->select('currencies.currency_name', 'currencies.symbol', 'currencies.position')->first();

//

                            if ($currency->position == 'b') {

                                $options .= '<option  value="' . $block->id . '_' . $lowest_rate . '_' . $tax . '" ';
                                if ($block->id == $children->block_id) {
                                    $options .= 'selected';
                                }
                                $options .= '>' . $block->name . ' ' . $currency->symbol . '' . $lowest_rate . '</option>';
                            } else {
                                $options .= '<option  value="' . $block->id . '_' . $lowest_rate . '_' . $tax . '" ';
                                if ($block->id == $children->block_id) {
                                    $options .= 'selected';
                                }
                                $options .= '>' . $block->name . ' ' . $lowest_rate . ' ' . $currency->symbol . '</option>';
                            }

                        }
                    }

                    $options .= '</select>';

                    $select_html = '<select class="form-control chzn-select blocks fa child'.$index.'" id="blocks' . $index . '" data-sale_id="' . $children->id . '" data-start="' . $start . '" data-end="' . $end . '" data-end="' . $end . '" data-rate="' . $lowest_rate . '" data-tax="' . $tax . '" data-counter="' . $index . '" name="block' . $index . '">';

                    $blocks_html = $select_html . '' . $options;

                    array_push($child_blocks, $blocks_html);

                } else {

                    $options = '';
                    $options .= '<select class="form-control chzn-select blocks child'.$index.' fa " id="blocks' . $index . '" data-sale_id="' . $children->id . '" data-start="' . $start . '" data-end="' . $end . '" data-counter="' . $index . '" name="block' . $index . '">';
                    $options .= '<option  value="" selected>Choose Block</option>';
                    $options .= '</select>';

                    array_push($child_blocks, $options);
                }
            }
        }

        $expiry = Sale::where('booking_id',$booking_id)->select('booking_expiry')->first();

        $expiryTime =  Carbon::parse($expiry->booking_expiry);

        if( $expiryTime > Carbon::now()) {
            $remainingTime = $expiryTime->diffInSeconds(Carbon::now());

            if(Session::get('arrowsPage')== 2) {
                Session::put('arrowsPage', 3);
                Session::put('page_number', 3);
            }

            $placeholders = FrontendPlaceholder::where('key','choose-locker-child-name-date-title')->pluck('placeholder')->toArray();
            $child_name_date_placeholders = implode(",", $placeholders);
//                dd($childrens);
            return view('frontend.pages.editBooking.choose_locker_edit', compact('childrens', 'childrens_count', 'location_group', 'schoolYear', 'child_blocks', 'booking_id', 'remainingTime', 'child_name_date_placeholders'));

        }else{

            Session::forget('arrowsPage');
            return redirect('/');
        }

    }

    public function saveSchoolBookingBack()
    {

        if(Session::has('saleKey')) {

            $booking_id = Session::get('saleKey');

            return  $this->show_bookings($booking_id);

        }else{

            Session::forget('arrowsPage');
            return redirect('/');
        }



    }



    public function saveSchoolBookingforward ()
    {

        if(Session::has('saleKey')) {

            $booking_id = Session::get('saleKey');

            $companyIfo = Sale::Join('lockers','lockers.id', '=', 'sales.locker_id')
                ->Join('blocks','blocks.id', '=', 'sales.block_id')
                ->Join('companies','companies.id', '=', 'sales.company_id')
                ->Join('users','users.id', '=', 'sales.user_id')
                ->select('blocks.name as block_name','lockers.locker_number','sales.id as sale_id','companies.id as company_id','companies.location_group_id','sales.booking_id as booking_id', 'users.id as user_id','users.name','users.surname','users.username','users.email','users.address_1','users.address_2','users.city as user_city','users.post_code as user_post_code','users.state as user_state', 'sales.lang','sales.lock_type','sales.price','sales.tax','companies.name as company_name','companies.street as company_street','companies.city as company_city','companies.country as company_country')
                ->where('sales.booking_id',$booking_id)
                ->first();

            $lockerInfo = Sale::Join('lockers','lockers.id', '=', 'sales.locker_id')
                ->Join('blocks','blocks.id', '=', 'sales.block_id')
                ->Join('companies','companies.id', '=', 'sales.company_id')
                ->select('blocks.name as block_name','lockers.locker_number','sales.id as sale_id','sales.booking_id as booking_id','sales.start','sales.end','sales.child_email',DB::raw("CONCAT(sales.child_first_name, ' ' ,sales.child_surname) AS childName"),'sales.price','sales.tax')
                ->where('sales.booking_id',$booking_id)
                ->get();

            $total_price = 0;

            foreach ($lockerInfo as $locker){

                $total_price += $locker->price;
                $total_price = number_format((float)$total_price, 2, '.', '');
            }



            $currency = Company::join('currencies', 'companies.currency', '=', 'currencies.currency_name')
                ->where('companies.id', $companyIfo->company_id)
                ->select('currencies.currency_name', 'currencies.symbol', 'currencies.position')->first();

            $charity = Charity::first();
            $placeholders = FrontendPlaceholder::where('key', 'payments-charity-card-main-disclaimer')->pluck('placeholder')->toArray();
            $charity_declaimer_placeholder = implode(",", $placeholders);

            $expiryTime = Carbon::now()->addMinutes(15);
                $remainingTime = $expiryTime->diffInSeconds(Carbon::now());
             $payment = Payment::where('booking_id',$booking_id)->first();
                return view('frontend.pages.editBooking.payments_edit',compact('booking_id','companyIfo','lockerInfo','currency','total_price','remainingTime', 'charity', 'charity_declaimer_placeholder','payment'));

        }else{
            return  redirect('/');
        }



    }

    public function payments (Request $request)
    {

        if(Session::has('saleKey')) {
            if (Session::get('arrowsPage') == 3) {
                Session::put('arrowsPage', 4);
                Session::put('page_number', 4);
            }
            $booking_id = $request->sale_id;

            $companyIfo = Sale::Join('lockers','lockers.id', '=', 'sales.locker_id')
                ->Join('blocks','blocks.id', '=', 'sales.block_id')
                ->Join('companies','companies.id', '=', 'sales.company_id')
                ->Join('users','users.id', '=', 'sales.user_id')
                ->select('blocks.name as block_name','lockers.locker_number','sales.id as sale_id','companies.id as company_id','companies.location_group_id','sales.booking_id as booking_id', 'users.id as user_id','users.name','users.surname','users.username','users.email','users.address_1','users.address_2','users.city as user_city','users.post_code as user_post_code','users.state as user_state', 'sales.lang','sales.lock_type','sales.price','sales.tax','companies.name as company_name','companies.street as company_street','companies.city as company_city','companies.country as company_country')
                ->where('sales.booking_id',$booking_id)
                ->first();
//            dd($companyIfo);


            $lockerInfo = Sale::Join('lockers', 'lockers.id', '=', 'sales.locker_id')
                ->Join('blocks', 'blocks.id', '=', 'sales.block_id')
                ->Join('companies', 'companies.id', '=', 'sales.company_id')
                ->select('blocks.name as block_name', 'lockers.locker_number', 'sales.id as sale_id', 'sales.booking_id as booking_id', 'sales.start', 'sales.end', 'sales.child_email', DB::raw("CONCAT(sales.child_first_name, ' ' ,sales.child_surname) AS childName"), 'sales.price', 'sales.tax')
                ->where('sales.booking_id', $booking_id)
                ->get();

            $total_price = 0;


            foreach ($lockerInfo as $locker) {

                $total_price += $locker->price;
                $total_price = number_format((float)$total_price, 2, '.', '');
            }

            $expiry = Sale::where('booking_id', $booking_id)->select('booking_expiry')->first();
            $expiryTime = Carbon::parse($expiry->booking_expiry);

            $currency = Company::join('currencies', 'companies.currency', '=', 'currencies.currency_name')
                ->where('companies.id', $companyIfo->company_id)
                ->select('currencies.currency_name', 'currencies.symbol', 'currencies.position')->first();

            $charity = Charity::first();

            $placeholders = FrontendPlaceholder::where('key', 'payments-edit-add-another-transaction')->pluck('placeholder')->toArray();
            $add_another_transaction_placeholders = implode(",", $placeholders);

                $remainingTime = $expiryTime->diffInSeconds(Carbon::now());

                $payment = Payment::where('booking_id',$booking_id)->first();
//                dd($payment);

                return view('frontend.pages.editBooking.payments_edit', compact('booking_id', 'companyIfo', 'lockerInfo', 'currency', 'total_price', 'remainingTime', 'charity', 'add_another_transaction_placeholders','payment'));

        }else{
            return redirect('/');
        }



    }

    public function updateSale (Request $request)
    {

        Session::forget('schoolTermSubmit');
        if(Session::has('saleKey')) {

            $sale = Sale::where('id',$request['sale_id'])->first();
            $company_id = $sale->company_id;
            $start = $sale->start;
            $end = $sale->end;

            $booked_lockers = Sale::where('company_id',$company_id)
                ->where('locker_id',$request['locker_id'])
                ->where('block_id',$request['block_id'])
                ->where(function ($query){
                    $query->where('status','b')
                        ->orWhere('status','r');
                })
                ->where(function ($query) use ($start,$end) {
                    $query->where('end','>=',$start)
                        ->Where('start','<=',$end);
                })
                ->first();
            if($booked_lockers){
                $data = [
                    'status' => 'already_exists',
                    'locker_id' => $request['locker_id']
                ];

            }else {
                $block = Block::find($request['block_id']);

                $locker_move = new LockerMove();
                $locker_move->sale_id = $sale->id;
                $locker_move->booking_id = $sale->booking_id;
                $locker_move->old_locker_id = $sale->locker_id;
                $locker_move->old_locker_number = $sale->number;
                $locker_move->new_locker_id = $request['locker_id'];
                $locker_move->new_locker_number = $request['locker_number'];
                $locker_move->save();

                $sale->locker_id = $request['locker_id'];
                $sale->block_id = $request['block_id'];
                $sale->block_name = $block->name;
                $sale->number = $request['locker_number'];
                $sale->price = $request['rate'];
                $sale->price_original = $request['rate'];
                $sale->tax = $request['tax'];
                $sale->save();


                //update Manual locker code using sale_id
                $code= new Sale();
                $code->update_locker_code($request['sale_id']);

                //add sales_audit log
                $sales_audit_obj = new SalesAudit();
                $sales_audit_obj->sales_audit_log($request['sale_id'],$type = 'validate');

                $hold_next_year_booking = Sale::where('hold_sales_id',$request['sale_id'])->first();

                if($hold_next_year_booking) {

                    $locker_move = new LockerMove();
                    $locker_move->sale_id = $hold_next_year_booking->id;
                    $locker_move->booking_id = $hold_next_year_booking->booking_id;
                    $locker_move->old_locker_id = $hold_next_year_booking->locker_id;
                    $locker_move->old_locker_number = $hold_next_year_booking->number;
                    $locker_move->new_locker_id = $request['locker_id'];
                    $locker_move->new_locker_number = $request['locker_number'];
                    $locker_move->save();

                    $hold_next_year_booking->locker_id = $request['locker_id'];
                    $hold_next_year_booking->block_id = $request['block_id'];
                    $hold_next_year_booking->block_name = $block->name;
                    $hold_next_year_booking->number = $request['locker_number'];
                    $hold_next_year_booking->save();

                    $code= new Sale();
                    $code->update_locker_code($hold_next_year_booking->id);

                    //add sales_audit log
                    $sales_audit_obj = new SalesAudit();
                    $sales_audit_obj->sales_audit_log($hold_next_year_booking->id,$type = 'validate');



                }

                $data = [
                    'status' => 'available',
                    'locker_id' => $request['locker_id']
                ];

            }
            return response()->json($data);

        }else{
            return  redirect('/');
        }

    }

    public function savepayments (Request $request)
    {


        if(Session::has('saleKey')) {
//            Artisan::call('sagepay:status');
            $booking_id = $request->sale_id;

            $company_currency = new Payment();
            $currency = $company_currency->company_currency($booking_id);
            if($request->ajax()){
                $ajax = true;
            }else{
                $ajax = false;
            }
            $phone_number = null;
            if ($request->phone_number) {
                $request_phone = $request->phone_number[0];
                if ($request_phone == "0") {
                    $phone_number = "+44" . substr($request->phone_number, 1);

                } else if ($request_phone == "+") {
                    $phone_number = $request->phone_number;
                } else {
                    $phone_number = "+44" . $request->phone_number;
                }

                $locker_code = new Payment();
                $locker_code->update_phone_number($booking_id,$phone_number);

            }
            $user_id = Auth::user()->id;
            $user_email = Auth::user()->email;
            $first_name = $request['firstname'];
            $last_name = $request['payer_surname'];
            $billing_address1 = $request['billing_address1'];
            $billing_address2 = $request['billing_address2'];
            $city = $request['city'];
            $country = $request['country'];
            $charity_id = $request['charity_id'];
//            $phone_number = $request['phone_number'];

            $post_code = $request['post_code'];
            $state = $request['state'];
            $last4Digits = substr(str_replace(' ', '', $request['cardNumber']), -4);

//            dd($request->charity_text_custom);
            $price_calc = new Payment();
            $price = $price_calc->price_calculation($booking_id, $request->charity_text_custom);
//dd($price);
            $total_lockers_price = $price[0];
            $charity_price = $price[1];
            $total_price = $price[2];


            $payment_type = $request['payment_type'];
            if ($request['payment_type'] == 'Virtual') {
                $transaction_type = '01';
            } else {
                $transaction_type = '02';
            }

            $booking_id_in_payments = Payment::where('booking_id',$booking_id)->first();
//            $booking_id_in_transaction = Payment::where('booking_id',$booking_id)->where('waiting_response_sagepay',1)->first();

            if(empty($booking_id_in_payments)) {

                $payment = new Payment();
                $payment_id = $payment->insert_payments($user_id, $booking_id,$billing_address1,$billing_address2,$city,$country,$first_name,$phone_number,$post_code,$state,$last_name,$currency,$user_email);

                $fraud_score = 0;
                $amount = null;
                $transactionId = null;
                $vendorTxCode = 'lockers_' . Carbon::now()->format('Ymdhis') . '_' . $booking_id;
                $status_code = '2000.xweb';
                $status_message = 'No Response';
                $waiting_response_sagepay =1;
                $transaction = new Payment();
                $transaction->insert_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $amount, $transaction_type, $status_code, $vendorTxCode, $status_message, $fraud_score, $transactionId,$waiting_response_sagepay);


            }else {

                $payment_id =  $booking_id_in_payments->id;
//                $transaction_id =  $booking_id_in_transaction->id;
            }
            $payment = Payment::find($payment_id);

            if(empty($payment->amount) || $payment->amount == '0.00' ){

                Session::put('user_payments_form_info', $request->all());
                Session::put('on_payments_booking_id', $booking_id);

                $Authorization = paymentConfig::integrationKey();
                $merchant_session_url = \Config::get('app.merchant_session_url');

                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $merchant_session_url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => '{ "vendorName": "lockers" }',
                    CURLOPT_HTTPHEADER => array(
                        "Authorization: Basic $Authorization",
                        "Cache-Control: no-cache",
                        "Content-Type: application/json"
                    ),
                ));
                $session_response = curl_exec($curl);

                $err = curl_error($curl);

                curl_close($curl);

                $session_response = json_decode($session_response);

                if (isset($session_response->merchantSessionKey)) {
                    $merchantSessionKey = $session_response->merchantSessionKey;

                    $expiryDate = $request['expiryDate'] . $request['expiryYear'];

                    $curl = curl_init();
                    $card_postArray = [
                        'cardDetails' => [
                            'cardholderName' => $request['owner'],
                            'cardNumber' => str_replace(' ', '', $request['cardNumber']),
                            'expiryDate' => $expiryDate,
                            'securityCode' => $request['cvv'],
                        ]
                    ];
                    $card_postArray_json = json_encode($card_postArray);
                    $card_identifier_url = \Config::get('app.card_identifier_url');

                    curl_setopt_array($curl, array(
                        CURLOPT_URL => $card_identifier_url,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => $card_postArray_json,
                        CURLOPT_HTTPHEADER => array(
                            "Authorization: Bearer $merchantSessionKey",
                            "Cache-Control: no-cache",
                            "Content-Type: application/json"
                        ),
                    ));

                    $card_response = curl_exec($curl);
                    $err = curl_error($curl);
                    curl_close($curl);

                    $card_response = json_decode($card_response);

                    if (isset($card_response->cardIdentifier)) {

                        $cardIdentifier = $card_response->cardIdentifier;
                        $transaction = Transaction::where('booking_id',$booking_id)->where('waiting_response_sagepay','1')->first();
                        if(empty($transaction))
                        {
                            $fraud_score = 0;
                            $amount = null;
                            $transactionId = null;
                            $vendorTxCode = 'lockers_' . Carbon::now()->format('Ymdhis') . '_' . $booking_id;
                            $status_code = '2000.xweb';
                            $status_message = 'No Response';
                            $waiting_response_sagepay =1;
                            $transaction = new Payment();
                            $transaction_id = $transaction->insert_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $amount, $transaction_type, $status_code, $vendorTxCode, $status_message, $fraud_score, $transactionId,$waiting_response_sagepay);
                            $transaction = Transaction::where('booking_id',$booking_id)->where('waiting_response_sagepay','1')->first();
                        }

                        $vendorTxCode = $transaction->vendor_tx_code;
                        $command = 'getTransactionDetail';

                        $get_transaction= new Payment();
                        $sage_pay_response = $get_transaction->get_transaction_from_sagepay_using_vendor_tx_code($vendorTxCode,$command);
//                             dd($sage_pay_response);
                        if($sage_pay_response['errorcode'] == '0043') {

                            $sagepay_transaction = new Payment();
                            return $sagepay_transaction->sagepay_transaction($merchantSessionKey,$cardIdentifier,$Authorization,$vendorTxCode,$total_price,$currency,$transaction_type,
                                $first_name,$last_name,$billing_address1,$billing_address2,$city,$state,$post_code,$country,$phone_number,$booking_id,$payment_id,$user_id,$user_email,$last4Digits,$total_lockers_price,$charity_price,$request->charity_id,$ajax);

                        }else{

                            $transactionId = $sage_pay_response['vpstxid'];

                            $get_transaction= new Payment();
                            $retrieve_transaction_response = $get_transaction->get_transaction_from_sagepay_using_transaction_id($transactionId);

                            if(isset($retrieve_transaction_response->statusCode )) {
                                if ($retrieve_transaction_response->statusCode == '0000') {

                                    if (Session::get('hold_locker_checked') == true) {

                                        $locker_hold = new Payment();
                                        $locker_hold->hold_locker_for_next_year($booking_id);
                                        Session::forget('hold_locker_checked');
                                    }

                                    $booked_status = new Payment();
                                    $booked_status->change_booked_status($booking_id);

                                    $total_price = $retrieve_transaction_response->amount->totalAmount / 100;
                                    $price_sage_pay = new Payment();
                                    $price = $price_sage_pay->price_calculation_sagepay_response($booking_id, $total_price);
                                    $total_lockers_price = $price[0];
                                    $charity_price = $price[1];
                                    $total_price = $price[2];

                                    $payment = Payment::where('booking_id', $booking_id)->first();

                                    if (empty($payment->amount) || $payment->amount == '0.00') {

                                        $status_message = $retrieve_transaction_response->statusCode . ':' . $retrieve_transaction_response->statusDetail;
                                        $statusCode = $retrieve_transaction_response->statusCode;

                                        $payment = new Payment();
                                        $payment->update_payments($payment_id, $total_price, $user_id, $billing_address1, $billing_address2, $city, $country, $first_name, $phone_number, $post_code, $state, $last_name, $currency, $user_email);

                                        $transaction = new Payment();
                                        $transaction->update_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $total_lockers_price, $transaction_type, $statusCode, $vendorTxCode, $status_message, 0, $transactionId);

                                        if ($charity_price > 0) {
                                            $waiting_response_sagepay =0;
                                            $transaction = new Payment();
                                            $transaction->insert_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $charity_price, '03', $statusCode, $vendorTxCode, $status_message, 0, $transactionId,$waiting_response_sagepay);

                                            $create_charity = new Payment();
                                            $create_charity->create_charity($booking_id,$charity_price,$charity_id);
                                        }
                                    }

                                    $confirm_payments = new Payment();
                                    return $confirm_payments->confirmation_payments($booking_id, $total_lockers_price, $charity_price, $total_price,$ajax);

                                } else {
                                    $sagepay_transaction = new Payment();
                                    return $sagepay_transaction->sagepay_transaction($merchantSessionKey, $cardIdentifier, $Authorization, $vendorTxCode, $total_price, $currency, $transaction_type,
                                        $first_name, $last_name, $billing_address1, $billing_address2, $city, $state, $post_code, $country, $phone_number, $booking_id, $payment_id, $user_id, $user_email, $last4Digits, $total_lockers_price, $charity_price, $request->charity_id,$ajax);

                                }
                            }
                            elseif(isset($retrieve_transaction_response->code)){
//
                                $sagepay_transaction = new Payment();
                                return $sagepay_transaction->sagepay_transaction($merchantSessionKey, $cardIdentifier, $Authorization, $vendorTxCode, $total_price, $currency, $transaction_type,
                                    $first_name, $last_name, $billing_address1, $billing_address2, $city, $state, $post_code, $country, $phone_number, $booking_id, $payment_id, $user_id, $user_email, $last4Digits, $total_lockers_price, $charity_price, $request->charity_id,$ajax);

                            }
                        }

                    } else {

                        $error_msgs = [];
                        if (isset($card_response->errors)) {
                            foreach ($card_response->errors as $error) {

                                $status_message = $error->code . ':' . $error->clientMessage;

                                $fraud_score = 1;
                                $amount = null;
                                $transactionId = null;
                                $vendorTxCode = null;
                                $waiting_response_sagepay =0;
                                $transaction = new Payment();
                                $transaction->insert_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $amount, $transaction_type, $error->code, $vendorTxCode, $status_message, $fraud_score, $transactionId,$waiting_response_sagepay);

                                array_push($error_msgs, $error->clientMessage);
                            }
                        }

                        $data = [
                            'status' => 'errors',
                            'error_msgs' => $error_msgs
                        ];
                        return response()->json($data);

                    }

                } else {
                    if (isset($session_response->code) || isset($session_response->description)) {
                        $status_message = $session_response->code . ':' . $session_response->description;
                    }
                    else {
                        $status_message = $session_response;
                    }


                    $fraud_score = 1;
                    $amount = null;
                    $transactionId = null;
                    $vendorTxCode = null;
                    $waiting_response_sagepay =0;

                    $transaction = new Payment();
                    $transaction->insert_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $amount, $transaction_type, $session_response->code, $vendorTxCode, $status_message, $fraud_score, $transactionId,$waiting_response_sagepay);

                    if (isset($session_response->description)) {
                        $error_msgs = [];
                        array_push($error_msgs, $session_response->description);
                        $data = [
                            'status' => 'errors',
                            'error_msgs' => $error_msgs
                        ];
                        return response()->json($data);

                    } else {

                        $error_msgs = [];
                        array_push($error_msgs, $session_response);
                        $data = [
                            'status' => 'errors',
                            'error_msgs' => $error_msgs
                        ];

                        return response()->json($data);

                    }

                }


            }
            else{

                $locker_code = new Payment();
                $locker_code->change_booked_status($booking_id);

                $confirm_payments = new Payment();
                return $confirm_payments->confirmation_payments($booking_id,$total_lockers_price,$charity_price,$total_price,$ajax);
            }
        }else{
            $url = URL('/');

            $data = [
                'status'=> 'redirect',
                'url'=>$url
            ];
            return response()->json($data);
        }

    }

}
