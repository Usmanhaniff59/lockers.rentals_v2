<?php
namespace App\Http\Controllers\Frontend;
use App\Email;
use App\Http\Controllers\Controller;
use App\Language;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AddressController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
     public function getUserAddress()
    {
       return response()->json(Auth::User());
    }

     public function saveUserAddress(Request $request)
    {
      
        if (Session::has('saleKey')) {
            // $data = User::where('id', Auth::User()->id)->update([
            //     'name' => $request->firstname,
            //     'surname' => $request->payer_surname,
            //     'address_1' => $request->address_1,
            //     'address_2' => $request->address_2,
            //     'city' => $request->city,
            //     'state' => $request->state,
            //     'country' => $request->country,
            //     'post_code' => $request->post_code,
            // ]);
            $data=User::where('id',Auth::User()->id)->first();
            $data->name=$request->firstname;
            $data->surname=$request->payer_surname;
            $data->address_1=$request->address_1;
            $data->address_2=$request->address_2;
            $data->city=$request->city;
            $data->state=$request->state;
            $data->country=$request->country;
            $data->post_code=$request->post_code;
            $data->save();


            return response()->json($data);
       
        }else{
                return response()->json('home_page');
            }
        }
}