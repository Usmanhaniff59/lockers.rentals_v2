<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdministrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function getlockers(Request $request){
//        dd($request->all());

        if(Session::has('saleKey')) {
            $start = Carbon::parse($request['start'])->format('Y-m-d H:i:s');
            $end = Carbon::parse($request['end'])->format('Y-m-d H:i:s');
//        dd($end);

            $locker_numbers = Locker::where('lockers.block_id', $request->block_id)
                ->where('active',1)
                ->where('display',1)
                ->get();

//        $locker_numbers =$locker_numbers->values();
//        dd($locker_numbers[0]->id);
//        dd($lockers->count());
//        leftJoin('sales', function ($join) use ($start, $end) {
//            $join->on('lockers.id', '=', 'sales.locker_id')
//                ->where('sales.end', '>=', $start)
//                ->Where('sales.start', '<=', $end)
//                ->where(function ($query) {
//                    $query->where('status', 'b')
//                        ->orWhere('status', 'r');
//                });
//        })
//            ->leftJoin('maintenances', function ($join) use ($start, $end) {
//                $join->on('lockers.id', '=', 'maintenances.locker_id')
//                    ->where('maintenances.to', '>=', $start)
//                    ->Where('maintenances.from', '<=', $end);
//            })
//            ->select('lockers.id as locker_id', 'lockers.block_id', 'lockers.locker_number', 'lockers.active', 'lockers.display', 'sales.id as sale_id', 'maintenances.id as maintenance_id')
//            ->where('lockers.block_id', $request->block_id)
//            ->where('lockers.active', 1)
//            ->where('lockers.display', 1)
//            ->orderBy('locker_id', 'ASC')

//            ->toArray();
//
//        dd($locker_numbers);
//        $locker_numbers = $collection->unique('locker_id');
//        $locker_numbers = $locker_numbers->values();
//        dd($locker_numbers);
            $total_lockers = $locker_numbers->count();
//        $locker_numbers[0]->sales->

//        dd($total_lockers);

            $output = '';
            dd($total_lockers);
            $blocks = Block::leftJoin('locker_block_image', 'locker_block_image.id', '=', 'blocks.locker_block_image_id')
                ->select('blocks.*', 'locker_block_image.file_name as locker_block_image')
                ->where('blocks.id', $request->block_id)->first();
//        dd($blocks->locker_blocks);

            $output .= '<div class="col-md-12" ><div class="row" style="overflow-x:auto;"><div class="col-md-6 lockers_table" ><table id="locker_table' . $request['counter'] . '">';

            $counter = 0;
            $counter_red = 0;
            for ($i = 1; $i <= $blocks->locker_blocks; $i++) {

                $red_column = 0;
                $output .= '<tr>';

                for ($j = 1; $j <= $blocks->block_height; $j++) {
                    if ($counter_red <= $total_lockers - 1) {
                        if  (Sale::where('locker_id',$locker_numbers[$counter]->id)
                                ->where(function ($query) use ($start,$end) {
                                    $query->where('end','>=',$start)
                                        ->Where('start','<=',$end);
                                })
                                ->where(function ($query) {
                                    $query->where('status', '=', 'b')
                                        ->orWhere('status', '=', 'r');
                                })->count('id') > 0
                            ||

                            $locker_numbers[$counter]->maintenances->where( 'to' ,'>=', $start)
                                ->where('from','<=' ,$end)->where('active',1)->count('id') > 0) {

                            $red_column++;
                        }
//
                    }
                    else {
                        $red_column++;
                    }
                    $counter_red++;
                }

                dd($red_column);


                for ($j = 1; $j <= $blocks->block_height; $j++) {

                    if ($counter <= $total_lockers - 1) {


                        if (Sale::where('locker_id',$locker_numbers[$counter]->id)
                                ->where(function ($query) use ($start,$end) {
                                    $query->where('end','>=',$start)
                                        ->Where('start','<=',$end);
                                })
                                ->where(function ($query) {
                                    $query->where('status', '=', 'b')
                                        ->orWhere('status', '=', 'r');
                                })->count('id') > 0  ||
                            $locker_numbers[$counter]->maintenances->where( 'to' ,'>=', $start)
                                ->where('from','<=' ,$end)->where('active',1)->count('id') > 0){
//                                                    dd($locker_numbers[$counter]);

                            $output .= '<td ><input type = "radio" name = "locker_id' . $request['counter'] . '" class="invisible-radio' . $request['counter'] . '" checked>';

                            if ($red_column == $blocks->block_height) {
                                $output .= '<label for="radio' . $locker_numbers[$counter]->locker_id . '" class="red-lockers" > ';
                            } else {
                                $output .= '<label for="radio' . $locker_numbers[$counter]->locker_id . '" class="" > ';

                            }

                            if ($locker_numbers[$counter]->id != intval($request->selected_lockers)) {

                                $output .= '<div class="styled-radio-red' . $request['counter'] . ' text-center" style = "color: white">' . $locker_numbers[$counter]->locker_number . '<br ><i class="fa fa-times icon-cross" ></i ></div ></label ></td>';
                            } else {

                                $output .= '<div class="styled-radio' . $request['counter'] . ' text-center" style = "color: white; border: 5px solid #66ff66">' . $locker_numbers[$counter]->locker_number . '<br ><i class="fa fa-check rounded-circle" style="background-color:#66ff66; padding: 6px""></i ></div ></label ></td>';
                                // $output .= '<div class="styled-radio'.$request['counter'].' text-center" style = "color: white;">'.$locker_numbers[$counter]->locker_number.'<br ><i class="fa fa-check icon-tick" ></i ></div ></label ></td >';
                            }
                        }
//                    else if ($locker_numbers[$counter]->sale_id != null || $locker_numbers[$counter]->maintenance_id != null)
//                    {
////                                                    dd($locker_numbers[$counter]);
//
//                        $output .= '<td ><input type = "radio" name = "locker_id' . $request['counter'] . '" class="invisible-radio' . $request['counter'] . '" checked>';
//                        if ($red_column == $blocks->block_height) {
//                            $output .= '<label for="radio' . $locker_numbers[$counter]->locker_id . '" class="red-lockers" > ';
//                        } else {
//                            $output .= '<label for="radio' . $locker_numbers[$counter]->locker_id . '" class="" > ';
//
//                        }
//                        if ($locker_numbers[$counter]->locker_id != intval($request->selected_lockers)) {
//
//                            $output .= '<div class="styled-radio-red' . $request['counter'] . ' text-center" style = "color: white">' . $locker_numbers[$counter]->locker_number . '<br ><i class="fa fa-times icon-cross" ></i ></div ></label ></td>';
//                        } else {
//
//                            $output .= '<div class="styled-radio' . $request['counter'] . ' text-center" style = "color: white; border: 5px solid #66ff66">' . $locker_numbers[$counter]->locker_number . '<br ><i class="fa fa-check rounded-circle" style="background-color:#66ff66; padding: 6px""></i ></div ></label ></td>';
//                            // $output .= '<div class="styled-radio'.$request['counter'].' text-center" style = "color: white;">'.$locker_numbers[$counter]->locker_number.'<br ><i class="fa fa-check icon-tick" ></i ></div ></label ></td >';
//                        }
//                    }

                        else {
                            $output .= '<td><input type = "radio" name = "locker_id' . $request['counter'] . '" id = "radio' . $locker_numbers[$counter]->id . '' . $request['counter'] . '" value="' . $locker_numbers[$counter]->id . '" data-sale_id = "' . $request->sale_id . '" data-block_id = "' . $locker_numbers[$counter]->block_id . '" data-locker_id = "' . $locker_numbers[$counter]->id . '" data-rate = "' . $request['rate'] . '" data-tax = "' . $request['tax'] . '" data-counter = "' . $request['counter'] . '" data-hold_locker="' . $request['hold_locker'] . '" class="invisible-radio' . $request['counter'] . ' choose-locker" >
                                                            <label for="radio' . $locker_numbers[$counter]->id . '' . $request['counter'] . '" class="puffOut">
                                                               <div class="styled-radio' . $request['counter'] . ' text-center" style = "color: white;">' . $locker_numbers[$counter]->locker_number . '<br ><i class="fa fa-check icon-tick" ></i ></div ></label ></td >';
                        }
                    } else {
//                                                    $output.='<td> <input type = "radio" name = "id'.$request['counter'].'" class="invisible-radio'.$request['counter'].'" >
//                                                        <label for="radio" >
//                                                            <div class="styled-radio-gray'.$request['counter'].' text-center" style = "color: white" ></div ></label >
//                                                    </td>';
                    }
                    $counter++;
                }
                $output .= '</tr>';
            }

            // dd($collection[0]->locker_block_image);
            $output .= '</table></div><div class="col-md-6">
        <img style="width:70%" src="' . url('uploads/' . $blocks->locker_block_image) . '" class="img-responsive" alt="Block Image">
        </div></div></div>';
        }else{
            return redirect('/');
        }

        return response()->json($output);
    }



    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
