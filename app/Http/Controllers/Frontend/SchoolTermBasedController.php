<?php

namespace App\Http\Controllers\Frontend;

use App\Sale;
use App\VoucherCode;
use App\VoucherCodeUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SchoolTermBasedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function voucherCodeValidation(Request $request)
    {

         $date =Carbon::now()->toDateTimeString();
         $code =VoucherCode::where('code',$request->code)->where('start','<=',$date)->where('end','>=',$date)->where('company_id',$request->company_id)->where('issued',1)->first();

        if (!empty($code)) {

            if(!empty($request->user_id)) {

                $used_code = VoucherCodeUser::where('code', $request->code)
//                    ->where('user_id', $request->user_id)
                    ->first();
                $used_code = false;

                /*
                Temp code written to validate if the person has already made a booking
                This code can only be used for existing bookings for a school year
                */

                //hard coded needs to be made dynamic based on vouchure table
//                $date = date("2019-09-01 00:00:00");
//                $booking_made = 0;
//                $checkDate = Sale::Where('status','b')
//                    ->where('user_id', $request->user_id)
//                    ->where('start','<=',$date)
//                    ->where('end','>=',$date)
//                    ->first();
//                if(!empty($checkDate))
//                     $booking_made = 1;

                $booking_made = 1;
                //check if vouchure code has been used on a booking yet


                if (empty($booking_made)) {
                    $data = [
                        'status' => 'user_not_booked'
                    ];
                }
                else if (!empty($used_code)) {
                    $data = [
                        'status' => 'already_used'
                    ];

                } else {
                    $data = [
                        'status' => 'valid_code',
                        'code_id' => $code->id
                    ];
                }

            }
            else {

                $data = [
                    'status' => 'log_in'
                ];

            }



        } else {

            $data = [
                'status' => 'not_valid'
            ];

        }



        return response()->json($data);
    }
}
