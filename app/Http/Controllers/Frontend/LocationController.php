<?php

namespace App\Http\Controllers\Frontend;

use App\Company;
use App\LocationGroup;
use App\SchoolRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function searchlocation(Request $request)
    {
        $location = $request['location'];
//        dd($location);
//

        $locations = Company::where('active', 1)
            ->where('display', 1)
            ->where(function($q) use ($location) {
                $q->where('name', 'LIKE', '%' . $location . '%')
                    ->orWhere('city', 'LIKE', '%' . $location. '%');
            })
            ->orderBy('name', 'ASC')->get();

        $radio_locations = '';
        foreach($locations as $location) {

//            $radio_locations .='<div class="row" >
//                        <label style="margin-left: 20px" class="text-left">
//                            <input type = "radio" class="radio-locations" name = "options" id="companyId'.$location->id.'" value="'.$location->id.'" data-location_id = "'.$location->id.'" autocomplete = "off" >   '.
//                $location->name .'
//                        </label >
//                      </div >';

            $radio_locations .=' <div class="row" ><div class="radio">
                <label style="margin-left: 20px" class="text-left">
                    <input type="radio" class="radio-locations" name = "options" id="companyId'.$location->id.'" value="'.$location->id.'" data-location_name="'.$location->name.'" data-location_id = "'.$location->id.'" autocomplete = "off">
                     <span class="cr"><i class="cr-icon fa fa-circle"></i></span>
                    '. $location->name .'
                </label>
            </div></div >';
//            $radio_locations .=' <div class="row" >
//                <label>  <input class="radiobox-focus" type="radio" name="x"/><span> Focus</span></label>
//
//                </div >';
        }
        $data = [
            'locations' => $locations,
            'radio_locations' => $radio_locations,

        ];
        Session::forget('page_number');
        if(Session::has('companyId')){
           Session::flash('companyId',Session::get('companyId'));

           Session::forget('arrowsPage');

        }
        return response()->json($data);

    }

    public function getlocationgroup(Request $request)
    {
        $location_id = $request['location_id'];
        $location_name = Company::with('locationGroup')->find($location_id);
//        dd($location_name);

        return response()->json($location_name);

    }

    public function schoolRequest(Request $request)
    {
//        dd($request->all());


//        $url = 'https://www.google.com/recaptcha/api/siteverify';
//        $remoteip = $_SERVER['REMOTE_ADDR'];
//        $data = [
//            'secret' => config('services.recaptcha.secret'),
//            'response' => $request->get('recaptcha'),
//            'remoteip' => $remoteip
//        ];
//        $options = [
//            'http' => [
//                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
//                'method' => 'POST',
//                'content' => http_build_query($data)
//            ]
//        ];
//        $context = stream_context_create($options);
//        $result = file_get_contents($url, false, $context);
//        $resultJson = json_decode($result);
//        if ($resultJson->success != true) {
//            $data = [
//
//                'recapcha' => false,
//            ];
//            return response()->json($data);
//            // return back()->withErrors(['captcha' => 'ReCaptcha Error']);
//        }
//        if ($resultJson->score >= 0.3) {


            $saved_request = SchoolRequest::where('school_name', $request['school_name'])->where('user_id', $request['user_id'])->first();

            if ($saved_request) {
                $saved_request->requests = ++$saved_request->requests;
                $saved_request->save();
            } else {


                $school = new SchoolRequest();
                $school->user_id = $request['user_id'];
                $school->school_name = $request['school_name'];
                $school->line_1_address = $request['line_1_address'];
                $school->line_2_address = $request['line_2_address'];
                $school->town = $request['town'];
                $school->post_code = $request['post_code'];
                $school->country = $request['country'];
                $school->date_requested = Carbon::now();
                $school->requests = 1;
                $school->response = 0;
                $school->save();
            }

            $current_user_request = SchoolRequest::where('school_name',$request['school_name'])->where('user_id',$request['user_id'])->first();
            $others_saved_requests = SchoolRequest::where('school_name',$request['school_name'])->whereNotIn('user_id',[$request['user_id']])->count();
            $data = [
                'current_user_request' => $current_user_request,
                'others_saved_requests' => $others_saved_requests,
                'recapcha' => true,
            ];


            return response()->json($data);
            //Validation was successful, add your form submission logic here
            // return back()->with('message', 'Thanks for your message!');
//        } else {
//            //return back()->withErrors(['captcha' => 'ReCaptcha Error']);
//            $data = [
//
//                'recapcha' => false,
//            ];
//            return response()->json($data);
//        }



    }
}
