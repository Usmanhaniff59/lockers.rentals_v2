<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Sale;

class TranslationController extends Controller
{
    public function changeLocale(Request $request)
    {

        $this->validate($request, ['locale' => 'required']);

         Session::put('locale', $request['locale']);
        Session::save();

        if(Session::has('saleKey')) {
            $sale_id = Session::get('saleKey');
            $sales = Sale::where('booking_id', $sale_id)->get();
            foreach ($sales as $sale){
                 $child =Sale::findOrFail($sale->id);
                 if(Session::get('locale') == 'fr') {
                     $child->lang = 'FR';
                 }else {
                     $child->lang = 'EN';
                 }
                $child->save();
            }
        }
        return redirect()->back();

    }
}
