<?php
namespace App\Http\Controllers\Frontend;
use App\FrontendPlaceholder;
use App\Http\Controllers\Controller;
use App\Label;
use App\Language;
use App\Pages;
use App\Test;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Sale;
use Illuminate\Support\Facades\Session;
use App\EmailTemplate;
use App\User;
use App\Locker;
use Carbon\Carbon;
use App\Company;
use App\Block;
use App\Email;
use App\Maintenance;


class PageController extends Controller
{

    public function aboutUs(){
         return view('frontend.pages.about_us');
        
    }

    public function getUserInfoByEmail(Request $request){
        $user=User::where(['email'=>$request->email,'active'=>1])->first();
        if (!empty($user)){
            return $user;
        }else{
            $password= bcrypt('123456');
            $user=User::create(['email'=>$request->email,'name'=>$request->name,'password'=>$password]);
            return $user;
        }
    }

     public function faq(){
         return view('frontend.pages.faq');
        
    }

    public function policy(){
        return view('frontend.pages.policy');
    }


    public function contactUs(){
        return view('frontend.pages.contact_us');
    }

    public function unsubcribe($id){

        $data = Sale::where('id', $id)->update([
            'status' => 'l'
        ]);
        $data = Sale::where('booking_id', $id)->update([
            'status' => 'l'
        ]);

        return view('frontend.pages.unsubscribe');
    }

    public function bookingExpired($id){

        $sale = Sale::where('id', $id)->first();
        $placeholders = FrontendPlaceholder::where('key', 'booking-expired-section1-text')->pluck('placeholder')->toArray();
        $sale_desc_placeholder = implode(",", $placeholders);
        return view('frontend.pages.booking_expired', compact('sale', 'sale_desc_placeholder'));
    }

    public function sendContactMail(Request $request){
        $data['subject_title'] = 'Message from locker.rentals';
        $data['subject'] = $request->subject;
        $data['email'] = $request->email;
        $data['phone'] = $request->phone;
        $data['name'] = 'Locker Rental';
        $data['my_message'] = $request->message;
        $data['bookingId'] = $request->bookingId;
        $data['school_name'] = $request->school;
        
        Mail::send('frontend.email.contact_mail', $data, function($message) use ($data) {
            $message->from('noreply@locker.rentals', $data['subject_title']);
            $message->to('admin@locker.rentals',$data['name'])->subject($data['subject_title']);
        });
        if (Mail::failures()) {
            return Redirect::back()->withErrors(array('messageError' => 'Mail can not send please try again.'));
        }else{
            return Redirect::back()->withInput()->withErrors(array('messageSuccess' => 'Mail sent successfully.'));
        }
    }

    public function show($url)
    {
//        dd($url);
//        return redirect()->route('/');
        return redirect()->to('/');
//        $page = Pages::where('slug',$url)->first();
//
//        return view('frontend.pages.frontpage', compact('page'));
    }

    public function savepage(Request $request)
    {
        $page_id = $_POST['pageid'];
        $page_content = $_POST['pagecontent'];

        $page = Pages::where('id',$page_id)->firstOrFail();
        $page->body = $page_content;
        $page->save();
        $array = array('1');
        return json_encode( $array );
    }

    public function changeText(Request $request)
    {

        $data_id = $_POST['data_id'];
        $data_value = $_POST['data_value'];

        if (Session::has('locale')) {
            $locale = Session::get('locale', \Illuminate\Support\Facades\Config::get('app.locale'));
        } else {
            $locale = 'en';
        }

        $page = Label::where('key', $data_id)->where('lang', $locale)->first();
        if ($page){
            $page->value = $data_value;
            $page->save();
         }else{
            $text = new Label();
            $text->key = $data_id;
            $text->lang = $locale;
            $text->value = $data_value;
            $text->scope = 'global';
            $text->save();

        }
        $array = array('1');
        Artisan::call('cache:clear');
//        Artisan::call('cache:clear');
        return json_encode( $array);

//        $data_id = $_POST['data_id'];
//        $languages = $_POST['languages'];
//        $data_values = $_POST['data_values'];
////        dd($data_values);
//
//        if (Session::has('locale')) {
//            $locale = Session::get('locale', \Illuminate\Support\Facades\Config::get('app.locale'));
//        } else {
//                $locale = 'en';
//        }
////
//        foreach($languages as $key=>$language){
//        $page = Label::where('key', $data_id)->where('lang', $language)->first();
//        $page->value = $data_values[$key];
//        $page->save();
//    }
////        $array = array($locale);
//
//        return response()->json($locale);
    }
    public function placeholders(Request $request)
    {


        $data_id = $_POST['data_id'];
        $placeholders = FrontendPlaceholder::where('key',$data_id)->first();
//        dd($placeholders);

        return response()->json($placeholders);


    }
    public function multiplaceholders(Request $request)
    {

        $data_id = $_POST['data_id'];
        $placeholders = FrontendPlaceholder::where('key',$data_id)->get();

        return response()->json($placeholders);


    }
    public function switchcms(Request $request)
    {
        if($_POST['switchCMS'] == true) {
            Session::put('switchCMS', $_POST['switchCMS']);
        }else{
            Session::forget('switchCMS');
        }
        $array =array(1);
        return json_encode( $array);
    }

    public function getLanguages(Request $request)
    {

        $lang = Language::where('status',1)->pluck('locale');
        $texts = Label::where('key',$_POST['textId'])->whereIn('lang',$lang)->get();
        if (Session::has('locale')) {
            $locale = Session::get('locale', \Illuminate\Support\Facades\Config::get('app.locale'));
        } else {
            $locale = 'en';
        }
//        $languages = Label::select('lang')->distinct()->get();
        $data = [
            'texts' => $texts,
            'locale' => $locale,
        ];
//        dd($languages);
        return response()->json($data);
    }
}