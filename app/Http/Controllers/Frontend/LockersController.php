<?php

namespace App\Http\Controllers\Frontend;

use App\Block;
use App\Golive;
use App\Company;
use App\CronSchedule;
use App\Currency;
use App\Email;
use App\EmailTemplate;
use App\EmailTemplateAlt;
use App\FrontendPlaceholder;
use App\Http\Controllers\Controller;
use App\Common;
use App\LockerOffSale;
use App\paymentConfig;
use App\Jobs\SendEmailJob;
use App\Label;
use App\Locker;
use App\Mail\SendEmailTest;
use App\Maintenance;
use App\Payment;
use App\Charity;
use App\PaymentDiscount;
use App\RequestAvailability;
use App\Sale;
use App\SalesAudit;
use App\SchoolAcademicYear;
use App\SchoolYear;
use App\MarketingPictures;
use App\Transaction;
use App\CharityTransactions;
use App\User;
use App\ErrorLog;
use App\VoucherCode;
use App\VoucherCodeUser;
use Carbon\Carbon;
use Couchbase\Exception;
use DOMDocument;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use SimpleXMLElement;

class LockersController extends Controller
{
    public function customErrorLog(Request $request){
        // $errorLog=ErrorLog::where('message',$request->message)->first();
        if (!empty($request->message)){
            $message=$request->message;
            $info="Requested Api : ".url('/').$request->apiUrl." ".$message;
            Log::info($info);
        }
    }

    public function requestavailability()
    {
//        Session::forget('saleKey');
//        Session::forget('schoolTermSubmit');
//        Session::forget('bookingTimer');
//        Session::forget('show_hide_lockers');
//        Session::forget('on_payments_booking_id');
//        Session::forget('user_payments_form_info');
//        Session::forget('postCodeMethod');
//        Session::forget('payment_id');
//        Session::forget('arrowsPage');
//        Session::forget('page_number');
//        Session::forget('schoolTermReSubmit');
//        Session::forget('schooltermbasedback');
//        Session::forget('companyId');
//        Session::forget('company_info');
//        Session::forget('payment');
//        Session::forget('show_hide_lockers');
//        Session::forget('on_payments_booking_id');
//        Session::forget('user_payments_form_info');
//        Session::forget('postCodeMethod');
//        Session::forget('schoolTermReSubmit');
        return view('frontend.pages.requestavailability');
    }

    public function schooltermbased(Request $request)
    {
        $sessions_expire = new Payment();
        $sessions_expire->sessions_expire();

        if ($request->has('options')) {

            Session::put('companyId', $_POST['options']);
        }
        $companyId = Session::get('companyId');

          $school = Company::where('id', $companyId)->first();
       
          if(isset($school)){
          $liveCheck=Golive::where('company_id',$companyId)->orderBy('id','desc')->first();
          if(isset($liveCheck) && $liveCheck->active==1)
          {
            if(isset($liveCheck->liveDate))
            {
                if(Carbon::now()>$liveCheck->liveDate)
                {
                    $schoolYearCheck=$liveCheck->school_year;
                }else{
                     $schoolYearCheck=0;
                }
            }else{
            $schoolYearCheck=$liveCheck->school_year;
        }
          }else{
            $schoolYearCheck=0;
          }
          // dd($schoolYearCheck);
        $years = SchoolYear::where([['status', 1],['to_date','>=',Carbon::now()]])->orWhere('id',$schoolYearCheck)->get()->toArray();

        $academic_years = SchoolAcademicYear::Join('company_years', 'company_years.school_academic_year_id', '=', 'school_academic_years.id')->where('company_years.company_id', $companyId)->select('school_academic_years.*')->get()->toArray();

        //dd($academic_years);

      
        Session::put('company_info', $school);

        Session::put('bookingTimer', 15);
        Session::put('schoolTermSubmit', 1);
        Session::forget('voucher_code_id');
        if (Session::has('arrowsPage') == false) {

            Session::put('arrowsPage', 2);
            Session::put('page_number', 2);

        }
        $placeholders = FrontendPlaceholder::where('key', 'school-term-login-user-msg')->pluck('placeholder')->toArray();
        $login_user_msg_placeholders = implode(",", $placeholders);

        $book_lock_desc = FrontendPlaceholder::where('key', 'booking-locker-disclaimer')->pluck('placeholder')->toArray();
        $book_lock_desc_placeholders = implode(",", $book_lock_desc);

        $book_lock_desc_cond = FrontendPlaceholder::where('key', 'booking-locker-disclaimer-condition')->pluck('placeholder')->toArray();
        $book_lock_desc_cond_placeholders = implode(",", $book_lock_desc_cond);


        $sales_count = 1;
        return view('frontend.pages.schooltermbased', compact('school', 'years', 'academic_years', 'login_user_msg_placeholders', 'sales_count', 'book_lock_desc_placeholders', 'book_lock_desc_cond_placeholders'));
    }else{
       return redirect('/');
    }
//         }else{
//             dd('he');
//             Session::get('page_number');
//         }
    }

    public function schooltermbasedAfterLogout($id)
    {
        $companyId = $id;
        Session::put('companyId', $id);
        $years = SchoolYear::where('status', 1)->get()->toArray();
        $academic_years = SchoolAcademicYear::all()->toArray();
        $school = Company::where('id', $companyId)->first();

        $placeholders = FrontendPlaceholder::where('key', 'school-term-login-user-msg')->pluck('placeholder')->toArray();
        $login_user_msg_placeholders = implode(",", $placeholders);
        $book_lock_desc = FrontendPlaceholder::where('key', 'booking-locker-disclaimer')->pluck('placeholder')->toArray();
        $book_lock_desc_placeholders = implode(",", $book_lock_desc);

        $book_lock_desc_cond = FrontendPlaceholder::where('key', 'booking-locker-disclaimer-condition')->pluck('placeholder')->toArray();
        $book_lock_desc_cond_placeholders = implode(",", $book_lock_desc_cond);
        $sales_count = 1;

        Session::forget('show_hide_lockers');
        Session::forget('on_payments_booking_id');
        Session::forget('user_payments_form_info');
        Session::forget('postCodeMethod');
        Session::forget('schoolTermReSubmit');

        if (Session::has('arrowsPage') == false) {

            Session::put('arrowsPage', 2);
            Session::put('page_number', 2);

        }
        Session::put('bookingTimer', 15);
        Session::put('schoolTermSubmit', 1);
        return view('frontend.pages.schooltermbased', compact('school', 'years', 'academic_years', 'login_user_msg_placeholders', 'sales_count', 'book_lock_desc_placeholders', 'book_lock_desc_cond_placeholders'));
    }

    public function schooltermbasedback()
    {
//           dd(Session::has('saleKey'));
        if (Session::has('saleKey')) {
            $sale_id = Session::get('saleKey');
            $sales = Sale::where('booking_id', $sale_id)->get();
            $user = User::where('id', $sales[0]->user_id)->first();
            $useremail = $user->email;
            $sales_count = $sales->count();
            $voucher_code_id = Session::get('voucher_code_id');
            if (isset($voucher_code_id)) {
                $voucher_code = VoucherCode::find($voucher_code_id);
                $VoucherCodeUser = VoucherCodeUser::where('code', $voucher_code->code)
                    ->first();
                // if(!empty($VoucherCodeUser)){
                // $VoucherCodeUser->code = null;
                //$VoucherCodeUser->save();
                //}

            }

            $companyId = Session::get('companyId');

            $years = SchoolYear::where('status', 1)->get()->toArray();

            $school = Company::where('id', $companyId)->first();
//            $academic_years = SchoolAcademicYear::all()->toArray();
            $academic_years = SchoolAcademicYear::Join('company_years', 'company_years.school_academic_year_id', '=', 'school_academic_years.id')->where('company_years.company_id', $companyId)->select('school_academic_years.*')->get()->toArray();


            $placeholders = FrontendPlaceholder::where('key', 'school-term-login-user-msg')->pluck('placeholder')->toArray();
            $login_user_msg_placeholders = implode(",", $placeholders);

            $book_lock_desc = FrontendPlaceholder::where('key', 'booking-locker-disclaimer')->pluck('placeholder')->toArray();
            $book_lock_desc_placeholders = implode(",", $book_lock_desc);

            $book_lock_desc_cond = FrontendPlaceholder::where('key', 'booking-locker-disclaimer-condition')->pluck('placeholder')->toArray();
            $book_lock_desc_cond_placeholders = implode(",", $book_lock_desc_cond);

            Session::put('schoolTermSubmit', 1);
            Session::put('schoolTermReSubmit', 1);
            Session::put('schooltermbasedback', 1);

            if (Session::get('reservedLocker') == 1) {

                $locker_ids = [];
                $sale_ids = [];
                foreach ($sales as $sale) {
                    if ($sale->hold_booking_date <= Carbon::now()->toDateTimeString()) {
                        return redirect('/booking-expired');
                    } else {

                        array_push($locker_ids, $sale->locker_id);
                        array_push($sale_ids, $sale->id);
                    }
                }
                return view('frontend.pages.schooltermbasedreserved', compact('school', 'academic_years', 'years', 'login_user_msg_placeholders', 'sales', 'sales_count', 'locker_ids', 'sale_ids', 'useremail', 'book_lock_desc_placeholders', 'book_lock_desc_cond_placeholders'));

            } else {

                return view('frontend.pages.schooltermbased', compact('school', 'academic_years', 'years', 'login_user_msg_placeholders', 'sales', 'sales_count', 'useremail', 'book_lock_desc_placeholders', 'book_lock_desc_cond_placeholders'));
            }
        } else {
            return redirect('/');
        }

    }

    public function removechild(Request $request)
    {


        $sale = Sale::findOrFail($request->sale_id);
        $sale->delete();

        return response()->json('success');
    }

    public function reissueLockercode()
    {
        return view('frontend.pages.reissue_lockercode');
    }

    public function saverequestavailability(Request $request)
    {
//        dd($request->all());

//        $url = 'https://www.google.com/recaptcha/api/siteverify';
//        $remoteip = $_SERVER['REMOTE_ADDR'];
//        $data = [
//            'secret' => config('services.recaptcha.secret'),
//            'response' => $request->get('recaptcha'),
//            'remoteip' => $remoteip
//        ];
//        $options = [
//            'http' => [
//                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
//                'method' => 'POST',
//                'content' => http_build_query($data)
//            ]
//        ];
//        $context = stream_context_create($options);
//        $result = file_get_contents($url, false, $context);
//        $resultJson = json_decode($result);
//
//        if ($resultJson->success != true) {
//            return back()->withErrors(['captcha' => 'ReCaptcha Error']);
//        }
//        if ($resultJson->score >= 0.3) {
//            //Validation was successful, add your form submission logic here
//            return back()->with('message', 'Thanks for your message!');
//        } else {
//            return back()->withErrors(['captcha' => 'ReCaptcha Error']);
//        }
        $validator = $this->validate($request, [
            'type' => 'required',
            'location' => 'required',
            'town' => 'required|max:120',
            'first_name' => 'required|max:120',
            'surname' => 'required|max:120',
            'email' => 'required',
            'notes' => 'required',


        ]);
//        Mail::to($this->email_to)->send($email);


        RequestAvailability::create($request->only('type', 'location', 'town', 'first_name', 'surname', 'email', 'notes'));

        $request_email_template = EmailTemplate::where('id', 1)->first();
        if (!empty($request_email_template)) {
            $to = $request['email'];
            $subject = $request_email_template->subject;
            $template_text = $request_email_template->template_text;


            $placeholders = ["%location%", "%town%", "%first_name%", "%surname%", "%email%", "%notes%"];
            $placeholder_values = [$request['location'], $request['town'], $request['first_name'], $request['surname'], $request['email'], $request['notes']];
            $email_html = str_replace($placeholders, $placeholder_values, $template_text);

            $phone_number = null;
            $sms_body = null;

            $sale = new Sale();
            $sale->saveEmailForCron($to, $phone_number, $subject, $email_html, $sms_body);

            //$response = file_get_contents('https://my.fastsms.co.uk/api?Token=17AG-3Afl-6d0f-40z7&Action=Send&DestinationAddress=+923348229276&SourceAddress=API&Body='.$request_email_template->sms_message);

//        $booking_cron = CronSchedule::where('class', 'Email')->where('controller', 'LockersController')->where('status', 'Pending')->get();
//
//        if ($booking_cron->isEmpty() == true) {
//            $cron = new CronSchedule();
//            $cron->class = 'Email';
//            $cron->controller = 'LockersController';
//            $cron->where_check = '0';
//            $cron->due = Carbon::now();
//            $cron->completed = '0';
//            $cron->status = 'Pending';
//            $cron->save();
//        }
        }
        Session::forget('company_info');

        return redirect()->route('requestavailability')
            ->with('success', 'Request successfully generated.');

    }

    public function saveSchoolBooking(Request $request)
    {

    
        if ($request->futureComunication) {
            User::where('id', auth()->user()->id)->update([
                'subscription' => 1
            ]);
        }

        Session::put('voucher_code_id', $request->voucher_code_id);

        if (isset($request->hold_locker)) {
            Session::put('hold_locker_checked', true);
        }

        if (Session::get('schooltermbasedback') == 1) {

            Session::forget('sale_id');
            Session::forget('hold_locker_id');

        }

        if (Session::get('schoolTermSubmit') == 1) {

            $date_hold = (new \App\Sale)->hold_booking_date();
            $expiry_time = (new \App\Sale)->booking_expiry_time();
//            $date_hold = $date_hold;

            Session::put('show_hide_lockers', 'Grid Hidden');

            if ($request['start_dateTime1'] && $request['end_dateTime1']) {

                $start_datetime = explode(' ', $request['start_dateTime1']);
                $start_day = $start_datetime[0];
                $start_month = explode(',', $start_datetime[1]);

                $start_month = Carbon::parse($start_month[0])->month;

                $start_year = $start_datetime[2];

                $start_time = explode(':', $start_datetime[3]);
                $start_hour = $start_time[0];
                $start_minutes = $start_time[1];

                $start = Carbon::create($start_year, $start_month, $start_day, $start_hour, $start_minutes, 00)->toDateTimeString();

                $end_datetime = explode(' ', $request['end_dateTime1']);
                $end_day = $end_datetime[0];
                $end_month = explode(',', $end_datetime[1]);

                $end_month = Carbon::parse($end_month[0])->month;

                $end_year = $end_datetime[2];

                $end_time = explode(':', $end_datetime[3]);

                $end_hour = $end_time[0];
                $end_minutes = $end_time[1];

                $end = Carbon::create($end_year, $end_month, $end_day, $end_hour, $end_minutes, 00)->toDateTimeString();


            } elseif ($request['start_date1'] && $request['end_date1']) {


                $start_date = explode(' ', $request['start_date1']);

                $start_day = $start_date[0];

                $start_month = explode(',', $start_date[1]);

                $start_month = Carbon::parse($start_month[0])->month;

                $start_year = $start_date[2];

                $start = Carbon::create($start_year, $start_month, $start_day)->toDateTimeString();

                $end_date = explode(' ', $request['end_date1']);

                $end_day = $end_date[0];

                $end_month = explode(',', $end_date[1]);

                $end_month = Carbon::parse($end_month[0])->month;

                $end_year = $end_date[2];

                $end = Carbon::create($end_year, $end_month, $end_day)->endOfDay()->toDateTimeString();


            }
            if ($request['school_year_id1']) {


                $school_year_id = $request['school_year_id1'];
                $schoolYear = SchoolYear::where('id', $school_year_id)->first();
                $splitName = explode('-', $schoolYear->title);

                $start = Carbon::parse($schoolYear->from_date)->startOfDay()->toDateTimeString();

                $end = Carbon::parse($schoolYear->to_date)->endOfDay()->toDateTimeString();


            } else {
                $school_year_id = null;
            }

            $locationGroup = Company::where('id', $request['company_id'])->select('location_group_id')->first();
            $language = Session::get('locale');

            if (Session::get('schoolTermReSubmit') == 1) {
                Session::put('bookingTimer', 15);

                $expired_time = Carbon::now()->addMinutes(15);
                $bookings = Sale::where('booking_id', $request['booking_id'])->orderBy('id', 'ASC')->get();

                foreach ($bookings as $i => $booking) {
                    $i++;

                    if ($request['start_dateTime' . $i] && $request['end_dateTime' . $i]) {

                        //start date Time format
                        $start_datetime = explode(' ', $request['start_dateTime' . $i]);
                        $start_day = $start_datetime[0];
                        $start_month = explode(',', $start_datetime[1]);

                        $start_month = Carbon::parse($start_month[0])->month;

                        $start_year = $start_datetime[2];

                        $start_time = explode(':', $start_datetime[3]);
                        $start_hour = $start_time[0];
                        $start_minutes = $start_time[1];

                        $start = Carbon::create($start_year, $start_month, $start_day, $start_hour, $start_minutes, 00)->toDateTimeString();
                        //end date Time format
                        $end_datetime = explode(' ', $request['end_dateTime' . $i]);
                        $end_day = $end_datetime[0];
                        $end_month = explode(',', $end_datetime[1]);

                        $end_month = Carbon::parse($end_month[0])->month;

                        $end_year = $end_datetime[2];

                        $end_time = explode(':', $end_datetime[3]);

                        $end_hour = $end_time[0];
                        $end_minutes = $end_time[1];

                        $end = Carbon::create($end_year, $end_month, $end_day, $end_hour, $end_minutes, 00)->toDateTimeString();


                    } elseif ($request['start_date' . $i] && $request['end_date' . $i]) {
                        $start_date = explode(' ', $request['start_date' . $i]);

                        $start_day = $start_date[0];

                        $start_month = explode(',', $start_date[1]);

                        $start_month = Carbon::parse($start_month[0])->month;

                        $start_year = $start_date[2];

                        $start = Carbon::create($start_year, $start_month, $start_day)->toDateTimeString();

                        $end_date = explode(' ', $request['end_date' . $i]);

                        $end_day = $end_date[0];

                        $end_month = explode(',', $end_date[1]);

                        $end_month = Carbon::parse($end_month[0])->month;

                        $end_year = $end_date[2];

                        $end = Carbon::create($end_year, $end_month, $end_day)->endOfDay()->toDateTimeString();

                    } else {
                        $start = null;
                        $end = null;
                    }

                    if ($request['school_year_id' . $i]) {

                        $school_year_id = $request['school_year_id' . $i];
                        $schoolYear = SchoolYear::where('id', $school_year_id)->first();

                        $start = Carbon::parse($schoolYear->from_date)->startOfDay()->toDateTimeString();

                        $end = Carbon::parse($schoolYear->to_date)->endOfDay()->toDateTimeString();


                    } else {
                        $school_year_id = null;
                    }

                    $sale = Sale::find($booking->id);
                    if (!auth()->user()->can('3D Secure')) {
                        $sale->user_id = $request['user_id'];
                    }

                    if (isset($language)) {
                        if ($language == 'fr') {
                            $sale->lang = 'FR';
                        } else {
                            $sale->lang = 'EN';
                        }
                    } else {
                        $sale->lang = 'EN';
                    }
                    $sale->title = 'LOCKER';
                    $sale->lock_type = 'm';
                    $sale->company_id = $request['company_id'];

                    $sale->locker_id = $request['locker_id' . $i];
                    $sale->block_id = $request['block_id' . $i];
                    $sale->start = $start;
                    $sale->end = $end;
                    if (!auth()->user()->can('3D Secure')) {
                        $sale->email = $request['userEmail'];
                    }
//                    if ($request['locker_id' . $i] != 0 && $request['block_id' . $i] != 0) {
                    $sale->status = 'r';
//                    } else {
//                        $sale->status = 0;
//                    }

                    if ($request['hold_booking_date' . $i] >= Carbon::now()->subHour()->toDateTimeString() && $request['hold_booking_date' . $i] <= Carbon::now()->toDateTimeString()) {
                        $sale->hold_booking_date = Carbon::parse($request['hold_booking_date' . $i])->addHour()->toDateTimeString();
                    }
                    $sale->confirmation_sent = 0;
                    $sale->delete = 0;
                    $sale->street = $request['street'];
                    $sale->city = $request['city'];
                    $sale->post_code = $request['post_code'];
                    $sale->country = $request['country'];
                    $sale->child_first_name = $request['child_first_name' . $i];
                    $sale->child_surname = $request['child_surname' . $i];
                    if ($request['child_email' . $i]) {
                        $sale->child_email = $request['child_email' . $i];
                    }
                    $sale->school_year_id = $school_year_id;
                    if ($request['hold_locker']) {
                        if ($request['hold_locker' . $i]) {
                            $sale->hold_locker = 1;
                        } else {
                            $sale->hold_locker = 0;
                        }
                    } else {
                        $sale->hold_locker = 0;
                    }
                    if ($locationGroup->location_group_id == 1) {
                        $sale->school_academic_year_id = $request['school_academic_year_id' . $i];
                    }
                    $sale->booking_expiry = $expiry_time;
                    $sale->save();

                    //add sales_audit log
                    $sales_audit_obj = new SalesAudit();
                    $sales_audit_obj->sales_audit_log($sale->id, $type = 'validate');


                }
//                $expiry = Sale::where('booking_id',$sale_id)->select('booking_expiry')->first();

                if ($request['child_count'] > $i) {

                    for ($j = $i + 1; $j <= $request['child_count']; $j++) {

                        if ($request['start_dateTime' . $j] && $request['end_dateTime' . $j]) {
                            //start date Time format
                            $start_datetime = explode(' ', $request['start_dateTime' . $j]);
                            $start_day = $start_datetime[0];
                            $start_month = explode(',', $start_datetime[1]);

                            $start_month = Carbon::parse($start_month[0])->month;

                            $start_year = $start_datetime[2];

                            $start_time = explode(':', $start_datetime[3]);
                            $start_hour = $start_time[0];
                            $start_minutes = $start_time[1];

                            $start = Carbon::create($start_year, $start_month, $start_day, $start_hour, $start_minutes, 00)->toDateTimeString();
                            //end date Time format
                            $end_datetime = explode(' ', $request['end_dateTime' . $j]);
                            $end_day = $end_datetime[0];
                            $end_month = explode(',', $end_datetime[1]);

                            $end_month = Carbon::parse($end_month[0])->month;

                            $end_year = $end_datetime[2];

                            $end_time = explode(':', $end_datetime[3]);

                            $end_hour = $end_time[0];
                            $end_minutes = $end_time[1];

                            $end = Carbon::create($end_year, $end_month, $end_day, $end_hour, $end_minutes, 00)->toDateTimeString();


                        } elseif ($request['start_date' . $j] && $request['end_date' . $j]) {
                            $start_date = explode(' ', $request['start_date' . $j]);

                            $start_day = $start_date[0];

                            $start_month = explode(',', $start_date[1]);

                            $start_month = Carbon::parse($start_month[0])->month;

                            $start_year = $start_date[2];

                            $start = Carbon::create($start_year, $start_month, $start_day)->toDateTimeString();

                            $end_date = explode(' ', $request['end_date' . $j]);

                            $end_day = $end_date[0];

                            $end_month = explode(',', $end_date[1]);

                            $end_month = Carbon::parse($end_month[0])->month;

                            $end_year = $end_date[2];

                            $end = Carbon::create($end_year, $end_month, $end_day)->endOfDay()->toDateTimeString();


                        } else {
                            $start = null;
                            $end = null;
                        }

                        if ($request['school_year_id' . $j]) {

                            $school_year_id = $request['school_year_id' . $j];
                            $schoolYear = SchoolYear::where('id', $school_year_id)->first();
                            $splitName = explode('-', $schoolYear->title);

                            $start = Carbon::parse($schoolYear->from_date)->startOfDay()->toDateTimeString();

                            $end = Carbon::parse($schoolYear->to_date)->endOfDay()->toDateTimeString();

                        } else {
                            $school_year_id = null;
                        }


                        $sale = new Sale();
//                        if(!auth()->user()->can('3D Secure')) {
                        $sale->user_id = $request['user_id'];
//                        }
                        $sale->booking_id = $request['booking_id'];
                        $sale->booking_type = 1;
                        if (isset($language)) {
                            if ($language == 'fr') {
                                $sale->lang = 'FR';
                            } else {
                                $sale->lang = 'EN';
                            }
                        } else {
                            $sale->lang = 'EN';
                        }
                        $sale->title = 'LOCKER';
                        $sale->lock_type = 'm';
                        $sale->company_id = $request['company_id'];

                        $sale->start = $start;
                        $sale->end = $end;
                        $sale->email = $request['userEmail'];
//                        if ($request['locker_id' . $j] != 0 && $request['block_id' . $j] != 0) {
                        $sale->status = 'r';
//                        } else {
//                            $sale->status = 0;
//                        }
                        if ($request['booking_id']) {
                            $hold_sale = Sale::find($request['booking_id']);
//                            dd($hold_sale->hold_booking_date);
                            if ($hold_sale->hold_booking_date >= Carbon::now()->subHour()->toDateTimeString() && $hold_sale->hold_booking_date <= Carbon::now()->toDateTimeString()) {
                                $hold_booking_date = Carbon::parse($hold_sale->hold_booking_date)->addHour()->toDateTimeString();
                            } else {
                                $hold_booking_date = $hold_sale->hold_booking_date;
                            }
                        } else {
                            $hold_booking_date = Carbon::now()->addHour();
                        }
                        $sale->hold_booking_date = $hold_booking_date;
                        $sale->confirmation_sent = 0;
                        $sale->delete = 0;
                        $sale->street = $request['street'];
                        $sale->city = $request['city'];
                        $sale->post_code = $request['post_code'];
                        $sale->country = $request['country'];
                        $sale->child_first_name = $request['child_first_name' . $j];
                        $sale->child_surname = $request['child_surname' . $j];
                        if ($request['child_email' . $j]) {
                            $sale->child_email = $request['child_email' . $j];
                        }

                        $sale->school_year_id = $school_year_id;
                        if ($request['hold_locker' . $j]) {
                            if ($request['hold_locker' . $j]) {
                                $sale->hold_locker = 1;
                            } else {
                                $sale->hold_locker = 0;
                            }
                        } else {
                            $sale->hold_locker = 0;
                        }
                        if ($locationGroup->location_group_id == 1) {
                            $sale->school_academic_year_id = $request['school_academic_year_id' . $j];
                        }
                        $sale->booking_expiry = $expiry_time;
                        $sale->save();

                        //add sales_audit log
                        $sales_audit_obj = new SalesAudit();
                        $sales_audit_obj->sales_audit_log($sale->id, $type = 'validate');

                    }

                }


            } else {

                $sale = new Sale();
                $sale->user_id = $request['user_id'];
                $sale->booking_type = 1;
                if (isset($language)) {
                    if ($language == 'fr') {
                        $sale->lang = 'FR';
                    } else {
                        $sale->lang = 'EN';
                    }
                } else {
                    $sale->lang = 'EN';
                }
                $sale->title = 'LOCKER';

                $sale->lock_type = 'm';
                $sale->company_id = $request['company_id'];
                $sale->locker_id = $request['locker_id1'];
                $sale->block_id = $request['block_id1'];
                $sale->start = $start;
                $sale->end = $end;
                $sale->email = $request['userEmail'];
//                if ($request['locker_id1'] != 0 && $request['block_id1'] != 0) {
                $sale->status = 'r';
//                } else {
//                    $sale->status = 0;
//                }
                $sale->hold_booking_date = Carbon::now()->addHour();
                $sale->confirmation_sent = 0;
                $sale->delete = 0;
                $sale->street = $request['street'];
                $sale->city = $request['city'];
                $sale->post_code = $request['post_code'];
                $sale->country = $request['country'];
                $sale->child_first_name = $request['child_first_name1'];
                $sale->child_surname = $request['child_surname1'];
                if ($request['child_email1']) {
                    $sale->child_email = $request['child_email1'];
                }
                $sale->school_year_id = $school_year_id;
                if ($request['hold_locker']) {
                    Session::put('hold_locker_page2_checked', true);
                    if ($request['hold_locker1']) {
                        $sale->hold_locker = 1;
                    }
                }
                if ($locationGroup->location_group_id == 1) {
                    $sale->school_academic_year_id = $request['school_academic_year_id1'];
                }
                $sale->booking_expiry = $expiry_time;
                $sale->save();
                $sale_id = $sale->id;

                Session::put('sale_id', $sale_id);

                Sale::where('id', $sale_id)->update(['booking_id' => $sale_id]);

                //add sales_audit log
                $sales_audit_obj = new SalesAudit();
                $sales_audit_obj->sales_audit_log($sale->id);

                if ($request['child_count'] > 1) {

                    for ($i = 2; $i <= $request['child_count']; $i++) {

                        if ($request['start_dateTime' . $i] && $request['end_dateTime' . $i]) {

                            //start date Time format
                            $start_datetime = explode(' ', $request['start_dateTime' . $i]);
                            $start_day = $start_datetime[0];
                            $start_month = explode(',', $start_datetime[1]);

                            $start_month = Carbon::parse($start_month[0])->month;

                            $start_year = $start_datetime[2];

                            $start_time = explode(':', $start_datetime[3]);
                            $start_hour = $start_time[0];
                            $start_minutes = $start_time[1];

                            $start = Carbon::create($start_year, $start_month, $start_day, $start_hour, $start_minutes, 00)->toDateTimeString();
                            //end date Time format
                            $end_datetime = explode(' ', $request['end_dateTime' . $i]);
                            $end_day = $end_datetime[0];
                            $end_month = explode(',', $end_datetime[1]);

                            $end_month = Carbon::parse($end_month[0])->month;

                            $end_year = $end_datetime[2];

                            $end_time = explode(':', $end_datetime[3]);

                            $end_hour = $end_time[0];
                            $end_minutes = $end_time[1];

                            $end = Carbon::create($end_year, $end_month, $end_day, $end_hour, $end_minutes, 00)->toDateTimeString();


                        } elseif ($request['start_date' . $i] && $request['end_date' . $i]) {
                            $start_date = explode(' ', $request['start_date' . $i]);

                            $start_day = $start_date[0];

                            $start_month = explode(',', $start_date[1]);

                            $start_month = Carbon::parse($start_month[0])->month;

                            $start_year = $start_date[2];

                            $start = Carbon::create($start_year, $start_month, $start_day)->toDateTimeString();

                            $end_date = explode(' ', $request['end_date' . $i]);

                            $end_day = $end_date[0];

                            $end_month = explode(',', $end_date[1]);

                            $end_month = Carbon::parse($end_month[0])->month;

                            $end_year = $end_date[2];

                            $end = Carbon::create($end_year, $end_month, $end_day)->endOfDay()->toDateTimeString();

                        } else {
                            $start = null;
                            $end = null;
                        }

                        if ($request['school_year_id' . $i]) {

                            $school_year_id = $request['school_year_id' . $i];
                            $schoolYear = SchoolYear::where('id', $school_year_id)->first();

                            $start = Carbon::parse($schoolYear->from_date)->startOfDay()->toDateTimeString();

                            $end = Carbon::parse($schoolYear->to_date)->endOfDay()->toDateTimeString();


                        } else {
                            $school_year_id = null;
                        }

                        $sale = new Sale();
                        $sale->user_id = $request['user_id'];
                        $sale->booking_id = $sale_id;
                        $sale->booking_type = 1;
                        if (isset($language)) {
                            if ($language == 'fr') {
                                $sale->lang = 'FR';
                            } else {
                                $sale->lang = 'EN';
                            }
                        } else {
                            $sale->lang = 'EN';
                        }
                        $sale->title = 'LOCKER';

                        $sale->lock_type = 'm';
                        $sale->company_id = $request['company_id'];
                        $sale->locker_id = $request['locker_id' . $i];
                        $sale->block_id = $request['block_id' . $i];
                        $sale->start = $start;
                        $sale->end = $end;
                        $sale->email = $request['userEmail'];
//                        if ($request['locker_id' . $i] != 0 && $request['block_id' . $i] != 0) {
                        $sale->status = 'r';
//                        } else {
//                            $sale->status = 'r';
//                        }
                        $sale->hold_booking_date = Carbon::now()->addHour();
                        $sale->confirmation_sent = 0;
                        $sale->delete = 0;
                        $sale->street = $request['street'];
                        $sale->city = $request['city'];
                        $sale->post_code = $request['post_code'];
                        $sale->country = $request['country'];
                        $sale->child_first_name = $request['child_first_name' . $i];
                        $sale->child_surname = $request['child_surname' . $i];
                        if ($request['child_email' . $i]) {
                            $sale->child_email = $request['child_email' . $i];
                        }
                        $sale->school_year_id = $school_year_id;
                        if ($request['hold_locker']) {
                            if ($request['hold_locker' . $i]) {
                                $sale->hold_locker = 1;
                            }
                        }
                        if ($locationGroup->location_group_id == 1) {
                            $sale->school_academic_year_id = $request['school_academic_year_id' . $i];
                        }

                        $sale->booking_expiry = $expiry_time;
                        $sale->save();

                        //add sales_audit log
                        $sales_audit_obj = new SalesAudit();
                        $sales_audit_obj->sales_audit_log($sale->id);

                    }
                }

                Session::put('saleKey', $sale_id);
                Session::put('schoolTermSubmit', 2);
                Session::forget('schooltermbasedback');
            }
        }

        if (Session::has('saleKey')) {

//            if(Session::has('hold_locker_id')) {
//
//                $hold_locker_id = Session::get('hold_locker_id');
//                $hold_locker_childerens = Sale::where('booking_id', $hold_locker_id)->get();
//            }
            $voucher_code_id = $request['voucher_code_id'];
            $booking_id = Session::get('saleKey');

            return $this->show_bookings($booking_id);

        } else {
           
            Session::forget('arrowsPage');
            return redirect('/');
        }

    }

    public function show_bookings($booking_id)
    {

        $childrens = Sale::where('booking_id', $booking_id)->get();

        $childrens_count = $childrens->count();

        $child_blocks = [];
        $child_academic_year = [];

        if (Carbon::now() >= Carbon::parse($childrens[0]->hold_booking_date)->subHour()->toDateTimeString() && Carbon::now() <= $childrens[0]->hold_booking_date) {
            $hold_booking_date = (new \App\Sale)->hold_booking_date();
        }

        foreach ($childrens as $index => $children) {

            $index++;

            $location_group = Sale::join('companies', 'sales.company_id', '=', 'companies.id')
                ->where('sales.id', $children->id)
                ->select('companies.location_group_id')->first();

            $location_group = $location_group->location_group_id;

            if ($location_group == 1) {

                $schoolYear = SchoolYear::where('id', $children->school_year_id)->first();

                $start = Carbon::parse($schoolYear->from_date)->startOfDay()->toDateTimeString();
                $end = Carbon::parse($schoolYear->to_date)->endOfDay()->toDateTimeString();
//                dd(Session::get('hold_locker_checked'));
                if (Session::get('hold_locker_checked') == true) {

                    $book_next_year = true;
                } else {
                    $book_next_year = false;
                }

                $sales = new Sale();
                $blocks = $sales->school_year_blocks($children->company_id, $children->school_academic_year_id);
              
                if (count($blocks) > 0) {
                    $options = '';
                    $options .= '<option value="" selected>Choose Block</option>';
                    // dd($blocks);
                    foreach ($blocks as $block) {
                        // $block = Block::where('id', $block->block_id)->where('active', 1)->where('display', 1)->where([['start_date','<=',$children->start]])->orWhere('end_date','>=',$children->end)->orWhereNull('end_date')->first();
                        // print_r('qwe');
                   

                          $block=DB::select('SELECT * FROM blocks  WHERE id ='.$block->block_id.' AND active=1 AND display=1 AND IF(end_date IS NULL,(start_date BETWEEN "'.$children->start.'" AND "'.$children->end.'"), ((start_date BETWEEN "'.$children->start.'" AND "'.$children->end.'") OR (end_date BETWEEN "'.$children->start.'" AND "'.$children->end.'")) )  ORDER BY name ASC');
                            
//                        $block = Block::find($block->block_id);

//                        $sales = new Sale();
//                        $available_lockers = $sales->total_available_lockers_in_block($block->id,$start,$end,$book_next_year);
//dd($available_lockers);
//                        if($available_lockers['total_available'] > 0 || $children->block_id == $block->id) {
                        if(!empty($block) && count($block)>0){
                            $block=$block[0];
                            
                            $curdate=Carbon::now();
                           
                            $rates = $sales->rate_calculation($location_group, $block->id, $children->start, $children->end, $children->booking_id);
                            $unit = $rates[0];
                            $tax = $rates[1];

                            $sale = Sale::find($children->id);
                            if (Carbon::now() >= Carbon::parse($children->hold_booking_date)->subHour()->toDateTimeString() && Carbon::now() <= $children->hold_booking_date) {

                                $sale->hold_booking_date = $hold_booking_date;
                            }
                            if ($children->block_id == $block->id) {
                                $sale->price = $unit;
                                $sale->price_original = $unit;
                                $sale->tax = $tax;
                            }

                            $sale->save();


                            $currency = Company::join('currencies', 'companies.currency', '=', 'currencies.currency_name')
                                ->where('companies.id', $children->company_id)
                                ->select('currencies.currency_name', 'currencies.symbol', 'currencies.position')->first();

                            if ($currency->position == 'b') {

                                $options .= '<option  value="' . $block->id . '_' . $unit . '_' . $tax . '" ';
                                if ($block->id == $children->block_id) {
                                    $options .= 'selected';
                                }
                                $options .= '>' . $block->name . ' ' . $currency->symbol . '' . $unit . '</option>';

                            } else {

                                $options .= '<option  value="' . $block->id . '_' . $unit . '_' . $tax . '" ';
                                if ($block->id == $children->block_id) {
                                    $options .= 'selected';
                                }
                                $options .= '>' . $block->name . ' ' . $unit . ' ' . $currency->symbol . '</option>';
                            }
                        
                        }

                    }
               
                    $options .= '</select>';
//                        dd($unit);

                    $select_html = '<select class="form-control chzn-select blocks fa " id="blocks" data-sale_id="' . $children->id . '" data-start="' . $start . '" data-end="' . $end . '"   data-counter="' . $index . '"   name="block">';

                    $blocks_html = $select_html . '' . $options;

                    array_push($child_blocks, $blocks_html);
//                        dd($child_blocks);

                } else {

                    $options = '';
                    $options .= '<select class="form-control chzn-select  fa " id="blocks" data-sale_id="' . $children->id . '" data-start="' . $start . '" data-end="' . $end . '" data-counter="' . $index . '" name="block">';
                    $options .= '<option  value="" selected>Choose Block</option>';
                    $options .= '</select>';
                    array_push($child_blocks, $options);
                }

                $school_academic_year = SchoolAcademicYear::find($children->school_academic_year_id);
//                dd($children->school_academic_year_id);
                array_push($child_academic_year, $school_academic_year->school_year);


            } elseif ($location_group == 2 || $location_group == 3) {

                $start = Carbon::parse($children->start)->format('Y-m-d H:i:s');
                $end = Carbon::parse($children->end)->format('Y-m-d H:i:s');
                $schoolYear = 0;


                // $blocks = Block::where('company_id', $children->company_id)->where('active', 1)->where('display', 1)->where([['start_date','<=',$children->start],['end_date','>=',$children->end]])->orderBy('name', 'ASC')->get();
                    $block=DB::select('SELECT * FROM blocks  WHERE id ='.$block->block_id.' AND active=1 AND display=1 AND IF(end_date IS NULL,(start_date BETWEEN "'.$children->start.'" AND "'.$children->end.'"), ((start_date BETWEEN "'.$children->start.'" AND "'.$children->end.'") OR (end_date BETWEEN "'.$children->start.'" AND "'.$children->end.'")) )  ORDER BY name ASC');
                 // print_r('123');
                        // dd($block->toArray());



                if (count($blocks) > 0) {


                    $options = '';

                    $options .= '<option  value="" selected>Choose Block</option>';


                    foreach ($blocks as $block) {
                         $curdate=Carbon::now();
                          
                        $book_next_year = false;

//                        $sales = new Sale();
//                        $available_lockers =  $sales->total_available_lockers_in_block($block->id,$start,$end,$book_next_year);

//                        if($available_lockers['total_available'] > 0 || $children->block_id == $block->id) {


                        $sales = new Sale();
                        $rates = $sales->rate_calculation($location_group, $block->id, $start, $end, $children->booking_id);
                        $lowest_rate = $rates[0];
                        $tax = $rates[1];

                        $currency = Company::join('currencies', 'companies.currency', '=', 'currencies.currency_name')
                            ->where('companies.id', $block->company_id)
                            ->select('currencies.currency_name', 'currencies.symbol', 'currencies.position')->first();

//

                        if ($currency->position == 'b') {

                            $options .= '<option  value="' . $block->id . '_' . $lowest_rate . '_' . $tax . '" ';
                            if ($block->id == $children->block_id) {
                                $options .= 'selected';
                            }
                            $options .= '>' . $block->name . ' ' . $currency->symbol . '' . $lowest_rate . '</option>';
                        } else {
                            $options .= '<option  value="' . $block->id . '_' . $lowest_rate . '_' . $tax . '" ';
                            if ($block->id == $children->block_id) {
                                $options .= 'selected';
                            }
                            $options .= '>' . $block->name . ' ' . $lowest_rate . ' ' . $currency->symbol . '</option>';
                        }

                    
                    }

                    $options .= '</select>';

                    $select_html = '<select class="form-control chzn-select blocks fa child' . $index . '" id="blocks' . $index . '" data-sale_id="' . $children->id . '" data-start="' . $start . '" data-end="' . $end . '" data-end="' . $end . '" data-rate="' . $lowest_rate . '" data-tax="' . $tax . '" data-counter="' . $index . '" name="block' . $index . '">';

                    $blocks_html = $select_html . '' . $options;

                    array_push($child_blocks, $blocks_html);

                } else {

                    $options = '';
                    $options .= '<select class="form-control chzn-select blocks child' . $index . ' fa " id="blocks' . $index . '" data-sale_id="' . $children->id . '" data-start="' . $start . '" data-end="' . $end . '" data-counter="' . $index . '" name="block' . $index . '">';
                    $options .= '<option  value="" selected>Choose Block</option>';
                    $options .= '</select>';

                    array_push($child_blocks, $options);
                }
            }
        }

        $expiry = Sale::where('booking_id', $booking_id)->select('booking_expiry')->first();

        $expiryTime = Carbon::parse($expiry->booking_expiry);

        if ($expiryTime > Carbon::now()) {
            $remainingTime = $expiryTime->diffInSeconds(Carbon::now());

            if (Session::get('arrowsPage') == 2) {
                Session::put('arrowsPage', 3);
                Session::put('page_number', 3);
            }

            $placeholders = FrontendPlaceholder::where('key', 'choose-locker-child-name-date-title')->pluck('placeholder')->toArray();
            $child_name_date_placeholders = implode(",", $placeholders);
//                dd($childrens);

            return view('frontend.pages.choose_locker', compact('childrens', 'childrens_count', 'location_group', 'schoolYear', 'child_blocks', 'booking_id', 'remainingTime', 'child_name_date_placeholders', 'child_academic_year'));

        } else {

            Session::forget('arrowsPage');
            return redirect('/');
        }

    }

    public function saveSchoolBookingBack()
    {

        if (Session::has('saleKey')) {

            $booking_id = Session::get('saleKey');

            return $this->show_bookings($booking_id);

        } else {

            Session::forget('arrowsPage');
            return redirect('/');
        }


    }

    public function saveSchoolBookingforward()
    {

        if (Session::has('saleKey')) {

            $booking_id = Session::get('sale_id');


            $companyIfo = Sale::Join('lockers', 'lockers.id', '=', 'sales.locker_id')
                ->Join('blocks', 'blocks.id', '=', 'sales.block_id')
                ->Join('companies', 'companies.id', '=', 'sales.company_id')
                ->Join('users', 'users.id', '=', 'sales.user_id')
                ->select('blocks.name as block_name', 'lockers.locker_number', 'sales.id as sale_id', 'companies.id as company_id', 'companies.location_group_id', 'sales.booking_id as booking_id', 'users.id as user_id', 'users.name', 'users.surname', 'users.username', 'users.email', 'users.address_1', 'users.address_2', 'users.city as user_city', 'users.post_code as user_post_code', 'users.state as user_state', 'sales.lang', 'sales.lock_type', 'sales.price', 'sales.tax', 'companies.name as company_name', 'companies.street as company_street', 'companies.city as company_city', 'companies.country as company_country')
                ->where('sales.booking_id', $booking_id)
                ->first();

            $lockerInfo = Sale::Join('lockers', 'lockers.id', '=', 'sales.locker_id')
                ->Join('blocks', 'blocks.id', '=', 'sales.block_id')
                ->Join('companies', 'companies.id', '=', 'sales.company_id')
                ->select('blocks.name as block_name', 'lockers.locker_number', 'sales.id as sale_id', 'sales.booking_id as booking_id', 'sales.start', 'sales.end', 'sales.child_email', DB::raw("CONCAT(sales.child_first_name, ' ' ,sales.child_surname) AS childName"), 'sales.price', 'sales.tax')
                ->where('sales.booking_id', $booking_id)
                ->get();

            $total_price = 0;

            foreach ($lockerInfo as $locker) {

                $total_price += $locker->price;
                $total_price = number_format((float)$total_price, 2, '.', '');
            }

            $expiry = Sale::where('booking_id', $booking_id)->select('booking_expiry')->first();
            $expiryTime = Carbon::parse($expiry->booking_expiry);

            $currency = Company::join('currencies', 'companies.currency', '=', 'currencies.currency_name')
                ->where('companies.id', $companyIfo->company_id)
                ->select('currencies.currency_name', 'currencies.symbol', 'currencies.position')->first();

            $charity = Charity::first();
            $placeholders = FrontendPlaceholder::where('key', 'payments-charity-card-main-disclaimer')->pluck('placeholder')->toArray();
            $charity_declaimer_placeholder = implode(",", $placeholders);


            if ($expiryTime > Carbon::now()) {

                $remainingTime = $expiryTime->diffInSeconds(Carbon::now());
                return view('frontend.pages.payments.payments', compact('booking_id', 'companyIfo', 'lockerInfo', 'currency', 'total_price', 'remainingTime', 'charity', 'charity_declaimer_placeholder'));
            } else {

                return redirect('/');
            }
        } else {
            return redirect('/');
        }


    }

    public function updateSale(Request $request)
    {

        Session::forget('schoolTermSubmit');

        if (Session::has('saleKey')) {

            $sale = Sale::where('id', $request['sale_id'])->first();
            $company_id = $sale->company_id;
            $start = $sale->start;
            $end = $sale->end;
            $booked_lockers = Sale::where('company_id', $company_id)
                ->where('locker_id', $request['locker_id'])
                ->where('block_id', $request['block_id'])
                ->where(function ($query) {
                    $query->where('status', 'b')
                        ->orWhere('status', 'r');
                })
                ->where(function ($query) use ($start, $end) {
                    $query->where('end', '>=', $start)
                        ->Where('start', '<=', $end);
                })
                ->first();
            if ($booked_lockers) {
                $data = [
                    'status' => 'already_exists',
                    'locker_id' => $request['locker_id']
                ];

            } else {

                $block = Block::find($request['block_id']);

                $sale->locker_id = $request['locker_id'];
                $sale->block_id = $request['block_id'];
                $sale->block_name = $block->name;
                $sale->number = $request['locker_number'];
                $sale->price = $request['rate'];
                $sale->price_original = $request['rate'];
                $sale->tax = $request['tax'];
                $sale->status = 'r';
                $sale->save();

                //update Manual locker code using sale_id
                $code = new Sale();
                $code->update_locker_code($request['sale_id']);

                //add sales_audit log
                $sales_audit_obj = new SalesAudit();
                $sales_audit_obj->sales_audit_log($sale->id);

                $data = [
                    'status' => 'available',
                    'locker_id' => $request['locker_id']
                ];

            }
            return response()->json($data);

        } else {
            return response()->json('home_page');
        }

    }

    public function expireBooking(Request $request)
    {

        $sessions_expire = new Payment();
        $sessions_expire->sessions_expire();


        return response()->json('success');

    }

    public function lockerAvailabilityCheck(Request $request)
    {

        $start = Carbon::parse($request['start_date'])->toDateTimeString();
        $end = Carbon::parse($request['end_date'])->toDateTimeString();

        $booked_lockers = Sale::where('company_id', $request['company_id'])
            ->where(function ($query) {
                $query->where('status', 'b')
                    ->orWhere('status', 'r');
            })
            ->where(function ($query) use ($start, $end) {
                $query->where('end', '>=', $start)
                    ->Where('start', '<=', $end);
            })
            ->get();

        $total_booked_lockers = $booked_lockers->count();

        $total_maintenance_lockers = Maintenance::where('company_id', $request['company_id'])->where('to', '>=', $start)->Where('from', '<=', $end)->where('active', 1)->count();


        $lockers_available = Locker::join('blocks', 'lockers.block_id', '=', 'blocks.id')->
        join('rates', function ($join) use ($start, $end) {
            $join->on('blocks.rate_id', '=', 'rates.id')
                ->where('rates.valid_too', '>=', $start)
                ->Where('rates.valid_from', '<=', $end);
        })->where('lockers.company_id', $request['company_id'])->where('blocks.active', 1)->where('blocks.display', 1)->where('lockers.active', 1)->where('lockers.display', 1)->count();

        $total = $lockers_available - $total_maintenance_lockers - $total_booked_lockers;

        if ($total >= 1) {
            $validation_error = false;
        } else {
            $validation_error = true;
        }

        return response()->json($validation_error);
    }

    public function lockerAvailabilityCheckSchoolYear(Request $request)
    {

        $schoolYear = SchoolYear::where('id', $request['shcoolYearId'])->first();
        $splitName = explode('-', $schoolYear->title);

        $start = Carbon::parse($schoolYear->from_date)->startOfDay()->toDateTimeString();
        $end = Carbon::parse($schoolYear->to_date)->endOfDay()->toDateTimeString();


        $available = (new \App\Sale)->school_year_validation($request['company_id'], $request['shcoolAcademicYearId'], $start, $end, $request['hold_locker']);

        $start_next = Carbon::parse($schoolYear->from_date)->startOfDay()->addYear()->toDateTimeString();
        $end_next = Carbon::parse($schoolYear->to_date)->endOfDay()->addYear()->toDateTimeString();


        if ($available['total_available'] >= 1) {
            $validation_error = false;
        } else {
            $validation_error = true;
        }
        $data = [
            'validation_error' => $validation_error,
            'splitName' => $splitName,
            'start_next' => $start_next,
            'end_next' => $end_next,
        ];

        return response()->json($data);


    }

    public function holdlocker(Request $request)
    {


        $schoolYear = SchoolYear::where('id', $request['shcoolYearId'])->first();

        $splitName = explode('-', $schoolYear->title);

        $start = Carbon::parse($schoolYear->from_date)->startOfDay()->toDateTimeString();
        $end = Carbon::parse($schoolYear->to_date)->endOfDay()->addYear()->toDateTimeString();
        $shcoolAcademicYearId = $request['shcoolAcademicYearId'];


        $blocks = Block::join('block_years', 'block_years.block_id', '=', 'blocks.id')->
        join('rates', function ($join) use ($start, $end) {
            $join->on('blocks.rate_id', '=', 'rates.id')
                ->where('rates.valid_too', '>=', $start)
                ->Where('rates.valid_from', '<=', $end);
        })->where('blocks.company_id', $request['company_id'])->where('blocks.active', 1)->where('blocks.active', 1)
            ->where(function ($query) use ($shcoolAcademicYearId) {
                $query->where('block_years.school_academic_year_id', $shcoolAcademicYearId);

            })->distinct('blocks.id')->pluck('blocks.id')->toArray();

        $booked_lockers = Sale::where('company_id', $request['company_id'])
            ->where(function ($query) {
                $query->where('status', 'b')
                    ->orWhere('status', 'r');
            })
            ->where('end', '>=', $start)
            ->Where('start', '<=', $end)
            ->whereIn('block_id', $blocks)
            ->get();

        $total_booked_lockers = $booked_lockers->count();
//        dd($total_booked_lockers);
        $total_maintenance_lockers = Maintenance::where('company_id', $request['company_id'])->where('to', '>=', $start)->Where('from', '<=', $end)->where('active', 1)->whereIn('block_id', $blocks)->count();

        $lockers_available = Locker::join('blocks', 'lockers.block_id', '=', 'blocks.id')
            ->join('block_years', 'block_years.block_id', '=', 'blocks.id')
            ->join('rates', function ($join) use ($start, $end) {
                $join->on('blocks.rate_id', '=', 'rates.id')
                    ->where('rates.valid_too', '>=', $start)
                    ->Where('rates.valid_from', '<=', $end);
            })->where('lockers.company_id', $request['company_id'])->where('blocks.active', 1)->where('blocks.active', 1)->where('lockers.active', 1)->where('lockers.display', 1)
            ->where('block_years.school_academic_year_id', $shcoolAcademicYearId)
            ->count();
//dd($total_maintenance_lockers);
        $total = $lockers_available - $total_maintenance_lockers - $total_booked_lockers;

        if ($total >= 1) {
            $validation_error = false;
        } else {
            $validation_error = true;
        }
        $start = Carbon::parse($schoolYear->from_date)->startOfDay()->addYear()->toDateTimeString();
        $end = Carbon::parse($schoolYear->to_date)->endOfDay()->addYear()->toDateTimeString();


        $data = [
            'validation_error' => $validation_error,
            'splitName' => $splitName,
            'start_date' => $start,
            'end_date' => $end,
        ];

        return response()->json($data);
    }

    public function getlockers(Request $request)
    {
        // check if sale key is not available 
        // put sale key again in the session for further process
        if(!Session::has('saleKey'))
        {
            Session::put('saleKey', $request->sale_id);
        }
        if (Session::has('saleKey')) {

            $start = Carbon::parse($request['start'])->format('Y-m-d H:i:s');
            $end = Carbon::parse($request['end'])->format('Y-m-d H:i:s');
            if (Session::get('hold_locker_checked') == true) {
                $book_next_year = true;
            } else {
                $book_next_year = false;
            }
//            dd($book_next_year);
            $sale = Sale::find($request->sale_id);
            $selected_block_id = $sale->block_id;

            $sales = new Sale();
            $available_lockers = $sales->total_available_lockers_in_block($request->block_id, $start, $end, $book_next_year);
        
            if ($available_lockers['total_available'] > 0 || $selected_block_id == $request->block_id) {

                $lockers = Locker::where('lockers.block_id', $request->block_id)
                    ->where('active', 1)
//                        ->where('display',1)
                    ->orderBy('locker_order', 'ASC')
                    ->get();

                $total_lockers = $lockers->count();
                $lockers->toArray();

                $booked_lockers = Sale::where('booking_id', Session::get('saleKey'))->pluck('locker_id')->toArray();

                $assign_locker = 0;
                if (empty(intval($request->selected_lockers))) {
                    $selected_lockers = Sale::where('id', $request->sale_id)->first();
                    if (!empty($selected_lockers->locker_id)) {
                        $assign_locker = $selected_lockers->locker_id;
                    }
                }

                //$request->selected_lockerssale_id
                $output = '';

                $blocks = Block::leftJoin('locker_block_image', 'locker_block_image.id', '=', 'blocks.locker_block_image_id')
                    ->select('blocks.*', 'locker_block_image.file_name as locker_block_image')
                    ->where('blocks.id', $request->block_id)->first();

                $output .= '<div class="col-md-12" ><div class="row" style="overflow-x:auto;"><div class="col-md-12 lockers_table" style="padding-left: 0px !important;padding-right:0px !important;" ><table id="locker_table' . $request['counter'] . '">';

                $counter = 0;
                $counter_red = 0;
            
                for ($i = 1; $i <= $blocks->locker_blocks; $i++) {

                    $red_column = 0;
                    $output .= '<tr>';
                    for ($j = 1; $j <= $blocks->block_height; $j++) {
                        if ($counter_red <= $total_lockers - 1) {
                            $sales = new Sale();
                            $available = $sales->is_locker_available($lockers[$counter_red]['id'], $start, $end, $book_next_year);

                            if (empty($available)) {

                                if (!in_array($lockers[$counter_red]['id'], $booked_lockers))
                                    $red_column++;
                            }

                        } else {
                            $red_column++;
                        }

                        $counter_red++;

                    }

                    for ($j = 1; $j <= $blocks->block_height; $j++) {

                        if ($counter <= $total_lockers - 1) {

                            $sales = new Sale();

                            //// if(in_array($lockers[$counter]['id'],$booked_lockers))
                            $available = 1;
                            //  else
                            $available = $sales->is_locker_available($lockers[$counter]['id'], $start, $end, $book_next_year);
                            $is_off_sale = LockerOffSale::where('locker_id', $lockers[$counter]['id'])->where('active', 1)->first();


                            if (empty($available)) {

                                $output .= '<td ><input type = "radio" name = "locker_id' . $request['counter'] . '" class="invisible-radio' . $request['counter'] . '" checked>';
                                if ($red_column == $blocks->block_height) {
                                    $output .= '<label for="radio' . $lockers[$counter]['id'] . '" class="red-lockers" > ';
                                } else {
                                    $output .= '<label for="radio' . $lockers[$counter]['id'] . '" class="" > ';

                                }

                                $already_selected = 0;
                                if ($lockers[$counter]['id'] == intval($request->selected_lockers))
                                    $already_selected = 1;

                                else if ($lockers[$counter]['id'] == $assign_locker)
                                    $already_selected = 1;

                                if (empty($already_selected)) {
                                    $output .= '<div class="styled-radio-red' . $request['counter'] . ' text-center" style = "color: white">' . $lockers[$counter]->locker_number . '<br ><i class="fa fa-times icon-cross" ></i ></div ></label ></td>';
                                } else {

                                    $output .= '<div class="styled-radio' . $request['counter'] . ' text-center" style = "color: white; border: 5px solid #66ff66">' . $lockers[$counter]->locker_number . '<br ><i class="fa fa-check rounded-circle" style="background-color:#66ff66; padding: 6px""></i ></div ></label ></td>';
                                    // $output .= '<div class="styled-radio'.$request['counter'].' text-center" style = "color: white;">'.$lockers[$counter]->locker_number.'<br ><i class="fa fa-check icon-tick" ></i ></div ></label ></td >';
                                }

                            } elseif (!empty($is_off_sale)) {
                                $output .= '<td><input type = "radio" name = "locker_id' . $request['counter'] . '"  class="invisible-radio' . $request['counter'] . '" >
                                <label for="radio' . $lockers[$counter]['id'] . '' . $request['counter'] . '" class="">
                                    <div class="styled-radio-red' . $request['counter'] . ' text-center" style = "color: white;">' . $lockers[$counter]->locker_number . '<br ><i class="fa fa-check rounded-circle" style="background-color:#66ff66; padding: 6px""></i ></div ></label ></td >';
                            } elseif ($lockers[$counter]['active'] == 1 && $lockers[$counter]['display'] == 0) {
                                $output .= '<td><input type = "radio" name = "locker_id' . $request['counter'] . '"  class="invisible-radio' . $request['counter'] . '" >
                                <label for="radio' . $lockers[$counter]['id'] . '' . $request['counter'] . '" class="">
                                    <div class="styled-radio-gray' . $request['counter'] . ' text-center" style = "color: white;"></div ></label ></td >';
                            } else {
                                $output .= '<td><input type = "radio" name = "locker_id' . $request['counter'] . '" id = "radio' . $lockers[$counter]['id'] . '' . $request['counter'] . '" value="' . $lockers[$counter]['id'] . '" data-sale_id = "' . $request->sale_id . '" data-block_id = "' . $lockers[$counter]->block_id . '" data-locker_id = "' . $lockers[$counter]['id'] . '" data-locker_number = "' . $lockers[$counter]->locker_number . '" data-rate = "' . $request['rate'] . '" data-tax = "' . $request['tax'] . '" data-counter = "' . $request['counter'] . '" data-hold_locker="' . $request['hold_locker'] . '" class="invisible-radio' . $request['counter'] . ' choose-locker" >
                                <label for="radio' . $lockers[$counter]['id'] . '' . $request['counter'] . '" class="puffOut">
                                    <div class="styled-radio' . $request['counter'] . ' text-center" style = "color: white;">' . $lockers[$counter]->locker_number . '<br ><i class="fa fa-check icon-tick" ></i ></div ></label ></td >';
                            }


                        } else {
                            //                                                    $output.='<td> <input type = "radio" name = "id'.$request['counter'].'" class="invisible-radio'.$request['counter'].'" >
                            //                                                        <label for="radio" >
                            //                                                            <div class="styled-radio-gray'.$request['counter'].' text-center" style = "color: white" ></div ></label >
                            //                                                    </td>';
                        }
                        $counter++;
                    }
                    $output .= '</tr>';
                }

                $output .= '</table></div><div style="display: none;" class="col-md-12 locker_image">
                    <img style="width:100%;" src="' . url('uploads/' . $blocks->locker_block_image) . '" class=" img-responsive" alt="Block Image">
                    </div></div></div>';

                return response()->json($output);
            } else {

                $output = 'booked';
                return response()->json($output);
            }
        } else {
            return response()->json('home_page');

        }


    }

    public function validateSale(Request $request)
    {
//        dd($request->all());
        if (Session::has('saleKey')) {
            $bookings = Sale::where('booking_id', $request['sale_id'])->get();
            if (Session::get('hold_locker_checked') == true) {
                $book_next_year = true;
            } else {
                $book_next_year = false;
            }
            $sales = new Sale();
            $existed_lockers = [];
            $selected_lockers = [];
            foreach ($bookings as $booking) {
                if ($booking->locker_id != null & $booking->block_id != null) {
                    $available = $sales->is_locker_available_except_this($booking->id, $booking->locker_id, $booking->start, $booking->end, $book_next_year);

                    if (empty($available)) {
                        array_push($selected_lockers, $booking->locker_id);

                        array_push($existed_lockers, $booking->id);

                    }

                }
            }

            $bookings = Sale::where('booking_id', $request['sale_id'])->get();
            $count = $bookings->count();
            $data = [
                'bookings' => $bookings,
                'count' => $count,
                'existed_lockers' => $existed_lockers,
                'selected_lockers_db' => $selected_lockers,
            ];

            return response()->json($data);
        } else {
            return response()->json('home_page');
        }
    }

    public function payments(Request $request)
    {

        if (Session::has('saleKey')) {
            if (Session::get('arrowsPage') == 3) {
                Session::put('arrowsPage', 4);
                Session::put('page_number', 4);
            }
            $booking_id = $request->sale_id;

            $companyIfo = Sale::Join('lockers', 'lockers.id', '=', 'sales.locker_id')
                ->Join('blocks', 'blocks.id', '=', 'sales.block_id')
                ->Join('companies', 'companies.id', '=', 'sales.company_id')
                ->Join('users', 'users.id', '=', 'sales.user_id')
                ->select('blocks.name as block_name', 'lockers.locker_number', 'sales.id as sale_id', 'companies.id as company_id', 'companies.location_group_id', 'sales.booking_id as booking_id', 'users.id as user_id', 'users.name', 'users.surname', 'users.username', 'users.email', 'users.address_1', 'users.address_2', 'users.city as user_city', 'users.post_code as user_post_code', 'users.state as user_state', 'sales.lang', 'sales.lock_type', 'sales.price', 'sales.tax', 'companies.name as company_name', 'companies.street as company_street', 'companies.city as company_city', 'companies.country as company_country')
                ->where('sales.booking_id', $booking_id)
                ->first();


            $lockerInfo = Sale::Join('lockers', 'lockers.id', '=', 'sales.locker_id')
                ->Join('blocks', 'blocks.id', '=', 'sales.block_id')
                ->Join('companies', 'companies.id', '=', 'sales.company_id')
                ->select('blocks.name as block_name', 'lockers.locker_number', 'sales.id as sale_id', 'sales.booking_id as booking_id', 'sales.start', 'sales.end', 'sales.child_email', DB::raw("CONCAT(sales.child_first_name, ' ' ,sales.child_surname) AS childName"), 'sales.price', 'sales.tax')
                ->where('sales.booking_id', $booking_id)
                ->get();

            $total_price = 0;


            foreach ($lockerInfo as $locker) {

                $total_price += $locker->price;
                $total_price = number_format((float)$total_price, 2, '.', '');
            }

            $expiry = Sale::where('booking_id', $booking_id)->select('booking_expiry')->first();
            $expiryTime = Carbon::parse($expiry->booking_expiry);

            $currency = Company::join('currencies', 'companies.currency', '=', 'currencies.currency_name')
                ->where('companies.id', $companyIfo->company_id)
                ->select('currencies.currency_name', 'currencies.symbol', 'currencies.position')->first();

            $charity = Charity::first();

            $placeholders = FrontendPlaceholder::where('key', 'payments-charity-card-main-disclaimer')->pluck('placeholder')->toArray();
            $charity_declaimer_placeholder = implode(",", $placeholders);


            if ($expiryTime > Carbon::now()) {
                
                $remainingTime = $expiryTime->diffInSeconds(Carbon::now());
                return view('frontend.pages.payments.payments', compact('booking_id', 'companyIfo', 'lockerInfo', 'currency', 'total_price', 'remainingTime', 'charity', 'charity_declaimer_placeholder'));
            } else {

                return redirect('/');
            }
        } else {
            return redirect('/');
        }


    }

    public function charitywork(Request $request)
    {
        // dd($request->all());
        if (Session::has('saleKey')) {
            $placeholders = FrontendPlaceholder::where('key', 'payments-charity-card-main-disclaimer')->pluck('placeholder')->toArray();
            $charity_declaimer_placeholder = implode(",", $placeholders);

            $data = str_replace(explode(',', $charity_declaimer_placeholder), [$request->lockerInfo, $request->total_price, $request->price, 'logout'], t('payments-charity-card-main-disclaimer'));
            return response()->json($data);
        } else {
            return response()->json('home_page');
        }
    }

    public function refresh_payments(Request $request)
    {
        if (Session::has('saleKey')) {
            Artisan::call('sagepay:status');

            return response()->json('success');
        } else {
            return response()->json('home_page');
        }
    }

    public function savepayments(Request $request)
    {
//          dd($request->all());

        if (Session::has('saleKey')) {
//            Artisan::call('sagepay:status');
            $booking_id = $request->sale_id;

            $company_currency = new Payment();
            $currency = $company_currency->company_currency($booking_id);
            if ($request->ajax()) {
                $ajax = true;
            } else {
                $ajax = false;
            }
            $phone_number = null;
            if ($request->phone_number) {
                $request_phone = $request->phone_number[0];
                if ($request_phone == "0") {
                    $phone_number = "+44" . substr($request->phone_number, 1);

                } else if ($request_phone == "+") {
                    $phone_number = $request->phone_number;
                } else {
                    $phone_number = "+44" . $request->phone_number;
                }

                $update_phone_number = new Payment();
                $update_phone_number->update_phone_number($booking_id, $phone_number);

            }

            $user_info = new Payment();
            $user = $user_info->user_info($booking_id);
            $user_id = $user->id;
            $user_email = $user->email;
            $first_name = $request['firstname'];
            $last_name = $request['payer_surname'];
            $billing_address1 = $request['billing_address1'];
            $billing_address2 = $request['billing_address2'];
            $city = $request['city'];
            $country = $request['country'];
            $charity_id = $request['charity_id'];

            $post_code = $request['post_code'];
            $state = $request['state'];
            $last4Digits = substr(str_replace(' ', '', $request['cardNumber']), -4);

            $price_calc = new Payment();
            $price = $price_calc->price_calculation($booking_id, $request->charity_text_custom);

            $total_lockers_price = $price[0];
            $charity_price = $price[1];
            $total_price = $price[2];


            $payment_type = $request['payment_type'];
            if ($request['payment_type'] == 'Virtual') {
                $transaction_type = '01';
            } else {
                $transaction_type = '02';
            }

            $booking_id_in_payments = Payment::where('booking_id', $booking_id)->first();

            if (empty($booking_id_in_payments)) {

                $payment = new Payment();
                $payment_id = $payment->insert_payments($user_id, $booking_id, $billing_address1, $billing_address2, $city, $country, $first_name, $phone_number, $post_code, $state, $last_name, $currency, $user_email);

            } else {

                $payment_id = $booking_id_in_payments->id;

            }

            //check if waiting response 1 exists in transaction
            $booking_id_in_transaction = Transaction::where('booking_id', $booking_id)->where('waiting_response_sagepay', 1)->first();
//            dd($booking_id_in_transaction);
           //if waiting response 1 exists update to 0
            if ($booking_id_in_transaction) {

                $code = '77777';
                $status_message = 'Transaction not completed by unknown system error';
                $fraud_score = 1;
                $amount = null;
                $transactionId = null;
                $waiting_response_sagepay = 0;

                $transaction = new Payment();
                $transaction->update_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $amount, $transaction_type, $code, $status_message, $fraud_score, $transactionId, $waiting_response_sagepay);


            }

            //create new empty transaction
            $fraud_score = 0;
            $amount = null;
            $transactionId = null;
            $vendorTxCode = 'lockers_' . Carbon::now()->format('Ymdhis') . '_' . $booking_id;
            $status_code = '2000.xweb';
            $status_message = 'No Response';
            $waiting_response_sagepay = 1;

            $transaction = new Payment();
            $transaction->insert_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $amount, $transaction_type, $status_code, $vendorTxCode, $status_message, $fraud_score, $transactionId, $waiting_response_sagepay);

             $payment = Payment::find($payment_id);

            if (empty($payment->amount) || $payment->amount == '0.00') {

                Session::put('user_payments_form_info', $request->all());
                Session::put('on_payments_booking_id', $booking_id);
                if ($total_price > 0) {

                    $Authorization = paymentConfig::integrationKey();
//                    dd($Authorization);

                    $merchant_session_url = \Config::get('app.merchant_session_url');

                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => $merchant_session_url,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => '{ "vendorName": "lockers" }',
                        CURLOPT_HTTPHEADER => array(
                            "Authorization: Basic $Authorization",
                            "Cache-Control: no-cache",
                            "Content-Type: application/json"
                        ),
                    ));
                    $session_response = curl_exec($curl);

                    $err = curl_error($curl);

                    curl_close($curl);

                    $session_response = json_decode($session_response);

                    if (isset($session_response->merchantSessionKey)) {
                        $merchantSessionKey = $session_response->merchantSessionKey;

                        $expiryDate = $request['expiryDate'] . $request['expiryYear'];
//
                        $curl = curl_init();
                        $card_postArray = [
                            'cardDetails' => [
                                'cardholderName' => $request['owner'],
                                'cardNumber' => str_replace(' ', '', $request['cardNumber']),
                                'expiryDate' => $expiryDate,
                                'securityCode' => $request['cvv'],
                            ]
                        ];
                        $card_postArray_json = json_encode($card_postArray);
                        $card_identifier_url = \Config::get('app.card_identifier_url');

                        curl_setopt_array($curl, array(
                            CURLOPT_URL => $card_identifier_url,
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_CUSTOMREQUEST => "POST",
                            CURLOPT_POSTFIELDS => $card_postArray_json,
                            CURLOPT_HTTPHEADER => array(
                                "Authorization: Bearer $merchantSessionKey",
                                "Cache-Control: no-cache",
                                "Content-Type: application/json"
                            ),
                        ));

                        $card_response = curl_exec($curl);
                        $err = curl_error($curl);
                        curl_close($curl);

                        $card_response = json_decode($card_response);
//                        dd($card_response);
                        if (isset($card_response->cardIdentifier)) {


                            $cardIdentifier = $card_response->cardIdentifier;
//                            $transaction = Transaction::where('booking_id', $booking_id)->where('waiting_response_sagepay', '1')->first();
//                            if (empty($transaction)) {
//                                $fraud_score = 0;
//                                $amount = null;
//                                $transactionId = null;
//                                $vendorTxCode = 'lockers_' . Carbon::now()->format('Ymdhis') . '_' . $booking_id;
//                                $status_code = '2000.xweb';
//                                $status_message = 'No Response';
//                                $waiting_response_sagepay = 1;
//                                $transaction = new Payment();
//                                $transaction_id = $transaction->insert_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $amount, $transaction_type, $status_code, $vendorTxCode, $status_message, $fraud_score, $transactionId, $waiting_response_sagepay);
//                                $transaction = Transaction::where('booking_id', $booking_id)->where('waiting_response_sagepay', '1')->first();
//                            }
//
//                            $vendorTxCode = $transaction->vendor_tx_code;
                            $command = 'getTransactionDetail';

                            $get_transaction = new Payment();
                            $sage_pay_response = $get_transaction->get_transaction_from_sagepay_using_vendor_tx_code($vendorTxCode, $command);

//                             dd($sage_pay_response);
                            if ($sage_pay_response['errorcode'] == '0043') {

                                $sagepay_transaction = new Payment();
                                return $sagepay_transaction->sagepay_transaction($merchantSessionKey, $cardIdentifier, $Authorization, $vendorTxCode, $total_price, $currency, $transaction_type,
                                    $first_name, $last_name, $billing_address1, $billing_address2, $city, $state, $post_code, $country, $phone_number, $booking_id, $payment_id, $user_id, $user_email, $last4Digits, $total_lockers_price, $charity_price, $request->charity_id, $ajax);

                            } else {

                                $transactionId = $sage_pay_response['vpstxid'];

                                $get_transaction = new Payment();
                                $retrieve_transaction_response = $get_transaction->get_transaction_from_sagepay_using_transaction_id($transactionId);

                                if (isset($retrieve_transaction_response->statusCode)) {
                                    if ($retrieve_transaction_response->statusCode == '0000') {

                                        if (Session::get('hold_locker_checked') == true) {

                                            $locker_hold = new Payment();
                                            $locker_hold->hold_locker_for_next_year($booking_id);
                                            Session::forget('hold_locker_checked');
                                        }

                                        $booked_status = new Payment();
                                        $booked_status->change_booked_status($booking_id);

                                        $total_price = $retrieve_transaction_response->amount->totalAmount / 100;
                                        $price_sage_pay = new Payment();
                                        $price = $price_sage_pay->price_calculation_sagepay_response($booking_id, $total_price);
                                        $total_lockers_price = $price[0];
                                        $charity_price = $price[1];
                                        $total_price = $price[2];

                                        $payment = Payment::where('booking_id', $booking_id)->first();

                                        if (empty($payment->amount) || $payment->amount == '0.00') {

                                            $status_message = $retrieve_transaction_response->statusCode . ':' . $retrieve_transaction_response->statusDetail;
                                            $statusCode = $retrieve_transaction_response->statusCode;

                                            $payment = new Payment();
                                            $payment->update_payments($payment_id, $total_price, $user_id, $billing_address1, $billing_address2, $city, $country, $first_name, $phone_number, $post_code, $state, $last_name, $currency, $user_email);

                                            $transaction = new Payment();
                                            $transaction->update_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $total_lockers_price, $transaction_type, $statusCode, $status_message, 0, $transactionId);

                                            if ($charity_price > 0) {
                                                $waiting_response_sagepay = 0;
                                                $transaction = new Payment();
                                                $transaction->insert_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $charity_price, '03', $statusCode, $vendorTxCode, $status_message, 0, $transactionId, $waiting_response_sagepay);

                                                $create_charity = new Payment();
                                                $create_charity->create_charity($booking_id, $charity_price, $charity_id);
                                            }
                                        }

                                        $confirm_payments = new Payment();
                                        return $confirm_payments->confirmation_payments($booking_id, $total_lockers_price, $charity_price, $total_price, $ajax);

                                    } else {
                                        $sagepay_transaction = new Payment();
                                        return $sagepay_transaction->sagepay_transaction($merchantSessionKey, $cardIdentifier, $Authorization, $vendorTxCode, $total_price, $currency, $transaction_type,
                                            $first_name, $last_name, $billing_address1, $billing_address2, $city, $state, $post_code, $country, $phone_number, $booking_id, $payment_id, $user_id, $user_email, $last4Digits, $total_lockers_price, $charity_price, $request->charity_id, $ajax);

                                    }
                                } elseif (isset($retrieve_transaction_response->code)) {
//
                                    $sagepay_transaction = new Payment();
                                    return $sagepay_transaction->sagepay_transaction($merchantSessionKey, $cardIdentifier, $Authorization, $vendorTxCode, $total_price, $currency, $transaction_type,
                                        $first_name, $last_name, $billing_address1, $billing_address2, $city, $state, $post_code, $country, $phone_number, $booking_id, $payment_id, $user_id, $user_email, $last4Digits, $total_lockers_price, $charity_price, $request->charity_id, $ajax);

                                }
                            }

                        } else {

                            $error_msgs = [];
                            if (isset($card_response->errors)) {
                                $error_count = 1;
                                foreach ($card_response->errors as $error) {

                                    $status_message = $error->code . ':' . $error->clientMessage;

                                    $fraud_score = 1;
                                    $amount = null;
                                    $transactionId = null;
                                    $waiting_response_sagepay = 0;
                                    if ($error_count == 1) {
                                        $transaction = new Payment();
                                        $transaction->update_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $amount, $transaction_type, $error->code, $status_message, $fraud_score, $transactionId, $waiting_response_sagepay);
                                        $error_count++;
                                    } else {
                                        $vendorTxCode = 'lockers_' . Carbon::now()->format('Ymdhis') . '_' . $booking_id;
                                        $transaction = new Payment();
                                        $transaction->insert_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $amount, $transaction_type, $error->code, $vendorTxCode, $status_message, $fraud_score, $transactionId, $waiting_response_sagepay);

                                    }
                                    array_push($error_msgs, $error->clientMessage);
                                }
                            }

                            $data = [
                                'status' => 'errors',
                                'error_msgs' => $error_msgs
                            ];
                            return response()->json($data);

                        }

                    } else {
                        if (isset($session_response->code) || isset($session_response->description)) {
                            $status_message = $session_response->code . ':' . $session_response->description;
                            $code = $session_response->code;

                        } else {
                            $code = '';
                            $status_message = $session_response;
                        }

                        $fraud_score = 1;
                        $amount = null;
                        $transactionId = null;
                        $waiting_response_sagepay = 0;

                        $transaction = new Payment();
                        $transaction->update_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $amount, $transaction_type, $code, $status_message, $fraud_score, $transactionId, $waiting_response_sagepay);

                        if (isset($session_response->description)) {
                            $error_msgs = [];
                            array_push($error_msgs, $session_response->description);
                            $data = [
                                'status' => 'errors',
                                'error_msgs' => $error_msgs
                            ];
                            return response()->json($data);

                        } else {

                            $error_msgs = [];
                            array_push($error_msgs, $session_response);
                            $data = [
                                'status' => 'errors',
                                'error_msgs' => $error_msgs
                            ];

                            return response()->json($data);

                        }

                    }
                } else {

                    //100% discount voucher confirmation response
                    $status_message = '0000 : 100% discount voucher applied';
                    $statusCode = '0000';
                    $transactionId = null;

                    $payment = new Payment();
                    $payment->update_payments($payment_id, $total_price, $user_id, $billing_address1, $billing_address2, $city, $country, $first_name, $phone_number, $post_code, $state, $last_name, $currency, $user_email);

                    $transaction = new Payment();
                    $transaction->update_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $total_lockers_price, $transaction_type, $statusCode, $status_message, 0, $transactionId);


                    $booked_status = new Payment();
                    $booked_status->change_booked_status($booking_id);

                    $confirm_payments = new Payment();
                    return $confirm_payments->confirmation_payments($booking_id, $total_lockers_price, $charity_price, $total_price, $ajax);

                }


            } else {

                $booked_status = new Payment();
                $booked_status->change_booked_status($booking_id);

                $confirm_payments = new Payment();
                return $confirm_payments->confirmation_payments($booking_id, $total_lockers_price, $charity_price, $total_price, $ajax);

            }
        } else {
            $url = URL('/');

            $data = [
                'status' => 'redirect',
                'url' => $url
            ];
            return response()->json($data);
        }

    }

    public function payments_3D(Request $request)
    {

        if (Session::has('saleKey')) {

            if ($request->ajax()) {
                $ajax = true;
            } else {
                $ajax = false;
            }


            $Authorization = paymentConfig::integrationKey();
            $user_form = Session::get('user_payments_form_info');
            $booking_id = Session::get('on_payments_booking_id');

            $user_info = new Payment();
            $user = $user_info->user_info($booking_id);
            $user_id = $user->id;
            $user_email = $user->email;

            $first_name = $user_form['firstname'];
            $last_name = $user_form['payer_surname'];
            $billing_address1 = $user_form['billing_address1'];
            $billing_address2 = $user_form['billing_address2'];
            $city = $user_form['city'];
            $country = $user_form['country'];
            $phone_number = $user_form['phone_number'];


            $post_code = $user_form['post_code'];
            $state = $user_form['state'];

            if ($user_form['payment_type'] == 'Virtual') {
                $transaction_type = '01';
            } else {
                $transaction_type = '02';
            }

            $split_data = explode(',', $request['MD']);
            $transactionId = $split_data[0];
            $vendorTxCode = $split_data[1];
            $payment_id = $split_data[2];
            $last4Digits = $split_data[3];

            $total_lockers_price = $split_data[4];
            $charity_price = $split_data[5];
            $total_price = $split_data[6];

            $currency = new Payment();
            $currency = $currency->company_currency($booking_id);

            $threeD_data = [
                'paRes' => $request['PaRes']
            ];

            $threeD_data = json_encode($threeD_data);

            $payment = Payment::find($payment_id);

            if (empty($payment->amount || $payment->amount == '0.00')) {

                $get_transaction = new Payment();
                $retrieve_transaction_response = $get_transaction->get_transaction_from_sagepay_using_transaction_id($transactionId);
//                dd($retrieve_transaction_response);

                if (isset($retrieve_transaction_response->code)) {
                    if ($retrieve_transaction_response->code == '1012') {
                        $curl = curl_init();
                        $transaction_3D_url = \Config::get('app.3D_transaction_url');
                        $transaction_3D_url = str_replace("{transactionId}", $transactionId, $transaction_3D_url);

                        curl_setopt_array($curl, array(
                            CURLOPT_URL => $transaction_3D_url,
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_CUSTOMREQUEST => "POST",
                            CURLOPT_POSTFIELDS => $threeD_data,
                            CURLOPT_HTTPHEADER => array(
                                "Authorization: Basic $Authorization",
                                "Cache-Control: no-cache",
                                "Content-Type: application/json"
                            ),
                        ));

                        $threeD_response = curl_exec($curl);

                        $err = curl_error($curl);
                        curl_close($curl);
                        $threeD_response = json_decode($threeD_response);

                        $get_transaction = new Payment();
                        $retrieve_transaction_response = $get_transaction->get_transaction_from_sagepay_using_transaction_id($transactionId);

                        if ($retrieve_transaction_response->statusCode == '0000') {

                            if (Session::get('hold_locker_checked') == true) {

                                $locker_hold = new Payment();
                                $locker_hold->hold_locker_for_next_year($booking_id);
                                Session::forget('hold_locker_checked');
                            }


                            $booked_status = new Payment();
                            $booked_status->change_booked_status($booking_id);

                            $total_price = $retrieve_transaction_response->amount->totalAmount / 100;
                            $price_sage_pay = new Payment();
                            $price = $price_sage_pay->price_calculation_sagepay_response($booking_id, $total_price);
                            $total_lockers_price = $price[0];
                            $charity_price = $price[1];
                            $total_price = $price[2];

                            $payment = Payment::where('booking_id', $booking_id)->first();

                            if (empty($payment->amount) || $payment->amount == '0.00') {

                                $status_message = $retrieve_transaction_response->statusCode . ':' . $retrieve_transaction_response->statusDetail;
                                $statusCode = $retrieve_transaction_response->statusCode;

                                $payment = new Payment();
                                $payment->update_payments($payment_id, $total_price, $user_id, $billing_address1, $billing_address2, $city, $country, $first_name, $phone_number, $post_code, $state, $last_name, $currency, $user_email);

                                $transaction = new Payment();
                                $transaction->update_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $total_lockers_price, $transaction_type, $statusCode, $status_message, 0, $transactionId);

                                if ($charity_price > 0) {
                                    $waiting_response_sagepay = 0;
                                    $transaction = new Payment();
                                    $transaction->insert_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $charity_price, '03', $statusCode, $vendorTxCode, $status_message, 0, $transactionId, $waiting_response_sagepay);

                                    $create_charity = new Payment();
                                    $create_charity->create_charity($booking_id, $charity_price, $user_form['charity_id']);
                                }
                            }

                            $confirm_payments = new Payment();
                            return $confirm_payments->confirmation_payments($booking_id, $total_lockers_price, $charity_price, $total_price, $ajax);


                        } else {
                            $status_message = $retrieve_transaction_response->statusCode . ':' . $retrieve_transaction_response->statusDetail;

                            $fraud_score = 1;
                            $amount = null;
                            $waiting_response_sagepay = 0;
                            $transaction = new Payment();
                            $transaction->update_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $amount, $transaction_type, $retrieve_transaction_response->statusCode, $status_message, $fraud_score, $transactionId, $waiting_response_sagepay);

                            return redirect()->route('payments')->with('status', $retrieve_transaction_response->statusDetail);

                        }
                    }
                } elseif (isset($retrieve_transaction_response->statusCode)) {
                    if ($retrieve_transaction_response->statusCode == '0000') {

                        if (Session::get('hold_locker_checked') == true) {

                            $locker_hold = new Payment();
                            $locker_hold->hold_locker_for_next_year($booking_id);
                            Session::forget('hold_locker_checked');
                        }

                        $booked_status = new Payment();
                        $booked_status->change_booked_status($booking_id);

                        $total_price = $retrieve_transaction_response->amount->totalAmount / 100;
                        $price_sage_pay = new Payment();
                        $price = $price_sage_pay->price_calculation_sagepay_response($booking_id, $total_price);
                        $total_lockers_price = $price[0];
                        $charity_price = $price[1];
                        $total_price = $price[2];

                        $payment = Payment::where('booking_id', $booking_id)->first();

                        if (empty($payment->amount) || $payment->amount == '0.00') {

                            $status_message = $retrieve_transaction_response->statusCode . ':' . $retrieve_transaction_response->statusDetail;
                            $statusCode = $retrieve_transaction_response->statusCode;

                            $payment = new Payment();
                            $payment->update_payments($payment_id, $total_price, $user_id, $billing_address1, $billing_address2, $city, $country, $first_name, $phone_number, $post_code, $state, $last_name, $currency, $user_email);

                            $transaction = new Payment();
                            $transaction->update_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $total_lockers_price, $transaction_type, $statusCode, $status_message, 0, $transactionId);

                            if ($charity_price > 0) {
                                $waiting_response_sagepay = 0;
                                $transaction = new Payment();
                                $transaction->insert_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $charity_price, '03', $statusCode, $vendorTxCode, $status_message, 0, $transactionId, $waiting_response_sagepay);

                                $create_charity = new Payment();
                                $create_charity->create_charity($booking_id, $charity_price, $user_form['charity_id']);
                            }
                        }

                        $confirm_payments = new Payment();
                        return $confirm_payments->confirmation_payments($booking_id, $total_lockers_price, $charity_price, $total_price, $ajax);


                    } else {
                        $status_message = $retrieve_transaction_response->statusCode . ':' . $retrieve_transaction_response->statusDetail;

                        $fraud_score = 1;
                        $amount = null;
                        $waiting_response_sagepay = 0;
                        $transaction = new Payment();
                        $transaction->update_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $amount, $transaction_type, $retrieve_transaction_response->statusCode, $status_message, $fraud_score, $transactionId, $waiting_response_sagepay);

                        return redirect()->route('payments')->with('status', $retrieve_transaction_response->statusDetail);

                    }
                }

            } else {
                $booked_status = new Payment();
                $booked_status->change_booked_status($booking_id);

                $confirm_payments = new Payment();
                return $confirm_payments->confirmation_payments($booking_id, $total_lockers_price, $charity_price, $total_price, $ajax);

            }


        } else {
            return redirect('/');
        }
    }

    public function paymentsBack(Request $request)
    {

//        $sale_id = Session::get('sale_id');
        if (Session::has('saleKey')) {
            $booking_id = Session::get('on_payments_booking_id');

            $companyIfo = Sale::Join('lockers', 'lockers.id', '=', 'sales.locker_id')
                ->Join('blocks', 'blocks.id', '=', 'sales.block_id')
                ->Join('companies', 'companies.id', '=', 'sales.company_id')
                ->Join('users', 'users.id', '=', 'sales.user_id')
                ->select('blocks.name as block_name', 'lockers.locker_number', 'sales.id as sale_id', 'companies.id as company_id', 'companies.location_group_id', 'sales.booking_id as booking_id', 'users.id as user_id', 'users.name', 'users.surname', 'users.username', 'users.email', 'users.address_1', 'users.address_2', 'users.city as user_city', 'users.post_code as user_post_code', 'users.state as user_state', 'sales.lang', 'sales.lock_type', 'sales.price', 'sales.tax', 'companies.name as company_name', 'companies.street as company_street', 'companies.city as company_city', 'companies.country as company_country')
                ->where('sales.booking_id', $booking_id)
                ->first();

            $lockerInfo = Sale::Join('lockers', 'lockers.id', '=', 'sales.locker_id')
                ->Join('blocks', 'blocks.id', '=', 'sales.block_id')
                ->Join('companies', 'companies.id', '=', 'sales.company_id')
                ->select('blocks.name as block_name', 'lockers.locker_number', 'sales.id as sale_id', 'sales.booking_id as booking_id', 'sales.start', 'sales.end', 'sales.child_email', DB::raw("CONCAT(sales.child_first_name, ' ' ,sales.child_surname) AS childName"), 'sales.price', 'sales.tax')
                ->where('sales.booking_id', $booking_id)
                ->get();

            $total_price = 0;

            foreach ($lockerInfo as $locker) {
                $total_price += $locker->price;
                $total_price = number_format($total_price, 2, '.', '');
            }


            $expiry = Sale::where('booking_id', $booking_id)->select('booking_expiry')->first();
            //        dd($booking_id);
            $expiryTime = Carbon::parse($expiry->booking_expiry);

            $currency = Company::join('currencies', 'companies.currency', '=', 'currencies.currency_name')
                ->where('companies.id', $companyIfo->company_id)
                ->select('currencies.currency_name', 'currencies.symbol', 'currencies.position')->first();
            $charity = Charity::first();
            $placeholders = FrontendPlaceholder::where('key', 'payments-charity-card-main-disclaimer')->pluck('placeholder')->toArray();
            $charity_declaimer_placeholder = implode(",", $placeholders);
            if ($expiryTime > Carbon::now()) {

                $remainingTime = $expiryTime->diffInSeconds(Carbon::now());
                return view('frontend.pages.payments.payments', compact('booking_id', 'companyIfo', 'lockerInfo', 'currency', 'total_price', 'remainingTime', 'charity', 'charity_declaimer_placeholder'));

            } else {

                return redirect('/');
            }

        } else {
            return redirect('/');
        }

    }

    public function resetSale(Request $request)
    {

        $sessions_expire = new Payment();
        $sessions_expire->sessions_expire();
        return response()->json('success');


    }

    public function postCodeSearch(Request $request)
    {
        if (Session::has('saleKey')) {
            Session::put('postCodeMethod', 'api');
            $output = file_get_contents('http://api.getthedata.com/postcode/' . $request['post_code']);


            return response()->json($output);
        } else {
            return response()->json('home_page');
        }

    }

    public function manualPostCodeSession(Request $request)
    {
        if (Session::has('saleKey')) {
            Session::put('postCodeMethod', 'manual');
//         Session::forget('postCodeMethod');
            return response()->json('success');
        } else {
            return response()->json('home_page');
        }


    }

    public function showLockersStatus(Request $request)
    {
        if (Session::has('saleKey')) {
            $status = Session::put('show_hide_lockers', $request['lockers_status']);
//           $status = Session::get('show_hide_lockers');
//           dd($status);

            return response()->json($status);
        } else {
            return response()->json('home_page');
        }

    }

    public function checkSms()
    {
        if (Session::has('saleKey')) {
            $response = file_get_contents('https://my.fastsms.co.uk/api?Token=1k91-Lmia-zRLt-njHm&Action=CheckCredits');
            // dd(intval($response));
            //if($response == "0.00")
            if (intval($response) == 0) {
                return response()->json(false);
            } else {
                return response()->json(true);
            }
        } else {
            return response()->json('home_page');
        }
    }

}

