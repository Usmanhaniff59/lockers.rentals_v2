<?php
namespace App\Http\Controllers\Frontend;
use App\Common;
use App\Email;
use App\EmailTemplate;
use App\Http\Controllers\Controller;
use App\Language;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Sale;
use App\Locker;
use App\ManualLockerCode;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    public function index()
    {
        $lockerInfo = '';
        if(Auth::check() == true){
            $user_id = Auth::user()->id;
            $lockerInfo = Sale::Join('lockers','lockers.id', '=', 'sales.locker_id')
            ->Join('blocks','blocks.id', '=', 'sales.block_id')
            ->Join('companies','companies.id', '=', 'sales.company_id')
            ->select('blocks.name as block_name','lockers.locker_number','companies.name as company_name','sales.id as sale_id','sales.booking_id as booking_id','sales.start','sales.end','sales.hold_booking_date','sales.child_email',DB::raw("CONCAT(sales.child_first_name, ' ' ,sales.child_surname) AS childName"),'sales.price','sales.tax')
            ->where('sales.user_id', $user_id)->where('sales.status', 'r')
            ->get();
        }
        $array = array();

        if ($lockerInfo) {
            $lockerInfo = $lockerInfo->where( 'hold_booking_date', '<=', Carbon::now()->addDays(45));
            foreach ($lockerInfo as $key => $value) {
                if(array_key_exists($value->booking_id, $array)){
                    $existingarr = $array[$value->booking_id];       
                    array_push($existingarr, $value);
                    $array[$value->booking_id] = $existingarr;
                } else {
                    $new_Arr = array();
                    array_push($new_Arr, $value);
                    $array[$value->booking_id] = $new_Arr;
                }
            }
        }


        $lockerInfo = $array;    
        return view('frontend.pages.homepage',compact('lockerInfo'));
    }


    public function createUniqueManualCode($block_id) {
        $number = sprintf("%04d",rand (0,9999));
//        $number = 6882;


        // call the same function if the barcode exists already
        if ($this->barcodeNumberExists($number,$block_id)) {

            return $this->createUniqueManualCode($block_id);
        }

        // otherwise, it's valid and can be used
        return $number;
    }

    public function barcodeNumberExists($number,$block_id) {
        // query the database and return a boolean
        $exists= ManualLockerCode::where('block_id', $block_id)->where('code', $number)->exists();
        return $exists;
    }


    public function test()
    {
//        $current_date_time = Carbon::now();
//        $total_emails = Email::where('complete',0)
//            ->where('send',0)
//            ->where('date_added','<=',$current_date_time)
//            ->get();
//        dd($total_emails);
        return view('backend.emails.test');
    }
    
     public function resendEmail(Request $request){

        $sales = Sale::where('email',$request->email)->where('status', 'b')->get();
        $count = count($sales);
        $today=date('Y-m-d', strtotime(Carbon::now()));
        if($count >= 1){
            foreach ($sales as $key=>$sale){
                $start = date('Y-m-d', strtotime($sale->start));
                $end = date('Y-m-d', strtotime($sale->end));
                if (($today >= $start) && ($today <= $end)){
                    $booking =Sale::find($sale->id);
                    $booking->confirmation_sent = 0;
                    $booking->resend_request = 1;
                    $booking->save();
                } else{
                    unset($sales[$key]);
                }
            }
            if(count($sales) >= 1){
                return response()->json(true);
            } else {
                return response()->json(false);
            }
        } else {
            return response()->json(false);
        }
    }

    public function userExist(Request $request)
    {
        $user = User::where('email',$request['email'])->first();

        if($user){
            $status = true;
        }else{
            $status = false;
        }
        return response()->json($status);
    }

    public function loginAjax(Request $request)
    {

        $email	       = $request->email;
        $password      = $request->password;
        $rememberToken = $request->remember;

        // now we use the Auth to Authenticate the users Credentials

        // Attempt Login for members
//        if (Auth::guard('member')->attempt(['email' => $email, 'password' => $password], $rememberToken)) {
//            $msg = array(
//                'auth' => true,
//////            'auth' => auth()->check(),
//            'token' => csrf_token(),
//            'user' => Auth::user(),
//            );
//            return response()->json($msg);
//        } else {
//            $msg = array(
//                'status'  => 'error',
//                'message' => 'Login Fail !'
//            );
//            return response()->json($msg);
//        }

//        $user = User::where('email',$request->email)->first();
//        Auth::login($user,true);
//        $auth =false;
//        $credentials = $request->only('email', 'password');
//
//        if (Auth::attempt($credentials, $request->has('remember'))) {
//            $auth = true; // Success
//        }
//        dd($auth);
//        return response()->json([
//            'auth' => true,
////            'auth' => auth()->check(),
//            'token' => csrf_token(),
//            'user' => Auth::user(),
//
//        ]);
//        return response()->json('success');
    }

    public function logout(Request $request)
    {

        Auth::logout();


        return response()->json('success');
    }
    public function sendresetemail(Request $request)
    {
      $emailTemplate=EmailTemplate::where('template_name','Reset Password')->first();
      $user = User::where('email', $request->email)->first();
      // dd($emailTemplate->template_text);
      if($user)
      {
        $token = app('auth.password.broker')->createToken($user);
        // dd($token,$user,$emailTemplate,$request->all());
        $date=date('Y-m-d');

        $url=\URL::to('/').'/password/reset/'.$token.'?email='.$request->email;
        $exp = explode('abcdef',$emailTemplate->template_text);
        // dd($url);
        $urln=$exp[0].$url.$exp[1];
        // dd($urln);
        // dd($url,);
        Email::create([
              'email' => $request->email,
              'star' => '0',
              'result_code' => '0',
              'send' => '0',
              'send' => '0',
              'action'=> 'sent',
              'complete' => '0',
              'deleted' => '0',
              'fail' => '0',
              'email_folder_id' => '0',
              'subject' => 'Forget password',
              'mail_body' => $urln,
              'date_added' => $date
          ]);
      }
      return response()->json(['message'=>'Success','status'=>true]);

      // dd($request->all());
    }
    public function sendresetsms(Request $request)
    {
//        dd($request->all());
       $user =  User::where('phone_number',$request->phone_number)->first();
//       $user =  User::where('id',1)->first();
       if($user) {
           $request_phone = $request->phone_number[0];
           if ($request_phone == "0") {
               $phone_number = "+44" . substr($request->phone_number, 1);

           } else if ($request_phone == "+") {
               $phone_number = $request->phone_number;
           } else {
               $phone_number = "+44" . $request->phone_number;
           }
//        dd($phone_number);
           $code = sprintf("%04d", rand(0, 9999));

           $user->reset_code = $code;
           $user->save();


           $sms_body = 'Please enter the code %code% to reset your passcode and view the portal';
           $sms_body = str_replace('%code%', $code, $sms_body);
//        dd($sms_body);
           $response = Common::sendSms($phone_number, $sms_body);
           $data = [
               'user_id' => $user->id
           ];
           return response()->json($data);
       }else{
           return response()->json('user_not_found');
       }
//        dd($response);

    }

    public function validatepasscode(Request $request)
    {
//     dd('dd');
       // dd($request->all());
       $user =  User::where('id',$request->reset_user_id)->first();
       if($user) {
           if($user->reset_code == $request->reset_code){
               $token = app('auth.password.broker')->createToken($user);
               $url = url('password/reset', $token);
//               dd($url);
               Session::put('reset_password_using_phone_number',true);
               Session::put('reset_password_email', $user->email);
               Session::put('reset_password_number', $user->phone_number);
//               dd('hh');
//               dd(Session::get('reset_password_using_phone_number'));
               $data = [
                   'status' => 'matched',
                   'url' => $url
               ];
           }else{
               $data = [
                   'status' => 'not_matched'
               ];
           }
           return response()->json($data);
       }
    }
    public function support()
    {
        return view('frontend.pages.generate_ticket');
    }
}