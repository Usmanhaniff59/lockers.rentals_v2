<?php

namespace App\Http\Controllers;
use App\Email;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
class ForgetPasswordController extends Controller
{
public function forgot() {
        $credentials = request()->validate(['email' => 'required|email']);
        Password::sendResetLink($credentials);
        return response()->json(["msg" => 'Reset password link sent on your email id.']);
}
public function reset() {
        $credentials = request()->validate([
            'email' => 'required|email',
            'token' => 'required|string',
            'password' => 'required|string|confirmed'
        ]);
        $reset_password_status = Password::reset($credentials, function ($user, $password) {
            $user->password = $password;
            $user->save();
        });
        if ($reset_password_status == Password::INVALID_TOKEN) {
            return response()->json(["msg" => "Invalid token provided"], 400);
        }
        return response()->json(["msg" => "Password has been successfully changed"]);
    }
    // public function view(){

    // return view('auth.passwords.resetPasswordForm'); 
}
	// public function view(Request $request){
	// 	$user=User::where('email',$request->email)->update([
	// 		'email' => $request->email;
	// 		'password' => $request->password_confirmation;
	// 	]);
	// 	}
 //    	return response()->json(["user" => $user]);


}
