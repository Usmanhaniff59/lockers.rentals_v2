<?php

namespace App\Http\Controllers;

use App\RequestAvailability;
use Illuminate\Http\Request;

class RequestAvailabilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function maintenance()
    {
        return view('maintenance');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RequestAvailability  $requestAvailability
     * @return \Illuminate\Http\Response
     */
    public function show(RequestAvailability $requestAvailability)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RequestAvailability  $requestAvailability
     * @return \Illuminate\Http\Response
     */
    public function edit(RequestAvailability $requestAvailability)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RequestAvailability  $requestAvailability
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RequestAvailability $requestAvailability)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RequestAvailability  $requestAvailability
     * @return \Illuminate\Http\Response
     */
    public function destroy(RequestAvailability $requestAvailability)
    {
        //
    }
}
