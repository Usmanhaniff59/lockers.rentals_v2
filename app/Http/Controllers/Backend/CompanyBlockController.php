<?php

namespace App\Http\Controllers\Backend;

use App\Block;
use App\BlockYears;
use App\Company;
use App\CronSchedule;
use App\Locker;
use App\SchoolYear;
use App\Golive;


use App\ManualLockerCode;
use App\SchoolAcademicYear;
use App\LockerBlockImage;
use App\Traits\UniqueManualCode;
use App\Traits\CronScheduleTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;

class CompanyBlockController extends Controller
{
    use UniqueManualCode, CronScheduleTrait;

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }
    
    public function findTier(Request $request)
    {
         $tier = LockerBlockImage::where('image_type', $request->block_type)->whereNotNull('tier')->orderBy('tier', 'asc')->get();
          return response()->json($tier);  
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    //block create
    public function store(Request $request)
    {
        //check block images against tier and assign image if found
        $tier = $request->block_height;
        $image = LockerBlockImage::where('image_type', $request->block_type)->where('tier', $request->block_height)->first();

        if (!$image) {
            return response()->json('image_validation');
        }

      
           if($request->endDate)
        {
           
            $endDate=date('Y-m-d H:i:s', strtotime($request->endDate));
        }else{
            $endDate=Null;
        }
       if($request->start_date)
        {
            $start_date =date('Y-m-d H:i:s', strtotime($request->start_date));
            // $start_date=Carbon::parse($request->start_date)->format('Y-m-d 00:00:00');
        }else{
            $start_date=Null;
        }
        
      $data=new Block();
      $data->company_id=$request->company_id;
      $data->lock_type=$request->lock_type;
      $data->order_list=$request->order_list;
      $data->name=$request->name;
      $data->location_group_id=$request->location_group_id;
      $data->block_height=$request->block_height;
      $data->locker_blocks=$request->locker_blocks;
      $data->rate_id=$request->rate_id;
      $data->display=$request->display;
      $data->active=$request->active;

      $data->block_type=$request->block_type;
      $data->locker_block_image_id= $image->id;
      $data->block_group=$request->block_group;
      $data->color_locker=$request->color_locker;
      $data->start_date=$start_date;
      $data->end_date=$endDate;
      $data->save();


        // $data = Block::create([
        //     'company_id' => $request->company_id,
        //     'lock_type' => $request->lock_type,
        //     'order_list' => $request->order_list,
        //     'name' => $request->name,
        //     'location_group_id' => $request->location_group_id,
        //     'block_height' => $request->block_height,
        //     'locker_blocks' => $request->locker_blocks,
        //     'rate_id' => $request->rate_id,
        //     'display' => $request->display,
        //     'active' => $request->active,
        //     'block_type' => $request->block_type,
        //     'locker_block_image_id' => $image->id,
        //     'block_group' => $request->block_group,
        //     'color_locker' => $request->color_locker,
        //      'start_date' => $start_date,
        //     'end_date' => $endDate,
            
        // ]);
        // assign all Manuall locker codes 

        //assign shool year to created block
        if (isset($request->school_academic_year_id)) {
            foreach ($request->school_academic_year_id as $key => $value) {
                BlockYears::insert([
                    'company_id' => $request->company_id,
                    'block_id' => $data->id,
                    'school_academic_year_id' => $value
                ]);
            }
        }


        //create new lockers against block tier * locker blocks
        $block_height = intval($request->block_height);
        $locker_blocks = intval($request->locker_blocks);

        $lockers_to_add = $block_height * $locker_blocks + 1;

        for ($i = 1; $i < $lockers_to_add; $i++) {

            $last_locker_inserted = Locker::create([
                'block_id' => $data->id,
                'company_id' => $request->company_id,
                'locker_number' => $i,
                'locker_order' => $i,
                'active' => 1,
                'display' => 1
            ]);
        }

        //add cron schedule for create new lockers code
        $class = 'ManualLockerCode';
        $controller = 'ManualLockerCode';
        // Artisan::call('dumyManualLocker:cron');
        $this->addCronSchedule($class, $controller);

        return response()->json($data);
    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

   //update the blocks
    public function update(Request $request, $id)
    {
        //check block images against tier and assign image if found
        $tier = $request->block_height;
        $image = LockerBlockImage::where('image_type', $request->block_type)->where('tier', $request->block_height)->first();

        if (!$image) {
            return response()->json('image_validation');
        }
        if($request->endDate)
        {
           
            $endDate=date('Y-m-d H:i:s', strtotime($request->endDate));
        }else{
            $endDate=Null;
        }
       if($request->start_date)
        {
            $start_date =date('Y-m-d H:i:s', strtotime($request->start_date));
            // $start_date=Carbon::parse($request->start_date)->format('Y-m-d 00:00:00');
        }else{
            $start_date=Null;
        }
        $checkBlock=Block::where('id', $id)->first();
        //update the block
        $data = Block::where('id', $id)->update([
            'lock_type' => $request->lock_type,
            'block_group' => $request->block_group,
            'order_list' => $request->order_list,
            'name' => $request->name,
            'block_height' => $request->block_height,
            'locker_blocks' => $request->locker_blocks,
            'rate_id' => $request->rate_id,
            'display' => $request->display,
            'active' => $request->active,
            'block_type' => $request->block_type,
            'locker_block_image_id' => $image->id,
            'color_locker' => $request->color_locker,
            'start_date' => $start_date,
            'end_date' => $endDate,
        ]);
        $manualLockers=ManualLockerCode::where('block_id',$id)->update([
            'block_group_id'=>$request->block_group,
        ]);
        //update the block years
        $company_id = Session::get('company_id_blocksss');
        BlockYears::where('block_id', $id)->where('company_id', $company_id)->delete();
        if (isset($request->choose_school_year)) {
            foreach ($request->choose_school_year as $key => $value) {
                BlockYears::insert([
                    'company_id' => $request->company_id,
                    'block_id' => $id,
                    'school_academic_year_id' => $value
                ]);

            }
        }
        //update the lockers against tier * locker blocks
        $block_height = intval($request->block_height);
        $locker_blocks = intval($request->locker_blocks);
        $locker_to_update = $block_height * $locker_blocks;

        $lockers = Locker::where('block_id', $id)->where('active', 1)->get();
        $locker_count = count($lockers);
        //if not update the tier * locker blocks
         if ($locker_count == $locker_to_update) {
            return response()->json($data);
        }
        //if existing locker is greater then the updated lockers..need to remvoe extra lockers
        else if ($locker_count > $locker_to_update) {
             $locker_to_reduce = $locker_count - $locker_to_update;
             $locker_to_skip = $locker_count - $locker_to_reduce;

            $inactive_lockers = Locker::where('block_id', $id)->where('active', 1)->orderBy('locker_order', 'desc')->take($locker_to_reduce)->get();
            foreach ($inactive_lockers as $locker) {
                $lockerupdate = Locker::where('id', $locker->id)->first();
                $lockerupdate->active = 0;
                $lockerupdate->display = 1;
                $lockerupdate->save();
            }
        }
        //if existing locker is lesser then the updated lockers..need to add extra lockers
        else if ($locker_count < $locker_to_update) {


            $latest = Locker::where('block_id', $id)->orderBy('locker_order', 'DESC')->where('active', 1)->first();
            $latest_order = Locker::where('block_id', $id)->orderBy('locker_order', 'DESC')->first();
            if (isset($latest)) {
                $latest_number = $latest->locker_number + 1;
            } else {
                $latest_number = 1;
            }
            $latest_order = Locker::where('block_id', $id)->orderBy('locker_order', 'DESC')->first();
            if (isset($latest_order)) {
                $latest_order_number = $latest_order->locker_order + 1;
            } else {
                $latest_order_number = 1;
            }

            $latestactive = Locker::where('block_id', $id)->where('active', 1)->orderBy('locker_number', 'DESC')->first();
            $locker_gap = $locker_to_update - $locker_count;
            for ($i = 0; $i < $locker_gap; $i++) {
                $lockerdeleted = Locker::where('block_id', $id)->where('active', 0)->where('locker_number', $latest_number)->first();
                if ($lockerdeleted) {

                    $lockerdeleted->locker_order = $latest_order_number;
                    $lockerdeleted->active = 1;
                    $lockerdeleted->display = 1;
                    $lockerdeleted->save();
                } else {

                    Locker::insert([
                        'block_id' => $id,
                        'company_id' => $company_id,
                        'locker_number' => $latest_number,
                        'locker_order' => $latest_order_number,
                        'active' => 1,
                        'display' => 1
                    ]);
                }

                $latest_number = $latest_number + 1;
                $latest_order_number = $latest_order_number + 1;
            }

        }

        //add cron schedule for manual locker code to update the codes
        $class = 'ManualLockerCode';
        $controller = 'ManualLockerCode';
        // Artisan::call('dumyManualLocker:cron');
         // $this->addCronSchedule($class, $controller);
        // for edit below code is done need update from Richard
        // if($checkBlock->block_group!=$request->block_group)
        // {
        //     $years = SchoolYear::where('status',0)->groupBy('from_date')->get();
        //     foreach ($years as $key => $year) {
        //        $blockgroupcodes=BlockGroupCode::where([['block_group_id',$request->block_group],['start_date',$year->from_date]])->get();
        //        // loop start
        //         $lockers=Locker::where([['active',1],['display',1],['block_id',$checkBlock->id]])->whereNotExists(function ($Inner_query) use ($year) {
        //             $query=$Inner_query->select(DB::raw(1))
        //                 ->from('manual_locker_codes')
        //                 ->whereRaw('lockers.id = manual_locker_codes.locker_id')
        //                     ->where('manual_locker_codes.start',$year->from_date);
        //                 })->get();
        //        $checkLocker = new ManualLockerCode();
        //        $d=$checkLocker->LockerBlockcodesEdit($blockgroupcodes,$lockers,$request->start_date,$request->end_date,$request->display,$checkBlock->id);
        //        // loop end
        //     }
        // }
       

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company_block = Block::find($id);
        if ($company_block) {

            $company_block->update([
                'active' => 0,
                'display' => 0
            ]);
            return response()->json(['success' => 'Company successfully deleted']);
        }
        return response()->json(['error' => 'Something went wrong! Company not found.']);
    }

    //get blocks against specific company
    public function getUpdated()
    {
        //get company id from session
        $company_idd = Session::get('company_id_blocksss');

        $company_blocks = Block::where('company_id', $company_idd)->where('active', 1)->orderBy('order_list', 'ASC')->get();

        $block_html = '<option value="">Select Block</option>';

        foreach ($company_blocks as $block) {
            $block_html .= '<option value="' . $block->id . '">' . $block->name . '</option>';
        }

        return response()->json($block_html);

    }

    //tables using datatable
    public function show_all()
    {

        $company_idd = Session::get('company_id_chats');
         $company_blocks = Block::where('company_id', $company_idd)->where('active', 1)->orderBy('order_list', 'ASC')->get();

        return Datatables::of($company_blocks)
            ->addColumn('lock_type', function ($company_blocks) {
                if ($company_blocks->lock_type == 'm')
                    return 'Manual';
                elseif ($company_blocks->lock_type == 'a')
                    return 'Automatic';
                elseif ($company_blocks->lock_type == 's')
                    return 'Static';
                else
                    return '';
            })

            ->addColumn('action', function ($company_blocks) {
                $buttons = '';
                $status = $company_blocks->lockers()->where('updated', '!=', 1)->exists();
                if ($status) {
                    // $buttons = '<a href="#edit_block" class="btn btn-primary btn-md adv_cust_mod_btn pull-left edit-block" data-toggle="modal" onclick="saveid('.$blocks->id.')" data-id="'. $blocks->id .'"  style="margin-right: 3px;">Edit</a>';
                    // $buttons.='<a href = "JavaScript:Void(0);" onclick = "deleteData(\''.route('company.blocks.destroy', $company_blocks->id).'\',\''.csrf_token().'\',\'\',company_blocks_table,\'DELETE\')" class="btn btn-danger" > Delete</a >';
                } else {
                    $buttons = '<a href="javascript:void(0)" class="btn btn-primary btn-md adv_cust_mod_btn pull-left edit-block" data-toggle="modal" onclick="saveid(' . $company_blocks->id . ')" data-id="' . $company_blocks->id . '"  style="margin-right: 3px;">Edit</a>';
                    $buttons .= '<a onclick = "deleteBlock(' . $company_blocks->id . ')" class="btn btn-danger" > Delete</a >';
                }
                return $buttons;
            })
            ->setRowClass(function ($company_blocks) {
                // dd($company->active);
                $active = $company_blocks->active;
                $display = $company_blocks->display;
                //dd($active, $display);
                if ($display == 1) {
                    return 'alert-success';
                } else if ($active == 0) {
                    return 'alert-danger';
                } else if ($display == 0) {
                    return 'alert-warning';
                }
            })

            ->make(true);
    }

    //show deleted blocks in table using datatable
    public function show_deleted()
    {
        $company_idd = Session::get('company_id_blocksss');

        $company_blocks = Block::where('company_id', $company_idd)->where('active', 0)->get();

        return Datatables::of($company_blocks)
            ->addColumn('lock_type', function ($company_blocks) {
                if ($company_blocks->lock_type == 'm')
                    return 'Manual';
                elseif ($company_blocks->lock_type == 'a')
                    return 'Automatic';
                elseif ($company_blocks->lock_type == 's')
                    return 'Static';
                else
                    return '';
            })
            ->addColumn('active', function ($company_blocks) {
                if ($company_blocks->active == '1')
                    return 'Yes';
                else
                    return 'No';


            })
            ->addColumn('display', function ($company_blocks) {
                if ($company_blocks->active == '1')
                    return 'Yes';
                else
                    return 'No';


            })
            ->addColumn('action', function ($company_blocks) {
                $buttons = '';
                $status = $company_blocks->lockers()->where('updated', '!=', 1)->exists();
                if ($status) {
                    // $buttons = '<a href="#edit_block" class="btn btn-primary btn-md adv_cust_mod_btn pull-left edit-block" data-toggle="modal" onclick="saveid('.$blocks->id.')" data-id="'. $blocks->id .'"  style="margin-right: 3px;">Edit</a>';
                    // $buttons.='<a href = "JavaScript:Void(0);" onclick = "deleteData(\''.route('company.blocks.destroy', $company_blocks->id).'\',\''.csrf_token().'\',\'\',company_blocks_table,\'DELETE\')" class="btn btn-danger" > Delete</a >';
                } else {
                    $buttons = '<a href="#edit_block" class="btn btn-primary btn-md adv_cust_mod_btn pull-left edit-block" data-toggle="modal" onclick="saveid(' . $company_blocks->id . ')" data-id="' . $company_blocks->id . '"  style="margin-right: 3px;">Edit</a>';
                    $buttons .= '<a href = "JavaScript:Void(0);" onclick = "deleteData(\'' . route('company.blocks.destroy', $company_blocks->id) . '\',\'' . csrf_token() . '\',\'\',company_blocks_table,\'DELETE\')" class="btn btn-info" > Delete</a >';
                }


                return $buttons;
            })
            ->setRowClass(function ($company_blocks) {
                // dd($company->active);
                $active = $company_blocks->active;
                $display = $company_blocks->display;
                //dd($active, $display);
                if ($active == 0) {
                    return 'alert-danger';
                }
            })
            ->make(true);
    }

    //get school years
    public function getSchoolYear()
    {
        $data = SchoolAcademicYear::orderBy('order_number', 'asc')->get();
        return response()->json($data);
    }

    //all images of blocks
    public function getBlockImages()
    {
        $data = LockerBlockImage::get();
        return response()->json($data);
    }
    public function updateSchoolYears(Request $request)
    {
        $id=$request->id;
        $date=$request->date;
        $start_date =date('Y-m-d H:i:s', strtotime($date));
     $schoolyear=SchoolYear::where('id',$id)->first();
     $schoolyear->go_live=$start_date;
     $schoolyear->status=0;
     $next_year=date('Y', strtotime($schoolyear->to_date));
     $schoolyear->save();
     $companies = Company::where('active',1)->where('display',1)->get();
     foreach ($companies as $key => $value) {
        $golive=Golive::where('company_id',$value->id)->orderBy('id','desc')->first();
        if($golive){
            $golive->active=0;
            $golive->save();
        }
        // $data= new Golive;
        // $data->company_id=$value->id;
        // $data->school_year=$id;
        // $data->liveDate=$start_date;
        // $data->active=1;
        // $data->save();
     }
     $next=$next_year+1;
         $findSchoolyear=SchoolYear::where('title','01 Sep, '.$next_year.' - 31 Aug, '.$next)->first();
         if(!isset($findSchoolyear) && !empty($findSchoolyear)){
       $data = SchoolYear::insert([
                'title' => '01 Sep, '.$next_year.' - 31 Aug, '.$next,
                'status' => 0,
                'from_date' =>  date($next_year.'-09-01 00:00:00'),
                'to_date' =>   date($next.'-08-31  23:59:59'),
            ]);
   }
    //add cron schedule for create new manaul for new years lockers code
        $class = 'ManualLockerCode';
        $controller = 'ManualLockerCode';
        // Artisan::call('dumyManualLocker:cron');
        $this->addCronSchedule($class, $controller);
     return 1;
    }
    //block details & block years of specific block
    public function getBlockDetails($id)
    {
        $company_id = Session::get('company_id_blocksss');
        $data = Block::where('id', $id)->first();
        $years = BlockYears::where('block_id', $id)->where('company_id', $company_id)->get();
           
         return response()->json(['data' => $data, 'years' => $years]);
    }

}