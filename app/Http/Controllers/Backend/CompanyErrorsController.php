<?php

namespace App\Http\Controllers\Backend;

use App\CompanyBlockError;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class CompanyErrorsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.pages.companyErrors.index');

    }
    public function show_all(){


        $errors = CompanyBlockError::query();

        $errors->where('resolved',0);
        if(auth()->user()->can('All Records')) {

        }elseif( Auth::user()->company_id && Auth::user()->franchisee_id) {
            $franchisee_companies = FranchiseeCompany::where('franchisee_id',Auth::user()->franchisee_id)->pluck('company_id')->toArray();
            $errors->Where(function ($query) use ($franchisee_companies) {
                $query->WhereIn('company_id',$franchisee_companies)
                    ->orWhere('company_id',Auth::user()->company_id);
            });
        }
        elseif( Auth::user()->franchisee_id ) {
            $franchisee_companies = FranchiseeCompany::where('franchisee_id',Auth::user()->franchisee_id)->pluck('company_id')->toArray();

            $errors->whereIn('company_id',$franchisee_companies);
        }
        elseif( Auth::user()->company_id ){

            $errors->where('company_id',Auth::user()->company_id);
        }else{
            $errors->where('id',0);
        }



        return Datatables::of($errors)


            ->addColumn('company', function ($errors) {
                if($errors->company)
                return $errors->company->name;

            })
            ->addColumn('block', function ($errors) {
                if($errors->block)
                return $errors->block->name;

            })
            ->addColumn('action', function ($errors) {
                $buttons ='';
                if(auth()->user()->can('Move Booking')) {
                    $details = $errors->details;
                    preg_match_all('!\d+!', $details, $matches);
                    $booking_id = $matches[0][0];

                    $buttons .= '<a href="#move_booking" class="btn btn-info pull-left button-style view-user" data-toggle="modal" style="margin-right: 3px;" data-href="#move_booking" onclick="moveBooking(\''.$booking_id.'\')">Move Booking</a>';
                }
//                $buttons .= '<form method="POST" action="'.url('admin/payment/edit').'"><input type="hidden" name="id" value="'.$errors->id.'"/> '.csrf_field().' <input type="submit" value="Resolved" class="btn btn-info pull-left button-style"  onclick="row_highlight(\'booking' . $errors->id . '\')"/></form>';

                $buttons .= '<a href="#." class="btn btn-info pull-left button-style view-user"   style="margin-right: 3px;" onclick="resolveError(\''.$errors->id.'\')">Resolved</a>';

//                if(auth()->user()->can('Delete Company')) {
//                    $buttons.='<a href = "JavaScript:Void(0);" onclick = "deleteData(\''.route('company.destroy', $errors->id).'\',\''.csrf_token().'\',\'\',company_table,\'DELETE\')" class="btn btn-danger" > Delete</a >';
//                }
                return $buttons;
            })
//            ->setRowClass(function ($errors) {
//                // dd($errors->active);
////                $active = $errors->active;
////                $display = $errors->display;
////                //dd($active, $display);
////                if($active == 1 && $display == 1){
////                    return 'alert-success';
////                } else if($active == 0 && $display == 0){
////                    return 'alert-danger';
////                } else if($active == 1 && $display == 0){
////                    return 'alert-warning';
////                }
//            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function resolved(Request $request)
    {
//        dd($request->all());
        $error =CompanyBlockError::find($request->id);
        $error->resolved = 1;
        $error->save();

        return response()->json('success');
    }
}
