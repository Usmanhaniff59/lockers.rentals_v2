<?php

namespace App\Http\Controllers\Backend;

use App\Block;
use App\FranchiseeCompany;
use App\Locker;
use App\LockerBlockImage;
use App\Rate;
use App\SchoolAcademicYear;
use App\SchoolYear;
use App\BlockYears;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;
use App\BlockGroup;
use App\LockerColor;
use Illuminate\Support\Facades\DB;

class OuterBlockController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rates = Rate::orderBy('order', 'ASC')->get();
        $school_years = SchoolAcademicYear::all();
        $locker_block_images = LockerBlockImage::all();
        $BlockGroup = BlockGroup::where('status',1)->where('delete',0)->orderBy('order_by', 'asc')->get();
         $LockerColor = LockerColor::where('active', 1)->where('delete', 0)->orderBy('color', 'asc')->get();
        return view('backend.pages.company.outerBlocks.index', compact('rates', 'school_years', 'locker_block_images','BlockGroup','LockerColor'));

    }

    public function show_all(Request $request)
    {


//       $role = DB::table('user_has_roles')->where('user_id',Auth::user()->id)->select('role_id')->first();


        $blocks = Block::query();
        $blocks->with('company', 'rate')->whereHas('company', function ($query) {
              // if(auth()->user()->hasRole('Franchisee'))
              //   {
              //       $franchisee_ids=DB::table('franchisee_user')->where('user_id',auth()->user()->id)->pluck('franchisee_id');
              //       $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id', $franchisee_ids)->pluck('company_id')->toArray();
              //       $query->whereIn('id', $franchisee_companies);
              //        $query->orWhere('id',auth()->user()->company_id);

              //   }else if(auth()->user()->hasRole('Company'))
              //   {
              //        $query->where('id',auth()->user()->company_id);
              //   }
                
            $query->where('active', '=', 1);

        })->select('blocks.id', 'blocks.company_id', 'blocks.lock_type', 'blocks.order_list', 'blocks.location_group_id',
            'blocks.rate_id', 'blocks.block_height', 'blocks.locker_blocks', 'blocks.name', 'blocks.street', 'blocks.city', 'blocks.country', 'blocks.post_code', 'blocks.locker_block_image_id',
            'blocks.active', 'blocks.display', 'blocks.resend_instruction', 'blocks.school_academic_year_id', 'blocks.created_at', 'blocks.updated_at');


        $blocks_obj = new Block();
        $blocks = $blocks_obj->outer_blocks_record_permissions($blocks);


        $blocks->whereNotNull('blocks.company_id');
        $blocks->where('blocks.active', 1);
       


//        $blocks->orderBy('blocks.company_id','ASC');
        return Datatables::of($blocks)
            ->addColumn('company_name', function ($blocks) {
                if ($blocks->company)
                    return $blocks->company->name;

            })
            ->addColumn('rateunit', function ($blocks) {
                if (isset($blocks->rate->unit)) {
                    return $blocks->rate->unit;
                } else {
                    return "";
                }

            })
            ->addColumn('locker_available', function ($blocks) {
                if ($blocks->company)
                    return $blocks->lockers()->where('lockers.active', 1)->where('lockers.display', 1)->count();

            })
            ->addColumn('numbering', function ($blocks) {
                if ($blocks->lockers()) {

                    $min = $blocks->lockers()->where('lockers.active', 1)->where('lockers.display', 1)->min('locker_order');

                    if ($min)
                        $min = $blocks->lockers()->where('lockers.locker_order', $min)->select('locker_number')->first();


                    $max = $blocks->lockers()->where('lockers.active', 1)->where('lockers.display', 1)->max('locker_order');

                    if ($max)
                        $max = $blocks->lockers()->where('lockers.locker_order', $max)->select('locker_number')->first();

                    $min_number = '';
                    $max_number = '';

                    if ($min)
                        $min_number = $min->locker_number;

                    if ($max)
                        $max_number = $max->locker_number;

                    if ($min_number || $max_number)
                        return $min_number . ' - ' . $max_number;
                }
                return 1;
            })
            ->addColumn('action', function ($blocks) {
//                $buttons ='';
//                if(auth()->user()->can('Edit y')) {
//
                $buttons = '<a href="#edit" class="btn btn-primary btn-md adv_cust_mod_btn edit-btn pull-left"  data-toggle="modal" style="margin-right: 3px;" data-href="#edit" id="outerblockedit" style="margin-right: 3px;" data-id="' . $blocks->id . '"  data-companyid="' . $blocks->company_id . '">Edit</a>';
//                }
//                if(auth()->user()->can('Delete Company')) {
                $buttons .= '<a href = "JavaScript:Void(0);" onclick = "deleteData(\'' . route('company.outer.blocks.destroy', $blocks->id) . '\',\'' . csrf_token() . '\',\'\',outer_blocks_table,\'DELETE\')" class="btn btn-danger" > Delete</a >';
//                }
//
                return $buttons;
            })->setRowClass(function ($blocks) {

                $active = $blocks->active;
                $display = $blocks->display;

                if ($active == 1 && $display == 1) {
                    return 'alert-success';
                } else if ($active == 0 && $display == 0) {
                    return 'alert-danger';
                } else if ($active == 1 && $display == 0) {
                    return 'alert-warning';
                }
            })->make(true);
    }

    public function show_deleted()
    {


        $blocks = Block::query();
        $blocks->with('company');


        if (auth()->user()->can('All Records')) {

        } elseif (Auth::user()->company_id && Auth::user()->franchisee_id) {
            $franchisee_companies = FranchiseeCompany::where('franchisee_id', Auth::user()->franchisee_id)->pluck('company_id')->toArray();
            $blocks->Where(function ($query) use ($franchisee_companies) {
                $query->WhereIn('company_id', $franchisee_companies)
                    ->orWhere('company_id', Auth::user()->company_id);
            });

        } elseif (Auth::user()->franchisee_id) {
            $franchisee_companies = FranchiseeCompany::where('franchisee_id', Auth::user()->franchisee_id)->pluck('company_id')->toArray();

            $blocks->whereIn('company_id', $franchisee_companies);
        } elseif (Auth::user()->company_id) {

            $blocks->where('company_id', Auth::user()->company_id);
        } else {
            $blocks->where('id', 0);
        }


        $blocks->whereNotNull('company_id');
        $blocks->where('active', 0);


        return Datatables::of($blocks)
            ->addColumn('company', function ($blocks) {
                if ($blocks->company)
                    return $blocks->company->name;
//               return 1;
            })
            ->addColumn('locker_available', function ($blocks) {
                if ($blocks->company)
                    return $blocks->lockers()->where('lockers.active', 1)->where('lockers.display', 1)->count();
//                return  1;
            })
            ->addColumn('Price', function ($blocks) {
                if (isset($blocks->rate)) {
                    if (isset($blocks->rate->unit)) {
                        return $blocks->rate->unit;
                    } else {
                        return "";
                    }
                } else {
                    return "";
                }
//                return  1;
            })
            ->addColumn('tier', function ($blocks) {
                return $blocks->block_height;

            })
            ->addColumn('locker_blocks', function ($blocks) {
                return $blocks->locker_blocks;
//                return  1;
            })
            ->addColumn('numbering', function ($blocks) {
                if ($blocks->lockers()) {
                    $min = $blocks->lockers()->where('lockers.active', 1)->where('lockers.display', 1)->min('locker_order');
                    $max = $blocks->lockers()->where('lockers.active', 1)->where('lockers.display', 1)->max('locker_order');

                    if ($min || $max)
                        return $min . ' - ' . $max;
                }
//                return  1;
            })
            ->addColumn('action', function ($blocks) {
//                $buttons ='';
//                if(auth()->user()->can('Edit y')) {
//
                $buttons = '<a href="#edit" class="btn btn-primary btn-md adv_cust_mod_btn edit-btn pull-left"  data-toggle="modal" style="margin-right: 3px;" data-href="#edit" id="outerblockedit" style="margin-right: 3px;" data-id="' . $blocks->id . '"  >Edit</a>';
//                }
//                if(auth()->user()->can('Delete y')) {
//                    $buttons.='<a href = "JavaScript:Void(0);" onclick = "deleteData(\''.route('y.destroy', $y->id).'\',\''.csrf_token().'\',\'\',y_table,\'DELETE\')" class="btn btn-danger" > Delete</a >';
//                }
                return $buttons;
            })->setRowClass(function ($blocks) {
                // dd($company->active);
                $active = $blocks->active;
                $display = $blocks->display;
                //dd($active, $display);
                if ($active == 1 && $display == 1) {
                    return 'alert-success';
                } else if ($active == 0 && $display == 0) {
                    return 'alert-danger';
                } else if ($active == 1 && $display == 0) {
                    return 'alert-warning';
                }
            })->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//        dd($id);
        $block = Block::findOrFail($id);
        if ($block) {
            $block->active = 0;
            $block->display = 0;
            $block->save();
            return response()->json(['success' => 'Company successfully deleted']);

        } else {
            return response()->json(['error' => 'Company Not deleted!']);
        }
    }


    public function show_lockers($id)
    {

    }

    public function getBlockDetails(Request $request)
    {
        Session::put('company_id_blocksss', $request->company_id);
        $data = Block::where('id', $request->block_id)->first();
        $years = BlockYears::where('block_id', $request->block_id)->where('company_id', $request->company_id)->get();
        //dd($years);
        return response()->json(['data' => $data, 'years' => $years]);
    }

    public function update(Request $request, $id)
    {
        //dd($id, $request->all());
        $data = Block::where('id', $id)->update([
            'lock_type' => $request->lock_type,
            'order_list' => $request->order_list,
            'name' => $request->name,
            'block_height' => $request->block_height,
            'locker_blocks' => $request->locker_blocks,
            'rate_id' => $request->rate_id,
            'display' => $request->display,
            'active' => $request->active,
            'locker_block_image_id' => $request->block_images_edit,
            'color_locker'=>$request->color_locker,
            'block_group'=>$request->block_group,
            
        ]);

        $company_id = $request->company_id;
        BlockYears::where('block_id', $id)->where('company_id', $company_id)->delete();
        if (isset($request->choose_school_year)) {
            foreach ($request->choose_school_year as $key => $value) {
                BlockYears::insert([
                    'company_id' => $request->company_id,
                    'block_id' => $id,
                    'school_academic_year_id' => $value
                ]);

            }
        }


        $block_height = intval($request->block_height);
        $locker_blocks = intval($request->locker_blocks);
        $locker_to_update = $block_height * $locker_blocks;

        $lockers = Locker::where('block_id', $id)->where('active', 1)->get();
        $locker_count = count($lockers);
//        dd($locker_count, $locker_to_update);
        if ($locker_count == $locker_to_update) {
            return response()->json($data);
        } else if ($locker_count > $locker_to_update) {
            //      dd("1");
            $locker_to_reduce = $locker_count - $locker_to_update;
//            dd($locker_to_reduce);
            $locker_to_skip = $locker_count - $locker_to_reduce;
//            $deleteUs = Locker::where('block_id', $id)->where('locker_number' ,'>', $locker_to_skip)->delete();
            $inactive_lockers = Locker::where('block_id', $id)->where('active', 1)->orderBy('locker_order', 'desc')->take($locker_to_reduce)->get();
            foreach ($inactive_lockers as $locker) {
                $lockerupdate = Locker::where('id', $locker->id)->first();
                $lockerupdate->active = 0;
                $lockerupdate->display = 1;
                $lockerupdate->save();
            }
        } else if ($locker_count < $locker_to_update) {

//            $latest = Locker::where('block_id', $id)->orderBy('locker_number', 'DESC')->first();
            $latest = Locker::where('block_id', $id)->orderBy('locker_order', 'DESC')->where('active', 1)->first();
            if (isset($latest)) {
                $latest_number = $latest->locker_number + 1;
            } else {
                $latest_number = 1;
            }
            $latest_order = Locker::where('block_id', $id)->orderBy('locker_order', 'DESC')->first();
            if (isset($latest_order)) {
                $latest_order_number = $latest_order->locker_order + 1;
            } else {
                $latest_order_number = 1;
            }


            $latestactive = Locker::where('block_id', $id)->where('active', 1)->orderBy('locker_number', 'DESC')->first();
            $locker_gap = $locker_to_update - $locker_count;
            for ($i = 0; $i < $locker_gap; $i++) {
                $lockerdeleted = Locker::where('block_id', $id)->where('active', 0)->where('locker_number', $latest_number)->first();
                if ($lockerdeleted) {

                    $lockerdeleted->locker_order = $latest_order_number;
                    $lockerdeleted->active = 1;
                    $lockerdeleted->display = 1;
                    $lockerdeleted->save();
                } else {

                    Locker::insert([
                        'block_id' => $id,
                        'company_id' => $company_id,
                        'locker_number' => $latest_number,
                        'locker_order' => $latest_order_number,
                        'active' => 1,
                        'display' => 1
                    ]);
                }

                $latest_number = $latest_number + 1;
                $latest_order_number = $latest_order_number + 1;
            }

        }

        return response()->json($data);
    }
}
