<?php

namespace App\Http\Controllers\Backend;

use App\Block;
use App\ChatTypes;
use App\Company;
use App\Franchisee;
use App\FranchiseeCompany;
use App\Locker;
use App\LockerOffSale;
use App\LockerRenumber;
use App\ManualLockerCode;
use App\Rate;
use App\Maintenance;
use App\DocumentTypes;
use App\Sale;
use App\Document;
use App\SchoolAcademicYear;
use App\SchoolYear;
use App\StatusLog;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;
use App\LocationGroup;
use App\CompanySchoolYear;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\BlockGroup;
use App\LockerColor;
use App\Golive;

use App\Franchisee_report;
use Spatie\Permission\Models\Role;

use App\BlockGroupCode;


class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index()
    {   
         // update the lockers
        $roles = Role::get();

        $locationGroups = LocationGroup::get();
        $rates = Rate::orderBy('order', 'ASC')->get();

        $franchises = Franchisee::query();
        // permission added
         $franchisee_report_obj =  new Franchisee_report();
            $franchises = $franchisee_report_obj->franchisee_dropdown_record_permissions($franchises);
            $franchises=$franchises->orderBy('name', 'ASC')->get();
            // $franchisees=$franchises;
        // permission denied
        $documents = Document::all();
        $chat_types = ChatTypes::get();
        $document_types = DocumentTypes::get();
        $school_academic_year = SchoolAcademicYear::orderBy('order_number', 'asc')->get();
       
        $school_years = SchoolYear::where('status', 1)->get();
        $unActive_school_years = SchoolYear::where('status', 0)->orderBy('id','desc')->first();

       $BlockGroup = BlockGroup::where('status',1)->where('delete',0)->orderBy('order_by', 'asc')->get();
        $LockerColor = LockerColor::where('active', 1)->where('delete', 0)->orderBy('color', 'asc')->get();

        return view('backend.pages.company.index', compact('locationGroups', 'documents', 'rates', 'school_academic_year', 'school_years', 'franchises', 'chat_types', 'document_types' ,'BlockGroup','LockerColor','roles','unActive_school_years'));
    }

    //show all_companies in table using datatable
    public function show_all(Request $request)
    {

        $company = Company::query();

       
        // if(auth()->user()->company_id)
        // {
        //     $company->where('id',auth()->user()->company_id);
        // }
        //filters
        if (isset($request->search_status_type) && !empty($request->search_status_type)) {
            if ($request->search_status_type == 'all') {

            } else {
                $company->where('status_type', $request->search_status_type);
            }
        }

        if (isset($request->search_franchise) && !empty($request->search_franchise)) {
            if ($request->search_franchise == 'all') {

            } else {
                $franchisee_companies = FranchiseeCompany::where('franchisee_id', $request->search_franchise)->pluck('company_id')->toArray();

                $company->whereIn('id', $franchisee_companies);//
            }
        }

        //  if(auth()->user()->can('All Records')) {

        // }else if(auth()->user()->hasRole('Franchisee'))
        // {
        //     $franchisee_ids=DB::table('franchisee_user')->where('user_id',auth()->user()->id)->pluck('franchisee_id');
        //     $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id', $franchisee_ids)->pluck('company_id')->toArray();
           

        //     array_push($franchisee_companies, auth()->user()->company_id);
             
        //     $company->whereIn('id', $franchisee_companies);//
        //      // $company->orWhere('id',auth()->user()->company_id);

        // }else if(auth()->user()->hasRole('Company'))
        // {
        //      $company->where('id',auth()->user()->company_id);
        // }else{
            
        // }

        //record permission on the basis of admin, franchise group & direct company assigned & user
        $company_obj = new Company();
        $company = $company_obj->company_record_permissions($company);

        $company->where('active', '!=', 0);
        // $company->orderBy('id', 'desc');


        return Datatables::of($company)
            ->addColumn('action', function ($company) {
                $buttons = '';
                if (auth()->user()->can('Edit Company')) {

                    $buttons = '<a href="#edit" class="btn btn-primary btn-md adv_cust_mod_btn edit-btn pull-left" data-toggle="modal" style="margin-right: 3px;" data-href="#edit" data-id="' . $company->id . '" data-name="' . $company->name . '">Edit</a>';
                }
                if (auth()->user()->can('Delete Company')) {
                    $buttons .= '<a href = "JavaScript:Void(0);" onclick = "deleteData(\'' . route('company.destroy', $company->id) . '\',\'' . csrf_token() . '\',\'\',company_table,\'DELETE\')" class="btn btn-danger" > Delete</a >';
                }
                return $buttons;
            })
            ->setRowClass(function ($company) {
                $active = $company->active;
                $display = $company->display;
                if ($active == 1 && $display == 1) {
                    return 'alert-success';
                } else if ($active == 0 && $display == 0) {
                    return 'alert-danger';
                } else if ($active == 1 && $display == 0) {
                    return 'alert-warning';
                }
            })
            ->make(true);
    }

    //show deleted companies
    public function show_deleted()
    {

        $company = Company::query();

        //record permission on the basis of admin, franchise group & direct company assigned & user
        $company_obj = new Company();
        $company = $company_obj->company_record_permissions($company);

        $company->where('active', 0)->where('display', 0)->orderBy('active', 'DESC')
            ->orderBy('display', 'DESC')
            ->orderBy('name', 'ASC');

        return Datatables::of($company)
            ->addColumn('action', function ($company) {
                $buttons = '';
                if (auth()->user()->can('Edit Company')) {

                    $buttons = '<a href="#edit" class="btn btn-primary btn-md adv_cust_mod_btn edit-btn pull-left" data-toggle="modal" style="margin-right: 3px;" data-href="#edit" data-id="' . $company->id . '" data-name="' . $company->name . '">Edit</a>';
                }
                return $buttons;
            })
            ->setRowClass(function ($company) {
                $active = $company->active;
                $display = $company->display;
                if ($active == 1 && $display == 1) {
                    return 'alert-success';
                } else if ($active == 0 && $display == 0) {

                    return 'alert-danger';
                } else if ($active == 1 && $display == 0) {
                    return 'alert-warning';
                }
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locationGroups = LocationGroup::get();
        return view('backend.pages.company.create', ['locationGroups' => $locationGroups]);
    }

    public function store(Request $request)
    {
        //validation
        $validator = $this->validate($request, [
            'location_group_id' => 'required',
            'currency' => 'required',
            'name' => 'required|max:120',

        ]);

        $company = new Company();
        $company->location_group_id = $request['location_group_id'];
        $company->currency = $request['currency'];
        $company->name = $request['name'];
        $company->name_number = $request['name_number'];
        $company->street = $request['street'];
        $company->city = $request['city'];
        $company->country = $request['country'];
        $company->post_code = $request['post_code'];

        if ($request['active']) {

            $company->active = 1;
        } else {
            $company->active = 0;
        }
        if ($request['display']) {

            $company->display = 1;
        } else {
            $company->display = 0;
        }
        if ($request['keep_block']) {

            $company->keep_block = 1;
        } else {
            $company->keep_block = 0;
        }
        if ($request['stop_auto_rebook']) {

            $company->stop_auto_rebook = 1;
        } else {
            $company->stop_auto_rebook = 0;
        }
        if (isset($request->status_type)) {

            $company->status_type = $request->status_type;

        }
        $data = $company->save();

        //school year added
        if ($request->roles) {
            foreach ($request->roles as $key => $value) {
                $company_school_year = CompanySchoolYear::insert([
                    'school_academic_year_id' => $value,
                    'company_id' => $company->id
                ]);
            }

        }

        //assign company to franchises
        if (isset($request->franchisee_id)) {

            $franchise_company = new FranchiseeCompany();
            $franchise_company->franchisee_id = $request->franchisee_id;
            $franchise_company->company_id = $company->id;
            $franchise_company->save();

        }

        return redirect()->route('company.index')
            ->with('flash_message',
                'Company successfully added.');
    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::findOrFail($id);
        if ($company) {
            $company->active = 0;
            $company->display = 0;
            $company->save();
            return response()->json(['success' => 'Company successfully deleted']);

        } else {
            return response()->json(['error' => 'Company Not deleted!']);
        }
    }


    //locker blank action & renumber
    public function posteditlocker(Request $request)
    {

        if (isset($request->selected_lockers)) {

            //make selected lockers in sorting order
            $locker_selected_array = $request->selected_lockers;
            foreach ($request->selected_lockers as $index => $selected_locker) {
                $locker = Locker::where('id', $selected_locker[1])->first();
                array_push($locker_selected_array[$index], $locker->locker_order);
            }

            usort($locker_selected_array, function ($a, $b) {
                return $a[2] <=> $b[2];
            });

            foreach ($locker_selected_array as $index => $selected_locker) {

                if (isset($selected_locker[0])) {

                    //reorder numbers
                    if ($request->reorder && $request->locker_active == 1 && $request->locker_display == 1) {

                        $locker = Locker::where('id', $selected_locker[1])->first();
                        $lockername = Locker::where('locker_number', (int)$selected_locker[0])->where('locker_number', '<', $locker->locker_number)->where('block_id', $locker->block_id)->get();

                        $all_lockers = Locker::where('locker_order', '>', $locker->locker_order)->where('block_id', $locker->block_id)->where('active', 1)->where('display', 1)->orderBy('locker_order', 'ASC')->get();
                        $newLockerName = (int)$selected_locker[0];
                        foreach ($all_lockers as $one_locker) {
                            $newLockerName = $newLockerName + 1;
                            $lockernew = Locker::where('id', $one_locker->id)->first();
                            $lockernew->locker_number = $newLockerName;
                            $lockernew->save();

                            $locker_renumber = new LockerRenumber();
                            $locker_renumber->locker_id = $selected_locker[1];
                            $locker_renumber->user_id = Auth::user()->id;
                            $locker_renumber->old_number = $one_locker->locker_number;
                            $locker_renumber->new_number = $newLockerName;
                            $locker_renumber->save();
                        }

                        $locker_renumber = new LockerRenumber();
                        $locker_renumber->locker_id = $selected_locker[1];
                        $locker_renumber->user_id = Auth::user()->id;
                        $locker_renumber->old_number = $locker->locker_number;
                        $locker_renumber->new_number = $selected_locker[0];
                        $locker_renumber->save();

                        $locker->locker_number = $selected_locker[0];
                        $locker->active = $request->locker_active;
                        $locker->display = $request->locker_display;
                        $locker->save();
                        return 1;
                    }

                    $locker = Locker::where('id', $selected_locker[1])->first();

                    $reorder = 0;
                    $locker_name = $selected_locker[0];
                    if ($locker_name == 0)
                        $locker_name = null;

                    //blank lockers
                    if ($request->locker_active == 1 && $request->locker_display == 0) {
//                        if($index == 2)
//                            dd($locker);
//                        $locker_order = $selected_locker[2];
                        $locker_order = (int)$locker->locker_order - (int)$index;
                        $change_lockers = Locker::where('locker_order', '>=', $locker_order)->where('block_id', $locker->block_id)->orderBy('locker_order', 'ASC')->get();
//                        dd($change_lockers);
                        $numItems = $change_lockers->count();
//                        dd($numItems);
                        $i = 0;

                        foreach ($change_lockers as $key => $one_locker) {
                            $lockernew = Locker::where('id', $one_locker->id)->first();
//                            if($one_locker->id == $selected_locker[1]){
//
//                            }
                            $lockernew->locker_order = $lockernew->locker_order + 1;
                            $lockernew->save();

//                            ++$i;
//                            if ($one_locker->display == 1 && $one_locker->active == 1) {
//
//                                if ($i === $numItems) {
////                                dd('dd');
//                                    if ($one_locker->display == 1) {
//                                        $one_locker->locker_order = $one_locker->locker_order + 1;
//                                        $one_locker->save();
//                                    }
//                                } else {
//                                    $incKey = $key + 1;
////                                dd($incKey);
////                                dd($i);
//
//                                    $blank = 0;
//                                    $increment = 1;
////                                $counter++;
////                                dd($counter);
//                                    while (true) {
//                                        if($incKey < $numItems) {
//                                            if ($change_lockers[$incKey]->display == 0 && $change_lockers[$incKey]->active == 1) {
//                                                $incKey = $incKey + 1;
//                                                $blank = 1;
//                                                $increment++;
////                                            $counter++;
//                                            } else {
//                                                break;
//                                            }
//                                        }else{
//                                            break;
//                                        }
//                                    }
//
//                                    if ($blank == 1) {
//                                        $one_locker->locker_order = $one_locker->locker_order + $increment;
//                                        $one_locker->save();
//                                    } else {
//                                        $one_locker->locker_order = $one_locker->locker_order + 1;
//                                        $one_locker->save();
//                                    }
//                                }
//                            }

//                            $locker->active = 1;
//                            $locker->->display = 0;


//                            if(++$i === $numItems) {
//                                if($lockernew->display == 1){
//                                    $lockernew->locker_order = $lockernew->locker_order + 1;
//                                }
//                            }
//
//                            else{
//                                $incKey = $key+1;
//                                $blank = 0;
//                                while(true){
//                                    if($change_lockers[$incKey]->display == 0 && $change_lockers[$incKey]->active == 1){
//                                        $incKey = $incKey + 1;
//                                        $blank = 1;
//                                    }
//                                    else{
//                                        break;
//                                    }
//                                }
//                                if($blank == 1){
//                                    $lockernew->locker_order = $lockernew->locker_order + $incKey;
//                                }
//                                else{
//                                    $lockernew->locker_order = $lockernew->locker_order + 1;
//                                }
//
//                                if($change_lockers[$key+1]->display == 0 && $change_lockers[$key+1]->active == 1){
//                                    $lockernew->locker_order = $lockernew->locker_order + 2;
//                                }
//                                else{
//                                    if($lockernew->display == 1){
//                                        $lockernew->locker_order = $lockernew->locker_order + 1;
//                                    }
//                                }
//                            }


                        }

//                        if($index == 2)
//                            dd($locker->locker_order);


//                        if($index == 2)
//                            dd($locker_order);

//                        dd($locker_order);
                        $newLocker = new Locker();
                        $newLocker->locker_number = $locker_name;
                        $newLocker->locker_order = $locker_order;
                        $newLocker->block_id = $locker->block_id;
                        $newLocker->company_id = $locker->company_id;
                        $newLocker->location_info = $locker->location_info;
                        $newLocker->updated = $locker->updated;
                        $newLocker->date_added = date('Y-m-d H:i:s');
                        $newLocker->active = 1;
                        $newLocker->display = 0;
                        $newLocker->save();
//                        dd('22');


                    } else if ($request->locker_active == 0 && $request->locker_display == 0) {
//                        dd($selected_locker[1]);
                        $restore_locker = Locker::find($selected_locker[1]);
                        $restore_locker->active = 1;
                        $restore_locker->display = 1;
                        $restore_locker->save();


                    } else if ($request->locker_active == 0 && $request->locker_display == 1) {
//                        dd($request->all());


                        $locker->locker_number = $locker_name;
                        $locker->active = $request->locker_active;
                        $locker->display = $request->locker_display;
                        $locker->save();

                        $lockers = Locker::where('block_id', $locker->block_id)->orderBy('locker_order', 'ASC')->get();

                        foreach ($lockers as $index => $locker) {
                            ++$index;
                            $locker->locker_order = $index;
                            $locker->save();

                        }


                    } else {
//                        dd($request->all());
                        if ($locker->display == 0 && $locker->active == 1) {

                            $locker_order = (int)$locker->locker_order;
                            $change_lockers = Locker::where('locker_order', '>', $locker_order)->where('block_id', $locker->block_id)->orderBy('locker_order', 'ASC')->get();
//                        dd($change_lockers);

                            foreach ($change_lockers as $key => $one_locker) {
                                $lockernew = Locker::where('id', $one_locker->id)->first();
                                $lockernew->locker_order = $lockernew->locker_order - 1;
                                $lockernew->save();

                            }
                            $del = Locker::where('id', $selected_locker[1])->delete();

                        } else {
//                            dd('dele');
                            $locker_renumber = new LockerRenumber();
                            $locker_renumber->locker_id = $selected_locker[1];
                            $locker_renumber->user_id = Auth::user()->id;
                            $locker_renumber->old_number = $locker->locker_number;
                            $locker_renumber->new_number = $locker_name;
                            $locker_renumber->save();

                            $locker->locker_number = $locker_name;
                            $locker->active = $request->locker_active;
                            $locker->display = $request->locker_display;
                            $locker->save();

                            $lockers = Locker::where('block_id', $locker->block_id)->orderBy('locker_order', 'ASC')->get();

                            foreach ($lockers as $index => $locker) {
                                ++$index;
                                $locker->locker_order = $index;
                                $locker->save();

                            }

                        }

                    }

                    $newLockerName = 0;

                } else {

                    $locker = Locker::where('id', $selected_locker[1])->first();
                    $locker->active = $request->locker_active;
                    $locker->display = $request->locker_display;
                    $locker->save();
                }

                $lockers = Locker::where('block_id',
                    $locker->block_id)->orderBy('locker_order', 'ASC')->get();

                foreach ($lockers as $index => $locker) {
                    ++$index;
                    $locker->locker_order = $index;
                    $locker->save();

                }
            }
        }
        return 1;
    }

    //put in maintenanace lockers
    public function maintenancelocker(Request $request)
    {
//        dd($request->all());

        if (isset($request->selected_lockers)) {
            $start_maintenance = Carbon::now()->toDateTimeString();

            if (isset($request->maintenance_till_date))
                $end_maintenance = Carbon::createFromFormat('d M, Y', $request->maintenance_till_date)->startOfDay()->toDateTimeString();
//                $end_maintenance =  Carbon::parse($request->maintenance_till_date)->endOfDay()->toDateTimeString();

            else
                $end_maintenance = Carbon::now()->endOfDay()->toDateTimeString();

//            dd($end_maintenance);

            foreach ($request->selected_lockers as $selected_locker) {
//            dd($selected_locker[1]);
//            dd(Carbon::parse($request->maintenance_till_date)->toDateTimeString());
                if (isset($selected_locker[1])) {

                    $is_maintained = Maintenance::where('locker_id', $selected_locker[1])
                        ->where('active', 1)
                        ->first();
                    $locker = Locker::find($selected_locker[1]);
                    if (empty($is_maintained)) {


                        $maintenance = new Maintenance();
                        $maintenance->company_id = $locker->company_id;
                        $maintenance->block_id = $locker->block_id;
                        $maintenance->locker_id = $selected_locker[1];
                        $maintenance->type = $request->maintenance_type;
                        $maintenance->from = $start_maintenance;
                        $maintenance->to = $end_maintenance;
                        $maintenance->active = $request->locker_active;
                        $maintenance->save();

                    } else {


                        $maintenance = Maintenance::where('locker_id', $selected_locker[1])->where('active', 1)->first();
//                            dd($maintenance);
                        $maintenance->active = 0;

                        $maintenance->save();
//                        Maintenance::where('to', '>=', $start_maintenance)->Where('from', '<=', $end_maintenance)
//                            ->where('locker_id',$selected_locker[1])
//                            ->where('active', 1)
//                            ->delete();
                    }


                }


            }
        }

        return 1;
    }
    //put in offsale lockers
    public function onOffSale(Request $request)
    {
//        dd($request->all());
//        dd(Carbon::parse($request->maintenance_till_date)->endOfDay()->toDateTimeString());
        if (isset($request->selected_lockers)) {
            foreach ($request->selected_lockers as $selected_locker) {
//            dd($selected_locker[1]);
//            dd(Carbon::parse($request->maintenance_till_date)->toDateTimeString());
                if (isset($selected_locker[1])) {
                    $is_off_sale = LockerOffSale::where('locker_id', $selected_locker[1])->where('active', 1)->first();
                    if (empty($is_off_sale)) {
                        $on_off_sale = new LockerOffSale();
                        $on_off_sale->locker_id = $selected_locker[1];
                        $on_off_sale->active = $request->locker_active;
                        $on_off_sale->save();


                    } else {
                        $on_off_sale = LockerOffSale::where('locker_id', $selected_locker[1])->where('active', 1)->first();
                        $on_off_sale->active = 0;
                        $on_off_sale->save();
//                        LockerOffSale::where('locker_id',$selected_locker[1])->where('active',1)->delete();
                    }
                }


            }
        }

        return 1;
    }

    //move within block lockers
    public function moveWithInBlock(Request $request)
    {
//        dd($request->all());
//        dd(Carbon::parse($request->maintenance_till_date)->endOfDay()->toDateTimeString());
        if (isset($request->selected_lockers)) {
            foreach ($request->selected_lockers as $selected_locker) {
//            dd($selected_locker[1]);
//            dd(Carbon::parse($request->maintenance_till_date)->toDateTimeString());
                if (isset($selected_locker[1])) {

                    $new_order_locker = Locker::where('id', $selected_locker[1])->first();
//                    dd($new_order_locker);
                    $moved_after_locker = Locker::where('locker_number', '=', $request->move_locker_number)->where('block_id', $new_order_locker->block_id)->first();
//                    dd($moved_after_locker);
                    $change_lockers = Locker::where('locker_order', '>=', $moved_after_locker->locker_order)->where('block_id', $new_order_locker->block_id)->orderBy('locker_order', 'ASC')->get();

                    $numItems = $change_lockers->count();
//                    dd($numItems);
                    $i = 0;
                    foreach ($change_lockers as $key => $locker) {
//                        ++$i;
//                        if ($locker->display == 1 && $locker->active == 1) {
//
//                            if ($i === $numItems) {
////                                dd('dd');
//                                if ($locker->display == 1) {
//                                    $locker->locker_order = $locker->locker_order + 1;
//                                    $locker->save();
//                                }
//                            } else {
//                                $incKey = $key + 1;
////                                dd($incKey);
////                                dd($i);
//
//                                $blank = 0;
//                                $increment = 1;
////                                $counter++;
////                                dd($counter);
//                                while (true) {
//                                    if($incKey < $numItems) {
//                                        if ($change_lockers[$incKey]->display == 0 && $change_lockers[$incKey]->active == 1) {
//                                            $incKey = $incKey + 1;
//                                            $blank = 1;
//                                            $increment++;
////                                            $counter++;
//                                        } else {
//                                            break;
//                                        }
//                                    }else{
//                                        break;
//                                    }
//                                }
//
//                                if ($blank == 1) {
//                                    $locker->locker_order = $locker->locker_order + $increment;
//                                    $locker->save();
//                                } else {
                        $locker->locker_order = $locker->locker_order + 1;
                        $locker->save();
//                                }
//                            }
//                        }


//                        $locker->locker_order = $locker->locker_order + 1;
//                        $locker->save();
                    }


                    $new_order_locker->locker_order = $moved_after_locker->locker_order;
                    $new_order_locker->save();

                    $lockers = Locker::where('block_id',
                        $new_order_locker->block_id)->orderBy('locker_order', 'ASC')->get();

                    foreach ($lockers as $index => $locker) {
                        ++$index;
                        $locker->locker_order = $index;
                        $locker->save();

                    }

                }


            }
        }

        return 1;
    }
    //move outside block lockers
    public function moveOutSideBlock(Request $request)
    {

        $locker_selected_array = $request->selected_lockers;
        $this->check_height_lockers_balanced($request->block_id);

        $moved_from_count = 0;
        $moved_to_count = 0;
        $total_moved = 0;
        $moved_from_block_height_increment = 0;
        $moved_to_block_height_increment = 1;

        //height update of blocks
        foreach ($request->selected_lockers as $count => $selected_locker) {

            $locker = Locker::where('id', $selected_locker[1])->first();

            $moved_locker_id = $locker->id;
//            dd($moved_locker_id);
            $moved_from_block = $locker->block_id;
            $moved_to_block = $request->block_id;
            $this->update_blocks_in_sale($moved_from_block, $moved_to_block, $moved_locker_id);
            $this->update_blocks_in_maintenance($moved_from_block, $moved_to_block, $moved_locker_id);
            $this->update_blocks_in_manual_locker_code($moved_from_block, $moved_to_block, $moved_locker_id);

            array_push($locker_selected_array[$count], $locker->locker_order);

            $moved_from = Block::find($locker->block_id);

            $moved_from_count++;
            if ($moved_from_count >= $moved_from->block_height) {
                $moved_from_block_height_increment++;
                $moved_from_count = 0;
            }

            $moved_to = Block::find($request->block_id);

            $moved_to_count++;
            if ($moved_to_count > $moved_to->block_height) {
                $moved_to_block_height_increment++;
                $moved_to_count = 0;
            }
            $total_moved++;


        }

//        dd($moved_from_block , $moved_to_block);

        usort($locker_selected_array, function ($a, $b) {
            return $a[2] <=> $b[2];
        });

        if (isset($request->selected_lockers)) {
            foreach ($locker_selected_array as $index => $selected_locker) {

                if (isset($selected_locker[1])) {

                    $moved_to_block = Locker::where('block_id', $request->block_id)->where('active', 1)->orderBy('locker_order', 'DESC')->first();

                    $update_locker = Locker::find($selected_locker[1]);


                    $moved_from = Locker::where('block_id', $update_locker->block_id)->where('active', 1)->orderBy('locker_order', 'DESC');
                    $moved_from_last_order_number = $moved_from->first();


                    if ($moved_from_block_height_increment > 0) {

                        $moved_from_block_updated = Block::find($moved_from_last_order_number->block_id);
                        $moved_from_block_updated->locker_blocks = $moved_from_block_updated->locker_blocks - $moved_from_block_height_increment;
//
                        $moved_from_block_updated->save();

                        $moved_from_block_height_increment = 0;

                    }

                    if ($moved_to_block_height_increment > 0) {

                        $moved_to_block_updated = Block::find($moved_to_block->block_id);
                        $moved_to_block_updated->locker_blocks = $moved_to_block_updated->locker_blocks + $moved_to_block_height_increment;
                        $moved_to_block_updated->save();

                        $moved_to_block_height_increment = 0;

                    }

                    $moved_from_block = $update_locker->block_id;

                    $update_locker->block_id = $request->block_id;
                    $update_locker->locker_order = $moved_to_block->locker_order + 1;
                    $update_locker->save();

                    if ($total_moved == ++$index) {
                        $this->check_equality_lockers($request->block_id);
                        for ($i = 0; $i < $moved_from_count; $i++) {

                            $moved_from_last_order_number = Locker::where('block_id', $moved_from_block)->where('active', 1)->orderBy('locker_order', 'DESC')->first();

                            $new_blank_locker = new Locker();
                            $new_blank_locker->block_id = $moved_from_last_order_number->block_id;
                            $new_blank_locker->company_id = $moved_from_last_order_number->company_id;
                            $new_blank_locker->date_added = Carbon::now();
                            $new_blank_locker->locker_order = $moved_from_last_order_number->locker_order + 1;
                            $new_blank_locker->active = 1;
                            $new_blank_locker->display = 0;
                            $new_blank_locker->save();
                        }
                    }


                    //update locker order ASC
                    $lockers = Locker::where('block_id',
                        $update_locker->block_id)->orderBy('locker_order', 'ASC')->get();

                    foreach ($lockers as $index => $locker) {
                        ++$index;
                        $locker->locker_order = $index;
                        $locker->save();

                    }

                }
            }
        }

        return 1;
    }

    //check activated lockers and update locker_blocks according to existing activated lockers
    public function check_height_lockers_balanced($block_id)
    {
        $moved_to_total_lockers = Locker::where('block_id', $block_id)->where('active', 1)->count();

        $moved_to_block_updated = Block::find($block_id);
        $block_width = ceil($moved_to_total_lockers / $moved_to_block_updated->block_height);
        $moved_to_block_updated->locker_blocks = $block_width;
        $moved_to_block_updated->save();

    }

    //
    public function update_blocks_in_sale($moved_from_block, $moved_to_block, $moved_locker_id)
    {

        $block = Block::find($moved_to_block);

        $sales = Sale::whereDate('end', '>=', Carbon::now()->toDateString())->where('locker_id', $moved_locker_id)->get();

        foreach ($sales as $sale) {

            $sale->block_id = $block->id;
            $sale->block_name = $block->name;
            $sale->save();
        }

    }

    public function update_blocks_in_maintenance($moved_from_block, $moved_to_block, $moved_locker_id)
    {

//       $block =  Block::find($moved_to_block);
        $maintenances = Maintenance::whereDate('to', '>=', Carbon::now()->toDateString())->where('locker_id', $moved_locker_id)->where('active', 1)->get();

        foreach ($maintenances as $maintenance) {

            $maintenance->block_id = $moved_to_block;
            $maintenance->save();
        }

    }

    //update blocks in manual locker codes
    public function update_blocks_in_manual_locker_code($moved_from_block, $moved_to_block, $moved_locker_id)
    {
        $manual_codes = ManualLockerCode::whereDate('end', '>=', Carbon::now()->toDateString())->where('locker_id', $moved_locker_id)->get();

        foreach ($manual_codes as $manual_code) {
            $manual_code->block_id = $moved_to_block;
            $manual_code->save();
        }

    }

    //check equality lockers with tier & lockers_blocks with existing lockers
    public function check_equality_lockers($block_id)
    {
        $moved_to_total_lockers = Locker::where('block_id', $block_id)->where('active', 1)->count();

        $moved_to_block_updated = Block::find($block_id);

        if ($moved_to_block_updated->block_height * $moved_to_block_updated->locker_blocks > $moved_to_total_lockers) {

            $add_blank_in_moved_count = $moved_to_block_updated->block_height * $moved_to_block_updated->locker_blocks - $moved_to_total_lockers;

             $moved_to_last_order_number = Locker::where('block_id', $block_id)->where('active', 1)->orderBy('locker_order', 'DESC')->first();

            for ($i = 0; $i < $add_blank_in_moved_count; $i++) {

                $new_blank_locker = new Locker();
                $new_blank_locker->block_id = $moved_to_last_order_number->block_id;
                $new_blank_locker->company_id = $moved_to_last_order_number->company_id;

                $new_blank_locker->date_added = Carbon::now();
                $new_blank_locker->locker_order = $moved_to_last_order_number->locker_order + 1;
                $new_blank_locker->active = 1;
                $new_blank_locker->display = 0;
                $new_blank_locker->save();

            }
        }


    }

    //get lockers after selected block in dropdown placed at company->edit-lockers tab
    public function getlockersBlock(Request $request)
    {

        //check activated lockers and update locker_blocks according to existing activated lockers
        $block_id = $request->block_id;
        $this->check_height_lockers_balanced($block_id);

        $checkboxvalue = $request->checkboxvalue;
        if (isset($block_id)) {
            //get blocks activated or deletec according to user request in checkboxvalue
            if ($checkboxvalue == 0) {
                $locker_numbers = Locker::where('lockers.block_id', $block_id)->where('active', 1)
                    ->orderBy('locker_order', 'ASC')
                    ->get();
            } else {
                $locker_numbers = Locker::where('lockers.block_id', $block_id)
                    ->where('active', 0)
                    ->orderBy('locker_order', 'ASC')
                    ->get();
            }

            $total_lockers = $locker_numbers->count();
            $locker_numbers->toArray();
           
            //set interval of dates u want to fetch lockers
            $current_date = Carbon::now();
            if (date("m") < 7) {
                $start = Carbon::now()->toDateTimeString();
                $end = Carbon::now()->toDateTimeString();
            } else {
                $start = date("Y-09-01 00:00:00");
                $end = date("Y-09-01 23:59:59");

            }

            $output = '';

            $blocks = Block::leftJoin('locker_block_image', 'locker_block_image.id', '=', 'blocks.locker_block_image_id')
                ->select('blocks.*', 'locker_block_image.file_name as locker_block_image')
                ->where('blocks.id', $block_id)->first();
            $locker_blocks = 0;
            $locker_blocks = $blocks->locker_blocks;

            $lockers_in_single_config = 0;
            $empty_lockers_in_config = 0;
            $lockers_in_single_column_array = [];
            $last_order_array = [];
            $last_order_in_single_column_array = [];

            $output .= '<div class="col-md-12" >';

            //row colors showing sample on the basis of deleted checkbox
            if ($checkboxvalue == 0) {
                $output .= '<div class="row">
                <div class="col-2 pr-2">
                    <div class="padding10" style="background: #00c0ef; text-align: center;"><span style="color: white">Active</span></div>
                </div>
                <div class="col-2 pr-2">
                    <div class="padding10" style="background: orange; text-align: center;"><span style="color: white">Booked</span></div>
                </div>
                <div class="col-2 pr-2">
                    <div class="padding10" style="background: #BA55D3; text-align: center;"><span style="color: white">Maintenance</span></div>
                </div>
          
                <div class="col-2 pr-2">
                    <div class="padding10" style="background: #FF333C; text-align: center;"><span style="color: white">On Sale/Off Sale</span></div>
                </div>
                <div class="col-2 pr-2">
                    <div class="padding10" style="background: #9d9d9d; text-align: center;"><span style="color: white">Blank</span></div>
                </div>
            
            </div>';
            } else {
                $output .= '<div class="col-2 pr-2">
                    <div class="padding10" style="background: #B5040C; text-align: center;"><span style="color: white">Deleted</span></div>
                </div>';
            }

            $output .= '<br><div class="row" style="overflow-x:auto;">        
            <table id="locker_table">';

            $counter = 0;
            $counter_red = 0;
            //run the loop on the basis of locker_block column in blocks table
            for ($i = 1; $i <= $locker_blocks; $i++) {

                $red_column = 0;
                $output .= '<tr>';
                $line_counter = 0;

                for ($j = 1; $j <= $blocks->block_height; $j++) {
                  
                    $line_counter++;

                    if ($counter <= $total_lockers - 1) {

                        $locker_number = $locker_numbers[$counter]->locker_number;
                        

                        if ($locker_number == '' or $locker_number == null) {
                            $locker_number = 0;
                        }
                        $is_sales = Sale::where('locker_id', $locker_numbers[$counter]['id'])->first();
                         
                        $is_sale_onging = Sale::where('locker_id', $locker_numbers[$counter]['id'])
                            ->where(function ($query) use ($current_date) {
                                $query->whereDate('sales.end', '>=', $current_date)
                                    ->whereDate('sales.start', '<=', $current_date);
                            })
                            ->whereIn('status', ['b','r'])
                            ->first();

                            // if(isset($is_sales) && isset($is_sale_onging))
                            // {
                            //     if($is_sales->created_at>$is_sale_onging->created_at)
                            //     {
                            //         unset($is_sale_onging);
                            //     }else{
                            //         unset($is_sales);
                            //     }
                            // }
                        $is_maintained = Maintenance::where('locker_id', $locker_numbers[$counter]['id'])
                            ->where('active', 1)
                            ->where(function ($query) use ($current_date) {
                                $query->whereDate('from', '<=', $current_date)
                                    ->whereDate('to', '>=', $current_date);
                            })
                            ->first();
                            if(isset($is_sale_onging)){
                                if(isset($is_maintained)){
                                    if($is_maintained->created_at>$is_sale_onging->created_at)
                                    {
                                        unset($is_sale_onging);
                                    }else{
                                        unset($is_maintained);
                                    }
                                }
                            }
                            // else if(isset($is_sales))
                            // {
                            //     if(isset($is_maintained)){
                            //         if($is_maintained->created_at>$is_sales->created_at)
                            //         {
                            //             unset($is_sales);
                            //         }else{
                            //             unset($is_maintained);
                            //         }
                            //     }
                            // }
                        $is_off_sale = LockerOffSale::where('locker_id', $locker_numbers[$counter]['id'])->where('active', 1)->first();
                        // if(isset($is_sales))
                        // {
                        //     if(isset($is_off_sale))
                        //     {
                        //         if($is_off_sale->created_at>$is_sales->created_at)
                        //         {
                        //             unset($is_sales);
                        //         }else{
                        //             unset($is_off_sale);
                        //         }
                        //     }
                        // }else
                         if(isset($is_sale_onging))
                        {
                            if(isset($is_off_sale))
                            {
                                 if($is_off_sale->created_at>$is_sale_onging->created_at)
                                {
                                    unset($is_sale_onging);
                                }else{
                                    unset($is_off_sale);
                                }
                            }

                        }else if(isset($is_maintained))
                        {
                              if(isset($is_off_sale))
                            {
                                 if($is_off_sale->created_at>$is_maintained->created_at)
                                {
                                    unset($is_maintained);
                                }else{
                                    unset($is_off_sale);
                                }
                            }
                        }
                        //showing lockers active = 1 & display= 1
                        if ($locker_numbers[$counter]['active'] == 1 && $locker_numbers[$counter]['display'] == 1) {
                            //is locker in maintenance
                            if (isset($is_maintained)) {
                                $output .= '<td >';
                                $lockers_in_single_config++;
                                if ($line_counter == 1) {
                                    $output .= '<button type="button" class="btn btn-primary btn-sm add_lockers_column' . $i . ' d-none" style="margin-left: 2.2rem">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </button><br>';

                                    $output .= '<span class="lockers' . $i . ' custom-control-description" style="color: white;padding-left: 2.4rem "></span>';
                                    $output .= '<br><hr class="line_cut' . $i . ' line" >';
                                }

                                $output .= '<input type = "checkbox" name = "locker_id" class="invisible-radio" id="locker' . $locker_numbers[$counter]['id'] . '"  data-type = "maintenance">';
                                $output .= '<label for="checkbox' . $locker_numbers[$counter]['id'] . '" class="red-lockers" > ';
                                $output .= '<div id="' . $locker_numbers[$counter]->id . '" class="styled-radio-purple text-center" onclick="clickOnlocker(' . $locker_numbers[$counter]->id . ',' . $locker_number . ',' . $locker_numbers[$counter]->active . ',' . $locker_numbers[$counter]->display . ')" style = "color: white; border: 5px solid #F1F1F1; background: #BA55D3">' . $locker_numbers[$counter]->locker_number . '<br style="margin: -15px 0px 0px 0px !important;display:block !important;content: \'\';"><span style="font-size: 11px;">' . Carbon::parse($is_maintained->to)->format('jS') . '</span><br style="margin: -29px 0px !important;display:block !important;content: \'\';"><span style="font-size: 11px;">' . Carbon::parse($is_maintained->to)->format('M y') . '</span></div ></label ></td>';
                            } //is locker in off sales
                            else if (isset($is_off_sale)) {
                                $output .= '<td >';
                                $lockers_in_single_config++;
                                //line counter of lockers
                                if ($line_counter == 1) {
                                    $output .= '<button type="button" class="btn btn-primary btn-sm add_lockers_column' . $i . ' d-none" style="margin-left: 2.2rem">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </button><br>';

                                    $output .= '<span class="lockers' . $i . ' custom-control-description" style="color: white;padding-left: 2.2rem "></span>';
                                    $output .= '<br><hr class="line_cut' . $i . ' line" >';
                                }

                                $output .= '<input type = "checkbox" name = "locker_id" class="invisible-radio" id="locker' . $locker_numbers[$counter]['id'] . '"  data-type = "on_off_sale">';
                                $output .= '<label for="checkbox' . $locker_numbers[$counter]['id'] . '" class="red-lockers" > ';
                                $output .= '<div id="' . $locker_numbers[$counter]->id . '" class="styled-radio text-center" onclick="clickOnlocker(' . $locker_numbers[$counter]->id . ',' . $locker_number . ',' . $locker_numbers[$counter]->active . ',' . $locker_numbers[$counter]->display . ')" style = "color: white; border: 5px solid #F1F1F1; background: #FF333C">' . $locker_numbers[$counter]->locker_number . '<br><span style="color: #FF333C;font-size: 11px">' . Carbon::parse($is_off_sale->created_at)->toDateString() . '</span></div ></label ></td>';
                            } else {

                                $output .= '<td >';
                                $lockers_in_single_config++;
                                if ($line_counter == 1) {
                                    $output .= '<button type="button" class="btn btn-primary btn-sm add_lockers_column' . $i . ' d-none" style="margin-left: 2.2rem">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </button><br>';

                                    $output .= '<span class="lockers' . $i . ' custom-control-description" style="color: white;padding-left: 2.2rem "></span>';
                                    $output .= '<br><hr class="line_cut' . $i . ' line" >';
                                }
                                //is locker sale ongoing
                                if (isset($is_sale_onging)) {
                                    $output .= '<input type = "checkbox" name = "locker_id" class="invisible-radio" id="locker' . $locker_numbers[$counter]['id'] . '"  >';
                                    $output .= '<label for="checkbox' . $locker_numbers[$counter]['id'] . '" class="red-lockers" > ';
                                    $output .= '<div id="' . $locker_numbers[$counter]->id . '" class="styled-radio-orange text-center" onclick="clickOnlockerNotEditable(' . $locker_numbers[$counter]->id . ',' . $locker_number . ',' . $locker_numbers[$counter]->active . ',' . $locker_numbers[$counter]->display . ')" style = "color: white; border: 5px solid #F1F1F1; background: orange">' . $locker_numbers[$counter]->locker_number . '<br ><span style="color: orange;font-size: 11px">' . Carbon::now()->toDateString() . '</span></div ></label ></td>';

                                } //available lockers
                                else {
                                    $output .= '<input type = "checkbox" name = "locker_id" class="invisible-radio" id="locker' . $locker_numbers[$counter]['id'] . '" >';


                                    $output .= '<label for="checkbox' . $locker_numbers[$counter]['id'] . '" class="red-lockers" > ';

                                    $output .= '<div id="' . $locker_numbers[$counter]->id . '" class="styled-radio text-center" onclick="clickOnlocker(' . $locker_numbers[$counter]->id . ',' . $locker_number . ',' . $locker_numbers[$counter]->active . ',' . $locker_numbers[$counter]->display . ')" style = "color: white; border: 5px solid #F1F1F1; ">' . $locker_numbers[$counter]->locker_number . '<br > <span style="color: #00c0ef;font-size: 11px">' . Carbon::now()->toDateString() . '</span></div ></label ></td>';
                                }

                            }
                        } //showing lockers active = 1 & display= 0
                        else if ($locker_numbers[$counter]['active'] == 1 && $locker_numbers[$counter]['display'] == 0) {
                            $output .= '<td >';
                            //line counter of lockers
                            if ($line_counter == 1) {

                                $output .= '<button type="button" class="btn btn-primary btn-sm add_lockers_column' . $i . ' d-none" style="margin-left: 2.2rem">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </button><br>';

                                $output .= '<span class="lockers' . $i . ' custom-control-description" style="color: white;padding-left: 2.2rem "></span>';
                                $output .= '<br><hr class="line_cut' . $i . ' line" >';
//

                            }
                            $empty_lockers_in_config++;
                            $output .= '<input type = "checkbox" name = "locker_id" id = "locker' . $locker_numbers[$counter]['id'] . '" value="' . $locker_numbers[$counter]['id'] . '" data-sale_id = "' . $request->sale_id . '" data-type = "blank" data-block_id = "' . $locker_numbers[$counter]->block_id . '" data-locker_id = "' . $locker_numbers[$counter]['id'] . '" data-rate = "' . $request['rate'] . '" data-tax = "' . $request['tax'] . '" data-counter = "" data-hold_locker="' . $request['hold_locker'] . '" class="invisible-radio choose-locker" >
                            <label for="checkbox' . $locker_numbers[$counter]['id'] . '" class="puffOut">
                            <div id="' . $locker_numbers[$counter]->id . '" class="styled-radio-gray text-center" onclick="clickOnlocker(' . $locker_numbers[$counter]->id . ',' . $locker_number . ',' . $locker_numbers[$counter]->active . ',' . $locker_numbers[$counter]->display . ')" style = "color: white;"><br ><span style="color: gray;font-size: 11px">' . Carbon::now()->toDateString() . '</span></div ></label ></td >';
                        } //showing lockers active = 0
                        else if ($locker_numbers[$counter]['active'] == 0) {


                            $output .= '<td ><input type = "checkbox" name = "locker_id" class="invisible-radio" id = "locker' . $locker_numbers[$counter]['id'] . '" >';

                            $output .= '<label for="checkbox' . $locker_numbers[$counter]['id'] . '" class="red-lockers" > ';

                            $output .= '<div id="' . $locker_numbers[$counter]->id . '" class="styled-radio-red text-center" onclick="clickOnlocker(' . $locker_numbers[$counter]->id . ',' . $locker_number . ',' . $locker_numbers[$counter]->active . ',' . $locker_numbers[$counter]->display . ')" style = "color: white; border: 5px solid #ffb2b2">' . $locker_numbers[$counter]->locker_number . '<br ></div ></label ></td>';
                        }

                        if ($line_counter == $blocks->block_height) {
                            $line_counter = 0;
                            array_push($lockers_in_single_column_array, $lockers_in_single_config);
                            array_push($last_order_array, $locker_numbers[$counter]->block_id);
                            array_push($last_order_array, $locker_numbers[$counter]->locker_order);
                            array_push($last_order_in_single_column_array, $last_order_array);
                            $last_order_array = [];
                            $lockers_in_single_config = 0;
                        } else if ($i == $locker_blocks && $counter == $total_lockers - 1) {

                            array_push($lockers_in_single_column_array, $lockers_in_single_config);
                            array_push($last_order_array, $locker_numbers[$counter]->block_id);
                            array_push($last_order_array, $locker_numbers[$counter]->locker_order);
                            array_push($last_order_in_single_column_array, $last_order_array);
                            $last_order_array = [];

                            $lockers_in_single_config = 0;
                        }
                    } else {
                    }
                    $counter++;
                }
                $output .= '</tr>';
            }
            $output .= '</table></div><div style="display: none;" class="col-md-12 locker_image">
            <img style="width:100%;" src="' . url('uploads/' . $blocks->locker_block_image) . '" class=" img-responsive" alt="Block Image">
        </div></div></div>';
//            dd($last_order_in_single_column_array);

            $data = [
                'output' => $output,
                'lockers_in_single_column_array' => $lockers_in_single_column_array,
                'last_order_in_single_column_array' => $last_order_in_single_column_array,
            ];
            return response()->json($data);
        }
    }

     public function getcompany(Request $request)
    {
        $territory_name = "";
        Session::put('company_id_blocksss', $request['company_id']);
        Session::put('company_id_chats', $request['company_id']);
        $companyid= $request->company_id;
        $company = Company::where('id', $companyid)->first();
        $locationGroups = LocationGroup::get();
//        dd($company->territory_code);
//        if($company->territory_code){
//            $franchisee = Franchisee::where('id', $company->territory_code)->first();
//            $territory_name = $franchisee->id;
//        }
       $franchisees = Franchisee::with(['franchisee_companies' =>function($query) use($request){
            return $query->where('company_id',$request->company_id);
        }])->orderBy('name', 'ASC')->get();
        
        $blocks = Block::where('company_id',$request['company_id'])->where('active',1)->orderBy('order_list','ASC')->get();
        $company_school_year = CompanySchoolYear::where('company_id', $request['company_id'])->orderBy('id', 'asc')->get();
        $block_html = '<option value="">Select Block</option>';

        foreach($blocks as $block)
        {
            $block_html .= '<option value="'.$block->id.'">'.$block->name.'</option>';
        }

        $school_academic_year = [];
        $years = [];

        foreach ($company_school_year as $key => $value) {
            $x = SchoolAcademicYear::where('id', $value->school_academic_year_id)->get();
            array_push($school_academic_year, $x);
        }
         // $go_live = Golive::where('company_id',$request['company_id'])->orderBy('id', 'desc')->first();

        $unActiveYear=SchoolYear::where([['status','0']])->orderBy('id', 'asc')->first();

        if($unActiveYear){
         $go_live = Golive::where([['company_id',$request['company_id'],['school_year',$unActiveYear->id]]])->orderBy('id', 'desc')->first();
        if($go_live){
      if($go_live->active==1)
      {
        $goliveCheck=1;
      }else{
        $goliveCheck=2;
      }
  }else{
    $goliveCheck=2;
  }
 }else{
    $goliveCheck=2;
 }
        //dd($school_academic_year, $company_school_year);

        $data = [
            'company' => $company,
            'locationGroups' => $locationGroups,
            'blocks' => $block_html,
            'franchisee' => $territory_name,
            'company_school_year' => $company_school_year,
            'school_academic_year' => $school_academic_year,
            'franchisees'=>$franchisees,
            'golive'=>$goliveCheck,
        ];
        

        return response()->json($data);
    }

    //update company
    public function updatecompany(Request $request)
    {
        $validator = $this->validate($request, [
            'location_group_id' => 'required',
            'currency' => 'required',
//            'name_comp'=>'required|max:120',
        ]);

        $company = Company::find($request['id']);

        $company->location_group_id = $request['location_group_id'];
        $company->currency = $request['currency'];
        $company->name = $request['name_comp'];
        //$company->stop_auto_rebook = $request['stop_auto_rebook'];
        $company->name_number = $request['name_number_comp'];
        $company->street = $request['street_comp'];
        $company->city = $request['city_comp'];
        $company->country = $request['country_comp'];
        $company->post_code = $request['post_code'];
        $company->note = $request['note'];
        if(!empty($request['franchisee_id']))
        {
            $franchisee_company = FranchiseeCompany::where('company_id', $request['id'])->first();
            if(!empty($franchisee_company))
            {
                $franchisee_company->franchisee_id =$request['franchisee_id'];
                $franchisee_company->save();
            }else{
                $add_franchisee_company = new FranchiseeCompany();
                $add_franchisee_company->franchisee_id = $request['franchisee_id'];
                $add_franchisee_company->company_id = $request['id'];
                $add_franchisee_company->save();
            }
        }
        if (!empty($request['territory_code'])) {
            $company->territory_code = $request['territory_code'];

            $franchisee_company = FranchiseeCompany::where('company_id', $request['id'])->first();
            if (isset($franchisee_company)) {
                $franchisee_company->franchisee_id = $request['territory_code'];
                $franchisee_company->save();
            } else {
//                dd($request['territory_code']);
                $add_franchisee_company = new FranchiseeCompany();
                $add_franchisee_company->franchisee_id = $request['territory_code'];
                $add_franchisee_company->company_id = $request['id'];
                $add_franchisee_company->save();
            }
        } else {
            $franchisee_company = FranchiseeCompany::where('company_id', $request['id'])->first();
            if ($franchisee_company)
                $franchisee_company->delete();

            $company->territory_code = null;
        }
        $company->short_post_code = $request['short_post_code'];
        $company->buzz_code = $request['buzz_code'];
        $company->town = $request['town'];
        $company->telephone = $request['telephone'];
        $company->gender = $request->gender;
        $company->la = $request['la'];
        $company->school_lea_code = $request['school_lea_code'];
        $company->low_age = $request['low_age'];
        $company->high_age = $request['high_age'];
        $company->open_date = $request['open_date'];
        $company->type_of_establishment = $request['type_of_establishment'];
        $company->sixth_form = $request['sixth_form'];
        $company->phase_of_education = $request['phase_of_education'];
        $company->boarding_school = $request['boarding_school'];
        $company->academy_trust = $request['academy_trust'];
        $company->religious_affiliation = $request['religious_affiliation'];
        $company->number_of_pupils = $request['number_of_pupils'];
        $company->area = $request['area'];
        $company->website_address = $request['website_address'];
        $company->general_email = $request['general_email'];
        $company->head_title = $request['head_title'];
        $company->head_first = $request['head_first'];
        $company->head_surname = $request['head_surname'];
        $company->free_school_meals_percentage = $request['free_school_meals_percentage'];


        if ($request['active']) {
            $company->active = 1;
        } else {
            $company->active = 0;
        }
        if ($request['status_type']) {
            $company->status_type = $request->status_type;

            $status_log = new StatusLog();
            $status_log->company_id = $request['id'];
            $status_log->user_id = Auth::user()->id;
            $status_log->status_type = $request['status_type'];
            $status_log->save();

        } else {
            $company->status_type = null;
        }
        if ($request['display']) {
            $company->display = 1;
        } else {
            $company->display = 0;
        }
        if ($request['keep_block']) {
            $company->keep_block = $request['keep_block'];
            if ($request['keep_block'] == 2) {
                $sales = Sale::where('company_id', $request['id'])->where('status', 'r')->update([
                    'locker_id' => NULL,
                    'number' => NULL
                ]);
            } else if ($request['keep_block'] == 1) {
                $sales = Sale::where('company_id', $request['id'])->where('status', 'r')->get();
                foreach ($sales as $key => $value) {
                    if ($value->hold_sales_id) {
                        $record = Sale::where('id', $value->hold_sales_id)->first();
                        $update = Sale::where('id', $value->id)->update([
                            'number' => $record->number,
                            'locker_id' => $record->locker_id
                        ]);
                    }
                }
            }
        } else {
            $company->keep_block = 0;
        }
        $company->save();

        if ($request->roles) {
            $x = CompanySchoolYear::where('company_id', $company->id)->delete();
            foreach ($request->roles as $key => $value) {
                $company_school_year = CompanySchoolYear::insert([
                    'school_academic_year_id' => $value,
                    'company_id' => $company->id
                ]);
            }

        }


        return redirect()->route('company.index')
            ->with('flash_message', 'Company successfully edited.');

    }

    //add locker column
    public function addLockerColumn(Request $request)
    {

//        dd($request->all());
        $block = block::find($request->block_id);
        $lockers = Locker::where('block_id', $request->block_id)->where('locker_order', '>', $request->last_order)->where('active', 1)->orderBy('locker_order', 'ASC')->get();

        $add_lcokers_after = Locker::where('block_id', $request->block_id)->where('locker_order', '<=', $request->last_order)->where('active', 1)->orderBy('locker_order', 'DESC')->take($block->block_height)->get()->reverse()->values();
        $last_locker_order = $request->last_order;
//        dd($lockers);
        if ($lockers->isEmpty()) {
            $last_locker_number = Locker::where('block_id', $request->block_id)->where('active', 1)->where('display', 1)->orderBy('locker_order', 'DESC')->first();
            $last_locker_number = $last_locker_number->locker_number;

            $increment = 0;
            foreach ($add_lcokers_after as $index => $locker) {

                $index++;
                $increment++;
                $updated_locker_number = $last_locker_number + $increment;
                if ($locker->active == 1 && $locker->display == 0) {
                    $updated_locker_number = null;
                    $increment--;
                }
                $new_locker = new Locker();
                $new_locker->block_id = $locker->block_id;
                $new_locker->company_id = $locker->company_id;
                $new_locker->locker_number = $updated_locker_number;
//                $new_locker->locker_number = $locker->locker_number;
                $new_locker->date_added = Carbon::now();
                $new_locker->locker_order = $last_locker_order + $index;
                $new_locker->active = $locker->active;
                $new_locker->display = $locker->display;
                $new_locker->save();


                // create manual locker for this 
                $this->createManulLocker($new_locker->id,$locker->block_id,$updated_locker_number);
            }


        } else {

            foreach ($lockers as $index => $locker) {
                $locker->locker_order = $locker->locker_order + $block->block_height;
                $locker->save();
            }

            foreach ($add_lcokers_after as $index => $locker) {

                $index++;

                $new_locker = new Locker();
                $new_locker->block_id = $locker->block_id;
                $new_locker->company_id = $locker->company_id;
//            $new_locker->locker_number = $updated_locker_number;
                $new_locker->locker_number = 0;
                $new_locker->date_added = Carbon::now();
                $new_locker->locker_order = $last_locker_order + $index;
                $new_locker->active = $locker->active;
                $new_locker->display = $locker->display;
                $new_locker->save();
                $this->createManulLocker($new_locker->id,$locker->block_id,0);
            }

        }

    }
    public function createManulLocker($lockerId,$blockId,$lockerNumber)
    {
        $schools = SchoolYear::where('to_date','>=',Carbon::now())->get();
        $block=Block::where('id',$blockId)->first();
        foreach ($schools as $key => $sy) {

            $blockcode=BlockGroupCode::where([['block_group_id',$block->block_group],['locker_number',$lockerNumber],['start_date',$sy->from_date]])->first();
            
            $endDate=explode(' ', $sy->to_date);
            $endDate1=$endDate[0].' 00:00:00';

            if($blockcode){
            $code = new ManualLockerCode();
                              $code->code = $blockcode->code;
                              $code->locker_id = $lockerId;
                              $code->block_id = $blockId;
                              $code->start = $sy->from_date;
                              $code->end = $endDate1;
                              $code->block_group_id  = $block->block_group;
                              $code->save();
                          }

        }
        return 1;
    }
    //locker order changed
    public function changeLockerOrder()
    {
        $blocks = Block::all();
        foreach ($blocks as $block) {
            $lockers = Locker::where('block_id',
                $block->id)->orderBy('locker_order', 'ASC')->get();

            foreach ($lockers as $index => $locker) {
                ++$index;
                $locker->locker_order = $index;
                $locker->save();

            }
        }

        echo "done!";


    }

    //company info
    public function info(Request $request)
    {
        $company = Company::query();
        $company->where('id', $request->company_id);

        return Datatables::of($company)
            ->make(true);

    }

      public function addLiveStatus(Request $request)
    {
        if($request->status==1)
        {
            $school_years = SchoolYear::where('status',0)->groupBy('from_date')->orderBy('id', 'desc')->first();
            if(!empty($school_years)){
            Golive::insert([
                    'company_id' => $request->company_id,
                    'school_year' => $school_years->id,
                    'active' => 1,
                ]);
            }else{
                return 2;
            }
        }else{
            $go_live = Golive::where('company_id',$request->company_id)->orderBy('id', 'desc')->first();
            $go_live->active=0;
            $go_live->save();
        }
       return 1;
    }
}