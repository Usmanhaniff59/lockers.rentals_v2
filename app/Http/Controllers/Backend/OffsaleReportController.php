<?php

namespace App\Http\Controllers\Backend;

use App\Franchisee_report;
use App\FranchiseeCompany;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\OffSaleReport;
use App\Franchisee;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;

class OffsaleReportController extends Controller
{
    public function index()
	    {
  	         $offsalereport = Franchisee::query();
            $franchisee_report_obj =  new Franchisee_report();
            $offsalereport = $franchisee_report_obj->franchisee_dropdown_record_permissions($offsalereport);

            $offsalereport = $offsalereport->get();
  	     return view('backend.pages.reports.offsale.OffsaleReport',compact('offsalereport'));
        }

    public function offsaleCount(Request $request){
      
        $query = $this->query_data($request);
        $count =  $query->count();

        return response()->json($count);
    }   
     
    public function getData(Request $request)
    {
        $query = $this->query_data($request);
        if($request->page == 0) {
            $data = $query->take(1000)->get();
        }else{
            $skip = $request->page * 1000;
            $data = $query->skip($skip)->take(1000)->get();
        }
        $data = [
            'data' => $data
        ];

        return response()->json($data);
    }

    public function OffsaleReport_show(Request $request){
           $offsalereport = $this->query_data($request);
          
            return Datatables::of($offsalereport)
            ->addColumn('month',function($offsalereport){
                if($offsalereport->month == '1')
                    return 'January';
                elseif($offsalereport->month == '2')
                    return 'February';

                elseif($offsalereport->month == '3')
                    return 'March';

                elseif($offsalereport->month == '4')
                    return 'April';

                elseif($offsalereport->month == '5')
                    return 'May';

                elseif($offsalereport->month == '6')
                    return 'June';

                elseif($offsalereport->month == '7')
                    return 'July';

                elseif($offsalereport->month == '8')
                    return 'August';

                elseif($offsalereport->month == '9')
                    return 'September';

                elseif($offsalereport->month == '10')
                    return 'October';

                elseif($offsalereport->month == '11')
                    return 'November';

                elseif($offsalereport->month == '12')
                    return 'December';

                else
                    return '';
            })->make(true);
        }
    public function query_data($request){


        $offsalereport = OffSaleReport::join('companies','companies.id','=','off_sale_reports.company_id')
        ->join('franchisees','franchisees.id','=','off_sale_reports.franchisee_id');

        if (isset($request->offsale_year) && !empty($request->offsale_year)){
            $offsalereport->where('off_sale_reports.year',$request->offsale_year);
        };

         if (isset($request->offsale_month) && !empty($request->offsale_month)){
            $offsalereport->where('off_sale_reports.month',$request->offsale_month);
        };

        if (isset($request->offsale_franchise) && !empty($request->offsale_franchise)){
            $offsalereport->where('off_sale_reports.franchisee_id',$request->offsale_franchise);
        };

        if (isset($request->offsale_company) && !empty($request->offsale_company)){
            $offsalereport->where('companies.name','LIKE',DB::raw("'%$request->offsale_company%'"));
        };

        $offsalereport_obj =  new OffSaleReport();
        $offsalereport =  $offsalereport_obj->off_sale_record_permissions($offsalereport);

        $offsalereport->select('franchisees.name as franchisee_id' ,'off_sale_reports.year' , 'off_sale_reports.month','companies.name as company_id','off_sale_reports.total_active_lockers','off_sale_reports.total_off_sale','off_sale_reports.maintenance_cleaning','off_sale_reports.maintenance_fixing','off_sale_reports.total_lockers','off_sale_reports.offsale_late','off_sale_reports.maintenance_late');
       // $offsalereport->get();

        return  $offsalereport;

    }
}