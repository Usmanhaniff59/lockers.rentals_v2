<?php

namespace App\Http\Controllers\Backend;

use App\CurrentOffSaleMaintenance;
use App\Franchisee;
use App\Franchisee_report;
use App\FranchiseeCompany;
use App\Locker;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;

class CurrentMaintenaceController extends Controller
{
    public function index()
    {


        Artisan::call('current:maintenance');
        $franchise_list = Franchisee::orderBy('name', 'ASC');

        $franchisee_report_obj =  new Franchisee_report();
        $franchise_list = $franchisee_report_obj->franchisee_dropdown_record_permissions($franchise_list);

        $franchise_list = $franchise_list->get();
        return view('backend.pages.reports.currentoffsale.currentoffsale', compact('franchise_list'));
    }

    public function currentlocker_show(Request $request)
    {

        $currentlocker = $this->query_data($request);
        return Datatables::of($currentlocker)->make(true);
    }

    public function offsaleCount(Request $request)
    {

        $query = $this->query_data($request);
        $count = $query->count();
        return response()->json($count);
    }

    public function getData(Request $request)
    {

        $query = $this->query_data($request);
        $query = $query->orderBy('lockers.locker_number', 'ASC');
        if ($request->page == 0) {

            $data = $query->take(1000)->get();
        } else {
            $skip = $request->page * 1000;
            $data = $query->skip($skip)->take(1000)->get();
        }
        $data = [
            'data' => $data
        ];

        return response()->json($data);
    }

    public function query_data($request)
    {


        $maintenances = CurrentOffSaleMaintenance::leftJoin('companies', 'companies.id', '=', 'current_off_sale_maintenances.company_id')
            ->leftJoin('franchisees', 'franchisees.id', '=', 'companies.territory_code')
            ->leftJoin('blocks', 'blocks.id', '=', 'current_off_sale_maintenances.block_id')
            ->leftJoin('lockers', 'lockers.id', '=', 'current_off_sale_maintenances.locker_id');

        if (isset($request->currentlocker_company) && !empty($request->currentlocker_company)) {
            $maintenances->where('companies.name', 'LIKE', DB::raw("'%$request->currentlocker_company%'"));
        }
        if (isset($request->currentlocker_block) && !empty($request->currentlocker_block)) {
            $maintenances->where('blocks.name', 'LIKE', DB::raw("'%$request->currentlocker_block%'"));
        }
        if (isset($request->currentlocker_locker) && !empty($request->currentlocker_locker)) {
            $maintenances->where('lockers.locker_number', 'LIKE', DB::raw("'%$request->currentlocker_locker%'"));
        }
        if (isset($request->currentlocker_franchise) && !empty($request->currentlocker_franchise)) {
            $maintenances->where('current_off_sale_maintenances.franchise_id', $request->currentlocker_franchise);
        }
        if (isset($request->currentlocker_type) && !empty($request->currentlocker_type)) {
            $maintenances->where('current_off_sale_maintenances.type', $request->currentlocker_type);
        }

        $off_sale_maintenance_obj = new CurrentOffSaleMaintenance();
        $maintenances = $off_sale_maintenance_obj->current_offsale_maintenance_record_permissions($maintenances);

        $maintenances->select('companies.name as company_name', 'franchisees.name as franchise_name', 'blocks.name as block_name', 'lockers.locker_number', 'current_off_sale_maintenances.type', DB::raw('DATE_FORMAT(current_off_sale_maintenances.start_date, "%M %D, %Y %H:%m") as start'));


        return $maintenances;


    }
}
