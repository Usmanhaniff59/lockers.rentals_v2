<?php

namespace App\Http\Controllers\Backend;

use App\Company;
use App\Franchisee;
use App\Franchisee_report;
use App\FranchiseeCompany;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

use App\User;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Session;
use Spatie\Permission\Traits\HasRoles;
 use Yajra\DataTables\DataTables;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth'])
            ->except('editprofile', 'saveprofile');
    }

 //load users crud page
    public function index()
    {
        $roles = Role::get();
        // $franchisees = Franchisee::orderBy('name', 'ASC')->get();

        $franchisees = Franchisee::query();

        // permission added
         $franchisee_report_obj =  new Franchisee_report();
            $franchisees = $franchisee_report_obj->franchisee_dropdown_record_permissions($franchisees);
            $franchisees=$franchisees->get(); 

        $companies = Company::query();

        //record permission on the basis of admin, franchise group & direct company assigned & user
        $company_obj = new Company();
        $companies = $company_obj->company_record_permissions($companies);
        $companies = $companies->get();

        return view('backend.pages.users.index', compact('roles', 'franchisees', 'companies'));
    }

    //load table data using datatable
    public function show_all(Request $request)
    {

        $users = User::query();

        //record permission on the basis of admin, franchise group & direct company assigned & user
        $user_obj = new User();
        $users = $user_obj->users_record_permissions($users);
        $users->orderBy('id', 'desc');

        //recycle bin decided
        if($request->deleted == 'false')
            $users->where('deleted', 0);
        else
            $users->where('deleted', 1);

        return Datatables::of($users)
            ->addColumn('full_name', function ($users) {
                return $users->full_name;
            })
            ->addColumn('roles', function ($users) {
                return $users->roles()->pluck('name')->implode(' ');
            })
            ->addColumn('action', function ($users) {
                $buttons = '';
                if ($users->deleted == 0) {
                    if (auth()->user()->can('Edit User')) {

                        $buttons .= '<a href="#edit" class="btn btn-primary btn-md adv_cust_mod_btn edit-btn pull-left" data-toggle="modal" style="margin-right: 3px;" data-href="#edit" data-id="' . $users->id . '" data-name="' . $users->name . '" data-email="' . $users->email . '" >Edit</a>';
                    }

                    if (auth()->user()->can('Delete User')) {
                        $buttons .= '<a href = "JavaScript:Void(0);" onclick = "deleteData(\'' . route('users.destroy', $users->id) . '\',\'' . csrf_token() . '\',\'\',user_table,\'DELETE\')" class="btn btn-danger" > Delete</a >';
                    }
                }
                return $buttons;
            })
            ->setRowClass(function ($users) {
                if ($users->deleted == 1) {
                    return 'alert-danger';
                }
            })
            ->make(true);
    }


    public function create()
    {
    }

    //create new user
    public function store(Request $request)
    {
        //validation
        $this->validate($request, [
            'name' => 'required|max:120',
            'email' => 'required|email|unique:users',
            'password' => 'nullable|min:6|confirmed',
            'roles'=>'required',
        ]);
          if (auth()->user()->can('Ticketing Permission')) {
            $this->validate($request,[
                'response_group_id' => 'required',
            ]);
            $response_group_id=$request->response_group_id;
          }else{
            $response_group_id=1;
            $request['response_group_id']=1;
          }

          if(!isset($request->password))
          {
            $request['password']='Db123(000';
          }

        //create new franchise group
        $franchisee_groups = $request->franchisee_groups;
         if ( auth()->user()->can('Assign Company To User')) {
        if ($request->franchisee_name) {
            $franchisee_id = Franchisee::create(['name' => $request->franchisee_name]);
            $franchisee_groups = $franchisee_id;

              // $FranchiseeCompany = FranchiseeCompany::where([['franchisee_id', '=', $franchisee_id->id],['company_id','=',$request->company_id]])->first();
            // if(empty($FranchiseeCompany)){
            //      FranchiseeCompany::create(['franchisee_id'=>$franchisee_id->id,'company_id'=>$request->company_id]);
            // }

        }
    }
        //create new user with hash password
        $request->merge(['password' => Hash::make($request['password'])]);
        $user = User::create($request->only('email', 'name', 'response_group_id', 'company_id', 'password'));

        //assign franchise groups to user
        if ($request->franchisee_groups || $request->franchisee_name) {
            $user->franchisee()->sync($franchisee_groups);
        }

        //assign roles to user
        $roles = $request['roles'];
        if (isset($roles)) {
            // foreach ($roles as $role) {
                $role_r = Role::where('id', '=', $roles)->firstOrFail();
                $user->assignRole($role_r);
            // }
        }

//        dd($user);
        $this->ticktetingUsers(User::findOrFail($user->id));

        return response()->json(['success' => 'Successfully added user']);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    //edit profile on dropdpown of backend
    public function editprofile()
    {
        $id = \Illuminate\Support\Facades\Auth::id();
        $user = User::findOrFail($id);
        $roles = Role::get();

        return view('backend.pages.users.editprofile', compact('user', 'roles'));
    }

    //save user edit profile
    public function saveprofile(Request $request)
    {
        $id = \Illuminate\Support\Facades\Auth::id();
        $user = User::findOrFail($id);
        $this->validate($request, [
            'name' => 'required|max:120',
            'email' => 'required|email|unique:users,email,' . $id,
            'phone_number' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'password' => 'required|min:6|confirmed'
        ]);

        //update user info
        $request->merge(['password' => Hash::make($request['password'])]);
        $input = $request->only(['name', 'email', 'phone_number', 'password']);
        $user->fill($input)->save();

        //update roles
        $roles = $request['roles'];
        if (isset($roles)) {
            $user->roles()->sync($roles);
        }

        return redirect('admin');
    }

    //update new user
     public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $this->validate($request, [
            'name' => 'required|max:120',
            'email' => 'required|email|unique:users,email,' . $id,
            
            'password' => 'nullable|min:6|confirmed',
         
        ]);
        if (auth()->user()->can('Ticketing Permission')) {
            $this->validate($request,[
                'response_group_id' => 'required',
            ]);
            // $response_group_id=$request->response_group_id;
          }else{
            // $response_group_id=$user;
            $request['response_group_id']=$user->response_group_id;
          }

          if(auth()->user()->can('Assign Role To User'))
          {
            $this->validate($request,[
                   'roles'=>'required',
            ]);
        }else{
             $request['roles']=$user->roles;
          // 
        }
        $franchisee_groups=$request->franchisee_groups;
        //create new franchise group
        if ( auth()->user()->can('Assign Company To User')) {
            if ($request->franchisee_name) {
                $franchisee_id = Franchisee::create(['name' => $request->franchisee_name]);
                $franchisee_groups = $franchisee_id;

                // assign franchise to company
                    // $FranchiseeCompany = FranchiseeCompany::where([['franchisee_id', '=', $franchisee_id->id],['company_id','=',$request->company_id]])->first();
                    //     if(empty($FranchiseeCompany)){
                    //          FranchiseeCompany::create(['franchisee_id'=>$franchisee_id->id,'company_id'=>$request->company_id]);
                    //     }

             }
            $array = ['name', 'email', 'company_id', 'phone_number', 'address_1', 'address_2', 'country', 'city', 'post_code', 'state', 'country'];
        }

//        elseif (auth()->user()->can('Assign Franchisee To User')) {
//            if ($request->franchisee_name) {
//                $franchisee_id = Franchisee::create(['name' => $request->franchisee_name]);
//                $franchisee_groups = $franchisee_id;
////                $request->merge(['franchisee_id' => $franchisee_id->id]);
//            }
//            $array = ['name', 'email', 'franchisee_id', 'phone_number', 'address_1', 'address_2', 'country', 'city', 'post_code', 'state', 'country'];
//        }

        //assign company to user
        elseif (auth()->user()->can('Assign Company To User')) {
            $array = ['name', 'email', 'company_id', 'phone_number', 'address_1', 'address_2', 'country', 'city', 'post_code', 'state', 'country'];
        } else {
            $array = ['name', 'email', 'phone_number', 'address_1', 'address_2', 'country', 'city', 'post_code', 'state', 'country'];
        }


        if (isset($request->response_group_id)) {
            array_push($array, 'response_group_id');
        }

        //update user password also
        if ($request['password'] && $request['password_confirmation']) {
            array_push($array, 'password');
            $request->merge(['password' => Hash::make($request['password'])]);
            $input = $request->only($array);
        } else {
            $input = $request->only($array);
        }

       if ($request->franchisee_groups || $request->franchisee_name) {
           $user->franchisee()->sync($franchisee_groups);
       }else{
           $user->franchisee()->detach();

       }

        $user->fill($input)->save();

        //tickting type assign
        $this->ticktetingUsers(User::findOrFail($id));

        //update roles
        if (auth()->user()->can('Assign Role To User')) {
            $roles = $request['roles'];
            if (isset($roles)) {
                $user->roles()->sync($roles);
            } else {
                $user->roles()->detach();
            }
        }

        return redirect()->route('users.index')->with('flash_message', 'User successfully edited.');
    }

    //ticketing roles
    public function ticktetingUsers($request)
    {

//        if ($_SERVER['HTTP_HOST'] == "dev.locker.rentals") {
            if (isset($request->response_group_id)) {
                $response_group_id = $request->response_group_id;
            } else {
                $response_group_id = 1;
            }
            $array = [
                'name' => $request->name,
                'email' => $request->email,
                'password' => $request->password,
                'response_group_id' => $response_group_id,
            ];
            // ::on('mysql_ticketing')
            $user = User::where('email', $request->email)->first();
            \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            
            if (!empty($user)) {
                $user = User::where('email', $request->email)->update($array);
            } else {
                $user = User::create($array);
            }
            \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
//        }
    }



    //delete user
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if ($user) {
            $user->deleted = 1;
            $user->save();
            return response()->json(['success' => 'User successfully deleted']);

        } else {
            return response()->json(['error' => 'User Not deleted!']);
        }

//        return redirect()->route('users.index')
//            ->with('flash_message',
//             'User successfully deleted.');
    }


    public function getuserroles(Request $request)
    {
        $user = User::findOrFail($request['user_id']);
        $roles = Role::get();

        //franchisee dropdown with record permission
        $franchisees = Franchisee::orderBy('name', 'ASC');
        $franchisee_report_obj = new Franchisee_report();
        $franchisees = $franchisee_report_obj->franchisee_dropdown_record_permissions($franchisees);
        $franchisees = $franchisees->get();

        //get assigned franchise groups
        $user_franchisees = $user->franchisee()->pluck('franchisee_id')->toArray();

        //get companies according to record permission
        $companies = Company::query();
        $company_obj = new Company();
        $companies = $company_obj->company_record_permissions($companies);
        $companies = $companies->get();

        $modal = view('backend.pages.users.editform', compact('user', 'roles', 'franchisees', 'companies', 'user_franchisees'))->render();

        return response()->json($modal);
    }

        public function companyFranchise(Request $request)
    {
        // $roles = Role::get();
        $companyId=$request->company_id;
        $franchisees=$this->franchiseesList($companyId);
          $modal = view('backend.pages.users.franchise', compact('franchisees'))->render();
          return response()->json($modal);
    }

    public function franchiseesList($company_id)
    {
           $franchisees = Franchisee::with(['franchisee_companies' =>function($query) use($company_id){
            return $query->where('company_id',$company_id);
        }])->orderBy('name', 'ASC')->get();
           return $franchisees;
    }
}
