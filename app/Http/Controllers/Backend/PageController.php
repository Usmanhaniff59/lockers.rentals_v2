<?php

namespace App\Http\Controllers\Backend;
use App\Help;
use App\Http\Controllers\Controller;

use App\Pages;
use Illuminate\Http\Request;

use Auth;
use Session;

class PageController extends Controller
{
    
    public function __construct()
    {
        $this->middleware(['auth', 'clearance'])
            ->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    public function index()
    {
        $pages = Pages::orderby('id', 'desc')->paginate(5);
        return view('backend.pages.page.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('backend.pages.page.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'=>'required|max:100',
            'body' =>'required',
            ]);

        $title = $request['title'];
        $body = $request['body'];
        $slug = self::slugify($title);

        $page = new Pages();
        $page->title = $title;
        $page->body = $body;
        $page->slug = $slug;
        $page->save();

        return redirect()->route('pages.index')
            ->with('flash_message', 'Page,
             '. $page->title.' created');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//        $page = Pages::findOrFail($id);
//
//        return view ('backend.pages.page.show', compact('page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//        $page = Pages::findOrFail($id);
//
//        return view('backend.pages.page.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = $request['id'];
        $this->validate($request, [
            'title'=>'required|max:100',
            'body'=>'required',
        ]);

        $page = Pages::findOrFail($id);
        $page->title = $request->input('title');
        $page->body = $request->input('body');
        $page->save();

        return redirect()->route('pages.index',
            $page->id)->with('flash_message', 
            'Article, '. $page->title.' updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Pages::findOrFail($id);
        $page->delete();

        return redirect()->route('pages.index')
            ->with('flash_message',
             'Article successfully deleted');
    }
}
