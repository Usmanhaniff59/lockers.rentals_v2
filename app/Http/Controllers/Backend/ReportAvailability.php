<?php

namespace App\Http\Controllers\Backend;

use App\Block;
use App\Company;
use App\Exports\reports\LockerAvailability;
use App\FranchiseeCompany;
use App\Locker;
use App\LockerOffSale;
use App\Maintenance;
use App\Rate;
use App\Sale;
use App\ManualLockerCode;
use App\SchoolAcademicYear;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;
use App\LocationGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class ReportAvailability extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locationGroups = LocationGroup::get();
        $rates = Rate::orderBy('order', 'ASC')->get();
        $school_academic_year = SchoolAcademicYear::orderBy('order_number', 'asc')->get();

        return view('backend.pages.reports.availability.index', compact('locationGroups', 'rates', 'school_academic_year'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */

    public function show_all(Request $request)
    {
        $query = $this->availability_query($request);
        $locker = $query[0];
        $date_passed = $query[1];
        $this_month_start_date = $query[2];
        $this_month_end_date = $query[3];

        return $this->availability_dataTable($locker, $date_passed, $this_month_start_date, $this_month_end_date);

    }

    public function availability_query($request)
    {

        $locker = Locker::query();
        $locker->select('lockers.*');


        $search_input = $request->search_input;

        if (date("m") < 7) {
            $date_passed = date('Y-m-d 00:00:00');
        } else {
            $date_passed = date("Y-09-01 00:00:00");

        }

        $this_month_start_date = date('Y-m-d 00:00:00');
        $this_month_end_date = date('Y-m-d 23:59:59');

        if (isset($request->search_start_date) && !empty($request->search_start_date)) {
            $date_passed = Carbon::createFromFormat('m/d/Y',$request->search_start_date)->toDateString();

            $this_month_start_date = date('Y-m-d 00:00:00', strtotime($request->search_start_date));
            $this_month_end_date = date('Y-m-d 23:59:59', strtotime($request->search_start_date));
        }

        $locker->LeftJoin('companies', 'companies.id', '=', 'lockers.company_id');
        $locker->LeftJoin('blocks', 'blocks.id', '=', 'lockers.block_id');
        $locker->whereNotNull('lockers.company_id');
        $locker->whereNotNull('lockers.block_id');
        $locker->where('lockers.active', 1);
        $locker->where('lockers.display', 1);
        $locker->where('blocks.active', 1);
        $locker->where('companies.active', 1);

   // if(auth()->user()->hasRole('Franchisee'))
   //      {
   //          $franchisee_ids=DB::table('franchisee_user')->where('user_id',auth()->user()->id)->pluck('franchisee_id');
   //          $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id', $franchisee_ids)->pluck('company_id')->toArray();
   //          $locker->whereIn('companies.id', $franchisee_companies);
   //      }else if(auth()->user()->hasRole('Company'))
   //      {
   //           $locker->where('companies.id',auth()->user()->company_id);
   //      }
//        $locker->orderBy('blocks.order_list','ASC');
//        $locker->orderBy('lockers.locker_order','ASC');
//        $locker->orderBy('lockers.locker_number','ASC');


        if (isset($search_input) && !empty($search_input)) {
            $locker->where(function ($query) use ($search_input) {
                $string = explode(' ', $search_input);
                if (isset($string[0])) {
                    $search_input = $string[0];
                } else {
                    $search_input = $search_input;
                }
                if (isset($string[1])) {
                    $search_input = $string[1];
                } else {
                    $search_input = $search_input;
                }
                if (isset($string[2])) {
                    $search_input = $string[2];
                } else {
                    $search_input = $search_input;
                }
                $query->orWhere('companies.name', 'LIKE', '%' . $search_input . '%')
                    ->orWhere('blocks.name', 'LIKE', '%' . $search_input . '%')
                    ->orWhere('lockers.locker_number', 'LIKE', '%' . $search_input . '%');
            });
        }

        if (isset($request->search_company) && !empty($request->search_company)) {
            $locker->where('companies.name', 'LIKE', '%' . $request->search_company . '%');
        }

        if (isset($request->search_block) && !empty($request->search_block)) {
            $locker->where('blocks.name', 'LIKE', '%' . $request->search_block . '%');
        }

        $locker_obj =  new Locker();
        $locker = $locker_obj->locker_available_and_booked_report_record_permissions($locker);



        $locker->select('lockers.id', 'companies.name as company_name', 'blocks.name as block', 'lockers.locker_number');

         return [$locker, $date_passed, $this_month_start_date, $this_month_end_date];

    }

    public function availability_dataTable($locker, $date_passed, $this_month_start_date, $this_month_end_date)
    {


        return Datatables::of($locker, $date_passed, $this_month_start_date, $this_month_end_date)
            ->addColumn('manual_code', function ($locker) use ($date_passed) {

                $manual_code = ManualLockerCode::where('locker_id', $locker->id)
                    ->where(function ($query) use ($date_passed) {
                        $query->whereDate('start', '<=', $date_passed)
                            ->whereDate('end', '>=', $date_passed);
                    })
//
                    ->first();
                if (empty($manual_code)) {
                    return "";
                } else {

                    return substr(sprintf("%04d", $manual_code->code), -4);
                }


            })
            ->addColumn('status', function ($locker) use ($this_month_start_date, $this_month_end_date) {


                $lockerStatus = Sale::Where('locker_id', $locker->id)
                   ->where(function ($query) use ($this_month_start_date,$this_month_end_date) {
                        $query->whereDate('start', '<=', $this_month_end_date)
                            ->whereDate('end', '>=', $this_month_start_date);
                    })
//
                    ->where(function ($status) {
                        $status->where('status', 'b')
                            ->orWhere('status', 'r');
                    })
                    ->orderBy('status', 'ASC')
                    ->first();

                $is_off_sale = LockerOffSale::where('locker_id', $locker->id)->where('active', 1)->first();

                $maintenances = Maintenance::
//                where('to', '>=', $this_month_start_date)->Where('from', '<=', $this_month_end_date)
//                    ->
                where('locker_id', $locker->id)
                    ->where('active', 1)
                    ->first();

                if (!empty($maintenances)) {
                    return "Maintenance";

                } elseif (!empty($is_off_sale)) {
                    return "Off Sale";

                } elseif (empty($lockerStatus->status)) {
                    return "Available";
                } else if ($lockerStatus->status == 'b') {
                    return "Booked";

                } else if ($lockerStatus->status == 'r') {
                    return "Reserved";
                }

            })->make(true);


    }


    public function getavailabilityreportcount(Request $request)
    {

        $query = $this->availability_query($request);
        $locker = $query[0];
        $count = $locker->count();

//        $count = 5;
        return response()->json($count);
    }

    public function getavailabilityreport(Request $request)
    {

        $query = $this->availability_query($request);
        $locker = $query[0]->orderBy('lockers.locker_number', 'ASC');

         if ($request->page == 0) {
            $data = $locker->take(1000)->get();
        } else {
            $skip = $request->page * 1000;
            $data = $locker->skip($skip)->take(1000)
                ->get();
        }

        $date_passed = $query[1];
        $this_month_start_date = $query[2];
        $this_month_end_date = $query[3];

         foreach ($data as $locker) {

            $manual_code = ManualLockerCode::where('locker_id', $locker->id)->where('start', '<=', date('Y-m-d 00:00:00', strtotime($date_passed)))
                ->where('end', '>=', date('Y-m-d 00:00:00', strtotime($date_passed)))->first();

            if (empty($manual_code)) {
                $code = "";
            } else {

                $code = substr(sprintf("%04d", $manual_code->code), -4);
            }
            $locker->manual_code = $code;

            //status
            $lockerStatus = Sale::Where('locker_id', $locker->id)
                ->WhereDate('start', '<=', $this_month_end_date)
                ->WhereDate('end', '>=', $this_month_start_date)
                ->where(function ($status) {
                    $status->where('status', 'b')
                        ->orWhere('status', 'r');
                })
                ->orderBy('status', 'ASC')
                ->first();

            $is_off_sale = LockerOffSale::where('locker_id', $locker->id)->where('active', 1)->first();

            $maintenances = Maintenance::where('to', '>=', $this_month_start_date)->Where('from', '<=', $this_month_end_date)
                ->where('locker_id', $locker->id)
                ->where('active', 1)
                ->first();

            if (!empty($maintenances)) {
                $status = "Maintenance";

            } elseif (!empty($is_off_sale)) {
                $status = "Off Sale";

            } elseif (empty($lockerStatus->status)) {
                $status = "Available";
            } else if ($lockerStatus->status == 'b') {
                $status = "Booked";

            } else if ($lockerStatus->status == 'r') {
                $status = "Reserved";
            }
            $locker->status = $status;

        }

        $data = [
            'data' => $data
        ];

        return response()->json($data);
    }

}
