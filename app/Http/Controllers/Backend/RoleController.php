<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use App\Sale;
use App\SalesAudit;
use Illuminate\Http\Request;

use Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Session;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::orderBy('id','ASC')->get();

        $permissions = Permission::all();

        return view('backend.pages.roles.index',compact('roles','permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::all();

        return view('backend.pages.roles.create', ['permissions'=>$permissions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name'=>'required|unique:roles|max:20',
            'permissions' =>'required',
            ]
        );

        $name = $request['name'];

        $role = new Role();
        $role->name = $name;
        $role->save();

        $permissions = $request['permissions'];
        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail();
            $role = Role::where('name', '=', $name)->first();
            $role->givePermissionTo($p);
        }

        return redirect()->route('roles.index')
            ->with('flash_message',
             'Role'. $role->name.' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('roles');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::findOrFail($id);
        $selected_role = Role::findOrFail($id)->permissions->pluck('id')->toArray();
        $permissions = Permission::all();

        return view('backend.pages.roles.edit', compact('role', 'permissions','selected_role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd($request->all());

        $role = Role::findOrFail($id);
        $this->validate($request, [
            'name'=>'required|max:20|unique:roles,name,'.$id,
            'permissions' =>'required',
        ]);
        $role_name =Role::find($id);
        $role_name->name =$request['name'];
        $role_name->save();

        $input = $request->except(['permissions']);
        $permissions = $request['permissions'];
        $role->fill($input)->save();
        $p_all = Permission::get()->toArray();
//        $role->revokePermissionTo($p_all);
        foreach ($p_all as $p) {

            $role->revokePermissionTo($p);

        }
//            dd($p_all);
        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail(); //Get corresponding form permission in db
            $role->givePermissionTo($p);  
        }

        return redirect()->route('roles.index')
            ->with('flash_message',
             'Role'. $role->name.' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();

        return redirect()->route('roles.index')
            ->with('flash_message',
             'Role deleted!');
    }

    public function getroles(Request $request)
    {
        $id = $request['id'];
        $role = Role::findOrFail($id);
        $selected_role = Role::findOrFail($id)->permissions->pluck('id')->toArray();
        $permissions = Permission::all();
        $modal = view('backend.pages.roles.editform',compact('role' ,'selected_role','permissions'))->render();
        return response()->json($modal);
    }

    public function get_sales_count(Request $request)
    {
//        dd('hh');
        $sales = Sale::where(function ($status) {
            $status->where('status', 'b')
                ->orWhere('status', 'r');
        })
            ->count();
//            $sales = 1000;
            return response()->json($sales);
    }
    public function add_in_sales_autdit(Request $request)
    {
        $query = Sale::where(function ($status) {
            $status->where('status', 'b')
                ->orWhere('status', 'r');
        });
        if($request->page == 0) {
            $data = $query->take(1000)->get();
        }else{
            $skip = $request->page * 1000;

            $data = $query->skip($skip)->take(1000)->get();
        }

        foreach ($data as $sale){

            $sale_audit = new SalesAudit();
            $sale_audit->sale_id = $sale->id;
            $sale_audit->user_id = $sale->user_id;
            $sale_audit->booking_id = $sale->booking_id;
            $sale_audit->number = $sale->number;
            $sale_audit->net_pass = $sale->net_pass;
            $sale_audit->company_id = $sale->company_id;
            $sale_audit->locker_id = $sale->locker_id;
            $sale_audit->block_id = $sale->block_id;
            $sale_audit->block_name = $sale->block_name;
            $sale_audit->start = $sale->start;
            $sale_audit->end = $sale->end;
            $sale_audit->price = $sale->price;
            $sale_audit->email = $sale->email;
            $sale_audit->status = $sale->status;
            $sale_audit->street = $sale->street;
            $sale_audit->city = $sale->city;
            $sale_audit->post_code = $sale->post_code;
            $sale_audit->country = $sale->country;
            $sale_audit->child_first_name = $sale->child_first_name;
            $sale_audit->child_surname = $sale->child_surname;
            $sale_audit->child_email = $sale->child_email;
            $sale_audit->school_year_id = $sale->school_year_id;
            $sale_audit->school_academic_year_id = $sale->school_academic_year_id;
            $sale_audit->type = 'add';
            $sale_audit->save();

        }



        return response()->json('success');
    }
}
