<?php

namespace App\Http\Controllers\Backend;

use App\Company;
use App\Document;
use Carbon\Carbon;
use App\User;
use Illuminate\Http\Request;
use App\DocumentTypes;
use NcJoes\OfficeConverter\OfficeConverter;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;

class DocumentController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $document_types = DocumentTypes::get();  
        return view('backend.documents.index' , ['document_types' => $document_types]);

    }

    public function create()
    {
    //   
    }

 
    public function upload(Request $request)
    {
        $validatedData = $request->validate([
            'document_type' => ['required'],
            'file' => ['required'],
            'title' => ['required']
            
        ]);
        $input=$request->all();
        $user = Auth::user();
        /*$company = $request->company;
        $company_idd = Company::where('name', $company)->first();
        if(!$company_idd){
            $company_idd=0;
        }
        $company_idd->id; */
        $company_idd=0;
       
        if(isset($request->file))
        {
            // $time = time();
            // $file = $request->file('file');
            // $name = $file->getClientOriginalName();
            // $attachments = $time.'.'.$file->getClientOriginalExtension();
            // $destinationPath = public_path('/uploads');
            // $file->move($destinationPath, $attachments);
            // $input['file']= $destinationPath.'/'.$time.'.pdf';
            // $converter = new OfficeConverter($destinationPath.'/'.$attachments);
            // $converter->convertTo($input['file']);
            
           

            $time = time();
            $file = $request->file('file');
            $name = $file->getClientOriginalName();
            $pdf=explode(".",$name);
            $attachments = $time.'.'.$file->getClientOriginalExtension();
            $destinationPath = public_path('documentUploads');
            //dd($attachments);
            $file->move($destinationPath, $attachments);
            // $input['file']= $time.'.pdf';
            // $converter = new OfficeConverter($destinationPath.'/'.$attachments);
            // $converter->convertTo($input['file']);
            // $input['pdf']  = $input['file'];
          
        }
        Document::create([
            'title' => $request->title,
            'type_id' => $request->document_type,
            'uploaded_by' =>   $user->id,
            'company_id' => $company_idd,
            'original_path' => $attachments,
            'converted_path' =>  $input["file"],

        ]);
        return response()->json($validatedData);
    }
    

    public function show(Request $request)
    {
       $documents = Document::find($request->id);

        return response()->json($documents);
    }



   
    public function edit($id)
    {
        //
    }

    public function update(Request $request)
    {   $input=$request->all();
       
        $user = Auth::user();
        $document = Document::where('id', $request->id)->first();
       
        if(isset($request->edit_file))
        {
            $time = time();
            $file = $request->file('edit_file');
            if($file){
                $name = $file->getClientOriginalName();
                $pdf=explode(".",$name);
                $attachments = $time.'.'.$file->getClientOriginalExtension();
                $destinationPath = public_path('documentUploads');
                $file->move($destinationPath, $attachments);
                $input['edit_file']= $time.'.pdf';
                $converter = new OfficeConverter($destinationPath.'/'.$attachments);
                $converter->convertTo($input['edit_file']);
                $input['pdf']  = $input['edit_file'];
                $document->original_path = $attachments;
                $document->converted_path = $input['edit_file'] ;
            }
          
        }
        $document->uploaded_by =$user->id;
        $document->title = $request->edit_title;
        $document->type_id = $request->edit_type;
      
        $document->save();
      
        return response()->json("success");
    }

    public function destroy($id)
    {
    }
    public function download($id){
        $file = Document::find($id);
        // dd($file);
        //$destinationPath = public_path('\documentUploads');
        $destinationPath = public_path('documentUploads/'.$file->converted_path);
        // dd($destinationPath);
        return response()->download($destinationPath);
    }

    public function show_all(){
        $company_documents = Document::where('company_id', '0')->orderBy('id', 'desc')->get();
        return Datatables::of($company_documents)->addColumn('details', function ($company_documents) {
            $date= $company_documents->updated_at->format('d M, Y H:i');
                // return $date;
            $name = '<b style="text-transform:capitalize;">'.$company_documents->user->name. ' ' . $company_documents->user->surname.'</b><br>'. $date ; 
            return $name;
        })->addColumn('type_id', function ($company_documents) {
            $type = $company_documents->document_type->name;
            return $type;
        })->addColumn('action', function ($company_documents) {
            $buttons ='';
            $buttons = '<a href="#edit_document" class="btn btn-primary btn-md adv_cust_mod_btn pull-left edit-document" data-toggle="modal" onclick="saveDocumentId('.$company_documents->id.')" data-id="'. $company_documents->id .'"  style="margin-right: 5px;">Edit</a>';
            $buttons.='<a href="company/documents/download/'. $company_documents->id .'" class="btn btn-primary" style="background:#feb965;border-color:#feb965" > Download</a >';
             $buttons.='<a href="javascript:void(0)" data-id="'.$company_documents->id.'" class="btn btn-danger delete-doc" > Delete</a >';
            return $buttons;


        })->setRowClass(function ($company_documents) {
             return 'alert-success';
        
        })->rawColumns(['details','action']) ->make(true);
    }

    public function show_deleted(){
    }

}