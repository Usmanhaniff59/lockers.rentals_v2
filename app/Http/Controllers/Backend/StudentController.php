<?php

namespace App\Http\Controllers\Backend;

use App\Block;
use App\BlockYears;
use App\Company;
use App\CompanySchoolYear;
use App\FranchiseeCompany;
use App\CronSchedule;
use App\Email;
use App\FrontendPlaceholder;
use App\LocationGroup;
use App\Locker;
use App\LockerMove;
use App\LockerOffSale;
use App\Payment;
use App\SalesAudit;
use App\SchoolAcademicYear;
use App\SchoolYear;
use App\User;
use App\Franchisee;
use App\paymentConfig;
use App\Sale;
use App\VoucherCodeUser;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Transaction;
use App\EmailTemplate;
use App\RefundTransaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Events\TransactionBeginning;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Spatie\Permission\Traits\HasRoles;
use Yajra\DataTables\DataTables;

class StudentController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $franchisee_companies = FranchiseeCompany::where('franchisee_id',Auth::user()->franchisee_id)->pluck('company_id')->toArray();
        Session::forget('selectedrows');
        return view('backend.pages.students.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function show_all(Request $request){
        $payments = Sale::query();
        $date = date('Y-m-d',strtotime($request->search_date));
        $now = Carbon::now();
        $user = Auth::user();
        $franchisees = $user->franchisee()->pluck('franchisee_id')->toArray();
      
            $payments->join('companies','companies.id','=','sales.company_id')
                ->join('blocks','blocks.id','=','sales.block_id')
                 ->join('users','users.id','=','sales.user_id')
                ->join('school_academic_years','school_academic_years.id','=','sales.school_academic_year_id');
                // permission setting for the student start

                 $search_input = $request->search_input;
                    Session::put('payments-search',$search_input);
                  if(auth()->user()->can('All Records')) {
                   
                }elseif (Auth::user()->company_id && $franchisees) {
                  $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id',$franchisees)->pluck('company_id')->toArray();
                   array_push($franchisee_companies, auth()->user()->company_id);
                        $payments->Where(function ($query) use ($franchisee_companies) {
                            $query->WhereIn('companies.id', $franchisee_companies);
                                // ->orWhere('id', Auth::user()->company_id);
                        });
                }elseif($franchisees ) {

                        $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id',$franchisees)->pluck('company_id')->toArray();

                        $payments->whereIn('companies.id',$franchisee_companies);
                    }
                    elseif( Auth::user()->company_id ){

                        $payments->where('companies.id',Auth::user()->company_id);
                    }
                // permission setting end for the student
            if (isset($request->search_bookingId) || isset($request->search_block) || isset($request->search_company) || isset($request->search_locker_number) ){

            }
            else{
                $payments->whereYear('sales.start', $now->year);
            }
            if (isset($request->search_date) && !empty($request->search_date )){

                $payments->where(function ($query) use ($date) {
                    $query->where('sales.end', '>=', $date)
                        ->Where('sales.start', '<=', $date);
                });
            }
            if (isset($request->search_company) && !empty($request->search_company)){
                $payments->where('companies.name','LIKE',DB::raw("'%$request->search_company%'"));
            }
            if (isset($request->search_block) && !empty($request->search_block)){
                $payments->where('blocks.name','LIKE',DB::raw("'%$request->search_block%'"));
            }
            if (isset($request->search_locker_number) && !empty($request->search_locker_number)){
                $payments->where('sales.number','LIKE',DB::raw("'%$request->search_locker_number%'"));
            }
            if (isset($request->search_bookingId) && !empty($request->search_bookingId)){
                $payments->where('sales.booking_id',$request->search_bookingId);
            }

            if (isset($request->search_email) && !empty($request->search_email)){
                $payments->where('users.email','LIKE',DB::raw("'%$request->search_email%'"));
            }

            if (isset($request->search_school_year) && !empty($request->search_school_year)){
                $payments->where('school_academic_years.school_year','LIKE',DB::raw("'%$request->search_school_year%'"));
            }

            if (isset($search_input) && !empty($search_input)){
                $payments->where(function ($query) use($search_input) {
                    $string=explode(' ',$search_input);
                    if (isset($string[0])){
                        $search_input=$string[0];
                    }else{
                        $search_input=$search_input;
                    }
                    if (isset($string[1])){
                        $search_input=$string[1];
                    }else{
                        $search_input=$search_input;
                    }
                    if (isset($string[2])){
                        $search_input=$string[2];
                    }else{
                        $search_input=$search_input;
                    }
                    if($search_input == "booked"){
                        $query->orWhere('sales.status','=' ,'b');
                    }
                    $query->orWhere('users.name','LIKE',DB::raw("'%$search_input%'"))
                        ->orWhere('users.surname','LIKE',DB::raw("'%$search_input%'"))
                        ->orWhere('users.email','LIKE',DB::raw("'%$search_input%'"))
                        ->orWhere('users.username', 'LIKE', DB::raw("'%$search_input%'"));
                });
            }

            $payments->select('sales.*');
            $payments->where('sales.status','b');
            $payments->orderBy('sales.number','ASC');
        

        return Datatables::of($payments)

            ->addColumn('checkbox', function ($payments) {
                 
                $selectedrows = Session::get('selectedrows');
                if(!isset($selectedrows)){
                    $selectedrows = [];
                }
                $exists = in_array ($payments->id,$selectedrows);
                if($exists== true) {
                    return '<div class="checkbox">
                                <label><input type="checkbox" value="" class="selectedrowproject project_checked"  id="project' . $payments->id . '" data-company_id="' . $payments->company_id . '" checked>
                                    <span class="cr">
                                        <i class="cr-icon fa fa-check">
                                        </i>
                                    </span>
                                </label>
                            </div>';
                }else{
                    return '<div class="checkbox">
                                <label><input type="checkbox" value="" class="selectedrowproject project_checked"  id="project' . $payments->id . '" data-company_id="' . $payments->company_id . '">
                                    <span class="cr">
                                        <i class="cr-icon fa fa-check">
                                        </i>
                                    </span>
                                </label>
                            </div>';
                }
            })
            ->addColumn('created', function ($payments) {
//                $sale = Sale::where('booking_id',$payments->booking_id)->first();

                return Carbon::parse($payments->start)->format('d M, Y').'  -  ' .Carbon::parse($payments->end)->format('d M, Y');

            })

            ->addColumn('company', function ($payments) {
                $company = null;

                if(isset($payments->company_id)) {
                    $company_data = Company::where('id', $payments->company_id)->select('name')->first();

                    if($company_data)
                        $company =$company_data->name;
                }
                return $company;
            })
            ->addColumn('block', function ($payments) {
                $block = null;

                if(isset($payments->block_id)) {
                    $block_data = Block::where('id', $payments->block_id)->select('name')->first();

                    if($block_data)
                        $block =$block_data->name;
                }
                return $block;
            })
            ->addColumn('status', function ($payments) {
                if($payments->status == "b"){
                    return "Booked";
                } else if($payments->status == "r"){
                    return "Reserved";
                } else {
                    return "Lost";
                }
            })
            ->addColumn('year_group', function ($payments) {
                $year = SchoolAcademicYear::where('id', $payments->school_academic_year_id)->first();
                if(isset($year->school_year)){
                    return $year->school_year;
                } else {
                    return "";
                }
            })
            ->addColumn('year_booked', function ($payments) {
                $year = SchoolYear::where('id', $payments->school_year_id)->first();
                if(isset($year->title)){
                    return $year->title;
                } else {
                    return "";
                }
            })
            ->addColumn('action', function ($payments) {
                $buttons ='';
                $name = '';
                if(isset($payments->user_id)) {
                    if($payments->status == "b")
                        $buttons .= '<a href="#viewuser" class="btn btn-info pull-left button-style view-user" data-toggle="modal" style="margin-right: 3px;" data-href="#edit" data-id="' . $payments->user_id . '" >View User</a>';
                }
                if(auth()->user()->can('Resend Email')) {

                        if ($payments->confirmation_sent == 1)
                            if($payments->status == "b")
                                $buttons .= '<a href="#." id="resend' . $payments->booking_id . '" class="btn btn-success pull-left button-style " onclick="resend_confirm(\'' . $payments->booking_id . '\');row_highlight(\'resend' . $payments->booking_id . '\')" >Resend Email</a>';
                }
                if(auth()->user()->can('Move Booking')) {
                    $buttons .= '<a href="#move_booking" class="btn btn-info pull-left button-style view-user" data-toggle="modal" style="margin-right: 3px;" data-href="#move_booking" onclick="moveBooking(\''.$payments->id.'\',\''.$payments->booking_id.'\')">Move Booking</a>';
                }
                return $buttons;
            })
            ->setRowClass(function ($payments) {
                    if($payments->checked ==  1){
                        return 'alert-primary';
                    }
            })
            ->rawColumns(['checkbox','action'])->make(true);


    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
//        $id;
//        dd($request->all());


        if(isset($request->id)){
            $id = $request->id;

        } else {

            $id = $request->session()->get('booking_id_payment');

        }
        $record_access =false;
        $payments = Payment::leftJoin('sales', function($join)
        {
            $join->on('sales.booking_id', '=', 'payments.booking_id');
        })
            ->join('companies','companies.id','=','sales.company_id')
            ->join('users','users.id','=','payments.user_id');

        if(auth()->user()->can('All Records')) {
            $record_access = true;

        }elseif( Auth::user()->company_id && Auth::user()->franchisee_id) {
            $franchisee_companies = FranchiseeCompany::where('franchisee_id',Auth::user()->franchisee_id)->pluck('company_id')->toArray();

            $payments->Where(function ($query) use ($franchisee_companies)  {
                $query->whereIn('sales.company_id', $franchisee_companies)
                    ->orWhere('sales.company_id',Auth::user()->company_id)
                    ->orWhere('payments.user_id', Auth::user()->id);
            })->where('payments.booking_id',$id)->first();

            if(!empty($payments))
                $record_access =true;

        }
        elseif( Auth::user()->franchisee_id ) {
            $franchisee_companies = FranchiseeCompany::where('franchisee_id',Auth::user()->franchisee_id)->pluck('company_id')->toArray();

            $payments->Where(function ($query) use ($franchisee_companies)  {
                $query->whereIn('sales.company_id', $franchisee_companies)
                    ->orWhere('payments.user_id', Auth::user()->id);
            })->where('payments.booking_id',$id)->first();
            if(!empty($payments))
                $record_access =true;
        }

        elseif( Auth::user()->company_id ){
            $payments->Where(function ($query) {
                $query->Where('payments.user_id', Auth::user()->id)
                    ->orWhere('sales.company_id', Auth::user()->company_id);
            })->where('payments.booking_id',$id)->first();
            if(!empty($payments))
                $record_access =true;

        }else{
            $payments->where('payments.user_id',Auth::user()->id)->where('payments.booking_id',$id)->first();
            if(!empty($payments))
                $record_access =true;
        }

        if($record_access == true) {

            $transactions = $lockerInfo = Sale::where('booking_id', $id)->get();
            $charities = $lockerInfo = Transaction::where('booking_id', $id)
                ->where('transactions.type', '03')
                ->get();

            return view('backend.pages.students.edit', compact('transactions', 'charities'));
        }else{

            return redirect()->back()->with('error_message','You don\'t have access of this record!');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getuserroles(Request $request)
    {
        $user = User::findOrFail($request['user_id']);
        $roles = Role::get();
        $franchisees =  Franchisee::orderBy('name','ASC')->get();
        if(auth()->user()->can('All Records')) {
            $companies =  Company::all();
        }
        elseif( Auth::user()->company_id && Auth::user()->franchisee_id) {

            $franchisee_companies = FranchiseeCompany::where('franchisee_id',Auth::user()->franchisee_id)->pluck('company_id')->toArray();

            $companies = Company::whereIn('id',$franchisee_companies)->orwhere('id',Auth::user()->company_id)->get();

        }
        elseif( Auth::user()->franchisee_id ) {

            $franchisee_companies = FranchiseeCompany::where('franchisee_id',Auth::user()->franchisee_id)->pluck('company_id')->toArray();
            $companies = Company::whereIn('id',$franchisee_companies)->get();
        }
        elseif( Auth::user()->company_id ){

            $companies = Company::where('id',Auth::user()->company_id)->get();

        }else{

            $companies = Company::where('id',null)->get();

        }

        $modal = view('backend.pages.students.viewuser',compact('user' ,'roles','franchisees','companies'))->render();
        return response()->json($modal);
    }

    public function changeSalePrice(Request $request)
    {
        Sale::where('id',$request['sale_id'])->update([
            'price'=>$request['price']
        ]);
        $sale = Sale::where('id',$request['sale_id'])->first();
        $reduce_sale_price = abs($request['original_price'] - $request['price']);

        $transaction = new Transaction();
        $transaction->booking_id = $request['booking_id'];
        $transaction->user_id = Auth::user()->id;
        $transaction->currency = $sale->company->currency;
        $transaction->amount = $reduce_sale_price;
        $transaction->type = '05';
        $transaction->fraud_score = 0;
        $transaction->notes = $request['notes'];
        $transaction->save();

        return redirect('admin/payment/' . $request['booking_id'] . '/edit')
            ->with('flash_message',
                'Price successfully edited.');

    }

    public function refundPayment(Request $request)
    {
        $request->session()->put('booking_id_amount', $request['booking_id']);
        $allPaidTransactions = Transaction::where('booking_id', $request['booking_id'])->where(function($query) {
            return $query->where('type', '01')
                ->orWhere('type', '02')
                ->orWhere('type', '03');
        })->where('status', '0000')->get();
        $Authorization = paymentConfig::integrationKey();

        $refunded = 0;

        $amount_to_refund = floatval($request['refund_amount']) * 100;

        foreach ($allPaidTransactions as  $paidTransaction) {

            if(empty($paidTransaction->transaction_id)){

                $this->saveTransaction($paidTransaction->booking_id, $paidTransaction->payment_id, "", "", "", 0, 1010, "Refund Manually, Old Transaction", 0, "Refund Manually, Old Transaction");
                return redirect()->back()
                    ->with('error_message', "Refund Manually, Old Transaction");
                break;
            }

            $retrieve_transaction_url = \Config::get('app.retrieve_transaction_url');
            $retrieve_transaction_url = str_replace("<transactionId>",$paidTransaction->transaction_id,"$retrieve_transaction_url");
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $retrieve_transaction_url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Basic $Authorization",
                    "Cache-Control: no-cache"
                ),
            ));

            $retrieve_transaction_response = curl_exec($curl);
            $retrieve_transaction_response = json_decode($retrieve_transaction_response);
            $err = curl_error($curl);
            curl_close($curl);

            $amounts = get_object_vars($retrieve_transaction_response->amount);
            $totalAmount = $amounts['totalAmount'];
            $previous_refunds = RefundTransaction::where('transaction_id', $paidTransaction->transaction_id)->where('status', 1)->sum('amount');
            $previous_refunds = $previous_refunds * 100;
            $available = $totalAmount - $previous_refunds;

            if($amount_to_refund == 0){

                return redirect()->back()
                    ->with('flash_message', "Successfully Refunded");
                break;
            }

            if($available != 0){
                if($amount_to_refund < $available){
                    $available = $amount_to_refund;
                } else {
                    $available = $available - $refunded;
                }

                $return = $this->refund($paidTransaction->payment_id, intval($available), $request['notes'], $paidTransaction->transaction_id, $paidTransaction->booking_id, $paidTransaction->last4Digits, $paidTransaction->currency, $request['message'], $request['sendEmail'], $request['updatepayment']);

                if($return['success']){
                    $amount_to_refund = $amount_to_refund - $available;
                    $refunded += $available;
                } else {
                    return redirect()->back()
                        ->with('error_message', $return['message']);
                    break;
                }
            }
        }
        return redirect()->back()
            ->with('flash_message', 'Successfully Refunded');
    }

    public function refund($payment_id, $amount, $notes, $trxid, $booking_id, $last4Digits, $currency, $message, $sendEmail, $updatepayment){

        $Authorization = paymentConfig::integrationKey();

        $vendorTxCode = 'lockers_' . Carbon::now()->format('Ymdhis') . '_' . $booking_id;

        $curl = curl_init();
        $refund_transaction_data = [
            'transactionType' => 'refund',
            'vendorTxCode' => '' . $vendorTxCode . '',
            'referenceTransactionId' => $trxid,
            'amount' => $amount,
            'currency' => $currency,
            'description' => 'lockers Refund Amount',
        ];

        $refund_transaction_data = json_encode($refund_transaction_data);
        $refund_url = \Config::get('app.refund_url');
        curl_setopt_array($curl, array(
            CURLOPT_URL => $refund_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $refund_transaction_data,
            CURLOPT_HTTPHEADER => array(
                "Authorization: Basic $Authorization",
                "Cache-Control: no-cache",
                "Content-Type: application/json"
            ),
        ));

        $refund_response = curl_exec($curl);

        $refund_response = json_decode($refund_response);

        $err = curl_error($curl);

        curl_close($curl);

        if (isset($refund_response->status)) {
            if ($refund_response->statusCode == '0000') {

                $status_message = $refund_response->statusCode . ':' . $refund_response->statusDetail;

                $this->saveTransaction($booking_id, $payment_id, $vendorTxCode, $currency, $refund_response, $amount, 0000, $status_message, 0, $notes);

                $insert = RefundTransaction::insert([
                    'transaction_id' => $trxid,
                    'amount' => $amount/100,
                    'status' => 1,
                ]);

                $update_payment = Payment::where('id', $payment_id)->update([
                    'has_transaction' => 1,
                ]);

                if($sendEmail == "on"){
                    $this->sendEmail($payment_id, $booking_id, $amount/100, $currency, $message);
                }

                if($updatepayment == "on"){
                    $paymentRecord = Payment::where('id', $payment_id)->first();
                    $previous_amount = $paymentRecord->amount;
                    $money_refunded = floatval($amount/100);
                    $new_amount = floatval($previous_amount - $money_refunded);

                    $success = Payment::where('id', $payment_id)->update([
                        'account_balance' => $new_amount,
                    ]);
                }

                return (['success'=> true, 'message' => 'Successfully Refunded']);
            }
        } else if (isset($refund_response->code)){
            if($refund_response->code == 1018) {

                $status_message = $refund_response->code . ':' . $refund_response->description;

                $this->saveTransaction($booking_id, $payment_id, $vendorTxCode, $currency, $refund_response, $amount, $refund_response->code, $status_message, 1, $notes);

                return (['success'=> false, 'message' => $refund_response->description]);
            }
        }
        else {

            $status_message = 'Refund failed!';
            //dd($refund_response);

            $this->saveTransaction($booking_id, $payment_id, $vendorTxCode, $currency, $refund_response, $amount, 1, $status_message, 1, $notes);

            return (['success'=> false, 'message' => $status_message]);
        }
    }

    public function saveTransaction($booking_id, $payment_id, $vendorTxCode, $currency, $refund_response, $amount, $status, $status_message, $fraud_score, $notes){

        $last4Digits = "";
        $transaction_id = "";
        if(isset($refund_response->paymentMethod)){
            $last4Digits = $refund_response->paymentMethod->card->lastFourDigits;
            $transaction_id = $refund_response->transactionId;
        }
        $transaction = new Transaction();
        $transaction->booking_id = $booking_id;
        $transaction->payment_id = $payment_id;
        $transaction->vendor_tx_code = $vendorTxCode;
        $transaction->user_id = Auth::user()->id;
        $transaction->currency = $currency;
        $transaction->last4Digits = $last4Digits;
        $transaction->refund = $amount/100;
        $transaction->type = '06';
        $transaction->status = $status;
        $transaction->status_message = $status_message;
        $transaction->fraud_score = $fraud_score;
        $transaction->transaction_id = $transaction_id;
        $transaction->notes = $notes;
        $transaction->save();

        return true;
    }

    public function sendEmail($payment_id, $booking_id, $amount, $currency, $message){

        $payment = Payment::where('id', $payment_id)->first();
        $user = User::where('id', $payment->user_id)->first();
        $current_date_time = Carbon::now()->toDateTimeString();
        $transaction = new Transaction();
        $transactions = $transaction->getOutstaningBalance($booking_id);

        $balance = '0';

        if(isset($transactions->first()->account_balance)){
            $balance =  $transactions->first()->account_balance;
        } else {
            $balance = '0';
        }

        $request_email_template = EmailTemplate::where('id',10)->first();
        $to = $user->email;
        //$to = 'rperris@xweb4u.com';
        $subject = $request_email_template->subject;
        $template_text = $request_email_template->template_text;

        $placeholders = ["%name%", "%booking_id%", "%amount%", "%date%", "%balance%", "%currency%", "%message%"];
        $placeholder_values   = [$user->name, $booking_id, $amount,  date("F jS, Y", strtotime($current_date_time)), $balance,$currency, $message];
        $email_html = str_replace($placeholders, $placeholder_values, $template_text);
        $sms_body =null;
        $phone_number =null;

        $sale = new Sale();
        $sale->saveEmailForCron($to,$phone_number, $subject, $email_html,$sms_body);

    }

    public function refundPaymentList(Request $request)
    {

        if(isset($request->id)){
            $id = $request->id;
        } else {
            $id = $request->session()->get('booking_id_amount');
        }
        $record_access =false;
        $payments = Payment::leftJoin('sales', function($join)
        {
            $join->on('sales.booking_id', '=', 'payments.booking_id');
        })
            ->join('companies','companies.id','=','sales.company_id')
            ->join('users','users.id','=','payments.user_id');

        if(auth()->user()->can('All Records')) {
            $record_access = true;

        }elseif( Auth::user()->company_id && Auth::user()->franchisee_id) {
            $franchisee_companies = FranchiseeCompany::where('franchisee_id',Auth::user()->franchisee_id)->pluck('company_id')->toArray();

            $payments->Where(function ($query) use ($franchisee_companies)  {
                $query->whereIn('sales.company_id', $franchisee_companies)
                    ->orWhere('sales.company_id',Auth::user()->company_id)
                    ->orWhere('payments.user_id', Auth::user()->id);
            })->where('payments.booking_id',$id)->first();

            if(!empty($payments))
                $record_access =true;

        }
        elseif( Auth::user()->franchisee_id ) {
            $franchisee_companies = FranchiseeCompany::where('franchisee_id',Auth::user()->franchisee_id)->pluck('company_id')->toArray();

            $payments->Where(function ($query) use ($franchisee_companies)  {
                $query->whereIn('sales.company_id', $franchisee_companies)
                    ->orWhere('payments.user_id', Auth::user()->id);
            })->where('payments.booking_id',$id)->first();
            if(!empty($payments))
                $record_access =true;
        }

        elseif( Auth::user()->company_id ){
            $payments->Where(function ($query) {
                $query->Where('payments.user_id', Auth::user()->id)
                    ->orWhere('sales.company_id', Auth::user()->company_id);
            })->where('payments.booking_id',$id)->first();
            if(!empty($payments))
                $record_access =true;

        }else{
            $payments->where('payments.user_id',Auth::user()->id)->where('payments.booking_id',$id)->first();
            if(!empty($payments))
                $record_access =true;
        }

        if($record_access == true) {
            $transaction = new Transaction();
            $data = new Transaction();
            $transactions = $transaction->getOutstaningBalance($id);
            $payment = Payment::where('booking_id', $id)->first();
            $data =  $data->getOutstaningBalance($id);
            if($data){
                if($payment->amount != $data->first()->account_balance){
                    $not_balanced = true;
                    return view('backend.pages.students.refund-list', compact('transactions', 'not_balanced', 'payment', 'data'));
                } else {
                    return view('backend.pages.students.refund-list', compact('transactions'));
                }
            }
        }else{

            return redirect()->back()->with('error_message','You don\'t have access of this record!');
        }
    }

    public function filter(Request $request){
        $transaction = new Transaction();
        $data = new Transaction();
        $start_date =  date('Y-m-d 00:00:00',strtotime($request->start_date));
        $end_date =date('Y-m-d 23:00:00',strtotime($request->end_date));
        $transactions = $transaction->getOutstaningBalance($request->booking_id);
        $transactions = $transactions->where('created_at', '>=', $start_date)
            ->where('created_at', '<=', $end_date);
        $payment = Payment::where('booking_id', $request->booking_id)->first();
        $data =  $data->getOutstaningBalance($request->booking_id);

        if($transactions->isEmpty()){
            return redirect('admin/refundPaymentList/')
                ->with('error_message',
                    'No Records Found')->with('id', $request->booking_id);
        } else {
            if($payment->amount != $data->first()->account_balance){
                $not_balanced = true;
                return view('backend.pages.students.refund-list', compact('transactions', 'not_balanced', 'payment', 'data'));
            } else {
                return view('backend.pages.students.refund-list', compact('transactions'));
            }
        }

    }

    public function getPaymentTypes($booking_id, $type){
        // $input = $request->all();
        $transaction = new Transaction();
        $data = $transaction->getAmount($booking_id, $type);
        return response()->json($data);
    }

    public function changePrice(Request $request){
        $request->session()->put('booking_id_amount', $request->booking_id);
        $transaction_data = Transaction::where('booking_id', $request->booking_id)->first();
        $type = '';
        if($request->type == 'l'){
            $type = '05';
        } else if($request->type == 'c'){
            $type = '04';
        }
        $amount = floatval($request->amount);
        //dd($amount, $request->all());
        $data = Transaction::insert([
            'booking_id' => $request->booking_id,
            'payment_id' => $transaction_data->payment_id,
            'user_id' => Auth::user()->id,
            'amount_change' => $amount,
            'type' => $type,
            'notes' => $request->reason,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        $update_payment = Payment::where('id', $transaction_data->payment_id)->update([
            'has_transaction' => 1,
        ]);
        return redirect()->back()->with('success', ['Success']);
    }

    public function resendEmail(Request $request){

        $sales = Sale::where('booking_id',$request->booking_id)->get();
        foreach ($sales as $sale){
            $booking =Sale::find($sale->id);
            $booking->confirmation_sent = 0;
            $booking->save();
        }

        return response()->json('success');
    }

    public function fixPayments(){
        ini_set('memory_limit', '-1');
        $payments = Payment::all();

        foreach ($payments as $key => $value) {
            Payment::where('id', $value->id)->whereNotNull('amount')->update([
                'account_balance' => $value->amount
            ]);
        }
    }

    public function fixPaymentsCreatedAt(){
        ini_set('memory_limit', '-1');
        $payments = Payment::whereNull('created_at')->get();

        foreach ($payments as $key => $value) {
            $sale = Sale::where('id', $value->booking_id)->first();
            if(isset($sale->updated_at) && !empty($sale->updated_at)){
                Payment::where('id', $value->id)->update([
                    'created_at' => $sale->updated_at,
                ]);
            }
        }
    }

    public function fixPaymentTransactions(){
        ini_set('memory_limit', '-1');
        $payments = Payment::where('has_transaction', 0)->get();

        foreach ($payments as $key => $value) {
            $update = Payment::where('id', $value->id)->update([
                'has_transaction' => 0,
            ]);
        }
        foreach ($payments as $key => $value) {
            $sale = Transaction::where('payment_id', $value->id)->first();
            if(isset($sale) && !empty($sale)){
                Payment::where('id', $value->id)->update([
                    'has_transaction' => 1,
                ]);
            }
        }
    }

    public function changeStatus(Request $request){
        // dd($request->all());
        $request->session()->put('booking_id_payment', $request->booking_id);
        if(isset($request->date)){
            $update = Sale::where('id', $request->booking_id)->update([
                'status' => $request->type,
                'hold_booking_date' => date('Y-m-d',strtotime($request->date)),
            ]);
        }
        else {
            $update = Sale::where('id', $request->booking_id)->update([
                'status' => $request->type,
            ]);
        }

        return redirect()->back();
    }

    public function getBlocks(Request $request){
//        dd($request->all());
        $sale_id = $request->sale_id;
        $booking_id = $request->booking_id;

        $response = $this->show_bookings($sale_id);

        $hold_next_year = false;
        $hold_next_year_sale_id = null;
        $hold_next_year_booking = Sale::where('hold_sales_id',$sale_id)->where('status','r')->first();
        if($hold_next_year_booking) {
            $hold_next_year = true;
            $hold_next_year_sale_id = $hold_next_year_booking->id;

        }
//        dd($hold_next_year);

        $sale = Sale::where('id', $sale_id)->first();
        if($sale->checked == 1){
            $move_checked = true;
        }else{
            $move_checked = false;
        }

//dd( $current_child->id);
        $data = [
            'blocks' => $response[0],
            'locker_ids' => $response[1],
            'hold_next_year' => $hold_next_year,
            'booking_id' =>$booking_id,
            'current_child_sale_id' => $sale_id,
            'hold_next_year_sale_id' => $hold_next_year_sale_id,
            'move_checked' => $move_checked,
        ];
//      dd($bookings);

        return response()->json($data);
    }

    public function show_bookings($sale_id){

        Session::put('saleKey', $sale_id);

        $childrens = Sale::where('id', $sale_id)->get();


        $childrens_count = $childrens->count();

        $child_blocks = [];
        $child_academic_year = [];
        $locker_ids=[];

        $selected_block_and_number_group = [];


        foreach ($childrens as $index => $children) {
            $selected_block_and_number = [];
            $block_name = $children->block->name;
            $locker_number = $children->number;
            array_push($selected_block_and_number,$block_name);
            array_push($selected_block_and_number,$locker_number);

            $index++;

            $location_group = Sale::join('companies', 'sales.company_id', '=', 'companies.id')
                ->where('sales.id', $children->id)
                ->select('companies.location_group_id')->first();

            $location_group = $location_group->location_group_id;

            if ($location_group == 1) {


                if(empty($children->school_year_id)) {
                    $school_year = SchoolYear::whereDate('from_date', $children->start)->whereDate('to_date', 'LIKE', '%' .Carbon::parse($children->end)->format('Y-m-d').'%')->first();
//                   dd($school_year);
                    if ($school_year) {
                        $school_year_id = $school_year->id;
                    } else {
                        $start_title = Carbon::parse($children->start)->format('d M, Y');
                        $end_title = Carbon::parse($children->end)->format('d M, Y');
                        $title = $start_title . ' - ' . $end_title;
                        $dontAllowDuplicate = SchoolYear::where('title', $title)->first();
                     if(empty($dontAllowDuplicate)){
                        $SchoolYear = new SchoolYear();
                        $SchoolYear->title = $title;
                        $SchoolYear->from_date = $children->start;
                        $SchoolYear->to_date = $children->end;
                        $SchoolYear->status = 0;
                        $SchoolYear->save();
                        $school_year_id = $SchoolYear->id;
                    }else{
                        $school_year_id = $dontAllowDuplicate->id;
                    }

                    }
                    $children->school_year_id = $school_year_id;

                }

                if(!empty($children->block_id)) {

                    if(empty($children->school_academic_year_id)) {
                        $school_year = BlockYears::where('block_id', $children->block_id)->first();

                        if(!empty($school_year->school_academic_year_id))
                            $children->school_academic_year_id = $school_year->school_academic_year_id;

                    }
                }
                $children->save();

                $schoolYear = SchoolYear::where('id', $children->school_year_id)->first();

                $start = Carbon::parse($schoolYear->from_date)->startOfDay()->toDateTimeString();
                $end = Carbon::parse($schoolYear->to_date)->endOfDay()->toDateTimeString();
//                dd(Session::get('hold_locker_checked'));

                $hold_next = Sale::where('hold_sales_id',$sale_id)->first();

                if(!empty($hold_next)){
                    $book_next_year = true;
                    Session::put('hold_locker_checked', true);
                }else{
                    $book_next_year = false;
                    Session::put('hold_locker_checked', false);
//                    dd(Session::get('hold_locker_checked'));
                }

                $sales = new Sale();
                $blocks = $sales->school_year_blocks($children->company_id , $children->school_academic_year_id);
//                 dd($blocks);
                if (count($blocks) > 0) {
                    $options = '';
                    $options .= '<option value="" selected>Choose Block</option>';

                    foreach ($blocks as $block) {
//                        $block = Block::where('id',$block->block_id)->where('active',1)->where('display',1)->first();
                        $block = Block::where('id',$block->block_id)->where('active',1)->first();
//                        $block = Block::find($block->block_id);

//                        $sales = new Sale();
//                        $available_lockers = $sales->total_available_lockers_in_block($block->id,$start,$end,$book_next_year);
//dd($available_lockers);
//                        if($available_lockers['total_available'] > 0 || $children->block_id == $block->id) {
                        if(!empty($block)) {
//                            dd($block);

                            $rates = $sales->rate_calculation($location_group,$block->id,$children->start,$children->end,$children->booking_id);

//                           dd($rates);
                            $unit = $rates[0];
                            $tax = $rates[1];

//                            $sale = Sale::find($children->id);
////                            if (Carbon::now() >= Carbon::parse($children->hold_booking_date)->subHour()->toDateTimeString() && Carbon::now() <= $children->hold_booking_date) {
////
////                                $sale->hold_booking_date = $hold_booking_date;
////                            }
//                            if ($children->block_id == $block->id ) {
//                                $sale->price = $unit;
//                                $sale->price_original = $unit;
//                                $sale->tax = $tax;
//                            }
//
//                            $sale->save();


                            $currency = Company::join('currencies', 'companies.currency', '=', 'currencies.currency_name')
                                ->where('companies.id', $children->company_id)
                                ->select('currencies.currency_name', 'currencies.symbol', 'currencies.position')->first();

                            if ($currency->position == 'b') {

                                $options .= '<option  value="' . $block->id .'_'.$unit.'_'.$tax. '" ';
                                if ($block->id == $children->block_id) {
                                    if($block->display == 0) {
                                        array_push($selected_block_and_number, 'hidden');
                                    }else{
                                        array_push($selected_block_and_number, 'show');

                                    }
                                    $options .= 'selected';
                                }
                                $options .= '>' . $block->name . ' ' . $currency->symbol . '' . $unit . '';
                                if($block->display == 0) {
                                    $options .= '  This Block is Hidden';
                                }
                                $options .= '</option>';

                            } else {

                                $options .= '<option  value="' . $block->id .'_'.$unit.'_'.$tax. '" ';
                                if ($block->id == $children->block_id) {
                                    if($block->display == 0) {
                                        array_push($selected_block_and_number, 'hidden');
                                    }else{
                                        array_push($selected_block_and_number, 'show');

                                    }
                                    $options .= 'selected';
                                }
                                $options .= '>' . $block->name . ' ' . $unit . ' ' . $currency->symbol . '';
                                if($block->display == 0) {
                                    $options .= '  This Block is Hidden';
                                }
                                $options .= '</option>';
                            }
                        }

                    }

                    $options .= '</select>';
//                        dd($unit);

                    $select_html = '<select class="form-control chzn-select blocks fa " id="blocks" data-sale_id="' . $children->id . '" data-start="' . $start . '" data-end="' . $end . '"   data-counter="' . $index . '"   name="block">';

                    $blocks_html = $select_html . '' . $options;

                    array_push($child_blocks, $blocks_html);
//                        dd($child_blocks);

                } else {

                    $options = '';
                    $options .= '<select class="form-control chzn-select  fa " id="blocks" data-sale_id="' . $children->id . '" data-start="' . $start . '" data-end="' . $end . '" data-counter="' . $index . '" name="block">';
                    $options .= '<option  value="" selected>Choose Block</option>';
                    $options .= '</select>';
                    array_push($child_blocks, $options);

                }

                $school_academic_year = SchoolAcademicYear::find($children->school_academic_year_id);
//                dd($children->school_academic_year_id);
                array_push($child_academic_year,$school_academic_year->school_year);

                array_push($locker_ids, $children->locker_id);
            }
            elseif ($location_group == 2 || $location_group == 3){

                $start = Carbon::parse($children->start)->format('Y-m-d H:i:s');
                $end = Carbon::parse($children->end)->format('Y-m-d H:i:s');
                $schoolYear = 0;


//                $blocks = Block::where('company_id', $children->company_id)->where('active', 1)->where('display', 1)
//                    ->orderBy('name', 'ASC')->get();

                $blocks = Block::where('company_id', $children->company_id)->where('active', 1)
                    ->orderBy('name', 'ASC')->get();


                if (count($blocks) > 0) {


                    $options = '';

                    $options .= '<option  value="" selected>Choose Block</option>';

                    foreach ($blocks as $block) {

                        $book_next_year = false;

                        $sales = new Sale();
                        $rates = $sales->rate_calculation($location_group , $block->id,$start,$end,$children->booking_id);
                        $lowest_rate = $rates[0];
                        $tax = $rates[1];

                        $currency = Company::join('currencies', 'companies.currency', '=', 'currencies.currency_name')
                            ->where('companies.id', $block->company_id)
                            ->select('currencies.currency_name', 'currencies.symbol', 'currencies.position')->first();

//

                        if ($currency->position == 'b') {

                            $options .= '<option  value="' . $block->id . '_' . $lowest_rate . '_' . $tax . '" ';
                            if ($block->id == $children->block_id) {
                                if($block->display == 0) {
                                    array_push($selected_block_and_number, 'hidden');
                                }else{
                                    array_push($selected_block_and_number, 'show');

                                }
                                $options .= 'selected';
                            }
                            $options .= '>' . $block->name . ' ' . $currency->symbol . '' . $lowest_rate . '';
                            if($block->display == 0) {
                                $options .= '  This Block is Hidden';
                            }
                            $options .= '</option>';

                        } else {
                            $options .= '<option  value="' . $block->id . '_' . $lowest_rate . '_' . $tax . '" ';
                            if ($block->id == $children->block_id) {
                                if($block->display == 0) {
                                    array_push($selected_block_and_number, 'hidden');
                                }else{
                                    array_push($selected_block_and_number, 'show');

                                }
                                $options .= 'selected';
                            }
                            $options .= '>' . $block->name . ' ' . $lowest_rate . ' ' . $currency->symbol . '';
                            if($block->display == 0) {
                                $options .= '  This Block is Hidden';
                            }
                            $options .= '</option>';
                        }

//                        }
                    }

                    $options .= '</select>';

                    $select_html = '<select class="form-control chzn-select blocks fa child'.$index.'" id="blocks' . $index . '" data-sale_id="' . $children->id . '" data-start="' . $start . '" data-end="' . $end . '" data-end="' . $end . '" data-rate="' . $lowest_rate . '" data-tax="' . $tax . '" data-counter="' . $index . '" name="block' . $index . '">';

                    $blocks_html = $select_html . '' . $options;

                    array_push($child_blocks, $blocks_html);
                    array_push($locker_ids, $children->locker_id);

                } else {

                    $options = '';
                    $options .= '<select class="form-control chzn-select blocks child'.$index.' fa " id="blocks' . $index . '" data-sale_id="' . $children->id . '" data-start="' . $start . '" data-end="' . $end . '" data-counter="' . $index . '" name="block' . $index . '">';
                    $options .= '<option  value="" selected>Choose Block</option>';
                    $options .= '</select>';

                    array_push($child_blocks, $options);
                }
            }
//            $display_lcoker = '<div class="lockers_printing" style="text-align:left;" id="display_lockers'.$index.'"></div>';
            array_push($selected_block_and_number_group,$selected_block_and_number);

        }

//dd($child_blocks);
        $placeholders = FrontendPlaceholder::where('key','choose-locker-child-name-date-title')->pluck('placeholder')->toArray();
        $child_name_date_placeholders = implode(",", $placeholders);
//        dd($selected_block_and_number_group);

        $render_view = view('backend.pages.students.choose_locker_for_move_render', compact('childrens', 'childrens_count', 'location_group', 'schoolYear', 'child_blocks', 'child_name_date_placeholders', 'sale_id', 'child_academic_year', 'selected_block_and_number_group'))->render();
        return [$render_view ,$locker_ids];

    }

    public function updateNextYearBooking(Request $request){
//        dd($request->all());
        $booking_id = $request->booking_id;
        $sale_id_next_year = $request->sale_id_next_year;
        $current_child_sale_id = $request->current_child_sale_id;

        $current_child = Sale::find($current_child_sale_id);
//        $current_child_sale_id =null;
//dd($current_child_sale_id);
        $hold_next_year_booking = Sale::where('hold_sales_id',$current_child_sale_id)->where('status','!=','b')->first();
        if(!empty($hold_next_year_booking)) {

            if ($request->checkbox_next == 'false') {

                if (!empty($hold_next_year_booking)) {

                    $hold_next_year_booking->status = 'l';
                    $hold_next_year_booking->street = 'moved now';
                    $hold_next_year_booking->save();
                }

            } else {

                $is_hold_next_reserved = Sale::where('hold_sales_id', $current_child_sale_id)->first();
//            dd($is_hold_next_reserved);
                $next_year_booking = null;
                $current_bookings = Sale::where('booking_id', $booking_id)->get();
                foreach ($current_bookings as $current_booking) {
                    $next_year = Sale::where('hold_sales_id', $current_booking->id)->first();
                    if ($next_year) {
                        $next_year_booking = $next_year;
                    }
                }
//            dd($next_year_booking);


                $hold_locker = Sale::find($current_child_sale_id);

                $location_group_id = $hold_locker->company->location_group_id;
                if ($location_group_id == 1) {
                    $language = Session::get('locale');

                    //                    foreach ($hold_lockers as $hold_locker) {

                    $schoolYear = SchoolYear::where('id', $hold_locker->school_year_id)->first();
                    $start_hold = Carbon::parse($schoolYear->from_date)->startOfDay()->addYear()->toDateTimeString();
                    $year = Carbon::parse($schoolYear->from_date)->startOfDay()->addYear()->format('Y');
                    $date_added = $year . '-05-01 23:59:59';
                    $end_hold = Carbon::parse($schoolYear->to_date)->endOfDay()->addYear()->toDateTimeString();

                    $school_year = SchoolYear::whereDate('from_date', $start_hold)->whereDate('to_date', $end_hold)->first();

                    if ($school_year) {
                        $school_year_id = $school_year->id;
                    } else {
                        $start_title = Carbon::parse($start_hold)->format('d M, Y');
                        $end_title = Carbon::parse($end_hold)->format('d M, Y');
                        $title = $start_title . ' - ' . $end_title;
                        $dontAllowDuplicate = SchoolYear::where('title', $title)->first();
                     if(empty($dontAllowDuplicate)){
                        $SchoolYear = new SchoolYear();
                        $SchoolYear->title = $title;
                        $SchoolYear->from_date = $start_hold;
                        $SchoolYear->to_date = $end_hold;
                        $SchoolYear->status = 0;
                        $SchoolYear->save();
                        $school_year_id = $SchoolYear->id;
                    }else{
                         $school_year_id = $dontAllowDuplicate->id;
                    }
                    }
                }
//dd($is_hold_next_reserved);
                if (!empty($is_hold_next_reserved)) {
//                dd($is_hold_next_reserved);
//                $hold_next_year = Sale::find($sale_id_next_year);

                    $is_hold_next_reserved->status = 'r';
                    $is_hold_next_reserved->save();
                } else if (!empty($next_year_booking)) {
                    $hold_locker = Sale::find($current_child_sale_id);
//                        dd($hold_locker);

                    $sale = new Sale();
                    $sale->user_id = Auth::user()->id;
                    $sale->booking_id = $next_year_booking->booking_id;
                    $sale->booking_type = 2;
                    if (isset($language)) {
                        if ($language == 'fr') {
                            $sale->lang = 'FR';
                        } else {
                            $sale->lang = 'EN';
                        }
                    } else {
                        $sale->lang = 'EN';
                    }
                    $sale->title = 'LOCKER';
                    $sale->number = $hold_locker->number;
                    //                    $sale->net_pass = $hold_locker->net_pass;
                    $sale->lock_type = $hold_locker->lock_type;
                    $sale->company_id = $hold_locker->company_id;

                    $sale->locker_id = $hold_locker->locker_id;

                    $sale->block_id = $hold_locker->block_id;
                    $sale->block_name = $hold_locker->block_name;
                    $sale->start = $start_hold;
                    $sale->end = $end_hold;
                    $sale->email = $hold_locker->email;
                    $sale->phone_number = $hold_locker->phone_number;
                    $sale->status = 'r';
                    $sale->hold_booking_date = $date_added;
                    $sale->confirmation_sent = 0;
                    $sale->delete = 0;
                    $sale->street = $hold_locker->street;
                    $sale->city = $hold_locker->city;
                    $sale->post_code = $hold_locker->post_code;
                    $sale->country = $hold_locker->country;
                    $sale->child_first_name = $hold_locker->child_first_name;
                    $sale->child_surname = $hold_locker->child_surname;
                    $sale->child_email = $hold_locker->child_email;
                    $sale->school_year_id = $school_year_id;
                    $sale->school_academic_year_id = $hold_locker->school_academic_year_id;
                    $sale->hold_sales_id = $hold_locker->id;
                    $sale->save();
//dd($sale->id);
                    $sale_obj = new Sale();
                    $sale_obj->update_locker_code($sale->id);

                } else {

                    $hold_locker = Sale::find($current_child_sale_id);
//                dd($hold_locker);
                    $location_group_id = $hold_locker->company->location_group_id;
                    if ($location_group_id == 1) {

//                    $hold_lockers = Sale::where('booking_id', $booking_id)->orderBy('id', 'ASC')->get();
                        $language = Session::get('locale');


                        $schoolYear = SchoolYear::where('id', $hold_locker->school_year_id)->first();
                        $start_hold = Carbon::parse($schoolYear->from_date)->startOfDay()->addYear()->toDateTimeString();
                        $year = Carbon::parse($schoolYear->from_date)->startOfDay()->addYear()->format('Y');
                        $date_added = $year . '-05-01 23:59:59';
                        $end_hold = Carbon::parse($schoolYear->to_date)->endOfDay()->addYear()->toDateTimeString();

                        $school_year = SchoolYear::whereDate('from_date', $start_hold)->whereDate('to_date', $end_hold)->first();
                        //
                        if ($school_year) {
                            $school_year_id = $school_year->id;
                        } else {
                            $start_title = Carbon::parse($start_hold)->format('d M, Y');
                            $end_title = Carbon::parse($end_hold)->format('d M, Y');
                            $title = $start_title . ' - ' . $end_title;
                             $dontAllowDuplicate = SchoolYear::where('title', $title)->first();
                         if(empty($dontAllowDuplicate)){
                            $SchoolYear = new SchoolYear();
                            $SchoolYear->title = $title;
                            $SchoolYear->from_date = $start_hold;
                            $SchoolYear->to_date = $end_hold;
                            $SchoolYear->status = 0;
                            $SchoolYear->save();
                            $school_year_id = $SchoolYear->id;
                            }else{
                                 $school_year_id = $dontAllowDuplicate->id;
                            }
                        }

                        $sale = new Sale();
                        $sale->user_id = Auth::user()->id;
                        $sale->booking_type = 2;
                        if (isset($language)) {
                            if ($language == 'fr') {
                                $sale->lang = 'FR';
                            } else {
                                $sale->lang = 'EN';
                            }
                        } else {
                            $sale->lang = 'EN';
                        }
                        $sale->title = 'LOCKER';
                        $sale->number = $hold_locker->number;
                        //                    $sale->net_pass = $hold_locker->net_pass;
                        $sale->lock_type = $hold_locker->lock_type;
                        $sale->company_id = $hold_locker->company_id;
                        $sale->locker_id = $hold_locker->locker_id;
                        $sale->block_id = $hold_locker->block_id;
                        $sale->block_name = $hold_locker->block_name;
                        $sale->start = $start_hold;
                        $sale->end = $end_hold;
                        $sale->email = $hold_locker->email;
                        $sale->phone_number = $hold_locker->phone_number;
                        $sale->status = 'r';
                        $sale->hold_booking_date = $date_added;
                        $sale->confirmation_sent = 0;
                        $sale->delete = 0;
                        $sale->street = $hold_locker->street;
                        $sale->city = $hold_locker->city;
                        $sale->post_code = $hold_locker->post_code;
                        $sale->country = $hold_locker->country;
                        $sale->child_first_name = $hold_locker->child_first_name;
                        $sale->child_surname = $hold_locker->child_surname;
                        $sale->child_email = $hold_locker->child_email;
                        $sale->school_year_id = $school_year_id;
                        $sale->school_academic_year_id = $hold_locker->school_academic_year_id;
                        $sale->hold_sales_id = $hold_locker->id;
                        $sale->save();

                        $sale_obj = new Sale();
                        $sale_obj->update_locker_code($sale->id);

                        Sale::where('id', $sale->id)->update(['booking_id' => $sale->id]);

                    }
                }
            }
        }




        return response()->json('success');
    }

    public function updateCheckedStatus(Request $request){
//        dd($request->all());
        $booking_id = $request->booking_id;
//        dd($request->checkbox_next);
                   Sale::where('id', $booking_id)->update([
            'child_first_name' => $request['f_name'],
            'child_surname' => $request['l_name'],
            
        ]);
        if($request->checkbox_next == 'false'){
//            dd($booking_id);
            $booking = Sale::where('id',$booking_id)->first();
            $booking->checked = 0;
            $booking->save();

        }else{
            $booking = Sale::where('id',$booking_id)->first();
            $booking->checked = 1;
            $booking->save();
        }




        return response()->json('success');
    }

    public function clearAllChecked(Request $request){

        $bookings = Sale::where('checked',1)->get();
        foreach ($bookings as $booking) {
            $booking->checked = 0;
            $booking->save();
        }

        return response()->json('success');
    }

    public function updateSale (Request $request)
    {

        Session::forget('schoolTermSubmit');
        if(Session::has('saleKey')) {

            $sale = Sale::where('id',$request['sale_id'])->first();
            $company_id = $sale->company_id;
            $start = $sale->start;
            $end = $sale->end;

            $booked_lockers = Sale::where('company_id',$company_id)
                ->where('locker_id',$request['locker_id'])
                ->where('block_id',$request['block_id'])
                ->where(function ($query){
                    $query->where('status','b')
                        ->orWhere('status','r');
                })
                ->where(function ($query) use ($start,$end) {
                    $query->where('end','>=',$start)
                        ->Where('start','<=',$end);
                })
                ->first();

            if($booked_lockers){
                $data = [
                    'status' => 'already_exists',
                    'locker_id' => $request['locker_id']
                ];

            }else {

                $block = Block::find($request['block_id']);

                $locker_move = new LockerMove();
                $locker_move->sale_id = $sale->id;
                $locker_move->booking_id = $sale->booking_id;
                $locker_move->old_locker_id = $sale->locker_id;
                $locker_move->old_locker_number = $sale->number;
                $locker_move->new_locker_id = $request['locker_id'];
                $locker_move->new_locker_number = $request['locker_number'];
                $locker_move->save();

                $sale->locker_id = $request['locker_id'];
                $sale->block_id = $request['block_id'];
                $sale->block_name = $block->name;
                $sale->number = $request['locker_number'];
                $sale->save();

                //update Manual locker code using sale_id
                $code= new Sale();
                $code->update_locker_code($request['sale_id']);

                //add sales_audit log
                $sales_audit_obj = new SalesAudit();
                $sales_audit_obj->sales_audit_log($request['sale_id'],$type = 'validate');

                $hold_next_year_booking = Sale::where('hold_sales_id',$request['sale_id'])->first();

                if($hold_next_year_booking) {

                            $locker_move = new LockerMove();
                            $locker_move->sale_id = $hold_next_year_booking->id;
                            $locker_move->booking_id = $hold_next_year_booking->booking_id;
                            $locker_move->old_locker_id = $hold_next_year_booking->locker_id;
                            $locker_move->old_locker_number = $hold_next_year_booking->number;
                            $locker_move->new_locker_id = $request['locker_id'];
                            $locker_move->new_locker_number = $request['locker_number'];
                            $locker_move->save();

                            $hold_next_year_booking->locker_id = $request['locker_id'];
                            $hold_next_year_booking->block_id = $request['block_id'];
                            $hold_next_year_booking->block_name = $block->name;
                            $hold_next_year_booking->number = $request['locker_number'];
                            $hold_next_year_booking->save();

                            $code= new Sale();
                            $code->update_locker_code($hold_next_year_booking->id);


                            //add sales_audit log
                            $sales_audit_obj = new SalesAudit();
                            $sales_audit_obj->sales_audit_log($hold_next_year_booking->id,$type = 'validate');
                }

                $data = [
                    'status' => 'available',
                    'locker_id' => $request['locker_id']
                ];

            }
            return response()->json($data);

        }else{
            return response()->json('home_page');
        }

    }

    public function getlockers(Request $request){

        if(Session::has('saleKey')) {

            $start = Carbon::parse($request['start'])->format('Y-m-d H:i:s');
            $end = Carbon::parse($request['end'])->format('Y-m-d H:i:s');
//            if(date("m") < 7  ){
//                $start = Carbon::now()->toDateTimeString();
//                $end = Carbon::now()->endOfDay()->toDateTimeString();
//            } else {
//                $start = date("Y-09-01 00:00:00");
//                $end = date("Y-09-01 23:59:59");
//
//            }
            if(Session::get('hold_locker_checked') == true){
                $book_next_year = true;
            }else{
                $book_next_year = false;
            }
//            dd($book_next_year);
            $sale = Sale::find($request->sale_id);
            $selected_block_id = $sale->block_id;

            $sales = new Sale();
            $available_lockers = $sales->total_available_lockers_in_block($request->block_id,$start,$end,$book_next_year);

//            if($available_lockers['total_available'] > 0 || $selected_block_id == $request->block_id) {

            $lockers = Locker::where('lockers.block_id', $request->block_id)
                ->where('active',1)
//                        ->where('display',1)
                ->orderBy('locker_order', 'ASC')
                ->get();

            $total_lockers = $lockers->count();
            $lockers->toArray();

            $booked_lockers = Sale::where('booking_id',Session::get('saleKey'))->pluck('locker_id')->toArray();

            $assign_locker = 0;
            if(empty(intval($request->selected_lockers))){
                $selected_lockers = Sale::where('id',$request->sale_id)->first();
                if(!empty($selected_lockers->locker_id))
                {
                    $assign_locker = $selected_lockers->locker_id;
                }
            }

            //$request->selected_lockerssale_id
            $output = '';

            $blocks = Block::leftJoin('locker_block_image', 'locker_block_image.id', '=', 'blocks.locker_block_image_id')
                ->select('blocks.*', 'locker_block_image.file_name as locker_block_image')
                ->where('blocks.id', $request->block_id)->first();

            $output .= '<div class="col-md-12" ><div class="row" style="overflow-x:auto;"><div class="col-md-12 lockers_table" style="padding-left: 0px !important;padding-right:0px !important;" ><table id="locker_table' . $request['counter'] . '">';

            $counter = 0;
            $counter_red = 0;
            for ($i = 1; $i <= $blocks->locker_blocks; $i++) {

                $red_column = 0;
                $output .= '<tr>';
                for ($j = 1; $j <= $blocks->block_height; $j++) {
                    if ($counter_red <= $total_lockers - 1) {
                        $sales =new Sale();
                        $available = $sales->is_locker_available($lockers[$counter_red]['id'],$start,$end,$book_next_year);

                        if (empty($available)) {

                            if(!in_array($lockers[$counter_red]['id'], $booked_lockers))
                                $red_column++;
                        }

                    } else {
                        $red_column++;
                    }

                    $counter_red++;

                }

                for ($j = 1; $j <= $blocks->block_height; $j++) {

                    if ($counter <= $total_lockers - 1) {

                        $sales =new Sale();

                        //// if(in_array($lockers[$counter]['id'],$booked_lockers))
                        $available = 1;
                        //  else
                        $available = $sales->is_locker_available($lockers[$counter]['id'],$start,$end,$book_next_year);
                        $is_off_sale = LockerOffSale::where('locker_id',$lockers[$counter]['id'])->where('active', 1)->first();


                        if (empty($available)){

                            $output .= '<td ><input type = "radio" name = "locker_id' . $request['counter'] . '" class="invisible-radio' . $request['counter'] . '" checked>';
                            if ($red_column == $blocks->block_height) {
                                $output .= '<label for="radio' . $lockers[$counter]['id'] . '" class="red-lockers" > ';
                            } else {
                                $output .= '<label for="radio' . $lockers[$counter]['id'] . '" class="" > ';

                            }

                            $already_selected = 0;
                            if ($lockers[$counter]['id'] == intval($request->selected_lockers))
                                $already_selected = 1;

                            else if ($lockers[$counter]['id'] == $assign_locker)
                                $already_selected = 1;

                            if(empty($already_selected)) {
                                $output .= '<div class="styled-radio-red' . $request['counter'] . ' text-center" style = "color: white">'.$lockers[$counter]->locker_number . '<br ><i class="fa fa-times icon-cross" ></i ></div ></label ></td>';
                            } else {

                                $output .= '<div class="styled-radio' . $request['counter'] . ' text-center" style = "color: white; border: 5px solid #66ff66">' . $lockers[$counter]->locker_number . '<br ><i class="fa fa-check rounded-circle" style="background-color:#66ff66; padding: 6px""></i ></div ></label ></td>';
                                // $output .= '<div class="styled-radio'.$request['counter'].' text-center" style = "color: white;">'.$lockers[$counter]->locker_number.'<br ><i class="fa fa-check icon-tick" ></i ></div ></label ></td >';
                            }

                        } elseif(!empty($is_off_sale))  {
                            $output .= '<td><input type = "radio" name = "locker_id' . $request['counter'] . '"  class="invisible-radio' . $request['counter'] . '" >
                                <label for="radio' . $lockers[$counter]['id'] . '' . $request['counter'] . '" class="">
                                    <div class="styled-radio-red' . $request['counter'] . ' text-center" style = "color: white;">' . $lockers[$counter]->locker_number . '<br ><i class="fa fa-check rounded-circle" style="background-color:#66ff66; padding: 6px""></i ></div ></label ></td >';
                        }elseif($lockers[$counter]['active'] == 1 && $lockers[$counter]['display'] == 0 )  {
                            $output .= '<td><input type = "radio" name = "locker_id' . $request['counter'] . '"  class="invisible-radio' . $request['counter'] . '" >
                                <label for="radio' . $lockers[$counter]['id'] . '' . $request['counter'] . '" class="">
                                    <div class="styled-radio-gray' . $request['counter'] . ' text-center" style = "color: white;"></div ></label ></td >';
                        }else  {
                            $output .= '<td><input type = "radio" name = "locker_id' . $request['counter'] . '" id = "radio' . $lockers[$counter]['id'] . '' . $request['counter'] . '" value="' . $lockers[$counter]['id'] . '" data-sale_id = "' . $request->sale_id . '" data-block_id = "' . $lockers[$counter]->block_id . '" data-locker_id = "' . $lockers[$counter]['id'] . '" data-locker_number = "' . $lockers[$counter]->locker_number . '" data-rate = "' . $request['rate'] . '" data-tax = "' . $request['tax'] . '" data-counter = "' . $request['counter'] . '" data-hold_locker="' . $request['hold_locker'] . '" class="invisible-radio' . $request['counter'] . ' choose-locker" >
                                <label for="radio' . $lockers[$counter]['id'] . '' . $request['counter'] . '" class="puffOut">
                                    <div class="styled-radio' . $request['counter'] . ' text-center" style = "color: white;">' . $lockers[$counter]->locker_number . '<br ><i class="fa fa-check icon-tick" ></i ></div ></label ></td >';
                        }


                    } else {
                        //                                                    $output.='<td> <input type = "radio" name = "id'.$request['counter'].'" class="invisible-radio'.$request['counter'].'" >
                        //                                                        <label for="radio" >
                        //                                                            <div class="styled-radio-gray'.$request['counter'].' text-center" style = "color: white" ></div ></label >
                        //                                                    </td>';
                    }
                    $counter++;
                }
                $output .= '</tr>';
            }

            $output .= '</table></div><div style="display: none;" class="col-md-12 locker_image">
                    <img style="width:100%;" src="' . url('uploads/' . $blocks->locker_block_image) . '" class=" img-responsive" alt="Block Image">
                    </div></div></div>';

            return response()->json($output);
//            }
//            else{
//
//                $output='booked';
//                return response()->json($output);
//            }
        }else{
            return response()->json('home_page');

        }


    }

    public function showMoveableBlocks(Request $request){
        $company = Company::find($request->company_id);
        if(isset($request->company_id)){
            $year = CompanySchoolYear::where('company_id', $request->company_id)->get();
            foreach ($year as $key => $value) {
               $block = BlockYears::where('company_id', $request->company_id)->where('school_academic_year_id', $value->school_academic_year_id)->first();
               if($block){} else {
                unset($year[$key]);
               }
            }
        }else {
            $year = null;
        }
        $options = '';
        $options .= '<select class="form-control chzn-select  fa " id="blocks"  name="block">';
        $options .= '<option  value="" selected>Choose Year</option>';


        if(isset($year)) {
            foreach ($year as $block) {
                $school_academic_year = SchoolAcademicYear::where('id',  $block->school_academic_year_id)->first();

                $options .= '<option  value="' . $school_academic_year->id . '" >' . $school_academic_year->school_year . '</option>';
            }
        }
        $options .= '</select>';
        $data = [
            'blocks' =>$options,
            'company' =>$company->name,
        ];
        return response()->json($data);
    }

    public function saveMultiLockers(Request $request){
        if(date("m") < 7  ){
            $start = Carbon::now()->toDateTimeString();
            $end = Carbon::now()->endOfDay()->toDateTimeString();
        } else {
            $start = date("Y-09-01 00:00:00");
            $end = date("Y-09-01 23:59:59");

        }
        $book_next_year = null;

        foreach ($request->ids as $data){

             $sale = Sale::find($data);
             $sale->company_id =$request->company_id;
             $sale->block_id =$request->block_id;
             $sale->locker_id = null;
             $sale->number = null;
             $sale->save();
        }

        foreach ($request->ids as $data){
           $locker= $this->total_available_lockers_in_block($request->block_id,$start,$end,$book_next_year);

             $sale = Sale::find($data);
             $sale->company_id =$request->company_id;
             $sale->block_id =$request->block_id;
             $sale->locker_id =$locker->id;
             $sale->number =$locker->locker_number;
             $sale->save();
        }

        $data = [
            'blocks' =>'success',

        ];


        return response()->json($data);
    }

    public function total_available_lockers_in_block($block_id,$start,$end,$book_next_year,$used_lockers)
    {

        $available_lockers_array['total_available'] = 0;
        $locker = null;
        $blocks = Block::where('id',$block_id)
            ->where('active',1)
            ->where('display',1)
            ->get();

        foreach ($blocks as $b) {

                $lockers = Locker::where('block_id', $b->id)
                    ->where('active', 1)
                    ->where('display', 1)
                     ->get()->except($used_lockers);

                foreach ($lockers as $l) {
                $sale_obj = new Sale();
                    $available = $sale_obj->is_locker_available($l->id, $start, $end, $book_next_year);

                    if (!empty($available)) {

                            $locker = $l;

                    }

                }
            }
        return $locker;
    }

    public function setSession(Request $request){
        Session::put('selectedrows', $request->selectedrows);
        return response()->json('s');
    }

    public function blockValidation(Request $request){
        if(date("m") < 7  ){
            $start = Carbon::now()->toDateTimeString();
            $end = Carbon::now()->endOfDay()->toDateTimeString();
        } else {
            $start = date("Y-09-01 00:00:00");
            $end = date("Y-09-01 23:59:59");

        }
        $book_next_year = false;
        $moveable_lockers = [];

        $used_lockers = [];
        $can_move = '<h3>Can Move</h3><br>';
        $can_not_move = '<h3>Can\'t Move</h3><br>';
        $is_empty_can_not_move = true;

        foreach ($request->ids as $data){
            $block = BlockYears::where('company_id', $request->company_id)->where('school_academic_year_id', $request->block_id)->first();
            $locker= $this->total_available_lockers_in_block($block->id,$start,$end,$book_next_year,$used_lockers);
            $sale = Sale::find($data);

            if($locker){
                $locker_data = [];
                array_push($used_lockers,$locker->id);
                array_push($locker_data,$sale->id);
                array_push($locker_data,$block->id);
                array_push($locker_data,$locker->id);
                array_push($locker_data,$locker->locker_number);
                array_push($moveable_lockers,$locker_data);

                $can_move .=    '<div class="row" style="margin-left: 3px">
                                    <p>Locker '.$sale->number.' Block '.$sale->block_id.' - Moved to '.$locker->locker_number.' Block '.$locker->block_id.'  </p>
                                </div>';
            }else{
                $can_not_move .=    '<div class="row" style="margin-left: 3px">
                                        <p>Locker '.$sale->number.' Block '.$sale->block_id.'</p>
                                    </div>';
                $is_empty_can_not_move = false;
            }
        }
        if($is_empty_can_not_move == true)
            $can_not_move .= '<p style="margin-left: 3px">None</p>';

        $done_html = '<div class="checkbox">            
                                <br>
                                <label>
                                    <input type="checkbox" value="" id="multi_done" checked>
                                    <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                    Done
                                </label>
                            </div>';
        $data = [

            'can_move' => $can_move,
            'can_not_move' =>$can_not_move,
            'moveable_lockers' =>$moveable_lockers,
            'done_html' =>$done_html,

        ];


        return response()->json($data);
    }

    public function updateMultiDone(Request $request){
        if(date("m") < 7  ){
            $start = Carbon::now()->toDateTimeString();
            $end = Carbon::now()->endOfDay()->toDateTimeString();
        } else {
            $start = date("Y-09-01 00:00:00");
            $end = date("Y-09-01 23:59:59");

        }
        if(isset($request->moveable_lockers)) {
             foreach ($request->moveable_lockers as $data) {

        $sale = Sale::find($data[0]);
        $block = Block::find($data[1]);

        $locker_move = new LockerMove();
        $locker_move->sale_id = $sale->id;
        $locker_move->booking_id = $sale->booking_id;
        $locker_move->old_locker_id = $sale->locker_id;
        $locker_move->old_locker_number = $sale->number;
        $locker_move->new_locker_id = $data[2];
        $locker_move->new_locker_number = $data[3];
        $locker_move->save();

        $sale->block_id = $data[1];
        $sale->block_name = $block->name;
        $sale->locker_id = $data[2];
        $sale->school_academic_year_id = $request->school_year;
        $sale->number = $data[3];

        if ($request->checkbox_done == 'true') {
            $sale->checked = 1;
        }

        $sale->save();

        //update Manual locker code using sale_id
        $code = new Sale();
        $code->update_locker_code($data[0]);

         //add sales_audit log
         $sales_audit_obj = new SalesAudit();
         $sales_audit_obj->sales_audit_log($data[0],$type = 'validate');

        $hold_next_year_booking = Sale::where('hold_sales_id', $data[0])->first();

        if ($hold_next_year_booking) {

            $locker_move = new LockerMove();
            $locker_move->sale_id = $hold_next_year_booking->id;
            $locker_move->booking_id = $hold_next_year_booking->booking_id;
            $locker_move->old_locker_id = $hold_next_year_booking->locker_id;
            $locker_move->old_locker_number = $hold_next_year_booking->number;
            $locker_move->new_locker_id = $data[2];
            $locker_move->new_locker_number = $data[3];
            $locker_move->save();

            $hold_next_year_booking->locker_id = $data[2];
            $hold_next_year_booking->block_id = $data[1];
            $hold_next_year_booking->block_name = $block->name;
            $hold_next_year_booking->number = $data[3];
            $hold_next_year_booking->school_academic_year_id = $request->school_year;
            $hold_next_year_booking->save();

            $code = new Sale();
            $code->update_locker_code($hold_next_year_booking->id);

            //add sales_audit log
            $sales_audit_obj = new SalesAudit();
            $sales_audit_obj->sales_audit_log($hold_next_year_booking->id,$type = 'validate');

        }
    }
        }

        Session::forget('selectedrows');

        return response()->json($data);
    }
}
