<?php

namespace App\Http\Controllers\Backend;

use App\AcademicYearReport;
use App\FranchiseeCompany;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class AcademicYearController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//      $academ AcademicYearReport::all();
//        dd('s');
        return view('backend.pages.reports.academicYear.index');
    }


    public function show_all(Request $request){

        $data = $this->query_data($request);
        return Datatables::of($data)
            ->addColumn('lockers_sold_percentage',function ($data){
                if($data->lockers_sold_percentage < 0)
                    return  '<span style="color: red" >('.$data->lockers_sold_percentage.'%)</span>';
                else
                    return $data->lockers_sold_percentage.'%';
            })
            ->editColumn('change_from_last_year_percentage',function ($data){
                if($data->change_from_last_year_percentage < 0)
                    return  '<span style="color: red" >('. $data->change_from_last_year_percentage.'%)</span>';
                else
                    return $data->change_from_last_year_percentage.'%';

            })
            ->rawColumns(['lockers_sold_percentage', 'action'])
            ->rawColumns(['change_from_last_year_percentage', 'action'])
            ->make(true);
    }

    public function get_count(Request $request){

        $query = $this->query_data($request);
        $count =  $query->count();
        return response()->json($count);
    }

    public function get_data(Request $request)
    {

        $query = $this->query_data($request);
        $query->orderBy('year','ASC');
        if($request->page == 0) {

            $data = $query->take(1000)->get();
        }else{
            $skip = $request->page * 1000;
            $data = $query->skip($skip)->take(1000)->get();
        }
        $data = [
            'data' => $data
        ];

        return response()->json($data);
    }

    public function query_data($request){

//            $query = AcademicYearReport::query();
        $query =  AcademicYearReport::leftJoin('companies','companies.id','=','academic_year_reports.company_id');

        if (isset($request->search_company) && isset($request->search_year)) {
            $query->where('companies.name', 'LIKE', DB::raw("'%$request->search_company%'"));
            $query->where('academic_year_reports.year', $request->search_year);

        }else if (isset($request->search_company)) {
            $query->where('companies.name', 'LIKE', DB::raw("'%$request->search_company%'"));

        }else if (isset($request->search_year) && !empty($request->search_year)) {
            $query->where('academic_year_reports.year', $request->search_year);
        }else{
            if(date('m') > 7) {
                $query->where('academic_year_reports.year', Carbon::now()->format('Y'));
            }else{
                $query->where('academic_year_reports.year', Carbon::now()->subYear()->format('Y'));

            }

        }

        $acadmic_year_obj =  new AcademicYearReport();
        $query = $acadmic_year_obj->academic_year_record_permissions($query);

        $query->select('companies.name as company_name','academic_year_reports.*');


        return $query;


    }
}
