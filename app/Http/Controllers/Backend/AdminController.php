<?php
namespace App\Http\Controllers\Backend;
use App\EmailTemplateDistributionList;
use App\Http\Controllers\Controller;
use App\User;
use App\Locker;
use App\Sale;
use App\Company;
use App\ManualLockerCode;
use App\LockerMove;
use Session;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'clearance']);
    }
    public function index()
    {

       $total_messages = EmailTemplateDistributionList::count();


        return view('backend.pages.index',compact('total_messages'));
    }

    public function show(){
       $non_group_users = $users =User::paginate(10);
              $data = [
                  'status' => 'non-selected',
                  'non_group_users' => $non_group_users,
              ];
              $users = 'empty';

         // dd($non_group_users->links().'#HTML');   
         $pagination = $non_group_users->onEachSide(1)->links().'';

        Session::put('non_group_users', $pagination);

       
        return response()->json(['data' => $data, 'paginate' => $pagination]);
    }

    public function fixCodes($company_id){
      $company = Company::where('id', $company_id)->first();
      if($company){
          print_r("Fixing codes for ".$company->name ."\n");
          $lockersByCompany = Sale::where('company_id', $company_id)->get();
          $i = 0;
          $lockers_updated= array();
          foreach($lockersByCompany as $sale){
              $locker_id = $sale->locker_id;
              $manual_codes = ManualLockerCode::where('locker_id', $locker_id)->get();
              if(count($manual_codes) >=1 ){
                  $has_this_year_code = false;
                  $has_last_year_code = false;
                  $old_code = "";
                  $new_code = "";
                  $old_code_id = "";
                  $new_code_id = "";
                  
                  foreach($manual_codes as $code){
                    $start_date = $code->start;
                    if($start_date == "2020-09-01 00:00:00"){
                        $has_this_year_code = true;
                        $new_code = $code->code;
                        $new_code_id = $code->id;
                    }
                    if($start_date == "2019-09-01 00:00:00"){
                        $has_last_year_code = true;
                        $old_code = $code->code;
                        $old_code_id = $code->id;
                    }
                  }
                  
                  if($has_last_year_code){
                      if($has_this_year_code){
                        $update= ManualLockerCode::where('id', $new_code_id)->update([
                                'code' => $old_code,
                            ]);
                        $i++;
                        array_push($lockers_updated, $locker_id);
                      }
                  }
              }
          }
          print_r("lockers updated count = ".$i." and lockers updated are ");
          print_r($lockers_updated);
          print_r("Preparing to correct codes and send emails ...");
          
          $j = 0;
          $emails_resent_for_bookings = array();
          
          $this_year_sales = Sale::where('company_id', $company_id)->where('start', '2020-09-01 00:00:00')->where('status', 'b')->get();
          foreach($this_year_sales as $sale){
              $locker_id = $sale->locker_id;
              $manual_code = ManualLockerCode::where('locker_id', $locker_id)->where('start', '2020-09-01 00:00:00')->first();
              if(!isset($manual_code->code) || $sale->net_pass != $manual_code->code){
                  $locker_move = LockerMove::insert([
                        'sale_id' => $sale->id,
                        'booking_id' => $sale->booking_id,
                        'old_locker_id' => $sale->locker_id,
                        'old_locker_number' => $sale->number,
                        'new_locker_id' => $sale->locker_id,
                        'new_locker_number' => $sale->number,
                        'complete' => 0,
                        'created_at' => date("Y-m-d 21:00:00", strtotime("-1 day")),
                      ]);
                $j++;
                array_push($emails_resent_for_bookings, $sale->id);
              }
          }
          print_r("bookings updated count = ".$j." and booking email sent for are\n");
          print_r($emails_resent_for_bookings);
      } else {
          print_r("No company for this ID");
      }
    }

}
