<?php

namespace App\Http\Controllers\Backend;

use App\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class LanguagesController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $languages = Language::all()->sortByDesc("updated_at");;

        return view('backend.pages.languages.index')->with('languages', $languages);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('backend.pages.languages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|max:120',
            'locale'=>'required|max:2',

        ]);

        $key = str_replace(' ', '-', strtolower($request['key']));

        $label = new Language();
        $label->name = $request['name'];
        $label->native_name = $request['native_name'];
        $label->native_name = $request['native_name'];
        $label->flag = $request['flag'];
        $label->locale = $request['locale'];
        $label->canonical_locale = $request['canonical_locale'];
        $label->full_locale = $request['full_locale'];
        $label->status = $request['status'];
        $label->save();

        return redirect()->route('languages.index')
            ->with('flash_message',
                'Language successfully added.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $label = Language::where('id',$request['id'])->first();
        $label->name = $request['name'];
        $label->native_name = $request['native_name'];
        $label->native_name = $request['native_name'];
        $label->flag = $request['flag'];
        $label->locale = $request['locale'];
        $label->canonical_locale = $request['canonical_locale'];
        $label->full_locale = $request['full_locale'];
        $label->status = $request['status'];
        $label->save();
        return redirect()->route('languages.index')
            ->with('flash_message',
                'Language successfully edited.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
    public function editlabel($key , $lang)
    {
        $label = Language::where('key',$key)->where('lang',$lang)->first();
        return view('backend.pages.languages.edit', compact('label'));
    }
    public function destroylanguage ($key)
    {
        $label = Language::where('id',$key)->first();
        $label->delete();

        return redirect()->route('languages.index')
            ->with('flash_message',
                'Language successfully deleted.');
    }
    public function updateLanguageAjax(Request $request)
    {
        $label_url = $request->label_url;
        $label_title = $request->label_title;
        $label_detail = $request->label_detail;
        $label = Language::where('page_url', $label_url)->first();
        if(is_null($label)){
            $label = new Language();
            $label->title = $label_title;
            $label->detail = $label_detail;
            $label->page_url = $label_url;
            $label->save();
        }
        else{
            $label->title = $label_title;
            $label->detail = $label_detail;
            $label->save();
        }

        return 1;
    }
}
