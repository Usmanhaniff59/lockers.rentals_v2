<?php

namespace App\Http\Controllers\Backend;

use App\Label;
use App\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class LabelController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $labels = Label::all()->sortByDesc("updated_at");
        $langs = Language::where('status',1)->pluck('name','locale');

        return view('backend.pages.labels.index' ,compact('labels','langs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('backend.pages.labels.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'key'=>'required|max:120',
            'lang'=>'required',
            'value'=>'required',

        ]);

        $key = str_replace(' ', '-', strtolower($request['key']));

        $label = new Label();
        $label->key = $key;
        $label->lang = $request['lang'];
        $label->value = $request['value'];
        $label->save();

        return redirect()->route('labels.index')
            ->with('flash_message',
                'Label successfully added.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'value'=>'required',

        ]);

        $label = Label::where('key',$request['id'])->where('lang',$request['lang'])->first();
        $label->value = $request->value;
        $label->save();
        return redirect()->route('labels.index')
            ->with('flash_message',
                'Label successfully edited.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
    public function editlabel($key , $lang)
    {
        $label = Label::where('key',$key)->where('lang',$lang)->first();
        return view('backend.pages.labels.edit', compact('label'));
    }
    public function destroylabel($key , $lang)
    {
        $label = Label::where('key',$key)->where('lang',$lang)->first();
        $label->delete();

        return redirect()->route('labels.index')
            ->with('flash_message',
                'Label successfully deleted.');
    }
    public function updateLabelAjax(Request $request)
    {
        $label_url = $request->label_url;
        $label_title = $request->label_title;
        $label_detail = $request->label_detail;
        $label = Label::where('page_url', $label_url)->first();
        if(is_null($label)){
            $label = new Label();
            $label->title = $label_title;
            $label->detail = $label_detail;
            $label->page_url = $label_url;
            $label->save();
        }
        else{
            $label->title = $label_title;
            $label->detail = $label_detail;
            $label->save();
        }

        return 1;
    }
}
