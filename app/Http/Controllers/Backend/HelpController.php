<?php

namespace App\Http\Controllers\Backend;

use App\Help;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HelpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $helps = Help::all()->sortByDesc("updated_at");;

        return view('backend.pages.helps.index')->with('helps', $helps);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('backend.pages.helps.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'=>'required|max:120',
            'page_url'=>'required',

        ]);

        $help = Help::create($request->only('title', 'detail', 'page_url'));

        return redirect()->route('helps.index')
            ->with('flash_message',
                'User successfully added.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $help = Help::findOrFail($id);

        return view('backend.pages.helps.edit', compact('help'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = $request['id'];
        $help = Help::findOrFail($id);
        $this->validate($request, [
            'title'=>'required|max:120',
            'page_url'=>'required',

        ]);

        $input = $request->only(['title', 'detail', 'page_url']);
        $help->fill($input)->save();

        return redirect()->route('helps.index')
            ->with('flash_message',
                'help successfully edited.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $help = Help::findOrFail($id);
        $help->delete();

        return redirect()->route('helps.index')
            ->with('flash_message',
                'help successfully deleted.');
    }
    public function updateHelpAjax(Request $request)
    {
        $help_url = $request->help_url;
        $help_title = $request->help_title;
        $help_detail = $request->help_detail;
        $help = Help::where('page_url', $help_url)->first();
        if(is_null($help)){
            $help = new Help();
            $help->title = $help_title;
            $help->detail = $help_detail;
            $help->page_url = $help_url;
            $help->save();
        }
        else{
            $help->title = $help_title;
            $help->detail = $help_detail;
            $help->save();
        }

        return 1;
    }
}
