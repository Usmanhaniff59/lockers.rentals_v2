<?php

namespace App\Http\Controllers\Backend\Reports;

use App\VoucherCode;
use App\VoucherPaymentsReport;
use App\FranchiseeCompany;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class VoucherPaymentsReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//      $academ AcademicYearReport::all();
//        dd('s');
        return view('backend.pages.reports.voucherPayments.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show_all(Request $request){

        $data = $this->query_data($request);
        return Datatables::of($data)
            ->addColumn('full_name', function ($data) {
                if( isset($data->booking_id) )
                    return $data->full_name;
                else
                    return $data->email;


            })
            ->setRowClass(function ($data) {

                if($data->issued == 0)
                    return 'alert-danger';

                else {

                    if(isset($data->booking_id))
                        return 'alert-warning';

                    else
                        return 'alert-success';
                }
            })
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function query_data($request){
//         dd($request->record_type);


         if($request->record_type == 'all'){
             $query =  VoucherCode::leftJoin('voucher_payments_reports','voucher_codes.id','=','voucher_payments_reports.voucher_code_id')
                 ->leftJoin('companies','companies.id','=','voucher_codes.company_id')
                 ->leftJoin('users','users.id','=','voucher_payments_reports.user_id')
                 ->leftJoin('lockers','lockers.id','=','voucher_payments_reports.locker_id');
         }
         else if($request->record_type == 'issued'){
             $query =  VoucherCode::leftJoin('voucher_payments_reports','voucher_codes.id','=','voucher_payments_reports.voucher_code_id')
                 ->leftJoin('companies','companies.id','=','voucher_codes.company_id')
                 ->leftJoin('users','users.id','=','voucher_payments_reports.user_id')
                 ->leftJoin('lockers','lockers.id','=','voucher_payments_reports.locker_id');
             $query->where('voucher_codes.issued',1);

         }else if($request->record_type == 'not_issued'){
             $query =  VoucherCode::leftJoin('voucher_payments_reports','voucher_codes.id','=','voucher_payments_reports.voucher_code_id')
                 ->leftJoin('companies','companies.id','=','voucher_codes.company_id')
                 ->leftJoin('users','users.id','=','voucher_payments_reports.user_id')
                 ->leftJoin('lockers','lockers.id','=','voucher_payments_reports.locker_id');
             $query->where('voucher_codes.issued',0);

         }else if($request->record_type == 'used'){
             $query = '';
             $query =  VoucherCode::join('voucher_payments_reports','voucher_codes.id','=','voucher_payments_reports.voucher_code_id')
                 ->join('companies','companies.id','=','voucher_codes.company_id')
                 ->join('users','users.id','=','voucher_payments_reports.user_id')
                 ->join('lockers','lockers.id','=','voucher_payments_reports.locker_id');
         }


         if (isset($request->search_bookingId) && !empty($request->search_bookingId)){
            $query->where('voucher_payments_reports.booking_id',$request->search_bookingId);
        }
        if (isset($request->search_company) ){
            $query->where('companies.name','LIKE', DB::raw("'%$request->search_company%'"));
        }
        if (isset($request->search_name) && !empty($request->search_name)){
            $search_name = $request->search_namel;
            $query->where(function ($q)use($search_name){
                $q->where('users.name','LIKE', DB::raw("'%$search_name%'"))
                    ->orWhere('users.surname','LIKE', DB::raw("'%$search_name%'"));
            });
        }

        if (isset($request->search_lockernum) && !empty($request->search_lockernum)){
            $query->where('lockers.locker_number',$request->search_lockernum);
        }

         $start_date = $request->search_start_date;
         $end_date = $request->search_end_date;
//
//         if (!empty($request->search_bookingId) || !empty($request->search_name)|| !empty($request->search_surname)|| !empty($request->search_lockernum)) {
//
//         }
//         else if (!empty($request->search_start_date) && !empty($request->search_end_date)){
//              $query->whereDate('voucher_payments_reports.start', '>=', $start_date)
//                  ->whereDate('voucher_payments_reports.end', '<=', $end_date);
//
//          }else if (!empty($request->search_start_date)){
//
//             $query->whereDate('voucher_payments_reports.start','>=' ,$start_date);
//         }
//         else if ( !empty($request->search_end_date)){
//
//             $query->whereDate('voucher_payments_reports.end','<=' ,$end_date);
//         }
//         else{
//             $start_date = Carbon::now()->toDateTimeString();
//             $end_date = Carbon::now()->endOfDay()->toDateTimeString();
//             $query->whereDate('voucher_codes.end','>=' ,$start_date)
//                 ->whereDate('voucher_codes.start','<=' ,$end_date);
//         }
//         else {
//             $start_date = date("Y-09-01 00:00:00");
//             $end_date =   date("Y-09-01 23:59:59");
//             $query->whereDate('voucher_payments_reports.end','>=' ,$start_date);
//             $query->whereDate('voucher_payments_reports.start','<=' ,$end_date);
//
//         }

        $voucher_obj = new  VoucherPaymentsReport();
         $query = $voucher_obj->voucher_report_record_permissions($query);


         $query =  $query->select('voucher_codes.*','companies.name as company_name',DB::raw("CONCAT(users.name,' ' ,users.surname) as full_name"),'lockers.locker_number','voucher_payments_reports.booking_id','voucher_payments_reports.booking_cost','voucher_payments_reports.voucher_cost');

        return $query;
    }
    public function get_count(Request $request){
//         dd($request->all());
        $query = $this->query_data($request);
//        $data = $this->query_data($request);
//        dd($data->first());
        $count =  $query->count();
        return response()->json($count);
    }

    public function get_data(Request $request)
    {
        // dd('dd');

        $query = $this->query_data($request);
        // $query->orderBy('year','ASC');
        if($request->page == 0) {

            $data = $query->take(1000)->get();
        }else{
            $skip = $request->page * 1000;
            $data = $query->skip($skip)->take(1000)->get();
        }
        $data = [
            'data' => $data
        ];

        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VoucherPaymentsReport  $voucherPaymentsReport
     * @return \Illuminate\Http\Response
     */
    public function show(VoucherPaymentsReport $voucherPaymentsReport)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VoucherPaymentsReport  $voucherPaymentsReport
     * @return \Illuminate\Http\Response
     */
    public function edit(VoucherPaymentsReport $voucherPaymentsReport)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VoucherPaymentsReport  $voucherPaymentsReport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VoucherPaymentsReport $voucherPaymentsReport)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VoucherPaymentsReport  $voucherPaymentsReport
     * @return \Illuminate\Http\Response
     */
    public function destroy(VoucherPaymentsReport $voucherPaymentsReport)
    {
        //
    }
}
