<?php

namespace App\Http\Controllers\Backend\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;
use App\BlockGroupCode;
use App\BlockGroup;

use Illuminate\Support\Facades\DB;
class LockerNumberReportController extends Controller
{
      public function index()
    {
        $BlockGroup = BlockGroup::where('status',1)->get();
        // $rates = Rate::orderBy('order', 'ASC')->get();
        // $school_academic_year = SchoolAcademicYear::orderBy('order_number', 'asc')->get();

        return view('backend.pages.reports.lockerNumber.index',compact('BlockGroup'));
    }
        public function show_all(Request $request)
    {
        // $query = $this->availability_query($request);
        // $locker = $query[0];
        // $date_passed = $query[1];
        // $this_month_start_date = $query[2];
        // $this_month_end_date = $query[3];

        return $this->availability_dataTable($request);

    }
    
     public function availability_dataTable($request)
    {


  $lockerReport = $this->availability_query($request);

        return Datatables::of($lockerReport)
            ->addColumn('blockName', function ($lockerReport) {
                    return $lockerReport->blockgroup['name'];
            })

            ->addColumn('lockerNumber', function ($lockerReport) {
              
                return $lockerReport->locker_number;
            })->addColumn('StartDate', function ($lockerReport) {
              	$s_date=date('F jS,Y', strtotime($lockerReport->start_date));
                return $s_date;
            })->addColumn('end_date', function ($lockerReport) {
              
               $e_date=date('F jS,Y', strtotime($lockerReport->end_date));
                return $e_date;
            })->addColumn('code', function ($lockerReport) {
                return $lockerReport->code;
            })->make(true);


    }
        public function getavailabilityreportcount(Request $request)
    {

        $query = $this->availability_query($request);
        $locker = $query;
        $count = $locker->count();

//        $count = 5;
        return response()->json($count);
    }
     public function availability_query($request)
    {

        $name=$request->lockerGroup;

    	     $lockerReport = BlockGroupCode::with('blockgroup')->whereHas('blockgroup', function ($query) use ($name) {
                if(isset($name))
                {
                  return  $query->where('name','LIKE',DB::raw("'%$name%'"))->where('status', '=', 1)->orderBy('order_by', 'asc');
                }else{
    return $query->where('status', '=', 1)->orderBy('order_by', 'asc');
}

});
     if(isset($request->search_start_date) && !empty($request->search_start_date))
     {
     	
    	 			 $c_date=date_create($request->search_start_date);
                     $date=date_format($c_date,"Y-m-d H:i:s");
     	$lockerReport->where('start_date','<=',$date);
     	$lockerReport->where('end_date','>=',$date);

     }
     if(isset($request->lockerNumber) && !empty($request->lockerNumber)){
        $lockerReport->where('locker_number',$request->lockerNumber); 
     }
     if(isset($request->search_input) && !empty($request->search_input))
     {
     	

     	$lockerReport->where('locker_number',$request->search_input);
     }
    $lockerReport->get();

    return $lockerReport;

    }


 public function getavailabilityreport(Request $request)
    {

        $query = $this->availability_query($request);
        $locker = $query;

         if ($request->page == 0) {
            $data = $locker->take(1000)->get();
        } else {
            $skip = $request->page * 1000;
            $data = $locker->skip($skip)->take(1000)
                ->get();
        }

        $data = [
            'data' => $data
        ];

        return response()->json($data);
    }

}
