<?php

namespace App\Http\Controllers\Backend\Reports;

use App\CharityReport;
use App\FranchiseeCompany;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use App\User;

class CharityReportController extends Controller
{
    public function index()
    {
        return view('backend.pages.reports.charityReport.index');
    }
    public function update(Request $request)
    {
        CharityReport::where('id',$request->id)
        ->update(['charity_processed' => $request->status]);
        return response()->json('success');
    }
    public function show_all(Request $request){
        $data = $this->query_data($request);
        return Datatables::of($data)
            ->addColumn('month',function($data){
                if($data->month == '1')
                    return 'January';
                elseif($data->month == '2')
                    return 'February';

                elseif($data->month == '3')
                    return 'March';

                elseif($data->month == '4')
                    return 'April';

                elseif($data->month == '5')
                    return 'May';

                elseif($data->month == '6')
                    return 'June';

                elseif($data->month == '7')
                    return 'July';

                elseif($data->month == '8')
                    return 'August';

                elseif($data->month == '9')
                    return 'September';

                elseif($data->month == '10')
                    return 'October';

                elseif($data->month == '11')
                    return 'November';

                elseif($data->month == '12')
                    return 'December';

                else
                    return '';
            })
            ->addColumn('charity_revenue_percentage', function ($data) {
                return $data->charity_revenue_percentage.'%';
            })
            ->addColumn('action', function ($data) {
                if($data->charity_processed == '1')
                     return '<input type="checkbox"class="checkbox" checked data-id="'.$data->id.'" >';

                else
                    return '<input type="checkbox" class="checkbox" data-id="'.$data->id.'" >';

//                if($data->charity_processed == '1')
//                return '<div class="form-group">
//                                                <div class="radio_basic_swithes_padbott">
//                                                    <input type="checkbox" class="js-switch sm_toggle_add'.$data->id.'" name="roles"  />
//                                                    <span class="radio_switchery_padding"></span>
//                                                    <br />
//                                                </div>
//                                            </div>';

            })
            ->setRowClass(function ($data) {

                $charity_processed = $data->charity_processed;

                //dd($active, $display);
                if($charity_processed == 1 ){
                    return 'alert-success';
                } else {
                    return 'alert-warning';
                }
            })
        ->make(true);
    }
    public function query_data($request){
           $query = CharityReport::query();
         if (isset($request->search_year) && !empty($request->search_year)){
            $query->where('year',$request->search_year);
        }
        if (isset($request->search_month) ){
            $query->where('month',$request->search_month);
        }
        $query->orderBy('year','ASC');
        $query->orderBy('currency','DESC');
        $query->orderBy('month','DESC');
        return $query;
    }
     public function get_count(Request $request){
        $query = $this->query_data($request);
        $count =  $query->count();
        return response()->json($count);
    }

    public function get_data(Request $request)
    {
        $query = $this->query_data($request);
        if($request->page == 0) {
            $data = $query->take(1000)->get();
        }else{
            $skip = $request->page * 1000;
            $data = $query->skip($skip)->take(1000)->get();
        }
        $data = [
            'data' => $data
        ];
        return response()->json($data);
    }

   
}
