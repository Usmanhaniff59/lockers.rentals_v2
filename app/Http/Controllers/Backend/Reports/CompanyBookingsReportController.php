<?php


namespace App\Http\Controllers\Backend\Reports;

use App\CompanyBookingsReport;
use App\FranchiseeCompany;
use App\Sale;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use DB;


class CompanyBookingsReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//      $academ AcademicYearReport::all();
//        dd('s');
        return view('backend.pages.reports.companyBooking.index');
    }
    public function get_count(Request $request){

        $query = $this->query_data($request);
        $count =  $query->count();
        return response()->json($count);
    }
    public function get_data(Request $request)
    {

        $query = $this->query_data($request);
        // $query->orderBy('year','ASC');
        if($request->page == 0) {

            $data = $query->take(1000)->get();
        }else{
            $skip = $request->page * 1000;
            $data = $query->skip($skip)->take(1000)->get();
        }
        $data = [
            'data' => $data
        ];

        return response()->json($data);
    }

    public function show_all(Request $request){
        $data = $this->query_data($request);
        return Datatables::of($data)
            ->addColumn('lockers_sold_percentage',function ($data){
                if($data->lockers_sold_percentage < 0)
                    return  '<span style="color: red" >('.$data->lockers_sold_percentage.'%)</span>';
                else
                    return $data->lockers_sold_percentage.'%';
            })
            ->editColumn('change_from_last_year_percentage',function ($data){
                if($data->change_from_last_year_percentage < 0)
                    return  '<span style="color: red" >('. $data->change_from_last_year_percentage.'%)</span>';
                else
                    return $data->change_from_last_year_percentage.'%';

            })
            ->rawColumns(['lockers_sold_percentage', 'action'])
            ->rawColumns(['change_from_last_year_percentage', 'action'])
            ->make(true);
    }
    public function query_data($request){

         $query =  Sale::join('companies','companies.id','=','sales.company_id')
                    ->join('users','users.id','=','sales.user_id');
           
        if (isset($request->search_schoolname)){
            $query->where('companies.name','LIKE', DB::raw("'%$request->search_schoolname%'"));
        }
        if (isset($request->search_name)){
            $query->where('users.name','LIKE', DB::raw("'%$request->search_name%'"));
        }
        if (isset($request->search_surname) ){
            $query->where('users.surname','LIKE', DB::raw("'%$request->search_surname%'"));
        }
        if (isset($request->search_email) ){
            $query->where('users.email',$request->search_email);
        }
        if (!empty($request->search_schoolname) || !empty($request->search_name)|| !empty($request->search_surname)|| !empty($request->search_email)) {

        } else if(date("m") < 7  ){
            $start_date = Carbon::now()->toDateTimeString();
            $end_date = Carbon::now()->endOfDay()->toDateTimeString();
            $query->whereDate('sales.end','>=' ,$start_date)
                ->whereDate('sales.start','<=' ,$end_date);
        } else {
            $start_date = date("Y-09-01 00:00:00");
            $end_date =   date("Y-09-01 23:59:59");
            $query->whereDate('sales.end','>=' ,$start_date);
            $query->whereDate('sales.start','<=' ,$end_date);

        }
        $company_booking_report_obj = new CompanyBookingsReport();
        $query = $company_booking_report_obj->company_booking_record_permissions($query);

        $query->select('companies.name as company_name','users.name','users.surname','users.email','users.phone_number','users.address_1',DB::raw("CONCAT(sales.booking_id,'(' ,DATE_FORMAT(sales.start, '%Y'),')') AS bookings"));

        return $query;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompanyBookingsReport  $companyBookingsReport
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyBookingsReport $companyBookingsReport)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompanyBookingsReport  $companyBookingsReport
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyBookingsReport $companyBookingsReport)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompanyBookingsReport  $companyBookingsReport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanyBookingsReport $companyBookingsReport)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompanyBookingsReport  $companyBookingsReport
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyBookingsReport $companyBookingsReport)
    {
        //
    }
}
