<?php

namespace App\Http\Controllers\Backend\Reports;

use App\PupilPremiumReport;
use App\FranchiseeCompany;
use App\Sale;
use App\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;
use DB;

class PupilPremiumReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return view('backend.pages.reports.pupilPremiumReport.index');
    }
    public function show_all(Request $request){
        $data = $this->query_data($request);
        return Datatables::of($data)
            ->addColumn('childName', function ($data) {
                 $user_obj = new User();
               return $user_obj->first_letter_capital($data->childName);
            })
            ->make(true);
    }
    public function query_data($request){
        $search=$request->searchParam;
        $searchStatus=$request->searchStatus;
        $searchDate=$request->searchDate;

                if(isset($searchStatus) && $searchStatus!='undefined' && $searchStatus=='Booked')
                {
                    $status_search=['sales.status','=','b'];
                }else if($searchStatus=='Lost')
                {
                    $status_search=['sales.status','=','l'];   
                }else{
                     $status_search=['sales.id', '!=', 0];
                }

                if(isset($searchDate) && $searchDate!='undefined' && !empty($searchDate))
                {
                      // $date = Carbon::createFromFormat('Y-m-d H:i:s', $searchDate);
                    $c_date=date_create($searchDate);
                     $date=date_format($c_date,"Y-m-d H:i:s");

                    $StartDateQuery=['sales.start','<=',$date];
                    $EndDateQuery=['sales.end','>=',$date];

                }else{
                    $StartDateQuery=['sales.id', '!=', 0];
                    $EndDateQuery=['sales.id', '!=', 0];

                }

        $query =  Sale:: Join('lockers', 'lockers.id', '=', 'sales.locker_id')
            ->Join('blocks', 'blocks.id', '=', 'sales.block_id')
            ->Join('companies', 'companies.id', '=', 'sales.company_id')
            ->where([['sales.title','LOCKER_M'],['sales.pupil_premium',1],$status_search,$StartDateQuery,$EndDateQuery]);

         if (isset($request->search_bookingId) && !empty($request->search_bookingId)){

            $query->where('sales.booking_id',$request->search_bookingId);
        }
        if (isset($request->search_name) ){
           $name =  $request->search_name;
            $query->where(function ($q)use($name){
                $q->where('sales.child_first_name','LIKE','%'.$name.'%')->orwhere('sales.child_surname','LIKE','%'.$name.'%');
            });
        }
        if (isset($request->search_company) ){
            $query->where('companies.name','LIKE','%'.$request->search_company.'%');
        }
        if (isset($request->search_block) ){
            $query->where('blocks.name','LIKE','%'.$request->search_block.'%');
        }
        if (isset($request->search_lockerNo) ){
            $query->where('lockers.locker_number',$request->search_lockerNo);
        }

          if (!empty($request->search_bookingId) || !empty($request->search_name)|| !empty($request->search_company)|| !empty($request->search_block)|| !empty($request->search_lockerNo)) {

          }else if(date("m") < 7  ){
              $start_date = Carbon::now()->toDateTimeString();
              $end_date = Carbon::now()->endOfDay()->toDateTimeString();
              $query->whereDate('sales.end','>=' ,$start_date)
                  ->whereDate('sales.start','<=' ,$end_date);
          } else {
              $start_date = date("Y-09-01 00:00:00");
              $end_date =   date("Y-09-01 23:59:59");
              $query->whereDate('sales.end','>=' ,$start_date);
              $query->whereDate('sales.start','<=' ,$end_date);

          }

          $pupil_obj = new PupilPremiumReport();
            $query = $pupil_obj->pupil_premium_record_permissions($query);

        $query->select('sales.id','sales.booking_id', 'companies.name as company_name', 'blocks.name as block_name', 'lockers.company_id', 'lockers.locker_number','sales.child_first_name','sales.child_surname',DB::raw("CONCAT(sales.child_first_name, ' ' ,sales.child_surname) AS childName"), DB::raw('DATE_FORMAT(sales.start, "%M %D, %Y %H:%m") as start'),DB::raw('DATE_FORMAT(sales.end, "%M %D, %Y %H:%m") as end'));

         return $query;
    }
    public function get_count(Request $request){
        $query = $this->query_data($request);
        $count =  $query->count();
        return response()->json($count);
    }

    public function get_data(Request $request)
    {

        $query = $this->query_data($request);

        $user_obj = new User();
        // $query->orderBy('year','ASC');
        if($request->page == 0) {

            $data = $query->take(1000)->get();

            //convert name first letter capital
            $data = $data->map(function ($item, $key) use($user_obj) {
                  $item->childName = $user_obj->first_letter_capital($item->childName);
                  return $item;
            });

        }else{
            $skip = $request->page * 1000;
            $data = $query->skip($skip)->take(1000)->get();

            //convert name first letter capital
            $data = $data->map(function ($item, $key) use($user_obj) {
                $item->childName = $user_obj->first_letter_capital($item->childName);
                return $item;
            });
        }

        $data = [
            'data' => $data
        ];

        return response()->json($data);
    }
    
}
