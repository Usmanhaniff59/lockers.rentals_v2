<?php

namespace App\Http\Controllers\Backend;

use App\Block;
use App\BlockYears;
use App\Company;
use App\Chat;
use App\ChatTypes;
use App\FranchiseeCompany;
use NcJoes\OfficeConverter\OfficeConverter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;

class CompanyChatController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function create()
    {
    //    
    }

 
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'detail' => ['required'],
            'type' => ['required'],
            'title' => ['required']
            
        ]);
        
        $user = Auth::user();
        $company_idd = Session::get('company_id_blocksss');
        $data=Chat::create([
            'details' => $request->detail,
            'title' => $request->title,
            'chat_type_id' => $request->type,
            'user_id' =>   $user->id,
            'company_id' => $company_idd
        ]);
        return response()->json($validatedData);
    }
    

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function upload(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    public function getChatTypes(){
        $data = ChatTypes::orderBy('id', 'asc')->get();
        return response()->json($data);
    }

    public function show_all(){
        $company_idd = Session::get('company_id_blocksss');
//        dd($company_idd);
        $company_chats = Chat::where('company_id', $company_idd)->orderBy('id', 'desc')->get();
        return Datatables::of($company_chats)
        ->addColumn('user_id', function ($company_chats) {
            $name = $company_chats->user->name. " " . $company_chats->user->surname;
            return $name;
            
        })->addColumn('chat_type_id', function ($company_chats) {
            if($company_chats->chat_type_id == '1')
                return 'Introduction Call';
            elseif($company_chats->chat_type_id == '2')
                return 'Meeting Arranged';

            elseif($company_chats->chat_type_id == '3')
                return 'Follow Up Call';

            elseif($company_chats->chat_type_id == '4')
                return 'Sale Complete';

            elseif($company_chats->chat_type_id == '5')
                return 'Courtesy Call';
            else
                return '';
        })
            ->addColumn('details', function ($company_chats) {
            $date= str_limit($company_chats->details, 30);
                return $date;
        })
            ->addColumn('created_at', function ($company_chats) {
            $date= $company_chats->created_at->format('d M, Y H:i');
                return $date;
        })
            ->addColumn('action', function ($company_chats) {

                $buttons ='';
                $buttons .= '<a href="#view_chat" class="btn btn-info pull-left button-style view-chat" id="view_chat_btn" data-toggle="modal" style="margin-right: 3px;"  data-id="'. $company_chats->id .'" >View</a>';
                return $buttons;
            })
            ->setRowClass(function ($chat_blocks) {
             return 'alert-success';
        
        }) ->make(true);
    }

// Chat Reports
    public function chatreport()
        {
            $chat_types = ChatTypes::get();
            return view('backend.pages.reports.chatreport.chatreports',compact('chat_types'));
        }

    public function chatreportshow(Request $request)
        {
//            dd($request->all());
            $company_chats = $this->query_data($request);

            return Datatables::of($company_chats)
             ->make(true);
        }

    public function query_data($request){

        $company_chats = Chat::join('users','users.id','=','chats.user_id')
            ->join('companies','companies.id','=','chats.company_id')
            ->join('chat_types','chat_types.id','=','chats.chat_type_id');
           
             // if(auth()->user()->hasRole('Franchisee'))
             //    {
             //        $franchisee_ids=DB::table('franchisee_user')->where('user_id',auth()->user()->id)->pluck('franchisee_id');
             //        $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id', $franchisee_ids)->pluck('company_id')->toArray();
             //        $company_chats->whereIn('companies.id', $franchisee_companies);
             //    }
        if (isset($request->search_company) && !empty($request->search_company)){
            $company_chats->where('companies.name','LIKE',DB::raw("'%$request->search_company%'"));
        };

        if (isset($request->search_company_type) && !empty($request->search_company_type)){
            if($request->search_company_type == 'all'){

            }else {
                $company_chats->where('chats.chat_type_id', $request->search_company_type);
            }
        };
        if (isset($request->search_details_chat) && !empty($request->search_details_chat)){
            $company_chats->where('chats.details','LIKE',DB::raw("'%$request->search_details_chat%'"));
        };
        if (isset($request->search_persons_name) && !empty($request->search_persons_name)){
            $search_persons_name = $request->search_persons_name;

            $company_chats ->where(function ($query) use ($search_persons_name) {
                $query->where('users.name','LIKE',DB::raw("'%$search_persons_name%'"))
                    ->orWhere('users.surname','LIKE',DB::raw("'%$search_persons_name%'"));
            });
        };

        if (isset($request->search_date) && !empty($request->search_date)){

            $search_date = date('Y-m-d',strtotime($request->search_date));

            $company_chats->whereDate('chats.created_at',$search_date);

        }


        $chat_obj =  new Chat();
        $company_chats = $chat_obj->chat_record_permissions($company_chats);


        $company_chats->select('companies.name as company_name' ,'chats.title' ,'chats.details' , 'chat_types.type',
            DB::raw('DATE_FORMAT(chats.created_at, "%M %D, %Y %H:%m") as created_at1')
            ,DB::raw('(users.name) as name1' ));
//        $company_chats->get();

        return  $company_chats;

    }

    public function chatCount(Request $request)
    {
        $query = $this->query_data($request);
        $count =  $query->count();

        return response()->json($count);
    }

    public function getData(Request $request)
    {
        $query = $this->query_data($request);
        if($request->page == 0) {
            $data = $query->take(1000)->get();
        }else{
            $skip = $request->page * 1000;
            $data = $query->skip($skip)->take(1000)->get();
        }
//        dd($query);

        $data = [
            'data' => $data
        ];

        return response()->json($data);
    }

    public function view(Request $request)
    {
       $chats = Chat::find($request->id);

        return response()->json($chats);
    }

}