<?php

namespace App\Http\Controllers\Backend;

use App\Company;
use App\FranchiseeCompany;
use Yajra\DataTables\DataTables;
use App\LocationGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;


class BlocksController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }


    public function get(){

        $role = DB::table('user_has_roles')->where('user_id',Auth::user()->id)->select('role_id')->first();
            if($role->role_id == 1) {
                $company = Company::query();
        }elseif(Auth::user()->franchisee_id != null){
                $company = Company::query();
                $company->join('franchisee_companies','franchisee_companies.company_id','=','companies.id')
                    ->where('franchisee_id',Auth::user()->franchisee_id)->select('companies.*')
                    ->orWhere('companies.id',Auth::user()->company_id);


        }else{
                $company =  Company::query();
                $company->Where('id',Auth::user()->company_id)
                ->select('companies.*');
        }

        return Datatables::of($company)
            ->addColumn('action', function ($company) {
                $buttons ='';
                if(auth()->user()->can('Edit Company')) {

                    $buttons = '<a href="#edit" class="btn btn-primary btn-md adv_cust_mod_btn edit-btn pull-left" data-toggle="modal" style="margin-right: 3px;" data-href="#edit" data-id="'. $company->id .'" data-name="'. $company->name .'">Editttttt</a>';
                }
                if(auth()->user()->can('Delete Company')) {
                    $buttons.='<a href = "JavaScript:Void(0);" onclick = "deleteData(\''.route('company.destroy', $company->id).'\',\''.csrf_token().'\',\'\',company_table,\'DELETE\')" class="btn btn-danger" > Delete</a >';
                }
                return $buttons;
            })
            ->make(true);
    }

}