<?php

namespace App\Http\Controllers\Backend;

use App\DistributionListGroup;
use App\DistributionListGroupMember;
use App\EmailTemplate;
use App\EmailTemplateDistributionList;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DistributionlistGroupController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'isAdmin']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $distributionLists = DistributionListGroup::all();
        $users = User::all();

        return view('backend.pages.distributionLists.index',compact('distributionLists','users'));
    }


    public function create()
    {

        return view('backend.pages.distributionLists.create');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|max:120'

        ]);

        $distributionList = DistributionListGroup::create($request->only( 'name'));
        return redirect()->route('distributionlists.index')
            ->with('flash_message',
                 'Distribution list Group Added!');


    }


    public function show($id)
    {
//        $distributionLists = DistributionListGroup::where('id',$id)->first();
//        $email_templates = EmailTemplate::all();
//        EmailTemplateDistributionList::where('')
//        $distributionLists_id = $id;

//        return view ('backend.pages.distributionLists.show', compact('distributionLists','email_templates'));
                    return response()->json('hi');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $distributionList = DistributionListGroup::findOrFail($id);
        $users = User::all();


        return view('backend.pages.distributionLists.edit', compact('distributionList','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd($request->all());
        $distributionList = DistributionListGroup::findOrFail($id);
        $this->validate($request, [
            'name'=>'required|max:120'

        ]);

        $input = $request->only(['name']);
        $distributionList->fill($input)->save();
        if($request['users']) {

            foreach ($request['users'] as $user_id) {
//            dd($user_id);
                $emailTDL = new EmailTemplateDistributionList;
                $emailTDL->email_template_id = $request['email_template_id'];
                $emailTDL->distribution_list_group_id = $request['distribution_list_group_id'];
                $emailTDL->user_id = $user_id;

                $user = User::where('id', $user_id)->first();

                $emailTDL->name = $user->name;
                $emailTDL->email = $user->email;
                $emailTDL->sms = 'from distribution list group edit';
                $emailTDL->save();
            }
        }
        return redirect()->route('distributionlists.index')
            ->with('flash_message',
                'Distribution list Group successfully edited.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $distributionList = DistributionListGroup::findOrFail($id);
        $distributionList->delete();

        return redirect()->route('distributionlists.index')
            ->with('flash_message',
                'User successfully deleted.');
    }
    public function getnondistributionusers(Request $request)
    {
          $members = DistributionListGroupMember::where('distribution_list_group_id',$request['dist_group_id'])
              ->pluck('user_id');
          $users =User::whereNotIn('id',$members)->get();
//        $distributionList->delete();

        return response()->json($users);
    }

    public function getdistributionusers(Request $request)
    {
          $emailTDLs = EmailTemplateDistributionList::where('email_template_id',$request['email_template_id'])
              ->where('distribution_list_group_id',$request['dist_group_id'])
              ->pluck('user_id');

        $users = EmailTemplateDistributionList::where('email_template_distribution_lists.email_template_id',$request['email_template_id'])
            ->where('email_template_distribution_lists.distribution_list_group_id', $request['dist_group_id'])
            ->leftJoin('users', 'users.id', '=', 'email_template_distribution_lists.user_id')
            ->select('users.id','users.name','users.email')
            ->get();
//          $users =User::whereIn('id',$emailTDLs)->get();
//        $distributionList->delete();

        return response()->json($users);
    }
    public function getdistributionusersview(Request $request)
    {
        $users = DistributionListGroupMember::where('distribution_list_group_id',$request['dist_group_id'])
            ->join('users','users.id','=','distribution_list_group_members.user_id')
            ->select('users.id','users.name','users.email')
            ->get();



        return response()->json($users);
    }
}
