<?php

namespace App\Http\Controllers\Backend;


use App\Block;
use App\Company;
use App\CronSchedule;
use App\DistributionListGroup;
use App\DistributionListGroupMember;
use App\Email;
use App\EmailTemplate;
use App\EmailTemplateAlt;
use App\EmailTemplateDistributionList;
use App\EmailTemplatePlaceHolder;
use App\Jobs\SendEmailJob;
use App\Mail\SendEmailTest;
use App\Sale;
use Session;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Queue\Jobs\Job;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;


class EmailTemplateController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'isAdmin']);
    }
    public function index()
    {
        $email_templates = EmailTemplate::all();
        return view('backend.pages.emailTemplates.index')->with('email_templates', $email_templates);
    }
    public function create()
    {
        // return response()->json('hii');
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'template_name'=>'required|max:120',
            'subject'=>'required|max:120',
            'template_text'=>'required'
        ]);
        $email_template = EmailTemplate::create($request->only('template_name', 'subject', 'template_text'));
        return redirect()->route('emailtemplates.index')
            ->with('flash_message',
                'Email Template successfully added.');
    }
    public function show($id)
    {
        $email_template_placeholders = EmailTemplatePlaceHolder::where('email_template_id' ,$id)->get();
        $email_template = EmailTemplate::findOrFail($id);
        return view ('backend.pages.emailTemplates.show', compact('email_template','email_template_placeholders'));
    }
    public function edit($id)
    {
        //
    }
    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $user = EmailTemplate::findOrFail($id);
        $user->delete();
        return redirect()->route('emailtemplates.index')
            ->with('flash_message',
                'User successfully deleted.');
    }
    public function getemailtemplate(Request $request)
    {
        $distributionLists = DistributionListGroup::all();
        $emailtemplate = EmailTemplate::findOrFail($request['template_id']);
        $email_template_placeholders = EmailTemplatePlaceHolder::where('email_template_id' ,$request['template_id'])->get();
        $standard_placeholders = EmailTemplatePlaceHolder::where('email_template_id' ,null)->get();
        $locations = Company::leftJoin('blocks','companies.id','=','blocks.company_id')
                   ->select('blocks.id',DB::raw('CONCAT(companies.name, " ", blocks.name) AS name'))
                    ->orderBy('blocks.id','ASC','companies.id','ASC')
                   ->get();

        $emailTemplateAlts =  EmailTemplateAlt::where('email_template_id',$request['template_id'])->get();
        if (Carbon::now()->month >= 8) {

            $this_start_month = Carbon::now()->startOfMonth()->format('F Y');
            $this_end_month = Carbon::now()->addYear()->startOfMonth()->subMonth()->format('F Y');

            $last_start_month = Carbon::now()->subYear()->startOfMonth()->format('F Y');
            $last_end_month = Carbon::now()->startOfMonth()->subMonth()->format('F Y');

        } else {
            $this_start_month = Carbon::now()->subYear()->startOfMonth()->format('F Y');
            $this_end_month = Carbon::now()->startOfMonth()->subMonth()->format('F Y');

            $last_start_month = Carbon::now()->subYear(2)->startOfMonth()->format('F Y');
            $last_end_month = Carbon::now()->subYear()->startOfMonth()->subMonth()->format('F Y');
        }
        $data = [
            'emailtemplate' => $emailtemplate,
            'standard_placeholders' => $standard_placeholders,
            'email_template_placeholders' => $email_template_placeholders,
            'distributionLists' => $distributionLists,
            'locations' => $locations,
            'this_start_month' => $this_start_month,
            'this_end_month' => $this_end_month,
            'last_start_month' => $last_start_month,
            'last_end_month' => $last_end_month,
            'emailTemplateAlts' => $emailTemplateAlts,
        ];
        return response()->json($data);
    }
    public function updateemailtemplate(Request $request)
    {
        $emailtemplate = EmailTemplate::findOrFail($request['template_id']);
        $input = $request->only(['template_name', 'subject', 'template_text', 'sms_message']);
        $emailtemplate->fill($input)->save();

        if($request['group_membrs_data']) {

            $group_membrs =  explode(',', $request['group_membrs_data']);

            foreach ($group_membrs as $groups_member) {

                $user = User::where('id', $groups_member)->first();

                $emailTDL = new EmailTemplateDistributionList();
                $emailTDL->email_template_id = $request['template_id'];
                $emailTDL->user_id = $groups_member;
                $emailTDL->name = $user->name;
                $emailTDL->email = $user->email;
                $emailTDL->sms = 'coming from email template updateemailtemplate';
                $emailTDL->save();
            }
        }
        if($request['non_group_users'] ) {
            foreach ($request['non_group_users'] as $non_group_user) {
                $user = User::where('id', $non_group_user)->first();
                $emailTDL = new EmailTemplateDistributionList();
                $emailTDL->email_template_id = $request['template_id'];
                $emailTDL->user_id = $non_group_user;
                $emailTDL->name = $user->name;
                $emailTDL->email = $user->email;
                $emailTDL->sms = 'coming from email template updateemailtemplate';
                $emailTDL->save();
            }
        }
        if($request['filtered-users']) {
            $email_users =  explode(',', $request['filtered-users']);
            foreach ($email_users as $email_user) {
                $to = $email_user;
                $subject = $request['subject'];
                $email_html = $request['template_text'];
                $email = new Email();
                $email->email = $to;
                $email->action = 'sent';
                $email->subject = $subject;
                $email->mail_body = $email_html;
                $email->date_added = Carbon::now();
                $email->type = 'email';
                $email->result_code = '0';
                $email->star = '0';
                $email->send = '0';
                $email->complete = '0';
                $email->deleted = '0';
                $email->save();
            }
            $cron = new CronSchedule();
            $cron->class = 'Email';
            $cron->controller = 'EmailTemplate';
            $cron->where_check = '0';
            $cron->due = Carbon::now();
            $cron->completed = '0';
            $cron->status = 'Pending';
            $cron->save();
        }

        return redirect()->route('emailtemplates.index')
            ->with('flash_message',
                'template edited successfully.');

        //        $email_template = EmailTemplate::findOrFail($request['template_id']);
//        $placeholders = EmailTemplatePlaceHolder::where('email_template_id',$request['template_id'])->get();

//        $placeholders = ["%location%", "%town%", "%first_name%", "%surname%", "%email%", "%notes%"];
//        $placeholder_values   = [$request['location'], $request['town'], $request['first_name'], $request['surname'], $request['email'], $request['notes']];
//        $email_html = str_replace($placeholders, $placeholder_values, $template_text);
//        $template_text =$email_template->template_text;
//        $name =$request['name'];

//        $bodytag = str_replace("%name%", $name, $template_text);
    }
    public function getEmailtemplateAlt(Request $request)
    {
        $emailtemplate = EmailTemplateAlt::where('id',$request['email_template_alt_id'])->first();
        return response()->json($emailtemplate);
    }
    public function sendtestemail(Request $request)
    {
//        dd($request->all());
//        $emailtemplate = EmailTemplate::findOrFail($request['template_id']);
//        $input = $request->only(['template_name', 'subject', 'template_text']);
//        $emailtemplate->fill($input)->save();


        $user = Auth::user();
        $name = $user->name;
        $surname = $user->surname;
        $email = $user->email;
//        $email = 'sufyanhashmi301@gmail.com';
        $link = '<a href="https://www.w3schools.com" target="_blank">test link</a>';
//        dd($user->email);

        $placeholders = EmailTemplatePlaceHolder::where('email_template_id' , $request['template_id'])->orderBy('id','ASC')->pluck('placeholder')->toArray();
//dd($placeholders);
        //        $email_template = EmailTemplate::findOrFail($request['template_id']);
//        $placeholders = EmailTemplatePlaceHolder::where('email_template_id',$request['template_id'])->get();

//        $placeholders = ["%firs_name%", "%surname%", "%link%"];
        $placeholder_values   = [$name, $surname, $link];
        $email_html = str_replace($placeholders, $placeholder_values, $request['template_text']);
//        dd($email_html);
//        $template_text =$email_template->template_text;
//        $name =$request['name'];

//        $bodytag = str_replace("%name%", $name, $template_text);
        $email_text = new SendEmailTest($email_html);

        $mail = Mail::to($email)->send($email_text);
        return response()->json('success');

    }
    public function updateSystemFunction(Request $request)
    {
//        dd($request->all());
        $emailtemplate = EmailTemplate::findOrFail($request['template_id']);
        $input = $request->only(['template_name', 'subject', 'template_text']);
        $emailtemplate->fill($input)->save();
        return response()->json('success');
    }

    public function templatedistributionusers(Request $request)
    {

          if(isset($request['distribution_groups'])) {
//                $users = EmailTemplateDistributionList::where('email_template_id', $request['template_id'])
//                    ->WhereIn('distribution_list_group_id', $request['distribution_groups'])
//                    ->get();

              $groups_users = DistributionListGroupMember::WhereIn('distribution_list_group_members.distribution_list_group_id', $request['distribution_groups'])
                  ->leftJoin('users', 'users.id', '=', 'distribution_list_group_members.user_id')
                  ->select('users.id','users.name','users.email')
                  ->distinct()->get();

              $groups_users_id = DistributionListGroupMember::WhereIn('distribution_list_group_members.distribution_list_group_id', $request['distribution_groups'])
                  ->leftJoin('users', 'users.id', '=', 'distribution_list_group_members.user_id')
                  ->distinct()->pluck('users.id');

              $non_group_users = $users =User::whereNotIn('id',$groups_users_id)->paginate(10);

              $data = [
                  'status' => 'selected',
                  'groups_users_id1' => $groups_users_id,
                  'groups_users' => $groups_users,
                  'non_group_users' => $non_group_users,
              ];
          }else{
              $non_group_users = $users =User::paginate(10);
              $data = [
                  'status' => 'non-selected',
                  'non_group_users' => $non_group_users,
              ];
              $users = 'empty';
          }
   $pagination = $non_group_users->onEachSide(1)->links().'';

       
        return response()->json(['data' => $data, 'paginate' => $pagination]);
    }


    public function getsales(Request $request)
    {
//        dd($request->all());
        if(isset($request['blocks']) || isset($request['currentYear']) || isset($request['previouseYear']) || $request['reservationDate'] != null ) {
            $query = Sale::select('*');
            if (isset($request['blocks'])) {
                $query = $query->whereIn('block_id', $request['blocks']);
            }
            if (isset($request['currentYear']) && !isset($request['previouseYear']) && $request['reservationDate'] == null ) {
//                dd('currentYear');
                if (Carbon::now()->month >= 8) {

                    $start = Carbon::now()->startOfMonth()->format('Y-m-d');
                    $end = Carbon::now()->addYear()->startOfMonth()->format('Y-m-d');

                    $query = $query->where(function ($q) use ($start, $end) {
                        $q->WhereDate('start', '>=', $start)
                            ->whereDate('end', '<', $end);
                    });
//                    dd($request->all());
                } else {
                    $start = Carbon::now()->subYear()->startOfMonth()->format('Y-m-d');
                    $end = Carbon::now()->startOfMonth()->format('Y-m-d');

                    $query = $query->where(function ($q) use ($start, $end) {
                        $q->WhereDate('start', '>=', $start)
                            ->whereDate('end', '<', $end);
                    });

                }
            }
            if(isset($request['previouseYear']) && !isset($request['currentYear']) && $request['reservationDate'] == null) {
//                dd('previouseYear');
                if(Carbon::now()->month >= 8){
                    $start = Carbon::now()->subYear()->startOfMonth()->format('Y-m-d');
                    $end = Carbon::now()->startOfMonth()->format('Y-m-d');

                  $query = $query->where(function($q) use ($start ,$end) {
                      $q->WhereDate('start', '>=', $start)
                          ->whereDate('end', '<', $end);
                  });
              }else{
                    $start = Carbon::now()->subYear(2)->startOfMonth()->format('Y-m-d');
                    $end = Carbon::now()->subYear()->startOfMonth()->format('Y-m-d');

                  $query = $query->where(function($q) use ($start ,$end) {
                      $q->WhereDate('start', '>=', $start)
                          ->whereDate('end', '<', $end);
                  });

              }


          }
            if(isset($request['previouseYear']) && isset($request['currentYear']) && $request['reservationDate'] == null) {
//                dd('both');
                if(Carbon::now()->month >= 8){

                    $start = Carbon::now()->subYear()->startOfMonth()->format('Y-m-d');
                    $end = Carbon::now()->addYear()->startOfMonth()->format('Y-m-d');

                  $query = $query->where(function($q) use ($start ,$end) {
                      $q->WhereDate('start', '>=', $start)
                          ->whereDate('end', '<', $end);
                  });
              }else{
                    $start = Carbon::now()->subYear(2)->startOfMonth()->format('Y-m-d');
                    $end = Carbon::now()->startOfMonth()->format('Y-m-d');

                  $query = $query->where(function($q) use ($start ,$end) {
                      $q->WhereDate('start', '>=', $start)
                          ->whereDate('end', '<', $end);
                  });

              }


          }

          if ($request['reservationDate'] != null) {

                $dates = $request['reservationDate'];

              $str_arr = explode(" - ", $dates);
              $start_date1 = $str_arr[0];
              $start_date1 = explode("/", $start_date1);
              $day = $start_date1[0];
              $month = $start_date1[1];
              $year = $start_date1[2];
              $start = $year .'-'. $month .'-' . $day ;

              $end_date1 = $str_arr[1];
              $end_date = explode("/", $end_date1);
              $day = $end_date[0];
              $month = $end_date[1];
              $year = $end_date[2];
              $end = $year .'-'. $month .'-' . $day ;


                $query = $query->where(function ($q) use ($start, $end) {
                    $q->WhereDate('start', '>=', $start)
                        ->whereDate('end', '<=', $end);
                });


            }

            $sales = $query->get();


        $data = [
                'status' => 'success',
                'sales' => $sales,
               ];

        }
        else{
            $data = [
                'status' => 'empty',

            ];
        }
        return response()->json($data);
    }

}
