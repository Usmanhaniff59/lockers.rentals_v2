<?php

namespace App\Http\Controllers\Backend;

use App\DistributionListGroup;
use App\EmailTemplateDistributionList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmailTemplateDistributionListController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.emailTempDistList.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $this->validate($request, [
            'email_template_id'=>'required',
            'distribution_list_group_id'=>'required',
            'name'=>'required|max:120',
            'email'=>'required',
            'sms'=>'required',

        ]);
//          foreach ($distListGroups as $distListGroup) {
        $email_template = EmailTemplateDistributionList::create([
            'email_template_id' => $request['email_template_id'],
            'distribution_list_group_id' => $request['distribution_list_group_id'],
            'name' => $request['name'],
            'email' => $request['email'],
            'sms' => $request['sms'],
        ]);
//          }
//        return Redirect::back();
        return redirect()->route('distributionlists.show',2)
            ->with('flash_message',
                'Email Template successfully added.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $email_template = DistributionListGroup::findOrFail($id);

        return view ('backend.pages.emailTemplates.show', compact('email_template','email_template_placeholders'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
