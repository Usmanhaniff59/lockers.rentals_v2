<?php

namespace App\Http\Controllers\Backend;

use App\Block;
use App\BlockYears;
use App\Company;
use App\FranchiseeCompany;
use App\FrontendPlaceholder;
use App\Locker;
use App\Rate;
use App\Sale;
use App\CompanySchoolYear;
use App\SalesAudit;
use App\SchoolAcademicYear;
use App\SchoolYear;
use App\User;
use App\VoucherCode;
use App\VoucherCodeUser;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use DB;
class ReservedLockersController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('backend.pages.ReservedOrders.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function show_all(Request $request){
//        ini_set('memory_limit', '-1');
        $sale = Sale::query();
//        $sale = Sale::take(5);
        $sale->with('company','user','block');
        $sale->where('status', 'r')->where( 'hold_booking_date', '<=', Carbon::now()->addDays(45));
        $search_input = $request->search_input;
//        dd($search_input);

        $user = Auth::user();
        $franchisees = $user->franchisee()->pluck('franchisee_id')->toArray();
//        dd($franchisees);
        Session::put('reserved-lockers',$search_input);
//        if(auth()->user()->can('3D Secure')) {

            if (auth()->user()->can('All Records')) {
                if (isset($request->search_company) && !empty($request->search_company)){
                    $sale->WhereHas('company',function ($query) use ($request) {
                        $query->where('name','LIKE',DB::raw("'%$request->search_company%'"));
                    });
                }
                if (isset($request->search_block) && !empty($request->search_block)){
                    $sale->WhereHas('block',function ($query) use ($request) {
                        $query->where('name', 'LIKE',DB::raw("'%$request->search_block%'"));
                    });
                }
                if (isset($request->search_date) && !empty($request->search_date)){
                     $date = date('Y-m-d',strtotime($request->search_date));
                    
                    $sale->where('start','<=' ,$date)->where('end','>=' ,$date);
                }
                if (isset($request->search_bookingId) && !empty($request->search_bookingId)){
                    $sale->where('booking_id',$request->search_bookingId);
                }
                if (isset($request->search_email) && !empty($request->search_email)){
                    $sale->where('email','LIKE',DB::raw("'%$request->search_email%'"));
                }
                if (isset($request->search_name) && !empty($request->search_name)){
                    $sale->Where(DB::raw("CONCAT(`child_first_name`, ' ', `child_surname`)"), 'LIKE', "%".$request->search_name."%");
                }
                if (isset($search_input) && !empty($search_input)){
//                    dd($search_input);
                    $string=explode(' ',$search_input);
                    if (isset($string[0])){
                        $search_input=$string[0];
                    }else{
                        $search_input=$search_input;
                    }
                    if (isset($string[1])){
                        $search_input=$string[1];
                    }else{
                        $search_input=$search_input;
                    }
                    if (isset($string[2])){
                        $search_input=$string[2];
                    }else{
                        $search_input=$search_input;
                    }

                    $sale->WhereHas('user',function ($query) use ($request,$search_input) {
                        $query->orWhere('name', 'LIKE',DB::raw("'%$search_input%'"))
                        ->orWhere('email', 'LIKE',DB::raw("'%$search_input%'"));
                    });
                }

            } else if(auth()->user()->hasRole('User'))
                {
                          $sale->Where(function ($query) use($request){
                    $query->where('user_id', Auth::user()->id);
                });

                if (isset($request->search_company) && !empty($request->search_company)){
                    $sale->WhereHas('company',function ($query) use ($request) {
                        $query->where('name','LIKE',DB::raw("'%$request->search_company%'"));
                    });
                }
                if (isset($request->search_block) && !empty($request->search_block)){
                    $sale->WhereHas('block',function ($query) use ($request) {
                        $query->where('name', 'LIKE',DB::raw("'%$request->search_block%'"));
                    });
                }
                if (isset($request->search_date) && !empty($request->search_date)){
                    $date = $request->search_date;
                    $sale->where(function($q) use ($date) {
                        $q->WhereDate('start', '<=', $date)
                            ->whereDate('end', '>=', $date);
                    });
                }
                if (isset($request->search_bookingId) && !empty($request->search_bookingId)){
                    $sale->where('booking_id',$request->search_bookingId);
                }
                if (isset($request->search_email) && !empty($request->search_email)){
                    $sale->where('email','LIKE',DB::raw("'%$request->search_email%'"));
                }
                if (isset($request->search_name) && !empty($request->search_name)){
                    $sale->Where(DB::raw("CONCAT(`child_first_name`, ' ', `child_surname`)"), 'LIKE', "%".$request->search_name."%");
                }
                if (isset($search_input) && !empty($search_input)){
                    $string=explode(' ',$search_input);
                    if (isset($string[0])){
                        $search_input=$string[0];
                    }else{
                        $search_input=$search_input;
                    }
                    if (isset($string[1])){
                        $search_input=$string[1];
                    }else{
                        $search_input=$search_input;
                    }
                    if (isset($string[2])){
                        $search_input=$string[2];
                    }else{
                        $search_input=$search_input;
                    }
                    $sale->WhereHas('user',function ($query) use ($request,$search_input) {
                        $query->orWhere('name', 'LIKE',DB::raw("'%$search_input%'"))
                            ->orWhere('email', 'LIKE',DB::raw("'%$search_input%'"));
                    });
                }
                   
                }elseif (Auth::user()->company_id && $franchisees) {


                $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id', $franchisees)->pluck('company_id')->toArray();
                $sale->Where(function ($query) use ($franchisee_companies,$request) {
                    $query->WhereIn('company_id', $franchisee_companies)
                        ->orWhere('company_id', Auth::user()->company_id)
                        ->orWhere('user_id', Auth::user()->id);
                });

                if (isset($request->search_company) && !empty($request->search_company)){
                    $sale->WhereHas('company',function ($query) use ($request) {
                        $query->where('name','LIKE',DB::raw("'%$request->search_company%'"));
                    });
                }
                if (isset($request->search_block) && !empty($request->search_block)){
                    $sale->WhereHas('block',function ($query) use ($request) {
                        $query->where('name', 'LIKE',DB::raw("'%$request->search_block%'"));
                    });
                }
                if (isset($request->search_date) && !empty($request->search_date)){
                    $date = date('Y-m-d',strtotime($request->search_date)); 
                    $sale->where('start','<=' ,$date)->where('end','>=' ,$date);
                    
                }
                if (isset($request->search_bookingId) && !empty($request->search_bookingId)){
                    $sale->where('booking_id',$request->search_bookingId);
                }
                if (isset($request->search_email) && !empty($request->search_email)){
                    $sale->where('email','LIKE',DB::raw("'%$request->search_email%'"));
                }
                if (isset($request->search_name) && !empty($request->search_name)){
                    $sale->Where(DB::raw("CONCAT(`child_first_name`, ' ', `child_surname`)"), 'LIKE', "%".$request->search_name."%");
                }
                if (isset($search_input) && !empty($search_input)){
                    $string=explode(' ',$search_input);
                    if (isset($string[0])){
                        $search_input=$string[0];
                    }else{
                        $search_input=$search_input;
                    }
                    if (isset($string[1])){
                        $search_input=$string[1];
                    }else{
                        $search_input=$search_input;
                    }
                    if (isset($string[2])){
                        $search_input=$string[2];
                    }else{
                        $search_input=$search_input;
                    }
                    $sale->WhereHas('user',function ($query) use ($request,$search_input) {
                        $query->orWhere('name', 'LIKE',DB::raw("'%$search_input%'"))
                            ->orWhere('email', 'LIKE',DB::raw("'%$search_input%'"));
                    });
                }

            } elseif ($franchisees) {
                $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id', $franchisees)->pluck('company_id')->toArray();
//                dd($franchisee_companies);
                $sale->Where(function ($query) use ($franchisee_companies,$request)  {
                    $query->whereIn('company_id', $franchisee_companies)
                        ->orWhere('user_id', Auth::user()->id);
                });

                if (isset($request->search_company) && !empty($request->search_company)){
                    $sale->WhereHas('company',function ($query) use ($request) {
                        $query->where('name','LIKE',DB::raw("'%$request->search_company%'"));
                    });
                }
                if (isset($request->search_block) && !empty($request->search_block)){
                    $sale->WhereHas('block',function ($query) use ($request) {
                        $query->where('name', 'LIKE',DB::raw("'%$request->search_block%'"));
                    });
                }
                if (isset($request->search_date) && !empty($request->search_date)){
                    $date = $request->search_date;
                    $sale->where(function($q) use ($date) {
                        $q->WhereDate('start', '<=', $date)
                            ->whereDate('end', '>=', $date);
                    });
                }
                if (isset($request->search_bookingId) && !empty($request->search_bookingId)){
                    $sale->where('booking_id',$request->search_bookingId);
                }
                if (isset($request->search_email) && !empty($request->search_email)){
                    $sale->where('email','LIKE',DB::raw("'%$request->search_email%'"));
                }
                if (isset($request->search_name) && !empty($request->search_name)){
                    $sale->Where(DB::raw("CONCAT(`child_first_name`, ' ', `child_surname`)"), 'LIKE', "%".$request->search_name."%");
                }
                if (isset($search_input) && !empty($search_input)){
                    $string=explode(' ',$search_input);
                    if (isset($string[0])){
                        $search_input=$string[0];
                    }else{
                        $search_input=$search_input;
                    }
                    if (isset($string[1])){
                        $search_input=$string[1];
                    }else{
                        $search_input=$search_input;
                    }
                    if (isset($string[2])){
                        $search_input=$string[2];
                    }else{
                        $search_input=$search_input;
                    }
                    $sale->WhereHas('user',function ($query) use ($request,$search_input) {
                        $query->orWhere('name', 'LIKE',DB::raw("'%$search_input%'"))
                            ->orWhere('email', 'LIKE',DB::raw("'%$search_input%'"));
                    });
                }

                $sale->Where(function ($query) use ($request)  {
                    $query->Where('company_id',Auth::user()->company_id)
                        ->orWhere('user_id', Auth::user()->id);
                });

            } elseif (Auth::user()->company_id) {
//                dd('dd');
                $sale->Where(function ($query) use($request){
                    $query->Where('company_id',Auth::user()->company_id)
                        ->orWhere('user_id', Auth::user()->id);
                });

                if (isset($request->search_company) && !empty($request->search_company)){
                    $sale->WhereHas('company',function ($query) use ($request) {
                        $query->where('name','LIKE',DB::raw("'%$request->search_company%'"));
                    });
                }
                if (isset($request->search_block) && !empty($request->search_block)){
                    $sale->WhereHas('block',function ($query) use ($request) {
                        $query->where('name', 'LIKE',DB::raw("'%$request->search_block%'"));
                    });
                }
                if (isset($request->search_date) && !empty($request->search_date)){
                    $date = $request->search_date;
                    $sale->where(function($q) use ($date) {
                        $q->WhereDate('start', '<=', $date)
                            ->whereDate('end', '>=', $date);
                    });
                }
                if (isset($request->search_bookingId) && !empty($request->search_bookingId)){
                    $sale->where('booking_id',$request->search_bookingId);
                }
                if (isset($request->search_email) && !empty($request->search_email)){
                    $sale->where('email','LIKE',DB::raw("'%$request->search_email%'"));
                }
                if (isset($request->search_name) && !empty($request->search_name)){
                    $sale->Where(DB::raw("CONCAT(`child_first_name`, ' ', `child_surname`)"), 'LIKE', "%".$request->search_name."%");
                }
                if (isset($search_input) && !empty($search_input)){
                    $string=explode(' ',$search_input);
                    if (isset($string[0])){
                        $search_input=$string[0];
                    }else{
                        $search_input=$search_input;
                    }
                    if (isset($string[1])){
                        $search_input=$string[1];
                    }else{
                        $search_input=$search_input;
                    }
                    if (isset($string[2])){
                        $search_input=$string[2];
                    }else{
                        $search_input=$search_input;
                    }
                    $sale->WhereHas('user',function ($query) use ($request,$search_input) {
                        $query->orWhere('name', 'LIKE',DB::raw("'%$search_input%'"))
                            ->orWhere('email', 'LIKE',DB::raw("'%$search_input%'"));
                    });

                }


            } else {
                $sale->Where(function ($query) use($request){
                    $query->where('user_id', Auth::user()->id);
                });

                if (isset($request->search_company) && !empty($request->search_company)){
                    $sale->WhereHas('company',function ($query) use ($request) {
                        $query->where('name','LIKE',DB::raw("'%$request->search_company%'"));
                    });
                }
                if (isset($request->search_block) && !empty($request->search_block)){
                    $sale->WhereHas('block',function ($query) use ($request) {
                        $query->where('name', 'LIKE',DB::raw("'%$request->search_block%'"));
                    });
                }
                if (isset($request->search_date) && !empty($request->search_date)){
                    $date = $request->search_date;
                    $sale->where(function($q) use ($date) {
                        $q->WhereDate('start', '<=', $date)
                            ->whereDate('end', '>=', $date);
                    });
                }
                if (isset($request->search_bookingId) && !empty($request->search_bookingId)){
                    $sale->where('booking_id',$request->search_bookingId);
                }
                if (isset($request->search_email) && !empty($request->search_email)){
                    $sale->where('email','LIKE',DB::raw("'%$request->search_email%'"));
                }
                if (isset($request->search_name) && !empty($request->search_name)){
                    $sale->Where(DB::raw("CONCAT(`child_first_name`, ' ', `child_surname`)"), 'LIKE', "%".$request->search_name."%");
                }
                if (isset($search_input) && !empty($search_input)){
                    $string=explode(' ',$search_input);
                    if (isset($string[0])){
                        $search_input=$string[0];
                    }else{
                        $search_input=$search_input;
                    }
                    if (isset($string[1])){
                        $search_input=$string[1];
                    }else{
                        $search_input=$search_input;
                    }
                    if (isset($string[2])){
                        $search_input=$string[2];
                    }else{
                        $search_input=$search_input;
                    }
                    $sale->WhereHas('user',function ($query) use ($request,$search_input) {
                        $query->orWhere('name', 'LIKE',DB::raw("'%$search_input%'"))
                            ->orWhere('email', 'LIKE',DB::raw("'%$search_input%'"));
                    });
                }
//                $sale->where('user_id', Auth::user()->id);
            }
//        }
//        else{
////            dd('else');
//            $sale->Where(function ($query) use($request){
//                $query->where('user_id', Auth::user()->id);
//            });
//
//            if (isset($request->search_company) && !empty($request->search_company)){
//                $sale->WhereHas('company',function ($query) use ($request) {
//                    $query->where('name','LIKE',DB::raw("'%$request->search_company%'"));
//                });
//            }
//            if (isset($request->search_block) && !empty($request->search_block)){
//                $sale->WhereHas('block',function ($query) use ($request) {
//                    $query->where('name', 'LIKE',DB::raw("'%$request->search_block%'"));
//                });
//            }
//            if (isset($request->search_date) && !empty($request->search_date)){
//                $date = $request->search_date;
//                $sale->where(function($q) use ($date) {
//                    $q->WhereDate('start', '<=', $date)
//                        ->whereDate('end', '>=', $date);
//                });
//            }
//            if (isset($request->search_bookingId) && !empty($request->search_bookingId)){
//                $sale->where('booking_id',$request->search_bookingId);
//            }
//            if (isset($request->search_email) && !empty($request->search_email)){
//                $sale->where('email','LIKE',DB::raw("'%$request->search_email%'"));
//            }
//            if (isset($request->search_name) && !empty($request->search_name)){
//                $sale->Where(DB::raw("CONCAT(`child_first_name`, ' ', `child_surname`)"), 'LIKE', "%".$request->search_name."%");
//            }
//            if (isset($search_input) && !empty($search_input)){
//                $string=explode(' ',$search_input);
//                if (isset($string[0])){
//                    $search_input=$string[0];
//                }else{
//                    $search_input=$search_input;
//                }
//                if (isset($string[1])){
//                    $search_input=$string[1];
//                }else{
//                    $search_input=$search_input;
//                }
//                if (isset($string[2])){
//                    $search_input=$string[2];
//                }else{
//                    $search_input=$search_input;
//                }
//                $sale->WhereHas('user',function ($query) use ($request,$search_input) {
//                    $query->orWhere('name', 'LIKE',DB::raw("'%$search_input%'"))
//                        ->orWhere('email', 'LIKE',DB::raw("'%$search_input%'"));
//                });
//
//            }
//        }
//        $sale->where('active', '!=' ,0);
//        $sale->orderBy('id', 'desc');
        $sale->take(100);
        return Datatables::of($sale)

            ->addColumn('hold_booking_date', function ($sale) {
                return Carbon::parse($sale->hold_booking_date)->format('d M, Y H:i') ;
            })
            ->addColumn('name', function ($sale) {
                if($sale->user)
                    return $sale->user->name;
            })
            ->addColumn('booking_date', function ($sale) {

                $val= Carbon::parse($sale->start)->format('d M, Y')." - ";
                $val.= Carbon::parse($sale->end)->format('d M, Y');

                return $val ;
            })
            ->addColumn('block', function ($sale) {

                if($sale->block_id) {
                    if(!empty($sale->block->name))
                        return $sale->block->name;
                }
            })
            ->addColumn('action', function ($sale) {

                $buttons ='';
//                $buttons .= '<form method="POST" action="'.route('reserved.lockers.schooltermbasedreserved').'"> <input type="hidden" name="id" value="'.$sale->id.'"/>'.csrf_field().' <input type="submit" value="Purchase" class="btn btn-warning pull-left button-style" id="purchase' . $sale->id . '"   onclick="row_highlight(\'purchase' . $sale->id . '\')"  data-booking_id = "'. $sale->booking_id .'" /></form> ';
                $buttons .= '<a href = "'.route('reserved.lockers.schooltermbasedreserved',$sale->booking_id).'" class="btn btn-success btn-md adv_cust_mod_btn edit-btn pull-left button-style" id="purchase' . $sale->id . '"   onclick="row_highlight(\'purchase' . $sale->id . '\')"  data-booking_id = "'. $sale->booking_id .'" > Purchase </a >';

                return $buttons;
            })

            ->make(true);
    }


    public function delete_all(Request $request){
        $sale = Sale::query();
        $sale->with('company','user','block');
        $sale->where('status','l');
//        $sale->groupBy('booking_id');
        $date = date('Y-m-d',strtotime($request->search_date));
        $search_input = $request->search_input;
        Session::put('reserved-lockers',$search_input);

        $user = Auth::user();
        $franchisees = $user->franchisee()->pluck('franchisee_id')->toArray();
//        if(auth()->user()->can('3D Secure')) {

            if (auth()->user()->can('All Records')) {
                if (isset($request->search_company) && !empty($request->search_company)){
                    $sale->WhereHas('company',function ($query) use ($request) {
                        $query->where('name','LIKE',DB::raw("'%$request->search_company%'"));
                    });
                }
                if (isset($request->search_block) && !empty($request->search_block)){
                    $sale->WhereHas('block',function ($query) use ($request) {
                        $query->where('name', 'LIKE',DB::raw("'%$request->search_block%'"));
                    });
                }
                if (isset($request->search_date) && !empty($request->search_date)){
                     $date = date('Y-m-d',strtotime($request->search_date)); 
                    $sale->where('start','<=' ,$date)->where('end','>=' ,$date);
                }
                if (isset($request->search_bookingId) && !empty($request->search_bookingId)){
                    $sale->where('booking_id',$request->search_bookingId);
                }
                if (isset($request->search_email) && !empty($request->search_email)){
                    $sale->where('email','LIKE',DB::raw("'%$request->search_email%'"));
                }
                if (isset($request->search_name) && !empty($request->search_name)){
                    $sale->Where(DB::raw("CONCAT(`child_first_name`, ' ', `child_surname`)"), 'LIKE', "%".$request->search_name."%");
                }
                if (isset($search_input) && !empty($search_input)){
                    $string=explode(' ',$search_input);
                    if (isset($string[0])){
                        $search_input=$string[0];
                    }else{
                        $search_input=$search_input;
                    }
                    if (isset($string[1])){
                        $search_input=$string[1];
                    }else{
                        $search_input=$search_input;
                    }
                    if (isset($string[2])){
                        $search_input=$string[2];
                    }else{
                        $search_input=$search_input;
                    }
                    $sale->WhereHas('user',function ($query) use ($request,$search_input) {
                        $query->orWhere('name', 'LIKE',DB::raw("'%$search_input%'"));
                        $query->orWhere('email', 'LIKE',DB::raw("'%$search_input%'"));
                    });
                }

            } elseif (Auth::user()->company_id && $franchisees) {
                $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id', $franchisees)->pluck('company_id')->toArray();
                $sale->Where(function ($query) use ($franchisee_companies,$request) {
                    $query->WhereIn('company_id', $franchisee_companies)
                        ->orWhere('company_id', Auth::user()->company_id)
                        ->orWhere('user_id', Auth::user()->id);
                });

                if (isset($request->search_company) && !empty($request->search_company)){
                    $sale->WhereHas('company',function ($query) use ($request) {
                        $query->where('name','LIKE',DB::raw("'%$request->search_company%'"));
                    });
                }
                if (isset($request->search_block) && !empty($request->search_block)){
                    $sale->WhereHas('block',function ($query) use ($request) {
                        $query->where('name', 'LIKE',DB::raw("'%$request->search_block%'"));
                    });
                }
                if (isset($request->search_date) && !empty($request->search_date)){
                    $date = $request->search_date;
                    $sale->where(function($q) use ($date) {
                        $q->WhereDate('start', '<=', $date)
                            ->whereDate('end', '>=', $date);
                    });
                }
                if (isset($request->search_bookingId) && !empty($request->search_bookingId)){
                    $sale->where('booking_id',$request->search_bookingId);
                }
                if (isset($request->search_email) && !empty($request->search_email)){
                    $sale->where('email','LIKE',DB::raw("'%$request->search_email%'"));
                }
                if (isset($request->search_name) && !empty($request->search_name)){
                    $sale->Where(DB::raw("CONCAT(`child_first_name`, ' ', `child_surname`)"), 'LIKE', "%".$request->search_name."%");
                }
                if (isset($search_input) && !empty($search_input)){
                    $string=explode(' ',$search_input);
                    if (isset($string[0])){
                        $search_input=$string[0];
                    }else{
                        $search_input=$search_input;
                    }
                    if (isset($string[1])){
                        $search_input=$string[1];
                    }else{
                        $search_input=$search_input;
                    }
                    if (isset($string[2])){
                        $search_input=$string[2];
                    }else{
                        $search_input=$search_input;
                    }
                    $sale->WhereHas('user',function ($query) use ($request,$search_input) {
                        $query->orWhere('name', 'LIKE',DB::raw("'%$search_input%'"));
                        $query->orWhere('email', 'LIKE',DB::raw("'%$search_input%'"));
                    });
                }

            } elseif ($franchisees) {
                $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id', $franchisees)->pluck('company_id')->toArray();
                $sale->Where(function ($query) use ($franchisee_companies,$request)  {
                    $query->whereIn('company_id', $franchisee_companies)
                        ->orWhere('user_id', Auth::user()->id);
                });

                if (isset($request->search_company) && !empty($request->search_company)){
                    $sale->WhereHas('company',function ($query) use ($request) {
                        $query->where('name','LIKE',DB::raw("'%$request->search_company%'"));
                    });
                }
                if (isset($request->search_block) && !empty($request->search_block)){
                    $sale->WhereHas('block',function ($query) use ($request) {
                        $query->where('name', 'LIKE',DB::raw("'%$request->search_block%'"));
                    });
                }
                if (isset($request->search_date) && !empty($request->search_date)){
                    $date = $request->search_date;
                    $sale->where(function($q) use ($date) {
                        $q->WhereDate('start', '<=', $date)
                            ->whereDate('end', '>=', $date);
                    });
                }
                if (isset($request->search_bookingId) && !empty($request->search_bookingId)){
                    $sale->where('booking_id',$request->search_bookingId);
                }
                if (isset($request->search_email) && !empty($request->search_email)){
                    $sale->where('email','LIKE',DB::raw("'%$request->search_email%'"));
                }
                if (isset($request->search_name) && !empty($request->search_name)){
                    $sale->Where(DB::raw("CONCAT(`child_first_name`, ' ', `child_surname`)"), 'LIKE', "%".$request->search_name."%");
                }
                if (isset($search_input) && !empty($search_input)){
                    $string=explode(' ',$search_input);
                    if (isset($string[0])){
                        $search_input=$string[0];
                    }else{
                        $search_input=$search_input;
                    }
                    if (isset($string[1])){
                        $search_input=$string[1];
                    }else{
                        $search_input=$search_input;
                    }
                    if (isset($string[2])){
                        $search_input=$string[2];
                    }else{
                        $search_input=$search_input;
                    }
                    $sale->WhereHas('user',function ($query) use ($request,$search_input) {
                        $query->orWhere('name', 'LIKE',DB::raw("'%$search_input%'"));
                        $query->orWhere('email', 'LIKE',DB::raw("'%$search_input%'"));
                    });
                }

                $sale->Where(function ($query) use ($request)  {
                    $query->Where('company_id',Auth::user()->company_id)
                        ->orWhere('user_id', Auth::user()->id);
                });

            } elseif (Auth::user()->company_id) {
//                dd('dd');
                $sale->Where(function ($query) use($request){
                    $query->Where('company_id',Auth::user()->company_id)
                        ->orWhere('user_id', Auth::user()->id);
                });

                if (isset($request->search_company) && !empty($request->search_company)){
                    $sale->WhereHas('company',function ($query) use ($request) {
                        $query->where('name','LIKE',DB::raw("'%$request->search_company%'"));
                    });
                }
                if (isset($request->search_block) && !empty($request->search_block)){
                    $sale->WhereHas('block',function ($query) use ($request) {
                        $query->where('name', 'LIKE',DB::raw("'%$request->search_block%'"));
                    });
                }
                if (isset($request->search_date) && !empty($request->search_date)){
                    $date = $request->search_date;
                    $sale->where(function($q) use ($date) {
                        $q->WhereDate('start', '<=', $date)
                            ->whereDate('end', '>=', $date);
                    });
                }
                if (isset($request->search_bookingId) && !empty($request->search_bookingId)){
                    $sale->where('booking_id',$request->search_bookingId);
                }
                if (isset($request->search_email) && !empty($request->search_email)){
                    $sale->where('email','LIKE',DB::raw("'%$request->search_email%'"));
                }
                if (isset($request->search_name) && !empty($request->search_name)){
                    $sale->Where(DB::raw("CONCAT(`child_first_name`, ' ', `child_surname`)"), 'LIKE', "%".$request->search_name."%");
                }
                if (isset($search_input) && !empty($search_input)){
                    $string=explode(' ',$search_input);
                    if (isset($string[0])){
                        $search_input=$string[0];
                    }else{
                        $search_input=$search_input;
                    }
                    if (isset($string[1])){
                        $search_input=$string[1];
                    }else{
                        $search_input=$search_input;
                    }
                    if (isset($string[2])){
                        $search_input=$string[2];
                    }else{
                        $search_input=$search_input;
                    }
                    $sale->WhereHas('user',function ($query) use ($request,$search_input) {
                        $query->orWhere('name', 'LIKE',DB::raw("'%$search_input%'"));
                        $query->orWhere('email', 'LIKE',DB::raw("'%$search_input%'"));
                    });

                }


            } else {
                $sale->Where(function ($query) use($request){
                    $query->where('user_id', Auth::user()->id);
                });

                if (isset($request->search_company) && !empty($request->search_company)){
                    $sale->WhereHas('company',function ($query) use ($request) {
                        $query->where('name','LIKE',DB::raw("'%$request->search_company%'"));
                    });
                }
                if (isset($request->search_block) && !empty($request->search_block)){
                    $sale->WhereHas('block',function ($query) use ($request) {
                        $query->where('name', 'LIKE',DB::raw("'%$request->search_block%'"));
                    });
                }
                if (isset($request->search_date) && !empty($request->search_date)){
                    $date = $request->search_date;
                    $sale->where(function($q) use ($date) {
                        $q->WhereDate('start', '<=', $date)
                            ->whereDate('end', '>=', $date);
                    });
                }
                if (isset($request->search_bookingId) && !empty($request->search_bookingId)){
                    $sale->where('booking_id',$request->search_bookingId);
                }
                if (isset($request->search_email) && !empty($request->search_email)){
                    $sale->where('email','LIKE',DB::raw("'%$request->search_email%'"));
                }
                if (isset($request->search_name) && !empty($request->search_name)){
                    $sale->Where(DB::raw("CONCAT(`child_first_name`, ' ', `child_surname`)"), 'LIKE', "%".$request->search_name."%");
                }
                if (isset($search_input) && !empty($search_input)){
                    $string=explode(' ',$search_input);
                    if (isset($string[0])){
                        $search_input=$string[0];
                    }else{
                        $search_input=$search_input;
                    }
                    if (isset($string[1])){
                        $search_input=$string[1];
                    }else{
                        $search_input=$search_input;
                    }
                    if (isset($string[2])){
                        $search_input=$string[2];
                    }else{
                        $search_input=$search_input;
                    }
                    $sale->WhereHas('user',function ($query) use ($request,$search_input) {
                        $query->orWhere('name', 'LIKE',DB::raw("'%$search_input%'"));
                        $query->orWhere('email', 'LIKE',DB::raw("'%$search_input%'"));
                    });
                }
//                $sale->where('user_id', Auth::user()->id);
            }
//        }
//        else{
//            $sale->Where(function ($query) use($request){
//                $query->where('user_id', Auth::user()->id);
//            });
//
//            if (isset($request->search_company) && !empty($request->search_company)){
//                $sale->WhereHas('company',function ($query) use ($request) {
//                    $query->where('name','LIKE',DB::raw("'%$request->search_company%'"));
//                });
//            }
//            if (isset($request->search_block) && !empty($request->search_block)){
//                $sale->WhereHas('block',function ($query) use ($request) {
//                    $query->where('name', 'LIKE',DB::raw("'%$request->search_block%'"));
//                });
//            }
//            if (isset($request->search_date) && !empty($request->search_date)){
//                $date = $request->search_date;
//                $sale->where(function($q) use ($date) {
//                    $q->WhereDate('start', '<=', $date)
//                        ->whereDate('end', '>=', $date);
//                });
//            }
//            if (isset($request->search_bookingId) && !empty($request->search_bookingId)){
//                $sale->where('booking_id',$request->search_bookingId);
//            }
//            if (isset($request->search_email) && !empty($request->search_email)){
//                $sale->where('email','LIKE',DB::raw("'%$request->search_email%'"));
//            }
//            if (isset($request->search_name) && !empty($request->search_name)){
//                $sale->where(DB::raw("CONCAT(`child_first_name`, ' ', `child_surname`)"), 'LIKE', "%".$request->search_name."%");
//            }
//            if (isset($search_input) && !empty($search_input)){
//                $string=explode(' ',$search_input);
//                if (isset($string[0])){
//                    $search_input=$string[0];
//                }else{
//                    $search_input=$search_input;
//                }
//                if (isset($string[1])){
//                    $search_input=$string[1];
//                }else{
//                    $search_input=$search_input;
//                }
//                if (isset($string[2])){
//                    $search_input=$string[2];
//                }else{
//                    $search_input=$search_input;
//                }
//                $sale->WhereHas('user',function ($query) use ($request,$search_input) {
//                    $query->orWhere('name', 'LIKE',DB::raw("'%$search_input%'"));
//                    $query->orWhere('email', 'LIKE',DB::raw("'%$search_input%'"));
//                });
//
//            }
//        }
//        $sale->where('active', '!=' ,0);
//        $sale->orderBy('id', 'desc');


        return Datatables::of($sale)

            ->addColumn('created_at', function ($sale) {

                return Carbon::parse($sale->created_at)->format('d M, Y H:i') ;
            })
            ->addColumn('name', function ($sale) {
                if($sale->user)
                    return $sale->user->name;
            })
            ->addColumn('booking_date', function ($sale) {

                $val= Carbon::parse($sale->start)->format('d M, Y')." - ";
                $val.= Carbon::parse($sale->end)->format('d M, Y');

                return $val ;
            })
            ->addColumn('block', function ($sale) {

                if($sale->block_id) {
                    if(!empty($sale->block->name))
                        return $sale->block->name;
                }
            })
            ->addColumn('action', function ($sale) {

                $buttons ='';
//                $buttons .= '<a href = "'.route('reserved.lockers.schooltermbasedreserved',$sale->booking_id).'" class="btn btn-success btn-md adv_cust_mod_btn edit-btn pull-left button-style" id="restore' . $sale->id . '"   onclick="row_highlight(\'restore' . $sale->id . '\')"  data-booking_id = "'. $sale->booking_id .'" > Restore Booking </a >';
//                $buttons .= '<a href="#restore_booking" class="btn btn-primary btn-md adv_cust_mod_btn pull-left edit-block" data-toggle="modal" onclick="saveid('.$sale->booking_id.');row_highlight(\'restore' . $sale->id . '\')" data-id="'. $sale->id .'"  style="margin-right: 3px;" id="restore' . $sale->id . '"      data-booking_id = "'. $sale->booking_id .'">Restore Booking </a>';

                return $buttons;
            })
            ->setRowClass(function ($company_blocks) {
                return 'alert-danger';
            })

            ->make(true);
    }

    public function delete_all_old(){
        $sale = Sale::query();
        $sale->with('company','user','block');
        $sale->where('status','l');
        $user = Auth::user();
        $franchisees = $user->franchisee()->pluck('franchisee_id')->toArray();
//        if(auth()->user()->can('3D Secure')) {
            if (auth()->user()->can('All Records')) {

            } elseif (Auth::user()->company_id && $franchisees) {
                $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id', $franchisees)->pluck('company_id')->toArray();
                $sale->Where(function ($query) use ($franchisee_companies) {
                    $query->WhereIn('company_id', $franchisee_companies)
                        ->orWhere('company_id', Auth::user()->company_id)
                        ->orWhere('user_id', Auth::user()->id);
                });
            } elseif ($franchisees) {
                $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id', $franchisees)->pluck('company_id')->toArray();

                $sale->Where(function ($query) use ($franchisee_companies)  {
                    $query->whereIn('company_id', $franchisee_companies)
                        ->orWhere('user_id', Auth::user()->id);
                });
                $sale->Where(function ($query)  {
                    $query->Where('company_id',Auth::user()->company_id)
                        ->orWhere('user_id', Auth::user()->id);
                });
            } elseif (Auth::user()->company_id) {
//                dd('dd');
                $sale->Where(function ($query)  {
                    $query->Where('company_id',Auth::user()->company_id)
                        ->orWhere('user_id', Auth::user()->id);
                });
//
            } else {
                $sale->where('user_id', Auth::user()->id);
            }
//        }
//        else{
//            $sale->where('user_id', Auth::user()->id);
//        }
//        $sale->where('active', '!=' ,0);
//        $sale->orderBy('id', 'desc');


        return Datatables::of($sale)

            ->addColumn('created_at', function ($sale) {

                return Carbon::parse($sale->created_at)->format('d M, Y H:i') ;
            })
            ->addColumn('name', function ($sale) {
                if($sale->user)
                    return $sale->user->name;
            })
            ->addColumn('booking_date', function ($sale) {

                $val= Carbon::parse($sale->start)->format('d M, Y')." - ";
                $val.= Carbon::parse($sale->end)->format('d M, Y');

                return $val ;
            })
            ->addColumn('block', function ($sale) {

                if($sale->block_id) {
                    if(!empty($sale->block->name))
                        return $sale->block->name;
                }
            })
            ->addColumn('action', function ($sale) {

                $buttons ='';
//                $buttons .= '<a href = "'.route('reserved.lockers.schooltermbasedreserved',$sale->booking_id).'" class="btn btn-success btn-md adv_cust_mod_btn edit-btn pull-left button-style" id="purchase' . $sale->id . '"   onclick="row_highlight(\'purchase' . $sale->id . '\')"  data-booking_id = "'. $sale->booking_id .'" > Purchase </a >';

                return $buttons;
            })
            ->setRowClass(function ($company_blocks) {
                return 'alert-danger';
            })

            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function restoreBooking(Request $request)
    {
//        dd($request->all());
        $sales = Sale::where('booking_id',$request->booking_id)->get();
        foreach ($sales as $sale){
            if(isset($sale->locker_id)){
                $book_next_year=false;
                $sale_obj = new Sale();
                $available = $sale_obj->is_locker_available($sale->locker_id,$sale->start,$sale->end,$book_next_year);
//dd($available);
                if ($available == 1) {


//                    $html = ''
//                    dd($available);0
                }
            }
        }

//        dd($sales);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function schooltermreserved($id)
    {

//        $id = $request->id;
        $sales = Sale::where('booking_id',$id)->orderBy('id','ASC')->where('status','r')->get();

        $locker_ids =[];
        $sale_ids =[];

        if(count($sales) > 0) {

            $user = User::where('id', $sales[0]->user_id)->first();
            $useremail = $user->email;
            foreach ($sales as $sale) {

                if ($sale->hold_booking_date <= Carbon::now()->toDateTimeString()) {

                    return redirect('/booking-expired/' . $sale->id);
                }else {
//                    $school_year = SchoolYear::where('from_date', $sale->start)->where('to_date','LIKE', '%' .Carbon::parse($sale->end)->format('Y-m-d').'%')->first();
//                    dd($school_year);
                    if(empty($sale->school_year_id)) {
                        $school_year = SchoolYear::whereDate('from_date', $sale->start)->whereDate('to_date', 'LIKE', '%' .Carbon::parse($sale->end)->format('Y-m-d').'%')->first();
                        if ($school_year) {
                            $school_year_id = $school_year->id;
                        } else {
                            $start_title = Carbon::parse($sale->start)->format('d M, Y');
                            $end_title = Carbon::parse($sale->end)->format('d M, Y');
                            $title = $start_title . ' - ' . $end_title;
                            $dontAllowDuplicate = SchoolYear::where('title', $title)->first();
                     if(empty($dontAllowDuplicate)){
                            $SchoolYear = new SchoolYear();
                            $SchoolYear->title = $title;
                            $SchoolYear->from_date = $sale->start;
                            $SchoolYear->to_date = $sale->end;
                            $SchoolYear->status = 0;
                            $SchoolYear->save();
                            $school_year_id = $SchoolYear->id;
                        }else{
                            $school_year_id = $dontAllowDuplicate->id;
                        }

                        }
                        $sale->school_year_id = $school_year_id;
                        $sale->save();
                        //add sales_audit log
                        $sales_audit_obj = new SalesAudit();
                        $sales_audit_obj->sales_audit_log($sale->id,$type = 'validate');
                    }

                    if(!empty($sale->block_id)) {

                        if(empty($sale->school_academic_year_id)) {
                            $school_year = BlockYears::where('block_id', $sale->block_id)->first();

                            if(!empty($school_year->school_academic_year_id))
                                $sale->school_academic_year_id = $school_year->school_academic_year_id;


                            $sale->save();
                            //add sales_audit log
                            $sales_audit_obj = new SalesAudit();
                            $sales_audit_obj->sales_audit_log($sale->id,$type = 'validate');

                        }
                    }

                    array_push($locker_ids, $sale->locker_id);
                    array_push($sale_ids, $sale->id);
                }
            }

            $sales_count = $sales->count();

            $school = Company::where('id', $sales[0]->company_id)->first();

            $years = SchoolYear::where('status', 1)->get()->toArray();

            $company_school_year = CompanySchoolYear::where('company_id', $sales[0]->company_id)->orderBy('id', 'asc')->get();

            $academic_years = [];

            foreach ($company_school_year as $key => $value) {
                $x = SchoolAcademicYear::where('id', $value->school_academic_year_id)->first();
                array_push($academic_years, $x);
            }

            //dd($academic_years);

            //$academic_years = SchoolAcademicYear::all()->toArray();

            $placeholders = FrontendPlaceholder::where('key', 'school-term-login-user-msg')->pluck('placeholder')->toArray();

             $book_lock_desc = FrontendPlaceholder::where('key', 'booking-locker-disclaimer')->pluck('placeholder')->toArray();
        $book_lock_desc_placeholders = implode(",", $book_lock_desc);

        $book_lock_desc_cond = FrontendPlaceholder::where('key', 'booking-locker-disclaimer-condition')->pluck('placeholder')->toArray();
        $book_lock_desc_cond_placeholders = implode(",", $book_lock_desc_cond);

            $login_user_msg_placeholders = implode(",", $placeholders);
            \Illuminate\Support\Facades\Session::put('page_number', 3);
            $expiry = Sale::where('booking_id', $id)->select('booking_expiry')->first();
//        Session::put('company_info', $school);
            Session::put('saleKey', $id);
            Session::put('companyId', $sales[0]->company_id);
            Session::put('bookingTimer', 15);
            Session::put('schoolTermSubmit', 1);
            Session::put('schoolTermReSubmit', 1);
            Session::put('reservedLocker', 1);
            Session::put('show_hide_lockers', 'Grid Hidden');
            if (Session::has('arrowsPage') == false) {
                Session::put('arrowsPage', 2);
            }
            $sales = Sale::where('booking_id',$id)->orderBy('id','ASC')->where('status','r')->get();

            $voucher_code_user = VoucherCodeUser::where('booking_id',$id)->whereNotNull('code')->first();
            if(!empty($voucher_code_user)){
                $voucher_code = VoucherCode::where('code',$voucher_code_user->code)->first();
                Session::put('voucher_code_id',$voucher_code->id);
                $voucher_code->code = null;
                $voucher_code->save();
            }
            return view('frontend.pages.schooltermbasedreserved', compact('school', 'academic_years', 'years', 'login_user_msg_placeholders', 'sales', 'sales_count', 'locker_ids', 'sale_ids', 'useremail',  'book_lock_desc_placeholders', 'book_lock_desc_cond_placeholders'));
        }else{
            return redirect('/booking-expired/' . $id);
        }
    }

    public function dateChangeLostLocker(Request $request)
    {
        $sale = Sale::find($request->sale_id);
        $sale->block_id = null;
        $sale->locker_id = null;
        $sale->save();

        return response()->json('success');
    }

    public function checkSameSchoolYear(Request $request)
    {
        $schoolYear = SchoolYear::where('id', $request->shcool_year_id)->first();
        $splitName = explode('-', $schoolYear->title);

     $block_years = BlockYears::where('company_id',$request->company_id)->where('block_id',$request->block_id)->where('school_academic_year_id',$request->school_academic_year_id)->first();
     $block = Block::where('id',$request->block_id)->where('active',1)->where('display',1)->first();
     $locker = Locker::where('id',$request->locker_id)->where('active',1)->where('display',1)->first();
     $company = Company::find($request->company_id);
//        dd($block_years);
     if(isset($block_years) && isset($block) && isset($locker)){
         $data =[
             'status'=>'exists'
         ];
     }else{
         $start_next = Carbon::parse($schoolYear->from_date)->startOfDay()->addYear()->toDateTimeString();
         $end_next = Carbon::parse($schoolYear->to_date)->endOfDay()->addYear()->toDateTimeString();

         $data =[
             'status'=>'not_exists',
             'splitName' => $splitName,
             'start_next' => $start_next,
             'end_next' => $end_next,
             'company_name' => $company->name,
         ];
     }

        return response()->json($data);
    }


}
