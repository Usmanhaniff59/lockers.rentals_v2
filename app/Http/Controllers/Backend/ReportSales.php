<?php
 
namespace App\Http\Controllers\Backend;

use App\Block;
use App\Company;
use App\FranchiseeCompany;
use App\Locker;
use App\Payment;
use App\Rate;
use App\Maintenance;
use App\SalesReport;
use App\Sale;
use App\Transaction;
use App\SchoolAcademicYear;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Throwable;
use Yajra\DataTables\DataTables;
use App\LocationGroup;
use App\CompanySchoolYear;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;


class ReportSales extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $locationGroups = LocationGroup::get();
       $rates = Rate::orderBy('order','ASC')->get();
       $school_academic_year = SchoolAcademicYear::orderBy('order_number', 'asc')->get();


        return view('backend.pages.reports.sales.index',compact('locationGroups','rates', 'school_academic_year'));
    }
    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */

     public function show_all(Request $request){

        $transaction = Transaction::query()->where('created_at', '!=', 'NULL')->where('currency', '!=', 'NULL');
                    
//         ini_set('memory_limit', '-1');

         $search_input = $request->search_input;
        
        $start_date=date('Y-m-d',strtotime($request->search_start_date));
        $end_date=date('Y-m-d',strtotime($request->search_end_date));

        $this_month_start_date =   date('Y-m-01');
        $this_month_end_date = date('Y-m-t');

        $data = [];
               
        if(auth()->user()->can('All Records')) {
            $data= SalesReport::query();
            if (isset($request->search_start_date) && isset($request->search_end_date)){
                $data->whereBetween('date', [$start_date, $end_date]);

            }else if (isset($request->search_start_date) && !empty($request->search_start_date)){
                $data->where('date', '>=', $start_date);
            }else if (isset($request->search_end_date) && !empty($request->search_end_date)){
                $data->where('date', '<=', $end_date);
            }


            if (isset($request->search_currency) && !empty($request->search_currency)){
                $data->where('currency','LIKE',DB::raw("'%$request->search_currency%'"));
            } 

            if (isset($search_input) && !empty($search_input)){

                $data->where(function ($query) use($search_input) {
                    $string=explode(' ',$search_input);
                    if (isset($string[0])){
                        $search_input=$string[0];
                    }else{
                        $search_input=$search_input;
                    }
                    if (isset($string[1])){
                        $search_input=$string[1];
                    }else{
                        $search_input=$search_input;
                    }
                    if (isset($string[2])){
                        $search_input=$string[2];
                    }else{
                        $search_input=$search_input;
                    }
                    $query->orWhere('date','LIKE',DB::raw("'%$search_input%'"))
                        ->orWhere('currency','LIKE',DB::raw("'%$search_input%'"))
                         ->orWhere('amount','LIKE',DB::raw("'%$search_input%'"))
                          ->orWhere('sales_count','LIKE',DB::raw("'%$search_input%'"))
                         ->orWhere('refund','LIKE',DB::raw("'%$search_input%'"));
                });
            }
        }

        return Datatables::of($data)
        ->make(true);
    }
}
