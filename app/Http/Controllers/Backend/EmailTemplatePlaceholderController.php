<?php

namespace App\Http\Controllers\Backend;

use App\EmailTemplatePlaceHolder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmailTemplatePlaceholderController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //        $this->validate($request, [
//            'template_name'=>'required|max:120',
//            'subject'=>'required|max:120',
//            'template_text'=>'required'
//        ]);
//
        $email_template = EmailTemplatePlaceHolder::create($request->only('email_template_id', 'placeholder', 'description'));
//
//        return redirect()->route('emailtemplates.index')
//            ->with('flash_message',
//                'Email Template successfully added.');
        return response()->json($email_template);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
