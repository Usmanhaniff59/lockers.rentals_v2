<?php

namespace App\Http\Controllers\Backend;

use App\Block;
use App\BlockYears;
use App\Company;
use App\CompanySchoolYear;
use App\FranchiseeCompany;
use App\CronSchedule;
use App\Email;
use App\FrontendPlaceholder;
use App\LocationGroup;
use App\Locker;
use App\LockerMove;
use App\LockerOffSale;
use App\Payment;
use App\SalesAudit;
use App\SchoolAcademicYear;
use App\SchoolYear;
use App\User;
use App\Franchisee;
use App\paymentConfig;
use App\Sale;
use App\VoucherCodeUser;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Transaction;
use App\EmailTemplate;
use App\RefundTransaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Events\TransactionBeginning;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Spatie\Permission\Traits\HasRoles;
use Yajra\DataTables\DataTables;


class paymentController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('backend.pages.payments.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function show_all(Request $request)
    {
//dd($request->record_type);
        $payments = Payment::query();
        $payments->join('transactions','transactions.booking_id','=','payments.booking_id');
        $payments->leftJoin('sales', function($join)
        {
            $join->on('sales.booking_id', '=', 'payments.booking_id');
        })
        ->join('companies','companies.id','=','sales.company_id')
        ->join('users','users.id','=','payments.user_id');
        //radio filters like all,paid,lost,reserved
        $payments = $this->payments_radio_filters($payments, $request);

        //search filters like booking ID,company,name
        $payments = $this->payments_search_filters($payments, $request);

        //record permissions like admin,user,franchise
        $payments = $this->payments_record_permission($payments);

        $payments->select('payments.*');
        $payments->distinct('transactions.booking_id');

        return Datatables::of($payments)
            ->addColumn('created', function ($payments) {
                $sale = Sale::where('booking_id',$payments->booking_id)->first();
                return Carbon::parse($sale->start)->format('d M, Y').'  -  ' .Carbon::parse($sale->end)->format('d M, Y');
            })
            ->addColumn('billing_first_name', function ($payments) {
                $name = '';
                if(isset($payments->user_id)) {
                    if($payments->user->name || $payments->user->surname) {
                        $name = $payments->user->name . ' ' . $payments->user->surname;
                        return $name;
                    }elseif($payments->user->username){
                        $name=  $payments->user->username.' ';
                        return $name;
                    }else{
                        return $name;
                    }

                }else{
                    return $name;
                }
            })
            ->addColumn('company', function ($payments) {
                $company = null;

                if(isset($payments->sale)) {
                    $company_data = Company::where('id', $payments->sale->company_id)->select('name')->first();

                    if($company_data)
                        $company =$company_data->name;
                }
                return $company;
            })
            ->addColumn('status', function ($payments) {
                if($payments->sale->status == "b"){
                    return "Booked";
                } else if($payments->sale->status == "r"){
                    return "Reserved";
                } else {
                    return "Lost";
                }
            })
            ->addColumn('total_paid_amount', function ($payments) {
                if(auth()->user()->can('Account Balance')){
                    return $payments->account_balance;
                } else {
                    return "N/A";
                }

            })
            ->addColumn('action', function ($payments) {

                $buttons ='';

                if(auth()->user()->can('Transaction Log Order')) {

                    if ($payments->booking_id){
                        if($payments->has_transaction == "1"){
//                            if($payments->sale->status == "b" ){
                            $buttons .= '<form method="POST" action="'.url('admin/refundPaymentList').'"><input type="hidden" name="id" value="'.$payments->booking_id.'"/> '.csrf_field().' <input type="submit" value="Transaction Log" class="btn btn-warning pull-left button-style" /></form>';
//                            }
                        }
                    }
                }
                if(auth()->user()->can('Bookings Order')) {
                    if ($payments->booking_id)
                        $buttons .= '<form method="POST" action="'.url('admin/payment/edit').'"><input type="hidden" name="id" value="'.$payments->booking_id.'"/> '.csrf_field().' <input type="submit" value="Bookings" class="btn btn-info pull-left button-style"  onclick="row_highlight(\'booking' . $payments->booking_id . '\')"/></form>';
                    // $buttons .= '<a href="' . url('admin/payment/edit') . '" id="booking' . $payments->id . '" class="btn btn-info pull-left button-style" onclick="row_highlight(\'booking' . $payments->id . '\')" >Bookings</a>';
                }

                $name = '';
                if(isset($payments->user_id)) {
                    if($payments->sale->status == "b")
                        $buttons .= '<a href="#viewuser" class="btn btn-info pull-left button-style view-user" data-toggle="modal" style="margin-right: 3px;" data-href="#edit" data-id="' . $payments->user_id . '" >View User</a>';
                }

                if(auth()->user()->can('Resend Email')) {


                    if (isset($payments->sale))
                        if ($payments->sale->confirmation_sent == 1)
                            if($payments->sale->status == "b")
                                $buttons .= '<a href="#." id="resend' . $payments->booking_id . '" class="btn btn-success pull-left button-style " onclick="resend_confirm(\'' . $payments->booking_id . '\');row_highlight(\'resend' . $payments->booking_id . '\')" >Resend Email</a>';
                }
                if(auth()->user()->can('Edit Booking')) {
                    $buttons .= '<a href="' . url('admin/edit/' . $payments->booking_id . '/schoolterm/booking') . '" id="booking-edit' . $payments->id . '" class="btn btn-info pull-left button-style" onclick="row_highlight(\'booking-edit' . $payments->id . '\')" >Edit</a>';
                }
//                if(isset($payments->user_id)) {
//                    if($payments->sale->status == "b")
                if(auth()->user()->can('Move Booking')) {
                    $buttons .= '<a href="#move_booking" class="btn btn-info pull-left button-style view-user" data-toggle="modal" style="margin-right: 3px;" data-href="#move_booking" onclick="moveBooking(\''.$payments->booking_id.'\')">Move Booking</a>';
                }

                return $buttons;
            })
            ->setRowClass(function ($payments) {
                if($payments->amount != $payments->account_balance){
                    return 'alert-danger';
                }else
                    if($payments->checked ==  1){
                        return 'alert-primary';
                    }
            })
            ->make(true);

    }

    public function payments_search_filters($payments, $request)
    {
        // $date = date('Y-m-d', strtotime($request->search_date));
        $date_end = date('Y-m-d', strtotime($request->search_date_end));
        $now = Carbon::now();

        $search_input = $request->search_input;
        Session::put('payments-search', $search_input);

        // if (isset($request->search_date) && !empty($request->search_date)) {
        //     if (isset($request->search_date_end) && !empty($request->search_date_end)) {
        //         $payments->whereDate('payments.created_at', '>=', $date)
        //             ->whereDate('payments.created_at', '<=', $date_end);
        //     }else{
        //          $payments->whereDate('payments.created_at', '>=', $date);
        //             // ->whereDate('payments.created_at', '<=', $date_end);
        //     }

        // } else
        if (isset($request->search_bookingId) && !empty($request->search_bookingId)) {
            $payments->where('sales.booking_id', $request->search_bookingId);
        }else{
         if(isset($request->search_date_end) && !empty($request->search_date_end)) {
               $payments->whereDate('sales.start', '<=', $date_end)
                    ->whereDate('sales.end', '>=', $date_end);
            // $payments->whereDate('payments.created_at', '<=', $date_end);
        } else {
            $todayDate= date('Y-m-d');
            $endDate = date('Y-m-d', strtotime('-1 years'));
            $payments->whereDate('sales.start', '<=', $todayDate)
                    ->whereDate('sales.end', '>=', $endDate);
            // $payments->whereYear('payments.created_at', $now->year);
        }

        if (isset($request->search_company) && !empty($request->search_company)) {

            $payments->where('companies.name', 'LIKE', DB::raw("'%$request->search_company%'"));
        }
        

        if (isset($request->search_block) && !empty($request->search_block)) {
            $payments->where('sales.email', 'LIKE', DB::raw("'%$request->search_block%'"));
        }

        if (isset($search_input) && !empty($search_input)) {
            $payments->where(function ($query) use ($search_input) {
                $string = explode(' ', $search_input);
                if (isset($string[0])) {
                    $search_input = $string[0];
                } else {
                    $search_input = $search_input;
                }
                if (isset($string[1])) {
                    $search_input = $string[1];
                } else {
                    $search_input = $search_input;
                }
                if (isset($string[2])) {
                    $search_input = $string[2];
                } else {
                    $search_input = $search_input;
                }

                if ($search_input == "reserved") {
                    $query->orWhere('sales.status', '=', 'r');
                }
                if ($search_input == "booked") {
                    $query->orWhere('sales.status', '=', 'b');
                }
                if ($search_input == "lost") {
                    $query->orWhere('sales.status', '=', 'l');
                }
                $query->orWhere('users.name', 'LIKE', DB::raw("'%$search_input%'"))
                    ->orWhere('users.surname', 'LIKE', DB::raw("'%$search_input%'"))
                    ->orWhere('users.email', 'LIKE', DB::raw("'%$search_input%'"))
                    ->orWhere('users.username', 'LIKE', DB::raw("'%$search_input%'"));
            });
        }
    }

        return $payments;
    }

    public function payments_record_permission($payments)
    {
        $user = Auth::user();
        $franchisees = $user->franchisee()->pluck('franchisee_id')->toArray();

        if (auth()->user()->can('All Records')) {


        } elseif (Auth::user()->company_id && $franchisees) {

            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id', $franchisees)->pluck('company_id')->toArray();
            array_push($franchisee_companies, auth()->user()->company_id);
            $payments->Where(function ($query) use ($franchisee_companies) {
                $query->whereIn('sales.company_id', $franchisee_companies)
                    // ->orWhere('sales.company_id', Auth::user()->company_id)
                    ->orWhere('payments.user_id', Auth::user()->id);
            });

        } elseif ($franchisees) {

            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id', $franchisees)->pluck('company_id')->toArray();

            $payments->Where(function ($query) use ($franchisee_companies) {
                $query->whereIn('sales.company_id', $franchisee_companies)
                    ->orWhere('payments.user_id', Auth::user()->id);
            });

        } elseif (Auth::user()->company_id) {
            $payments->Where(function ($query) {
                $query->Where('payments.user_id', Auth::user()->id)
                    ->orWhere('sales.company_id', Auth::user()->company_id);
            });

        } else {
            $payments->where('payments.user_id', Auth::user()->id);
        }

        return $payments;
    }

    public function payments_radio_filters($payments, $request)
    {
        if ($request->record_type == 'all'){

        }else if ($request->record_type == 'paid') {
            $payments->where('sales.status','b');
            $payments->where('payments.has_transaction',1);

        }else if ($request->record_type == 'lost') {
            $payments->where('sales.status','l');

        }else if ($request->record_type == 'reserved') {
            $payments->where('sales.status','r');
        }

        return $payments;
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
//        $id;
//        dd($request->all());


        if (isset($request->id)) {
            $id = $request->id;

        } else {

            $id = $request->session()->get('booking_id_payment');

        }
        $record_access = false;
        $payments = Payment::leftJoin('sales', function ($join) {
            $join->on('sales.booking_id', '=', 'payments.booking_id');
        })
            ->join('companies', 'companies.id', '=', 'sales.company_id')
            ->join('users', 'users.id', '=', 'payments.user_id');

        $user = Auth::user();
        $franchisees = $user->franchisee()->pluck('franchisee_id')->toArray();

        if (auth()->user()->can('All Records')) {
            $record_access = true;

        } elseif (Auth::user()->company_id && $franchisees) {
            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id', $franchisees)->pluck('company_id')->toArray();

            $payments->Where(function ($query) use ($franchisee_companies) {
                $query->whereIn('sales.company_id', $franchisee_companies)
                    ->orWhere('sales.company_id', Auth::user()->company_id)
                    ->orWhere('payments.user_id', Auth::user()->id);
            })->where('payments.booking_id', $id)->first();

            if (!empty($payments))
                $record_access = true;

        } elseif ($franchisees) {
            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id', $franchisees)->pluck('company_id')->toArray();

            $payments->Where(function ($query) use ($franchisee_companies) {
                $query->whereIn('sales.company_id', $franchisee_companies)
                    ->orWhere('payments.user_id', Auth::user()->id);
            })->where('payments.booking_id', $id)->first();
            if (!empty($payments))
                $record_access = true;
        } elseif (Auth::user()->company_id) {
            $payments->Where(function ($query) {
                $query->Where('payments.user_id', Auth::user()->id)
                    ->orWhere('sales.company_id', Auth::user()->company_id);
            })->where('payments.booking_id', $id)->first();
            if (!empty($payments))
                $record_access = true;

        } else {
            $payments->where('payments.user_id', Auth::user()->id)->where('payments.booking_id', $id)->first();
            if (!empty($payments))
                $record_access = true;
        }

        if ($record_access == true) {

            $transactions = $lockerInfo = Sale::where('booking_id', $id)->get();
            $charities = $lockerInfo = Transaction::where('booking_id', $id)
                ->where('transactions.type', '03')
                ->get();

            return view('backend.pages.payments.edit', compact('transactions', 'charities'));
        } else {

            return redirect()->back()->with('error_message', 'You don\'t have access of this record!');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function updateUser(Request $request, $id)
    {

       // dd($request->all());

        $user = User::findOrFail($id);

        $this->validate($request, [
            'name' => 'required|max:120',
            'email' => 'required|email|unique:users,email,' . $id,
        ]);

        $array = ['name', 'surname','email','phone_number', 'address_1', 'address_2'];


        if ($request['password'] && $request['password_confirmation']) {

            array_push($array, 'password');

            $request->merge(['password' => Hash::make($request['password'])]);

            $input = $request->only($array);
        } else {

            $input = $request->only($array);
        }
       // dd($input);
        $user->fill($input)->save();
        $sale = Sale::where('user_id',$id)->update([
            'child_first_name' => $request->name,        
            'child_surname' => $request->surname,        
            'email' => $request->email,
            'phone_number' => $request->phone_number,        

            ]);
        // dd($user);
        return redirect()->back()
            ->with('flash_message',
                'User successfully edited.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getuserroles(Request $request)
    {
//        dd($request->all());
        $user = User::findOrFail($request['user_id']);
        $roles = Role::get();

        $login_user = Auth::user();
        $franchisees = $login_user->franchisee()->pluck('franchisee_id')->toArray();
        if (auth()->user()->can('All Records')) {
            $companies = Company::all();
        } elseif (Auth::user()->company_id && $franchisees) {

            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id', $franchisees)->pluck('company_id')->toArray();

            $companies = Company::whereIn('id', $franchisee_companies)->orwhere('id', Auth::user()->company_id)->get();

        } elseif ($franchisees) {

            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id', $franchisees)->pluck('company_id')->toArray();
            $companies = Company::whereIn('id', $franchisee_companies)->get();
        } elseif (Auth::user()->company_id) {

            $companies = Company::where('id', Auth::user()->company_id)->get();

        } else {

            $companies = Company::where('id', null)->get();

        }
        $franchisees = Franchisee::orderBy('name', 'ASC')->get();
        $modal = view('backend.pages.payments.viewuser', compact('user', 'roles', 'franchisees', 'companies'))->render();
        return response()->json($modal);
    }

    public function changeSalePrice(Request $request)
    {
        Sale::where('id', $request['sale_id'])->update([
            'price' => $request['price']
        ]);
        $sale = Sale::where('id', $request['sale_id'])->first();
        $reduce_sale_price = abs($request['original_price'] - $request['price']);

        $transaction = new Transaction();
        $transaction->booking_id = $request['booking_id'];
        $transaction->user_id = Auth::user()->id;
        $transaction->currency = $sale->company->currency;
        $transaction->amount = $reduce_sale_price;
        $transaction->type = '05';
        $transaction->fraud_score = 0;
        $transaction->notes = $request['notes'];
        $transaction->save();

        return redirect('admin/payment/' . $request['booking_id'] . '/edit')
            ->with('flash_message',
                'Price successfully edited.');

    }

    public function refundPayment(Request $request)
    {
        // update the name 
        // working by amir ishaque
         Sale::where('id', $request['sale_id'])->update([
            'child_first_name' => $request['first_name'],
            'child_surname' => $request['surname'],

        ]);
        // update end 

        $request->session()->put('booking_id_amount', $request['booking_id']);
        $allPaidTransactions = Transaction::where('booking_id', $request['booking_id'])->where(function ($query) {
            return $query->where('type', '01')
                ->orWhere('type', '02')
                ->orWhere('type', '03');
        })->where('status', '0000')->get();
        $Authorization = paymentConfig::integrationKey();

        $refunded = 0;

        $amount_to_refund = floatval($request['refund_amount']) * 100;

        foreach ($allPaidTransactions as $paidTransaction) {

            if (empty($paidTransaction->transaction_id)) {

                $this->saveTransaction($paidTransaction->booking_id, $paidTransaction->payment_id, "", "", "", 0, 1010, "Refund Manually, Old Transaction", 0, "Refund Manually, Old Transaction");
                return redirect()->back()
                    ->with('error_message', "Refund Manually, Old Transaction");
                break;
            }

            $retrieve_transaction_url = \Config::get('app.retrieve_transaction_url');
            $retrieve_transaction_url = str_replace("<transactionId>", $paidTransaction->transaction_id, "$retrieve_transaction_url");
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $retrieve_transaction_url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Basic $Authorization",
                    "Cache-Control: no-cache"
                ),
            ));

            $retrieve_transaction_response = curl_exec($curl);
            $retrieve_transaction_response = json_decode($retrieve_transaction_response);
            $err = curl_error($curl);
            curl_close($curl);

            $amounts = get_object_vars($retrieve_transaction_response->amount);
            $totalAmount = $amounts['totalAmount'];
            $previous_refunds = RefundTransaction::where('transaction_id', $paidTransaction->transaction_id)->where('status', 1)->sum('amount');
            $previous_refunds = $previous_refunds * 100;
            $available = $totalAmount - $previous_refunds;

            if ($amount_to_refund == 0) {

                return redirect()->back()
                    ->with('flash_message', "Successfully Refunded");
                break;
            }

            if ($available != 0) {
                if ($amount_to_refund < $available) {
                    $available = $amount_to_refund;
                } else {
                    $available = $available - $refunded;
                }

                $return = $this->refund($paidTransaction->payment_id, intval($available), $request['notes'], $paidTransaction->transaction_id, $paidTransaction->booking_id, $paidTransaction->last4Digits, $paidTransaction->currency, $request['message'], $request['sendEmail'], $request['updatepayment']);

                if ($return['success']) {
                    $amount_to_refund = $amount_to_refund - $available;
                    $refunded += $available;
                } else {
                    return redirect()->back()
                        ->with('error_message', $return['message']);
                    break;
                }
            }
        }
        return redirect()->back()
            ->with('flash_message', 'Successfully Refunded');
    }

    public function refund($payment_id, $amount, $notes, $trxid, $booking_id, $last4Digits, $currency, $message, $sendEmail, $updatepayment)
    {

        $Authorization = paymentConfig::integrationKey();

        $vendorTxCode = 'lockers_' . Carbon::now()->format('Ymdhis') . '_' . $booking_id;

        $curl = curl_init();
        $refund_transaction_data = [
            'transactionType' => 'refund',
            'vendorTxCode' => '' . $vendorTxCode . '',
            'referenceTransactionId' => $trxid,
            'amount' => $amount,
            'currency' => $currency,
            'description' => 'lockers Refund Amount',
        ];

        $refund_transaction_data = json_encode($refund_transaction_data);
        $refund_url = \Config::get('app.refund_url');
        curl_setopt_array($curl, array(
            CURLOPT_URL => $refund_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $refund_transaction_data,
            CURLOPT_HTTPHEADER => array(
                "Authorization: Basic $Authorization",
                "Cache-Control: no-cache",
                "Content-Type: application/json"
            ),
        ));

        $refund_response = curl_exec($curl);

        $refund_response = json_decode($refund_response);

        $err = curl_error($curl);

        curl_close($curl);

        if (isset($refund_response->status)) {
            if ($refund_response->statusCode == '0000') {

                $status_message = $refund_response->statusCode . ':' . $refund_response->statusDetail;

                $this->saveTransaction($booking_id, $payment_id, $vendorTxCode, $currency, $refund_response, $amount, 0000, $status_message, 0, $notes);

                $insert = RefundTransaction::insert([
                    'transaction_id' => $trxid,
                    'amount' => $amount / 100,
                    'status' => 1,
                ]);

                $update_payment = Payment::where('id', $payment_id)->update([
                    'has_transaction' => 1,
                ]);

                if ($sendEmail == "on") {
                    $this->sendEmail($payment_id, $booking_id, $amount / 100, $currency, $message);
                }

                if ($updatepayment == "on") {
                    $paymentRecord = Payment::where('id', $payment_id)->first();
                    $previous_amount = $paymentRecord->amount;
                    $money_refunded = floatval($amount / 100);
                    $new_amount = floatval($previous_amount - $money_refunded);

                    $success = Payment::where('id', $payment_id)->update([
                        'account_balance' => $new_amount,
                    ]);
                }

                return (['success' => true, 'message' => 'Successfully Refunded']);
            }
        } else if (isset($refund_response->code)) {
            if ($refund_response->code == 1018) {

                $status_message = $refund_response->code . ':' . $refund_response->description;

                $this->saveTransaction($booking_id, $payment_id, $vendorTxCode, $currency, $refund_response, $amount, $refund_response->code, $status_message, 1, $notes);

                return (['success' => false, 'message' => $refund_response->description]);
            }
        } else {

            $status_message = 'Refund failed!';
            //dd($refund_response);

            $this->saveTransaction($booking_id, $payment_id, $vendorTxCode, $currency, $refund_response, $amount, 1, $status_message, 1, $notes);

            return (['success' => false, 'message' => $status_message]);
        }
    }

    public function saveTransaction($booking_id, $payment_id, $vendorTxCode, $currency, $refund_response, $amount, $status, $status_message, $fraud_score, $notes)
    {

        $last4Digits = "";
        $transaction_id = "";
        if (isset($refund_response->paymentMethod)) {
            $last4Digits = $refund_response->paymentMethod->card->lastFourDigits;
            $transaction_id = $refund_response->transactionId;
        }
        $transaction = new Transaction();
        $transaction->booking_id = $booking_id;
        $transaction->payment_id = $payment_id;
        $transaction->vendor_tx_code = $vendorTxCode;
        $transaction->user_id = Auth::user()->id;
        $transaction->currency = $currency;
        $transaction->last4Digits = $last4Digits;
        $transaction->refund = $amount / 100;
        $transaction->type = '06';
        $transaction->status = $status;
        $transaction->status_message = $status_message;
        $transaction->fraud_score = $fraud_score;
        $transaction->transaction_id = $transaction_id;
        $transaction->notes = $notes;
        $transaction->save();

        return true;
    }

    public function sendEmail($payment_id, $booking_id, $amount, $currency, $message)
    {

        $payment = Payment::where('id', $payment_id)->first();
        $user = User::where('id', $payment->user_id)->first();
        $current_date_time = Carbon::now()->toDateTimeString();
        $transaction = new Transaction();
        $transactions = $transaction->getOutstaningBalance($booking_id);

        $balance = '0';

        if (isset($transactions->first()->account_balance)) {
            $balance = $transactions->first()->account_balance;
        } else {
            $balance = '0';
        }

        $request_email_template = EmailTemplate::where('id', 10)->first();
        $to = $user->email;
        //$to = 'rperris@xweb4u.com';
        $subject = $request_email_template->subject;
        $template_text = $request_email_template->template_text;

        $placeholders = ["%name%", "%booking_id%", "%amount%", "%date%", "%balance%", "%currency%", "%message%"];
        $placeholder_values = [$user->name, $booking_id, $amount, date("F jS, Y", strtotime($current_date_time)), $balance, $currency, $message];
        $email_html = str_replace($placeholders, $placeholder_values, $template_text);
        $sms_body = null;
        $phone_number = null;

        $sale = new Sale();
        $sale->saveEmailForCron($to, $phone_number, $subject, $email_html, $sms_body);

    }

    public function refundPaymentList(Request $request)
    {

        if (isset($request->id)) {
            $id = $request->id;
        } else {
            $id = $request->session()->get('booking_id_amount');
        }
        $record_access = false;
        $payments = Payment::leftJoin('sales', function ($join) {
            $join->on('sales.booking_id', '=', 'payments.booking_id');
        })
            ->join('companies', 'companies.id', '=', 'sales.company_id')
            ->join('users', 'users.id', '=', 'payments.user_id');

        $user = Auth::user();
        $franchisees = $user->franchisee()->pluck('franchisee_id')->toArray();

        if (auth()->user()->can('All Records')) {
            $record_access = true;

        } elseif (Auth::user()->company_id && $franchisees) {
            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id', $franchisees)->pluck('company_id')->toArray();

            $payments->Where(function ($query) use ($franchisee_companies) {
                $query->whereIn('sales.company_id', $franchisee_companies)
                    ->orWhere('sales.company_id', Auth::user()->company_id)
                    ->orWhere('payments.user_id', Auth::user()->id);
            })->where('payments.booking_id', $id)->first();

            if (!empty($payments))
                $record_access = true;

        } elseif ($franchisees) {
            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id', $franchisees)->pluck('company_id')->toArray();

            $payments->Where(function ($query) use ($franchisee_companies) {
                $query->whereIn('sales.company_id', $franchisee_companies)
                    ->orWhere('payments.user_id', Auth::user()->id);
            })->where('payments.booking_id', $id)->first();
            if (!empty($payments))
                $record_access = true;
        } elseif (Auth::user()->company_id) {
            $payments->Where(function ($query) {
                $query->Where('payments.user_id', Auth::user()->id)
                    ->orWhere('sales.company_id', Auth::user()->company_id);
            })->where('payments.booking_id', $id)->first();
            if (!empty($payments))
                $record_access = true;

        } else {
            $payments->where('payments.user_id', Auth::user()->id)->where('payments.booking_id', $id)->first();
            if (!empty($payments))
                $record_access = true;
        }

        if ($record_access == true) {
            $transaction = new Transaction();
            $data = new Transaction();
            $transactions = $transaction->getOutstaningBalance($id);
            $payment = Payment::where('booking_id', $id)->first();
            $data = $data->getOutstaningBalance($id);
            if ($data) {
                if ($payment->amount != $data->first()->account_balance) {
                    $not_balanced = true;
                    return view('backend.pages.payments.refund-list', compact('transactions', 'not_balanced', 'payment', 'data'));
                } else {
                    return view('backend.pages.payments.refund-list', compact('transactions'));
                }
            }
        } else {

            return redirect()->back()->with('error_message', 'You don\'t have access of this record!');
        }
    }

    public function filter(Request $request)
    {
        $transaction = new Transaction();
        $data = new Transaction();
        $start_date = date('Y-m-d 00:00:00', strtotime($request->start_date));
        $end_date = date('Y-m-d 23:00:00', strtotime($request->end_date));
        $transactions = $transaction->getOutstaningBalance($request->booking_id);
        $transactions = $transactions->where('created_at', '>=', $start_date)
            ->where('created_at', '<=', $end_date);
        $payment = Payment::where('booking_id', $request->booking_id)->first();
        $data = $data->getOutstaningBalance($request->booking_id);

        if ($transactions->isEmpty()) {
            return redirect('admin/refundPaymentList/')
                ->with('error_message',
                    'No Records Found')->with('id', $request->booking_id);
        } else {
            if ($payment->amount != $data->first()->account_balance) {
                $not_balanced = true;
                return view('backend.pages.payments.refund-list', compact('transactions', 'not_balanced', 'payment', 'data'));
            } else {
                return view('backend.pages.payments.refund-list', compact('transactions'));
            }
        }

    }

    public function getPaymentTypes($booking_id, $type)
    {
        // $input = $request->all();
        $transaction = new Transaction();
        $data = $transaction->getAmount($booking_id, $type);
        return response()->json($data);
    }

    public function changePrice(Request $request)
    {
        $request->session()->put('booking_id_amount', $request->booking_id);
        $transaction_data = Transaction::where('booking_id', $request->booking_id)->first();
        $type = '';
        if ($request->type == 'l') {
            $type = '05';
        } else if ($request->type == 'c') {
            $type = '04';
        }
        $amount = floatval($request->amount);
        //dd($amount, $request->all());
        $data = Transaction::insert([
            'booking_id' => $request->booking_id,
            'payment_id' => $transaction_data->payment_id,
            'user_id' => Auth::user()->id,
            'amount_change' => $amount,
            'type' => $type,
            'notes' => $request->reason,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        $update_payment = Payment::where('id', $transaction_data->payment_id)->update([
            'has_transaction' => 1,
        ]);
        return redirect()->back()->with('success', ['Success']);
    }

    public function resendEmail(Request $request)
    {

        $sales = Sale::where('booking_id', $request->booking_id)->get();
        foreach ($sales as $sale) {
            $booking = Sale::find($sale->id);
            $booking->confirmation_sent = 0;
            $booking->save();
        }

        return response()->json('success');
    }

    public function fixPayments()
    {
        ini_set('memory_limit', '-1');
        $payments = Payment::all();

        foreach ($payments as $key => $value) {
            Payment::where('id', $value->id)->whereNotNull('amount')->update([
                'account_balance' => $value->amount
            ]);
        }
    }

    public function fixPaymentsCreatedAt()
    {
        ini_set('memory_limit', '-1');
        $payments = Payment::whereNull('created_at')->get();

        foreach ($payments as $key => $value) {
            $sale = Sale::where('id', $value->booking_id)->first();
            if (isset($sale->updated_at) && !empty($sale->updated_at)) {
                Payment::where('id', $value->id)->update([
                    'created_at' => $sale->updated_at,
                ]);
            }
        }
    }

    public function fixPaymentTransactions()
    {
        ini_set('memory_limit', '-1');
        $payments = Payment::where('has_transaction', 0)->get();

        foreach ($payments as $key => $value) {
            $update = Payment::where('id', $value->id)->update([
                'has_transaction' => 0,
            ]);
        }
        foreach ($payments as $key => $value) {
            $sale = Transaction::where('payment_id', $value->id)->first();
            if (isset($sale) && !empty($sale)) {
                Payment::where('id', $value->id)->update([
                    'has_transaction' => 1,
                ]);
            }
        }
    }

    //change status click on bookings button in orders and click on edit
    public function changeStatus(Request $request)
    {
         $request->session()->put('booking_id_payment', $request->booking_id);
        if (isset($request->date)) {
            $update = Sale::where('id', $request->booking_id)->update([
                'status' => $request->type,
                'hold_booking_date' => date('Y-m-d', strtotime($request->date)),
            ]);

        } else {
            $update = Sale::where('id', $request->booking_id)->update([
                'status' => $request->type,
            ]);

        }

        //next year booking will change status lost if user make
        if($request->type == 'l') {

            $update_next_year = Sale::where('hold_sales_id', $request->booking_id)->first();
            if($update_next_year){
                $update_next_year->status = $request->type;
                $update_next_year->save();

                //add sales_audit log
                $sales_audit_obj = new SalesAudit();
                $sales_audit_obj->sales_audit_log($update_next_year->id, $type = 'validate');
            }


        }elseif($request->type == 'b'){
            //next year booking
            $update_next_year = Sale::where('hold_sales_id', $request->booking_id)->first();
            if($update_next_year){
                $update_next_year->status = 'r';
                $update_next_year->save();

                //add sales_audit log
                $sales_audit_obj = new SalesAudit();
                $sales_audit_obj->sales_audit_log($update_next_year->id, $type = 'validate');
            }
        }

        //add sales_audit log
        $sales_audit_obj = new SalesAudit();
        $sales_audit_obj->sales_audit_log($request->booking_id, $type = 'validate');


        return redirect()->back();
    }

    public function getBlocks(Request $request)
    {
//        dd($request->all());
        $booking_id = $request->booking_id;

        $response = $this->show_bookings($booking_id);

        $hold_next = Sale::where('hold_sales_id', $booking_id)->first();
//dd($hold_next);
        if (!empty($hold_next)) {
            $hold_next_year = true;
        } else {
            $hold_next_year = false;
        }
        $payment = Payment::where('booking_id', $booking_id)->first();
        if ($payment->checked == 1) {
            $move_checked = true;
        } else {
            $move_checked = false;
        }


        $data = [
            'blocks' => $response[0],
            'locker_ids' => $response[1],
            'hold_next_year' => $hold_next_year,
            'move_checked' => $move_checked,
        ];
//      dd($bookings);

        return response()->json($data);
    }

    public function show_bookings($booking_id)
    {

        Session::put('saleKey', $booking_id);

        $childrens = Sale::where('booking_id', $booking_id)->get();


        $childrens_count = $childrens->count();

        $child_blocks = [];
        $child_academic_year = [];
        $locker_ids = [];

        $selected_block_and_number_group = [];

        foreach ($childrens as $index => $children) {
            $selected_block_and_number = [];
            $block_name = null;

            if (isset($children->block_id))
                $block_name = $children->block->name;

            $locker_number = $children->number;
            array_push($selected_block_and_number, $block_name);
            array_push($selected_block_and_number, $locker_number);
            array_push($selected_block_and_number_group, $selected_block_and_number);

            $index++;

            $location_group = Sale::join('companies', 'sales.company_id', '=', 'companies.id')
                ->where('sales.id', $children->id)
                ->select('companies.location_group_id')->first();

            $location_group = $location_group->location_group_id;

            if ($location_group == 1) {

                if (empty($children->school_year_id)) {
                    $school_year = SchoolYear::whereDate('from_date', $children->start)->whereDate('to_date', 'LIKE', '%' . Carbon::parse($children->end)->format('Y-m-d') . '%')->first();
//                    dd($school_year);
                    if ($school_year) {
                        $school_year_id = $school_year->id;
                    } else {
                        $start_title = Carbon::parse($children->start)->format('d M, Y');
                        $end_title = Carbon::parse($children->end)->format('d M, Y');
                        $title = $start_title . ' - ' . $end_title;
                        $dontAllowDuplicate = SchoolYear::where('title', $title)->first();
                     if(empty($dontAllowDuplicate)){
                        $SchoolYear = new SchoolYear();
                        $SchoolYear->title = $title;
                        $SchoolYear->from_date = $children->start;
                        $SchoolYear->to_date = $children->end;
                        $SchoolYear->status = 0;
                        $SchoolYear->save();
                        $school_year_id = $SchoolYear->id;
                    }else{
                        $school_year_id = $dontAllowDuplicate->id;
                    }

                    }
                    $children->school_year_id = $school_year_id;

                }

                if (!empty($children->block_id)) {

                    if (empty($children->school_academic_year_id)) {
                        $school_year = BlockYears::where('block_id', $children->block_id)->first();

                        if (!empty($school_year->school_academic_year_id))
                            $children->school_academic_year_id = $school_year->school_academic_year_id;

                    }
                }
                $children->save();

                $schoolYear = SchoolYear::where('id', $children->school_year_id)->first();

                $start = Carbon::parse($schoolYear->from_date)->startOfDay()->toDateTimeString();
                $end = Carbon::parse($schoolYear->to_date)->endOfDay()->toDateTimeString();
//                dd(Session::get('hold_locker_checked'));

                $hold_next = Sale::where('hold_sales_id', $booking_id)->first();

                if (!empty($hold_next)) {
                    $book_next_year = true;
                    Session::put('hold_locker_checked', true);
                } else {
                    $book_next_year = false;
                    Session::put('hold_locker_checked', false);
//                    dd(Session::get('hold_locker_checked'));
                }

                $sales = new Sale();
                $blocks = $sales->school_year_blocks($children->company_id, $children->school_academic_year_id);
//                $blocks = Block::where()
//                 dd($blocks);
                if (count($blocks) > 0) {
                    $options = '';
                    $options .= '<option value="" selected>Choose Block</option>';

                    foreach ($blocks as $block) {
                        $block = Block::where('id', $block->block_id)->where('active', 1)->where('display', 1)->first();
//                        $block = Block::find($block->block_id);

//                        $sales = new Sale();
//                        $available_lockers = $sales->total_available_lockers_in_block($block->id,$start,$end,$book_next_year);
//dd($available_lockers);
//                        if($available_lockers['total_available'] > 0 || $children->block_id == $block->id) {
                        if (!empty($block)) {

                            $rates = $sales->rate_calculation($location_group, $block->id, $children->start, $children->end, $children->booking_id);
                            $unit = $rates[0];
                            $tax = $rates[1];

                            $sale = Sale::find($children->id);
//                            if (Carbon::now() >= Carbon::parse($children->hold_booking_date)->subHour()->toDateTimeString() && Carbon::now() <= $children->hold_booking_date) {
//
//                                $sale->hold_booking_date = $hold_booking_date;
//                            }
                            if ($children->block_id == $block->id) {
                                $sale->price = $unit;
                                $sale->price_original = $unit;
                                $sale->tax = $tax;
                            }

                            $sale->save();


                            $currency = Company::join('currencies', 'companies.currency', '=', 'currencies.currency_name')
                                ->where('companies.id', $children->company_id)
                                ->select('currencies.currency_name', 'currencies.symbol', 'currencies.position')->first();

                            if ($currency->position == 'b') {

                                $options .= '<option  value="' . $block->id . '_' . $unit . '_' . $tax . '" ';
                                if ($block->id == $children->block_id) {
                                    $options .= 'selected';
                                }
                                $options .= '>' . $block->name . ' ' . $currency->symbol . '' . $unit . '</option>';

                            } else {

                                $options .= '<option  value="' . $block->id . '_' . $unit . '_' . $tax . '" ';
                                if ($block->id == $children->block_id) {
                                    $options .= 'selected';
                                }
                                $options .= '>' . $block->name . ' ' . $unit . ' ' . $currency->symbol . '</option>';
                            }
                        }

                    }

                    $options .= '</select>';
//                        dd($unit);

                    $select_html = '<select class="form-control chzn-select blocks fa " id="blocks" data-sale_id="' . $children->id . '" data-start="' . $start . '" data-end="' . $end . '"   data-counter="' . $index . '"   name="block">';

                    $blocks_html = $select_html . '' . $options;

                    array_push($child_blocks, $blocks_html);
//                        dd($child_blocks);

                } else {

                    $options = '';
                    $options .= '<select class="form-control chzn-select  fa " id="blocks" data-sale_id="' . $children->id . '" data-start="' . $start . '" data-end="' . $end . '" data-counter="' . $index . '" name="block">';
                    $options .= '<option  value="" selected>Choose Block</option>';
                    $options .= '</select>';
                    array_push($child_blocks, $options);

                }

                $school_academic_year = SchoolAcademicYear::find($children->school_academic_year_id);
//                dd($children->school_academic_year_id);
                array_push($child_academic_year, $school_academic_year->school_year);

                array_push($locker_ids, $children->locker_id);
            } elseif ($location_group == 2 || $location_group == 3) {

                $start = Carbon::parse($children->start)->format('Y-m-d H:i:s');
                $end = Carbon::parse($children->end)->format('Y-m-d H:i:s');
                $schoolYear = 0;


                $blocks = Block::where('company_id', $children->company_id)->where('active', 1)->where('display', 1)
                    ->orderBy('name', 'ASC')->get();


                if (count($blocks) > 0) {


                    $options = '';

                    $options .= '<option  value="" selected>Choose Block</option>';


                    foreach ($blocks as $block) {

                        $book_next_year = false;

//                        $sales = new Sale();
//                        $available_lockers =  $sales->total_available_lockers_in_block($block->id,$start,$end,$book_next_year);

//                        if($available_lockers['total_available'] > 0 || $children->block_id == $block->id) {


                        $sales = new Sale();
                        $rates = $sales->rate_calculation($location_group, $block->id, $start, $end, $children->booking_id);
                        $lowest_rate = $rates[0];
                        $tax = $rates[1];

                        $currency = Company::join('currencies', 'companies.currency', '=', 'currencies.currency_name')
                            ->where('companies.id', $block->company_id)
                            ->select('currencies.currency_name', 'currencies.symbol', 'currencies.position')->first();

//

                        if ($currency->position == 'b') {

                            $options .= '<option  value="' . $block->id . '_' . $lowest_rate . '_' . $tax . '" ';
                            if ($block->id == $children->block_id) {
                                $options .= 'selected';
                            }
                            $options .= '>' . $block->name . ' ' . $currency->symbol . '' . $lowest_rate . '</option>';
                        } else {
                            $options .= '<option  value="' . $block->id . '_' . $lowest_rate . '_' . $tax . '" ';
                            if ($block->id == $children->block_id) {
                                $options .= 'selected';
                            }
                            $options .= '>' . $block->name . ' ' . $lowest_rate . ' ' . $currency->symbol . '</option>';
                        }

//                        }
                    }

                    $options .= '</select>';

                    $select_html = '<select class="form-control chzn-select blocks fa child' . $index . '" id="blocks' . $index . '" data-sale_id="' . $children->id . '" data-start="' . $start . '" data-end="' . $end . '" data-end="' . $end . '" data-rate="' . $lowest_rate . '" data-tax="' . $tax . '" data-counter="' . $index . '" name="block' . $index . '">';

                    $blocks_html = $select_html . '' . $options;

                    array_push($child_blocks, $blocks_html);
                    array_push($locker_ids, $children->locker_id);

                } else {

                    $options = '';
                    $options .= '<select class="form-control chzn-select blocks child' . $index . ' fa " id="blocks' . $index . '" data-sale_id="' . $children->id . '" data-start="' . $start . '" data-end="' . $end . '" data-counter="' . $index . '" name="block' . $index . '">';
                    $options .= '<option  value="" selected>Choose Block</option>';
                    $options .= '</select>';

                    array_push($child_blocks, $options);
                }
            }
//            $display_lcoker = '<div class="lockers_printing" style="text-align:left;" id="display_lockers'.$index.'"></div>';

        }

//dd($child_blocks);
        $placeholders = FrontendPlaceholder::where('key', 'choose-locker-child-name-date-title')->pluck('placeholder')->toArray();
        $child_name_date_placeholders = implode(",", $placeholders);
//        dd($selected_block_and_number_group);
        $render_view = view('backend.pages.payments.choose_locker_for_move_render', compact('childrens', 'childrens_count', 'location_group', 'schoolYear', 'child_blocks', 'child_name_date_placeholders', 'booking_id', 'child_academic_year', 'selected_block_and_number_group'))->render();
        return [$render_view, $locker_ids];

    }

    public function updateNextYearBooking(Request $request)
    {
//        dd($request->all());
        $booking_id = $request->booking_id;
//        dd($request->checkbox_next);

        if ($request->checkbox_next == 'false') {
//            dd($booking_id);
            $hold_next_years = Sale::where('hold_sales_id', $booking_id)->where('status', '!=', 'b')->get();
            if (!empty($hold_next_year_booking)) {
                foreach ($hold_next_years as $hold_next_year) {
                    $hold_next_year->status = 'l';
                    $hold_next_year->save();
                }
            }
        } else {
            $is_hold_next_reserved = Sale::where('hold_sales_id', $booking_id)->where('status', '!=', 'b')->first();
            if (!empty($hold_next_year_booking)) {
                if (!empty($is_hold_next_reserved)) {
                    $hold_next_years = Sale::where('hold_sales_id', $booking_id)->get();
                    foreach ($hold_next_years as $hold_next_year) {
                        $hold_next_year->status = 'r';
                        $hold_next_year->save();
                    }
                } else {
                    $payment_obj = new Payment();
                    $payment_obj->hold_locker_for_next_year($booking_id);
                }
            }
        }


        return response()->json('success');
    }

    public function updateCheckedStatus(Request $request)
    {
//        dd($request->all());
            Sale::where('id', $request['sale_id'])->update([
            'child_first_name' => $request['first_name'],
            'child_surname' => $request['surname'],
            
        ]);
        
        $booking_id = $request->booking_id;
//        dd($request->checkbox_next);
        if ($request->checkbox_next == 'false') {
//            dd($booking_id);
            $booking = Payment::where('booking_id', $booking_id)->first();
            $booking->checked = 0;
            $booking->save();

        } else {
            $booking = Payment::where('booking_id', $booking_id)->first();
            $booking->checked = 1;
            $booking->save();
        }


        return response()->json('success');
    }

    public function clearAllChecked(Request $request)
    {

        $bookings = Payment::where('checked', 1)->get();
        foreach ($bookings as $booking) {
            $booking->checked = 0;
            $booking->save();
        }

        return response()->json('success');
    }

    public function updateSale(Request $request)
    {

        Session::forget('schoolTermSubmit');
        if (Session::has('saleKey')) {

            $sale = Sale::where('id', $request['sale_id'])->first();
            $company_id = $sale->company_id;
            $start = $sale->start;
            $end = $sale->end;
            $booked_lockers = Sale::where('company_id', $company_id)
                ->where('locker_id', $request['locker_id'])
                ->where('block_id', $request['block_id'])
                ->where(function ($query) {
                    $query->where('status', 'b')
                        ->orWhere('status', 'r');
                })
                ->where(function ($query) use ($start, $end) {
                    $query->where('end', '>=', $start)
                        ->Where('start', '<=', $end);
                })
                ->first();
            if ($booked_lockers) {
                $data = [
                    'status' => 'already_exists',
                    'locker_id' => $request['locker_id']
                ];

            } else {

                $block = Block::find($request['block_id']);

                $locker_move = new LockerMove();
                $locker_move->sale_id = $sale->id;
                $locker_move->booking_id = $sale->booking_id;
                $locker_move->old_locker_id = $sale->locker_id;
                $locker_move->old_locker_number = $sale->number;
                $locker_move->new_locker_id = $request['locker_id'];
                $locker_move->new_locker_number = $request['locker_number'];
                $locker_move->save();

                $sale->locker_id = $request['locker_id'];
                $sale->block_id = $request['block_id'];
                $sale->block_name = $block->name;
                $sale->number = $request['locker_number'];
                $sale->save();


                //update Manual locker code using sale_id
                $code = new Sale();
                $code->update_locker_code($request['sale_id']);

                //add sales_audit log
                $sales_audit_obj = new SalesAudit();
                $sales_audit_obj->sales_audit_log($request['sale_id'], $type = 'validate');

                $hold_next_year_booking = Sale::where('hold_sales_id', $request['sale_id'])->first();
                if ($hold_next_year_booking) {

                    $hold_next_year_booking->locker_id = $request['locker_id'];
                    $hold_next_year_booking->block_id = $request['block_id'];
                    $hold_next_year_booking->block_name = $block->name;
                    $hold_next_year_booking->number = $request['locker_number'];
                    $hold_next_year_booking->save();

                    $code = new Sale();
                    $code->update_locker_code($hold_next_year_booking->id);

                    //add sales_audit log
                    $sales_audit_obj = new SalesAudit();
                    $sales_audit_obj->sales_audit_log($hold_next_year_booking->id, $type = 'validate');
                }

                $data = [
                    'status' => 'available',
                    'locker_id' => $request['locker_id']
                ];

            }
            return response()->json($data);

        } else {
            return response()->json('home_page');
        }

    }

    public function getlockers(Request $request)
    {

        if (Session::has('saleKey')) {
//
            $start = Carbon::parse($request['start'])->format('Y-m-d H:i:s');
            $end = Carbon::parse($request['end'])->format('Y-m-d H:i:s');
//            if(date("m") < 7  ){
//                $start = Carbon::now()->toDateTimeString();
//                $end = Carbon::now()->endOfDay()->toDateTimeString();
//            } else {
//                $start = date("Y-09-01 00:00:00");
//                $end = date("Y-09-01 23:59:59");
//
//            }
            if (Session::get('hold_locker_checked') == true) {
                $book_next_year = true;
            } else {
                $book_next_year = false;
            }
//            dd($book_next_year);
            $sale = Sale::find($request->sale_id);
            $selected_block_id = $sale->block_id;

            $sales = new Sale();
            $available_lockers = $sales->total_available_lockers_in_block($request->block_id, $start, $end, $book_next_year);

//            if($available_lockers['total_available'] > 0 || $selected_block_id == $request->block_id) {

            $lockers = Locker::where('lockers.block_id', $request->block_id)
                ->where('active', 1)
//                        ->where('display',1)
                ->orderBy('locker_order', 'ASC')
                ->get();

            $total_lockers = $lockers->count();
            $lockers->toArray();

            $booked_lockers = Sale::where('booking_id', Session::get('saleKey'))->pluck('locker_id')->toArray();

            $assign_locker = 0;
            if (empty(intval($request->selected_lockers))) {
                $selected_lockers = Sale::where('id', $request->sale_id)->first();
                if (!empty($selected_lockers->locker_id)) {
                    $assign_locker = $selected_lockers->locker_id;
                }
            }

            //$request->selected_lockerssale_id
            $output = '';

            $blocks = Block::leftJoin('locker_block_image', 'locker_block_image.id', '=', 'blocks.locker_block_image_id')
                ->select('blocks.*', 'locker_block_image.file_name as locker_block_image')
                ->where('blocks.id', $request->block_id)->first();

            $output .= '<div class="col-md-12" ><div class="row" style="overflow-x:auto;"><div class="col-md-12 lockers_table" style="padding-left: 0px !important;padding-right:0px !important;" ><table id="locker_table' . $request['counter'] . '">';

            $counter = 0;
            $counter_red = 0;
            for ($i = 1; $i <= $blocks->locker_blocks; $i++) {

                $red_column = 0;
                $output .= '<tr>';
                for ($j = 1; $j <= $blocks->block_height; $j++) {
                    if ($counter_red <= $total_lockers - 1) {
                        $sales = new Sale();
                        $available = $sales->is_locker_available($lockers[$counter_red]['id'], $start, $end, $book_next_year);

                        if (empty($available)) {

                            if (!in_array($lockers[$counter_red]['id'], $booked_lockers))
                                $red_column++;
                        }

                    } else {
                        $red_column++;
                    }

                    $counter_red++;

                }

                for ($j = 1; $j <= $blocks->block_height; $j++) {

                    if ($counter <= $total_lockers - 1) {

                        $sales = new Sale();

                        //// if(in_array($lockers[$counter]['id'],$booked_lockers))
                        $available = 1;
                        //  else
                        $available = $sales->is_locker_available($lockers[$counter]['id'], $start, $end, $book_next_year);
                        $is_off_sale = LockerOffSale::where('locker_id', $lockers[$counter]['id'])->where('active', 1)->first();


                        if (empty($available)) {

                            $output .= '<td ><input type = "radio" name = "locker_id' . $request['counter'] . '" class="invisible-radio' . $request['counter'] . '" checked>';
                            if ($red_column == $blocks->block_height) {
                                $output .= '<label for="radio' . $lockers[$counter]['id'] . '" class="red-lockers" > ';
                            } else {
                                $output .= '<label for="radio' . $lockers[$counter]['id'] . '" class="" > ';

                            }

                            $already_selected = 0;
                            if ($lockers[$counter]['id'] == intval($request->selected_lockers))
                                $already_selected = 1;

                            else if ($lockers[$counter]['id'] == $assign_locker)
                                $already_selected = 1;

                            if (empty($already_selected)) {
                                $output .= '<div class="styled-radio-red' . $request['counter'] . ' text-center" style = "color: white">' . $lockers[$counter]->locker_number . '<br ><i class="fa fa-times icon-cross" ></i ></div ></label ></td>';
                            } else {

                                $output .= '<div class="styled-radio' . $request['counter'] . ' text-center" style = "color: white; border: 5px solid #66ff66">' . $lockers[$counter]->locker_number . '<br ><i class="fa fa-check rounded-circle" style="background-color:#66ff66; padding: 6px""></i ></div ></label ></td>';
                                // $output .= '<div class="styled-radio'.$request['counter'].' text-center" style = "color: white;">'.$lockers[$counter]->locker_number.'<br ><i class="fa fa-check icon-tick" ></i ></div ></label ></td >';
                            }

                        } elseif (!empty($is_off_sale)) {
                            $output .= '<td><input type = "radio" name = "locker_id' . $request['counter'] . '"  class="invisible-radio' . $request['counter'] . '" >
                                <label for="radio' . $lockers[$counter]['id'] . '' . $request['counter'] . '" class="">
                                    <div class="styled-radio-red' . $request['counter'] . ' text-center" style = "color: white;">' . $lockers[$counter]->locker_number . '<br ><i class="fa fa-check rounded-circle" style="background-color:#66ff66; padding: 6px""></i ></div ></label ></td >';
                        } elseif ($lockers[$counter]['active'] == 1 && $lockers[$counter]['display'] == 0) {
                            $output .= '<td><input type = "radio" name = "locker_id' . $request['counter'] . '"  class="invisible-radio' . $request['counter'] . '" >
                                <label for="radio' . $lockers[$counter]['id'] . '' . $request['counter'] . '" class="">
                                    <div class="styled-radio-gray' . $request['counter'] . ' text-center" style = "color: white;"></div ></label ></td >';
                        } else {
                            $output .= '<td><input type = "radio" name = "locker_id' . $request['counter'] . '" id = "radio' . $lockers[$counter]['id'] . '' . $request['counter'] . '" value="' . $lockers[$counter]['id'] . '" data-sale_id = "' . $request->sale_id . '" data-block_id = "' . $lockers[$counter]->block_id . '" data-locker_id = "' . $lockers[$counter]['id'] . '" data-locker_number = "' . $lockers[$counter]->locker_number . '" data-rate = "' . $request['rate'] . '" data-tax = "' . $request['tax'] . '" data-counter = "' . $request['counter'] . '" data-hold_locker="' . $request['hold_locker'] . '" class="invisible-radio' . $request['counter'] . ' choose-locker" >
                                <label for="radio' . $lockers[$counter]['id'] . '' . $request['counter'] . '" class="puffOut">
                                    <div class="styled-radio' . $request['counter'] . ' text-center" style = "color: white;">' . $lockers[$counter]->locker_number . '<br ><i class="fa fa-check icon-tick" ></i ></div ></label ></td >';
                        }


                    } else {
                        //                                                    $output.='<td> <input type = "radio" name = "id'.$request['counter'].'" class="invisible-radio'.$request['counter'].'" >
                        //                                                        <label for="radio" >
                        //                                                            <div class="styled-radio-gray'.$request['counter'].' text-center" style = "color: white" ></div ></label >
                        //                                                    </td>';
                    }
                    $counter++;
                }
                $output .= '</tr>';
            }

            $output .= '</table></div><div style="display: none;" class="col-md-12 locker_image">
                    <img style="width:100%;" src="' . url('uploads/' . $blocks->locker_block_image) . '" class=" img-responsive" alt="Block Image">
                    </div></div></div>';

            return response()->json($output);
//            }
//            else{
//
//                $output='booked';
//                return response()->json($output);
//            }
        } else {
            return response()->json('home_page');

        }


    }

}
