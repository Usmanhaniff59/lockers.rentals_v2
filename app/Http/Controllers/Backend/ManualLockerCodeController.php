<?php

namespace App\Http\Controllers\Backend;

use App\Block;
use App\AuditLog;
use App\Company;
use App\Exports\reports\ManualLockerCodeExport;
use App\FranchiseeCompany;
use App\LocationGroup;
use App\Locker;
use App\ManualLockerCode;
use App\Rate;
use App\Sale;
use App\BlockGroup;

use App\SchoolAcademicYear;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;

class ManualLockerCodeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $locationGroups = LocationGroup::get();
//        $rates = Rate::get(); 
//        $school_academic_year = SchoolAcademicYear::orderBy('order_number', 'asc')->get();


        return view('backend.pages.company.manualLockerCodes.index' );

    }

    public function show_all(Request $request){

        $query = $this->codes_query($request);
        $locker =  $query[0];
        $date_passed =  $query[1];
        $this_month_start_date =  $query[2];
        $this_month_end_date =  $query[3];

        return  $this->codes_dataTable($locker, $date_passed,$this_month_start_date,$this_month_end_date);


    }
    public function codes_query($request){

        $locker = Locker::query();
        $locker->select('lockers.*');

        $search_input = $request->search_input;


        if(date("m") < 7  ){
            $date_passed = date('Y-m-d 00:00:00');
        } else {
            $date_passed = date("Y-09-01 00:00:00");

        }

        $this_month_start_date =   date('Y-m-d 00:00:00');
        $this_month_end_date =   date('Y-m-d 23:59:59');
        
        if (isset($request->search_start_date) && !empty($request->search_start_date)){
            $date_passed = $request->search_start_date;
            $this_month_start_date = date('Y-m-d 00:00:00',strtotime($request->search_start_date));
            $this_month_end_date = date('Y-m-d 23:59:59',strtotime($request->search_start_date));
        }
                $locker->Join('manual_locker_codes','manual_locker_codes.locker_id','=','lockers.id');
        $locker->where('manual_locker_codes.start','<=', date('Y-m-d 00:00:00',strtotime($date_passed)));
        $locker->where('manual_locker_codes.end','>=', date('Y-m-d 00:00:00',strtotime($date_passed)));

        $locker->LeftJoin('companies','companies.id','=','lockers.company_id');
        $locker->LeftJoin('blocks','blocks.id','=','lockers.block_id');
        if($request->radio=='edit'){
        $locker->Join('manual_locker_codes','manual_locker_codes.locker_id','=','lockers.id');
        $locker->where('manual_locker_codes.start','<=', date('Y-m-d 00:00:00',strtotime($date_passed)));
        $locker->where('manual_locker_codes.end','>=', date('Y-m-d 00:00:00',strtotime($date_passed)));
        $locker->where('manual_locker_codes.updated','=', 1);

    }else if($request->radio=='not_edit'){
        $locker->Join('manual_locker_codes','manual_locker_codes.locker_id','=','lockers.id');
        $locker->where('manual_locker_codes.start','<=', date('Y-m-d 00:00:00',strtotime($date_passed)));
        $locker->where('manual_locker_codes.end','>=', date('Y-m-d 00:00:00',strtotime($date_passed)));
        $locker->where('manual_locker_codes.updated','=',0);
    }
        $locker->whereNotNull('lockers.company_id');
        $locker->whereNotNull('lockers.block_id');
        $locker->where('lockers.active',1);
        $locker->where('lockers.display',1);
        $locker->where('blocks.active',1);
        $locker->where('companies.active',1);
        //   if(auth()->user()->hasRole('Franchisee'))
        // {
        //     $franchisee_ids=DB::table('franchisee_user')->where('user_id',auth()->user()->id)->pluck('franchisee_id');
        //     $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id', $franchisee_ids)->pluck('company_id')->toArray();
        //     $locker->whereIn('companies.id', $franchisee_companies);
        //      $locker->orWhere('companies.id',auth()->user()->company_id);
        // }else if(auth()->user()->hasRole('Company'))
        // {
        //      $locker->where('companies.id',auth()->user()->company_id);
        // }
        $locker->orderBy('blocks.order_list','ASC');
        $locker->orderBy('lockers.locker_order','ASC');


        if (isset($search_input) && !empty($search_input)){
            $locker->where(function ($query) use($search_input) {
                $string=explode(' ',$search_input);
                if (isset($string[0])){
                    $search_input=$string[0];
                }else{
                    $search_input=$search_input;
                }
                if (isset($string[1])){
                    $search_input=$string[1];
                }else{
                    $search_input=$search_input;
                }
                if (isset($string[2])){
                    $search_input=$string[2];
                }else{
                    $search_input=$search_input;
                }
                $query->orWhere('companies.name','LIKE',DB::raw("'%$search_input%'"))
                    ->orWhere('blocks.name','LIKE',DB::raw("'%$search_input%'"))
                    ->orWhere('lockers.locker_number','LIKE',DB::raw("'%$search_input%'"));
            });
        }



        if (isset($request->search_company) && !empty($request->search_company)){
            $locker->where('companies.name','LIKE',DB::raw("'%$request->search_company%'"));
        }

        if (isset($request->search_block) && !empty($request->search_block)){
            $locker->where('blocks.name','LIKE',DB::raw("'%$request->search_block%'"));
        }

       $code_obj = new ManualLockerCode();
        $locker = $code_obj->manual_locker_codes_record_permissions($locker);

        return [ $locker, $date_passed,$this_month_start_date,$this_month_end_date];


    }

    public function codes_dataTable($locker, $date_passed,$this_month_start_date,$this_month_end_date){



        return Datatables::of($locker, $date_passed,$this_month_start_date,$this_month_end_date)

            ->addColumn('company_name', function( $locker) {
                $company = Company::where('id', $locker->company_id)->first();
                if(isset($company->name)){
                    return $company->name;
                } else {
                    return "";
                }

            })
            ->addColumn('block', function( $locker) {
                $block = Block::where('id', $locker->block_id)->first();
                $blockGroup = Block::where('id', $locker->block_id)->first();

                if(isset($block->name)){
                    return $block->name;
                } else {
                    return "";
                }

            })->addColumn('block_group', function( $locker) use ($date_passed) {
                $manual_code = ManualLockerCode::where('locker_id', $locker->id)->where('start','<=', date('Y-m-d 00:00:00',strtotime($date_passed)))
                    ->where('end','>=', date('Y-m-d 00:00:00',strtotime($date_passed)))->first();
                    
                    if(isset($manual_code)){
                $blockGroup = BlockGroup::where('id', $manual_code->block_group_id)->first();

                if(isset($blockGroup)){
                    return $blockGroup->name;
                } else {
                    return "";
                }
            }else{
                return "";
            }

            })
            ->addColumn('manual_code', function( $locker) use ($date_passed) {

                $manual_code = ManualLockerCode::where('locker_id', $locker->id)->where('start','<=', date('Y-m-d 00:00:00',strtotime($date_passed)))
                    ->where('end','>=', date('Y-m-d 00:00:00',strtotime($date_passed)))->first();

                if(empty($manual_code)){
                    return "";
                } else {

                    return  '<span id="c-'.$locker->id.'">'.$manual_code->code.'</span>' ;
                }



            })

            ->addColumn('locker_number', function($locker) {


                if(isset($locker->locker_number)){
                    return $locker->locker_number;
                } else {
                    return "";
                }

            })
            ->addColumn('action', function ($locker) use ($date_passed){
//                $buttons ='';
//                if(auth()->user()->can('Edit y')) {
//
                $buttons = '<a href="#edit" class="btn btn-primary btn-md  edit-btn "   data-toggle="modal" style="margin-right: 3px;" data-href="#edit" id="code_edit_btn" style="margin-right: 3px;" onclick="getCode(\''.$locker->id.'\',\''.$date_passed.'\')"  >Edit</a>';
//                }
//                if(auth()->user()->can('Delete y')) {
//                    $buttons.='<a href = "JavaScript:Void(0);" onclick = "deleteData(\''.route('y.destroy', $y->id).'\',\''.csrf_token().'\',\'\',y_table,\'DELETE\')" class="btn btn-danger" > Delete</a >';
//                }
                return $buttons;
            })->setRowClass(function ($locker) use ($date_passed) {
              
                $manual_code = ManualLockerCode::where('locker_id', $locker->id)->where('start','<=', date('Y-m-d 00:00:00',strtotime($date_passed)))
                    ->where('end','>=', date('Y-m-d 00:00:00',strtotime($date_passed)))->first();

                if(!empty($manual_code) && $manual_code->updated==1){
                    return "alert-danger code-".$locker->id." ";
                }else{
                    return "orange-row code-".$locker->id."";
                }

            }) ->escapeColumns('manual_code')->make(true);


    }

    public function codes_excel(Request $request)
    {
//        ini_set('max_execution_time', 900);

//        dd($request->all());
        $query = $this->codes_query($request);
        $locker =  $query[0];
        $data =  $locker->get();

        $date_passed =  $query[1];
        $this_month_start_date =  $query[2];
        $this_month_end_date =  $query[3];

        return Excel::download(new ManualLockerCodeExport($data,$date_passed,$this_month_start_date,$this_month_end_date), 'manualLockerCodes.csv');
    }

    public function getLockerCode(Request $request)
    {

        $manual_code = ManualLockerCode::where('locker_id', $request->locker_id)->where('start','<=', date('Y-m-d 00:00:00',strtotime($request->date_passed)))
            ->where('end','>=', date('Y-m-d 00:00:00',strtotime($request->date_passed)))->first();


        $data = [
            'manual_code' => $manual_code,
            'locker_id' => $request->locker_id,
        ];
        return response()->json($data);



          }

    public function updateCode(Request $request)
    {
//        dd($request->all());
       

        if(isset($request->id)) {
            $manual_code = ManualLockerCode::find($request->id);

            // scenario
            // add data in audit_log
            // Case 1
            // any update in Manual Locker Code insert data in audit_log
            if($request->code!=$manual_code->code)
            {
                $audit_log=new AuditLog();
                $audit_log->user_id =  Auth::id();
                $audit_log->table_name ='manual_locker_codes';
                $audit_log->field = 'code';
                $audit_log->old_val = $manual_code->code;
                $audit_log->updated_val = $request->code;
                $audit_log->action='update';
                $audit_log->save();

            }
            $manual_code->code = $request->code;
            $manual_code->updated=1;
            $manual_code->save();

//            dd($manual_code->start);
            $sales =  Sale::whereDate('end', '>=',$manual_code->start)->where('locker_id',$request->locker_id)->get();

            foreach ($sales as $sale){
                $sale->net_pass = $request->code;
                $sale->save();
            }
            



            // 
        }else{

            if(isset($request->search_date)){
                $searched_year = Carbon::parse($request->search_date)->format('Y');

                $start = $end = $searched_year . '-08-31 00:00:00';
                $next_year = Carbon::parse($start)->addYear()->format('Y');
                $end = $next_year . '-08-31 00:00:00';
            }else {
                $start = date('Y-09-01 00:00:00');
                $next_year = Carbon::now()->addYear()->format('Y');
                $end = $next_year . '-08-31 00:00:00';
            }

            $locker = Locker::find($request->locker_id);
            $code = new ManualLockerCode();
            $code->code = $request->code;
            $code->locker_id = $request->locker_id;
            $code->block_id = $locker->block_id;
            $code->start =$start;
            $code->end =$end;
            $code->save();

             
                $audit_log=new AuditLog();
                $audit_log->user_id =  Auth::id();
                $audit_log->table_name = 'manual_locker_codes';
                $audit_log->field = 'code';
                $audit_log->old_val = $request->code;
                $audit_log->updated_val = $request->code;
                $audit_log->action='save';
                $audit_log->save();

            

            $sales =  Sale::whereDate('end', '>=',$code->start)->where('locker_id',$request->locker_id)->get();

            foreach ($sales as $sale){
                $sale->net_pass = $request->code;
                $sale->save();
            }
        }
           $data = [
            'success' => 1,
        ];
        return response()->json($data);
        // return redirect()->back();


          }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function deleteAllcodes($value='')
    {
         $cards = DB::select("DELETE FROM manual_locker_codes WHERE id IN(SELECT manual_locker_codes.id FROM manual_locker_codes LEFT JOIN lockers ON lockers.id = manual_locker_codes.locker_id WHERE lockers.id IS NULL)");
        dd('Manual Locker codes deleted successfully');
    }
     public function deleteduplicates($value='')
    {
         $cards = DB::select("DELETE FROM manual_locker_codes WHERE id NOT IN (SELECT * FROM (SELECT MAX(n.id) FROM manual_locker_codes n
                        GROUP BY n.code) x)");
        dd('Duplicates Removed From the manual locker code');
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
