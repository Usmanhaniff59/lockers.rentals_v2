<?php

namespace App\Http\Controllers\Backend\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Validator;

// 
use App\Company;
use App\Franchisee;
use App\Franchisee_report;
use App\FranchiseeCompany;
// use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

// use Illuminate\Http\Request;

// use App\User;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Session;
use Spatie\Permission\Traits\HasRoles;
 // use Yajra\DataTables\DataTables;


class CompanyUserController extends Controller
{
    public function index($value='')
    {
    	# code...
    }
        public function show_all(Request $request){
//        dd($request->all());

        $data = $this->query_data($request);
        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                $buttons ='';
//                if(auth()->user()->can('Edit Company')) {
//
//                    $buttons = '<a href="#edit_pupil_premium" class="btn btn-primary btn-md  edit-btn-pupil-premium pull-left" data-toggle="modal" style="margin-right: 3px;"  data-id="'. $data->id .'" data-child_first_name="'. $data->child_first_name .'" data-child_surname="'. $data->child_surname .'">Edit</a>';
//                }
                // @can('User Management')
                if(auth()->user()->can('User Management')) {
                    $buttons .= '<a href="#edit-company-user" class="btn btn-info pull-left button-style user-edit-btn" data-toggle="modal" data-id="'.$data->id.'"  onclick="userEdit('.$data->id.')" style="margin-right: 3px;">Edit</a>
                    <a href="javascript:void(0)" data-id="'.$data->id.'" class="btn btn-danger pull-left button-style user-delete-btn" style="margin-right: 3px;">Delete</a>';
                }else{
                	$buttons .='No Action Available';
                }

                return $buttons;
            })
//            ->setRowClass(function ($data) {
//                // dd($company->active);
//                $active = $company->active;
//                $display = $company->display;
//                //dd($active, $display);
//                if($active == 1 && $display == 1){
//                    return 'alert-success';
//                } else if($active == 0 && $display == 0){
//                    return 'alert-danger';
//                } else if($active == 1 && $display == 0){
//                    return 'alert-warning';
//                }
//            })
            ->make(true);

    }
     public function query_data($request){
//        $query =  PupilPremium::query();
        $company_id = $request->company_id;
        // $search=$request->searchParam;
        // $searchStatus=$request->searchStatus;
        // $searchDate=$request->searchDate;
        
             // if(isset($search) && $search!='undefined' && $search=='Live')
             //    {
             //    $searchquery=['sales.end', '>=', Carbon::now()];
             //    }else if($search=='Archive'){
             //        $searchquery=['sales.end', '<=', Carbon::now()];
             //    }else{
             //         $searchquery=['sales.id', '!=', 0];
             //    }
             //    if(isset($searchStatus) && $searchStatus!='undefined' && $searchStatus=='Booked')
             //    {
             //        $status_search=['sales.status','=','b'];
             //    }else if($searchStatus=='Lost')
             //    {
             //        $status_search=['sales.status','=','l'];   
             //    }else{
             //         $status_search=['sales.id', '!=', 0];
             //    }

             //    if(isset($searchDate) && $searchDate!='undefined' && !empty($searchDate))
             //    {
             //          // $date = Carbon::createFromFormat('Y-m-d H:i:s', $searchDate);
             //        $c_date=date_create($searchDate);
             //         $date=date_format($c_date,"Y-m-d H:i:s");

             //        $StartDateQuery=['sales.start','<=',$date];
             //        $EndDateQuery=['sales.end','>=',$date];

             //    }else{
             //        $StartDateQuery=['sales.id', '!=', 0];
             //        $EndDateQuery=['sales.id', '!=', 0];

             //    }

//       
        $query= DB::select('SELECT id , name , email , company_id FROM users JOIN user_has_roles ON users.id=user_has_roles.user_id WHERE users.deleted=0 AND users.active=1 AND users.company_id= '.$company_id.' AND user_has_roles.role_id =3 ');
        // $query =  User::select('id','name','email','company_id')->where('company_id',$company_id)->where('deleted',0)->where('active',1)
        //     ->get();
        
        return  $query;

    }
        public function getuserroles(Request $request)
    {
        $user = User::findOrFail($request['user_id']);
        $roles = Role::where('name','User')->get();

        //franchisee dropdown with record permission
        $franchisees = Franchisee::with(['franchisee_companies' =>function($query) use($request){
            return $query->where('company_id',$request->company_id);
        }])->orderBy('name', 'ASC')->get();
        // $franchisee_report_obj = new Franchisee_report();
        // $franchisees = $franchisee_report_obj->franchisee_dropdown_record_permissions($franchisees);
        // $franchisees = $franchisees->get();

        //get assigned franchise groups
        $user_franchisees = $user->franchisee()->pluck('franchisee_id')->toArray();

        //get companies according to record permission
        // $companies = Company::query();
        // $company_obj = new Company();
        // $companies = $company_obj->company_record_permissions($companies);
        // $companies = $companies->get();

        $modal = view('backend.pages.company.users.editform', compact('user', 'roles', 'franchisees', 'user_franchisees'))->render();
        // $data ={$modal,$franchisees};
   //       $object = new stdClass();
   // $object->modal = $modal;
   // $object->franchisees = $franchisees;
        $object=['modal'=>$modal];

        return response()->json($object);
    }
    public function companyFranchise(Request $request)
    {
        $roles = Role::get();
        $companyId=$request->company_id;
        //  franchises
          $franchisees = Franchisee::query();

        // permission added
         $franchisee_report_obj =  new Franchisee_report();
            $franchisees = $franchisee_report_obj->franchisee_dropdown_record_permissions($franchisees);
            $franchisees=$franchisees->get(); 

          $modal = view('backend.pages.company.users.createForm', compact('roles', 'franchisees', 'companyId'))->render();
          return response()->json($modal);
    }
    public function update(Request $request)
    {
    	$id=$request->user_profile;
        $user = User::findOrFail($id);

      $this->validate($request, [
            'name' => 'required|max:120',
            'email' => 'required|email|unique:users,email,' . $id,
            // 'response_group_id' => 'required',
            'password' => 'nullable|min:6|confirmed',
            'roles'=>'required',
        ]);
      if (auth()->user()->can('Ticketing Permission')) {
            $this->validate($request,[
                'response_group_id' => 'required',
            ]);
            // $response_group_id=$request->response_group_id;
          }else{
            // $response_group_id=$user;
            $request['response_group_id']=$user->response_group_id;
          }
        $franchisee_groups=$request->franchisee_groups;
        //create new franchise group
   
            $array = ['name', 'email', 'phone_number', 'address_1', 'address_2', 'country', 'city', 'post_code', 'state', 'country'];
        


        if (isset($request->response_group_id)) {
            array_push($array, 'response_group_id');
        }

        //update user password also
        if ($request['password'] && $request['password_confirmation']) {
            array_push($array, 'password');
            $request->merge(['password' => Hash::make($request['password'])]);
            $input = $request->only($array);
        } else {
            $input = $request->only($array);
        }


        $user->fill($input)->save();

        //tickting type assign
        $this->ticktetingUsers(User::findOrFail($id));

        //update roles
        if (auth()->user()->can('Assign Role To User')) {
            $roles = $request['roles'];
            if (isset($roles)) {
                $user->roles()->sync($roles);
            } else {
                $user->roles()->detach();
            }
        }
        return response()->json(['error'=>'updated']);
       // return new JsonResponse(['data'=>'udpate the result'],200);
        // return redirect()->back()->with('flash_message', 'User successfully edited.');
    }
     public function ticktetingUsers($request)
    {

//        if ($_SERVER['HTTP_HOST'] == "dev.locker.rentals") {
            if (isset($request->response_group_id)) {
                $response_group_id = $request->response_group_id;
            } else {
                $response_group_id = 1;
            }
            $array = [
                'name' => $request->name,
                'email' => $request->email,
                'password' => $request->password,
                'response_group_id' => $response_group_id,
            ];
            // ::on('mysql_ticketing')
            $user = User::where('email', $request->email)->first();
            \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            
            if (!empty($user)) {
                $user = User::where('email', $request->email)->update($array);
            } else {
                $user = User::create($array);
            }
            \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
//        }
    }
     public function store(Request $request)
    {
        //validation
        $this->validate($request, [
            'name' => 'required|max:120',
            'email' => 'required|email|unique:users',
            // 'response_group_id' => 'required',
            'password' => 'nullable|min:6|confirmed',
            'roles'=>'required',

        ]);
           if (auth()->user()->can('Ticketing Permission')) {
            $this->validate($request,[
                'response_group_id' => 'required',
            ]);
            // $response_group_id=$request->response_group_id;
          }else{
            $response_group_id=1;
            $request['response_group_id']=1;
          }


          if(!isset($request->password))
          {
            $request['password']='Db123(000';
          }
        //create new franchise group
        $franchisee_groups = $request->franchisee_groups;
        // if ($request->franchisee_name) {
            
        //      $franchisee_id = Franchisee::create(['name' => $request->franchisee_name]);
        //         $franchisee_groups = $franchisee_id;
        //     // assign franchise to company
        //     $FranchiseeCompany = FranchiseeCompany::where([['franchisee_id', '=', $franchisee_id->id],['company_id','=',Session::get('company_id_blocksss')]])->first();
        //     if(empty($FranchiseeCompany)){
               
        //          FranchiseeCompany::create(['franchisee_id'=>$franchisee_id->id,'company_id'=>Session::get('company_id_blocksss')]);
        //     }

        // }
        //create new user with hash password
        $request->merge(['password' => Hash::make($request['password'])]);
        $user = User::create($request->only('email', 'name', 'response_group_id', 'company_id', 'password'));

        //assign franchise groups to user
        // if ($request->franchisee_groups || $request->franchisee_name) {
        //     $user->franchisee()->sync($franchisee_groups);
        // }

        //assign roles to user
        $roles = $request['roles'];
        if (isset($roles)) {
            // foreach ($roles as $role) {
                $role_r = Role::where('id', '=', $roles)->firstOrFail();
                $user->assignRole($role_r);
            // }
        }

//        dd($user);
        $this->ticktetingUsers(User::findOrFail($user->id));

        return response()->json(['success' => 'Successfully added user']);
    }
    public function delete(Request $request)
    {
       $id=$request->id;
        $user = User::findOrFail($id);
        $user->company_id = NULL;
        $user->save();
        return response()->json(['success'=>'User Updated Successfully']);
    }

}
