<?php

namespace App\Http\Controllers\Backend\Company;

use App\Company;
use App\EmailTemplate;
use App\Sale;
use App\VoucherCode;
use App\VoucherCodesIssued;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use App\Traits\UniqueVoucherCode;

class VoucherCodesController extends Controller
{
    use UniqueVoucherCode;

    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $validatedData = $request->validate([
            'start' => 'required',
            'end' => 'required',
            'discount_percent' => 'required|numeric|min:1|max:100',
            'number_of_vouchers' => 'required|numeric|min:1'

        ]);
//        dd($request->start , Carbon::createFromFormat('d M, Y', $request->start)->endOfDay());


        for ($i=0 ; $i < $request->number_of_vouchers ; $i++) {
            $data = VoucherCode::create([
                'start' => Carbon::createFromFormat('d M, Y', $request->start)->startOfDay(),
                'end' => Carbon::createFromFormat('d M, Y', $request->end)->endOfDay(),
                'code' => $this->createUniqueCode(),
                'discount_percent' => $request->discount_percent,
                'company_id' => $request->company_id
            ]);
        }
        return response()->json($validatedData);
    }

    public function show_all(Request $request){
//        dd($request->record_type);


        $company_vouchers = VoucherCode::query();
        $company_vouchers->where('company_id',$request->company_id);
        
        if($request->record_type == 'all'){

        }else if($request->record_type == 'issued'){
            $company_vouchers->where('issued',1);

        }else if($request->record_type == 'not_issued'){

            $company_vouchers->where('issued',0);

        }else if($request->record_type == 'used'){
            $company_vouchers ='';
            $company_vouchers =  VoucherCode::Join('voucher_payments_reports','voucher_payments_reports.voucher_code_id','=','voucher_codes.id');
            $company_vouchers->where('voucher_codes.company_id',$request->company_id);
            $company_vouchers->select('voucher_codes.*');
        }


        return Datatables::of($company_vouchers)
            ->editColumn('discount_percent', function ($company_vouchers) {
                $company_vouchers = $company_vouchers->discount_percent.'%';
                return $company_vouchers;

            })
            ->editColumn('start', function ($company_vouchers) {
                $start = Carbon::parse($company_vouchers->start)->format('d M, Y H:i');
                return $start;

            })
            ->editColumn('end', function ($company_vouchers) {
                $start = Carbon::parse($company_vouchers->end)->format('d M, Y H:i');
                return $start;

            })
            ->editColumn('issued', function ($company_vouchers) {
                if($company_vouchers->issued == 0)
                            return 'No';
                else
                    return 'Yes';
            })

            ->addColumn('action', function ($company_vouchers) {

                $buttons ='';
                $buttons .= '<a href="#assign_voucher_code" class="btn btn-info pull-left button-style view-chat" id="assign_voucher_code_btn" style="margin-right: 3px;"  data-id="'. $company_vouchers->id .'" >Assign Voucher</a>';
                return $buttons;
            })
            ->setRowClass(function ($company_vouchers) {
                if($company_vouchers->issued == 0)
                    return 'alert-danger';

                else
                    return 'alert-success';

            }) ->make(true);
    }
    
    public function query_data($request){

        $company_vouchers = VoucherCode::query();
      
        if(auth()->user()->can('All Records')) {


        }elseif(Auth::user()->company_id) {

            $franchisee_companies = FranchiseeCompany::where('franchisee_id',Auth::user()->franchisee_id)->pluck('company_id')->toArray();

            $company_vouchers->Where(function ($query) use ($franchisee_companies) {
                $query->WhereIn('chats.company_id',$franchisee_companies)
                    ->orWhere('chats.company_id',Auth::user()->company_id);
            });
        }
        elseif( Auth::user()->franchisee_id ) {
            $franchisee_companies = FranchiseeCompany::where('franchisee_id',Auth::user()->franchisee_id)->pluck('company_id')->toArray();

            $company_vouchers->whereIn('chats.company_id',$franchisee_companies);
        }
        elseif( Auth::user()->company_id ){

            $company_vouchers->where('chats.company_id',Auth::user()->company_id);

        }else{
            $company_vouchers->where('chats.company_id',0);
        }

        $company_vouchers->select('companies.name as company_name' ,'chats.title' ,'chats.details' , 'chat_types.type',
            DB::raw('DATE_FORMAT(chats.created_at, "%M %D, %Y %H:%m") as created_at1')
            ,DB::raw('(users.name) as name1' ));
//        $company_vouchers->get();

        return  $company_vouchers;

    }

    public function chatCount(Request $request)
    {
        $query = $this->query_data($request);
        $count =  $query->count();

        return response()->json($count);
    }

    public function getData(Request $request)
    {
        $query = $this->query_data($request);
        if($request->page == 0) {
            $data = $query->take(1000)->get();
        }else{
            $skip = $request->page * 1000;
            $data = $query->skip($skip)->take(1000)->get();
        }
//        dd($query);

        $data = [
            'data' => $data
        ];

        return response()->json($data);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function send_voucher_code(Request $request)
    {
        $company = Company::find($request->company_id);
        $company->send_voucher_code = $request->checked;
        $company->save();

        return response()->json('success');
    }

    public function assignCode(Request $request)
    {

        $validatedData = $request->validate([
            'assignee_name' => 'required',
            'assignee_email' => 'required|email',
        ]);
        $email_template = EmailTemplate::find(12);
        $subject = $email_template->subject;
        $template_text = $email_template->template_text;

        $voucher_code = VoucherCode::find($request->voucher_code_id);
        $start = Carbon::parse($voucher_code->start)->format('d M, Y H:i');
        $end = Carbon::parse($voucher_code->end)->format('d M, Y H:i');
        $discount_percent = $voucher_code->discount_percent;

        $placeholders = ["%name%", "%code%", "%from_date%", "%too_date%", "%discount_percent%", "%message%"];
        $placeholder_values = [$request['assignee_name'], $voucher_code->code, $start, $end, $discount_percent, $request['assignee_message']];
        $email_html = str_replace($placeholders, $placeholder_values, $template_text);

        $phone_number = null;
        $sms_body = null;
        $to = $request->assignee_email;

        $sale = new Sale();
        $sale->saveEmailForCron($to, $phone_number, $subject, $email_html, $sms_body);

        $voucher_code->email = $request->assignee_email;
        $voucher_code->issued = 1;
        $voucher_code->save();

        $voucher_codes_issued = new  VoucherCodesIssued();
        $voucher_codes_issued->Voucher_code_id = $request->voucher_code_id;
        $voucher_codes_issued->name = $request['assignee_name'];
        $voucher_codes_issued->email = $request['assignee_email'];
        $voucher_codes_issued->message = $request['assignee_message'];
        $voucher_codes_issued->save();

        return response()->json($validatedData);
    }

    public function updateIssuedStatus(Request $request)
    {
        $voucher_code = VoucherCode::find($request->voucher_code_id);
        $voucher_code->issued = 1;
        $voucher_code->save();

        return response()->json('success');
    }

    public function showMessages(Request $request)
    {
        $voucher_code_issued = VoucherCodesIssued::where('voucher_code_id',$request->voucher_code_id)->get();

//        foreach ($voucher_code_issued as $data){
//            dd($data->created_at);
////            dd(Carbon::parse($data->created_at)->format('d M, Y H:i'));
////            $data['created_at'] = Carbon::parse($data->created_at)->format('d M, Y H:i');
////            $data->update(array("created_at" => Carbon::parse($data->created_at)->format('d M, Y H:i')));
//
//
//
////            $data->merge(Carbon::parse($data->created_at)->format('d M, Y H:i'));
//
//        }



        return response()->json($voucher_code_issued);
    }
}
