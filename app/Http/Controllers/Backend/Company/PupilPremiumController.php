<?php

namespace App\Http\Controllers\Backend\Company;

use App\Block;
use App\BlockYears;
use App\Company;
use App\FranchiseeCompany;
use App\PupilPremium;
use App\Sale;
use App\SchoolYear;
use App\CompanySchoolYear;
use App\SchoolAcademicYear;
use App\Locker;
use App\User;
use App\Http\Requests\StorePupilPremium;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class PupilPremiumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function show_all(Request $request){
//        dd($request->all());

        $data = $this->query_data($request);
        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                $buttons ='';
//                if(auth()->user()->can('Edit Company')) {
//
//                    $buttons = '<a href="#edit_pupil_premium" class="btn btn-primary btn-md  edit-btn-pupil-premium pull-left" data-toggle="modal" style="margin-right: 3px;"  data-id="'. $data->id .'" data-child_first_name="'. $data->child_first_name .'" data-child_surname="'. $data->child_surname .'">Edit</a>';
//                }
                if(auth()->user()->can('Move Booking')) {
                    $buttons .= '<a href="#move_booking" class="btn btn-info pull-left button-style move_booking_btn" data-toggle="modal" style="margin-right: 3px;" data-href="#move_booking" data-child_first_name="'. $data->child_first_name .'" data-child_surname="'. $data->child_surname .'" onclick="moveBooking(\''.$data->id.'\',\''.$data->booking_id.'\')">Move Booking</a>';
                }

                return $buttons;
            })
//            ->setRowClass(function ($data) {
//                // dd($company->active);
//                $active = $company->active;
//                $display = $company->display;
//                //dd($active, $display);
//                if($active == 1 && $display == 1){
//                    return 'alert-success';
//                } else if($active == 0 && $display == 0){
//                    return 'alert-danger';
//                } else if($active == 1 && $display == 0){
//                    return 'alert-warning';
//                }
//            })
            ->make(true);

    }

    public function query_data($request){
//        $query =  PupilPremium::query();
        $company_id = $request->company_id;
        $search=$request->searchParam;
        $searchStatus=$request->searchStatus;
        $searchDate=$request->searchDate;
        
             if(isset($search) && $search!='undefined' && $search=='Live')
                {
                $searchquery=['sales.end', '>=', Carbon::now()];
                }else if($search=='Archive'){
                    $searchquery=['sales.end', '<=', Carbon::now()];
                }else{
                     $searchquery=['sales.id', '!=', 0];
                }
                if(isset($searchStatus) && $searchStatus!='undefined' && $searchStatus=='Booked')
                {
                    $status_search=['sales.status','=','b'];
                }else if($searchStatus=='Lost')
                {
                    $status_search=['sales.status','=','l'];   
                }else{
                     $status_search=['sales.id', '!=', 0];
                }

                if(isset($searchDate) && $searchDate!='undefined' && !empty($searchDate))
                {
                      // $date = Carbon::createFromFormat('Y-m-d H:i:s', $searchDate);
                    $c_date=date_create($searchDate);
                     $date=date_format($c_date,"Y-m-d H:i:s");

                    $StartDateQuery=['sales.start','<=',$date];
                    $EndDateQuery=['sales.end','>=',$date];

                }else{
                    $StartDateQuery=['sales.id', '!=', 0];
                    $EndDateQuery=['sales.id', '!=', 0];

                }

//       
        $query =  Sale:: Join('lockers', 'lockers.id', '=', 'sales.locker_id')
            ->Join('blocks', 'blocks.id', '=', 'sales.block_id')
            ->Join('companies', 'companies.id', '=', 'sales.company_id')
            ->select('sales.id','sales.status','sales.booking_id', 'blocks.name as block_name', 'lockers.company_id', 'lockers.locker_number','sales.child_first_name','sales.child_surname',DB::raw("CONCAT(sales.child_first_name, ' ' ,sales.child_surname) AS childName"), DB::raw('DATE_FORMAT(sales.start, "%M %D, %Y %H:%m") as start'),DB::raw('DATE_FORMAT(sales.end, "%M %D, %Y %H:%m") as end'))
            ->where([['sales.company_id',$company_id],['sales.title','LOCKER_M'],['sales.pupil_premium',1],$searchquery,$status_search,$StartDateQuery,$EndDateQuery])
            ->get();
//        dd($query);
        return  $query;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(StorePupilPremium $request)
    {
        $company_id = $request->company_id;
        $block_id = $request->block_id;
        $number_of_lockers = $request->number_of_lockers;
        $keyBlock=0;
        $keyCheck=true;
        $tt_locker=0;
        if(count($number_of_lockers)==0)
        {
             $response = [
            'status'=> 'validation_error',
            'msg'=> 'Block is required',
        ];
        return response()->json($response);
        }else{
            foreach ($number_of_lockers as $key => $block) {

                    if($block!=null)
                    {
                        $keyCheck=false;
                         $tt_locker=$tt_locker+$block;
                    }
            }
        }
        if($tt_locker==0){
           $response = [
            'status'=> 'validation_error',
            'msg'=> 'You are not allowed to book zero locker',
        ];
        return response()->json($response);
        }

        if($keyCheck)
        {
            $response = [
            'status'=> 'validation_error',
            'msg'=> 'Block is required',
        ];
        return response()->json($response);    
        }
        $school_academic_year_id = $request->school_academic_year_id;
        $user_id = $request->user_id;
        $school_year_id = $request->school_year_id;
      
        $child_first_name = $request->first_name;
        $child_surname = $request->surname;
        $pupil_premium = 1;
        $confrimation_email = $request->confrimation_email;


        $schoolYear = SchoolYear::where('id', $school_year_id)->first();
        $splitName = explode('-', $schoolYear->title);

        // dd($schoolYear->from_date);
        $start = Carbon::parse($schoolYear->from_date)->startOfDay()->toDateTimeString();
        $end = Carbon::parse($schoolYear->to_date)->endOfDay()->toDateTimeString();
        $book_next_year =false;

        $sale_obj = new Sale();

        foreach ($number_of_lockers as $key => $block) {
            # code...
        if($block!=null){
        // first we need to check that all the fields have the correct values
              $Block_id_p=$block_id[$key];

              $available_lockers = $sale_obj->total_available_lockers_in_block($Block_id_p,$start,$end,$book_next_year);

              if($available_lockers['total_available']< $block)
              {

                if($available_lockers['total_available']!=0){
                $response = [
                    'status'=> 'bookLookerError',
                    'msg'   => 'Can only book '.$available_lockers['total_available'].' lockers ',
                    'key'   => $key,
                ];
                return response()->json($response); 
                return false;
            }else{
                    $response = [
                    'status'=> 'bookLookerError',
                    'msg'   => 'No block available ',
                    'key'   => $key,
                ];
                return response()->json($response); 
                return false;
              }
            }
        }
          // if($available_lockers['total_available']>)
        // end 
      }
      
      foreach ($number_of_lockers as $key => $block) {
            # code...
        if($block!=null){
              $Block_id_p=$block_id[$key];
              $available_lockers = $sale_obj->total_available_lockers_in_block($Block_id_p,$start,$end,$book_next_year);

        if($available_lockers['total_available'] >= $block){

            $sale_obj = new Sale();
            $available_lockers = $sale_obj->ids_of_total_available_lockers_in_block($Block_id_p,$start,$end,$book_next_year);
            // going to randomize the array
                 shuffle($available_lockers); // this function shuffle the index of the array.
          
            $sale_obj = new Sale();
            for($booking = 0; $booking < $block; $booking++ ) {

                $sale_obj->create_new_booking($company_id, $Block_id_p, $available_lockers[$booking], $user_id,$start,$end,$child_first_name,$child_surname,$school_year_id,$school_academic_year_id,$pupil_premium,$confrimation_email);
            }
            $status = 'success';
            $validation_msg = 'successfully added lockers';

        }
    }
}

        $response = [
            'status'=> $status,
            'msg'=> $validation_msg,
        ];
        return response()->json($response);
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_blocks(Request $request)
    {
//        dd($request->all());
        // 4th Jan 2021  start (Amir Ishaque)

          $schoolYear = SchoolYear::where('id', $request->school_year)->first();
        $splitName = explode('-', $schoolYear->title);

        $start = Carbon::parse($schoolYear->from_date)->startOfDay()->toDateTimeString();
        $end = Carbon::parse($schoolYear->to_date)->endOfDay()->toDateTimeString();
        $book_next_year =false;
        $sale_obj = new Sale();
       
        // 4th Jan 2021 (END)
        // put the group by in the below query
        // ->groupBy('block_id')
        $blocks =  BlockYears::with('block')->where('company_id',$request->company_id)->where('school_academic_year_id',$request->school_academic_year_id)->groupBy('block_id')->get();
        // 4th Jan 2021  start (Amir Ishaque)
        foreach ($blocks as $key => $value) {
            # code...

             $lockers = Locker::where('block_id',$value['block_id'])
                    ->where('active', 1)
                    ->where('display', 1)
                    ->get();
                    $totalLockers=$lockers->count();

             $available_lockers = $sale_obj->total_available_lockers_in_block($value['block_id'],$start,$end,false);
             $value['city']=$available_lockers; // we are placing the number of availabe blocks here
             $value['country']=$totalLockers; // we are placing the number of availabe blocks here

        }
        // 4th Jan 2021 (END)

        return response()->json($blocks);
    }
    public function get_franchise(Request $request)
    {

        $users =  User::where('company_id',$request->company_id)->get();
        // Fetch the years of the company Start

        $school_academic_year = [];
        $company_school_year = CompanySchoolYear::with('school_academic_year')->where('company_id', $request->company_id)->orderBy('id', 'asc')->get();

        // Fetch the years of the company END
        $franchise_users = FranchiseeCompany::with('franchisee.users')->where('company_id',$request->company_id)->get();

        foreach ($franchise_users as $franchise_user) {

            $users = $users->merge($franchise_user['franchisee']['users']);

        }
        $users = $users->unique();
        // 4th Jan 2021
        // previously we are only sending the users from here now i am trying to the years as well from here 
        // amir ishaque
        $data = array('users' =>$users , 'years'=>$company_school_year );
        return response()->json($data);
    }

    public function updateName(Request $request)
    {

        $sale =Sale::find($request->sale_id);
        $sale->child_first_name = $request->child_first_name;
        $sale->child_surname = $request->child_surname;
        $sale->save();

        return response()->json('successs');
    }
}
