<?php

namespace App\Http\Controllers\Backend\Company;

use App\FranchiseeCompany;
use Carbon\Carbon;
use App\LockerBlockImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use App\User;
use App\Document;
use App\Session;
use App\OfficeConverter;

class LockerBlockImagesController extends Controller
{
    public function index()
    {
        return view('backend.pages.company.lockerBlockImage.index');
    }

    public function show_all(Request $request)
    {
        $data = $this->query_data($request);
        return Datatables::of($data)
        //   ->editColumn('file_name', function($data) {
      
        //     return '<a target="_blank" href="/uploads/'.$data->file_name.'" class="image-click"> <img class="block-images" src="/uploads/'.$data->file_name.'"></a>';
        // })
            ->addColumn('image_type', function ($data) {
                if($data->image_type == 'I')
                    return 'Internal';
                else
                    return 'External';
            })
            ->addColumn('action', function ($data) {
                $buttons = '';
                $buttons = '<a href="javascript:void(0)" class="btn btn-primary btn-md adv_cust_mod_btn pull-left edit-document" id="open_block_modal"  data-id="' . $data->id . '"  data-tier="'.$data->tier.'" data-imageType="'.$data->image_type.'" data-image="'.$data->file_name.'" style="margin-right: 3px;">Edit</a>';
                return $buttons;
            })
           
            ->rawColumns(['file_name', 'action']) ->make(true);
    }

    public function query_data($request)
    {
        $query = LockerBlockImage::orderBy('tier')->get();
        return $query;
    }

    public function upload(Request $request)
    {
        // dd($request->all());
        $validatedData = $request->validate([
            'file_name' => ['required'],
            'image_type' => ['required'],
            'tier' => ['required']
        ]);

        if (isset($request->file_name)) {
            // $time = time();
            $file = $request->file('file_name');
            $name = $file->getClientOriginalName();
            // dd($name);
            $file_name = explode(".", $name);
            $attachments = $file_name[0] . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path('uploads');

        }
        LockerBlockImage::create([
            'file_name' => $attachments,
            'image_type' => $request->image_type,
            'tier' => $request->tier,
            'active' => '1',
            'deleted' => '0',
        ]);
        return response()->json($validatedData);
    }

    public function update(Request $request)
    {
        $LockerBlockImage = LockerBlockImage::find($request->id);
        $validatedData = $request->validate([
            'image_type' => ['required'],
            'tier' => ['required']
        ]);
        if (isset($request->file_name)) {
            $file = $request->file('file_name');
            $name = $file->getClientOriginalName();
            // dd($name);
            $file_name = explode(".", $name);
            $attachments = $file_name[0] . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path('uploads');
            //dd($attachments);
            $file->move($destinationPath, $attachments);
            $LockerBlockImage->file_name = $attachments;
        }

        $LockerBlockImage->image_type = $request->image_type;
        $LockerBlockImage->tier = $request->tier;
        $LockerBlockImage->save();

        return response()->json($validatedData);
    }

    public function editabledata(Request $request)
    {
        $LockerBlockImage = LockerBlockImage::find($request->id);
        return response()->json($LockerBlockImage);
    }
}
