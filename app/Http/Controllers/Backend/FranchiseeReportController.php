<?php

namespace App\Http\Controllers\Backend;

use App\FranchiseeCompany;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Franchisee_report;
use App\Franchisee;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;

class FranchiseeReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function franchisee_index()
    {

        $franchise_list = Franchisee::orderBy('name','ASC');

        $franchisee_report_obj =  new Franchisee_report();
        $franchise_list = $franchisee_report_obj->franchisee_dropdown_record_permissions($franchise_list);

        $franchise_list = $franchise_list->get();

        return view('backend.pages.reports.franchisee.franchiseereport',compact('franchise_list'));
    }
    public function getData(Request $request)
    {
        $query = $this->query_data($request);
        if($request->page == 0) {
            $data = $query->take(1000)->get();
        }else{
            $skip = $request->page * 1000;
            $data = $query->skip($skip)->take(1000)->get();
        }
        $data = [
            'data' => $data
        ];

        return response()->json($data);
    }
   
    public function franshisee_show(Request $request)
        {
            $company_chats = $this->query_data($request);

            return Datatables::of($company_chats)
            ->addColumn('month',function($company_chats){
                if($company_chats->month == '1')
                    return 'January';
                elseif($company_chats->month == '2')
                    return 'February';

                elseif($company_chats->month == '3')
                    return 'March';

                elseif($company_chats->month == '4')
                    return 'April';

                elseif($company_chats->month == '5')
                    return 'May';

                elseif($company_chats->month == '6')
                    return 'June';

                elseif($company_chats->month == '7')
                    return 'July';

                elseif($company_chats->month == '8')
                    return 'August';

                elseif($company_chats->month == '9')
                    return 'September';

                elseif($company_chats->month == '10')
                    return 'October';

                elseif($company_chats->month == '11')
                    return 'November';

                elseif($company_chats->month == '12')
                    return 'December';

                else
                    return '';
            })
             ->make(true);
        }

    public function query_data($request){

        $company_chats = Franchisee_report::join('franchisees','franchisees.id','=','franchisee_reports.franchisee_id');

        if (isset($request->franchisee_month) && !empty($request->franchisee_month)){
            $company_chats->where('franchisee_reports.month','LIKE',DB::raw("'%$request->franchisee_month%'"));
        };

         if (isset($request->franchisee_year) && !empty($request->franchisee_year)){
            $company_chats->where('franchisee_reports.year','LIKE',DB::raw("'%$request->franchisee_year%'"));
        };

        if (isset($request->franchisee_id) && !empty($request->franchisee_id)){
            $company_chats->where('franchisee_reports.franchisee_id',$request->franchisee_id);
        };

        $franchisee_report_obj =  new Franchisee_report();
        $company_chats = $franchisee_report_obj->franchisee_record_permissions($company_chats);


        $company_chats->select('franchisees.name as franchisee_name' ,'franchisee_reports.month' , 'franchisee_reports.year','franchisee_reports.new_customer','franchisee_reports.customer','franchisee_reports.new_lead','franchisee_reports.lead','franchisee_reports.new_prospective','franchisee_reports.prospective','franchisee_reports.total');
       // $company_chats->get();

        return  $company_chats;

    }

    public function franchiseeCount(Request $request)
    {
        
        $query = $this->query_data($request);
        $count =  $query->count();

        return response()->json($count);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}