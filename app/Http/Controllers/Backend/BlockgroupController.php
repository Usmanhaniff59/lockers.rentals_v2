<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BlockGroup;
use App\BlockGroupCode;
use Yajra\DataTables\DataTables;
use Session;
use App\Traits\UniqueBlockGroupCode;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
class BlockgroupController extends Controller
{
     use UniqueBlockGroupCode;
      public function __construct()
    {
        $this->middleware(['auth', 'isAdmin']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $bookingTypes = LocationGroup::all();
    	// dd('123');
    	// ->with('bookingTypes', $bookingTypes)
        return view('backend.pages.blockGroup.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

          $this->validate($request, [
            'name'=>'required|max:100|unique:block_groups',
            'order'=>'required|numeric|min:0',
        ]);
          if(isset($request->status))
          {
            $status=1;
            
          }else{
            $status=0;
          }
          $blockgroup =  new BlockGroup();
          $blockgroup->name=$request->name;
          $blockgroup->order_by=$request->order;
          $blockgroup->status=$status;
          $blockgroup->save();
          // dd($blockgroup->toArray());
          if(isset($request->status))
          {

            $this->CreateBlockGroupCode($blockgroup->id);
          }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function show_all(Request $request)
    {
     
        // $company_idd = Session::get('company_id_chats');
        // ::where('company_id', $company_idd)->where('active', 1)->orderBy('order_list', 'ASC')
         $blockgroup = BlockGroup::where('delete',0)->orderBy('order_by', 'asc')->get();
        
        return Datatables::of($blockgroup)
          ->addColumn('order', function ($blockgroup) {
               return $blockgroup->order_by;
            })
            ->addColumn('name', function ($blockgroup) {
               return $blockgroup->name;
            })

            ->addColumn('action', function ($blockgroup) {
                // $buttons = '';
                // $status = $company_blocks->lockers()->where('updated', '!=', 1)->exists();
            
                    if($blockgroup->status==1)
                    {
                        $check='checked';
                        $val=1;
                    }else{
                        $check='';
                        $val=0;

                    }
                    $buttons = '<a href = "javascript:void(0)" class="btn sort_id" data-id="'.$blockgroup->id.'" > <label class="switch">
  <input type="checkbox" class="toggle-checkbox" value="'.$val.'" '.$check.'  data-id="'.$blockgroup->id.'" id="toggle-'.$blockgroup->id.'">
  <span class="slider round"></span>
</label></a ><a href="javascript:void(0)" class="btn btn-danger edit-block" data-order="'.$blockgroup->order_by.'" data-name="'.$blockgroup->name.'" data-id="'.$blockgroup->id.'" >Edit</a> <a href="javascript:void(0)" class="btn btn-danger delete-block" id="delete-block-'.$blockgroup->id.'" data-name="'.$blockgroup->name.'" data-id="'.$blockgroup->id.'" >Delete</a>';
                
                return $buttons;
            })  ->setRowClass(function ($blockgroup) {
                
                $status = $blockgroup->status;
                if ($status == 1) {
                    return 'alert-success';
                } 
            })->make(true);
    }
     public function updateName(Request $request)
    {
           $this->validate($request, [
            'name'=>'required|max:100',
             'order'=>'required|numeric|min:0',
        ]);
        $id=$request->id;
        $name=$request->name;
        if(isset($request->status))
          {
            $status=1;
            
          }else{
            $status=0;
          }
        $blockgroup = BlockGroup::where('id', $id)->first();
                $blockgroup->name = $name;
                $blockgroup->order_by = $request->order;
                $blockgroup->status = $status;


                
                $blockgroup->save();
                if(isset($request->status))
                {
                       $blockGroups = BlockGroup::where('id', $id)->whereNotExists( function ($query) {
                        $query->select(DB::raw(1))
                                ->from('block_group_code')
                                ->whereRaw('block_group_code.block_group_id = block_groups.id')
                                ->where('block_group_code.start_date','>=',  date('Y-09-01 00:00:00'));
                      })->get();
                       if(!empty($blockGroups) && isset($blockGroups) && $blockGroups->count()!=0)
                       {
                     
                                // block group code added from here
                        $last=BlockGroupCode::orderBy('order', 'desc')->first();
                            if(!empty($last))
                            {
                                $order=$last->order;
                            }else{
                                $order=1;
                            }
                             $code_counter=1;
                             $next_year =Carbon::now()->addYear($code_counter - 1)->format('Y');
                                    $start = $next_year.'-09-01 00:00:00';
                                    $next_2_year =Carbon::now()->addYear($code_counter)->format('Y');
                                    $end = $next_2_year.'-08-31 00:00:00';
                            for($i=0;$i<=1000;$i++)
                            {
                                // $number = sprintf("%04d",$i);
                                $order=$order+1;
                               $locker_number =  $this->createUniqueGroupCode($blockgroup->id);

                                $block_group_code = new BlockGroupCode();
                                $block_group_code->order=$order;
                                $block_group_code->locker_number=$i;
                                $block_group_code->block_group_id =$blockgroup->id;
                                $block_group_code->start_date = $start;
                                $block_group_code->end_date =$end;
                                $block_group_code->code =$locker_number;

                                $block_group_code->save();


                                // add the report from here 

                                // $lockerReport = new LockerNumberReport();
                                // $lockerReport->order=$order;
                                // $lockerReport->locker_number=$locker_number;
                                // $lockerReport->block_group_id=$blockgroup->id;
                                // $block_group_code->start_date = $start;
                                // $block_group_code->end_date =$end;
                                // $lockerReport->save();


                            }
                       }
                }
                // $blockgroup->save();
                Session::flash('flash_message','Block updated successfully.');
               return redirect()->route('blockgroup.index');
    }
     public function updateStatus(Request $request)
    {
          
        $id=$request->id;
        $status=$request->status;
        $blockgroup = BlockGroup::where('id', $id)->first();
                $blockgroup->status = $status;

                
                $blockgroup->save();

                  if($status==1)
                {
                       $blockGroups = BlockGroup::where('id', $id)->whereNotExists( function ($query) {
                        $query->select(DB::raw(1))
                                ->from('block_group_code')
                                ->whereRaw('block_group_code.block_group_id = block_groups.id')
                                ->where('block_group_code.start_date','>=',  date('Y-09-01 00:00:00'));
                      })->get();
                       if(!empty($blockGroups) && isset($blockGroups) && $blockGroups->count()!=0)
                       {
                     
                                // block group code added from here
                        $last=BlockGroupCode::orderBy('order', 'desc')->first();
                            if(!empty($last))
                            {
                                $order=$last->order;
                            }else{
                                $order=1;
                            }
                             $code_counter=1;
                             $next_year =Carbon::now()->addYear($code_counter - 1)->format('Y');
                                    $start = $next_year.'-09-01 00:00:00';
                                    $next_2_year =Carbon::now()->addYear($code_counter)->format('Y');
                                    $end = $next_2_year.'-08-31 00:00:00';
                            for($i=1;$i<=1000;$i++)
                            {
                                // $number = sprintf("%04d",$i);
                                $order=$order+1;
                               $locker_number =  $this->createUniqueGroupCode($blockgroup->id);

                                $block_group_code = new BlockGroupCode();
                                $block_group_code->order=$order;
                                $block_group_code->locker_number=$i;
                                $block_group_code->block_group_id =$blockgroup->id;
                                $block_group_code->start_date = $start;
                                $block_group_code->end_date =$end;
                                $block_group_code->code =$locker_number;

                                $block_group_code->save();


                                // add the report from here 

                                // $lockerReport = new LockerNumberReport();
                                // $lockerReport->order=$order;
                                // $lockerReport->locker_number=$locker_number;
                                // $lockerReport->block_group_id=$blockgroup->id;
                                // $block_group_code->start_date = $start;
                                // $block_group_code->end_date =$end;
                                // $lockerReport->save();


                            }
                       }
                }
                return 1;
                // $blockgroup->save();
               //  Session::flash('flash_message','Block updated successfully.');
               // return redirect()->route('blockgroup.index');
    }
    public function deleteBlock(Request $request)
    {
        $id=$request->id;
        $blockgroup = BlockGroup::where('id', $id)->first();
        if(!empty($blockgroup)){
          $blockgroup->delete=1;
          $blockgroup->save();
        // $res=BlockGroup::where('id',$id)->delete();
        // $blockgroup->blockgroupcode()->delete();
        return 1;
    }else{
        return 2;
        # code...
    }
    }
    public function updateOrder(Request $request)
    {
      $list=$request->order;
     
      foreach ($list as $key => $item) {
      if(isset($item['id'])){
         $blockgroup = BlockGroup::where('id', $item['id'])->first();
                $blockgroup->order_by = $item['position'];
                
                $blockgroup->save();
      }
      }
      return 1;
    }

//      ->setRowClass(function ($company_blocks) {
                // dd($company->active);
            //     $active = $company_blocks->active;
            //     $display = $company_blocks->display;
            //     //dd($active, $display);
            //     if ($display == 1) {
            //         return 'alert-success';
            //     } else if ($active == 0) {
            //         return 'alert-danger';
            //     } else if ($display == 0) {
            //         return 'alert-warning';
            //     }
            // })

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//        $bookingType = LocationGroup::findOrFail($id);
//
//        return view('backend.pages.bookingTypes.edit', compact('bookingType'));
    }

}
