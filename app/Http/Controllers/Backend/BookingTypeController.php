<?php

namespace App\Http\Controllers\Backend;

use App\BookingType;
use App\LocationGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BookingTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'isAdmin']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bookingTypes = LocationGroup::all();

        return view('backend.pages.bookingTypes.index')->with('bookingTypes', $bookingTypes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//        $bookingType = LocationGroup::findOrFail($id);
//
//        return view('backend.pages.bookingTypes.edit', compact('bookingType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd($request->all());
        $id = $request['id'];
        $bookingType = LocationGroup::findOrFail($id);
        $this->validate($request, [
            'name'=>'required|max:120'
        ]);

        $input = $request->only(['name']);

        $bookingType->fill($input)->save();

        return redirect()->route('bookingtype.index')
            ->with('flash_message',
                'Booking type successfully edited.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
