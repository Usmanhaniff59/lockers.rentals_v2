<?php
 
namespace App\Http\Controllers\Backend;

use App\Block;
use App\Company;
use App\Exports\reports\BookedLocker;
use App\FranchiseeCompany;
use App\Locker;
use App\Rate;
use App\Maintenance;
use App\Sale;
use App\Payment;
use App\SchoolAcademicYear;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;
use App\LocationGroup;
use App\CompanySchoolYear;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;


class ReportBooked extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $locationGroups = LocationGroup::get();
       $rates = Rate::orderBy('order','ASC')->get();
       $school_academic_year = SchoolAcademicYear::orderBy('order_number', 'asc')->get();


        return view('backend.pages.reports.booked.index',compact('locationGroups','rates', 'school_academic_year'));
    }
    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */

     public function show_all(Request $request){

         $sale = $this->booked_query($request);

         return  $this->booked_dataTable($sale);


    }

    public function booked_query($request){


        $sale = Sale::query();
        $sale->where('sales.status', 'b');

        $search_input = $request->search_input;

        $search_date=date('Y-m-d',strtotime($request->search_date));

        $sale->leftJoin('users','users.id','=','sales.user_id')
            ->leftJoin('companies','companies.id','=','sales.company_id')
            ->leftJoin('blocks','blocks.id','=','sales.block_id')
            ->leftJoin('lockers','lockers.id','=','sales.locker_id');
               // if(auth()->user()->hasRole('Franchisee'))
               //  {
               //      $franchisee_ids=DB::table('franchisee_user')->where('user_id',auth()->user()->id)->pluck('franchisee_id');
               //      $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id', $franchisee_ids)->pluck('company_id')->toArray();
               //      $sale->whereIn('companies.id', $franchisee_companies);
               //  }else if(auth()->user()->hasRole('Company'))
               //  {
               //       $sale->where('companies.id',auth()->user()->company_id);
               //  }
        if (isset($request->booking_id) && !empty($request->booking_id)){
            $sale->where('sales.booking_id',$request->booking_id);
        }
        if (isset($request->search_company) && !empty($request->search_company)){
            $sale->where('companies.name','LIKE',DB::raw("'%$request->search_company%'"));
        }
        if (isset($request->search_block) && !empty($request->search_block)){
            $sale->where('blocks.name','LIKE',DB::raw("'%$request->search_block%'"));
        }

        if (isset($request->search_name) && !empty($request->search_name)){
            $sale->where('sales.child_first_name','LIKE',DB::raw("'%$request->search_name%'"));
        }
        if (isset($request->search_surname) && !empty($request->search_surname)){
            $sale->where('sales.child_surname','LIKE',DB::raw("'%$request->search_surname%'"));
        }
        if (isset($request->search_lockernum) && !empty($request->search_lockernum)){
            $sale->where('lockers.locker_number','LIKE',DB::raw("'%$request->search_lockernum%'"));
        }
        if (!empty($request->booking_id) || !empty($request->search_company)|| !empty($request->search_block)|| !empty($request->search_name)|| !empty($request->search_surname)|| !empty($request->search_lockernum)) {

            if (isset($request->search_date)) {
                $sale->where(function ($query) use ($search_date) {
                    $query->whereDate('sales.start', '<=', $search_date)
                        ->whereDate('sales.end', '>=', $search_date);
                });
            }

        }else if (isset($request->search_date)){
            $sale->where(function ($query) use ($search_date) {
                $query->whereDate('sales.start', '<=', $search_date)
                    ->whereDate('sales.end', '>=', $search_date);
            });

        }

        else if(date("m") < 7  ){
            $search_date = Carbon::now()->toDateString();
            $sale->whereDate('sales.end','>=' ,$search_date)
                ->whereDate('sales.start','<=' ,$search_date);
        } else {

            $search_date = date("Y-09-01");
            $sale->whereDate('sales.end','>=' ,$search_date);
            $sale->whereDate('sales.start','<=' ,$search_date);

        }
        $locker_obj =  new Locker();
        $sale = $locker_obj->locker_available_and_booked_report_record_permissions($sale);



        $sale->select('sales.booking_id','companies.name as company_name','blocks.name as block' ,'sales.number as locker_number','sales.status','sales.confirmation_sent' ,'sales.child_first_name' ,'sales.child_surname' ,'sales.net_pass',DB::raw('DATE_FORMAT(sales.start, "%M %D, %Y %H:%m") as start') ,DB::raw('DATE_FORMAT(sales.end, "%M %D, %Y %H:%m") as end')  );
        return  $sale;

    }

    public function booked_dataTable($sale){


        return Datatables::of($sale)

//            ->addColumn('company_name', function( $sale) {
//                //   dd($sale);
//                $company = Company::where('id', $sale->company_id)->first();
//
//                if(isset($company->name)){
//                    return $company->name;
//
//                } else {
//                    return "";
//                }
//
//
//            })
//            ->addColumn('block', function( $sale) {
//                $block = Block::where('id', $sale->block_id)->first();
//                if(isset($block->name)){
//                    return $block->name;
//                } else {
//                    return "";
//                }
//
//            })
//            ->addColumn('start', function( $sale) {
//                return date("F jS, Y H:m", strtotime($sale->start));
//            })
//            ->addColumn('end', function( $sale) {
//                return date("F jS, Y H:m", strtotime($sale->end));
//            })
//            ->addColumn('net_pass', function( $sale) {
//                if(empty($sale->net_pass)){
//                    $code =  "";
//                } else {
//
//                    $code =  substr(sprintf("%04d",$sale->net_pass),  -4);
//                }
//                return $code;
//            })
            ->addColumn('action', function ($sale) {
                $buttons = '';

                if(isset($sale->user_id)) {
//                    if($sale->status == "b")
//                        $buttons .= '<a href="#viewuser" class="btn btn-info pull-left button-style view-user" data-toggle="modal" style="margin-right: 3px;" data-href="#edit" data-id="' . $sale->user_id . '" >View User</a>';
                }

                if(auth()->user()->can('Resend Email')) {
//                    if ($sale->confirmation_sent == 1)
//                        if($sale->status == "b")
//                            $buttons .= '<a href="#." id="resend' . $sale->booking_id . '" class="btn btn-success pull-left button-style " onclick="resend_confirm(\'' . $sale->booking_id . '\');row_highlight(\'resend' . $sale->booking_id . '\')" >Resend Email</a>';
                }

                return $buttons;
            })
            ->make(true);

    }

    public function booked_excel(Request $request)
    {
//        dd($request->all());
        $query = $this->booked_query($request);
        $data =  $query->get();
//        dd($data);
//        if($request->excel_array == 1)
//        {
//
//        }
//        else
//        {
//            $array=json_decode($request->excel_array);
//            $data= LocationBreakdown::whereIn('id',$array)->where('type','e')->with('location','equipment')->orderBy('id', 'desc')->get();
//        }
        return Excel::download(new BookedLocker($data), 'bookedReport.csv');
    }

    public function getbookedreportcount(Request $request)
    {
        $query = $this->booked_query($request);
        $count =  $query->count();

        return response()->json($count);
    }
    public function getbookedreport(Request $request)
    {


        $query = $this->booked_query($request);
        $query->orderBy('lockers.locker_number','ASC');
        if($request->page == 0) {
            $data = $query->take(1000)->get();
        }else{
            $skip = $request->page * 1000;
            $data = $query->skip($skip)->take(1000)->get();
        }

        $data = [
            'data' => $data
        ];

        return response()->json($data);
    }


}
