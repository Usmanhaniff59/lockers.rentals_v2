<?php

namespace App\Http\Controllers\Backend;

use App\Label;
use App\Language;
use App\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SlidersController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'isAdmin']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::all();
        return view('backend.pages.sliders.index',compact('sliders'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $validator = $this->validate($request, [
            'title'=>'required',
            'button_text'=>'required',
            'button_link'=>'required',
            'img_url' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);


        if ($request->has('img_url'))
        {
            $destinationPath = 'backend/img/sliders';
            $imageName = time().'.'.request()->img_url->getClientOriginalExtension();
            $moved = request()->img_url->move(public_path($destinationPath), $imageName);

        }
        $title = str_replace(' ', '-', strtolower($request['title']));
        $btnText = str_replace(' ', '-', strtolower($request['button_text']));

        $slider = new Slider();
        $slider->title = $title;
        $slider->button_text = $btnText;
        $slider->button_link = $request['button_link'];
        if($request['open_in_new_tab'] == 'on') {
            $slider->open_in_new_tab = 1;
        }
        $slider->img_url = $imageName;
        $slider->save();

//        $languages = Label::distinct('lang')->pluck('lang');
        $languages = Language::where('status',1)->pluck('locale');

        foreach ($languages as $language){
            $label = new Label();
            $label->key = $title;
            $label->lang = $language;
            $label->value = $title;
            $label->scope = 'global';
            $label->save();

        }
        foreach ($languages as $language){

            $label = new Label();
            $label->key = $btnText;
            $label->lang = $language;
            $label->value = $btnText;
            $label->scope = 'global';
            $label->save();

        }

        return redirect()->route('sliders.index')
            ->with('flash_message',
                'slider successfully added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd('hi');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        dd($id);
//        $slider = Slider::findOrFail($id);
//
//        return view ('backend.pages.sliders.edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

//        dd($request->all());
        $id = $request['slider_id'];

        $validator = $this->validate($request, [
            'title'=>'required',
            'button_link'=>'required',
            'button_link'=>'required',
            'img_url' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);

        if ($request->has('img_url'))
        {
            $destinationPath = 'backend/img/sliders';
            $imageName = time().'.'.request()->img_url->getClientOriginalExtension();
            $moved = request()->img_url->move(public_path($destinationPath), $imageName);

        }

        $title = str_replace(' ', '-', strtolower($request['title']));
        $btnText = str_replace(' ', '-', strtolower($request['button_text']));

        $slider = Slider::findOrFail($id);
        $slider->title = $title;
        $slider->button_text = $btnText;
        $slider->button_link = $request['button_link'];
        if($request['open_in_new_tab'] == 'on') {
            $slider->open_in_new_tab = 1;
        }else{
            $slider->open_in_new_tab = 0;
        }
        if ($request->has('img_url')) {
            $slider->img_url = $imageName;
        }
        $slider->save();

        $languages = Label::distinct('lang')->pluck('lang');

        foreach ($languages as $language){
            $label = Label::where('key',$request['oldTitle'])->where('lang' ,$language)->first();
            $label->key = $title;
            $label->save();

            $label = Label::where('key',$request['oldbutton_text'])->where('lang' ,$language)->first();
            $label->key = $btnText;
            $label->save();

        }

        return redirect()->route('sliders.index')
            ->with('flash_message',
                'slider successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $slider = Slider::findOrFail($id);
        $slider->delete();

        $labels = Label::where('key',$request['title'])->get();
        foreach ($labels as $label){
            $label = Label::where('key',$request['title'])->first();
            $label->delete();
        }

        $labels = Label::where('key',$request['button_text'])->get();
        foreach ($labels as $label){
            $label = Label::where('key',$request['button_text'])->first();
            $label->delete();
        }

        return redirect()->route('sliders.index')
            ->with('flash_message',
                'slider successfully deleted.');
    }
}
