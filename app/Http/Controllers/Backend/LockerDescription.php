<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LockerColor;
use Yajra\DataTables\DataTables;
use Session;
class LockerDescription extends Controller
{
       public function __construct()
    {
        $this->middleware(['auth', 'isAdmin']);
    }
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
    {
        // $bookingTypes = LocationGroup::all();
    	// dd('123');
    	// ->with('bookingTypes', $bookingTypes)
        return view('backend.pages.lockerDescription.index');
    }
       public function store(Request $request)
    {
        //
          $this->validate($request, [
            'color'=>'required|max:100'
        ]);
          $color=LockerColor::where('color', $request->color)->where('delete',0)->first();
          if(!isset($color)){
          $blockgroup =  new LockerColor();
          $blockgroup->color=$request->color;
          $blockgroup->save();
           Session::flash('flash_message','Locker color added successfully.');
        return back();
      }else{
        Session::flash('flash_message','Locker color has been already added.');
               return redirect()->route('locker_description.index');
      }
    }
    public function show_all(Request $request)
    {
     
        // $company_idd = Session::get('company_id_chats');
        // ::where('company_id', $company_idd)->where('active', 1)->orderBy('order_list', 'ASC')
         // $blockgroup = LockerColor::orderBy('color', 'asc')->get();
         $blockgroup = LockerColor::where('delete', 0)->orderBy('color', 'asc')->get();
        return Datatables::of($blockgroup)
            ->addColumn('name', function ($blockgroup) {
               return $blockgroup->color;
            })

            ->addColumn('action', function ($blockgroup) {
                // $buttons = '';
                // $status = $company_blocks->lockers()->where('updated', '!=', 1)->exists();
                  if($blockgroup->active==1)
                    {
                        $check='checked';
                        $val=1;
                    }else{
                        $check='';
                        $val=0;

                    }
                     
                    $buttons = '<a href = "javascript:void(0)" class="btn sort_id" data-id="'.$blockgroup->id.'" > <label class="switch">
  <input type="checkbox" class="toggle-checkbox" value="'.$val.'" '.$check.'  data-id="'.$blockgroup->id.'" id="toggle-'.$blockgroup->id.'">
  <span class="slider round"></span>
</label></a ><a href="javascript:void(0)" class="btn btn-danger edit-block" data-name="'.$blockgroup->color.'" data-id="'.$blockgroup->id.'" >Edit</a> <a href="javascript:void(0)" class="btn btn-danger delete-block" data-table="locker_color" id="delete-block-'.$blockgroup->id.'" data-name="'.$blockgroup->color.'" data-id="'.$blockgroup->id.'" >Delete</a>';
                
                return $buttons;
            })  ->setRowClass(function ($blockgroup) {
                
                $status = $blockgroup->status;
                if ($status == 1) {
                    return 'alert-success';
                } 
            })->make(true);
    }
      public function deleteBlock(Request $request)
    {
        $id=$request->id;
        $blockgroup = LockerColor::where('id', $id)->first();
        if(!empty($blockgroup)){
        // $res=LockerColor::where('id',$id)->delete();
          $blockgroup->delete=1;
           $blockgroup->save();
        return 1;
    }else{
        return 2;
        # code...
    }
    }
     public function updateStatus(Request $request)
    {
           $this->validate($request, [
            'color'=>'required|max:100'
        ]);
  $color=LockerColor::where('color', $request->color)->where('delete',0)->where('id','!=',$request->id)->first();
if(!isset($color)){
        $id=$request->id;
        $name=$request->color;
        $blockgroup = LockerColor::where('id', $id)->first();
                $blockgroup->color = $name;
                
                $blockgroup->save();
                // $blockgroup->save();
                Session::flash('flash_message','Locker color updated successfully.');
               return redirect()->route('locker_description.index');
             }else{
                   Session::flash('flash_message','Locker color has been already added.');
               return redirect()->route('locker_description.index');
               }
    }
     public function updateActive(Request $request)
    {

        $id=$request->id;
        $status=$request->status;
        $blockgroup = LockerColor::where('id', $id)->first();
                $blockgroup->active = $status;
                
                $blockgroup->save();
                // $blockgroup->save();
                Session::flash('flash_message','Locker color updated successfully.');
               return redirect()->route('locker_description.index');
    }
}
