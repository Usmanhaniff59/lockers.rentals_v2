<?php

namespace App\Http\Controllers\Backend;

use App\DistributionListGroup;
use App\DistributionListGroupMember;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DistributionListGroupMemberController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $this->validate($request, [
            'name'=>'required|max:120'

        ]);
        $distributionList = DistributionListGroup::findOrFail($request['distribution_list_group_id']);
        $input = $request->only(['name']);
        $distributionList->fill($input)->save();

        if($request['users']){
                foreach ($request['users'] as $user_id){
                    $dist_members = new DistributionListGroupMember();
                    $dist_members->distribution_list_group_id = $request['distribution_list_group_id'];
                    $dist_members->user_id = $user_id;
                    $dist_members->save();
                }
        }

        return redirect()->route('distributionlists.index')
            ->with('flash_message',
                'Distribution list Group successfully edited.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
