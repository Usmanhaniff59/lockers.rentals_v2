<?php

namespace App\Http\Controllers\Backend;
use App\Email;
use App\EmailFolder;
use App\Http\Controllers\Controller;
use App\Jobs\SendEmailJob;
use App\Pages;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;



class EmailManagementController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'clearance'])
            ->except('index', 'show');
    }

    //Email Inbox
    public function inbox(){

        $mails = Email::where('action','inbox')->where('deleted','0')->where('email_folder_id','0')->orderby('created_at','DESC')->paginate(15);
        $email_page = 'Inbox';
        $folders = EmailFolder::all();
        $foldersemails = Email::where('deleted','0')->orderby('created_at','DESC')->get();
        $users = User::all();
        return view('backend.pages.emailManagement.inbox', compact('email_page','mails','folders','foldersemails','users'));
    }

    //Email Compose
    public function compose(){

        $email_page = 'Compose';
        $users = User::all();
        return view('backend.pages.emailManagement.compose', compact('email_page','users'));
    }

    //Email View
    public function view($id){
        $mail = Email::where('id', $id)->first();
        $email_page = 'View Email';
        $folders = EmailFolder::all();
        $users = User::all();
        return view('backend.pages.emailManagement.view', compact('email_page','mail','folders','users'));
    }

    //Email Sent
    public function sent(){

        $mails = Email::where('action','sent')->where('send','1')->where('deleted','0')->get();
        $email_page = 'Sent';
        $users = User::all();
        return view('backend.pages.emailManagement.sent', compact('email_page','mails','users'));
    }
    public function emailsentstar(Request $request){
        $mails = Email::where('id',$request['id'])->update(['star' => $request['star']]);
        return response()->json($mails);
    }

    //Email Draft
    public function draft(){
        $mails = Email::where('action','sent')->where('send','0')->get();
        $email_page = 'Draft';
        $users = User::all();
        return view('backend.pages.emailManagement.draft', compact('email_page','mails','users'));
    }

    //Email Draft
    public function trash(){
        $mails = Email::where('deleted','1')->get();
        $email_page = 'Trash';
        $users = User::all();
        return view('backend.pages.emailManagement.trash', compact('email_page','mails','users'));
    }

    //Email Draft
    public function spam(){
        $email_page = 'Spam';
        $users = User::all();
        return view('backend.pages.emailManagement.spam', compact('email_page','users'));
    }
    public function email_delete($id){
        $send_email = Email::where('id',$id)->first();
        $send_email->deleted = '1';
        $send_email->save();
        if($send_email->action == 'sent'){
            return redirect()->route('emailsent')
                ->with('flash_message', 'Email Deleted');
        }
        else{
            return redirect()->route('emailinbox')
                ->with('flash_message', 'Email Deleted');
        }

    }
    public function email_trash($id){
        $send_email = Email::where('id',$id)->first();
        $send_email->deleted = '2';
        $send_email->save();
        return redirect()->route('emailtrash')
            ->with('flash_message', 'Email Deleted Permanently');

    }
    public function move($email_id,$folder_id){
//        dd($email_id);
        $send_email = Email::where('id',$email_id)->first();
        $send_email->email_folder_id = $folder_id;
        $send_email->save();
        return redirect()->route('emailinbox')
            ->with('flash_message', 'Email Moved');

    }
    public function send_email(Request $request)
    {
        $to = $request['email_to'];
        $subject = $request['email_subject'];
        $email_html = $request['email_html'];

        $email = new Email();
        $email->email = $to;
        $email->action = 'sent';
        $email->subject = $subject;
        $email->mail_body = $email_html;
        $email->date_added = Carbon::now();
        $email->type = 'email';
        $email->result_code = '0';
        $email->star = '0';
        $email->send = '0';
        $email->complete = '0';
        $email->deleted = '0';
        $email->save();

        $details['email'] = $to;
//        $email = new SendEmailJob($details,$email_html, $subject, $email->id);
//        dispatch($email);

        return redirect()->route('emailsent')
            ->with('flash_message', 'Email Sent');
    }

}