<?php

namespace App\Http\Controllers;

use App\BlockImagesController;
use Illuminate\Http\Request;

class BlockImagesControllerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BlockImagesController  $blockImagesController
     * @return \Illuminate\Http\Response
     */
    public function show(BlockImagesController $blockImagesController)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BlockImagesController  $blockImagesController
     * @return \Illuminate\Http\Response
     */
    public function edit(BlockImagesController $blockImagesController)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BlockImagesController  $blockImagesController
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BlockImagesController $blockImagesController)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BlockImagesController  $blockImagesController
     * @return \Illuminate\Http\Response
     */
    public function destroy(BlockImagesController $blockImagesController)
    {
        //
    }
}
