<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePupilPremium extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'school_year_id' => 'school year',
            'school_academic_year_id' => 'school academic year',
            'block_id' => 'block',
            'user_id' => 'assign to',

        ];
    }
    public function rules()
    {
        return [
                'company_id' => 'required',
                'school_year_id' => 'required',
                'school_academic_year_id' => 'required',
                'block_id' => 'required|array|min:1',
                'user_id' => 'required',
                'number_of_lockers' => 'required|array|min:1',
                'first_name' => 'required',
                'surname' => 'required',

        ];
    }
}
