<?php

namespace App\Traits;

use App\ManualLockerCode;
use Illuminate\Http\Request;

trait UniqueManualCode {

    /**
     * @param Request $request
     * @return $this|false|string
     */


    public function createUniqueManualCode($block_id) {
        $number = sprintf("%04d",rand (0,9999));
//        $number = 6882;


        // call the same function if the barcode exists already
        if ($this->barcodeNumberExists($number,$block_id)) {

            return $this->createUniqueManualCode($block_id);
        }

        // otherwise, it's valid and can be used
        return $number;
    }

    public function barcodeNumberExists($number,$block_id) {
        // query the database and return a boolean
        $exists= ManualLockerCode::where('block_id', $block_id)->where('code', $number)->exists();
        return $exists;
    }

}