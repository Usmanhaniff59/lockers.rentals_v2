<?php

namespace App\Traits;

use App\ManualLockerCode;
use App\VoucherCode;
use Illuminate\Http\Request;

trait UniqueVoucherCode {

    /**
     * @param Request $request
     * @return $this|false|string
     */


    public function createUniqueCode() {
        $number = sprintf("%04d",rand (0,9999));
//        $number = 6882;


        // call the same function if the barcode exists already
        if ($this->barcodeNumberExists($number)) {

            return $this->createUniqueCode();
        }

        // otherwise, it's valid and can be used
        return $number;
    }

    public function barcodeNumberExists($number) {
        // query the database and return a boolean
        $exists= VoucherCode::where('code', $number)->exists();
        return $exists;
    }

}