<?php

namespace App\Traits;

use App\BlockGroupCode;
use App\SchoolYear;

use Illuminate\Http\Request;
use Carbon\Carbon;
trait UniqueBlockGroupCode {

    /**
     * @param Request $request
     * @return $this|false|string
     */


    public function createUniqueGroupCode($block_id,$start,$end) {
        $number = sprintf("%04d",rand (0,9999));
//        $number = 6882;


        // call the same function if the barcode exists already
        if ($this->barcodeNumberExists($number,$block_id,$start,$end)) {

            return $this->createUniqueGroupCode($block_id,$start,$end);
        }

        // otherwise, it's valid and can be used
        return $number;
    }

    public function barcodeNumberExists($number,$block_id,$start,$end) {
        // query the database and return a boolean
        $first = date($start);
        $last  = date($end);
        $exists= BlockGroupCode::where('block_group_id', $block_id)->where('locker_number', $number)->where('start_date',$first)->where('end_date',$last)->exists();
        return $exists;
    }

    public function CreateBlockGroupCode($id,$year)
    {
         $last=BlockGroupCode::orderBy('order', 'desc')->first();
            if(!empty($last))
            {
                $order=$last->order;
            }else{
                $order=1;
            }
             $code_counter=1;
            $oneYear = SchoolYear::where([['from_date',$year->from_date]])->groupBy('from_date')->first();

             // $next_year =Carbon::now()->addYear($code_counter - 1)->format('Y');
             //        $start = $next_year.'-09-01 00:00:00';
             //        $next_2_year =Carbon::now()->addYear($code_counter)->format('Y');
             //        $end = $next_2_year.'-08-31 00:00:00';
            // dd($years->toArray());
            // foreach ($years as $key => $oneYear) {
                # code...
          
            for($i=0;$i<1000;$i++)
            {
                // dd($oneYear);
                // $number = sprintf("%04d",$i);
                $order=$order+1;
               $locker_number =  $this->createUniqueGroupCode($id,$oneYear->from_date,$oneYear->to_date);

                $block_group_code = new BlockGroupCode();
                $block_group_code->order=$order;
                $block_group_code->locker_number=$i;
                $block_group_code->block_group_id =$id;
                $block_group_code->start_date = $oneYear->from_date;
                $block_group_code->end_date =$oneYear->to_date;
                $block_group_code->code =$locker_number;
                $block_group_code->save();

            }
        // }
            return true;
    }

}