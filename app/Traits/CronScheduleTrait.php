<?php

namespace App\Traits;

use App\CronSchedule;
use App\ManualLockerCode;
use Carbon\Carbon;
use Illuminate\Http\Request;

trait CronScheduleTrait {

    /**
     * @param Request $request
     * @return $this|false|string
     */


    public function addCronSchedule($class , $controller ) {

        $booking_cron = CronSchedule::where('class', $class)->where('controller', $controller)->where('status', 'Pending')->first();

        if (empty($booking_cron)) {
            $cron = new CronSchedule();
            $cron->class = $class;
            $cron->controller = $controller;
            $cron->where_check = '0';
            $cron->due = Carbon::now();
            $cron->completed = '0';
            $cron->status = 'Pending';
            $cron->save();
        }

    }


}