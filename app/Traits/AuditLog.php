<?php

namespace App\Traits;

use Illuminate\Http\Request;
use App\AuditLog;
trait AuditLog {

    /**
     * @param Request $request
     * @return $this|false|string
     */

    public function createAuditLog($userId,$tableName,$tableId,$field,$oldVal='',$updated_val='',$action)
    {
        $audit = new AuditLog();
        $audit->user_id=$userId;
        $audit->table_name=$tableName;
        $audit->table_id=$tableId;
        $audit->field=$field;
        $audit->old_val=$oldVal;
        $audit->updated_val=$updated_val;
        $audit->action=$action;
        $audit->save();
    }



}