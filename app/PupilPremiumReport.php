<?php

namespace App;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class PupilPremiumReport extends Model
{
    protected $fillable = [ 'booking_id','company', 'name', 'block', 'locker_number', 'start', 'end'];
    
    public function getStartAttribute($value)
		{
	     return Carbon::parse($value)->format('d M, Y H:i');
		}
	public function getEndAttribute($value)
		{
	     return Carbon::parse($value)->format('d M, Y H:i');
		}

		public function pupil_premium_record_permissions($query){


            $user = Auth::user();
            $franchisees = $user->franchisee()->pluck('franchisee_id')->toArray();

            if(auth()->user()->can('All Records')) {


            }elseif( Auth::user()->company_id) {

                $franchisee_companies = FranchiseeCompany::where('franchisee_id',Auth::user()->franchisee_id)->pluck('company_id')->toArray();

                $query->Where(function ($query) use ($franchisee_companies) {
                    $query->WhereIn('sales.company_id',$franchisee_companies)
                        ->orWhere('sales.company_id',Auth::user()->company_id);
                });
            }
            elseif( Auth::user()->franchisee_id ) {
                $franchisee_companies = FranchiseeCompany::where('franchisee_id',Auth::user()->franchisee_id)->pluck('company_id')->toArray();

                $query->whereIn('sales.company_id',$franchisee_companies);
            }
            elseif( Auth::user()->company_id ){

                $query->where('sales.company_id',Auth::user()->company_id);

            }else{
                $query->where('sales.company_id',0);
            }
            return $query;
        }
}
