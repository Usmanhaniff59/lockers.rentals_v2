<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlockGroup extends Model
{
    protected $table = 'block_groups';

     public function blockgroupcode()
    {
    	//  return $this->belongsTo('App\BlockGroup','block_group','id');
        return $this->belongsTo('App\BlockGroupCode','id','block_group_id');

    }
     public function block()
    {
    	//  return $this->belongsTo('App\BlockGroup','block_group','id');
        return $this->belongsTo('App\Block','id','block_group');

    }

     public function lockers()
    {
    	//  return $this->belongsTo('App\BlockGroup','block_group','id');
        return $this->belongsTo('App\Locker','id','block_group');

    }

}
