<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class SalesAudit extends Model
{
   public  function sales_audit_log($sale_id  , $type = 'add'){

       $sale = Sale::find($sale_id);

       $sale_audit = new SalesAudit();
       $sale_audit->sale_id = $sale->id;
       $sale_audit->user_id = $sale->user_id;
       if(isset(Auth::user()->id))
            $sale_audit->updated_by = Auth::user()->id;

       $sale_audit->booking_id = $sale->booking_id;
       $sale_audit->number = $sale->number;
       $sale_audit->net_pass = $sale->net_pass;
       $sale_audit->company_id = $sale->company_id;
       $sale_audit->locker_id = $sale->locker_id;
       $sale_audit->block_id = $sale->block_id;
       $sale_audit->block_name = $sale->block_name;
       $sale_audit->start = $sale->start;
       $sale_audit->end = $sale->end;
       $sale_audit->price = $sale->price;
       $sale_audit->email = $sale->email;
       $sale_audit->status = $sale->status;
       $sale_audit->street = $sale->street;
       $sale_audit->city = $sale->city;
       $sale_audit->post_code = $sale->post_code;
       $sale_audit->country = $sale->country;
       $sale_audit->child_first_name = $sale->child_first_name;
       $sale_audit->child_surname = $sale->child_surname;
       $sale_audit->child_email = $sale->child_email;
       $sale_audit->school_year_id = $sale->school_year_id;
       $sale_audit->school_academic_year_id = $sale->school_academic_year_id;
       $sale_audit->hold_sales_id = $sale->hold_sales_id;
       $sale_audit->type = $type;
       $sale_audit->hold_locker = $sale->hold_locker;
       $sale_audit->save();

   }
   public  function update_audit_log($sale_id ){

       $sale = Sale::find($sale_id);
       $sale_audit = SalesAudit::where('sale_id',$sale_id)->orderBy('id','DESC')->first();

       $sale_audit->sale_id = $sale->id;
       $sale_audit->user_id = $sale->user_id;
       if(isset(Auth::user()->id))
            $sale_audit->updated_by = Auth::user()->id;

       $sale_audit->booking_id = $sale->booking_id;
       $sale_audit->number = $sale->number;
       $sale_audit->net_pass = $sale->net_pass;
       $sale_audit->company_id = $sale->company_id;
       $sale_audit->locker_id = $sale->locker_id;
       $sale_audit->block_id = $sale->block_id;
       $sale_audit->block_name = $sale->block_name;
       $sale_audit->start = $sale->start;
       $sale_audit->end = $sale->end;
       $sale_audit->price = $sale->price;
       $sale_audit->email = $sale->email;
       $sale_audit->status = $sale->status;
       $sale_audit->street = $sale->street;
       $sale_audit->city = $sale->city;
       $sale_audit->post_code = $sale->post_code;
       $sale_audit->country = $sale->country;
       $sale_audit->child_first_name = $sale->child_first_name;
       $sale_audit->child_surname = $sale->child_surname;
       $sale_audit->child_email = $sale->child_email;
       $sale_audit->school_year_id = $sale->school_year_id;
       $sale_audit->school_academic_year_id = $sale->school_academic_year_id;
       $sale_audit->hold_sales_id = $sale->hold_sales_id;
//       $sale_audit->type = $type;
       $sale_audit->hold_locker = $sale->hold_locker;
       $sale_audit->save();

   }

}
