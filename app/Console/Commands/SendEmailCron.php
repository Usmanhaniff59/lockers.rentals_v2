<?php

namespace App\Console\Commands;

use App\Common;
use App\CronSchedule;
use App\Email;
use App\Jobs\SendEmailJob;
use App\Mail\SendEmailTest;
use App\Sale;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendEmailCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send emails using cron & job queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    function isValidEmail($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL) 
            && preg_match('/@.+\./', $email);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $current_date_time = Carbon::now();
        $emails = Email::where('action', 'sent')
            ->where('complete', 0)
            ->where('send', 0)
            ->orderBy('priority','asc')
            ->skip(0)->take(8)
            ->get();


        if (count($emails) > 0) {

            foreach ($emails as $email) {

                if($this->isValidEmail($email->email)){
                    $user_email = $email->email;
                    $email_html = $email->mail_body;
                    $subject = $email->subject;
                    $email_id = $email->id;

                    $data['subject_title'] = $subject;
                    $data['subject'] = $subject;
                    $data['to'] = $email->email;
                    $data['name'] = 'Locker Rental';
                    $data['my_message'] = $email_html;
                    $data['emails'] = $email_html;
                    $data['from'] = \Config::get('mail.from')['address'];

                    if(!empty($email->phone_number)){

                            try {

                                $response = Common::sendSms($email->phone_number, $email->sms_body);
                                sleep(1);

                            } catch (\Exception $e) {

                            }

                    }
                   
                    Mail::send('backend.emails.test', $data, function ($message) use ($data) {
                        $message->from($data['from'], $data['subject_title']);
                        $message->to($data['to'], $data['name'])->subject($data['subject_title']);
                    });

                    if (count(Mail::failures()) > 0) {
                        $send_email = Email::where('id', $email_id)->first();
                        $send_email->send = '0';
                        $send_email->fail = 1;
                        $send_email->save();

                    } else {
                        $send_email = Email::where('id', $email_id)->first();
                        $send_email->complete = '1';
                        $send_email->send = '1';
                        $send_email->save();
                    }

                }
                else {
                    $send_email = Email::where('id', $email->id)->first();
                    $send_email->fail = 1;
                    $send_email->send = '0';
                    $send_email->save();
                }  
            }
        }
    }
}
