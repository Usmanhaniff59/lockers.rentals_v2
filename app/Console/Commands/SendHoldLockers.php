<?php

namespace App\Console\Commands;

use App\CronSchedule;
use App\Email;
use App\Sale;
use App\EmailTemplate;
use App\Maintenance;
use App\SalesAudit;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class SendHoldLockers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:checkhold';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check hold lockers and resend rebooking email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */

    //php artisan send:checkhold
    //php artisan send:emails
    ///opt/cpanel/ea-php72/root/usr/bin/php artisan optimize
    public function handle()
    {
        $reason_global = ''; // REVIEW added for terminal error
        $current_year = date("Y-09-01");
        $send_again = date("Y-m-d", strtotime("+1 week"));


        $emailBreak =0;
        $hold_sales = Sale::Where('status','r')
            ->where('start','=',$current_year)
            ->where('booking_type',2) // REVIIEW Email was going to wrong booking types
            ->whereNotNull('booking_id')
            ->where(function($status) {
                $status->where('rebooking_sent',null)
                    ->orWhere('rebooking_sent','<',5);
            })
            ->where(function($status) {
                $status->where('rebooking_email_date',null)
                    ->orWhere('rebooking_email_date','<',date("Y-m-d"));
            })
            ->orderBy('rebooking_sent','ASC')
            ->limit('3500')
            ->get();
        $reason = "";
        $array = array();
        foreach($hold_sales as $sale){


            $rebook_true = 0;

            if($sale->company->keep_block == 1)
                $rebook_true = 1;

            if($sale->company->keep_block == 2)
                $rebook_true = 1;

            if(empty($sale->company->active))
                $rebook_true = 0;

            if(empty($sale->company->display))
                $rebook_true = 0;

            if($rebook_true == 0)
                continue;



            if(empty($rebook_true)){

                $failStatus = 'Do not Send to company';
                if(empty($array[$failStatus])){
                    $array[$failStatus] = 1;
                }else{
                    $array[$failStatus]++;
                }

            }

            $reason_global.=$reason;
            $reason = "";
            $error_subject = '';
            $status = true;
            $locker_changed = false;
            $old_locker = 0;
            $new_locker = 0;
            $request_email_template = EmailTemplate::where('id',9)->first();


            $locker_number ='';
            if(!empty($sale->locker_id) && $sale->locker->id){

                if(!empty($sale->locker->locker_number))
                    $locker_number = $sale->locker->locker_number;
                else
                    $locker_number = '';

                if(empty($sale->locker->active) && $sale->company->keep_block == 1){
                    $status= false;
                    $reason.= "Locker ".$sale->locker->locker_number." Not Active %break%"; //review, is pointles '.' but i added as to flow with changes. Added $sale->locker->locker_number
                    $error_subject = 'Locker Issue';
                    $failStatus = 'Locker Not Active';
                    if(empty($array[$failStatus])){
                        $array[$failStatus] = 1;
                    }else{
                        $array[$failStatus]++;
                    }
                }

                if(empty($sale->locker->display)){
                    $status= false;
                    $reason.= "Locker is Blank in Error %break%"; //review, is pointles '.' but i added as to flow with changes.
                    $error_subject = 'Locker Issue';
                    $failStatus = 'Locker Blank';
                    if(empty($array[$failStatus])){
                        $array[$failStatus] = 1;
                    }else{
                        $array[$failStatus]++;
                    }
                }

                //check duplicates
                $double_booked = Sale::where('locker_id', $sale->locker_id)
                    ->where('start',$sale->start)
                    ->where('end',$sale->end)
                    ->where('id','NOT LIKE',$sale->id)
                    ->first(); // REVIEW ADDED
                if(!empty($double_booked->id)) // REVIEW ADDED
                {
                    $reason.= "Locker double Booked %break%";
                    $error_subject = 'Locker Issue';
                    $status = false;
                    $failStatus = 'Locker double Booked';
                    if(empty($array[$failStatus])){
                        $array[$failStatus] = 1;
                    }else{
                        $array[$failStatus]++;
                    }
                    $failStatus.= '_details';
                    $details = 'WHERE id='.$sale->id." || id = ".$double_booked->id;
                    if(empty($array[$failStatus])){
                        $array[$failStatus] = array($details);
                    } else
                        array_push($array[$failStatus],$details);
                }

                $start = date("Y-m-d H:i:s",strtotime($sale->start));
                $end = date("Y-m-d H:i:s",strtotime($sale->end));
                $is_maintained = Maintenance::Where('locker_id',$sale->locker_id)
                    ->whereBetween('from', array($start, $end))
                    ->whereBetween('to', array($start, $end))
                    ->first();
                if($is_maintained){
                    $status = false;
                    $reason.= "Locker Off to Maintenance %break%";
                    $error_subject = 'Locker Issue';
                    $failStatus = 'Locker Off to Maintenance';
                    if(empty($array[$failStatus])){
                        $array[$failStatus] = 1;
                    }else{
                        $array[$failStatus]++;
                    }
                }
            }

            else if(!empty($sale->locker_id) && $sale->locker->id){
                $status= false;
                $reason.= "Locker Does Not Exist %break%";
                $error_subject = 'Locker Issue';
                $failStatus = 'Locker Does Not Exist';
                if(empty($array[$failStatus])){
                    $array[$failStatus] = 1;
                }else{
                    $array[$failStatus]++;
                }
            }

            //check locker numbers match
            if(!empty($sale->locker_id) && $sale->locker->id){

                if(!empty($sale->hold_sales_id)){

                    $old_locker = Sale::Where('id', $sale->hold_sales_id)->first();
                    if($old_locker->locker_id != $sale->locker_id){
                        $locker_changed = true;
                        $old_locker = $old_locker->locker->locker_number;
                        $new_locker = $sale->locker->locker_number;
                    }
                }

            }

            //check blocks exist and active
            if(!empty($sale->block_id) && !empty($sale->block->id)){


                if(empty($sale->block->display)){
                    $status= false;
                    $reason.= "Block ".$sale->block->name." NOT Active %break%"; //review added as to flow with changes
                    $error_subject = 'Block Issue';
                    $failStatus = 'Block not Active';
                    if(empty($array[$failStatus])){
                        $array[$failStatus] = 1;
                    }else{
                        $array[$failStatus]++;
                    }

                    $failStatus.= ' -- '.$sale->company->name." -- ".$sale->block->name;
                    if(empty($array[$failStatus])){
                        $array[$failStatus] = 1;
                    }else{
                        $array[$failStatus]++;
                    }

                }

                // REVIEW need seperate funcyion
                if(empty($sale->block->active)){
                    $status= false;
                    $reason.= "Block ".$sale->block->name." NOT Deleted %break%";
                    $error_subject = 'Block Issue';
                    $failStatus = 'Block not Deleted';
                    if(empty($array[$failStatus])){
                        $array[$failStatus] = 1;
                    }else{
                        $array[$failStatus]++;
                    }

                    $failStatus.= ' -- '.$sale->company->name." -- ".$sale->block->name;
                    if(empty($array[$failStatus])){
                        $array[$failStatus] = 1;
                    }else{
                        $array[$failStatus]++;
                    }

                }


            } else if(!empty($sale->block_id)){


                $status= false;
                $reason.= "Block Does Not Exist %break%";
                $error_subject = 'Block Issue';
                $failStatus = 'Block Does Not Exist';
                if(empty($array[$failStatus])){
                    $array[$failStatus] = 1;
                }else{
                    $array[$failStatus]++;
                }

            }



            //check company exists
            if(!empty($sale->company->id)) {

                $company_name = $sale->company->name;

                //REVIEW altered functio as need to do seperate
                if (empty($sale->company->active)) {
                    $status = false;

                    $reason .= "Company " . $company_name . " Not Deleted %break%";
                    $error_subject = 'Company Issue';
                    $failStatus = 'Company Deleted';
                    if (empty($array[$failStatus])) {
                        $array[$failStatus] = 1;
                    } else {
                        $array[$failStatus]++;
                    }

                    $failStatus.= ' -- '.$sale->company->name;
                    if(empty($array[$failStatus])){
                        $array[$failStatus] = 1;
                    }else{
                        $array[$failStatus]++;
                    }

                }

                if (empty($sale->company->display)) {
                    $status = false;
                    $reason .= "Company " . $company_name . " Not Active %break%";
                    $error_subject = 'Company Issue';
                    $failStatus = 'Company Not Active';
                    if (empty($array[$failStatus])) {
                        $array[$failStatus] = 1;
                    } else {
                        $array[$failStatus]++;
                    }

                    $failStatus.= ' -- '.$sale->company->name;
                    if(empty($array[$failStatus])){
                        $array[$failStatus] = 1;
                    }else{
                        $array[$failStatus]++;
                    }

                }
            }

            else
            {
                $status= false;
                $company_name = 'Company Does not Exist';
                $reason.= "Company Does Not Exist %break%";
                $error_subject = 'Company Issue';

                $failStatus = 'Company Does Not Exist';
                if(empty($array[$failStatus])){
                    $array[$failStatus] = 1;
                }else{
                    $array[$failStatus]++;
                }

            }




            //check user exists
            $email_send = 'bookings@locker.rentals';
            $user_name= '-';
            if($sale->user->id){
                $first_name = $sale->user->fullname;
                $email_send = $sale->user->email;
                $user_name= $sale->user->email;
            } else {

                $first_name = "ERROR";
                $status= false;
                $reason.= "User Does Not Exist %break%";
                $error_subject = 'User Issue';
                $failStatus = 'User Does Not Exist';
                if(empty($array[$failStatus])){
                    $array[$failStatus] = 1;
                }else{
                    $array[$failStatus]++;
                }

            }


            $new_start_date = date("d M, Y", strtotime($sale->start)); //REview DADES around wrong way
            $new_end_date = date("d M, Y", strtotime($sale->end)); //REview DADES around wrong way
            $start_date = date("d M, Y", strtotime('-1 year', strtotime($sale->start)));
            $end_date = date("d M, Y", strtotime('-1 year', strtotime($sale->end)));
            $booking_cut_off_date = date("d M, Y", strtotime($sale->hold_booking_date)); //REview not created_at but hold_booking_date
            $re_book_locker_link = '<a style="color:green;" href="'.url('/admin/reserved/lockers/schooltermbasedreserved/'.$sale->booking_id).'"><u>renew this locker</u></a>';
            $unsubscribe = '<a style="color:red;" href="'.url('/unsubscribe/'.$sale->booking_id).'"><u>cancel your locker</u></a>';
            $template_text = $request_email_template->template_text;
            $reset_password_link = '<a style="color:red;" href="'.url('/password/reset').'"><u>reset</u></a>';
            $reset_password_link = "For your information your username is $user_name. If you do not remember your password you can $reset_password_link it.<br>";

            $placeholders = ["%first_name%", "%number%", "%company_name%", "%start_date%", "%end_date%", "%new_start_date%", "%new_end_date%", "%booking_cut_off_date%", "%re_book_locker_link%", "%do_not_require_locker_link%", "___if_locker_check___", "%old_locker_number%", "%new_locker_number%", "___endif_locker_check___","%error_reason%","%reset_password_link%","%user_name%"];

            $error_reason = str_replace('%break%','<br>', $reason);// added to display on email
            $error_reasonF ='';
            if(!empty($error_reason))
            {
                $error_reasonF = '<p>';
                $error_reasonF.= '<strong>Admin USE -- Reason for Email</strong><br>';
                $error_reasonF.= $error_reason;
                $error_reasonF.= '</p>';
            }


            if($locker_changed){

                $if_locker_check = '<span class="s1" style="display:block">';
                $endif_locker_check = '<span/>';

                $placeholder_values   = [$first_name, $locker_number, $company_name, $start_date, $end_date, $new_start_date, $new_end_date, $booking_cut_off_date, $re_book_locker_link, $unsubscribe, $if_locker_check, $old_locker, $new_locker, $endif_locker_check ,$error_reasonF,$reset_password_link,$user_name];
            } else {

                $if_locker_check = '<span class="s1" style="display:none">';
                $endif_locker_check = '<span/>';

                $placeholder_values   = [$first_name, $locker_number, $company_name, $start_date, $end_date, $new_start_date, $new_end_date, $booking_cut_off_date, $re_book_locker_link, $unsubscribe, $if_locker_check, "", "", $endif_locker_check,$error_reasonF,$reset_password_link,$user_name];
            }




            //if there all is perfect
            if($status){

                $failStatus = 'pass';
                if(empty($array[$failStatus])){
                    $array[$failStatus] = 1;
                }else{
                    $array[$failStatus]++;
                }


                $email_html = str_replace($placeholders, $placeholder_values, $template_text);
                //echo $template_text."\n\n\n\n";
                $email = new Email();
                $email->email = $email_send;
                //$email->email = 'rperris@xweb4u.com';
                // $email->email = 'sharon@prefectequipment.com';
                $email->action = 'sent';
                $email->subject = $request_email_template->subject;
                $email->mail_body = $email_html;
                $email->date_added = Carbon::now();
                $email->type = 'email';
                $email->result_code = '0';
                $email->priority = '2';
                $email->star = '0';
                $email->send = '0';
                $email->complete = '0';
                $email->deleted = '0';
                $email->save();

                $sale->rebooking_email_date = $send_again;
                $sale->rebooking_sent = $sale->rebooking_sent+1;
                $sale->save();


                //add sales_audit log
                $sales_audit_obj = new SalesAudit();
                $sales_audit_obj->sales_audit_log($sale->id ,$type = 'validate');


            } else {


                $failStatus = 'fail';
                if(empty($array[$failStatus])){
                    $array[$failStatus] = 1;
                }else{
                    $array[$failStatus]++;
                }

                $email_html = str_replace($placeholders, $placeholder_values, $template_text);

                $email = new Email();
                //$email->email = 'rperris@xweb4u.com';
                //$email->email = 'mdoberer@prefectequipment.com';
                //$email->email = 'sharon@prefectequipment.com';
                $email->email = 'bookings@locker.rentals';
                $email->action = 'sent';
                $email->subject = $error_subject.' '.$request_email_template->subject;
                $email->mail_body = $email_html;
                $email->date_added = Carbon::now();
                $email->type = 'email';
                $email->result_code = '0';
                $email->priority = '2';
                $email->star = '0';
                $email->send = '0';
                $email->complete = '0';
                $email->deleted = '0';
                $email->save();



            }


        }



        //$error_reason = str_replace('%break%',"\n", $reason_global);// added to echo out
        //echo $error_reason;
        print_r($array);
        Log::info($array);
    }



}
