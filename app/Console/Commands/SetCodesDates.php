<?php

namespace App\Console\Commands;

use App\Block;
use App\Locker;
use App\LockerOffSale;
use App\Maintenance;
use App\ManualLockerCode;
use App\Rate;
use App\Sale;
use App\SalesAudit;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SetCodesDates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'codes:date';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        ini_set('memory_limit','-1');
//        ini_set('max_execution_time', 900);
        //need to run only once
        $codes = ManualLockerCode::get();
//        dd($codes);
        foreach ($codes as $code){
            $code->start = date('Y-09-01 00:00:00',strtotime($code->start));
            $code->end = date('Y-08-31 00:00:00',strtotime($code->end));
            $code->save();
        }

//        $sales = Sale::join('lockers','lockers.id','=','sales.locker_id')
//            ->where('sales.status','b')
//            ->whereDate('sales.start', '>=','2020-09-01')
//            ->orderBy('sales.id','DESC')
//            ->select('sales.id','sales.booking_id','sales.locker_id','sales.number','lockers.locker_number','sales.created_at')
//            ->get();
//
//        $count =0;
//
//        foreach ($sales as $data){
//            if($data->number != $data->locker_number ){
//                $count++;
//                echo 'Sales ID : '.$data->id.' Sales Booking ID : '.$data->booking_id.', locker_id : '.$data->locker_id.', Sales Locker Number :  '.$data->number.', Actual Locker Number :  '.$data->locker_number.', created_at :  '.$data->created_at ."\n";
//
////                $data->number = $data->locker_number;
////                $data->save();
//
//            }
//        }




//        echo 'total : '.$count."\n";

//        $sales = Sale::where(function ($status) {
//            $status->where('status', 'b')
//                ->orWhere('status', 'r');
//            })
//            ->whereDate('sales.start', '>=','2020-09-01')
////            -> take(5)
//            ->get();

//        dd($sales);

//        foreach ($sales as $sale){
//
//            $sale_audit = new SalesAudit();
//            $sale_audit->sale_id = $sale->id;
//            $sale_audit->user_id = $sale->user_id;
//            $sale_audit->booking_id = $sale->booking_id;
//            $sale_audit->number = $sale->number;
//            $sale_audit->net_pass = $sale->net_pass;
//            $sale_audit->company_id = $sale->company_id;
//            $sale_audit->locker_id = $sale->locker_id;
//            $sale_audit->block_id = $sale->block_id;
//            $sale_audit->block_name = $sale->block_name;
//            $sale_audit->start = $sale->start;
//            $sale_audit->end = $sale->end;
//            $sale_audit->price = $sale->price;
//            $sale_audit->email = $sale->email;
//            $sale_audit->status = $sale->status;
//            $sale_audit->street = $sale->street;
//            $sale_audit->city = $sale->city;
//            $sale_audit->post_code = $sale->post_code;
//            $sale_audit->country = $sale->country;
//            $sale_audit->child_first_name = $sale->child_first_name;
//            $sale_audit->child_surname = $sale->child_surname;
//            $sale_audit->child_email = $sale->child_email;
//            $sale_audit->school_year_id = $sale->school_year_id;
//            $sale_audit->school_academic_year_id = $sale->school_academic_year_id;
//            $sale_audit->type = 'add';
//            $sale_audit->save();
//
//        }


    }









}
