<?php

namespace App\Console\Commands;

use App\Company;
use App\CompanyBlockError;
use App\EmailTemplate;
use App\EmailTemplateAlt;
use App\LocationGroup;
use App\ManualLockerCode;
use App\Payment;
use App\Sale;
use App\SalesAudit;
use App\Transaction;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SendConfirmationEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'confirmation:emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'sending confirmation email to client';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    //php artisan confirmation:emails
    public function handle()
    {
        $distinct_bookings = Sale::where('status', 'b')
            ->where('confirmation_sent', 0)
            ->distinct()->get(['booking_id']);

        foreach ($distinct_bookings as $distinct_booking){

            $booking_id = $distinct_booking->booking_id;

            $booking = Sale::where('booking_id',$booking_id)->first();


            $company_phone_number = \Config::get('mail.from')['company_phone_number'];
            $company_email = \Config::get('mail.from')['company_email'];

            if(isset($booking->user)) {



                $email = $booking->user->email;

                $account_first_name = $booking->user->name;
                $account_surname = $booking->user->surname;

                $sms_body = null;
                $phone_number = null;


                $sales = Sale::where('booking_id', $booking_id)->where('status', 'b')->where('confirmation_sent', 0)->get();
                if (count($sales) > 0) {

                    $do_not_process = 0;

                    foreach ($sales as $sale) {

                        //should we not start a check
                        //$code_errors = 0;


                        if (empty($sale->locker_id) || empty($sale->start) || empty($sale->end))
                            $do_not_process = 0;
                        $man_code = ManualLockerCode::where([['locker_id', $sale->locker_id],['start','=',$sale->start]])->first();

                        
                        if (!empty($man_code->code)) {

                            $code = substr(sprintf("%04d", $man_code->code), -4);
                            if (!empty($code)) {
                                // $sale->net_pass = $code;
                                $sale->save();


                                //add sales_audit log
                                $sales_audit_obj = new SalesAudit();
                                $sales_audit_obj->sales_audit_log($sale->id, $type = 'validate');
                            } else {
                                //log to error log file to be checked daily
                                // i.e could not send confirmation to xx booking id becuase code not found
                                //note should not be able to book lockers without a code
                                //continue;
                            }

                        }


                    }


                    $payment = Payment::where('booking_id', $booking->booking_id)->first();
                    if (empty($payment->id)) {

                        echo $booking->booking_id . "\n";
                        continue;
                    }
                    $total_price = $payment->amount;

                    $email_template_alt = EmailTemplateAlt::where('email_template_id', 5)->where('lang', $booking->lang)->where('link_text', $booking->lock_type)->first();


                    if ($email_template_alt) {
                        $replaced_text = $email_template_alt->text;

                        foreach ($sales as $sale) {

                            if ($sale->child_email) {
                                $child_email_template_alt = EmailTemplateAlt::where('email_template_id', 7)->where('lang', $sale->lang)->where('link_text', $sale->lock_type)->first();
                                $child_replaced_text = $email_template_alt->text;
                                $subject = $child_email_template_alt->tab_name;

                                $sales_email = new Sale();
                                $sales_email->saveEmailForCron($email, $phone_number, $subject, $child_replaced_text, $sms_body);
                            }
                        }

                        if (!empty($booking->phone_number)) {


                            $sms_template = EmailTemplate::where('id', 5)->pluck('sms_message');

                            $bookingLink = url('/login');

                            $company_name = $booking->company->name;
                            $sms_body = str_replace(["%number_of_lockers%", "%school_name%", "%admin_portal%", "%booking_number%"],
                                [strval(count($sales)), $company_name, $bookingLink, $booking_id],
                                $sms_template);

                            $sms_body = substr($sms_body, 2);
                            $sms_body = substr_replace($sms_body, "", -2);

                            $phone_number = $booking->phone_number;

                        }

                        $sales_email = new Sale();
                        $sales_email->saveEmailForCron($email, $phone_number, $subject, $replaced_text, $sms_body);
                    } else {
                        $template = EmailTemplate::where('id', 5)->first();
                        $subject = $template->subject;
                        $child_confirm_email_template_text = [];
                        $parent_confirm_email_template_text = [];
                        $template2 = explode("____Start Repeat for Child_____", $template->template_text);
                        $parent_part = $template2[0];


                        $template3 = explode("____End Repeat for Child_____", $template2[1]);

                        $child_part = $template3[0];

                        $instruction_part = $template3[1];

                        $repeat_child_text = [];
                        $replaced_repeated_childs_text = [];

                        $replaced_parent_text = str_replace(["%account_first_name%", "%account_surname%", "%reservation_id%", "%email%", "%total_cost%"],
                            [$account_first_name, $account_surname, $booking_id, $email, $total_price],
                            $parent_part);

                        array_push($parent_confirm_email_template_text, $replaced_parent_text);

                        foreach ($sales as $sale) {

                            if (empty($sale->block->name)) {
                                echo "no blod id for" . $sale->id;
                            } else {

                                $block_name = $sale->block->name;
                                $locker_number = $sale->locker->locker_number;
                                $locker_code = $sale->net_pass;
                                $company_name = $booking->company->name;

                                $location_group_id = $booking->company->location_group_id;

                                $location_group = LocationGroup::find($location_group_id);

                                $location_group_name = $location_group->name;


                                if ($location_group_id == 1 || $location_group_id == 2) {
                                    $start_date = Carbon::parse($sale->start)->format('d M, Y');
                                    $end_date = Carbon::parse($sale->end)->format('d M, Y');
                                } else {
                                    $start_date = Carbon::parse($sale->start)->format('d M, Y H:i');
                                    $end_date = Carbon::parse($sale->end)->format('d M, Y H:i');
                                }


                                $child_replaced_text = str_replace(["%child_first_name%", "%child_surname%", "%start_date%", "%end_date%", "%name%"
                                    , "%school_name%", "%block%", "%number%", "%code%", "%cost%"],
                                    [$sale->child_first_name, $sale->child_surname, $start_date, $end_date,
                                        $location_group_name, $company_name, $block_name, $locker_number, $locker_code, $sale->price],
                                    $child_part);

                                $replaced_instruction_text = str_replace(["%number%", "%code%", "%company_phone_number%", "%company_email%"],
                                    [$locker_number, $locker_code, $company_phone_number, $company_email],
                                    $instruction_part);

                                if (isset($sale->child_email)) {
                                    $child_template2 = EmailTemplate::where('id', 7)->first();
                                    $subject = $child_template2->subject;
                                    $child_replaced_text2 = str_replace(["%account_first_name%", "%account_surname%", "%reservation_id%", "%email%", "%total_cost%", "%child_first_name%", "%child_surname%", "%start_date%", "%end_date%", "%name%"
                                        , "%school_name%", "%block%", "%number%", "%code%", "%cost%", "%company_phone_number%", "%company_email%"],
                                        [$account_first_name, $account_surname, $booking_id, $email, $total_price, $sale->child_first_name, $sale->child_surname, $start_date, $end_date,
                                            $location_group_name, $company_name, $block_name, $locker_number, $locker_code, $sale->price, $company_phone_number, $company_email],
                                        $child_template2->template_text);

                                    $sales_email = new Sale();
                                    $sales_email->saveEmailForCron($sale->child_email, $phone_number, $subject, $child_replaced_text2, $sms_body);


                                }

                                array_push($parent_confirm_email_template_text, $child_replaced_text);

                            }
                        }

                        array_push($parent_confirm_email_template_text, $replaced_instruction_text);

                        $parent_confirm_email_template_text = implode(" ", $parent_confirm_email_template_text);


                        if (!empty($booking->phone_number)) {

                            $sms_template = EmailTemplate::where('id', 5)->pluck('sms_message');

                            $bookingLink = url('/login');

                            $company_name = $booking->company->name;
                            $sms_body = str_replace(["%number_of_lockers%", "%school_name%", "%admin_portal%", "%booking_number%"],
                                [strval(count($sales)), $company_name, $bookingLink, $booking_id],
                                $sms_template);

                            $sms_body = substr($sms_body, 2);
                            $sms_body = substr_replace($sms_body, "", -2);

                            $phone_number = $booking->phone_number;

                        }

                        $sales_email = new Sale();
                        $sales_email->saveEmailForCron($email, $phone_number, $subject, $parent_confirm_email_template_text, $sms_body);
                    }
                }

                foreach ($sales as $sale) {

                    $sale_confirmation_sent = Sale::find($sale->id);
                    $sale_confirmation_sent->confirmation_sent = 1;
                    $sale_confirmation_sent->save();

                }
            }else{

                $detail = 'BookingID ' . $booking->booking_id . ' has not exist User ID : ' . $booking->user_id . ' in our system. Kindly add appropriate user';

                $errors = CompanyBlockError::where('details',$detail)->where('resolved',0)->first();

                if(!$errors) {
                    $error = new CompanyBlockError();
                    $error->company_id = $booking->company_id;
                    $error->block_id = $booking->block_id;
                    $error->error_type = 'User Not Exist';
                    $error->details = $detail;
                    $error->save();
                }
            }

        }
    }
}
