<?php

namespace App\Console\Commands;

use App\CompanyBlockError;
use App\Sale;
use App\Transaction;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CheckBookings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:bookings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check booking is paid but lost';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (date("m") < 7) {
            $start = Carbon::now()->toDateTimeString();
            $end = Carbon::now()->endOfDay()->toDateTimeString();
        } else {
            $start = date("Y-09-01 00:00:00");
            $end = date("Y-09-01 23:59:59");

        }
        $bookings = Sale::select('sales.*')
            ->join('transactions', function ($join) {
                $join->on('transactions.booking_id', '=', 'sales.booking_id');
            })
            ->where(function ($q) {
                $q->where('transactions.type', 01)->orWhere('transactions.type', 02)->orWhere('transactions.type', 03)->orWhere('transactions.type', 07);

            })
            ->where('transactions.status', '0000')
            ->where(function ($query) use ($start, $end) {
                $query->where('end', '>=', $start)
                    ->Where('start', '<=', $end);
            })
            ->where('sales.status', 'l')
            ->get();

        foreach ($bookings as $booking) {
            $three_d_paid_amount = $booking->transactions->where('status', '0000')->where('type', 01)->sum('amount');
            $regular_paid_amount = $booking->transactions->where('status', '0000')->where('type', 02)->sum('amount');
            $charity_paid_amount = $booking->transactions->where('status', '0000')->where('type', 03)->sum('amount');
            $manually_paid_amount = $booking->transactions->where('status', '0000')->where('type', 07)->sum('amount');

            $total_paid_amount = $three_d_paid_amount + $charity_paid_amount + $regular_paid_amount + $manually_paid_amount;

            $refunded_amount = $booking->transactions->where('status', '0000')->where('type', 06)->sum('refund');

            $remaining_amount = number_format($total_paid_amount - $refunded_amount, 2, '.', '');


            $transaction = Transaction::where('booking_id', $booking->booking_id)->where('status', '0000')->first('currency');
            $currency = $transaction->currency;

            if ($remaining_amount > 0) {

                    $refunded_transaction = Transaction::where('booking_id', $booking->booking_id)->where('type','06')->first();
                    if(!$refunded_transaction) {
                        $sales = Sale::where('booking_id', $booking->booking_id)->get();
                        $sales_count = $sales->count();
                        $detail = 'Booking ID ' . $booking->booking_id . ' has a balance of ' . $remaining_amount . ' ' . $currency . '. This booking has ' . $sales_count . ' bookings.' . "\n";
                        foreach ($sales as $sale) {
                            $detail .= 'Booking Locker ' . $sale->number . ', ' . $sale->price . ' ' . $currency . '.' . "\n";
                        }

                        echo $detail . "\n";

                        $errors = CompanyBlockError::where('details', $detail)->where('resolved', 0)->first();

                        if (!$errors) {
                            $error = new CompanyBlockError();
                            $error->company_id = $booking->company_id;
                            $error->block_id = $booking->block_id;
                            $error->error_type = 'Paid but status is lost';
                            $error->details = $detail;
                            $error->save();
                        }
                    }



            }

        }

    }
}
