<?php

namespace App\Console\Commands;

use App\Block;
use App\Company;
use App\CompanyBlockError;
use App\DeveloperErrors;
use App\Locker;
use App\LockerOffSale;
use App\Maintenance;
use App\Payment;
use App\Sale;
use App\SalesAudit;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CompanyErrors extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'company:errors';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (date("m") < 7) {
            $start = Carbon::now()->toDateTimeString();
            $end = Carbon::now()->endOfDay()->toDateTimeString();
        } else {
            $start = date("Y-09-01 00:00:00");
            $end = date("Y-09-01 23:59:59");

        }

        //Off sale errors for booked
        $off_sales = Sale::join('locker_off_sales','locker_off_sales.locker_id','=','sales.locker_id')
            ->where(function ($query) use ($start, $end) {
                $query->whereDate('sales.end', '>=', $start)
                    ->whereDate('sales.start', '<=', $end);
            })
            ->where('sales.status', 'b')
            ->where('locker_off_sales.active',1)
            ->select('sales.*' )
            ->get();

        foreach ($off_sales as $data){
            $detail = 'BookingID ' . $data->booking_id . ' has booked locker ' . $data->number . ' from ' . Carbon::parse($data->start)->format('d M, Y') . ' - ' . Carbon::parse($data->end)->format('d M, Y') . ' but this locker is In Off Sale';
            $errors = CompanyBlockError::where('details',$detail)->where('resolved',0)->first();

            if(!$errors) {
                $error = new CompanyBlockError();
                $error->company_id = $data->company_id;
                $error->block_id = $data->block_id;
                $error->error_type = 'Off Sale Booked';
                $error->details = $detail;
                $error->save();
            }
        }

        //Off sale errors for reserved
        $off_sales = Sale::join('locker_off_sales','locker_off_sales.locker_id','=','sales.locker_id')
            ->where(function ($query) use ($start, $end) {
                $query->whereDate('sales.end', '>=', $start)
                    ->whereDate('sales.start', '<=', $end);
            })
            ->where('sales.status', 'r')
            ->where('locker_off_sales.active',1)
            ->select('sales.*' )
            ->get();

        foreach ($off_sales as $data){
            $detail = 'BookingID ' . $data->booking_id . ' has reserved locker ' . $data->number . ' from ' . Carbon::parse($data->start)->format('d M, Y') . ' - ' . Carbon::parse($data->end)->format('d M, Y') . ' but this locker is In Off Sale';
            $errors = CompanyBlockError::where('details',$detail)->where('resolved',0)->first();

            if(!$errors) {
                $error = new CompanyBlockError();
                $error->company_id = $data->company_id;
                $error->block_id = $data->block_id;
                $error->error_type = 'Off Sale Reserved';
                $error->details = $detail;
                $error->save();
            }
        }


        //Maintenance errors for booked
        $maintenances = Sale::join('maintenances','maintenances.locker_id','=','sales.locker_id')
            ->where(function ($query) use ($start, $end) {
                $query->whereDate('sales.end', '>=', $start)
                    ->whereDate('sales.start', '<=', $end);
            })
            ->where(function ($query) use ($start, $end) {
                $query->whereDate('maintenances.to', '>=', $start)
                    ->whereDate('maintenances.from', '<=', $end);
            })
            ->where('sales.status', 'b')
            ->where('maintenances.active',1)
            ->select('sales.*' )
            ->get();
//        dd($maintenances);

        foreach ($maintenances as $data){
            $detail = 'BookingID ' . $data->booking_id . ' has booked  locker ' . $data->number . ' from ' . Carbon::parse($data->start)->format('d M, Y') . ' - ' . Carbon::parse($data->end)->format('d M, Y') . ' but this locker is in Maintenance';
            $errors = CompanyBlockError::where('details',$detail)->where('resolved',0)->first();

            if(!$errors) {
                $error = new CompanyBlockError();
                $error->company_id = $data->company_id;
                $error->block_id = $data->block_id;
                $error->error_type = 'Maintenance Booked';
                $error->details = $detail;
                $error->save();
            }
        }

        //Maintenance errors for reserved
        $maintenances = Sale::join('maintenances','maintenances.locker_id','=','sales.locker_id')
            ->where(function ($query) use ($start, $end) {
                $query->whereDate('sales.end', '>=', $start)
                    ->whereDate('sales.start', '<=', $end);
            })
            ->where(function ($query) use ($start, $end) {
                $query->whereDate('maintenances.to', '>=', $start)
                    ->whereDate('maintenances.from', '<=', $end);
            })
            ->where('sales.status', 'r')
            ->where('maintenances.active',1)
            ->select('sales.*' )
            ->get();

        foreach ($maintenances as $data){
            $detail = 'BookingID ' . $data->booking_id . ' has reserved locker ' . $data->number . ' from ' . Carbon::parse($data->start)->format('d M, Y') . ' - ' . Carbon::parse($data->end)->format('d M, Y') . ' but this locker is in Maintenance';
            $errors = CompanyBlockError::where('details',$detail)->where('resolved',0)->first();

            if(!$errors) {
                $error = new CompanyBlockError();
                $error->company_id = $data->company_id;
                $error->block_id = $data->block_id;
                $error->error_type = 'Maintenance Reserved';
                $error->details = $detail ;
                $error->save();
            }
        }

        //Deleted errors for booked
        $deleteds = Sale::join('lockers','lockers.id','=','sales.locker_id')
            ->where(function ($query) use ($start, $end) {
                $query->whereDate('sales.end', '>=', $start)
                    ->whereDate('sales.start', '<=', $end);
            })
            ->where('sales.status', 'b')
            ->where('lockers.active',0)
            ->select('sales.*' )
            ->get();

        foreach ($deleteds as $data){
            $detail = 'Booking ID ' . $data->booking_id . ' has booked locker ' . $data->number . ' from ' . Carbon::parse($data->start)->format('d M, Y') . ' - ' . Carbon::parse($data->end)->format('d M, Y') . ' but this locker is Deleted';
            $errors = CompanyBlockError::where('details',$detail)->where('resolved',0)->first();

            if(!$errors) {
                $error = new CompanyBlockError();
                $error->company_id = $data->company_id;
                $error->block_id = $data->block_id;
                $error->error_type = 'Deleted Booked';
                $error->details = $detail ;
                $error->save();
            }
        }

        //Deleted errors for reserved
        $deleteds = Sale::join('lockers','lockers.id','=','sales.locker_id')
            ->where(function ($query) use ($start, $end) {
                $query->whereDate('sales.end', '>=', $start)
                    ->whereDate('sales.start', '<=', $end);
            })
            ->where('sales.status', 'r')
            ->where('lockers.active',0)
            ->select('sales.*' )
            ->get();

        foreach ($deleteds as $data){
            $detail = 'Booking ID ' . $data->booking_id . ' has reserved locker ' . $data->number . ' from ' . Carbon::parse($data->start)->format('d M, Y') . ' - ' . Carbon::parse($data->end)->format('d M, Y') . ' but this locker is Deleted';
            $errors = CompanyBlockError::where('details',$detail)->where('resolved',0)->first();

            if(!$errors) {
                $error = new CompanyBlockError();
                $error->company_id = $data->company_id;
                $error->block_id = $data->block_id;
                $error->error_type = 'Deleted Reserved';
                $error->details = $detail ;
                $error->save();
            }
        }

        //Hidden errors for booked
        $hiddens = Sale::join('lockers','lockers.id','=','sales.locker_id')
            ->where(function ($query) use ($start, $end) {
                $query->whereDate('sales.end', '>=', $start)
                    ->whereDate('sales.start', '<=', $end);
            })
            ->where('sales.status', 'b')
            ->where('lockers.display',0)
            ->select('sales.*' )
            ->get();

        foreach ($hiddens as $data){
            $detail = 'Booking ID ' . $data->booking_id . ' has booked locker ' . $data->number . ' from ' . Carbon::parse($data->start)->format('d M, Y') . ' - ' . Carbon::parse($data->end)->format('d M, Y') . ' but this locker is Hidden';
            $errors = CompanyBlockError::where('details',$detail)->where('resolved',0)->first();

            if(!$errors) {
                $error = new CompanyBlockError();
                $error->company_id = $data->company_id;
                $error->block_id = $data->block_id;
                $error->error_type = 'Hidden Booked';
                $error->details = $detail;
                $error->save();
            }
        }
        //Hidden errors for reserved
        $hiddens = Sale::join('lockers','lockers.id','=','sales.locker_id')
            ->where(function ($query) use ($start, $end) {
                $query->whereDate('sales.end', '>=', $start)
                    ->whereDate('sales.start', '<=', $end);
            })
            ->where('sales.status', 'r')
            ->where('lockers.display',0)
            ->select('sales.*' )
            ->get();

        foreach ($hiddens as $data){
            $detail = 'Booking ID '.$data->booking_id.' has reserved locker '.$data->number.' from '.Carbon::parse($data->start)->format('d M, Y').' - '.Carbon::parse($data->end)->format('d M, Y').' but this locker is Hidden';
            $errors = CompanyBlockError::where('details',$detail)->where('resolved',0)->first();

            if(!$errors) {
                $error = new CompanyBlockError();
                $error->company_id = $data->company_id;
                $error->block_id = $data->block_id;
                $error->error_type = 'Hidden Reserved';
                $error->details = $detail;
                $error->save();
            }

        }

        //Locker Number errors for booked
        $locker_numbers = Sale::join('lockers','lockers.id','=','sales.locker_id')
            ->where(function ($query) use ($start, $end) {
                $query->whereDate('sales.end', '>=', $start)
                    ->whereDate('sales.start', '<=', $end);
            })
            ->where('sales.status', 'b')
            ->where(function ($query)  {
                $query->whereNull('lockers.locker_number')
                    ->orWhere('lockers.locker_number',0);
            })

            ->select('sales.*' )
            ->get();
//        dd($locker_numbers);

        foreach ($locker_numbers as $data){

            $detail = 'Booking ID '.$data->booking_id.' has booked locker '.$data->number.' from '.Carbon::parse($data->start)->format('d M, Y').' - '.Carbon::parse($data->end)->format('d M, Y').' but Locker Number is missing';
           $errors = CompanyBlockError::where('details',$detail)->where('resolved',0)->first();
            if(!$errors) {

                $error = new CompanyBlockError();
                $error->company_id = $data->company_id;
                $error->block_id = $data->block_id;
                $error->error_type = 'Locker Number Booked';
                $error->details = $detail;
                $error->save();
            }
        }

        //Locker Number errors for reserved
        $locker_numbers = Sale::join('lockers','lockers.id','=','sales.locker_id')
            ->where(function ($query) use ($start, $end) {
                $query->whereDate('sales.end', '>=', $start)
                    ->whereDate('sales.start', '<=', $end);
            })
            ->where('sales.status', 'r')
            ->where(function ($query)  {
                $query->whereNull('lockers.locker_number')
                    ->orWhere('lockers.locker_number',0);
            })
            ->select('sales.*' )
            ->get();

        foreach ($locker_numbers as $data){

            $detail ='Booking ID '.$data->booking_id.' has reserved locker '.$data->number.' from '.Carbon::parse($data->start)->format('d M, Y').' - '.Carbon::parse($data->end)->format('d M, Y').' but Locker Number is missing';
            $errors = CompanyBlockError::where('details',$detail)->where('resolved',0)->first();
            if(!$errors) {

                $error = new CompanyBlockError();
                $error->company_id = $data->company_id;
                $error->block_id = $data->block_id;
                $error->error_type = 'Locker Number Reserved';
                $error->details = $detail;
                $error->save();
            }
        }

        $payments = Payment::
//            take(2) ->
           get();







        //check the different account balance on the basis of paid & refunded
        $payments = Payment::join('sales', 'payments.booking_id', '=', 'sales.booking_id')
            ->select('payments.*')
            ->get();

        foreach ($payments as $payment){

            $total_paid = $payment->transactions()->where(function ($query)  {
                $query->where('type',01)->orWhere('type',02)->orWhere('type',03)->orWhere('type',07);
            })->where('status',0000)->sum('amount');

            $total_refund = $payment->transactions()->where('type',06)
                ->where(function ($query)  {
                    $query->where('status',0000)->orWhere('status',0);
                })->sum('refund');

            $account_balance = $total_paid - $total_refund ;

            $account_balance = number_format((float)$account_balance, 2, '.', '');


            if($payment->account_balance != $account_balance) {
                echo 'booking_id : '. $payment->booking_id . ' current account balance : '.$payment->account_balance.' new balance : '.$account_balance."\n";

                $detail ='Booking ID '.$payment->booking_id.' has paid '.$total_paid.' and refunded '.$total_refund.' but account balance is not equal to current account_balance column in payments. Current account balance : '.$payment->account_balance.' new account balance '.$account_balance;
                $errors = DeveloperErrors::where('details',$detail)->where('resolved',0)->first();

                if(!$errors) {

                    $dev_error = new DeveloperErrors();
                    $dev_error->user_id = $payment->user_id;
                    $dev_error->booking_id = $payment->booking_id;
                    $dev_error->error_type = 'Not Equal Account Balance';
                    $dev_error->details = $detail;
                    $dev_error->save();
                }


                if($account_balance == 0) {

                    $sales = Sale::where('booking_id', $payment->booking_id)->get();

                    foreach ($sales as $sale) {
                        if ($sale->status == 'b') {

                            echo 'booking_id : '. $payment->booking_id .'is now booked but all refunded';

                            $detail ='Booking ID '.$payment->booking_id.' has paid '.$total_paid.' and refunded '.$total_refund.' and account balance is 0 but status is booked currently.its should be lost';

                            $errors = CompanyBlockError::where('details',$detail)->where('resolved',0)->first();
                            if(!$errors) {
                                $error = new CompanyBlockError();
                                $error->company_id = $sale->company_id;
                                $error->block_id = $sale->block_id;
                                $error->error_type = 'Refunded but booked';
                                $error->details = $detail;
                                $error->save();
                            }
                        }else {
                            $payment->account_balance = $account_balance;
                            $payment->save();
                        }
                    }
                }

            }
        }



//        //update the lost booking where account balance is > 0
        $bookings = Sale::join('payments', 'payments.booking_id', '=', 'sales.booking_id')
            ->where('payments.account_balance', '>', 0)
//             ->where('sales.status','!=','b')
            ->select('sales.*')
            ->orderBy('id','DESC')
            ->get();

        foreach ($bookings as $data) {


            if($data->status != 'b') {

                $detail = 'Booking ID ' . $data->booking_id . ' has account balance > 0 and status is not booked.Now its changed this status to booked why this??';

                $errors = DeveloperErrors::where('details', $detail)->where('resolved', 0)->first();

                if (!$errors) {

                    $dev_error = new DeveloperErrors();
                    $dev_error->user_id = $data->user_id;
                    $dev_error->booking_id = $data->booking_id;
                    $dev_error->error_type = 'Lost to Booked';
                    $dev_error->details = $detail;
                    $dev_error->save();
                }

                $data->status = 'b';
                $data->save();

                //add sales_audit log
                $sales_audit_obj = new SalesAudit();
                $sales_audit_obj->sales_audit_log($data->id , $type = 'validate');


            }
        }


        //duplicate lockers
        $bookings = Sale::join('payments', 'payments.booking_id', '=', 'sales.booking_id')
            ->select('sales.*')
            ->where('payments.account_balance', '>', 0)
            ->where('sales.status', 'b')
            ->orderBy('id','DESC')
            ->get();

        foreach ($bookings as $data) {

            $duplicate = Sale::where('locker_id',$data->locker_id)->where('start',$data->start)->where('end',$data->end)
                ->where(function ($query)  {
                    $query->where('status','b')->orWhere('status','r');
                })->where('id','<>',$data->id)->first();

            if($duplicate){
                echo 'duplicate '.$data->booking_id,' locker_id : '.$data->locker_id.' with sale ID '.$duplicate->id."\n";

                $detail ='Sale ID '.$data->id.' and '.$duplicate->id.' has duplicate booked locker ID '.$data->locker_id.' from '.Carbon::parse($data->start)->format('d M, Y').' - '.Carbon::parse($data->end)->format('d M, Y');

                $duplicate_id = $duplicate->id;
                $data_id = $data->id;
                $errors_ids = CompanyBlockError:: where(function ($query) use($duplicate_id ,$data_id ) {
                    $query->where('details', 'LIKE', '%' .$duplicate_id.'%')->orWhere('details', 'LIKE', '%' .$data_id.'%');
                })->where('error_type','Duplicate Booking')->where('resolved',0)->first();

                if(!$errors_ids ) {
                        $error = new CompanyBlockError();
                        $error->company_id = $data->company_id;
                        $error->block_id = $data->block_id;
                        $error->error_type = 'Duplicate Booking';
                        $error->details = $detail;
                        $error->save();
                }
            }

        }


        //account blance is zero but status is booked
        $sales = Sale::join('payments', 'payments.booking_id', '=', 'sales.booking_id')
            ->select('sales.*')
            ->where('payments.account_balance', 0)
            ->where('sales.status', 'b')
            ->whereDate('sales.start', '>=','2020-09-01')
            ->orderBy('id','DESC')
            ->get();

        foreach ($sales as $sale){

                echo 'booking_id : '. $sale->booking_id .' not paid but booked'."\n";

                $detail ='Booking ID '.$sale->booking_id.' has account balance is 0. Please review booking status.';

                $errors = CompanyBlockError::where('details',$detail)->where('resolved',0)->first();
                if(!$errors) {
                    $error = new CompanyBlockError();
                    $error->company_id = $sale->company_id;
                    $error->block_id = $sale->block_id;
                    $error->error_type = 'Zero Account Balance';
                    $error->details = $detail;
                    $error->save();
                }
        }


        //locker,copmany,block statuses & availability
        $sales = Sale::join('payments', 'payments.booking_id', '=', 'sales.booking_id')
            ->select('sales.*')
            ->where('payments.account_balance','>', 0)
            ->where('sales.status', 'b')
            ->whereDate('sales.start', '>=','2020-09-01')
            ->orderBy('id','DESC')
//            ->take(100)
            ->get();

        foreach ($sales as $data){


            if($data->locker_id == null){
                echo 'booking_id : ' . $data->booking_id . ' booked but locker is missing' . "\n";

                $detail = 'Booking ID ' . $data->booking_id . ' has booked from ' . Carbon::parse($data->start)->format('d M, Y') . ' - ' . Carbon::parse($data->end)->format('d M, Y') . ' but Locker is missing';

                $errors = CompanyBlockError::where('details', $detail)->where('resolved', 0)->first();
                if (!$errors) {
                    $error = new CompanyBlockError();
                    $error->company_id = $data->company_id;
                    $error->block_id = $data->block_id;
                    $error->error_type = 'Booked But Locker Missing';
                    $error->details = $detail;
                    $error->save();
                }
            }

            if($data->company_id == null){
                echo 'booking_id : ' . $data->booking_id . ' booked but Company is missing' . "\n";

                $detail = 'Booking ID ' . $data->booking_id . ' has booked from ' . Carbon::parse($data->start)->format('d M, Y') . ' - ' . Carbon::parse($data->end)->format('d M, Y') . ' but company is missing';

                $errors = CompanyBlockError::where('details', $detail)->where('resolved', 0)->first();
                if (!$errors) {
                    $error = new CompanyBlockError();
                    $error->company_id = $data->company_id;
                    $error->block_id = $data->block_id;
                    $error->error_type = 'Booked But Company Missing';
                    $error->details = $detail;
                    $error->save();
                }
            }


            if($data->block_id == null){
                echo 'booking_id : ' . $data->booking_id . ' booked but block is missing' . "\n";

                $detail = 'Booking ID ' . $data->booking_id . ' has booked from ' . Carbon::parse($data->start)->format('d M, Y') . ' - ' . Carbon::parse($data->end)->format('d M, Y') . ' but block is missing';

                $errors = CompanyBlockError::where('details', $detail)->where('resolved', 0)->first();
                if (!$errors) {
                    $error = new CompanyBlockError();
                    $error->company_id = $data->company_id;
                    $error->block_id = $data->block_id;
                    $error->error_type = 'Booked But Block Missing';
                    $error->details = $detail;
                    $error->save();
                }
            }

            if(isset($data->locker_id)) {
                $locker = Locker::where('id', $data->locker_id)->where('active',1)->first();
                if (empty($locker)) {
                    echo 'booking_id : ' . $data->booking_id . ' booked but Locker ' . $data->locker_id . ' is not active' . "\n";

                    $detail = 'Booking ID ' . $data->booking_id . ' has booked from ' . Carbon::parse($data->start)->format('d M, Y') . ' - ' . Carbon::parse($data->end)->format('d M, Y') . ' but Locker ' . $data->locker_id . ' is not active';

                    $errors = CompanyBlockError::where('details', $detail)->where('resolved', 0)->first();
                    if (!$errors) {
                        $error = new CompanyBlockError();
                        $error->company_id = $data->company_id;
                        $error->block_id = $data->block_id;
                        $error->error_type = 'Booked But Locker Not Active';
                        $error->details = $detail;
                        $error->save();
                    }
                }
            }
            if(isset($data->company_id)) {
                $company = Company::where('id', $data->company_id)->where('active',1)->first();
                if (empty($company)) {
                    echo 'booking_id : ' . $data->booking_id . ' booked but company ' . $data->company_id . ' is not active' . "\n";

                    $detail = 'Booking ID ' . $data->booking_id . ' has booked from ' . Carbon::parse($data->start)->format('d M, Y') . ' - ' . Carbon::parse($data->end)->format('d M, Y') . ' but company ' . $data->company_id . ' is not active';

                    $errors = CompanyBlockError::where('details', 'LIKE', '%' .$data->company_id.'%')->where('error_type', 'Booked But Company Not Active')->where('resolved', 0)->first();
                    if (!$errors) {
                        $error = new CompanyBlockError();
                        $error->company_id = $data->company_id;
                        $error->block_id = $data->block_id;
                        $error->error_type = 'Booked But Company Not Active';
                        $error->details = $detail;
                        $error->save();
                    }
                }
            }
            if(isset($data->block_id)) {
                $block = Block::where('id', $data->block_id)->where('active',1)->first();
                if (empty($block)) {
                    echo 'booking_id : ' . $data->booking_id . ' booked but block ' . $data->block_id . ' is not active ' . "\n";

                    $detail = 'Booking ID ' . $data->booking_id . ' has booked from ' . Carbon::parse($data->start)->format('d M, Y') . ' - ' . Carbon::parse($data->end)->format('d M, Y') . ' but block ' . $data->block_id . '  is not active';

                    $errors = CompanyBlockError::where('details', 'LIKE', '%' .$data->block_id.'%')->where('error_type', 'Booked But Block Not Active')->where('resolved', 0)->first();
                    if (!$errors) {
                        $error = new CompanyBlockError();
                        $error->company_id = $data->company_id;
                        $error->block_id = $data->block_id;
                        $error->error_type = 'Booked But Block Not Active';
                        $error->details = $detail;
                        $error->save();
                    }
                }
            }

            $maintenances = Maintenance::where('to', '>=', $data->start)->Where('from', '<=', $data->end)
                ->where('locker_id',$data->locker_id)
                ->where('active', 1)
                ->first();

            if (!empty($maintenances)) {

                echo 'booking_id : ' . $data->booking_id . ' booked but locker ' . $data->locker_id . ' in maintenance' . "\n";

                $detail = 'Booking ID ' . $data->booking_id . ' has booked locker ' . $data->number . ' from ' . Carbon::parse($data->start)->format('d M, Y') . ' - ' . Carbon::parse($data->end)->format('d M, Y') . ' but Locker ' . $data->locker_id . ' is in Maintenance';

                $errors = CompanyBlockError::where('details', $detail)->where('resolved', 0)->first();
                if (!$errors) {
                    $error = new CompanyBlockError();
                    $error->company_id = $data->company_id;
                    $error->block_id = $data->block_id;
                    $error->error_type = 'Locker Booked But In Maintenance';
                    $error->details = $detail;
                    $error->save();
                }
            }

            $locker_off_sale = LockerOffSale::where('locker_id', $data->locker_id)->where('active', 1)->first();

            if (!empty($locker_off_sale)) {
                echo 'booking_id : ' . $data->booking_id . ' booked but in off sale' . "\n";

                $detail = 'Booking ID ' . $data->booking_id . ' has booked locker ' . $data->number . ' from ' . Carbon::parse($data->start)->format('d M, Y') . ' - ' . Carbon::parse($data->end)->format('d M, Y') . ' but Locker ' . $data->locker_id . ' is in Off Sale';

                $errors = CompanyBlockError::where('details', $detail)->where('resolved', 0)->first();
                if (!$errors) {
                    $error = new CompanyBlockError();
                    $error->company_id = $data->company_id;
                    $error->block_id = $data->block_id;
                    $error->error_type = 'Locker Booked But In Off Sale';
                    $error->details = $detail;
                    $error->save();
                }
            }


        }



    }
}
