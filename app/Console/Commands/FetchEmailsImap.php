<?php

namespace App\Console\Commands;

use App\Email;
use Illuminate\Console\Command;
use Webklex\IMAP\Client;
use Carbon\Carbon;

class FetchEmailsImap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'imap:emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'fetch emails from server using imap';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $oClient = new Client([
            'host'          => 'mail.xweb4u.com',
            'port'          => 993,
            'encryption'    => 'ssl',
            'validate_cert' => true,
            'username'      => 'usman@xweb4u.com',
            'password'      => 'HpCS]$NLgEHH',
            'protocol'      => 'imap'
        ]);

        $connection =  $oClient->connect();


        $aFolder = $oClient->getFolders();

        foreach($aFolder as $oFolder){

            $aMessage = $oFolder->messages()->all()->get();

            foreach($aMessage as $oMessage){

                if($oMessage->moveToFolder('INBOX.read') == true){

                    $email_html = $oMessage->getHTMLBody(true);
                    $from = $oMessage->getFrom();
                    $from = $from[0]->mail;

                    $email = new Email();
                    $email->email = $from;
                    $email->action = 'inbox';
                    $email->subject = $oMessage->getSubject();
                    $email->mail_body = $email_html;
                    $email->date_added = Carbon::now();
                    $email->type = 'email';
                    $email->result_code = '0';
                    $email->star = '0';
                    $email->send = '0';
                    $email->complete = '0';
                    $email->deleted = '0';
                    $email->save();
                }else{
//                    echo 'Message could not be moved';
                }
            }
        }
    }
}
