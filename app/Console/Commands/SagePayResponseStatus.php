<?php

namespace App\Console\Commands;

use App\ManualLockerCode;
use App\Payment;
use App\Sale;
use App\SalesAudit;
use App\Transaction;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SagePayResponseStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sagepay:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        // we discussed transactions.waiting_response_sagepay == 1
        // its the trasaction you care about
        $tranactions = Transaction::where('waiting_response_sagepay', 1)
           ->where('created_at','<=',Carbon::now()->subMinute())->get();

       $payment_modal = new Payment();

        foreach ($tranactions as $transaction) {


              $booking_id = $transaction->booking_id;
              $payment = Payment::where('booking_id', $booking_id)->first();

              if (empty($payment)) {
                  $new_payment = new Payment();
                  $new_payment->booking_id = $booking_id;
                  $new_payment->user_id = $transaction->user_id;
                  $new_payment->save();
              }

              $booking_id = $transaction->booking_id;
              $payment_id = $transaction->payment->id;
              $user_id = $transaction->user_id;
              $currency = $transaction->payment->currency;

              $last4Digits = $transaction->last4Digits; // wjy????? this can be got frim sage pay if needed

              $vendorTxCode = $transaction->vendor_tx_code;
              //get transaction ID from Sage Pay

              $command = 'getTransactionDetail';
              $sage_pay_response = $payment_modal->get_transaction_from_sagepay_using_vendor_tx_code($vendorTxCode, $command);

              if (isset($sage_pay_response['vpstxid'])) {
                  $transactionId = $sage_pay_response['vpstxid'];
//                  $transactionId ='3A53A97F-6A1E-E64A-EDDB-49BFA1C81CE1';
                  //Get Trasaction Status From Sage Pay
                  $retrieve_transaction_response = $payment_modal->get_transaction_from_sagepay_using_transaction_id($transactionId);

                  if (isset($retrieve_transaction_response->statusCode)) {

                      $status_message = $retrieve_transaction_response->statusCode . ':' . $retrieve_transaction_response->statusDetail;
                      $statusCode = $retrieve_transaction_response->statusCode;

                      //If Paid
                      if ($retrieve_transaction_response->statusCode == '0000') {

                          $total_price = $retrieve_transaction_response->amount->totalAmount / 100;
                          $price_sage_pay = new Payment();
                          $price = $price_sage_pay->price_calculation_sagepay_response($booking_id, $total_price);
                          $total_lockers_price = $price[0];
                          $charity_price = $price[1];
                          $total_price = $price[2];

                          $payment->amount = $total_price;
                          $payment->account_balance = $total_price;
                          $payment->has_transaction = 1;
                          $payment->save();

                          //..$transaction-> ....
                          $transaction->transaction_id = $transactionId;
                          $transaction->amount = $total_lockers_price;
                          $transaction->status = $statusCode;
                          $transaction->status_message = $status_message;
                          $transaction->waiting_response_sagepay = 0;
                          $transaction->save();

                          if ($charity_price > 0) {
                              $waiting_response_sagepay = 0;
                              $payment_modal->insert_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $charity_price, '03', $statusCode, $vendorTxCode, $status_message, 0, $transactionId, $waiting_response_sagepay);
                          }

                          //update booking as booked
                          $sales = Sale::where('booking_id', $booking_id)->get();
                          if (!empty($sales)) {
                              foreach ($sales as $sale) {

                                  $sale->status = 'b';
                                  $sale->save();

                                  //add sales_audit log
                                  $sales_audit_obj = new SalesAudit();
                                  $sales_audit_obj->sales_audit_log($sale->id , $type = 'validate');

                              }
                          }

                      }


                  }

              }

          if (Carbon::parse($transaction->created_at)->addMinutes(15) <= Carbon::now()) {

              $sage_pay_response = $payment_modal->get_transaction_from_sagepay_using_vendor_tx_code($vendorTxCode, $command);

              if (isset($sage_pay_response['vpstxid'])) {
                  $transactionId = $sage_pay_response['vpstxid'];

                  //Get Trasaction Status From Sage Pay
                  $retrieve_transaction_response = $payment_modal->get_transaction_from_sagepay_using_transaction_id($transactionId);

                  //If Response Status Resolved in Sage Pay
                  if (isset($retrieve_transaction_response->statusCode)) {

                      $status_message = $retrieve_transaction_response->statusCode . ':' . $retrieve_transaction_response->statusDetail;
                      $statusCode = $retrieve_transaction_response->statusCode;

                      //If Paid
                      if ($retrieve_transaction_response->statusCode == '0000') {

                          $total_price = $retrieve_transaction_response->amount->totalAmount / 100;
                          $price_sage_pay = new Payment();
                          $price = $price_sage_pay->price_calculation_sagepay_response($booking_id, $total_price);
                          $total_lockers_price = $price[0];
                          $charity_price = $price[1];
                          $total_price = $price[2];

                          $payment = Payment::where('booking_id', $booking_id)->first();
                          if (empty($payment->amount) || $payment->amount == '0.00') {
                              $payment->amount = $total_price;
                              $payment->account_balance = $total_price;
                              $payment->has_transaction = 1;
                              $payment->save();

                              //..$transaction-> ....
                              $transaction->transaction_id = $transactionId;
                              $transaction->amount = $total_lockers_price;
                              $transaction->status = $statusCode;
                              $transaction->status_message = $status_message;
                              $transaction->waiting_response_sagepay = 0;
                              $transaction->save();

                              if ($charity_price > 0) {
                                  $waiting_response_sagepay = 0;
                                  $payment_modal->insert_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $charity_price, '03', $statusCode, $vendorTxCode, $status_message, 0, $transactionId, $waiting_response_sagepay);
                              }


                          }



                          //update booking as booked
                          $sales = Sale::where('booking_id', $booking_id)->get();
                          if (!empty($sales)) {
                              foreach ($sales as $sale) {

                                  $sale->status = 'b';
                                  $sale->save();

                              }
                          }

                      } else {
                          //..$transaction-> ....
                          $transaction->status = $statusCode;
                          $transaction->status_message = $status_message;
                          $transaction->waiting_response_sagepay = 0;
                          $transaction->save();
                      }


                  }elseif($retrieve_transaction_response->code){

                      $status_message = $retrieve_transaction_response->code . ':' . $retrieve_transaction_response->description;
                      $statusCode = $retrieve_transaction_response->code;

                      $transaction->status = $statusCode;
                      $transaction->status_message = $status_message;
                      $transaction->waiting_response_sagepay = 0;
                      $transaction->save();
                  }

                  //If not responed in 15 minutes


              } elseif (isset($sage_pay_response['errorcode'])) {
                  $status_message = $sage_pay_response['errorcode'] . ':' . $sage_pay_response['error'];
                  $statusCode = $sage_pay_response['errorcode'];
                  //..$transaction-> ....

                  $transaction->status = $statusCode;
                  $transaction->status_message = $status_message;
                  $transaction->waiting_response_sagepay = 0;
                  $transaction->save();

              }else{

                  $transaction->waiting_response_sagepay = 0;
                  $transaction->save();
              }
          }
        }



        $sales = Sale::where('status', 'b')->whereNull('net_pass')->get();

        if (!empty($sales)) {
            foreach ($sales as $sale) {

//                $codes = ManualLockerCode::where('locker_id', $sale->locker_id)
//                    ->where('start',date("Y-08-31 00:00:00",strtotime($sale->start)))
//                    ->where('end',date("Y-09-01 00:00:00",strtotime($sale->end)))
//                    ->first();

//                if (!empty($codes)) {
//                    $sale->net_pass = substr(sprintf("%04d",$codes->code),  -4);
//                    $sale->save();


//                }

            $sale_obj = new Sale();
            $sale_obj->update_locker_code($sale->id);
            }
        }


    }

}