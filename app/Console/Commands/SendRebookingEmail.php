<?php

namespace App\Console\Commands;

use App\Block;
use App\Company;
use App\Sale;
use App\User;
use Illuminate\Console\Command;
use App\Email;
use App\EmailTemplate;
use Carbon\Carbon;

class SendRebookingEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:rebooking';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    //php artisan send:rebooking
    public function handle()
    {

        /*
        if (date_default_timezone_get()) {
            echo 'date_default_timezone_set: ' . date_default_timezone_get() . "\n";
        }

         $dateTime = new \DateTime(null, new \DateTimeZone('Europe/London'));
        $dateTime->setTimestamp(strtotime(date("Y-m-d H:i:s")));
        echo "--- p ---".$dateTime->format('d M, Y H:i')."\n\n";


        */









        $count =0;
        $request_email_template = EmailTemplate::where('id', 11)->first();
        $template_text = $request_email_template->template_text;
        $send_again = date("Y-m-d H:i:s", strtotime("+45 minutes")); // remind again...
        $check_date = date("Y-m-d H:i:s", strtotime("+5 minutes")); // may still be doing the booking and gone for a drink

        function get_string_between($string, $start, $end){
            $string = ' ' . $string;
            $ini = strpos($string, $start);
            if ($ini == 0) return '';
            $ini += strlen($start);
            $len = strpos($string, $end, $ini) - $ini;
            return substr($string, $ini, $len);
        }

        $repeat_text_template = get_string_between($template_text, '______start_lockers_list______', '______end_lockers_list______');
        $template_text = str_replace($repeat_text_template,"%repeat_text_plaveholder%", $template_text);
        $template_text = str_replace(["______start_lockers_list______","______end_lockers_list______"],["",""], $template_text);

        //developers remove
        $booking_array = array();
        $hold_sales = Sale::Where('status', 'r')
            ->where('booking_type', 1)
            ->whereNotNull('block_id')
            ->whereNotNull('locker_id')
            ->whereNotNull('booking_id')
            ->where(function ($status) {
                $status->where('rebooking_sent', null)
                    ->orWhere('rebooking_sent', '<', 2);
            })
            ->where(function ($email_date) {
                $email_date->where('rebooking_email_date', null)
                    ->orWhere('rebooking_email_date', '<', date("Y-m-d H:i:s"));
            })
            ->where('booking_expiry','<',$check_date)
            ->get();
        //later we do no need to do this as planned to create a bookings table
        foreach ($hold_sales as $sale) {

            //->whereIn('block_id', $block_array)
            if (!in_array($sale->booking_id, $booking_array)) {
                $booking_array[] = $sale->booking_id;
            }

            $count++;

        }

        echo "Count: ".$count."'\n";
        echo "Date Send: ".$check_date."\n\n";



        foreach ($booking_array as $b) {


            $name = '';
            $company_name = '';
            $expiry_date = '';
            $email_address ='';


            $rec =0;
            $repeat_array = array();
            $sales = Sale::Where('status', 'r')
                ->where('booking_id', $b)
                ->where('booking_expiry','<',$check_date)
                ->get();
            foreach ($sales as $s) {

                 $s->rebooking_email_date = $send_again;
                 $s->rebooking_sent = $s->rebooking_sent + 1;
                 $s->save();

                 if($s->title == "LOCKER_M")
                     continue;

                if (empty($rec)) {

                    $name = $s->user->fullname;
                    $company_name = $s->company->name;

                    $dateTime = new \DateTime(null, new \DateTimeZone('Europe/London'));
                    $dateTime->setTimestamp(strtotime(date($s->hold_booking_date)));
                    $expiry_date = $dateTime->format('d M, Y H:i')."\n\n";

                    $email_address = $s->email;

                    //incase not loaded in to repeat area
                    $first_name = $s->child_first_name;
                    $surname = $s->child_surname;


                    $number = $s->number;
                    $block = $s->block->name;
                    $start = date("d M, Y", strtotime($s->start));
                    $end = date("d M, Y", strtotime($s->start));
                    $currency = $s->currency;
                    $cost = $s->price;
                }


                $repeat_number = 'Not Selected';
                if(!empty($s->number))
                    $repeat_number = $s->number;

                $repeat_block = 'Not Selected';
                if(!empty($s->block->name))
                    $repeat_block = $s->block->name;

                $repeat_currency = '';
                if(!empty($s->currency))
                    $repeat_currency = $s->currency;

                $repeat_price = "Not Quoted";
                if(!empty($s->price))
                    $repeat_price = number_format($s->price,2,'.',',');

                $repeat_array[] = [
                    'first_name' => $s->child_first_name,
                    'surname' => $s->child_surname,
                    'number' => $repeat_number,
                    'block' => $repeat_block,
                    'start' => date("d M, Y", strtotime($s->start)),
                    'end' => date("d M, Y", strtotime($s->end)),
                    'currency' => $repeat_currency,
                    'cost' => $repeat_price,
                ];

                $rec++;

            }


            $repeat_text ='';
            $count_sec = 0;
            foreach ($repeat_array as $r) {

                $repeat_first_name = $r["first_name"];
                $repeat_surname = $r["surname"];
                $repeat_number = $r["number"];
                $repeat_block = $r["block"];
                $repeat_start = $r["start"];
                $repeat_end = $r["end"];
                $repeat_currency = $r["currency"];
                $repeat_cost = $r["cost"];

                if(empty($count_sec))
                    $break_s = "<hr>";
                else
                    $break_s = "";

                $break_e = "<hr>";

                $repeat_placeholders = ["%first_name%", "%surname%", "%number%", "%block%", "%start%", "%end%", "%currency%", "%cost%"];
                $repeat_placeholder_values = [$repeat_first_name, $repeat_surname, $repeat_number, $repeat_block, $repeat_start, $repeat_end, $repeat_currency, $repeat_cost];
                $repeat_text.= "<p>".$break_s.str_replace($repeat_placeholders, $repeat_placeholder_values, $repeat_text_template).$break_e."</p>";
                $count_sec++;
            }

            if(empty($name) || empty($company_name) || empty($expiry_date) || empty($email_address) || empty($first_name) || empty($surname) || empty($block) || empty($number))
            {

                echo "ID: ".$b."\n";
                echo "Name: ".$name."\n";
                echo "Company: ".$company_name."\n";
                echo "Expire: ".$expiry_date."\n";
                echo "Email: ".$email_address."\n";
                continue;
            }

            $purchase_link = '<a style="color:green;" href="'.url('/admin/reserved/lockers/schooltermbasedreserved/'.$b).'"><u>Purchase</u></a>';
            $cancel = '<a style="color:red;" href="'.url('/unsubscribe/'.$b).'"><u>cancel your locker</u></a>';

            $placeholders = ["%name%", "%company_name%", "%purchase%", "%expiry_date%", "%first_name%", "%surname%", "%number%", "%block%", "%start%", "%%end%%", "%currency%", "%cost%", "%cancel%","%repeat_text_plaveholder%"];
            $placeholder_values = [$name, $company_name, $purchase_link, $expiry_date, $first_name, $surname, $number, $block, $start, $end, $currency, $cost, $cancel,$repeat_text];
            $email_html = str_replace($placeholders, $placeholder_values, $template_text);

            $email = new Email();
            //$email->email = 'rperris@xweb4u.com';//$user->email//'sharon@prefectequipment.com';//'dev@xweb4u.com';
            $email->email = $email_address;
            $email->action = 'sent';
            $email->subject = 'Message from Locker Rentals';
            $email->mail_body = $email_html;
            $email->date_added = Carbon::now();
            $email->type = 'email';
            $email->result_code = '0';
            $email->star = '0';
            $email->send = '0';
            $email->complete = '0';
            $email->deleted = '0';
            $email->save();



        }











    }




}
