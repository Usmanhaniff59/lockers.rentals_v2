<?php

namespace App\Console\Commands;

use App\Payment;
use App\Sale;
use App\SalesAudit;
use App\Transaction;
use Carbon\Carbon;
use Illuminate\Console\Command;

class GetSagePayByVendorTxCode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'response:vendorTxCode';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $booking_id = '1711967';
        $vendorTxCode = 'lockers_20201119090355_1711967';

        $payment = Payment::where('booking_id', $booking_id)->first();
        $payment_id = $payment->id;
        $user_id = $payment->user_id;
        $currency = $payment->currency;
        $payment_modal = new Payment();

        $transaction = Transaction::where('booking_id', $booking_id)->where('waiting_response_sagepay', 1)->first();

        if (empty($transaction)) {

            $fraud_score = 0;
            $amount = null;
            $status_code = '2000.xweb';
            $transactionId = null;
            $status_message = 'No Response';
            $waiting_response_sagepay = 1;
            $last4Digits = null;
            $transaction_type = '01';

//            $transaction = new Payment();
//            $transaction_id = $transaction->insert_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $amount, $transaction_type, $status_code, $vendorTxCode, $status_message, $fraud_score, $transactionId, $waiting_response_sagepay);
//
//            $transaction = Transaction::find($transaction_id);

        }

        //get transaction ID from Sage Pay

        $command = 'getTransactionDetail';
        $sage_pay_response = $payment_modal->get_transaction_from_sagepay_using_vendor_tx_code($vendorTxCode, $command);
//        dd($sage_pay_response);
        if (isset($sage_pay_response['vpstxid'])) {
            $transactionId = $sage_pay_response['vpstxid'];

            //Get Trasaction Status From Sage Pay
            $retrieve_transaction_response = $payment_modal->get_transaction_from_sagepay_using_transaction_id($transactionId);

                dd($retrieve_transaction_response);
            if (isset($retrieve_transaction_response->statusCode)) {

                $status_message = $retrieve_transaction_response->statusCode . ':' . $retrieve_transaction_response->statusDetail;
                $statusCode = $retrieve_transaction_response->statusCode;

                //If Paid
                if ($retrieve_transaction_response->statusCode == '0000') {

                    $total_price = $retrieve_transaction_response->amount->totalAmount / 100;

                    $price_sage_pay = new Payment();
                    $price = $price_sage_pay->price_calculation_sagepay_response($booking_id, $total_price);
                    $total_lockers_price = $price[0];
                    $charity_price = $price[1];
                    $total_price = $price[2];

                    $payment->amount = $total_price;
                    $payment->account_balance = $total_price;
                    $payment->has_transaction = 1;
                    $payment->save();

                    $last4Digits = $retrieve_transaction_response->paymentMethod->card->lastFourDigits;

                    //..$transaction-> ....
                    $transaction->transaction_id = $transactionId;
                    $transaction->last4Digits = $last4Digits;
                    $transaction->amount = $total_lockers_price;
                    $transaction->status = $statusCode;
                    $transaction->status_message = $status_message;
                    $transaction->waiting_response_sagepay = 0;
                    $transaction->save();

                    if ($charity_price > 0) {
                        $waiting_response_sagepay = 0;
                        $payment_modal->insert_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $charity_price, '03', $statusCode, $vendorTxCode, $status_message, 0, $transactionId, $waiting_response_sagepay);
                    }

                    //update booking as booked
                    $sales = Sale::where('booking_id', $booking_id)->get();
                    if (!empty($sales)) {
                        foreach ($sales as $sale) {

                            $sale->status = 'b';
                            $sale->save();

                            //add sales_audit log
                            $sales_audit_obj = new SalesAudit();
                            $sales_audit_obj->sales_audit_log($sale->id, $type = 'validate');

                        }
                    }

                }


            }

        }
        $sales = Sale::where('status', 'b')->whereNull('net_pass')->get();

        if (!empty($sales)) {
            foreach ($sales as $sale) {

                $codes = ManualLockerCode::where('locker_id', $sale->locker_id)
                    ->where('start', date("Y-08-31 00:00:00", strtotime($sale->start)))
                    ->where('end', date("Y-09-01 00:00:00", strtotime($sale->end)))
                    ->first();

                if (!empty($codes)) {
                    $sale->net_pass = substr(sprintf("%04d", $codes->code), -4);
                    $sale->save();


                }

                $sale_obj = new Sale();
                $sale_obj->update_locker_code($sale->id);
            }
        }
    }
}
