<?php

namespace App\Console\Commands;

use App\Company;
use App\AcademicYearReport;
use App\Sale;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CompanyAcademicYearReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'academicYear:report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        if(date('m') > 4) {

            $start_year = Carbon::now()->format('Y');
        }else{
            $start_year = Carbon::now()->subYear()->format('Y');
        }

        $sales = Sale::select(DB::raw('YEAR(start) as year'))
            ->where('status', 'b')
            ->whereYear('start','>=' , $start_year)
            ->distinct()
            ->get();

        foreach ($sales as $sale) {

            $calculated_year = $sale->year;
            $last_year = $calculated_year - 1;

            $companies = Company::
            withCount([
                'lockers as total_lockers' => function ($query) {
                    $query->join('blocks','lockers.block_id','=','blocks.id')
                    ->where('blocks.active', 1)->where('blocks.display', 1)
                    ->where('lockers.active', 1)->where('lockers.display', 1);
                },
                'sales as total_current_year_sold' => function ($query) use ($calculated_year) {
                    $query->whereYear('start', $calculated_year)->where('status', 'b');
                },
                'sales as total_last_year_sold' => function ($query) use ($last_year) {
                    $query->whereYear('start',
                        $last_year)->where('status', 'b');
                },

            ])
            ->where('active', 1)->where('display', 1)

            ->get();

            foreach ($companies as $company) {

                $total_available_lockers = 0;
                $lockers_sold_percentage = 0;
                $change_from_last_year_percentage = 0;

                if ($company->total_lockers - $company->total_current_year_sold > 0)
                    $total_available_lockers = $company->total_lockers - $company->total_current_year_sold;

                if ($company->total_lockers > 0)
                    $lockers_sold_percentage = round($company->total_current_year_sold * 100 / $company->total_lockers);

                if ($company->total_current_year_sold > 0 && $company->total_lockers > 0)
                    $change_from_last_year_percentage = round((($company->total_current_year_sold - $company->total_last_year_sold) / $company->total_lockers) * 100);


                $academic_report = AcademicYearReport::where('year', $calculated_year)->where('company_id', $company->id)->first();
                if ($academic_report) {

                    $academic_report->total_lockers = $company->total_lockers;
                    $academic_report->total_lockers_sold = $company->total_current_year_sold;
                    $academic_report->total_lockers_available = $total_available_lockers;
                    $academic_report->lockers_sold_percentage = $lockers_sold_percentage;
                    $academic_report->sold_last_year = $company->total_last_year_sold;
                    $academic_report->change_from_last_year_percentage = $change_from_last_year_percentage;
                    $academic_report->save();
//                    dd($academic_report);
                } else {
                    $add_academic_year_report = new AcademicYearReport();
                    $add_academic_year_report->year = $calculated_year;
                    $add_academic_year_report->company_id = $company->id;
                    $add_academic_year_report->total_lockers = $company->total_lockers;
                    $add_academic_year_report->total_lockers_sold = $company->total_current_year_sold;
                    $add_academic_year_report->total_lockers_available = $total_available_lockers;
                    $add_academic_year_report->lockers_sold_percentage = $lockers_sold_percentage;
                    $add_academic_year_report->sold_last_year = $company->total_last_year_sold;
                    $add_academic_year_report->change_from_last_year_percentage = $change_from_last_year_percentage;
                    $add_academic_year_report->save();
                }

            }
        }
    }
}
