<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\CronSchedule;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class CroneScheduleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:schedule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        dd('cronjschedule');
        $cron_schedules = CronSchedule::where('due','<',Carbon::now())->where('status', 'Pending')->get();

        foreach ($cron_schedules as $cron_schedule){

            if($cron_schedule->class == 'ManualLockerCode'){
                Artisan::call('dumyManualLocker:cron');
                $cron_schedule->status = 'Completed';
                $cron_schedule->save();
            }
        }

    }
}
