<?php

namespace App\Console\Commands;

use App\Sale;
use App\SalesAudit;
use Carbon\Carbon;
use Illuminate\Console\Command;

class LostLockers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lost:lockers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'change sales lockers status to lost';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $current_date_time = Carbon::now()->toDateTimeString();
        $sales = Sale::where('status','r')
            ->where('hold_booking_date', '<', $current_date_time)
            ->get();

        if($sales) {
            foreach ($sales as $sale) {

                $sale_status = Sale::where('id', $sale->id)->first();
                $sale_status->status = 'l';
                $sale_status->save();


                //add sales_audit log
                $sales_audit_obj = new SalesAudit();
                $sales_audit_obj->sales_audit_log($sale->id , $type = 'validate');

                //next year booking
                $update_next_year = Sale::where('hold_sales_id', $sale->id)->first();
                if($update_next_year){
                    $update_next_year->status = 'l';
                    $update_next_year->save();

                    //add sales_audit log
                    $sales_audit_obj = new SalesAudit();
                    $sales_audit_obj->sales_audit_log($update_next_year->id, $type = 'validate');
                }
            }
        }


    }

}
