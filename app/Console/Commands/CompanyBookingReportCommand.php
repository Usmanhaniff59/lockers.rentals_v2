<?php

namespace App\Console\Commands;

use App\CompanyBookingsReport;
use App\Sale;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CompanyBookingReportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'company:booking';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

            $bookings = Sale::select('company_id','user_id',DB::raw("YEAR(start) as year"),DB::raw('count(id) as `booked_companies`'))
                ->groupby('year','user_id','company_id')
                ->where('status','b')
                ->get();
//            Sale::where('')

            foreach ($bookings as $booking){


                $booking_report = CompanyBookingsReport::where('company_id',$booking->id)->where('user_id',$booking->user_id)->first();
               if($booking_report) {

                   $booking_report->bookings = $booking->booked_companies;
                   $booking_report->save();

               }else{

                   $company_booking = new CompanyBookingsReport();
                   $company_booking->company_id = $booking->company_id;
                   $company_booking->user_id = $bookings->user_id;
                   $company_booking->bookings = $booking->booked_companies;
                   $company_booking->save();
               }

            }


        }

}
