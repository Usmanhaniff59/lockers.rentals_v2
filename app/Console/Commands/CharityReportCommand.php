<?php

namespace App\Console\Commands;

use App\CharityReport;
use App\Payment;
use App\Sale;
use App\Transaction;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CharityReportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'charity:report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'charity report provides in euro & gbp';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $currnet_month = Carbon::now()->format('m');
//        $currnet_month = '11';
        $currnet_year = Carbon::now()->format('Y');
//        $currnet_year = '2020';


        //bookings against month & year
        $bookings = Sale::join('payments', 'payments.booking_id', '=', 'sales.booking_id')
            ->join('transactions', 'payments.booking_id', '=', 'transactions.booking_id')
            ->where('sales.status', 'b')
            ->where(function ($query) {
                $query->where('transactions.type', '01')->orWhere('transactions.type', '02')->orWhere('transactions.type', '03')->orWhere('transactions.type', '07');
            })
            ->where('transactions.status', '0000')
            ->whereYear('transactions.created_at', $currnet_year)
            ->whereMonth('transactions.created_at', $currnet_month)
            ->select('sales.booking_id', 'payments.amount', 'payments.currency', 'sales.status', 'sales.start', 'transactions.created_at', 'transactions.type', 'transactions.amount as transaction_amount', 'transactions.refund')
            ->get();

        //-------------------------Declare Variables GBP--------------------------------------
        $charity_transaction_GBP = 0;
        $charity_revenue_GBP = 0;
        $refund_of_booking = 0;
        $total_refund_GBP = 0;
        $payments_GBP = 0;
        $total_payment_GBP = 0;
        $charity_revenue_percentage_GBP = 0;

        //---------------------------Declare Variables EUR-----------------------------------------
        $charity_transaction_EUR = 0;
        $charity_revenue_EUR = 0;
        $refund_of_booking = 0;
        $total_refund_EUR = 0;
        $payments_EUR = 0;
        $total_payment_EUR = 0;
        $charity_revenue_percentage_EUR = 0;

        foreach ($bookings->unique('booking_id') as $booking) {

            //-------------------------Calculate GBP---------------------------------------
            //number of charities of booking GBP
            $charity_GBP = Transaction::where('booking_id', $booking->booking_id)->where('currency', 'GBP')->where('type', '03');
            $charity_count_GBP = $charity_GBP->count();
            $charity_transaction_GBP += $charity_count_GBP;

            //charity sum of booking GBP
            $charity_sum_of_booking_GBP = $charity_GBP->sum('amount');
            $charity_revenue_GBP += $charity_sum_of_booking_GBP;

            //total payments of booking GBP
            $total_payment_of_booking_GBP = Transaction::where('booking_id', $booking->booking_id)->where('currency', 'GBP')->where(function ($query) {
                $query->where('transactions.type', '01')->orWhere('transactions.type', '02')->orWhere('transactions.type', '07');
            })->sum('amount');
            $payments_GBP += $total_payment_of_booking_GBP;

            //total refund of booking GBP
            $refund_of_booking_GBP = Transaction::where('booking_id', $booking->booking_id)->where('currency', 'GBP')->where('transactions.type', '06')->sum('refund');
            $total_refund_GBP += $refund_of_booking_GBP;


            //---------------------------Calculate EUR-----------------------------------------
            //number of charities of booking EUR
            $charity_EUR = Transaction::where('booking_id', $booking->booking_id)->where('currency', 'EUR')->where('type', '03');
            $charity_count_EUR = $charity_EUR->count();
            $charity_transaction_EUR += $charity_count_EUR;

            //charity sum of booking EUR
            $charity_sum_of_booking_EUR = $charity_EUR->sum('amount');
            $charity_revenue_EUR += $charity_sum_of_booking_EUR;

            //total payments of booking EUR
            $total_payment_of_booking_EUR = Transaction::where('booking_id', $booking->booking_id)->where('currency', 'EUR')->where(function ($query) {
                $query->where('transactions.type', '01')->orWhere('transactions.type', '02')->orWhere('transactions.type', '07');
            })->sum('amount');
            $payments_EUR += $total_payment_of_booking_EUR;

            //total refund of booking EUR
            $refund_of_booking_EUR = Transaction::where('booking_id', $booking->booking_id)->where('currency', 'EUR')->where('transactions.type', '06')->sum('refund');
            $total_refund_EUR += $refund_of_booking_EUR;

        }

        //-------------------------Save GBP------------------------------------------
        $payments_GBP = $payments_GBP - $total_refund_GBP;
        $total_payment_GBP = $payments_GBP + $charity_revenue_GBP;

        if ($charity_revenue_GBP > 0 && $total_payment_GBP > 0)
            $charity_revenue_percentage_GBP = round($charity_revenue_GBP * 100 / $total_payment_GBP);
//        $charity_revenue GBP

        //check exist already against month & year GBP
        $charity_report_GBP = CharityReport::where('month', $currnet_month)->where('year', $currnet_year)->where('currency', 'GBP')->first();
        if ($charity_report_GBP) {

            $charity_report_GBP->bookings = $bookings->unique('booking_id')->count();
            $charity_report_GBP->payment = $payments_GBP;
            $charity_report_GBP->charity_transaction = $charity_transaction_GBP;
            $charity_report_GBP->charity_revenue = $charity_revenue_GBP;
            $charity_report_GBP->total_payment = $total_payment_GBP;
            $charity_report_GBP->charity_revenue_percentage = $charity_revenue_percentage_GBP;
            $charity_report_GBP->save();

        }
        else {
            $add_charity_report_GBP = new CharityReport();
            $add_charity_report_GBP->month = $currnet_month;
            $add_charity_report_GBP->year = $currnet_year;
            $add_charity_report_GBP->currency = 'GBP';
            $add_charity_report_GBP->bookings = $bookings->unique('booking_id')->count();
            $add_charity_report_GBP->payment = $payments_GBP;
            $add_charity_report_GBP->charity_transaction = $charity_transaction_GBP;
            $add_charity_report_GBP->charity_revenue = $charity_revenue_GBP;
            $add_charity_report_GBP->total_payment = $total_payment_GBP;
            $add_charity_report_GBP->charity_revenue_percentage = $charity_revenue_percentage_GBP;
            $add_charity_report_GBP->save();
        }



        //-------------------------------------Save EUR---------------------------
       $payments_EUR = $payments_EUR - $total_refund_EUR;
        $total_payment_EUR = $payments_EUR + $charity_revenue_EUR;

        if ($charity_revenue_EUR > 0 && $total_payment_EUR > 0)
            $charity_revenue_percentage_EUR = round($charity_revenue_EUR * 100 / $total_payment_EUR);
//        $charity_revenue EUR

        //check exist already against month & year EUR
        $charity_report_EUR = CharityReport::where('month', $currnet_month)->where('year', $currnet_year)->where('currency', 'EUR')->first();
        if ($charity_report_EUR) {

            $charity_report_EUR->bookings = $bookings->unique('booking_id')->count();
            $charity_report_EUR->payment = $payments_EUR;
            $charity_report_EUR->charity_transaction = $charity_transaction_EUR;
            $charity_report_EUR->charity_revenue = $charity_revenue_EUR;
            $charity_report_EUR->total_payment = $total_payment_EUR;
            $charity_report_EUR->charity_revenue_percentage = $charity_revenue_percentage_EUR;
            $charity_report_EUR->save();

        }
        else {
            $add_charity_report_EUR = new CharityReport();
            $add_charity_report_EUR->month = $currnet_month;
            $add_charity_report_EUR->year = $currnet_year;
            $add_charity_report_EUR->currency = 'EUR';
            $add_charity_report_EUR->bookings = $bookings->unique('booking_id')->count();
            $add_charity_report_EUR->payment = $payments_EUR;
            $add_charity_report_EUR->charity_transaction = $charity_transaction_EUR;
            $add_charity_report_EUR->charity_revenue = $charity_revenue_EUR;
            $add_charity_report_EUR->total_payment = $total_payment_EUR;
            $add_charity_report_EUR->charity_revenue_percentage = $charity_revenue_percentage_EUR;
            $add_charity_report_EUR->save();
        }


    }
}
