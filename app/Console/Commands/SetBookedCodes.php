<?php

namespace App\Console\Commands;

use App\ManualLockerCode;
use App\Sale;
use App\SalesAudit;
use Illuminate\Console\Command;

class SetBookedCodes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'booked:codes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sales = Sale::where('status','b')->where('net_pass','0000')->get();
//        dd($sales);
        foreach ($sales as $sale){
            $manual_code = ManualLockerCode::where('locker_id', $sale->locker_id)->where('start','<=', date('Y-m-d 00:00:00',strtotime($sale->end)))
                ->where('end','>=', date('Y-m-d 00:00:00',strtotime($sale->start)))->first();

            if(!empty($manual_code)) {

                if(isset($manual_code->code)) {
                    $sale->net_pass = substr(sprintf("%04d", $manual_code->code),-4);

                    if ($sale->end >= '2020-09-01 00:00:00')
                        $sale->confirmation_sent = 0;


                    $sale->save();


                    //add sales_audit log
                    $sales_audit_obj = new SalesAudit();
                    $sales_audit_obj->sales_audit_log($sale->id , $type = 'validate');
                }
            }
        }

    }
}
