<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\BlockGroupCode;
use App\LockerNumberReport;
use App\BlockGroup;
use App\Traits\UniqueBlockGroupCode;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
class BlockGroupCodes extends Command
{
    use UniqueBlockGroupCode;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'block:group';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'When a new group is added that is active OR every month run a cron to create codes for the block_group_codes table. Only create if does not exist. Do not ADD.

        locker_number = loop to issue 999 lockers for every group
        start_date && end_date = current logic YYYY-08-31  - YYYY-09-01
        code - random code between 0001 - 9999 (insure leading zero is used)



        After Jan of current year create codes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         $blockGroups = BlockGroup::whereNotExists( function ($query) {
        $query->select(DB::raw(1))
                ->from('block_group_code')
                ->whereRaw('block_group_code.block_group_id = block_groups.id')
                ->where('block_group_code.start_date','>=',  date('Y-09-01 00:00:00'));
      })->where('status',1)->where('delete',0)->get();
      // dd($blockGroups->toArray());

        $last=BlockGroupCode::orderBy('order', 'desc')->first();
        if(!empty($last))
        {
            $order=$last->order;
        }else{
            $order=1;
        }
        $code_counter=1;
         $next_year =Carbon::now()->addYear($code_counter - 1)->format('Y');
                    $start = $next_year.'-09-01 00:00:00';
                    $next_2_year =Carbon::now()->addYear($code_counter)->format('Y');
                    $end = $next_2_year.'-08-31 00:00:00';
        foreach ($blockGroups as $key => $blockgroup) {

            for($i=1;$i<=1000;$i++)
            {
                // $number = sprintf("%04d",$i);
                $order=$order+1;
               $locker_number =  $this->createUniqueGroupCode($blockgroup->id);

                $block_group_code = new BlockGroupCode();
                $block_group_code->order=$order;
                $block_group_code->locker_number=$i;
                $block_group_code->block_group_id =$blockgroup->id;
                $block_group_code->start_date = $start;
                $block_group_code->end_date =$end;
                $block_group_code->code =$locker_number;
                $block_group_code->save();


                // add the report from here 

                // $lockerReport = new LockerNumberReport();
                // $lockerReport->order=$order;
                // $lockerReport->locker_number=$locker_number;
                // $lockerReport->block_group_id=$blockgroup->id;
                // $block_group_code->start_date = $start;
                // $block_group_code->end_date =$end;
                // $lockerReport->save();


            }
          
            # code...
        }
          return true;
    }
}
