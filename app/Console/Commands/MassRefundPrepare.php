<?php

namespace App\Console\Commands;

use App\Sale;
use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Payment;
use App\CronSchedule;
use App\Email;
use App\MassRefunds;
use App\Transaction;
use App\paymentConfig;
use App\RefundTransaction;
use App\User;
use App\EmailTemplate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class MassRefundPrepare extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mass:refundprepare';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Preperation of Mass refunds';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    //php artisan mass:refundprepare
    public function handle()
    {
        echo "hhha";

        $year = '2020-09-01 00:00:00';

        $company_id = '109'; // coopers coburn year 7
        $block_id_array = array('376','377');// academic year id 2; // year 7

      //  $company_id = '86'; // beacon
       // $block_id_array= ''; //all

       // $company_id = '106'; // Beaulieu
       // $block_id_array= ''; //all



        /*
        $c = 0;
        $cc =0;
        $sales = Sale::Where('status','b')
            ->where('start','=',$year)
            ->where('company_id',$company_id)
            ->get();
        foreach ( $sales as $s){


            //code just written to do a one of job need to remove
            if($s->booking_id == '161722' || $s->booking_id == '162116' || $s->booking_id == '162331')
                continue;

            if($s->id == '161722' || $s->id == '162116' || $s->id == '162331')
                continue;

            $cc++;
            $refund = 1;
            $block_name = '';
            $child_name ='';


            if (!empty($block_id_array) && is_array($block_id_array)) {

                $refund = 0;

                if( in_array($s->block_id,$block_id_array)) {

                    $last = date("Y-m-d H:i:s", strtotime('-1 year', strtotime($s->start)));

                    $salesSameLast = Sale::Where('status', 'b')
                        ->where('start', '=', $last)
                        ->where('user_id', $s->user_id)
                        ->WhereIn('block_id', $block_id_array)
                        ->first();
                    if (!empty($salesSameLast->id)) {
                        $refund = 1;
                        $block_name = $salesSameLast->block->name;
                        $child_name =  $salesSameLast->child_first_name." ".$salesSameLast->child_surname;
                    }

                }

            }


            if(!empty($refund)){

                $c++;
                $paymnets = 0;
                $refunds = 0;
                $payment_id = 0;
                $transactions = Transaction::Where('booking_id',$s->booking_id)
                    ->where('status','0000')
                    ->get();
                foreach ($transactions as $t) {

                    $payment_id = $t->payment_id;

                    if($t->status == '0000') {
                        $paymnets = $paymnets + $t->amount;
                        $refunds = $refunds + $t->refund;
                    }

                }



                $total = $paymnets-$refunds;

                if(!empty($total)){

                    $mass_refunds = MassRefunds::Where('booking_id',$s->booking_id)
                        ->where('completed',0)
                        ->first();
                    if(empty($mass_refunds->id)){

                        $mass_refunds = new MassRefunds();
                        $mass_refunds->booking_id = $s->booking_id;
                        $mass_refunds->completed = 0;
                        $mass_refunds->save();

                    }


                    $transaction = Transaction::Where('booking_id',$s->booking_id)
                        ->where('notes','Refund in Bulk')
                        ->first();
                    if(empty($transaction->id)){

                        $transaction = new Transaction();
                        $transaction->booking_id = $s->booking_id;
                        $transaction->payment_id = $payment_id;
                        $transaction->user_id = 2;
                        $transaction->type = '05';
                        $transaction->amount_change = $total;
                        $transaction->notes = 'Refund in Bulk';
                        $transaction->void = 0;
                        $transaction->waiting_response_sagepay = 0;
                        $transaction->created_at = date('Y-m-d H:i:s');
                        $transaction->updated_at = date('Y-m-d H:i:s');
                        $transaction->save();

                    }

                    $thischildsName = $s->child_first_name." ".$s->child_surname;
                   // if(str_replace(' ', '', $thischildsName) != str_replace(' ', '',$child_name)){
                        echo "Sale: " . $s->id . "\n";
                        echo "Locker Number: " . $s->number . "\n";
                        echo "company: " . $s->company->name . "\n";
                        echo "block: " . $s->block->name . "\n";
                        echo "New Block: " . $block_name . "\n";
                        echo "Child_name: " . $s->child_first_name . " " . $s->child_surname . "\n";
                        echo "Child_name last: " . $child_name . "\n";
                        echo "total: " . $total . "\n";
                        echo "\n";
                  //  }

                    $s->status = 'l';
                    $s->save();

                }

                echo "Refund".$refund."\n";


            }


        }

        */

        //echo $c."/".$cc;

    }





}
