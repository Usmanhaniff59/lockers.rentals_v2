<?php


namespace App\Console\Commands;
use App\Locker;
use App\ManualLockerCode;
use App\Company;
use App\SchoolYear;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Traits\UniqueManualCode;
class CreateBlockManualLockerCode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dumyManualLocker:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
  public function handle()
    {
        // 
        //   foreach ($schools as $key => $school) {
                //   $query->where('manual_locker_codes.start',$school->from_date);
                // }
        $current_year = date("Y");
         $month = date("m");
          // if($current_year > 2021 || $month>='09' ){
                  $schools = SchoolYear::select('from_date')->where('to_date','>=',Carbon::now())->groupBy('from_date')->get();
             foreach ($schools as $key => $year) {
                if($key==2){
                 # code...


                
        
        // dd($schools->toArray());
         $companies = Company::with(['blocks'=>function($blockQuery){
                            return $blockQuery->where([['active',1],['display',1]]);
                         },'blocks.lockers'=>function($lockQuery) use ($year){
            return $lockQuery->whereNotExists( function ($Inner_query) use ($year) {
            $query=$Inner_query->select(DB::raw(1))
                ->from('manual_locker_codes')
                ->whereRaw('lockers.id = manual_locker_codes.locker_id')
                    ->where('manual_locker_codes.start',$year->from_date);
           
            })->where([['active',1],['display',1]]);
      }])->where([['active',1],['display',1]])->get();
        foreach ($companies as $key => $company) {
            $lockersId=[];
           
                foreach ($company->blocks as $keyu => $block) {
                   
                    if(count($block->lockers)>0){
                   $checkLocker = new ManualLockerCode();
                     $blockCodes=$checkLocker->block_Codes($block->block_group,$block->lockers,$company->id,$block->id,$year);
                    
                     $d=$checkLocker->LockerBlockcodes($blockCodes,$block->lockers,$block->start_date,$block->end_date,$block->display,$block->id);
                    


                 }
           
                }
               
             
            
            # code...
        }
    }
    }
        return 1;
    // }
        // dd('123');

    }
}
