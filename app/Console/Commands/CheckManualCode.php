<?php

namespace App\Console\Commands;

use App\Locker;
use App\ManualLockerCode;
use App\Company;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Traits\UniqueManualCode;

class CheckManualCode extends Command
{
    use UniqueManualCode;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'manual:code';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check manual code exists of lockers.if not exist then create codes for 2 years';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $current_year = date("Y");
         $month = date("m");
          if($current_year < 2021 && $month < 9 ){
                //  $lockers=Locker::whereNotExists( function ($query) {
                //     $query->select(DB::raw(1))
                //         ->from('manual_locker_codes')
                //         ->whereRaw('lockers.id = manual_locker_codes.locker_id')
                //         ->where('manual_locker_codes.start','>=',  date('Y-09-01 00:00:00'));
                //     })
                //     ->whereNotNull('company_id')->whereNotNull('block_id')->where('active',1)->where('display',1)->get();

                // foreach ($lockers as $locker){
                //     for($code_counter = 1 ; $code_counter<=2 ; $code_counter++){

                //         if($code_counter == 1){
                //             $start = date('Y-09-01 00:00:00');
                //             $next_year =Carbon::now()->addYear()->format('Y');
                //             $end = $next_year.'-08-31 00:00:00';

                //         }else{
                //             $next_year =Carbon::now()->addYear($code_counter - 1)->format('Y');
                //             $start = $next_year.'-09-01 00:00:00';
                //             $next_2_year =Carbon::now()->addYear($code_counter)->format('Y');
                //             $end = $next_2_year.'-08-31 00:00:00';
                //         }

                //        $locker_code =  $this->createUniqueManualCode($locker->block_id);

                //         $code = new ManualLockerCode();
                //         $code->code =$locker_code;
                //         $code->locker_id = $locker->id;
                //         $code->block_id = $locker->block_id;
                //         $code->start =$start;
                //         $code->end =$end;
                //         $code->save();
                //     }
                // }
    }
        // dd('123');

    }


}
