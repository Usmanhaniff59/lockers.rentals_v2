<?php

namespace App\Console\Commands;

use App\BlockYears;
use App\Sale;
use App\SalesAudit;
use App\SchoolYear;
use Illuminate\Console\Command;

class CheckEmptySchoolYear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'empty:schoolYear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sales = Sale::whereNull('school_academic_year_id')->get();
        foreach($sales as $sale){

            if(!empty($sale->block_id)) {
                $school_year = BlockYears::where('block_id', $sale->block_id)->first();
                $sale->school_academic_year_id = $school_year->school_academic_year_id;
                $sale->save();

                //add sales_audit log
                $sales_audit_obj = new SalesAudit();
                $sales_audit_obj->sales_audit_log($sale->id,$type = 'validate');
            }


      }

    }
}
