<?php

namespace App\Console\Commands;

use App\Company;
use App\CurrentOffSaleMaintenance;
use App\Locker;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CurrentOffSaleMaintenanceReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'current:maintenance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        CurrentOffSaleMaintenance::truncate();

        $currnet_date = Carbon::now();
         $maintenance_lockers =  Locker::join('maintenances', function ($join) use ($currnet_date) {
            $join->on('lockers.id', '=', 'maintenances.locker_id')
        ->where('maintenances.active', 1)->whereDate('maintenances.to', '>=', $currnet_date)->whereDate('maintenances.from', '<=', $currnet_date);
        })
             ->select('lockers.id','lockers.company_id','lockers.block_id','maintenances.from','maintenances.type')
             ->get();

                foreach ($maintenance_lockers as $locker){

                    $company = Company::find($locker->company_id);

                    $CurrentOffSaleMaintenance = New CurrentOffSaleMaintenance();
                    $CurrentOffSaleMaintenance->company_id = $locker->company_id;
                    $CurrentOffSaleMaintenance->franchise_id = $company->territory_code;
                    $CurrentOffSaleMaintenance->block_id = $locker->block_id;
                    $CurrentOffSaleMaintenance->locker_id = $locker->id;
                    $CurrentOffSaleMaintenance->start_date = $locker->from;
                    $CurrentOffSaleMaintenance->type = ucwords($locker->type);
                    $CurrentOffSaleMaintenance->save();
                }

                $off_sale_lockers =  Locker::join('locker_off_sales', function ($join) {
                    $join->on('lockers.id', '=', 'locker_off_sales.locker_id')
                    ->where('locker_off_sales.active', 1);
                    })
                     ->select('lockers.id','lockers.company_id','lockers.block_id','locker_off_sales.created_at')
                     ->get();

                foreach ($off_sale_lockers as $locker){

                    $company = Company::find($locker->company_id);

                    $CurrentOffSaleMaintenance = New CurrentOffSaleMaintenance();
                    $CurrentOffSaleMaintenance->company_id = $locker->company_id;
                    $CurrentOffSaleMaintenance->franchise_id = $company->territory_code;
                    $CurrentOffSaleMaintenance->block_id = $locker->block_id;
                    $CurrentOffSaleMaintenance->locker_id = $locker->id;
                    $CurrentOffSaleMaintenance->start_date = $locker->created_at;
                    $CurrentOffSaleMaintenance->type = 'Off Sale';
                    $CurrentOffSaleMaintenance->save();
                }


    }
}
