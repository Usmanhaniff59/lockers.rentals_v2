<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\SchoolYear;
use DateTime;
class SchoolYearApril extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schoolyear:june';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'On 15th June make last year in-active';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {  
         $current_year = date("Y");
         $month = date("m");
         if($month==06){
            $title='01 Sep, '.date('Y', strtotime('-1 year')).' - 31 Aug, '.$current_year;
             $data = SchoolYear::where('title', $title)->update([
                'status' => '0'
            ]);
             return $data;
        }else{
            return 1;
        }
    }
}
