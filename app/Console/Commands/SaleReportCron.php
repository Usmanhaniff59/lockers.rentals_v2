<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Transaction;
use Carbon\Carbon;
use App\SalesReport;

class SaleReportCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:sales';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $transaction = Transaction::query();
        $transaction->where('transactions.created_at', '!=', 'NULL')->where('transactions.currency', '!=', 'NULL')->where(function($q) {
              $q->where('transactions.status', '0000')
                ->orWhere('transactions.status', '0');
          });

        $dates = $transaction->get()->groupBy([function($date) {
            if($date->created_at){
                return Carbon::parse($date->created_at)->format('Y-m-d');
            }
        }, 'currency']);


        foreach ($dates as $date => $currencies) {
            $entry_exists = SalesReport::where('date', $date)->first();
            if(!empty($entry_exists)){
                foreach ($currencies as $currency => $data) {
                    $refund = 0.00;
                    $amount = 0.00;
                    foreach ($data as $key => $value) {
                        if(!is_null($value->refund)){
                            $refund += $value->refund;
                            echo $refund;
                            echo '\n update';
                        }
                        if(!is_null($value->amount)){
                            $amount += $value->amount;
                        }
                    }
                     $update = SalesReport::where('date', $date)->where('currency', $currency)->update([
                        'currency' => $currency,
                        'date' => $date,
                        'sales_count' => count($data),
                        'amount' => $amount,
                        'refund' => $refund,
                    ]);
                }
            } else {
                foreach ($currencies as $currency => $data) {
                    $refund = 0.00;
                    $amount = 0.00;
                    foreach ($data as $key => $value) {
                        if(!is_null($value->refund)){
                            $refund += $value->refund;
                            echo $refund;
                            echo '\n create';
                        }
                        if(!is_null($value->amount)){
                            $amount += $value->amount;
                        }
                    }
                    SalesReport::insert([
                        'currency' => $currency,
                        'date' => $date,
                        'sales_count' => count($data),
                        'amount' => $amount,
                        'refund' => $refund,
                    ]);
                }
            }
        }
    }
}
