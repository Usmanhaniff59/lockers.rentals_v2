<?php

namespace App\Console\Commands;

use App\LockerMove;
use App\Sale;
use Carbon\Carbon;
use Illuminate\Console\Command;

class MoveConfirmationEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'move:confirmation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $process_after = date("Y-m-d 23:59:59", strtotime("-1 day")); 
        $locker_moves =  LockerMove::where('complete',0)->where('created_at','<=',$process_after)->get();
        
        foreach ($locker_moves as $data){

            $sale = Sale::find($data->sale_id);
            $sale->confirmation_sent = 0;
            $sale->save();

            $data->complete = 1;
            $data->save();
        }
    }
}
