<?php

namespace App\Console\Commands;

use App\Block;
use App\Locker;
use App\LockerOffSale;
use App\Maintenance;
use App\Rate;
use App\Sale;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CheckLockersAvailabilityInBlock extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lockers:availability';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'lockers availability manual way give block id in and start date and end date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $block_id = 371;
        $start = '2020-09-01 00:00:00';
        $end = '2021-08-01 23:59:59';

        $book_next_year = false;
        $available_lockers_array['total_available'] = 0;

        $blocks = Block::where('id',$block_id)
            ->where('active',1)
            ->where('display',1)
            ->get();

         foreach ($blocks as $b) {

            $rate = Rate::where(function ($query) use ($start,$end) {
                $query->where('valid_too','>=',$start)
                    ->Where('valid_from','<=',$end);
            })
                ->where('id', $b->rate_id)
                ->first();

            if (!empty($rate)) {

                //get active lockers
                $lockers = Locker::where('block_id', $b->id)
                    ->where('active', 1)
                    ->where('display', 1)
                    ->get();
//                        ->count();
//                dd($lockers);
                foreach ($lockers as $l) {

                    $available = $this->is_locker_available($l->id, $start, $end, $book_next_year);

                    if (!empty($available)) {

//                        dd($available);

                        if (empty($available_lockers_array['total_available']))
                            $available_lockers_array['total_available'] = 1;
                        else
                            $available_lockers_array['total_available']++;

                    }

                }
            }

        }


        echo 'total available : '.$available_lockers_array['total_available'] ."\n";  //
    }

    public function is_locker_available($locker_id,$start,$end,$book_next_year)
    {
        $available = 1;
        $sales = Sale::where(function ($query) use ($start, $end) {
            $query->where('end', '>=', $start)
                ->Where('start', '<=', $end);
        })
            ->where('locker_id', $locker_id)
            ->where(function ($status) {
                $status->where('status', 'b')
                    ->orWhere('status', 'r');
            })
            ->first();

        if (!empty($sales)) {
            $available = 0;
//            echo 'sales ' . $sales->id ."\n";
        }


        $maintenances = Maintenance::where('to', '>=', $start)->Where('from', '<=', $end)
            ->where('locker_id',$locker_id)
            ->where('active', 1)
            ->first();

        if (!empty($maintenances)) {
            $available = 0;
//            echo 'mainte ' . $maintenances->id ."\n";
        }

        $locker_off_sale = LockerOffSale::where('locker_id', $locker_id)->where('active', 1)->first();

        if (!empty($locker_off_sale)) {
            $available = 0;
//            echo 'lockerofsale ' . $locker_off_sale->id ."\n";
        }

        if ($book_next_year == true) {

            $start_next = Carbon::parse($start)->startOfDay()->addYear()->toDateTimeString();
            $end_next = Carbon::parse($end)->endOfDay()->addYear()->toDateTimeString();

            $sales = Sale::where(function ($query) use ($start_next, $end_next) {
                $query->where('end', '>=', $start_next)
                    ->Where('start', '<=', $end_next);
            })
                ->where('locker_id', $locker_id)
                ->where(function ($status) {
                    $status->where('status', 'b')
                        ->orWhere('status', 'r');
                })
                ->first();
//                            dd($sales);

            if (!empty($sales))
                $available = 0;

            $maintenances = Maintenance::where('to', '>=', $start_next)->Where('from', '<=', $end_next)
                ->where('locker_id', $locker_id)
                ->where('active', 1)
                ->first();

            if (!empty($maintenances))
                $available = 0;


        }

        return $available;
    }

}
