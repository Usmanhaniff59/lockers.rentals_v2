<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Locker;
use App\Company;
use App\Maintenance;
use App\OffSaleReport;

use Carbon\Carbon;

class LockerOffSaleReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'offsale:report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $current_month = Carbon::now()->format('m');
        $currnet_date = Carbon::now();

        $current_year = Carbon::now()->format('Y');

        $companies = Company::withCount(['maintenances as maintenance_cleaning' =>function($query) use($currnet_date )  {
            $query->where('type', 'cleaning')->where('active', 1)->whereDate('to', '>=', $currnet_date)->whereDate('from', '<=', $currnet_date);
        },
            'maintenances as maintenance_fixing' => function($query) use($currnet_date )  {
                $query->where('type', 'fixing')->where('active', 1)->whereDate('to', '>=', $currnet_date)->whereDate('from', '<=', $currnet_date);
            } ,
            'lockers as total_active_lockers' => function($query)   {
                $query->where('active', 1);
            },
            'lockers as total_lockers',


        ])
            ->get();

        foreach ($companies as $company){

            $total_off_sale = 0;
            $total_late_off_sale = 0;
            $total_late_maintenance = 0;

            $lockers = Locker::where('company_id',$company->id)
                ->get();

            foreach ($lockers as $locker){

                $offSale = $locker->lockerOffSales()->where('active',1)->first();
                if($offSale){
                    $total_off_sale++;
                    $days = Carbon::parse($offSale->created_at)->diffInDays($currnet_date);
                    if($days > 27){
                        $total_late_off_sale++;
                    }
                }
            }

            $maintenances =  Maintenance::where('company_id',$company->id)->where('active', 1)->whereDate('to', '>=', $currnet_date)->whereDate('from', '<=', $currnet_date)->get();
            foreach ($maintenances as $maintenance){
                $days = Carbon::parse($maintenance->from)->diffInDays($currnet_date);
                if($days > 27){
                    $total_late_maintenance++;
                }

            }

            $off_sale_report = OffSaleReport::where('month',$current_month)->where('year',$current_year)->where('company_id',$company->id)->first();

            if(!empty($off_sale_report)) {

                $off_sale_report->franchisee_id = $company->franchise->id;
                $off_sale_report->total_active_lockers = $company->total_active_lockers;
                $off_sale_report->total_off_sale = $total_off_sale;
                $off_sale_report->maintenance_cleaning = $company->maintenance_cleaning;
                $off_sale_report->maintenance_fixing = $company->maintenance_fixing;
                $off_sale_report->total_lockers = $company->total_lockers;
                $off_sale_report->offsale_late = $total_late_off_sale;
                $off_sale_report->maintenance_late = $total_late_maintenance;
                $off_sale_report->save();

            }else{

                $add_off_sale_report = new OffSaleReport();
                $add_off_sale_report->month = $current_month;
                $add_off_sale_report->year = $current_year;
                $add_off_sale_report->company_id = $company->id;
                $add_off_sale_report->franchisee_id = ($company->franchise ? $company->franchise->id : null);
                $add_off_sale_report->total_active_lockers = $company->total_active_lockers;
                $add_off_sale_report->total_off_sale = $total_off_sale;
                $add_off_sale_report->maintenance_cleaning = $company->maintenance_cleaning;
                $add_off_sale_report->maintenance_fixing = $company->maintenance_fixing;
                $add_off_sale_report->total_lockers = $company->total_lockers;
                $add_off_sale_report->offsale_late = $total_late_off_sale;
                $add_off_sale_report->maintenance_late = $total_late_maintenance;
                $add_off_sale_report->save();

            }

        }
    }
}