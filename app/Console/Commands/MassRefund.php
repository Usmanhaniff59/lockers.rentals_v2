<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Payment;
use App\CronSchedule;
use App\Email;
use App\MassRefunds;
use App\Transaction;
use App\paymentConfig;
use App\RefundTransaction;
use App\User;
use App\EmailTemplate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class MassRefund extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mass:refund';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mass refunds based on mass_refunds table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    //php artisan mass:refund
    public function handle()
    {
        $mass_refund = MassRefunds::where('completed', 0)->first();
       

        if(empty($mass_refund)){
            echo "Nothing to refund";
        } else {
            $transaction = new Transaction();
            $refund_amount = $transaction->getAdjustments($mass_refund->booking_id);
            // return $refund_amount;
            $allPaidTransactions = Transaction::where('booking_id', $mass_refund->booking_id)->where(function($query) {
            return $query->where('type', '01')
                ->orWhere('type', '02')
                ->orWhere('type', '03');
            })->where('status', '0000')->get();
            $Authorization = paymentConfig::integrationKey();

            $refunded = 0;

            $amount_to_refund = $refund_amount * 100;

            foreach ($allPaidTransactions as  $paidTransaction) {

                $retrieve_transaction_url = \Config::get('app.retrieve_transaction_url');
                $retrieve_transaction_url = str_replace("<transactionId>",$paidTransaction->transaction_id,"$retrieve_transaction_url");
                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => $retrieve_transaction_url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_HTTPHEADER => array(
                        "Authorization: Basic $Authorization",
                        "Cache-Control: no-cache"
                    ),
                ));

                $retrieve_transaction_response = curl_exec($curl);
                $retrieve_transaction_response = json_decode($retrieve_transaction_response);
                $err = curl_error($curl);
                curl_close($curl);

                $amounts = get_object_vars($retrieve_transaction_response->amount);
                $totalAmount = $amounts['totalAmount'];
                $previous_refunds = RefundTransaction::where('transaction_id', $paidTransaction->transaction_id)->where('status', 1)->sum('amount');
                $previous_refunds = $previous_refunds * 100;
                $available = $totalAmount - $previous_refunds;

                if($amount_to_refund == 0){
                  $successful = MassRefunds::where('id', $mass_refund->id)->update([
                    'completed' => 1,
                  ]);
                    break;
                }

                if($available != 0){
                    if($amount_to_refund < $available){
                        $available = $amount_to_refund;
                    } else {
                        $available = $available - $refunded;
                    }

                    $return = $this->refund($paidTransaction->payment_id, intval($available), "mass refund", $paidTransaction->transaction_id, $paidTransaction->booking_id, $paidTransaction->last4Digits, $paidTransaction->currency, "mass refund", "off", "on");

                    if($return['success']){
                        $amount_to_refund = $amount_to_refund - $available;
                        $refunded += $available;
                    } else {
                        echo  $return['message'];
                        break;
                    }
                }
            }
             $successful = MassRefunds::where('id', $mass_refund->id)->update([
                'completed' => 1,
            ]);
        }
    }


    public function refund($payment_id, $amount, $notes, $trxid, $booking_id, $last4Digits, $currency, $message, $sendEmail, $updatepayment){

        $Authorization = paymentConfig::integrationKey();

        $vendorTxCode = 'lockers_' . Carbon::now()->format('Ymdhis') . '_' . $booking_id;

        $curl = curl_init();
        $refund_transaction_data = [
            'transactionType' => 'refund',
            'vendorTxCode' => '' . $vendorTxCode . '',
            'referenceTransactionId' => $trxid,
            'amount' => $amount,
            'currency' => $currency,
            'description' => 'lockers Refund Amount',
        ];

       // var_dump($refund_transaction_data);
        $refund_transaction_data = json_encode($refund_transaction_data);
        $refund_url = \Config::get('app.refund_url');
        curl_setopt_array($curl, array(
            CURLOPT_URL => $refund_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $refund_transaction_data,
            CURLOPT_HTTPHEADER => array(
                "Authorization: Basic $Authorization",
                "Cache-Control: no-cache",
                "Content-Type: application/json"
            ),
        ));

        $refund_response = curl_exec($curl);

        $refund_response = json_decode($refund_response);

        $err = curl_error($curl);

        curl_close($curl);
        $value = (array) $refund_response;
        
        var_dump($value["statusCode"]);
        
        if($value["statusCode"] == "5013"){
            var_dump($value["statusCode"]);
                $status_message = $value["statusCode"] . ':' . $value["statusDetail"];

                $this->saveTransaction($booking_id, $payment_id, $vendorTxCode, $currency, $refund_response, $amount, $value["statusCode"], $status_message, 1, $notes);

                return (['success'=> false, 'message' => $refund_response->statusDetail]);
        }
        
        if (isset($refund_response->status)) {
            if ($refund_response->statusCode == '0000') {

                $status_message = $refund_response->statusCode . ':' . $refund_response->statusDetail;

                $this->saveTransaction($booking_id, $payment_id, $vendorTxCode, $currency, $refund_response, $amount, 0000, $status_message, 0, $notes);

                $insert = RefundTransaction::insert([
                    'transaction_id' => $trxid,
                    'amount' => $amount/100,    
                    'status' => 1,
                ]);

                $update_payment = Payment::where('id', $payment_id)->update([
                    'has_transaction' => 1,
                ]);

                if($sendEmail == "on"){
                    $this->sendEmail($payment_id, $booking_id, $amount/100, $currency, $message);
                }

                if($updatepayment == "on"){
                    $paymentRecord = Payment::where('id', $payment_id)->first();
                    $previous_amount = $paymentRecord->amount;
                    $money_refunded = floatval($amount/100);
                    $new_amount = floatval($previous_amount - $money_refunded);

                    $success = Payment::where('id', $payment_id)->update([
                        'account_balance' => $new_amount,
                    ]);
                }

                return (['success'=> true, 'message' => 'Successfully Refunded']);
            }
        } else if (isset($refund_response->code)){
            if($refund_response->code == 1018) {
                echo $refund_response;
                echo "aa";
                $status_message = $refund_response->code . ':' . $refund_response->description;

                $this->saveTransaction($booking_id, $payment_id, $vendorTxCode, $currency, $refund_response, $amount, $refund_response->code, $status_message, 1, $notes);

                return (['success'=> false, 'message' => $refund_response->description]);
            }
            
        }
        
        else if(isset($refund_response->statusCode) && $refund_response->statusCode != 0000){
                echo $refund_response;
                echo "aa";
                $status_message = $refund_response->code . ':' . $refund_response->statusDetail;

                $this->saveTransaction($booking_id, $payment_id, $vendorTxCode, $currency, $refund_response, $amount, $refund_response->statusCode, $status_message, 1, $notes);

                return (['success'=> false, 'message' => $refund_response->statusDetail]);
            }
        else {

            $status_message = 'Refund failed!';
            echo $refund_response;

            $this->saveTransaction($booking_id, $payment_id, $vendorTxCode, $currency, $refund_response, $amount, 1, $status_message, 1, $notes);

            return (['success'=> false, 'message' => $status_message]);
        }
    }

    public function saveTransaction($booking_id, $payment_id, $vendorTxCode, $currency, $refund_response, $amount, $status, $status_message, $fraud_score, $notes){

        $last4Digits = "";
        $transaction_id = "";
        if(isset($refund_response->paymentMethod)){
            $last4Digits = $refund_response->paymentMethod->card->lastFourDigits;
            $transaction_id = $refund_response->transactionId;
        }
        $transaction = new Transaction();
        $transaction->booking_id = $booking_id;
        $transaction->payment_id = $payment_id;
        $transaction->vendor_tx_code = $vendorTxCode;
        $transaction->user_id = 2;
        $transaction->currency = $currency;
        $transaction->last4Digits = $last4Digits;
        $transaction->refund = $amount/100;
        $transaction->type = '06';
        $transaction->status = $status;
        $transaction->status_message = $status_message;
        $transaction->fraud_score = $fraud_score;
        $transaction->transaction_id = $transaction_id;
        $transaction->notes = $notes;
        $transaction->save();

        return true;
    }

    public function sendEmail($payment_id, $booking_id, $amount, $currency, $message){

        $payment = Payment::where('id', $payment_id)->first();
        $user = User::where('id', $payment->user_id)->first();
        $current_date_time = Carbon::now()->toDateTimeString();
        $transaction = new Transaction();
        $transactions = $transaction->getOutstaningBalance($booking_id);

        $balance = '0';

        if(isset($transactions->first()->account_balance)){
            $balance =  $transactions->first()->account_balance;
        } else {
            $balance = '0';
        }

        $request_email_template = EmailTemplate::where('id',10)->first();
        $to = $user->email;
        //$to = 'rperris@xweb4u.com';
        $subject = $request_email_template->subject;
        $template_text = $request_email_template->template_text;

        $placeholders = ["%name%", "%booking_id%", "%amount%", "%date%", "%balance%", "%currency%", "%message%"];
        $placeholder_values   = [$user->name, $booking_id, $amount,  date("F jS, Y", strtotime($current_date_time)), $balance,$currency, $message];
        $email_html = str_replace($placeholders, $placeholder_values, $template_text);
        $sms_body =null;
        $phone_number =null;

        $sale = new Sale();
        $sale->saveEmailForCron($to,$phone_number, $subject, $email_html,$sms_body);

    }
}
