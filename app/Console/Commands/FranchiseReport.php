<?php

namespace App\Console\Commands;

use App\Franchisee;
use App\Franchisee_report;
use App\FranchiseeCompany;
use App\StatusLog;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class FranchiseReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'franchise:report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $currnet_month = Carbon::now()->format('m');
        $last_mont = Carbon::now()->subMonth()->format('m');

        $currnet_year = Carbon::now()->format('Y');

        if($currnet_month > 01) {
            $last_year = Carbon::now()->format('Y');
        }else{
            $last_year = Carbon::now()->subYear()->format('Y');

        }

    $franchisee_groups = FranchiseeCompany::
            distinct('franchisee_id')->get();

    foreach ($franchisee_groups as $franchisee_group){
        $franchisee_companies = FranchiseeCompany::with('company')->where('franchisee_id',$franchisee_group->franchisee_id)->get();
       $total_cutomers = 0;
       $total_prospective = 0;
       $total_leads = 0;

        foreach ($franchisee_companies as $franchisee_company) {
//            dd($franchisee_company->company->status_type);
            if($franchisee_company->company) {
                if ($franchisee_company->company->status_type == 'company') {
//                dd('dd');
                    $total_cutomers++;
                } else if ($franchisee_company->company->status_type == 'prospective') {
                    $total_prospective++;
                } else if ($franchisee_company->company->status_type == 'leads') {
                    $total_leads++;
                }
            }
        }
//dd($total_cutomers);
        $total = $total_cutomers + $total_prospective + $total_leads;
        $franchisee_report = Franchisee_report::where('month',$currnet_month)->where('year',$currnet_year)->where('franchisee_id',$franchisee_group->franchisee_id)->first();
        if($franchisee_report){

            $last_mont_franchisee_report = Franchisee_report::where('month',$last_mont)->where('year',$last_year)->where('franchisee_id',$franchisee_group->franchisee_id)->first();

            if($last_mont_franchisee_report) {
                $current_month_new_lead = 0;
                $current_month_new_prospective = 0;
                $current_month_new_customer = 0;
                if($total_cutomers - $last_mont_franchisee_report->customer > 0){
                    $current_month_new_customer = $total_cutomers - $last_mont_franchisee_report->customer;
                }
                if($total_leads - $last_mont_franchisee_report->lead > 0){
                    $current_month_new_lead = $total_leads - $last_mont_franchisee_report->lead;
                }
                if($total_prospective - $last_mont_franchisee_report->prospective > 0){
                    $current_month_new_prospective = $total_prospective - $last_mont_franchisee_report->prospective;
                }

                $franchisee_report->new_customer = $current_month_new_customer ;
                $franchisee_report->new_lead = $current_month_new_lead ;
                $franchisee_report->new_prospective = $current_month_new_prospective ;
                $franchisee_report->customer = $total_cutomers;
                $franchisee_report->lead = $total_leads;
                $franchisee_report->prospective = $total_prospective;
                $franchisee_report->total = $total;
                $franchisee_report->save();

            }else{

                $franchisee_report->new_customer = $total_cutomers;
                $franchisee_report->new_lead = $total_leads;
                $franchisee_report->new_prospective = $total_prospective;
                $franchisee_report->customer = $total_cutomers;
                $franchisee_report->lead = $total_leads;
                $franchisee_report->prospective = $total_prospective;
                $franchisee_report->total = $total;
                $franchisee_report->save();
            }
        }else{

            $last_mont_franchisee_report = Franchisee_report::where('month',$last_mont)->where('year',$last_year)->where('franchisee_id',$franchisee_group->franchisee_id)->first();

            if($last_mont_franchisee_report) {

                $current_month_new_customer = 0;
                $current_month_new_prospective = 0;
                $current_month_new_lead = 0;

                if($total_cutomers - $last_mont_franchisee_report->customer > 0){
                    $current_month_new_customer = $total_cutomers - $last_mont_franchisee_report->customer;
                }
                if($total_leads - $last_mont_franchisee_report->lead > 0){
                    $current_month_new_lead = $total_leads - $last_mont_franchisee_report->lead;
                }
                if($total_prospective - $last_mont_franchisee_report->prospective > 0){
                    $current_month_new_prospective = $total_prospective - $last_mont_franchisee_report->prospective;
                }

                $add_franchisee_report = new Franchisee_report();
                $add_franchisee_report->month = $currnet_month;
                $add_franchisee_report->year = $currnet_year;
                $add_franchisee_report->franchisee_id = $franchisee_group->franchisee_id;
                $add_franchisee_report->new_customer = $current_month_new_customer ;
                $add_franchisee_report->new_lead = $current_month_new_lead ;
                $add_franchisee_report->new_prospective = $current_month_new_prospective ;
                $add_franchisee_report->customer = $total_cutomers;
                $add_franchisee_report->lead = $total_leads;
                $add_franchisee_report->prospective = $total_prospective;
                $add_franchisee_report->total = $total;
                $add_franchisee_report->save();

            }else{

                $add_franchisee_report = new Franchisee_report();
                $add_franchisee_report->month = $currnet_month;
                $add_franchisee_report->year = $currnet_year;
                $add_franchisee_report->franchisee_id = $franchisee_group->franchisee_id;
                $add_franchisee_report->new_customer = $total_cutomers;
                $add_franchisee_report->new_lead = $total_leads;
                $add_franchisee_report->new_prospective = $total_prospective;
                $add_franchisee_report->customer = $total_cutomers;
                $add_franchisee_report->lead = $total_leads;
                $add_franchisee_report->prospective = $total_prospective;
                $add_franchisee_report->total = $total;
                $add_franchisee_report->save();
            }
        }

    }

    }
}