<?php

namespace App\Console\Commands;

use App\CompanyBlockError;
use App\Payment;
use App\SagePayBalance;
use App\SagePayTransaction;
use App\Sale;
use App\Transaction;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SagepayTransactionList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transaction:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

//        for($i=0; $i<12; $i++) {

//            $date =  Carbon::createFromFormat('d/m/Y', '20/11/2020');
//            $date = $date->startOfDay()->addDays($i);
            $date = Carbon::now()->subDays(1);

            $command = 'getTransactionList';
            $row_start = 1;
            $row_end = 50;
            $status = true;
            $payment_modal = new Payment();
            $utc_date =  $date->startOfDay()->toDateTimeString();

            do {
                $start = $date->startOfDay()->format('d/m/Y H:i:s');
                $end = $date->endOfDay()->format('d/m/Y H:i:s');
                //echo $start,"\n";

                $sage_pay_transaction_lists = $payment_modal->get_transaction_list_from_sagepay($command, $start, $end, $row_start, $row_end);
//            dd($sage_pay_transaction_lists);

                if (isset($sage_pay_transaction_lists['transactions'])) {

                    if ($row_start <= $sage_pay_transaction_lists['transactions']['totalrows']) {

                        //create sage pay balance
                        $sage_pay_balance_id = $this->sage_pay_balance($utc_date, $sage_pay_transaction_lists['transactions']['startrow'], $sage_pay_transaction_lists['transactions']['endrow']);

                        echo 'Total record :' . $sage_pay_transaction_lists['transactions']['totalrows'] . "\n";

                        $transaction_lists = $sage_pay_transaction_lists['transactions']['transaction'];


                        //check sagepay response is in multi array or single array
                        if (isset($transaction_lists[0])) {

                            foreach ($transaction_lists as $transaction_list) {

                                $sage_pay_transactionId = $transaction_list['vpstxid'];
                                $vendorTxCode = $transaction_list['vendortxcode'];
                                $vendorTxCode_split = explode('_', $vendorTxCode);
                                $booking_id = $vendorTxCode_split[2];

                                //Get Transaction Status From Sage Pay
                                $sage_pay_response_by_transaction_id = $payment_modal->get_transaction_from_sagepay_using_transaction_id($sage_pay_transactionId);

                                //response handling from sage pay
                                $this->sage_pay_response_by_transactionID_handling($sage_pay_response_by_transaction_id, $transaction_list, $booking_id, $vendorTxCode, $sage_pay_transactionId, $sage_pay_balance_id);

                            }

                        } else {

                            $transaction_list = $transaction_lists;
                            $sage_pay_transactionId = $transaction_list['vpstxid'];
                            $vendorTxCode = $transaction_list['vendortxcode'];
                            $vendorTxCode_split = explode('_', $vendorTxCode);
                            $booking_id = $vendorTxCode_split[2];

                            //Get Transaction Status From Sage Pay
                            $sage_pay_response_by_transaction_id = $payment_modal->get_transaction_from_sagepay_using_transaction_id($sage_pay_transactionId);

                            //response handling from sage pay
                            $this->sage_pay_response_by_transactionID_handling($sage_pay_response_by_transaction_id, $transaction_list, $booking_id, $vendorTxCode, $sage_pay_transactionId, $sage_pay_balance_id);

                        }

                        if ($sage_pay_transaction_lists['transactions']['totalrows'] <= $row_end) {
                            $status = false;
                        }
                        $row_start = $row_end + 1;
                        $row_end = $row_end + 50;

                    } else {
                        $status = false;
                    }
                } else {
                    echo 'transactions not set ' . "\n";
                    print_r($sage_pay_transaction_lists);
                    $status = false;
                }

            } while ($status == true);
//        }
    }


    public function sage_pay_response_by_transactionID_handling($sage_pay_response_by_transaction_id, $transaction_list, $booking_id, $vendorTxCode, $sage_pay_transactionId , $sage_pay_balance_id)
    {

        $payment_modal = new Payment();

        if (isset($sage_pay_response_by_transaction_id->statusCode)) {

            $status_message = $sage_pay_response_by_transaction_id->statusCode . ':' . $sage_pay_response_by_transaction_id->statusDetail;
            $status_code = $sage_pay_response_by_transaction_id->statusCode;

            $booking_id_in_payments = Payment::where('booking_id', $booking_id)->first();

            $last4digits = null;

            if(isset($transaction_list['last4digits']))
                $last4digits = $transaction_list['last4digits'];

            $payment_id = $booking_id_in_payments->id;
            $user_id = $booking_id_in_payments->user_id;
            $currency = $transaction_list['currency'];
            $amount = $transaction_list['amount'];
            $transaction_type = '02';
            $fraud_score = 0;
            $waiting_response_sagepay = 0;

            if ($booking_id_in_payments) {

                $transaction_exist_check = Transaction::where('booking_id', $booking_id)->where('vendor_tx_code', $transaction_list['vendortxcode'])
                    ->where(function ($q) use ($status_code){
                        $q->where('status', $status_code)->orWhere('status', '0');
                    });

                if($transaction_list['transactiontype'] == 'Refund') {
                    $transaction_exist_check->where(function ($query) {
                        $query->where('type', '06');
                    });

                }else{
                    $transaction_exist_check->where(function ($query) {
                        $query->where('type', '01')->orWhere('type', '02')->orWhere('type', '03')->orWhere('type', '07');
                    });
                }
                $transaction_exist = $transaction_exist_check->first();
//                dd($transaction_exist);


                if (!$transaction_exist) {

                    if ($sage_pay_response_by_transaction_id->statusCode == '0000') {

//                            dd($sage_pay_response_by_transaction_id->statusCode);
                        $user_want_to_bookings = Sale::where('booking_id', $booking_id)->where('status', 'l')->get();

                        foreach ($user_want_to_bookings as $user_want_to_booking) {
                            if (isset($user_want_to_booking->locker_id)) {

                                $someone_else_already_booked = Sale::where('start', $user_want_to_booking->start)->where('end', $user_want_to_booking->end)->where('locker_id', $user_want_to_booking->locker_id)->where('booking_id', '<>', $user_want_to_booking->booking_id)->first();

                                if ($someone_else_already_booked) {

                                    $detail = 'Booking ID ' . $user_want_to_booking->booking_id . ' advised refund as booking is lost but has a payment on the account';

                                    echo $detail . "\n";

                                    $errors = CompanyBlockError::where('details', $detail)->where('resolved', 0)->first();

                                    if (!$errors) {
                                        $error = new CompanyBlockError();
                                        $error->company_id = $user_want_to_booking->company_id;
                                        $error->block_id = $user_want_to_booking->block_id;
                                        $error->error_type = 'Paid but Booked/Reserved by someone else';
                                        $error->details = $detail;
                                        $error->save();
                                    }

                                    $total_price = $amount;
//
                                    $price = $payment_modal->price_calculation_sagepay_response($booking_id, $total_price);
                                    $total_lockers_price = $price[0];
                                    $charity_price = $price[1];
                                    $total_price = $price[2];
//
                                    $booking_id_in_payments->amount = $total_price;
                                    $booking_id_in_payments->account_balance = $total_price;
                                    $booking_id_in_payments->has_transaction = 1;
                                    $booking_id_in_payments->save();

                                    //create transaction
                                    $transaction_id = $payment_modal->insert_transactions($booking_id, $payment_id, $user_id, $currency, $last4digits, $total_lockers_price, $transaction_type, $status_code, $vendorTxCode, $status_message, $fraud_score, $sage_pay_transactionId, $waiting_response_sagepay);

                                    //create sage_pay_transaction with exist false
                                    $exist = 0;
                                    $sage_pay_transaction_table_id = $this->sage_pay_transactions($sage_pay_balance_id, $sage_pay_transactionId, $exist);

                                    //update transaction with sage pay transaction id
                                    $this->update_transaction_sage_pay_id_in_transaction($transaction_id, $sage_pay_transaction_table_id);

                                    if ($charity_price > 0) {

                                        $waiting_response_sagepay = 0;
                                        $transaction_id = $payment_modal->insert_transactions($booking_id, $payment_id, $user_id, $currency, $last4digits, $charity_price, '03', $status_code, $vendorTxCode, $status_message, 0, $sage_pay_transactionId, $waiting_response_sagepay);

                                        //update transaction with sage pay transaction id
                                        $this->update_transaction_sage_pay_id_in_transaction($transaction_id, $sage_pay_transaction_table_id);
                                    }

                                }
                                else {

                                    $detail = 'Status changed to booked against  Booking ID ' . $user_want_to_booking->booking_id . ' , locker ID ' . $user_want_to_booking->locker_id . ' from ' . Carbon::parse($user_want_to_booking->start)->format('d M, Y') . ' - ' . Carbon::parse($user_want_to_booking->end)->format('d M, Y') . ' and user has paid ' . $transaction_list['amount'];

                                    echo $detail . "\n";

                                    $total_price = $amount;
//
                                    $price = $payment_modal->price_calculation_sagepay_response($booking_id, $total_price);
                                    $total_lockers_price = $price[0];
                                    $charity_price = $price[1];
                                    $total_price = $price[2];
//
                                    $booking_id_in_payments->amount = $total_price;
                                    $booking_id_in_payments->account_balance = $total_price;
                                    $booking_id_in_payments->has_transaction = 1;
                                    $booking_id_in_payments->save();
//
//
                                    $user_want_to_booking->status = 'b';
//                                    $user_want_to_booking->confirmation_sent = 0;
                                    $user_want_to_booking->save();

                                    //create transaction
                                    $transaction_id = $payment_modal->insert_transactions($booking_id, $payment_id, $user_id, $currency, $last4digits, $total_lockers_price, $transaction_type, $status_code, $vendorTxCode, $status_message, $fraud_score, $sage_pay_transactionId, $waiting_response_sagepay);

                                    //create sage_pay_transaction with exist false
                                    $exist = 0;
                                    $sage_pay_transaction_table_id = $this->sage_pay_transactions($sage_pay_balance_id, $sage_pay_transactionId, $exist);

                                    //update transaction with sage pay transaction id
                                    $this->update_transaction_sage_pay_id_in_transaction($transaction_id, $sage_pay_transaction_table_id);

                                    if ($charity_price > 0) {

                                        $waiting_response_sagepay = 0;
                                        $transaction_id = $payment_modal->insert_transactions($booking_id, $payment_id, $user_id, $currency, $last4digits, $charity_price, '03', $status_code, $vendorTxCode, $status_message, 0, $sage_pay_transactionId, $waiting_response_sagepay);

                                        //update transaction with sage pay transaction id
                                        $this->update_transaction_sage_pay_id_in_transaction($transaction_id, $sage_pay_transaction_table_id);
                                    }
                                }

                            } else {
                                echo 'booking ID ' . $booking_id . ' paid but not selected locker' . "\n";
                            }
                        }
                    } else {



                        echo 'Recorded Failure log. Vendor tx code ' . $vendorTxCode . ' booking Id ' . $booking_id .' status '.$sage_pay_response_by_transaction_id->statusCode. "\n";
                        //create transaction
                        $transaction_id = $payment_modal->insert_transactions($booking_id, $payment_id, $user_id, $currency, $last4digits, $amount, $transaction_type, $status_code, $vendorTxCode, $status_message, $fraud_score, $sage_pay_transactionId, $waiting_response_sagepay);

                        //create sage_pay_transaction with exist false
                        $exist = 0;
                        $sage_pay_transaction_table_id = $this->sage_pay_transactions($sage_pay_balance_id, $sage_pay_transactionId, $exist);

                        //update transaction with sage pay transaction id
                        $this->update_transaction_sage_pay_id_in_transaction($transaction_id, $sage_pay_transaction_table_id);

                    }


                } else {
                    $exist = 1;
                    $sage_pay_transaction_table_id = $this->sage_pay_transactions($sage_pay_balance_id, $sage_pay_transactionId, $exist);
//                        dd($sage_pay_transaction_table_id);
                    $transaction_exist_check = Transaction::where('booking_id', $booking_id)->where('vendor_tx_code', $transaction_list['vendortxcode'])
                        ->where(function ($q) use ($status_code){
                            $q->where('status', $status_code)->orWhere('status', '0');
                        });

                    if($transaction_list['transactiontype'] == 'Refund') {
                        $transaction_exist_check->where(function ($query) {
                            $query->where('type', '06');
                        });

                    }else{
                        $transaction_exist_check->where(function ($query) {
                            $query->where('type', '01')->orWhere('type', '02')->orWhere('type', '03')->orWhere('type', '07');
                        });
                    }
                    $transaction_exist_gets = $transaction_exist_check->get();
//                    dd($transaction_exist_gets);

                    foreach ($transaction_exist_gets as $transaction_exist_get) {

                        if(!isset($transaction_exist_get->transaction_id))
                            $transaction_exist_get->transaction_id = $sage_pay_transactionId;

                        $transaction_exist_get->sage_pay_transaction_id = $sage_pay_transaction_table_id;
                        $transaction_exist_get->save();
                    }
                }

            }else{
                echo 'payment not exist'."\n";
                echo 'booking Id '. $booking_id . ' vendortxcode '.$vendorTxCode . ' status '.$status_code;
            }

        }
        else{

            echo 'statusCode not exist. vendorTxCdoe '.$vendorTxCode.' booking ID '.$booking_id."\n";

            $status_message = $sage_pay_response_by_transaction_id->description;
            $status_code = $sage_pay_response_by_transaction_id->code;

            $booking_id_in_payments = Payment::where('booking_id', $booking_id)->first();

             $last4digits = null;

            if(isset($transaction_list['last4digits']))
                $last4digits = $transaction_list['last4digits'];

            $payment_id = $booking_id_in_payments->id;
            $user_id = $booking_id_in_payments->user_id;
            $currency = $transaction_list['currency'];
             $amount = $transaction_list['amount'];
            $transaction_type = '02';
            $fraud_score = 0;
            $waiting_response_sagepay = 0;

            if ($booking_id_in_payments) {

                $transaction_exist_check = Transaction::where('booking_id', $booking_id)->where('vendor_tx_code', $transaction_list['vendortxcode'])
                    ->where(function ($q) use ($status_code){
                        $q->where('status', $status_code)->orWhere('status', '0');
                    });

                if($transaction_list['transactiontype'] == 'Refund') {
                    $transaction_exist_check->where(function ($query) {
                        $query->where('type', '06');
                    });

                }else{
                    $transaction_exist_check->where(function ($query) {
                        $query->where('type', '01')->orWhere('type', '02')->orWhere('type', '03')->orWhere('type', '07');
                    });
                }
                $transaction_exist = $transaction_exist_check->first();

                if (!$transaction_exist) {

                    //create transaction
                    $transaction_id = $payment_modal->insert_transactions($booking_id, $payment_id, $user_id, $currency, $last4digits, $amount, $transaction_type, $status_code, $vendorTxCode, $status_message, $fraud_score, $sage_pay_transactionId, $waiting_response_sagepay);

                    //create sage_pay_transaction with exist false
                    $exist = 0;
                    $sage_pay_transaction_table_id = $this->sage_pay_transactions($sage_pay_balance_id, $sage_pay_transactionId, $exist);
//
                    //update transaction with sage pay transaction id
                    $this->update_transaction_sage_pay_id_in_transaction($transaction_id, $sage_pay_transaction_table_id);


                } else {
                    $exist = 1;
                    $sage_pay_transaction_table_id = $this->sage_pay_transactions($sage_pay_balance_id, $sage_pay_transactionId, $exist);

                    $transaction_exist_check = Transaction::where('booking_id', $booking_id)->where('vendor_tx_code', $transaction_list['vendortxcode'])
                        ->where(function ($q) use ($status_code){
                            $q->where('status', $status_code)->orWhere('status', '0');
                        });

                    if($transaction_list['transactiontype'] == 'Refund') {
                        $transaction_exist_check->where(function ($query) {
                            $query->where('type', '06');
                        });

                    }else{
                        $transaction_exist_check->where(function ($query) {
                            $query->where('type', '01')->orWhere('type', '02')->orWhere('type', '03')->orWhere('type', '07');
                        });
                    }
                    $transaction_exist_gets = $transaction_exist_check->get();
//                    dd($transaction_exist_gets);

                    foreach ($transaction_exist_gets as $transaction_exist_get) {

                        if(!isset($transaction_exist_get->transaction_id))
                            $transaction_exist_get->transaction_id = $sage_pay_transactionId;

                        $transaction_exist_get->sage_pay_transaction_id = $sage_pay_transaction_table_id;
                        $transaction_exist_get->save();
                    }
                }

            }
            else {
                echo 'payment not exist' . "\n";
                echo 'booking Id ' . $booking_id . ' vendortxcode ' . $vendorTxCode . ' status ' . $status_code;
            }
        }
    }

    public function sage_pay_balance($utc_date, $row_start, $row_end)
    {
        $sage_pay_balance = new SagePayBalance();
        $sage_pay_balance->date = $utc_date;
        $sage_pay_balance->start_transaction_no = $row_start;
        $sage_pay_balance->end_transaction_no = $row_end;
        $sage_pay_balance->save();

        return $sage_pay_balance->id;
    }

    public function sage_pay_transactions($sage_pay_balance_id, $transaction_id, $exist)
    {

        $sage_pay_transaction = new SagePayTransaction();
        $sage_pay_transaction->sage_pay_balance_id = $sage_pay_balance_id;
        $sage_pay_transaction->transaction_id = $transaction_id;
        $sage_pay_transaction->exist = $exist;
        $sage_pay_transaction->save();


        return $sage_pay_transaction->id;
    }

    public function update_transaction_sage_pay_id_in_transaction($transaction_id, $sage_pay_transaction_table_id)
    {

        $transaction = Transaction::find($transaction_id);
        $transaction->sage_pay_transaction_id = $sage_pay_transaction_table_id;
        $transaction->save();

    }

}
