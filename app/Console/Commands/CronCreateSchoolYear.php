<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\SchoolYear;
use DateTime;
use Carbon\Carbon;
class CronCreateSchoolYear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'createyear:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'add to current year';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $month = date("m");
         if($month==4){
           
              $schoolsYear = SchoolYear::select('from_date')->where('status',1)->orderBy('id','desc')->first();
             if(!empty($schoolsYear) && isset($schoolsYear))
             {
                $current_year=date('Y', strtotime($schoolsYear->from_date));
                $nextYear=$current_year+1;
                $double_nextYear=$current_year+2;
                $thirdYear=$current_year+3;


             }else{
                 $current_year = date("Y");
                 $nextYear=$current_year+1;
                $double_nextYear=$current_year+2;
                $thirdYear=$current_year+3;

             }
         


             $year=SchoolYear::where('title','01 Sep, '.$nextYear.' - 31 Aug, '.$double_nextYear)->first();

             if(!isset($year) && empty($year)){
            $data = SchoolYear::insert([
                'title' => '01 Sep, '.$nextYear.' - 31 Aug, '.$double_nextYear,
                'status' => 1,
                'from_date' =>  new DateTime($nextYear.'-09-01'),
                'to_date' =>  $double_nextYear.'-08-31  23:59:59',
                'go_live' =>  Carbon::now(),

            ]);
            }else {
                $year->status=1;
                $year->save();
            }
          
              $data = SchoolYear::insert([
                'title' => '01 Sep, '.$double_nextYear.' - 31 Aug, '.$thirdYear,
                'status' => 0,
                'from_date' =>  $double_nextYear.'-09-01 00:00:00',
                'to_date' =>  $thirdYear.'-08-31  23:59:59',
            ]);
            return $data;
        }else{
            return 1;
        }
    }
}
