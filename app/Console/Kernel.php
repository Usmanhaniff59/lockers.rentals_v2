<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\SendEmailCron',
        'App\Console\Commands\FetchEmailsImap',
        'App\Console\Commands\CronCreateSchoolYear',
        'App\Console\Commands\SendHoldLockers',
        'App\Console\Commands\LostLockers',
        'App\Console\Commands\SendConfirmationEmail',
        'App\Console\Commands\SagePayResponseStatus',
        'App\Console\Commands\SaleReportCron',
        'App\Console\Commands\SendRebookingEmail',
        'App\Console\Commands\MassRefund',
        'App\Console\Commands\CheckManualCode',
        'App\Console\Commands\MoveConfirmationEmail',
        'App\Console\Commands\FranchiseReport',
        'App\Console\Commands\CompanyAcademicYearReport',
        'App\Console\Commands\CroneScheduleCommand',
        'App\Console\Commands\SagepayTransactionList',
        'App\Console\Commands\CheckBookings',
        'App\Console\Commands\CharityReportCommand',
        'App\Console\Commands\SchoolYearApril',
        'App\Console\Commands\BlockGroupCodes',
        'App\Console\Commands\ManualLockerCodes',
        'App\Console\Commands\CreateBlockManualLockerCode',
        'App\Console\Commands\ActivateYear',
        
        
        


    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {   
        $schedule->command('lost:lockers')->everyMinute();
        $schedule->command('send:emails')->everyMinute();
        $schedule->command('sagepay:status')->everyMinute();
        $schedule->command('cron:schedule')->everyMinute();
         $schedule->command('activate:year')->everyMinute();
        
        $schedule->command('confirmation:emails')->everyFifteenMinutes();
        $schedule->command('report:sales')->twiceDaily(1, 13);
        $schedule->command('send:rebooking')->everyThirtyMinutes();
        $schedule->command('manual:code')->monthlyOn(1, '00:00');
        $schedule->command('codes:manual')->monthlyOn(1, '00:00');
        
        $schedule->command('move:confirmation')->dailyAt('00:00');
        $schedule->command('charity:report')->dailyAt('03:30');
        $schedule->command('franchise:report')->dailyAt('07:00');
        $schedule->command('check:bookings')->dailyAt('05:00');
        $schedule->command('academicYear:report')->dailyAt('03:00');
        $schedule->command('createyear:cron')->monthly();
        $schedule->command('schoolyear:june')->monthlyOn(15, '00:00');
        $schedule->command('block:group')->everyMinute();
        
       $schedule->command('transaction:list')->dailyAt('04:00');
         // $schedule->command('codes:manual')->everyMinute();
         // $schedule->command('createyear:cron')->everyMinute();
         // $schedule->command('manual:code')->everyMinute();

    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        
        $this->load(__DIR__.'/Commands');
        require base_path('routes/console.php');
    }
}
