<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CharityReport extends Model
{
    protected $fillable = [
        'year','month', 'currency','bookings','payment','charity_transaction', 'charity_revenue','total_payment','charity_revenue_percentage', 'charity_processed'
    ];
}
