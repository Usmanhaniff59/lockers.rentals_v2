<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanySchoolYear extends Model
{
    protected $table = 'company_years';

    public function school_academic_year()
    {
        return $this->belongsTo('App\SchoolAcademicYear', 'school_academic_year_id');
    }
}
