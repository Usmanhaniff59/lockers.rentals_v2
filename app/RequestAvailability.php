<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestAvailability extends Model
{
    protected $fillable = [
        'type', 'location', 'town','first_name','surname','email','notes',
    ];

}
