<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailTemplateDistributionList extends Model
{
    protected $fillable = [
        'email_template_id', 'distribution_list_group_id', 'name', 'email', 'sms',
    ];

    public function distributionListGroup()
    {
//        return $this->hasMany('App\DistributionListGroup');
        return $this->belongsTo('App\DistributionListGroup');
    }
}
