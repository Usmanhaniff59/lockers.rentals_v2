<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class VoucherCodesIssued extends Model
{


    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d M, Y H:i');
    }
}
