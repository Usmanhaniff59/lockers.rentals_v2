<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LockerColor extends Model
{
   protected $table = 'locker_color';
}
