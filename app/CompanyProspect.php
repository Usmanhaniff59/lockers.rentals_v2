<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyProspect extends Model
{
    protected $fillable = [
        'territory_code','short_post_code', 'buzz_code','establishment_name','town','country','post_code','telephone','gender','la','school_lea_code','low_age','high_age','open_date','type_of_establishment','sixth_form','phase_of_education','boarding_school','academy_trust','free_school_meals_percentage','religious_affiliation','number_of_pupils','area','website_address','general_email','head_title','head_first','head_surname',
    ];
}