<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Chat extends Model
{
    protected $table = 'chats';
    
    protected $fillable = [
        'details', 'chat_type_id', 'user_id', 'company_id', 'title'
    ];
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
    public function company()
    {
        return $this->belongsTo('App\Company','company_id');
    }

    public function chat_record_permissions($company_chats)
    {

        $user = Auth::user();
        $franchisees = $user->franchisee()->pluck('franchisee_id')->toArray();


        if(auth()->user()->can('All Records')) {


        }elseif(Auth::user()->company_id && $franchisees) {

            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id',$franchisees)->pluck('company_id')->toArray();
            if (!empty($franchisee_companies)) {

                $company_chats->Where(function ($query) use ($franchisee_companies) {
                    $query->WhereIn('chats.company_id', $franchisee_companies)
                        ->orWhere('chats.company_id', Auth::user()->company_id);
                });
            }else{
                $company_chats->Where(function ($query)  {
                    $query->Where('chats.company_id', Auth::user()->company_id);
                });
            }
        }
        elseif($franchisees ) {
            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id',$franchisees)->pluck('company_id')->toArray();

            $company_chats->whereIn('chats.company_id',$franchisee_companies);
        }
        elseif( Auth::user()->company_id ){

            $company_chats->where('chats.company_id',Auth::user()->company_id);

        }else{
            $company_chats->where('chats.company_id',0);
        }

        return $company_chats;

    }
}