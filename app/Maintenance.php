<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Maintenance extends Model
{
    //maintenances
    public function maintenance()
    {
        return $this->belongsTo('App\Locker','locker_id');
    }
    public function maintenance_block()
    {
        return $this->belongsTo('App\Block','block_id');
    }
    public function company()
    {
        return $this->belongsTo('App\Company','company_id');
    }
}
