<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VoucherCode extends Model
{

    protected $fillable = ['type','start','end','company_id','discount_percent','code','issued'];

    public function company()
    {
        return $this->belongsTo('App\Company','company_id');
    }
}
