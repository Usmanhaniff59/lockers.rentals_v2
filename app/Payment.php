<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Transaction;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Exception;
use Throwable;

class Payment extends Model
{
    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }

    public function sales()
    {
        return $this->hasMany('App\Sale', 'booking_id', 'booking_id');

    }

    public function sale()
    {
        return $this->hasOne('App\Sale', 'booking_id', 'booking_id');

    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function hold_locker_for_next_year($booking_id)
    {
        $booking = Sale::where('booking_id', $booking_id)->first();
        $location_group_id = $booking->company->location_group_id;

        if ($location_group_id == 1) {

            $hold_lockers = Sale::where('booking_id', $booking_id)->orderBy('id', 'ASC')->get();
            $language = Session::get('locale');

            foreach ($hold_lockers as $hold_locker) {

                $schoolYear = SchoolYear::where('id', $hold_locker->school_year_id)->first();
                $start_hold = Carbon::parse($schoolYear->from_date)->startOfDay()->addYear()->toDateTimeString();
                $year = Carbon::parse($schoolYear->from_date)->startOfDay()->addYear()->format('Y');
                $date_added = $year . '-05-01 23:59:59';
                $end_hold = Carbon::parse($schoolYear->to_date)->endOfDay()->addYear()->toDateTimeString();

                $school_year = SchoolYear::whereDate('from_date', $start_hold)->whereDate('to_date', $end_hold)->first();
                //
                if ($school_year) {
                    $school_year_id = $school_year->id;
                } else {
                    $start_title = Carbon::parse($start_hold)->format('d M, Y');
                    $end_title = Carbon::parse($end_hold)->format('d M, Y');
                    $title = $start_title . ' - ' . $end_title;
                     $dontAllowDuplicate = SchoolYear::where('title', $title)->first();
                     if(empty($dontAllowDuplicate)){
                    $SchoolYear = new SchoolYear();
                    $SchoolYear->title = $title;
                    $SchoolYear->from_date = $start_hold;
                    $SchoolYear->to_date = $end_hold;
                    $SchoolYear->status = 0;
                    $SchoolYear->save();
                    $school_year_id = $SchoolYear->id;
                    }else{
                        $school_year_id = $dontAllowDuplicate->id;
                    }
                }

                if (!Session::has('hold_locker_booking_id')) {

                    $sale = new Sale();
                    $sale->user_id = Auth::user()->id;
                    $sale->booking_type = 2;
                    if (isset($language)) {
                        if ($language == 'fr') {
                            $sale->lang = 'FR';
                        } else {
                            $sale->lang = 'EN';
                        }
                    } else {
                        $sale->lang = 'EN';
                    }
                    $sale->title = 'LOCKER';
                    $sale->number = $hold_locker->number;
//                    $sale->net_pass = $hold_locker->net_pass;
                    $sale->lock_type = $hold_locker->lock_type;
                    $sale->company_id = $hold_locker->company_id;
                    $sale->locker_id = $hold_locker->locker_id;
                    $sale->block_id = $hold_locker->block_id;
                    $sale->block_name = $hold_locker->block_name;
                    $sale->start = $start_hold;
                    $sale->end = $end_hold;
                    $sale->email = $hold_locker->email;
                    $sale->phone_number = $hold_locker->phone_number;
                    $sale->status = 'r';
                    $sale->hold_booking_date = $date_added;
                    $sale->confirmation_sent = 0;
                    $sale->delete = 0;
                    $sale->street = $hold_locker->street;
                    $sale->city = $hold_locker->city;
                    $sale->post_code = $hold_locker->post_code;
                    $sale->country = $hold_locker->country;
                    $sale->child_first_name = $hold_locker->child_first_name;
                    $sale->child_surname = $hold_locker->child_surname;
                    $sale->child_email = $hold_locker->child_email;
                    $sale->school_year_id = $school_year_id;
                    $sale->school_academic_year_id = $hold_locker->school_academic_year_id;
                    $sale->hold_sales_id = $hold_locker->id;
                    $sale->save();
                    Session::put('hold_locker_booking_id', $sale->id);

                    $sale_obj = new Sale();
                    $sale_obj->update_locker_code($sale->id);
                    $hold_locker_booking_id = Session::get('hold_locker_booking_id');
                    Sale::where('id', $hold_locker_booking_id)->update(['booking_id' => $hold_locker_booking_id]);

                    //add sales_audit log
                    $sales_audit_obj = new SalesAudit();
                    $sales_audit_obj->sales_audit_log($sale->id);

                } else {

                    $hold_locker_booking_id = Session::get('hold_locker_booking_id');
                    $sale = new Sale();
                    $sale->user_id = Auth::user()->id;
                    $sale->booking_id = $hold_locker_booking_id;
                    $sale->booking_type = 2;
                    if (isset($language)) {
                        if ($language == 'fr') {
                            $sale->lang = 'FR';
                        } else {
                            $sale->lang = 'EN';
                        }
                    } else {
                        $sale->lang = 'EN';
                    }
                    $sale->title = 'LOCKER';
                    $sale->number = $hold_locker->number;
//                    $sale->net_pass = $hold_locker->net_pass;
                    $sale->lock_type = $hold_locker->lock_type;
                    $sale->company_id = $hold_locker->company_id;

                    $sale->locker_id = $hold_locker->locker_id;

                    $sale->block_id = $hold_locker->block_id;
                    $sale->block_name = $hold_locker->block_name;
                    $sale->start = $start_hold;
                    $sale->end = $end_hold;
                    $sale->email = $hold_locker->email;
                    $sale->phone_number = $hold_locker->phone_number;
                    $sale->status = 'r';
                    $sale->hold_booking_date = $date_added;
                    $sale->confirmation_sent = 0;
                    $sale->delete = 0;
                    $sale->street = $hold_locker->street;
                    $sale->city = $hold_locker->city;
                    $sale->post_code = $hold_locker->post_code;
                    $sale->country = $hold_locker->country;
                    $sale->child_first_name = $hold_locker->child_first_name;
                    $sale->child_surname = $hold_locker->child_surname;
                    $sale->child_email = $hold_locker->child_email;
                    $sale->school_year_id = $school_year_id;
                    $sale->school_academic_year_id = $hold_locker->school_academic_year_id;
                    $sale->hold_sales_id = $hold_locker->id;
                    $sale->save();

                    $sale_obj = new Sale();
                    $sale_obj->update_locker_code($sale->id);

                    //add sales_audit log
                    $sales_audit_obj = new SalesAudit();
                    $sales_audit_obj->sales_audit_log($sale->id);

                }
            }

        }
    }

    public function change_booked_status($booking_id)
    {
        $sales = Sale::where('booking_id', $booking_id)->get();

        foreach ($sales as $sale) {

            Sale::where('id', $sale->id)->update([
                'status' => 'b',
            ]);

            //add sales_audit log
            $sales_audit_obj = new SalesAudit();
            $sales_audit_obj->sales_audit_log($sale->id);
        }

    }

    public function update_phone_number($booking_id, $phone_number)
    {
        $sales = Sale::where('booking_id', $booking_id)->get();

        foreach ($sales as $sale) {

            Sale::where('id', $sale->id)->update([
                'phone_number' => $phone_number
            ]);


        }


    }

    public function company_currency($booking_id)
    {
        $sale = Sale::where('booking_id', $booking_id)->first();
        return $sale->company->currency;

    }

    public function price_calculation($booking_id, $charity_input)
    {
        $sales = Sale::where('booking_id', $booking_id)->get();
        $total_price = 0.00;
        $total_lockers_price = 0.00;
        $charity_price = 0.00;
        foreach ($sales as $locker) {
            $total_lockers_price += $locker->price;
            $total_lockers_price = number_format($total_lockers_price, 2, '.', '');

            //update Manual locker code using sale_id
            $code = new Sale();
            $code->update_locker_code($locker->id);
        }
        $total_price = $total_lockers_price;
        if ($charity_input != null) {
            $total_price += intval($charity_input);
            $total_price = number_format($total_price, 2, '.', '');

        }
//
        $charity_price = number_format(intval($charity_input), 2, '.', '');


        return [$total_lockers_price, $charity_price, $total_price];

    }

    public function price_calculation_sagepay_response($booking_id, $total_price)
    {

        $total_lockers_price = 0.00;
        $sales = Sale::where('booking_id', $booking_id)->get();
        foreach ($sales as $locker) {
            $total_lockers_price += $locker->price;
        }
        $total_price = number_format($total_price, 2, '.', '');
        $total_lockers_price = number_format($total_lockers_price, 2, '.', '');
        $charity_price = $total_price - $total_lockers_price;

        if ($charity_price < 0)
            $charity_price = 0;

        $charity_price = number_format($charity_price, 2, '.', '');


        return [$total_lockers_price, $charity_price, $total_price];

    }

    public function insert_payments($user_id, $booking_id, $billing_address1, $billing_address2, $city, $country, $first_name, $phone_number, $post_code, $state, $last_name, $currency, $user_email)
    {
        $payment = new Payment();
        $payment->user_id = $user_id;
        $payment->booking_id = $booking_id;
        $payment->billing_address1 = $billing_address1;
        $payment->billing_address2 = $billing_address2;
        $payment->billing_city = $city;
        $payment->billing_country = $country;
        $payment->billing_first_name = $first_name;
        $payment->billing_phone = $phone_number;
        $payment->billing_post_code = $post_code;
        $payment->billing_state = $state;
        $payment->billing_surname = $last_name;
        $payment->currency = $currency;
        $payment->created = Carbon::now();
        $payment->customer_email = $user_email;
        $payment->save();
        return $payment->id;

    }

    public function update_payments($payment_id, $amount, $user_id, $billing_address1, $billing_address2, $city, $country, $first_name, $phone_number, $post_code, $state, $last_name, $currency, $user_email)
    {
        $payment = Payment::find($payment_id);
        $payment->amount = $amount;
        $payment->account_balance = $amount;
        $payment->user_id = $user_id;
        $payment->billing_address1 = $billing_address1;
        $payment->billing_address2 = $billing_address2;
        $payment->billing_city = $city;
        $payment->billing_country = $country;
        $payment->billing_first_name = $first_name;
        $payment->billing_phone = $phone_number;
        $payment->billing_post_code = $post_code;
        $payment->billing_state = $state;
        $payment->billing_surname = $last_name;
        $payment->currency = $currency;
        $payment->customer_email = $user_email;
        $payment->waiting_response_sagepay = 0;
        $payment->has_transaction = 1;
        $payment->save();

    }

    public function insert_transactions($booking_id, $payment_id, $user_id, $currency, $cardNumber, $amount, $type, $response_status_code, $vendorTxCode, $status_message, $fraud_score, $transactionId, $waiting_response_sagepay)
    {

        $transaction = new Transaction();
        $transaction->booking_id = $booking_id;
        $transaction->payment_id = $payment_id;
        $transaction->user_id = $user_id;
        $transaction->currency = $currency;
        $transaction->last4Digits = $cardNumber;
        $transaction->amount = $amount;
        $transaction->type = $type;
        $transaction->status = $response_status_code;
        $transaction->vendor_tx_code = $vendorTxCode;
        $transaction->status_message = $status_message;
        $transaction->fraud_score = $fraud_score;
        $transaction->transaction_id = $transactionId;
        $transaction->waiting_response_sagepay = $waiting_response_sagepay;
        $transaction->save();

        $payment = Payment::find($payment_id);
        $payment->waiting_response_sagepay = 0;
        $payment->save();

        return $transaction->id;

    }

    public function update_transactions($booking_id, $payment_id, $user_id, $currency, $cardNumber, $amount, $type, $response_status_code, $status_message, $fraud_score, $transactionId)
    {
        $transaction = Transaction::where('booking_id', $booking_id)->where('waiting_response_sagepay', '1')->first();
        $transaction->booking_id = $booking_id;
        $transaction->payment_id = $payment_id;
        $transaction->user_id = $user_id;
        $transaction->currency = $currency;
        $transaction->last4Digits = $cardNumber;
        $transaction->amount = $amount;
        $transaction->type = $type;
        $transaction->status = $response_status_code;
        $transaction->status_message = $status_message;
        $transaction->fraud_score = $fraud_score;
        $transaction->transaction_id = $transactionId;
        $transaction->waiting_response_sagepay = 0;
        $transaction->save();

//        $payment = Payment::find($payment_id);
//        $payment->waiting_response_sagepay = 0;
//        $payment->save();

    }

    public function sagepay_transaction($merchantSessionKey, $cardIdentifier, $Authorization, $vendorTxCode, $total_price, $currency, $payment_type,
                                        $first_name, $last_name, $billing_address1, $billing_address2, $city, $state, $post_code, $country, $phone_number,
                                        $booking_id, $payment_id, $user_id, $user_email, $last4Digits, $total_lockers_price, $charity_price, $charity_id, $ajax)
    {
//
            $curl = curl_init();

            $transaction_postArray1 = [
                'transactionType' => 'Payment',
                'paymentMethod' => [
                    'card' => [
                        'merchantSessionKey' => $merchantSessionKey,
                        'cardIdentifier' => $cardIdentifier,
                        'save' => 'false',
                    ],
                ],
                'vendorTxCode' => '' . $vendorTxCode . '',
                'amount' => intval($total_price * 100),
                'currency' => $currency,
                'description' => 'locker booking transaction',];

//  Virtual== '01' is a
//                            if ($payment_type == '01') {
//                                $transaction_postArray2 = [
//                                    'apply3DSecure' => 'Disable',
//                                ];
//
//                            } else {
//
//                                $transaction_postArray2 = [
//                                    'apply3DSecure' => 'UseMSPSetting',
//                                ];
//
//
//                            }

            $transaction_postArray2 = [
                'apply3DSecure' => 'Disable',
            ];

            $card_merge_array = array_merge($transaction_postArray1, $transaction_postArray2);


            $transaction_postArray3 = [

                'customerFirstName' => $first_name,
                'customerLastName' => $last_name,
                'billingAddress' => [
                    'address1' => $billing_address1,
                    'city' => $city,
                    'postalCode' => $post_code,
                    'country' => $country,
                ],
            ];
            $card_merge_array2 = array_merge($card_merge_array, $transaction_postArray3);

            if ($payment_type == '01') {
                $transaction_postArray4 = [
                    'entryMethod' => 'Ecommerce',

                ];
            } else {
                $transaction_postArray4 = [
                    'entryMethod' => 'Ecommerce',
                ];
            }

            $transaction_postArray = array_merge($card_merge_array2, $transaction_postArray4);

            $transaction_postArray_json = json_encode($transaction_postArray);


            $transaction_url = \Config::get('app.transaction_url');
        try {
            curl_setopt_array($curl, array(
                CURLOPT_URL => $transaction_url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => $transaction_postArray_json,
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Basic $Authorization",
                    "Cache-Control: no-cache",
                    "Content-Type: application/json"
                ),
            ));

            $transaction_response = curl_exec($curl);

            $err = curl_error($curl);
            curl_close($curl);

            $transaction_response = json_decode($transaction_response);

            //sage pay response handling
            return  $this->sage_pay_response_handling($transaction_response,$booking_id,$user_id,$payment_type,$currency,$last4Digits,$total_lockers_price,$charity_price,$charity_id,$total_price,$ajax,$vendorTxCode,$payment_id,$billing_address1, $billing_address2, $city, $country, $first_name, $phone_number, $post_code, $state, $last_name, $user_email);

//            dd('upper');
        } catch (Throwable $exception) {

            $code = $exception->getCode();
            $line = $exception->getLine();
            $message = $exception->getMessage();

            $this->error_log($code, $line, $message, $vendorTxCode);

            //sage pay response handling
          return  $this->sage_pay_response_handling($transaction_response,$booking_id,$user_id,$payment_type,$currency,$last4Digits,$total_lockers_price,$charity_price,$charity_id,$total_price,$ajax,$vendorTxCode,$payment_id,$billing_address1, $billing_address2, $city, $country, $first_name, $phone_number, $post_code, $state, $last_name, $user_email);
//            dd('inner');
        }

    }

    public function sage_pay_response_handling($transaction_response,$booking_id,$user_id,$payment_type,$currency,$last4Digits,$total_lockers_price,$charity_price,$charity_id,$total_price,$ajax,$vendorTxCode,$payment_id,$billing_address1, $billing_address2, $city, $country, $first_name, $phone_number, $post_code, $state, $last_name, $user_email){
//dd($transaction_response);
        if (isset($transaction_response->statusCode)) {
            $transactionId = $transaction_response->transactionId;

            if ($transaction_response->statusCode == '0000') {
//                dd($transaction_response->statusCode);
                if (Session::get('hold_locker_checked') == true) {

//                    $locker_hold = new Payment();
                    $this->hold_locker_for_next_year($booking_id);
                    Session::forget('hold_locker_checked');
                }

                $status_message = $transaction_response->statusCode . ':' . $transaction_response->statusDetail;
                //  Virtual== '01' is a

                $transaction_type = $payment_type;

//                $payment = new Payment();
                $this->update_payments($payment_id, $total_price, $user_id, $billing_address1, $billing_address2, $city, $country, $first_name, $phone_number, $post_code, $state, $last_name, $currency, $user_email);

//                $transaction = new Payment();
                $this->update_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $total_lockers_price, $transaction_type, $transaction_response->statusCode, $status_message, 0, $transactionId,$payment_id);

                if ($charity_price > 0) {
                    $waiting_response_sagepay = 0;
//                    $transaction = new Payment();
                    $this->insert_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $charity_price, '03', $transaction_response->statusCode, $vendorTxCode, $status_message, 0, $transactionId, $waiting_response_sagepay);

//                    $create_charity = new Payment();
                    $this->create_charity($booking_id, $charity_price, $charity_id);
                }

//                $locker_code = new Payment();
                $this->change_booked_status($booking_id);


//                dd($booking_id);
//                $confirm_payments = new Payment();
                return $this->confirmation_payments($booking_id, $total_lockers_price, $charity_price, $total_price, $ajax);


            } else if ($transaction_response->statusCode == '2007') {


                $acsUrl = $transaction_response->acsUrl;
                $paReq = $transaction_response->paReq;
                $last4Digits = $last4Digits;
                $transactionId = $transaction_response->transactionId . ',' . $vendorTxCode . ',' . $payment_id . ',' . $last4Digits . ',' . $total_lockers_price . ',' . $charity_price . ',' . $total_price;;

                $data = [
                    'status' => '3D',
                    'view' => view('frontend.pages.payments.3D_authentication', compact('acsUrl', 'paReq', 'transactionId'))->render()

                ];
                return response()->json($data);

            } else if ($transaction_response->statusCode == '2001') {

                $status_message = $transaction_response->statusCode . ':' . $transaction_response->statusDetail;

                $transaction_type = $payment_type;
                $fraud_score = 1;
                $amount = null;
                $waiting_response_sagepay = 0;
//                $transaction = new Payment();
                $this->update_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $amount, $transaction_type, $transaction_response->statusCode, $status_message, $fraud_score, $transactionId, $waiting_response_sagepay);


                $error_msgs = [];
                array_push($error_msgs, $transaction_response->statusDetail);

                $data = [
                    'status' => 'errors',
                    'error_msgs' => $error_msgs
                ];

                return response()->json($data);

            } elseif ($transaction_response->status == 'Invalid') {

                $status_message = $transaction_response->statusCode . ':' . $transaction_response->statusDetail;


                $transaction_type = $payment_type;
                $fraud_score = 1;
                $amount = null;
                $waiting_response_sagepay = 0;
//                $transaction = new Payment();
                $this->update_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $amount, $transaction_type, $transaction_response->statusCode, $status_message, $fraud_score, $transactionId, $waiting_response_sagepay);

                $error_msgs = [];
                array_push($error_msgs, $transaction_response->statusDetail);

                $data = [
                    'status' => 'errors',
                    'error_msgs' => $error_msgs
                ];

                return response()->json($data);


            } else {
                $status_message = $transaction_response->statusCode . ':' . $transaction_response->statusDetail;

                $transaction_type = $payment_type;
                $fraud_score = 1;
                $amount = null;
                $waiting_response_sagepay = 0;
//                $transaction = new Payment();
                $this->update_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $amount, $transaction_type, $transaction_response->statusCode, $status_message, $fraud_score, $transactionId, $waiting_response_sagepay);

                $error_msgs = [];
                array_push($error_msgs, $transaction_response->statusDetail);

                $data = [
                    'status' => 'errors',
                    'error_msgs' => $error_msgs
                ];
//dd($data);
                return response()->json($data);

            }
        } else
            if (isset($transaction_response->errors)) {
            $error_msgs = [];
            $error_count = 1;
            foreach ($transaction_response->errors as $error) {

                $status_message = $error->code . ':' . $error->description . '. ' . $error->property;

                $transaction_type = $payment_type;
                $fraud_score = 1;
                $amount = null;
                $transactionId = null;
                $waiting_response_sagepay = 0;

                if ($error_count == 1) {
//                    $transaction = new Payment();
                    $this->update_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $amount, $transaction_type, $error->code, $status_message, $fraud_score, $transactionId, $waiting_response_sagepay);

                } else {
                    $vendorTxCode = 'lockers_' . Carbon::now()->format('Ymdhis') . '_' . $booking_id;
//                    $transaction = new Payment();
                    $this->insert_transactions($booking_id, $payment_id, $user_id, $currency, $last4Digits, $amount, $transaction_type, $error->code, $vendorTxCode, $status_message, $fraud_score, $transactionId, $waiting_response_sagepay);

                }
                $response = $error->description . '. ' . $error->property;

                array_push($error_msgs, $response);
            }
            $data = [
                'status' => 'errors',
                'error_msgs' => $error_msgs
            ];
            return response()->json($data);
        }

    }

    public function confirmation_payments($booking_id, $total_lockers_price, $charity_price, $total_price, $ajax)
    {
//        dd('confirm');
//        dd($booking_id);
        $lockerInfo = Sale::Join('lockers', 'lockers.id', '=', 'sales.locker_id')
            ->Join('blocks', 'blocks.id', '=', 'sales.block_id')
            ->Join('companies', 'companies.id', '=', 'sales.company_id')

            ->select('blocks.name as block_name', 'lockers.company_id', 'lockers.locker_number', 'sales.net_pass', 'sales.locker_id', 'sales.block_id', 'sales.id as sale_id', 'sales.booking_id as booking_id', 'sales.lock_type', 'sales.lang', 'sales.start', 'sales.end', 'sales.child_first_name', 'sales.child_surname', 'sales.child_email', DB::raw("CONCAT(sales.child_first_name, ' ' ,sales.child_surname) AS childName"), 'sales.price', 'sales.tax')
            ->where('sales.booking_id', $booking_id)
            ->get();
           
            
        $companyIfo = Sale::Join('lockers', 'lockers.id', '=', 'sales.locker_id')
            ->Join('blocks', 'blocks.id', '=', 'sales.block_id')
            ->Join('companies', 'companies.id', '=', 'sales.company_id')
            ->Join('users', 'users.id', '=', 'sales.user_id')
            ->select('blocks.name as block_name', 'lockers.locker_number', 'sales.id as sale_id', 'companies.id as company_id', 'companies.location_group_id', 'sales.booking_id as booking_id', 'users.id as user_id', 'users.name', 'users.surname', 'users.username', 'users.email', 'users.address_1', 'users.address_2', 'users.city as user_city', 'users.post_code as user_post_code', 'users.state as user_state', 'sales.lang', 'sales.lock_type', 'sales.price', 'sales.tax', 'companies.name as company_name', 'companies.street as company_street', 'companies.city as company_city', 'companies.country as company_country')
            ->where('sales.booking_id', $booking_id)
            ->first();


        $currency = Company::join('currencies', 'companies.currency', '=', 'currencies.currency_name')
            ->where('companies.id', $companyIfo->company_id)
            ->select('currencies.currency_name', 'currencies.symbol', 'currencies.position')->first();

        $placeholders = FrontendPlaceholder::where('key', 'payments-confirmation-card-success-msg')->pluck('placeholder')->toArray();
        $payment_success_msg_placeholders = implode(",", $placeholders);

        $booking_confirmation = 'Confirmed';

        $marketing_pictures = MarketingPictures::where('active', 1)->where('deleted', 0)->get();

        //check Voucher code is applied
        $voucher_code_id = Session::get('voucher_code_id');
        if (isset($voucher_code_id)) {

            $this->save_voucher_code_user($voucher_code_id, $booking_id);
            $this->save_voucher_payments_report($voucher_code_id, $booking_id);
        }

        $sessions_expire = new Payment();
        $sessions_expire->sessions_expire();

        if ($ajax == true) {
            $data = [
                'status' => 'confirmation',
                'view' => view('frontend.pages.payments.payment_confirmation_render', compact('booking_confirmation', 'companyIfo', 'currency', 'lockerInfo', 'total_lockers_price', 'charity_price', 'total_price', 'payment_success_msg_placeholders', 'marketing_pictures'))->render()

            ];
            return response()->json($data);
        } else {
            return view('frontend.pages.payments.payment_confirmation', compact('booking_confirmation', 'companyIfo', 'currency', 'lockerInfo', 'total_lockers_price', 'charity_price', 'total_price', 'payment_success_msg_placeholders', 'marketing_pictures'));

        }
    }

    public function get_transaction_from_sagepay_using_vendor_tx_code($vendorTxCode, $command)
    {

        $vendor = \Config::get('app.sage_pay_vendor');
        $user = \Config::get('app.sage_pay_user');
        $password = \Config::get('app.sage_pay_password');
        $url = \Config::get('app.xml_get_transaction_url');
        $string = '<command>' . $command . '</command><vendor>' . $vendor . '</vendor><user>' . $user . '</user><vendortxcode>' . $vendorTxCode . '</vendortxcode>';
        $crypt = MD5($string . '<password>' . $password . '</password>');
        $xml = 'XML=<vspaccess>' . $string . '<signature>' . $crypt . '</signature></vspaccess>';

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);
        $result = curl_exec($curl);

        $xmlObject = simplexml_load_string($result);
        $jsonString = json_encode($xmlObject);
        $sage_pay_response = json_decode($jsonString, true);
        return $sage_pay_response;
    }

    public function get_transaction_list_from_sagepay($command,$start,$end,$row_start,$row_end)
    {

        $vendor = \Config::get('app.sage_pay_vendor');
        $user = \Config::get('app.sage_pay_user');
        $password = \Config::get('app.sage_pay_password');
        $url = \Config::get('app.xml_get_transaction_url');
        $string = '<command>' . $command . '</command>
                    <vendor>' . $vendor . '</vendor>
                    <user>' . $user . '</user>
                    <startdate>'.$start.'</startdate>
                    <enddate>'.$end.'</enddate>
                    <startrow>'.$row_start.'</startrow>
                    <endrow>'.$row_end.'</endrow>';

        $crypt = MD5($string . '<password>' . $password . '</password>');

        $xml = 'XML=<vspaccess>' . $string . '<signature>' . $crypt . '</signature></vspaccess>';

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);
        $result = curl_exec($curl);

        $xmlObject = simplexml_load_string($result);
        $jsonString = json_encode($xmlObject);
        $sage_pay_response = json_decode($jsonString, true);
        return $sage_pay_response;
    }

    public function save_voucher_code_user($voucher_code_id, $booking_id)
    {
        $voucher_code = VoucherCode::find($voucher_code_id);
        $VoucherCodeUser = VoucherCodeUser::where('booking_id', $booking_id)->first();

        if (empty($VoucherCodeUser)) {

            $voucher_code_user = new VoucherCodeUser();
            $voucher_code_user->user_id = Auth::user()->id;
            $voucher_code_user->booking_id = $booking_id;
            $voucher_code_user->code = $voucher_code->code;
            $voucher_code_user->save();

        } else {

            $VoucherCodeUser->user_id = Auth::user()->id;
            $VoucherCodeUser->code = $voucher_code->code;
            $VoucherCodeUser->save();
        }


    }

    public function save_voucher_payments_report($voucher_code_id, $booking_id)
    {
        $voucher_code = VoucherCode::find($voucher_code_id);
        $sales = Sale::where('booking_id', $booking_id)->get();
        foreach ($sales as $sale) {

            $payment_obj = new Sale();
            $booking_cost = $payment_obj->rate_calculation_without_discount($sale->company->location_group_id, $sale->block_id, $sale->start, $sale->end);
            $booking_cost = number_format((float)($booking_cost[0]), 2, '.', '');


            $voucher_cost = $year_cost = number_format((float)($voucher_code->discount_percent * $booking_cost / 100), 2, '.', '');

            $voucher_payments = new VoucherPaymentsReport();
            $voucher_payments->company_id = $sale->company_id;
            $voucher_payments->locker_id = $sale->locker_id;
            $voucher_payments->voucher_code_id = $voucher_code_id;
            $voucher_payments->user_id = Auth::user()->id;
            $voucher_payments->booking_id = $sale->booking_id;
            $voucher_payments->start = $sale->start;
            $voucher_payments->end = $sale->end;
            $voucher_payments->booking_cost = $booking_cost;
            $voucher_payments->voucher_cost = $voucher_cost;
            $voucher_payments->save();

        }

    }

    public function get_transaction_from_sagepay_using_transaction_id($transactionId)
    {

        $retrieve_transaction_url = \Config::get('app.retrieve_transaction_url');
        $Authorization = paymentConfig::integrationKey();
        $retrieve_transaction_url = str_replace("<transactionId>", $transactionId, "$retrieve_transaction_url");

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $retrieve_transaction_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array(
                "Authorization: Basic $Authorization",
                "Cache-Control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        $retrieve_transaction_response = json_decode($response);
        return $retrieve_transaction_response;
    }

    public function create_charity($booking_id, $charity_price, $charity_id)
    {
        $charity = CharityTransactions::where('booking_id', $booking_id)->first();
        if (empty($charity)) {
            CharityTransactions::create([
                'booking_id' => $booking_id,
                'amount' => $charity_price,
                'charity_id' => $charity_id
            ]);
        }

    }

    public function sessions_expire()
    {
        Session::forget('saleKey');
        Session::forget('schoolTermSubmit');
        Session::forget('bookingTimer');
        Session::forget('show_hide_lockers');
        Session::forget('on_payments_booking_id');
        Session::forget('user_payments_form_info');
        Session::forget('postCodeMethod');
        Session::forget('payment_id');
        Session::forget('arrowsPage');
        Session::forget('page_number');
        Session::forget('schoolTermReSubmit');
        Session::forget('schooltermbasedback');
        Session::forget('companyId');
        Session::forget('company_info');
        Session::forget('payment');
        Session::forget('show_hide_lockers');
        Session::forget('on_payments_booking_id');
        Session::forget('postCodeMethod');
        Session::forget('schoolTermReSubmit');
        Session::forget('hold_locker_booking_id');
        Session::forget('hold_locker_page2_checked');
        Session::forget('hold_locker_checked');
        Session::forget('reservedLocker');
        Session::forget('success_transaction_id');
        Session::forget('voucher_code_id');

    }

    public function user_info($booking_id)
    {
        $sale = Sale::where('booking_id', $booking_id)->first();
        return $sale->user;

    }

    public function error_log($code, $line, $message, $vendorTxCode)
    {
//        dd('d');
        $error = new ErrorLog();
        $error->code = $code;
        $error->line_number = $line;
        $error->message = $message;
        $error->vendor_tx_code = $vendorTxCode;
        $error->save();

    }


}