<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LockerNumberReport extends Model
{
   protected $table = 'locker_number_report';
       public function blockgroup()
    {
        return $this->belongsTo('App\BlockGroup','block_group_id');

    }
}
