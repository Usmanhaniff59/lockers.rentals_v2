<?php

namespace App\Jobs;

use App\CronSchedule;
use App\Email;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\SendEmailTest;
use Mail;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email_to;
    protected $email_html;
    protected $subject;
    protected $email_id;
    protected $cron_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($details, $email_html, $subject, $email_id,$cron_id)
    {
        $this->email_to = $details;
        $this->email_html = $email_html;
        $this->subject = $subject;
        $this->email_id = $email_id;
        $this->cron_id = $cron_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new SendEmailTest($this->email_html);
//        dd($email);
        Mail::to($this->email_to)->send($email); //->subject($this->subject)


        if (Mail::failures()) {
            $send_email = Email::where('id',$this->email_id)->first();
            $send_email->send = '0';
            $send_email->save();

            $cron_status = CronSchedule::where('id', $this->cron_id)->first();
            $cron_status->where_check = 0;
            $cron_status->status = 'Failed';
            $cron_status->save();

        }else{

            $send_email = Email::where('id',$this->email_id)->first();
            $send_email->complete = '1';
            $send_email->save();


            $emails = Email::where('action','sent')
                ->where('complete', 0)
                ->orWhere('send', 0)
                ->where('date_added', '<=', Carbon::now())
                ->get();
            if($emails->isEmpty() == true) {

                $cron_status = CronSchedule::where('id', $this->cron_id)->first();
                $cron_status->where_check = 0;
                $cron_status->completed = 1;
                $cron_status->status = 'Completed';
                $cron_status->save();
            }


        }
    }
//    public function failed(Exception $exception)
//    {
//        $send_email = Email::where('id',$this->email_id)->first();
//        $send_email->send = '0';
//        $send_email->save();
//
//    }
}