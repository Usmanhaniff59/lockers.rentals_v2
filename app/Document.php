<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table = 'documents';
    
    protected $fillable = [
        'title', 'type_id', 'original_path', 'converted_path', 'uploaded_by', 'company_id'
    ];
    public function user()
    {
        return $this->belongsTo('App\User','uploaded_by');
    }
    public function company()
    {
        return $this->belongsTo('App\Company','company_id');
    }
    public function document_type()
    {
        return $this->belongsTo('App\DocumentTypes','type_id');
    }
}