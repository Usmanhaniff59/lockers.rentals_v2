<?php

namespace App;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class VoucherPaymentsReport extends Model
{
    protected $fillable = [ 'locker_id', 'voucher_code_id', 'user_id', 'booking_id', 'start', 'end', 'booking_cost', 'voucher_cost'];

    public function getStartAttribute($value)
		{
	     return Carbon::parse($value)->format('d M, Y H:i');
		}
	public function setUserIdAttribute($value)
		{
			$string= ucfirst($value);
       	    return $string;
		}
	public function getUserIdAttribute($value)
		{
			$string= ucfirst($value);
       	    return $string;
		}
	public function getEndAttribute($value)
		{
	     return Carbon::parse($value)->format('d M, Y H:i');
		}

    public function voucher_report_record_permissions($query)
    {

        $user = Auth::user();
        $franchisees = $user->franchisee()->pluck('franchisee_id')->toArray();

        if(auth()->user()->can('All Records')) {


        }elseif( Auth::user()->company_id && $franchisees) {

            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id',$franchisees)->pluck('company_id')->toArray();

            $query->Where(function ($q) use ($franchisee_companies) {
                $q->WhereIn('voucher_codes.company_id',$franchisee_companies)
                    ->orWhere('voucher_codes.company_id',Auth::user()->company_id);
            });
        }
        elseif( $franchisees ) {
            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id',$franchisees)->pluck('company_id')->toArray();

            $query->whereIn('voucher_codes.company_id',$franchisee_companies);
        }
        elseif( Auth::user()->company_id ){

            $query->where('voucher_codes.company_id',Auth::user()->company_id);

        }else{
            $query->where('voucher_codes.company_id',0);
        }

        return $query;

    }
}
