<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationGroup extends Model
{

    protected $fillable = [ 'name' ];

    public function companies()
    {
        return $this->hasMany('App\Company','location_group_id');
    }
}
