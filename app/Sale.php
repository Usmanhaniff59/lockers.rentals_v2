<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
class Sale extends Model
{

    public function payment()
    {
        return $this->belongsTo('App\Payment','booking_id','booking_id');
    }

    public function transactions()
    {
        return $this->hasMany('App\Transaction','booking_id');
    }
    public function company()
    {
        return $this->belongsTo('App\Company','company_id','id');
    }
    public function block()
    {
        return $this->belongsTo('App\Block','block_id','id');
    }
    public function locker()
    {
        return $this->belongsTo('App\Locker','locker_id','id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }

    public function booking_expiry_time()
    {
        return Carbon::now()->addMinutes(15);
    }

    public function hold_booking_date()
    {
        return Carbon::now()->addHour()->toDateTimeString();
    }

    public function school_year_validation($company_id,$school_academic_year_id,$start,$end,$book_next_year)
    {
        $block_years = BlockYears::where('school_academic_year_id',$school_academic_year_id)
            ->where('company_id',$company_id)
            ->get();
        $available_lockers_array['total_available'] = 0;

        foreach ($block_years as $by ){


            // $blocks = Block::where('id',$by->block_id)
            //     ->where('active',1)
            //     ->where('display',1)
            //     ->get();
              $blocks=DB::select('SELECT * FROM blocks  WHERE id ='.$by->block_id.' AND active=1 AND display=1 AND IF(end_date IS NULL,(start_date BETWEEN "'.$start.'" AND "'.$end.'"), ((start_date BETWEEN "'.$start.'" AND "'.$end.'") OR (end_date BETWEEN "'.$start.'" AND "'.$end.'")) )  ORDER BY name ASC');

            foreach ($blocks as $b) {

                $rate = Rate::where(function ($query) use ($start,$end) {
                    $query->where('valid_too','>=',$start)
                        ->Where('valid_from','<=',$end);
                })
                    ->where('id', $b->rate_id)
                    ->first();


                if (!empty($rate)) {
                    //get active lockers
                    $lockers = Locker::where('block_id', $b->id)
                        ->where('active', 1)
                        ->where('display', 1)
                        ->get();

                    foreach ($lockers as $l) {

                        $available = $this->is_locker_available($l->id, $start, $end, $book_next_year);


                        if (!empty($available)) {

                            if (empty($available_lockers_array['total_available']))
                                $available_lockers_array['total_available'] = 1;
                            else
                                $available_lockers_array['total_available']++;


                            if (empty($available_lockers_array[$b->id]))
                                $available_lockers_array[$b->id] = 1;
                            else
                                $available_lockers_array[$b->id]++;




                        }
                    }



                }

            }
        }

        return $available_lockers_array;
    }

    public function is_locker_available($locker_id,$start,$end,$book_next_year)
    {
        $available = 1;
        $sales = Sale::where(function ($query) use ($start, $end) {
            $query->where('end', '>=', $start)
                ->Where('start', '<=', $end);
        })
            ->where('locker_id', $locker_id)
            ->where(function ($status) {
                $status->where('status', 'b')
                    ->orWhere('status', 'r');
            })
            ->first();
           
        if (!empty($sales)) {
            $available = 0;
        }


        $maintenances = Maintenance::where('to', '>=', $start)->Where('from', '<=', $end)
            ->where('locker_id',$locker_id)
            ->where('active', 1)
            ->first();

        if (!empty($maintenances))
            $available = 0;

        $locker_off_sale = LockerOffSale::where('locker_id', $locker_id)->where('active', 1)->first();

        if (!empty($locker_off_sale))
            $available = 0;

        if ($book_next_year == true) {

            $start_next = Carbon::parse($start)->startOfDay()->addYear()->toDateTimeString();
            $end_next = Carbon::parse($end)->endOfDay()->addYear()->toDateTimeString();

            $sales = Sale::where(function ($query) use ($start_next, $end_next) {
                $query->where('end', '>=', $start_next)
                    ->Where('start', '<=', $end_next);
            })
                ->where('locker_id', $locker_id)
                ->where(function ($status) {
                    $status->where('status', 'b')
                        ->orWhere('status', 'r');
                })
                ->first();
//                            dd($sales);

            if (!empty($sales))
                $available = 0;

            $maintenances = Maintenance::where('to', '>=', $start_next)->Where('from', '<=', $end_next)
                ->where('locker_id', $locker_id)
                ->where('active', 1)
                ->first();

            if (!empty($maintenances))
                $available = 0;


        }

        return $available;
    }

    public function school_year_blocks($company_id,$school_academic_year_id)
    {
        $block_years = BlockYears::where('school_academic_year_id',$school_academic_year_id)
            ->where('company_id',$company_id)
            ->get();


        return $block_years;
    }

    public function total_available_lockers_in_block($block_id,$start,$end,$book_next_year)
    {

        $available_lockers_array['total_available'] = 0;

        $blocks = Block::where('id',$block_id)
            ->where('active',1)
            ->where('display',1)
              // ->where('start_date','<=',Carbon::now()) 
            ->get();
        
        foreach ($blocks as $b) {
              $curdate=Carbon::now();
                           
                            $mydate=Carbon::parse($b->end_date);

                            // if(!isset($b->end_date) || $mydate>$curdate ){

            $rate = Rate::where(function ($query) use ($start,$end) {
                $query->where('valid_too','>=',$start)
                    ->Where('valid_from','<=',$end);
            })
                ->where('id', $b->rate_id)
                ->first();
//                dd($rate);

            if (!empty($rate)) {
                //get active lockers
                $lockers = Locker::where('block_id', $b->id)
                    ->where('active', 1)
                    ->where('display', 1)
                  
                    ->get();
//                        ->count();

                foreach ($lockers as $l) {
                 
                    $available = $this->is_locker_available($l->id, $start, $end, $book_next_year);

                    if (!empty($available)) {

                        if (empty($available_lockers_array['total_available']))
                            $available_lockers_array['total_available'] = 1;
                        else
                            $available_lockers_array['total_available']++;

                    }

                }

            }

        // }
    }


        return $available_lockers_array;
    }

    public function ids_of_total_available_lockers_in_block($block_id,$start,$end,$book_next_year)
    {

        $available_lockers_array = [];

        $blocks = Block::where('id',$block_id)
            ->where('active',1)
            ->where('display',1)
            ->get();
//            dd($blocks);
        foreach ($blocks as $b) {

            $rate = Rate::where(function ($query) use ($start,$end) {
                $query->where('valid_too','>=',$start)
                    ->Where('valid_from','<=',$end);
            })
                ->where('id', $b->rate_id)
                ->first();
//                dd($rate);

            if (!empty($rate)) {
                //get active lockers
                $lockers = Locker::where('block_id', $b->id)
                    ->where('active', 1)
                    ->where('display', 1)
                    ->get();
//                        ->count();
//                dd($lockers);
                foreach ($lockers as $l) {

                    $available = $this->is_locker_available($l->id, $start, $end, $book_next_year);

                    if (!empty($available)) {

                        array_push($available_lockers_array ,$l->id);

                    }

                }
            }

        }


        return $available_lockers_array;
    }

    public function rate_calculation($location_group , $block_id, $start , $end ,$booking_id)
    {

        $block = Block::find($block_id);
        $rate = $block->rate;
//        dd($rate);
        $voucher_code_id = Session::get('voucher_code_id');
        if ($location_group == 1){
            $now = Carbon::now();
            if(isset($voucher_code_id)){

                $voucher_code = VoucherCode::find($voucher_code_id);
                $sales = Sale::where('booking_id',$booking_id)->get();

                foreach ($sales as $sale){
                    $sale->discount_percent =$voucher_code->discount_percent;
                    $sale->save();
                }

                $voucher_discount_percent = $voucher_code->discount_percent/100;

                $voucher_discount_price = number_format((float)($rate->unit * $voucher_discount_percent), 2, '.', '');

                $unit = number_format((float)($rate->unit - $voucher_discount_price), 2, '.', '');

                $tax = number_format((float)($unit * $rate->vat), 2, '.', '');

            }
            else if ($start < $now) {

                $date = $now->format('F');

                $discount_month = PaymentDiscount::where('rate_id', $rate->id)->first()->toArray();
//                dd($date);
                if ($date == 'September' || $date == 'July') {
                    $month = strtolower(substr($date, 0, 4));
                } elseif ($date == 'April') {
                    $month = strtolower(substr($date, 0, 5));
                } elseif(($date == 'August')) {
                    $month = 'sept';
                }else {
                    $month = strtolower(substr($date, 0, 3));
                }
                $sales = Sale::where('booking_id',$booking_id)->get();
                foreach ($sales as $sale){
                    $sale->discount_percent = $discount_month[$month];
                    $sale->save();
                }
                $discount = number_format((float)($discount_month[$month] / 100), 2, '.', '');

                $discount_price = number_format((float)($rate->unit * $discount), 2, '.', '');

                $unit = number_format((float)($rate->unit - $discount_price), 2, '.', '');

                $tax = number_format((float)($unit * $rate->vat), 2, '.', '');

            } else {
                $unit = number_format((float)($rate->unit), 2, '.', '');
                $tax = number_format((float)($unit * $rate->vat), 2, '.', '');
            }

            return [$unit, $tax];
        }
        else{

            $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $start)->startOfDay();
            $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $end)->endOfDay();


            $diff_in_hours = ceil($from->floatDiffInHours($to, false));

            $days = $from->floatDiffInDays($to);
            $diff_in_days = ceil($days);
            $diff_in_weeks = ceil($days / 7);
            $diff_in_month = ceil($from->floatDiffInMonths($to, false));
            $diff_in_year = ceil($from->floatDiffInYears($to, false));

            $costs_array = [];

            $hour_cost = number_format((float)($diff_in_hours * $rate->hour), 2, '.', '');

            array_push($costs_array, $hour_cost);

            $day_cost = number_format((float)($diff_in_days * $rate->day), 2, '.', '');
            array_push($costs_array, $day_cost);


            $week_cost = number_format((float)($diff_in_weeks * $rate->week), 2, '.', '');
            array_push($costs_array, $week_cost);


            $month_cost = number_format((float)($diff_in_month * $rate->month), 2, '.', '');
            array_push($costs_array, $month_cost);


            $year_cost = number_format((float)($diff_in_year * $rate->year), 2, '.', '');
            array_push($costs_array, $year_cost);


            $lowest_rate = number_format((float)(min($costs_array)), 2, '.', '');

            $tax = number_format((float)($lowest_rate * $rate->vat), 2, '.', '');

            $voucher_code_id = Session::get('voucher_code_id');

            if(isset($voucher_code_id)){
                $voucher_code = VoucherCode::find($voucher_code_id);
                $sales = Sale::where('booking_id',$booking_id)->get();
                foreach ($sales as $sale){
                    $sale->discount_percent =$voucher_code->discount_percent;
                    $sale->save();
                }

                $voucher_discount_percent = $voucher_code->discount_percent/100;

                $voucher_discount_price = number_format((float)($lowest_rate * $voucher_discount_percent), 2, '.', '');

                $lowest_rate = number_format((float)($lowest_rate - $voucher_discount_price), 2, '.', '');

                $tax = number_format((float)($lowest_rate * $rate->vat), 2, '.', '');

                $VoucherCodeUser =VoucherCodeUser::where('booking_id',$booking_id)->first();

                if(empty($VoucherCodeUser)){

                    $voucher_code_user = new VoucherCodeUser();
                    $voucher_code_user->user_id = Auth::user()->id;
                    $voucher_code_user->booking_id = $booking_id;
                    $voucher_code_user->code = $voucher_code->code;
                    $voucher_code_user->save();
                }else{

                    $VoucherCodeUser->user_id = Auth::user()->id;
                    $VoucherCodeUser->code = $voucher_code->code;
                    $VoucherCodeUser->save();
                }
            }
            return [$lowest_rate, $tax];

        }
    }

    public function rate_calculation_without_discount($location_group , $block_id, $start , $end )
    {

        $block = Block::find($block_id);
        $rate = $block->rate;

        if ($location_group == 1){

            $unit = number_format((float)($rate->unit), 2, '.', '');
            $tax = number_format((float)($unit * $rate->vat), 2, '.', '');

            return [$unit,$tax];
        }
        else{

            $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $start)->startOfDay();
            $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $end)->endOfDay();


            $diff_in_hours = ceil($from->floatDiffInHours($to, false));

            $days = $from->floatDiffInDays($to);
            $diff_in_days = ceil($days);
            $diff_in_weeks = ceil($days / 7);
            $diff_in_month = ceil($from->floatDiffInMonths($to, false));
            $diff_in_year = ceil($from->floatDiffInYears($to, false));

            $costs_array = [];

            $hour_cost = number_format((float)($diff_in_hours * $rate->hour), 2, '.', '');

            array_push($costs_array, $hour_cost);

            $day_cost = number_format((float)($diff_in_days * $rate->day), 2, '.', '');
            array_push($costs_array, $day_cost);


            $week_cost = number_format((float)($diff_in_weeks * $rate->week), 2, '.', '');
            array_push($costs_array, $week_cost);


            $month_cost = number_format((float)($diff_in_month * $rate->month), 2, '.', '');
            array_push($costs_array, $month_cost);


            $year_cost = number_format((float)($diff_in_year * $rate->year), 2, '.', '');
            array_push($costs_array, $year_cost);

            $lowest_rate = number_format((float)(min($costs_array)), 2, '.', '');

            $tax = number_format((float)($lowest_rate * $rate->vat), 2, '.', '');

            return [$lowest_rate, $tax];

        }
    }

    public function rate_calculation_edit($location_group , $block_id, $start , $end ,$booking_id,$discount_percentage)
    {

        $block = Block::find($block_id);
        $rate = $block->rate;
//        dd($rate);

        if ($location_group == 1){

            $unit = number_format((float)($rate->unit), 2, '.', '');
            $tax = number_format((float)($unit * $rate->vat), 2, '.', '');

             if(isset($discount_percentage)){

                 $discount_percentage = $discount_percentage/100;
                $discount_price = number_format((float)($rate->unit * $discount_percentage), 2, '.', '');

                $unit = number_format((float)($rate->unit - $discount_price), 2, '.', '');

                $tax = number_format((float)($unit * $rate->vat), 2, '.', '');


            }

            return [$unit, $tax];
        }else{

            $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $start)->startOfDay();
            $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $end)->endOfDay();


            $diff_in_hours = ceil($from->floatDiffInHours($to, false));

            $days = $from->floatDiffInDays($to);
            $diff_in_days = ceil($days);
            $diff_in_weeks = ceil($days / 7);
            $diff_in_month = ceil($from->floatDiffInMonths($to, false));
            $diff_in_year = ceil($from->floatDiffInYears($to, false));

            $costs_array = [];

            $hour_cost = number_format((float)($diff_in_hours * $rate->hour), 2, '.', '');

            array_push($costs_array, $hour_cost);

            $day_cost = number_format((float)($diff_in_days * $rate->day), 2, '.', '');
            array_push($costs_array, $day_cost);


            $week_cost = number_format((float)($diff_in_weeks * $rate->week), 2, '.', '');
            array_push($costs_array, $week_cost);


            $month_cost = number_format((float)($diff_in_month * $rate->month), 2, '.', '');
            array_push($costs_array, $month_cost);


            $year_cost = number_format((float)($diff_in_year * $rate->year), 2, '.', '');
            array_push($costs_array, $year_cost);


            $lowest_rate = number_format((float)(min($costs_array)), 2, '.', '');

            $tax = number_format((float)($lowest_rate * $rate->vat), 2, '.', '');


            if(isset($discount_percentage)){

                $discount_percentage = $discount_percentage/100;
                $discount_price = number_format((float)($lowest_rate * $discount_percentage), 2, '.', '');

                $lowest_rate = number_format((float)($lowest_rate - $discount_price), 2, '.', '');

                $tax = number_format((float)($lowest_rate * $rate->vat), 2, '.', '');


            }
            return [$lowest_rate, $tax];

        }
    }

    public function school_term_reserved($location_group , $block_id, $start , $end)
    {


        $block = Block::find($block_id);
        $rate = $block->rate;

        if ($location_group == 1){
            $now = Carbon::now();
            if ($start < $now) {

                $date = $now->format('F');

                $discount_month = PaymentDiscount::where('rate_id', $rate->id)->first()->toArray();

                if ($date == 'September' || $date == 'July') {
                    $month = strtolower(substr($date, 0, 4));
                } elseif ($date == 'April') {
                    $month = strtolower(substr($date, 0, 5));
                } else {
                    $month = strtolower(substr($date, 0, 3));
                }

                $discount = number_format((float)($discount_month[$month] / 100), 2, '.', '');

                $discount_price = number_format((float)($rate->unit * $discount), 2, '.', '');

                $unit = number_format((float)($rate->unit - $discount_price), 2, '.', '');

                $tax = number_format((float)($unit * $rate->vat), 2, '.', '');

            } else {

                $unit = number_format((float)($rate->unit), 2, '.', '');

                $tax = number_format((float)($unit * $rate->vat), 2, '.', '');

            }

            return [$unit, $tax];
        }else{

            $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $start)->startOfDay();
            $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $end)->endOfDay();


            $diff_in_hours = ceil($from->floatDiffInHours($to, false));

            $days = $from->floatDiffInDays($to);
            $diff_in_days = ceil($days);
            $diff_in_weeks = ceil($days / 7);
            $diff_in_month = ceil($from->floatDiffInMonths($to, false));
            $diff_in_year = ceil($from->floatDiffInYears($to, false));

            $costs_array = [];

            $hour_cost = number_format((float)($diff_in_hours * $rate->hour), 2, '.', '');

            array_push($costs_array, $hour_cost);

            $day_cost = number_format((float)($diff_in_days * $rate->day), 2, '.', '');
            array_push($costs_array, $day_cost);


            $week_cost = number_format((float)($diff_in_weeks * $rate->week), 2, '.', '');
            array_push($costs_array, $week_cost);


            $month_cost = number_format((float)($diff_in_month * $rate->month), 2, '.', '');
            array_push($costs_array, $month_cost);


            $year_cost = number_format((float)($diff_in_year * $rate->year), 2, '.', '');
            array_push($costs_array, $year_cost);


            $lowest_rate = number_format((float)(min($costs_array)), 2, '.', '');

            $tax = number_format((float)($lowest_rate * $rate->vat), 2, '.', '');

            return [$lowest_rate, $tax];

        }
    }

    public function is_locker_available_except_this($id , $locker_id,$start,$end,$book_next_year)
    {
        $available = 1;
        $sales = Sale::where(function ($query) use ($start, $end) {
            $query->where('end', '>=', $start)
                ->Where('start', '<=', $end);
        })
            ->where('locker_id', $locker_id)
            ->where(function ($status) {
                $status->where('status', 'b')
                    ->orWhere('status', 'r');
            })
            ->where('id','<>',$id)
            ->first();

        if (!empty($sales)) {
            $available = 0;
        }

        if ($book_next_year == true) {

            $start_next = Carbon::parse($start)->startOfDay()->addYear()->toDateTimeString();
            $end_next = Carbon::parse($end)->endOfDay()->addYear()->toDateTimeString();

            $sales = Sale::where(function ($query) use ($start_next, $end_next) {
                $query->where('end', '>=', $start_next)
                    ->Where('start', '<=', $end_next);
            })
                ->where('locker_id', $locker_id)
                ->where(function ($status) {
                    $status->where('status', 'b')
                        ->orWhere('status', 'r');
                })
                ->where('id','<>',$id)
                ->first();
//                            dd($sales);

            if (!empty($sales))
                $available = 0;

        }

        return $available;
    }

    public function saveEmailForCron($toAddress,$phone_number, $subject, $email_html,$sms_body){
        $email = new Email();
        $email->email = $toAddress;
        $email->phone_number = $phone_number;
        $email->action = 'sent';
        $email->subject = $subject;
        $email->mail_body = $email_html;
        $email->sms_body = $sms_body;
        $email->date_added = Carbon::now();
        $email->type = 'email';
        $email->result_code = '0';
        $email->star = '0';
        $email->send = '0';
        $email->complete = '0';
        $email->deleted = '0';
        $email->save();
//        $booking_cron = CronSchedule::where('class', 'Email')->where('due','>', Carbon::now())->where('controller', 'LockersController')->where('status', 'Pending')->get();

//        if ($booking_cron->isEmpty() == true) {
//            $cron = new CronSchedule();
//            $cron->class = 'Email';
//            $cron->controller = 'LockersController';
//            $cron->where_check = '0';
//            $cron->due = Carbon::now();
//            $cron->completed = '0';
//            $cron->status = 'Pending';
//            $cron->save();
//        }
    }

    public function saveSMSForCron($phone_number, $subject, $sms_html){
        $email = new Email();
        $email->phone_number = $phone_number;
        $email->action = 'sent';
        $email->subject = $subject;
        $email->mail_body = $sms_html;
        $email->date_added = Carbon::now();
        $email->type = 'SMS';
        $email->result_code = '0';
        $email->star = '0';
        $email->send = '0';
        $email->complete = '0';
        $email->deleted = '0';
        $email->save();
//        $booking_cron = CronSchedule::where('class', 'Email')->where('due','>', Carbon::now())->where('controller', 'LockersController')->where('status', 'Pending')->get();

//        if ($booking_cron->isEmpty() == true) {
//            $cron = new CronSchedule();
//            $cron->class = 'Email';
//            $cron->controller = 'LockersController';
//            $cron->where_check = '0';
//            $cron->due = Carbon::now();
//            $cron->completed = '0';
//            $cron->status = 'Pending';
//            $cron->save();
//        }
    }

    public function sum_of_lockers($booking_id)
    {
        $sales = Sale::where('booking_id', $booking_id)->get();

        $total_lockers_price = 0.00;

        foreach ($sales as $locker) {
            $total_lockers_price += $locker->price;
            $total_lockers_price = number_format($total_lockers_price, 2, '.', '');
        }
     $total_lockers_price;


        return $total_lockers_price;

    }

    public function update_locker_code($sale_id)
    {

        $sale = Sale::where('id', $sale_id)->orderBy('id','desc')->first();

      
                $codes = ManualLockerCode::where('locker_id', $sale->locker_id)->orderBy('id','desc')
            ->where('start','<=', $sale->start)
            // ->where('end','>=' ,$sale->end)
            ->first();

            if (!empty($codes)) {

                if(isset($codes->code)) {
                    Sale::where('id', $sale->id)->update([

                        'net_pass' => substr(sprintf("%04d", $codes->code), -4)
                    ]);
                }

            }


    }

    public function update_saled_locker($sale_id)
    {
        $sale = Sale::where('id', $sale_id)->first();

        $code = $this->manual_locker_code($sale->locker_id,$sale->start,$sale->end);

        Sale::where('id', $sale->id)->update([
            'net_pass' => $code
        ]);
    }

    public function manual_locker_code($locker_id,$start,$end)
    {
        $locker_code = ManualLockerCode::where('locker_id', $locker_id)
            ->where('start','<=', $start)
            // ->where('end','>=' ,$end)
            ->first();

        $code='';
        if (!empty($locker_code))
            $code = substr(sprintf("%04d", $locker_code->code), -4);

          return $code;
    }

    public function create_new_booking($company_id, $block_id, $locker_id, $user_id,$start,$end,$child_first_name,$child_surname,$school_year_id,$school_academic_year_id,$pupil_premium=0,$confrimation_email) {

        $block = Block::find($block_id);
        $company =Company::find($company_id);
        $location_group = $company->location_group_id;

        $booking_cost = $this->rate_calculation_without_discount($location_group , $block_id, $start , $end);
        $price = number_format((float)($booking_cost[0]) , 2, '.', '');
        $tax = number_format((float)($booking_cost[1]) , 2, '.', '');

        $user = User::find($user_id);
        $locker = Locker::find($locker_id);

        $code = $this->manual_locker_code($locker_id,$start,$end);

        // if($confrimation_email)
        // {
        //     $code=0;
        // }else{
        //     $code=1;
        // }
        $sale = new Sale();
        $sale->user_id = $user->id;
        $sale->booking_type = 1;
        $sale->lang = 'EN';
        $sale->title = 'LOCKER_M';
        $sale->lock_type = 'm';
        $sale->company_id = $company_id;
        $sale->locker_id = $locker_id;
        $sale->net_pass = $code;
        $sale->number = $locker->locker_number;
        $sale->block_id = $block_id;
        $sale->block_name = $block->name;
        $sale->start = $start;
        $sale->end = $end;
        $sale->email = $user->email;
        $sale->price = $price;
        $sale->price_original = $price;
        $sale->tax =  $tax;
        $sale->status = 'b';
        $sale->hold_booking_date = Carbon::now()->addHour();
        $sale->confirmation_sent = 0;
        $sale->delete = 0;
        $sale->street = $user->street;
        $sale->city = $user->city;
        $sale->post_code = $user->post_code;
        $sale->country = $user->country;
        $sale->child_first_name =$child_first_name;
        $sale->child_surname = $child_surname;
        $sale->school_year_id = $school_year_id;
        $sale->school_academic_year_id = $school_academic_year_id;
        $sale->pupil_premium = $pupil_premium;
        $sale->save();

        $sale_id = $sale->id;
        Sale::where('id', $sale_id)->update(['booking_id' => $sale_id]);

        //add sales_audit log
        $sales_audit_obj = new SalesAudit();
        $sales_audit_obj->sales_audit_log($sale->id);

        $booking_id = $sale->id;
        $billing_address1 = $user->address_1;
        $billing_address2 = $user->address_2;
        $city = $user->city;
        $country = $user->country;
        $first_name = $user->name;
        $phone_number = $user->phone_number;
        $post_code = $user->post_code;
        $state = $user->state;
        $last_name = $user->surname;
        $user_email = $user->email;
        $currency = 'GBP';


        $payment_id = $this->insert_payments_pupil_premium($price,$user_id, $booking_id, $billing_address1, $billing_address2, $city, $country, $first_name, $phone_number, $post_code, $state, $last_name, $currency, $user_email);


        $cardNumber = null;
        $type = '07';
        $response_status_code = '0000';
        $status_message = 'Manual Invoice';
        $vendorTxCode = 'lockers_' . Carbon::now()->format('Ymdhis') . '_' . $booking_id;
        $fraud_score=0;
        $transactionId=null;
        $waiting_response_sagepay=1;

        $payment_obj = new Payment();
        $payment_obj->insert_transactions($booking_id, $payment_id, $user_id, $currency, $cardNumber, $price, $type, $response_status_code, $vendorTxCode, $status_message, $fraud_score, $transactionId,$waiting_response_sagepay);
    }

    public function insert_payments_pupil_premium($amount,$user_id, $booking_id, $billing_address1, $billing_address2, $city, $country, $first_name, $phone_number, $post_code, $state, $last_name, $currency, $user_email)
    {
        $payment = new Payment();
        $payment->amount = $amount;
        $payment->account_balance = $amount;
        $payment->user_id = $user_id;
        $payment->booking_id = $booking_id;
        $payment->billing_address1 = $billing_address1;
        $payment->billing_address2 = $billing_address2;
        $payment->billing_city = $city;
        $payment->billing_country = $country;
        $payment->billing_first_name = $first_name;
        $payment->billing_phone = $phone_number;
        $payment->billing_post_code = $post_code;
        $payment->billing_state = $state;
        $payment->billing_surname = $last_name;
        $payment->currency = $currency;
        $payment->created = Carbon::now();
        $payment->customer_email = $user_email;
        $payment->has_transaction = 1;
        $payment->save();

        return $payment->id;


    }

}
