<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Franchisee extends Model
{
    protected $fillable = [
        'name'
    ];

    public function franchisee_companies()
    {
        return $this->hasMany('App\FranchiseeCompany','franchisee_id');

    }
     public function franchisee_companies_specific()
    {
        return $this->belongsTo('App\FranchiseeCompany','franchisee_id');

    }
    public function companies()
    {
        return $this->hasMany('App\Company','territory_code');

    }
    public function users()
    {
        return $this->belongsToMany('App\User');

    }

}
