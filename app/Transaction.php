<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public function payment()
    {
        return $this->belongsTo('App\Payment');
    }
    public function sale()
    {
        return $this->belongsTo('App\Sale','booking_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function getAmount($id, $type){
        $transactions = Transaction::where('booking_id', $id)->get();
          $sum = 0;
          if($type== 'l'){
             foreach ($transactions as $key => $value) {
               if($value->type == '01' || $value->type == '02'){
                if($value->status == '0000'){
                   $sum += floatval($value->amount);
                }  
               }
               if($value->type == '05'){
                $sum = $sum - floatval($value->amount_change);
               }
              }
          } else if($type =='c'){
             foreach ($transactions as $key => $value) {
               if($value->type == '03'){
                if($value->status == '0000'){
                  $sum += floatval($value->amount);
                }
               }
               if($value->type == '04'){
                $sum = $sum - floatval($value->amount_change);
               }
              }
          }
          if($sum < 0){
            $sum = 0;
          }
          return $sum;
    }

    public function getOutstaningBalance($id){
        
        $transactions = Transaction::where('booking_id',$id)
        ->orderBy('id','asc')
        ->get();
        $refund_balance = (float) 0.00;
        $account_balance = (float) 0.00;
        foreach ($transactions as $key=>$value) {
            if($value->type == 01 || $value->type == 02 || $value->type == 03){
              if($value->status == '0000'){
                $account_balance += $value->amount;
              }
            }
            if($value->type == 04 || $value->type == 05){   
                $refund_balance += $value->amount_change;
            }
            if($value->type == 06){
              if($value->status == '0000'){
                $refund_balance = $refund_balance - $value->refund;
                $account_balance = $account_balance - $value->refund;
              }
            }
            $transactions[$key]['refund_balance'] = number_format($refund_balance, 2);
            $transactions[$key]['account_balance'] = number_format($account_balance, 2);

        }

        $data = $transactions->sortByDesc('id');


        return $data;
    } 

    public function getAdjustments($id){
      $transactions = Transaction::where('booking_id', $id)->orderBy('id','asc')
        ->get();

        $amount = (float) 0.00;

        foreach ($transactions as $key => $value) {
         if($value->type == 04 || $value->type == 05){   
                $amount += $value->amount_change;
            }
        }

        return $amount;
    }

}
