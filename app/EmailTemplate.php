<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    protected $fillable = [
        'template_name', 'subject', 'template_text', 'sms_message',
    ];
}
