<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolYear extends Model
{
    protected $table = 'school_year';
    protected $fillable = ['title','from_date','to_date','status'];

    protected $dates = ['from_date', 'to_date'];
}
