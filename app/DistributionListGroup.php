<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DistributionListGroup extends Model
{
    protected $fillable = [
        'name'
    ];

    public function emailTemplateDistributionLists()
    {
        return $this->hasMany('App\EmailTemplateDistributionList');

    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }
}
