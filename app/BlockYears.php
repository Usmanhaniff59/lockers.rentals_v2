<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlockYears extends Model
{
    protected $table = "block_years";

    public function block()
    {
        return $this->belongsTo('App\Block','block_id');

    }
}
