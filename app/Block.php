<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Block extends Model
{
     protected $fillable = [
        'company_id', 'lock_type', 'order_list', 'location_group_id', 'rate_id', 'block_height', 'locker_blocks',  'name','block_type', 'street', 'active', 'display','city','country','post_code','locker_block_image_id','resend_instruction','school_academic_year_id','block_group','color_locker'
    ];

    public function company()
    {
        return $this->belongsTo('App\Company','company_id');
    }
    public function block_errors()
    {
        return $this->hasMany('App\CompanyBlockError','block_id');

    }
    public function sales()
    {
        return $this->hasMany('App\Sale','block_id','id');

    }
    public function lockers()
    {
        return $this->hasMany('App\Locker','block_id');

    }
    public function maintenances()
    {
        return $this->hasMany('App\Maintenance','block_id');

    }
    public function blockYears()
    {
        return $this->hasMany('App\BlockYears','block_id');

    }
    public function rate()
    {
        return $this->belongsTo('App\Rate','rate_id','id');

    }
     public function block_group()
    {
        return $this->belongsTo('App\BlockGroup','block_group','id');

    }
         public function BlockGroupCode()
    {
        //  return $this->belongsTo('App\BlockGroup','block_group','id');
        return $this->hasMany('App\BlockGroupCode','block_group_id','block_group');

    }
    public function outer_blocks_record_permissions($blocks)
    {

        $user = Auth::user();
        $franchisees = $user->franchisee()->pluck('franchisee_id')->toArray();

        if(auth()->user()->can('All Records')) {

        }elseif( Auth::user()->company_id && $franchisees) {
            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id',$franchisees)->pluck('company_id')->toArray();
            array_push($franchisee_companies, auth()->user()->company_id);
            $blocks->Where(function ($query) use ($franchisee_companies) {
                $query->WhereIn('blocks.company_id',$franchisee_companies);
                    // ->orWhere('blocks.company_id',Auth::user()->company_id);
            });

        }
        elseif( $franchisees ) {
            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id',$franchisees)->pluck('company_id')->toArray();

            $blocks->whereIn('blocks.company_id',$franchisee_companies);
        }
        elseif( Auth::user()->company_id ){

            $blocks->where('blocks.company_id',Auth::user()->company_id);
        }else{
            $blocks->where('blocks.id',0);
        }

        return $blocks;

    }
}
