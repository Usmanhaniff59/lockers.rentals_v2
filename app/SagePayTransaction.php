<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SagePayTransaction extends Model
{
    protected $table  = 'sage_pay_transactions';
}
