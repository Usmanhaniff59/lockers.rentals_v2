<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\SchoolYear;
use App\Locker;
use App\BlockGroup;
use App\Block;
use App\BlockGroupCode;
use App\Traits\UniqueBlockGroupCode;
use Carbon\Carbon;


class ManualLockerCode extends Model
{
    use UniqueBlockGroupCode;
    public function locker()
    {
        return $this->belongsTo('App\Locker');
    }

    public function manual_locker_codes_record_permissions($locker)
    {

        $user = Auth::user();
        $franchisees = $user->franchisee()->pluck('franchisee_id')->toArray();

        if(auth()->user()->can('All Records')) {


        }elseif(Auth::user()->company_id && $franchisees) {


            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id',$franchisees)->pluck('company_id')->toArray();
            if(!empty($franchisee_companies)) {
                $locker->Where(function ($query) use ($franchisee_companies) {
                    $query->WhereIn('lockers.company_id', $franchisee_companies)
                        ->orWhere('lockers.company_id', Auth::user()->company_id);
                });

            } else {
                $locker->Where(function ($query) use ($franchisee_companies) {
                    $query->Where('lockers.company_id', Auth::user()->company_id);
                });
            }

        }
        elseif( $franchisees ) {
            $franchisee_companies = FranchiseeCompany::whereIn('franchisee_id',$franchisees)->pluck('company_id')->toArray();
            $locker->whereIn('lockers.company_id',$franchisee_companies);
        }
        elseif( Auth::user()->company_id ){
            $locker->where('lockers.company_id',Auth::user()->company_id);
        }else{
            $locker->where('lockers.company_id',0);
        }
        return $locker;

    }


    // check whether this locker number has assigned any blockgroup 
    // if yes than we need to check that any other block group is available or not
    // if any block group is available => assign that block group 
    // if we are not able to find any block group ... we need to create a separtae blockgroup for that 
    // and assign that block group to this block.
    public function assign_blockGroup($companyId,$blockId,$lockerNumber,$lockerId)
    {

        // find that this company 
        $blocks=Locker::with(['block'=>function($blockQuery){
            return $blockQuery->select('block_group')->whereNotNull('block_group');
        }])->where([['locker_number',$lockerNumber],['company_id',$companyId],['id','!=',$lockerId]])->get();
       if(isset($blocks) && !empty($blocks) && count($blocks)>0){
              

        foreach ($blocks as $key => $block) {
            if(isset($block->block)){
                $blockGroups=BlockGroup::whereNotIn('id',$block->block)->orderBy('id', 'ASC')->first();
            }else{
                $blockGroups=BlockGroup::orderBy('id', 'ASC')->first();
            }
            
            if(!empty($blockGroups) && isset($blockGroups))
            {
                $block=Block::where('id',$blockId)->first();
                $block->block_group=$blockGroups->id;
                $block->save();

                return $blockgroupcodes=BlockGroupCode::where('block_group_id',$blockGroups->id)->get();
            }else{
                // create block group
                $LastBlockGroup=BlockGroup::orderBy('id', 'DESC')->first();
                if(!empty($LastBlockGroup)){
                    $num=$LastBlockGroup->id+1;
                }else{
                     $num=1;
                }

                 $blockgroup = new BlockGroup();
                        $blockgroup->name = 'Block Group '.$num;
                        $blockgroup->order_by = 0;
                        $blockgroup->save();
                        $this->CreateBlockGroupCode($blockgroup->id);
                         $block=Block::where('id',$blockId)->first();
                         $block->block_group=$blockgroup->id;
                         $block->save();
                return $blockgroupcodes=BlockGroupCode::where('block_group_id',$blockgroup->id)->get();


            }

        }
            }else{
            
                $blockGroups=BlockGroup::orderBy('id', 'ASC')->first();   
                 if(!empty($blockGroups) && isset($blockGroups))
                    {
                        $block=Block::where('id',$blockId)->first();
                        $block->block_group=$blockGroups->id;
                        $block->save();

                        return $blockgroupcodes=BlockGroupCode::where('block_group_id',$blockGroups->id)->get();
                    }else{
                        // create block group
                        $LastBlockGroup=BlockGroup::orderBy('id', 'DESC')->first();
                        if(!empty($LastBlockGroup)){
                            $num=$LastBlockGroup->id+1;
                        }else{
                             $num=1;
                        }

                         $blockgroup = new BlockGroup();
                                $blockgroup->name = 'Block Group '.$num;
                                $blockgroup->order_by = 0;
                                $blockgroup->save();
                                $this->CreateBlockGroupCode($blockgroup->id);
                                 $block=Block::where('id',$blockId)->first();
                                 $block->block_group=$blockgroup->id;
                                 $block->save();
                        return $blockgroupcodes=BlockGroupCode::where('block_group_id',$blockgroup->id)->get();


                    }   
            }


    }

    // function to assign manaull locker codes
    public function block_Codes($blockGroup='',$lockersArray,$companyId,$blockId,$year)
    {

        if(isset($blockGroup))
        {
             $blockgroupcodes=BlockGroupCode::where([['block_group_id',$blockGroup],['start_date',$year->from_date]])->get();
             if(count($blockgroupcodes)>0)
             {
              return $blockgroupcodes;
             }else{
               return $BlockGroupCodes=$this->checkBlockGroupAssign($blockGroup,$blockId,$year);
             }
        }else{
            // $lockerIdArray=[];
            $lockerIdArray=$this->ArrayLockerIds($lockersArray);
            
              $blocks=Locker::where([['company_id',$companyId],['block_id','!=',$blockId],['active',1],['display',1]])->whereIn('locker_number',$lockerIdArray)->select('block_id')->groupBy('block_id')->get();
             
              $blockGroup=Block::whereIn('id',$blocks)->where([['active',1],['display',1]])->whereNotNull('block_group')->select('block_group')->get();

              if(count($blockGroup)>0){
              
                if($blockGroup[0]->block_group){
                    $blockGroups=BlockGroup::whereNotIn('id',$blockGroup)->orderBy('id', 'ASC')->first();
                      if(empty($blockGroups))
                          {
                            $blockGroups=BlockGroup::orderBy('id', 'DESC')->first();  
                               if(!empty($blockGroups)){
                                  $num=$blockGroups->id+1;
                              }else{
                                   $num=1;
                              }
                            $blockGroups = new BlockGroup();
                                              $blockGroups->name = 'Block Group '.$num;
                                              $blockGroups->order_by = 0;
                                              $blockGroups->save();

                          }
                    return $BlockGroupCodes=$this->checkBlockGroupAssign($blockGroups->id,$blockId,$year);
                }else{
                      $blockGroups=BlockGroup::orderBy('id', 'ASC')->first();   
                         if(empty($blockGroups))
                          {
                            $blockGroups = new BlockGroup();
                                              $blockGroups->name = 'Block Group 1';
                                              $blockGroups->order_by = 0;
                                              $blockGroups->save();

                          }
                       return $BlockGroupCodes=$this->checkBlockGroupAssign($blockGroups->id,$blockId,$year);
                }
            }else{
                   $blockGroups=BlockGroup::orderBy('id', 'ASC')->first();
                    if(empty($blockGroups))
                    {
                      $blockGroups = new BlockGroup();
                                        $blockGroups->name = 'Block Group 1';
                                        $blockGroups->order_by = 0;
                                        $blockGroups->save();

                    }

                       return $BlockGroupCodes=$this->checkBlockGroupAssign($blockGroups->id,$blockId,$year);
            }
              
              // $blockList=$this->blockArray($blocks);
              // dd($blocks->toArray());
        }
        return 1;
        # code...
    }
    
    public function ArrayLockerIds($lockersArray)
    {

        $id=array();
        foreach ($lockersArray as $key => $locker) {
            array_push($id,$locker->locker_number);
        }
        return $id;
        # code...
    }
    public function checkBlockGroupAssign($blockGroups,$blockId,$year)
    {
             if(!empty($blockGroups) && isset($blockGroups))
                            {
                                $block=Block::where('id',$blockId)->first();
                                $block->block_group=$blockGroups;
                                $block->save();
                              $blockgroupcodes=BlockGroupCode::where([['block_group_id',$blockGroups],['start_date',$year->from_date]])->get();
                              if(count($blockgroupcodes)>0)
                              {
                                return $blockgroupcodes;
                              }else{
                             $this->CreateBlockGroupCode($blockGroups,$year);
                             return $blockgroupcodes=BlockGroupCode::where([['block_group_id',$blockGroups],['start_date',$year->from_date]])->get();
                              }
                            }else{
                                // create block group
                                
                                     $LastBlockGroup=BlockGroup::orderBy('id', 'DESC')->first();
                                        if(!empty($LastBlockGroup)){
                                            $num=$LastBlockGroup->id+1;
                                        }else{
                                             $num=1;
                                        }

                                 $blockgroup = new BlockGroup();
                                        $blockgroup->name = 'Block Group '.$num;
                                        $blockgroup->order_by = 0;
                                        $blockgroup->save();
                                        $this->CreateBlockGroupCode($blockgroup->id,$year);
                                         $block=Block::where('id',$blockId)->first();
                                         $block->block_group=$blockgroup->id;
                                         $block->save();
                                return $blockgroupcodes=BlockGroupCode::where([['block_group_id',$blockgroup->id],['start_date',$year->from_date]])->get();


                            }  
    }


    public function LockerBlockcodes($blockCodes,$lockers,$startDate='',$endDate='',$display='',$blockId)
    {

        foreach ($lockers as $key => $locker) {
            # code...
         // $years = SchoolYear::where('to_date','>=',Carbon::now())->get();
     
                $p=0;

                foreach ($blockCodes as $key => $blockcode) {
                     if($blockcode->locker_number==$locker->locker_number ){ 

                      $endDate=explode(' ', $blockcode->end_date);
                     $endDate1=$endDate[0].' 00:00:00';

                        $previouscode = ManualLockerCode::where([['locker_id',$locker->id],['start',$blockcode->start_date],['end',$endDate1]])->first();                    
                         if(!isset($previouscode) && empty($previouscode)){
                       $block=Block::where('id',$blockId)->first();
                       if($block)
                       {
                        if($block->start_date<$blockcode->end_date){
                          if($block->end_date)
                          {
                            if($blockcode->start_date<$block->end_date && $blockcode->end_date>=$block->end_date ){
                             
                              $code = new ManualLockerCode();
                              $code->code = $blockcode->code;
                              $code->locker_id = $locker->id;
                              $code->block_id = $locker->block_id;
                              $code->start = $blockcode->start_date;
                              $code->end = $endDate1;
                              $code->block_group_id  = $blockcode->block_group_id;
                              $code->save();
                            }
                              // $p=1;
                          }else{
                         
                              $code = new ManualLockerCode();
                              $code->code = $blockcode->code;
                              $code->locker_id = $locker->id;
                              $code->block_id = $locker->block_id;
                              $code->start = $blockcode->start_date;
                              $code->end = $endDate1;
                              $code->block_group_id  = $blockcode->block_group_id;
                              $code->save();
                              // $p=1;
                          }
                        }
                    
                       }
                        
                    }
                   
                }
            // }
        }



    }
    return 1;


}
// below code is for edit block need richard confirmation on this
//  public function LockerBlockcodesEdit($blockCodes,$lockers,$startDate='',$endDate='',$display='',$blockId)
//     {

//         foreach ($lockers as $key => $locker) {
//             # code...
//          // $years = SchoolYear::where('to_date','>=',Carbon::now())->get();
     
//                 $p=0;

//                 foreach ($blockCodes as $key => $blockcode) {
//                      if($blockcode->locker_number==$locker->locker_number ){ 

//                       $endDate=explode(' ', $blockcode->end_date);
//                      $endDate1=$endDate[0].' 00:00:00';

//                         $previouscode = ManualLockerCode::where([['block_id',$blockId],['locker_id',$locker->id],['start',$blockcode->start_date],['end',$endDate1]])->first();                    
//                          if(!isset($previouscode) && empty($previouscode)){
//                        $block=Block::where('id',$blockId)->first();
//                        if($block)
//                        {
//                         if($block->start_date<$blockcode->end_date){
//                           if($block->end_date)
//                           {
//                             if($blockcode->start_date<$block->end_date && $blockcode->end_date>=$block->end_date ){
                             
//                               $code = new ManualLockerCode();
//                               $code->code = $blockcode->code;
//                               $code->locker_id = $locker->id;
//                               $code->block_id = $locker->block_id;
//                               $code->start = $blockcode->start_date;
//                               $code->end = $endDate1;
//                               $code->block_group_id  = $blockcode->block_group_id;
//                               $code->save();
//                             }
//                               // $p=1;
//                           }else{
                         
//                               $code = new ManualLockerCode();
//                               $code->code = $blockcode->code;
//                               $code->locker_id = $locker->id;
//                               $code->block_id = $locker->block_id;
//                               $code->start = $blockcode->start_date;
//                               $code->end = $endDate1;
//                               $code->block_group_id  = $blockcode->block_group_id;
//                               $code->save();
//                               // $p=1;
//                           }
//                         }
                    
//                        }
                        
//                     }
                   
//                 }
//             // }
//         }



//     }
//     return 1;


// }
}
