<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CronSchedule extends Model
{
    protected $fillable = [
        'class	', 'controller', 'where_check', 'due', 'completed', 'status'
    ];
}
