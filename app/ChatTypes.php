<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class ChatTypes extends Model
{
    protected $table = 'chat_types';
}